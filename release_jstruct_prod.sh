#!/bin/bash
# -----------------------------------------------------------------------------------
# release JSTRUCT to production environment
# -----------------------------------------------------------------------------------

echo
echo ------------------------------------------------------------------------------
echo ----------       release JSTRUCT to production environment...       ----------
echo ------------------------------------------------------------------------------


echo
echo -- changing to ~/git/jstruct directory [JSTRUCT root, home of pom.xml]
cd /home/russellw/git/jstruct

echo
echo -- undeploying existing production instance
mvn -Pprod tomcat:undeploy

echo
echo -- clean deploy new production instance
mvn -Pprod clean package tomcat:deploy -Dmaven.test.skip=true


echo
echo

