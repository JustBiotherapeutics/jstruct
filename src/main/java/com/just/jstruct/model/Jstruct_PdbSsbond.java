package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing disulfide bond info (SSBOND) PDB Header information (related pdb entries)
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect6.html#SSBOND
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_ssbond", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbSsbond.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbSsbond p "
                                                          + "WHERE p.structVerId = :structVerId"),
    
    @NamedQuery(name = "Jstruct_PdbSsbond.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbSsbond p "
                                                          + "WHERE p.structVerId = :structVerId")
        
})
public class Jstruct_PdbSsbond implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_ssbond_pdb_ssbond_id_seq", sequenceName="pdb_ssbond_pdb_ssbond_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_ssbond_pdb_ssbond_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_ssbond_id")
    private Long pdbSsbondId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Column(name = "ser_num")
    private Integer serNum;
    
    @Size(max = 3)
    @Column(name = "residue_1")
    private String residue1;
    
    @Size(max = 4)
    @Column(name = "chain_id_1")
    private String chainId1;
    
    @Column(name = "seq_num_1")
    private Integer seqNum1;
    
    @Size(max = 1)
    @Column(name = "icode_1")
    private String icode1;
    
    @Size(max = 3)
    @Column(name = "residue_2")
    private String residue2;
    
    @Size(max = 4)
    @Column(name = "chain_id_2")
    private String chainId2;
    
    @Column(name = "seq_num_2")
    private Integer seqNum2;
    
    @Size(max = 1)
    @Column(name = "icode_2")
    private String icode2;
    
    @Size(max = 6)
    @Column(name = "sym_1")
    private String sym1;
    
    @Size(max = 6)
    @Column(name = "sym_2")
    private String sym2;
    
    @Column(name = "bond_length")
    private Double bondLength;
    
    

    public Jstruct_PdbSsbond() {
    }

    public Jstruct_PdbSsbond(Long structVerId, Integer serNum, 
            String residue1, String chainId1, Integer seqNum1, String icode1, 
            String residue2, String chainId2, Integer seqNum2, String icode2, 
            String sym1, String sym2, Double bondLength) {
        this.structVerId = structVerId;
        this.serNum = serNum;
        this.residue1 = residue1;
        this.chainId1 = chainId1;
        this.seqNum1 = seqNum1;
        this.icode1 = icode1;
        this.residue2 = residue2;
        this.chainId2 = chainId2;
        this.seqNum2 = seqNum2;
        this.icode2 = icode2;
        this.sym1 = sym1;
        this.sym2 = sym2;
        this.bondLength = bondLength;
    }


    
    
    public Long getPdbSsbondId() {
        return pdbSsbondId;
    }

    public void setPdbSsbondId(Long pdbSsbondId) {
        this.pdbSsbondId = pdbSsbondId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public Integer getSerNum() {
        return serNum;
    }

    public void setSerNum(Integer serNum) {
        this.serNum = serNum;
    }

    public String getResidue1() {
        return residue1;
    }

    public void setResidue1(String residue1) {
        this.residue1 = residue1;
    }

    public String getChainId1() {
        return chainId1;
    }

    public void setChainId1(String chainId1) {
        this.chainId1 = chainId1;
    }

    public Integer getSeqNum1() {
        return seqNum1;
    }

    public void setSeqNum1(Integer seqNum1) {
        this.seqNum1 = seqNum1;
    }

    public String getIcode1() {
        return icode1;
    }

    public void setIcode1(String icode1) {
        this.icode1 = icode1;
    }

    public String getResidue2() {
        return residue2;
    }

    public void setResidue2(String residue2) {
        this.residue2 = residue2;
    }

    public String getChainId2() {
        return chainId2;
    }

    public void setChainId2(String chainId2) {
        this.chainId2 = chainId2;
    }

    public Integer getSeqNum2() {
        return seqNum2;
    }

    public void setSeqNum2(Integer seqNum2) {
        this.seqNum2 = seqNum2;
    }

    public String getIcode2() {
        return icode2;
    }

    public void setIcode2(String icode2) {
        this.icode2 = icode2;
    }

    public String getSym1() {
        return sym1;
    }

    public void setSym1(String sym1) {
        this.sym1 = sym1;
    }

    public String getSym2() {
        return sym2;
    }

    public void setSym2(String sym2) {
        this.sym2 = sym2;
    }

    public Double getBondLength() {
        return bondLength;
    }

    public void setBondLength(Double bondLength) {
        this.bondLength = bondLength;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbSsbondId != null ? pdbSsbondId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbSsbond)) {
            return false;
        }
        Jstruct_PdbSsbond other = (Jstruct_PdbSsbond) object;
        if ((this.pdbSsbondId == null && other.pdbSsbondId != null) || (this.pdbSsbondId != null && !this.pdbSsbondId.equals(other.pdbSsbondId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbSsbond[ pdbSsbondId=" + pdbSsbondId + " ]";
    }
    
}
