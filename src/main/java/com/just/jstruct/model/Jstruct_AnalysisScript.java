package com.just.jstruct.model;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: analysis_script
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "analysis_script", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_AnalysisScript.findById",
                                                    query = "SELECT a FROM Jstruct_AnalysisScript a "
                                                          + " WHERE a.analysisScriptId = :analysisScriptId"),
    
    
    @NamedQuery(name = "Jstruct_AnalysisScript.findByOwnerId", 
                                                    query = "SELECT a FROM Jstruct_AnalysisScript a "
                                                          + " WHERE a.ownerId = :ownerId "
                                                          + "ORDER BY a.scriptName "),
    
    @NamedQuery(name = "Jstruct_AnalysisScript.findByOwnerIdNotDeleted", 
                                                    query = "SELECT a FROM Jstruct_AnalysisScript a "
                                                          + " WHERE a.ownerId = :ownerId "
                                                          + "   AND a.deleteDate IS NULL "
                                                          + "ORDER BY a.scriptName "),
    
    
    @NamedQuery(name = "Jstruct_AnalysisScript.findAllMarkedAsGlobal", 
                                                    query = "SELECT a FROM Jstruct_AnalysisScript a "
                                                          + " WHERE a.isGlobal LIKE 'Y'" 
                                                          + "    OR a.isGlobal LIKE 'y'" 
                                                          + "ORDER BY a.scriptName "),
    
    @NamedQuery(name = "Jstruct_AnalysisScript.findAllMarkedAsGlobalNotDeleted", 
                                                    query = "SELECT a FROM Jstruct_AnalysisScript a "
                                                          + " WHERE (a.isGlobal LIKE 'Y' " 
                                                          + "    OR  a.isGlobal LIKE 'y') "
                                                          + "   AND a.deleteDate IS NULL " 
                                                          + "ORDER BY a.scriptName ")
    
})
public class Jstruct_AnalysisScript implements Serializable 
{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "analysis_script_id")
    private Long analysisScriptId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "script_name")
    private String scriptName;
    
    @Size(max = 2000)
    @Column(name = "description")
    private String description;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_global")
    private Character isGlobal;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "script_type")
    private String scriptType;
    
    @Column(name = "owner_id", insertable=false, updatable=false)
    private Long ownerId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "owner_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    private Jstruct_User owner;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Column(name = "delete_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteDate;

    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    private Jstruct_User insertUser;
    
    @Column(name = "delete_user_id", insertable=false, updatable=false)
    private Long deleteUserId;
    
    @JoinColumn(name = "delete_user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Jstruct_User deleteUser;
    
    
    
    // transient collections of script versions (all, active, defunct)
    @Transient
    private List<Jstruct_AnalysisScriptVer> allScriptVers = new ArrayList<>(10);
    @Transient
    private List<Jstruct_AnalysisScriptVer> activeScriptVers = new ArrayList<>(10);
    @Transient
    private List<Jstruct_AnalysisScriptVer> defunctScriptVers = new ArrayList<>(10);
    
    // transient current script version (only one!)
    @Transient
    private Jstruct_AnalysisScriptVer currentScriptVer;
    
    
    
    public Jstruct_AnalysisScript() {
        this.isGlobal = 'N' ; 
    }

    
    
    
    
    

    public Long getAnalysisScriptId() { return analysisScriptId; }
    public void setAnalysisScriptId(Long analysisScriptId) { this.analysisScriptId = analysisScriptId; }

    public String getScriptName() { return scriptName; }
    public void setScriptName(String scriptName) { this.scriptName = StringUtil.isSet(scriptName) ? scriptName.trim() : null; }
    
    public String getDescription() {return description; }
    public void setDescription(String description) { this.description = StringUtil.isSet(description) ? description.trim() : null; }
    
    public String getScriptType() {return scriptType; }
    public void setScriptType(String scriptType) { this.scriptType = scriptType; }
    
    public boolean isGlobal() { return isGlobal.equals('Y') || isGlobal.equals('y');  }
    public void setGlobal(boolean isGlobal) { this.isGlobal = isGlobal ? 'Y' : 'N' ; }
    
    

    public long getOwnerId() {return ownerId;}
    public void setOwnerId(long ownerId) { this.ownerId = ownerId;}
    
    public Jstruct_User getOwner() {return owner;}
    public void setOwner(Jstruct_User owner) { this.owner = owner;}
    
    public Date getInsertDate() { return insertDate; }
    public void setInsertDate(Date insertDate) { this.insertDate = insertDate; }

    public Date getDeleteDate() { return deleteDate; }
    public void setDeleteDate(Date deleteDate) { this.deleteDate = deleteDate;}

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return deleteUserId;}
    public void setUpdateUserId(long deleteUserId) {this.deleteUserId = deleteUserId;}
    
    public Jstruct_User getDeleteUser() {return deleteUser;}
    public void setDeleteUser(Jstruct_User deleteUser) { this.deleteUser = deleteUser;}
    
    
    // transient
    public List<Jstruct_AnalysisScriptVer> getAllScriptVers() { return allScriptVers; }
    public List<Jstruct_AnalysisScriptVer> getActiveScriptVers() { return activeScriptVers; }
    public List<Jstruct_AnalysisScriptVer> getDefunctScriptVers() { return defunctScriptVers; }
    public Jstruct_AnalysisScriptVer getCurrentScriptVer() { return currentScriptVer;}
    public void addScriptVer(Jstruct_AnalysisScriptVer scriptVer)
    {
        if(scriptVer != null){
            this.allScriptVers.add(scriptVer);

            switch(scriptVer.getStatus())
            {
                case Jstruct_AnalysisScriptVer.STATUS_ACTIVE:
                    activeScriptVers.add(scriptVer);
                    break;
                case Jstruct_AnalysisScriptVer.STATUS_CURRENT:
                    currentScriptVer = scriptVer;
                    break;
                case Jstruct_AnalysisScriptVer.STATUS_DEFUNCT:
                    defunctScriptVers.add(scriptVer);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid AnalysisScriptVer status: " + scriptVer.getStatus());
            }
        }
    }
    public void addScriptVers(List<Jstruct_AnalysisScriptVer> scriptVers)
    {
        if(CollectionUtil.hasValues(scriptVers))
        {
            for(Jstruct_AnalysisScriptVer scriptVer : scriptVers){
                addScriptVer(scriptVer); 
            }
        }
    }
    
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (analysisScriptId != null ? analysisScriptId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_AnalysisScript)) {
            return false;
        }
        Jstruct_AnalysisScript other = (Jstruct_AnalysisScript) object;
        if ((this.analysisScriptId == null && other.analysisScriptId != null) || (this.analysisScriptId != null && !this.analysisScriptId.equals(other.analysisScriptId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_AnalysisScript[ analysisScriptId=" + analysisScriptId + " ]";
    }
    
}
