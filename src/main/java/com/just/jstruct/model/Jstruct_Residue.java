package com.just.jstruct.model;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.AtomGroup;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: residue
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "residue", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_Residue.findByResidueId", 
                                                        query = "SELECT r FROM Jstruct_Residue r "
                                                              + " WHERE r.residueId = :residueId "),
    
    @NamedQuery(name = "Jstruct_Residue.findByAtomGroupingId", 
                                                        query = "SELECT r FROM Jstruct_Residue r "
                                                              + " WHERE r.atomGroupingId = :atomGroupingId "
                                                              + "ORDER BY r.resSeqNumber ASC, "
                                                              + "         r.residueId ASC "),
    
    @NamedQuery(name = "Jstruct_Residue.deleteByAtomGroupingId", 
                                                        query = "DELETE FROM Jstruct_Residue r "
                                                              + " WHERE r.atomGroupingId = :atomGroupingId ")
})
public class Jstruct_Residue implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="residue_residue_id_seq", sequenceName="residue_residue_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="residue_residue_id_seq")
    @Basic(optional = false)
    @Column(name = "residue_id")
    private Long residueId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "abbrev")
    private String abbrev;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "residue_type")
    private String residueType;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "res_seq_number")
    private int resSeqNumber;
    
    @Column(name = "icode")
    private Character icode;
    
    @Size(max = 2147483647)
    @Column(name = "atoms")
    private String atoms;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "has_all_coords")
    private Character hasAllCoords;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "has_any_coords")
    private Character hasAnyCoords;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "has_ca_coords")
    private Character hasCaCoords;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "has_bkbn_coords")
    private Character hasBkbnCoords;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "has_bkbn_cb_coords")
    private Character hasBkbnCbCoords;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "atom_grouping_id")
    private Long atomGroupingId;
    
    
    

    public Jstruct_Residue() {
    }

   /**
     * create a new Jstruct_Residue based on a ResidueObject (see CoreStruct) and it's related atomGroupingId
     * @param residueObj 
     * @param atomGroupingId 
     */
    public Jstruct_Residue(AtomGroup residueObj, Long atomGroupingId) {
        
        this.atomGroupingId = atomGroupingId;
        
        this.residueType = residueObj.getType().name();
        this.abbrev = residueObj.name();
        
        if(residueObj.getResidueNum() != null) {
            this.resSeqNumber = residueObj.getResidueNum();
        } else {
            this.resSeqNumber = -1;
        }
        
        this.icode = residueObj.getInsertCode();

        this.hasAllCoords = residueObj.hasFullCoordinates() ? 'Y' : 'N';
        this.hasAnyCoords = residueObj.hasAnyCoordinates() ? 'Y' : 'N';
        this.hasCaCoords = residueObj.hasCaCoordinates() ? 'Y' : 'N';
        this.hasBkbnCoords = residueObj.hasBackboneCoordinates() ? 'Y' : 'N';
        this.hasBkbnCbCoords = residueObj.hasBackboneCbCoordinates() ? 'Y' : 'N';

        if(CollectionUtil.hasValues(residueObj.getAtomNames())){
            this.atoms = StringUtil.join(residueObj.getAtomNames(), ",");
        }
        
    }
    
    
    
    public Long getResidueId() {
        return residueId;
    }

    public void setResidueId(Long residueId) {
        this.residueId = residueId;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    public String getResidueType() {
        return residueType;
    }

    public void setResidueType(String residueType) {
        this.residueType = residueType;
    }

    public int getResSeqNumber() {
        return resSeqNumber;
    }

    public void setResSeqNumber(int resSeqNumber) {
        this.resSeqNumber = resSeqNumber;
    }

    public Character getIcode() {
        return icode;
    }

    public void setIcode(Character icode) {
        this.icode = icode;
    }
    
    

    public String getAtoms() {
        return atoms;
    }

    public void setAtoms(String atoms) {
        this.atoms = atoms;
    }

    
    public boolean hasAllCoords() { return hasAllCoords.equals('Y') || hasAllCoords.equals('y'); }
    public void setHasAllCoords(boolean hasAllCoords) { this.hasAllCoords = hasAllCoords ? 'Y' : 'N' ; }
    
    public boolean hasAnyCoords() { return hasAnyCoords.equals('Y') || hasAnyCoords.equals('y'); }
    public void setHasAnyCoords(boolean hasAnyCoords) { this.hasAnyCoords = hasAnyCoords ? 'Y' : 'N' ; }
    
    
    public boolean hasCaCoords() { return hasCaCoords.equals('Y') || hasCaCoords.equals('y'); }
    public void setHasCaCoords(boolean hasCaCoords) { this.hasCaCoords = hasCaCoords ? 'Y' : 'N' ; }
    
    public boolean hasBkbnCoords() { return hasBkbnCoords.equals('Y') || hasBkbnCoords.equals('y'); }
    public void setHasBkbnCoords(boolean hasBkbnCoords) { this.hasBkbnCoords = hasBkbnCoords ? 'Y' : 'N' ; }
    
    public boolean hasBkbnCbCoords() { return hasBkbnCbCoords.equals('Y') || hasBkbnCbCoords.equals('y'); }
    public void setHasBkbnCbCoords(boolean hasBkbnCbCoords) { this.hasBkbnCbCoords = hasBkbnCbCoords ? 'Y' : 'N' ; }
    
  


    public Long getAtomGroupingId() {
        return atomGroupingId;
    }

    public void setAtomGroupingId(Long atomGroupingId) {
        this.atomGroupingId = atomGroupingId;
    }
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (residueId != null ? residueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_Residue)) {
            return false;
        }
        Jstruct_Residue other = (Jstruct_Residue) object;
        if ((this.residueId == null && other.residueId != null) || (this.residueId != null && !this.residueId.equals(other.residueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_Residue[ residueId=" + residueId + " ]";
    }
    
}
