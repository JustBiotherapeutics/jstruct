package com.just.jstruct.model;

import com.just.bio.structure.enums.ChainType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * Entity class for: SEQUENCES_ATOM_VIEW
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "sequences_atom_view", schema = "public")
@NamedQueries({

    @NamedQuery(name = "Jstruct_SequencesAtom.findByStructVerIds", 
                                                query = "SELECT sa FROM Jstruct_SequencesAtom sa "
                                                      + " WHERE sa.structVerId IN (:structVerIds) "
                                                      + " ORDER BY sa.structureId, sa.structVerId, sa.chainName "),
    
    @NamedQuery(name = "Jstruct_SequencesAtom.findAllStructVerIds", 
                                                query = "SELECT distinct sa.structVerId FROM Jstruct_SequencesAtom sa ")
    
    
})
public class Jstruct_SequencesAtom implements Serializable, Jstruct_SequencesView {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @Column(name = "atom_grouping_id")
    private Long atomGroupingId;
    
 //   @Size(max = 255)
 //   @Column(name = "atom_grouping_type")
 //   private String atomGroupingType;
    
    //@Column(name = "seq_with_all_coords")
    //private String seqWithAllCoords;
    
   
    @Column(name = "seq_with_any_coords")
    private String seqWithAnyCoords;
     /*
    @Column(name = "seq_with_ca_coords")
    private String seqWithCaCoords;
    
    @Column(name = "seq_with_bkbn_coords")
    private String seqWithBkbnCoords;
    
    @Column(name = "seq_with_bkbn_cb_coords")
    private String seqWithBkbnCbCoords;
    */
                    
    @Column(name = "from_rcsb")
    private Character fromRcsb;
    
//    @Size(max = 255)
//    @Column(name = "source_name")
//    private String sourceName;
    
    @Column(name = "is_obsolete")
    private Character isObsolete;
    
//    @Column(name = "obsolete_date")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date obsoleteDate;
    
    
    @Column(name = "structure_id")
    private Long structureId;
    
    @Column(name = "version")
    private Integer version;
    
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    @Column(name = "title")
    private String title;
    
    @Size(max = 4)
    @Column(name = "pdb_identifier")
    private String pdbIdentifier;
    
    @Size(max = 255)
    @Column(name = "chain_name")
    private String chainName;
    
    @Size(max = 255)
    @Column(name = "chain_type")
    private String chainType;
    
//    @Column(name = "residue_count")
//    private Integer residueCount;
    
    

    public Jstruct_SequencesAtom() {
    }
    
    
    
    
    @Override
    public boolean isFromRcsb() { return fromRcsb.equals('Y') || fromRcsb.equals('y'); }
    @Override
    public void setFromRcsb(boolean fromRcsb) { this.fromRcsb = fromRcsb ? 'Y' : 'N' ; }

    //@Override
    //public String getSourceName() { return sourceName; }
    //@Override
    //public void setSourceName(String sourceName) { this.sourceName = sourceName; }

    @Override
    public boolean isObsolete() { return isObsolete.equals('Y') || isObsolete.equals('y'); }
    @Override
    public void setObsolete(boolean isObsolete) { this.isObsolete = isObsolete ? 'Y' : 'N' ; }
    
    //@Override
    //public Date getObsoleteDate() {  return obsoleteDate; }
    //@Override
    //public void setObsoleteDate(Date obsoleteDate) { this.obsoleteDate = obsoleteDate; }
    

    @Override
    public Long getStructureId() {
        return structureId;
    }

    @Override
    public void setStructureId(Long structureId) {
        this.structureId = structureId;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public Long getStructVerId() {
        return structVerId;
    }

    @Override
    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getPdbIdentifier() {
        return pdbIdentifier;
    }

    @Override
    public void setPdbIdentifier(String pdbIdentifier) {
        this.pdbIdentifier = pdbIdentifier;
    }

    @Override
    public String getChainName() {
        return chainName;
    }

    @Override
    public void setChainName(String chainName) {
        this.chainName = chainName;
    }
    
    @Override
    public ChainType getChainType() {
        return ChainType.valueOf(chainType);
    }

    @Override
    public void setChainType(ChainType chainType) {
        this.chainType = chainType.name();
    }
    
    //@Override
    //public Integer getResidueCount() {
    //    return residueCount;
    //}

    //@Override
    //public void setResidueCount(Integer residueCount) {
    //    this.residueCount = residueCount;
    //}

    public Long getAtomGroupingId() {
        return atomGroupingId;
    }

    public void setAtomGroupingId(Long atomGroupingId) {
        this.atomGroupingId = atomGroupingId;
    }

    //public String getAtomGroupingType() {
    //    return atomGroupingType; 
    //}

    //public void setAtomGrouping(String atomGroupingType) {
    //    this.atomGroupingType = atomGroupingType; 
    //}
/*
    public String getSeqWithAllCoords() {
        return seqWithAllCoords;
    }

    public void setSeqWithAllCoords(String seqWithAllCoords) {
        this.seqWithAllCoords = seqWithAllCoords;
    }
*/
    public String getSeqWithAnyCoords() {
        return seqWithAnyCoords;
    }

    public void setSeqWithAnyCoords(String seqWithAnyCoords) {
        this.seqWithAnyCoords = seqWithAnyCoords;
    }
/*
    public String getSeqWithCaCoords() {
        return seqWithCaCoords;
    }

    public void setSeqWithCaCoords(String seqWithCaCoords) {
        this.seqWithCaCoords = seqWithCaCoords;
    }

    public String getSeqWithBkbnCoords() {
        return seqWithBkbnCoords;
    }

    public void setSeqWithBkbnCoords(String seqWithBkbnCoords) {
        this.seqWithBkbnCoords = seqWithBkbnCoords;
    }

    public String getSeqWithBkbnCbCoords() {
        return seqWithBkbnCbCoords;
    }

    public void setSeqWithBkbnCbCoords(String seqWithBkbnCbCoords) {
        this.seqWithBkbnCbCoords = seqWithBkbnCbCoords;
    }
*/
    @Override
    public String getPrimaryFastaSequence() {
        return seqWithAnyCoords;
    }
    
}
