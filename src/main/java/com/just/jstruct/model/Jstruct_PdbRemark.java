package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing remarks (REMARKS) PDB Header information
 *   http://www.wwpdb.org/documentation/file-format-content/format33/remarks.html
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_remark", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbRemark.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbRemark p "
                                                          + " WHERE p.structVerId = :structVerId "
                                                          + "ORDER BY p.remNum ASC "),
    
    @NamedQuery(name = "Jstruct_PdbRemark.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbRemark p "
                                                          + " WHERE p.structVerId = :structVerId ")
        
})
public class Jstruct_PdbRemark implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_remark_pdb_remark_id_seq", sequenceName="pdb_remark_pdb_remark_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_remark_pdb_remark_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_remark_id")
    private Long pdbRemarkId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Column(name = "rem_num")
    private Integer remNum;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "value")
    private String value;
    
    
    

    public Jstruct_PdbRemark() {
    }

    public Jstruct_PdbRemark(Long structVerId, Integer remNum, String name, String value) {
        this.structVerId = structVerId;
        this.remNum = remNum;
        this.name = name;
        this.value = value;
    }

    
    
    
    
    
    public Long getPdbRemarkId() {
        return pdbRemarkId;
    }

    public void setPdbRemarkId(Long pdbRemarkId) {
        this.pdbRemarkId = pdbRemarkId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public Integer getRemNum() {
        return remNum;
    }

    public void setRemNum(Integer remNum) {
        this.remNum = remNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbRemarkId != null ? pdbRemarkId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbRemark)) {
            return false;
        }
        Jstruct_PdbRemark other = (Jstruct_PdbRemark) object;
        if ((this.pdbRemarkId == null && other.pdbRemarkId != null) || (this.pdbRemarkId != null && !this.pdbRemarkId.equals(other.pdbRemarkId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbRemark[ pdbRemarkId=" + pdbRemarkId + " ]";
    }
    
}
