package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing source (SOURCE) PDB Header information
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#SOURCE
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_source", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbSource.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbSource p "
                                                          + " WHERE p.structVerId = :structVerId "
                                                          + "ORDER BY p.molId ASC "),
    
    @NamedQuery(name = "Jstruct_PdbSource.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbSource p "
                                                          + " WHERE p.structVerId = :structVerId ")
        
})
public class Jstruct_PdbSource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_source_pdb_source_id_seq", sequenceName="pdb_source_pdb_source_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_source_pdb_source_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_source_id")
    private Long pdbSourceId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "mol_id")
    private long molId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    
    @Size(min = 1, max = 2147483647)
    @Column(name = "value")
    private String value;
    

    public Jstruct_PdbSource() {
    }

    public Jstruct_PdbSource(Long structVerId, long molId, String name, String value) {
        this.structVerId = structVerId;
        this.molId = molId;
        this.name = name;
        this.value = value;
    }


    
    

    public Long getPdbSourceId() {
        return pdbSourceId;
    }

    public void setPdbSourceId(Long pdbSourceId) {
        this.pdbSourceId = pdbSourceId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public long getMolId() {
        return molId;
    }

    public void setMolId(long molId) {
        this.molId = molId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbSourceId != null ? pdbSourceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbSource)) {
            return false;
        }
        Jstruct_PdbSource other = (Jstruct_PdbSource) object;
        if ((this.pdbSourceId == null && other.pdbSourceId != null) || (this.pdbSourceId != null && !this.pdbSourceId.equals(other.pdbSourceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbSource[ pdbSourceId=" + pdbSourceId + " ]";
    }
    
}
