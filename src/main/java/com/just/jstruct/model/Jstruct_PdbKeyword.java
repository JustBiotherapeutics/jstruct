package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing Keyword/KeyPhrase (KEYWRD) PDB Header information (related pdb entries)
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#KEYWRD
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_keyword", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbKeyword.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbKeyword p "
                                                          + "WHERE p.structVerId = :structVerId"),
    
    @NamedQuery(name = "Jstruct_PdbKeyword.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbKeyword p "
                                                          + "WHERE p.structVerId = :structVerId")
        
})
public class Jstruct_PdbKeyword implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_keyword_pdb_keyword_id_seq", sequenceName="pdb_keyword_pdb_keyword_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_keyword_pdb_keyword_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_keyword_id")
    private Long pdbKeywordId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "value")
    private String value;
    
    
    

    public Jstruct_PdbKeyword() {
    }

    public Jstruct_PdbKeyword(Long structVerId, String value) {
        this.structVerId = structVerId;
        this.value = value;
    }

    
   
    
    
    
    

    public Long getPdbKeywordId() {
        return pdbKeywordId;
    }

    public void setPdbKeywordId(Long pdbKeywordId) {
        this.pdbKeywordId = pdbKeywordId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbKeywordId != null ? pdbKeywordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbKeyword)) {
            return false;
        }
        Jstruct_PdbKeyword other = (Jstruct_PdbKeyword) object;
        if ((this.pdbKeywordId == null && other.pdbKeywordId != null) || (this.pdbKeywordId != null && !this.pdbKeywordId.equals(other.pdbKeywordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbKeyword[ pdbKeywordId=" + pdbKeywordId + " ]";
    }
    
}
