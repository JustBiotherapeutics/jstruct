package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing seqres info (SEQRES) from PDB Primary Structure Section
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect3.html#SEQRES
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_seqres", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbSeqres.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbSeqres p "
                                                          + "WHERE p.structVerId = :structVerId"),
    
    @NamedQuery(name = "Jstruct_PdbSeqres.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbSeqres p "
                                                          + "WHERE p.structVerId = :structVerId")
        
})
public class Jstruct_PdbSeqres implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_seqres_pdb_seqres_id_seq", sequenceName="pdb_seqres_pdb_seqres_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_seqres_pdb_seqres_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_seqres_id")
    private Long pdbSeqresId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Size(max = 4)
    @Column(name = "chain_id")
    private String chainId;
    
    @Column(name = "num_res")
    private Integer numRes;
    
    @Size(max = 2147483647)
    @Column(name = "residues")
    private String residues;
    
    

    public Jstruct_PdbSeqres() {
    }

    public Jstruct_PdbSeqres(Long structVerId, String chainId, Integer numRes, String residues) {
        this.structVerId = structVerId;
        this.chainId = chainId;
        this.numRes = numRes;
        this.residues = residues;
    }
    

 
    
    public Long getPdbSeqresId() {
        return pdbSeqresId;
    }

    public void setPdbSeqresId(Long pdbSeqresId) {
        this.pdbSeqresId = pdbSeqresId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public Integer getNumRes() {
        return numRes;
    }

    public void setNumRes(Integer numRes) {
        this.numRes = numRes;
    }

    public String getResidues() {
        return residues;
    }

    public void setResidues(String residues) {
        this.residues = residues;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbSeqresId != null ? pdbSeqresId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbSeqres)) {
            return false;
        }
        Jstruct_PdbSeqres other = (Jstruct_PdbSeqres) object;
        if ((this.pdbSeqresId == null && other.pdbSeqresId != null) || (this.pdbSeqresId != null && !this.pdbSeqresId.equals(other.pdbSeqresId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbSeqres[ pdbSeqresId=" + pdbSeqresId + " ]";
    }
    
}
