package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: analysis_file
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "analysis_file", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_AnalysisFile.findById", 
                                                query = "SELECT af FROM Jstruct_AnalysisFile af "
                                                      + " WHERE af.analysisFileId = :analysisFileId"),
    
    @NamedQuery(name = "Jstruct_AnalysisFile.findByAnalysisScriptVerId", 
                                                query = "SELECT af FROM Jstruct_AnalysisFile af "
                                                      + " WHERE af.analysisScriptVerId = :analysisScriptVerId")
})
public class Jstruct_AnalysisFile implements Serializable 
{

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name="analysis_file_analysis_file_id_seq", sequenceName="analysis_file_analysis_file_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="analysis_file_analysis_file_id_seq")
    @Basic(optional = false)
    @Column(name = "analysis_file_id")
    private Long analysisFileId;
   
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "file_name")
    private String fileName;
    
    @Column(name = "file_data")
    private byte[] fileData;
    
    //fk to analysis_script_ver
    @Basic(optional = false)
    @NotNull
    @Column(name = "analysis_script_ver_id")
    private Long analysisScriptVerId;
    
    
    
    
    public Jstruct_AnalysisFile() {
    }

    public Jstruct_AnalysisFile(String fileName, byte[] fileData) 
    {
        this.fileName = fileName;
        this.fileData = fileData;
    }
    
    
    

    public Long getAnalysisFileId() {  return analysisFileId;  }
    public void setAnalysisFileId(Long analysisFileId) { this.analysisFileId = analysisFileId; }

    public String getFileName() { return fileName; }
    public void setFileName(String fileName) {  this.fileName = fileName; }

    public byte[] getFileData() {  return fileData; }
    public void setFileData(byte[] fileData) { this.fileData = fileData; }

    public Long getAnalysisScriptVerId() { return analysisScriptVerId; }
    public void setAnalysisScriptVerId(Long analysisScriptVerId) { this.analysisScriptVerId = analysisScriptVerId; }
    
    
    
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (analysisFileId != null ? analysisFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_AnalysisFile)) {
            return false;
        }
        Jstruct_AnalysisFile other = (Jstruct_AnalysisFile) object;
        if ((this.analysisFileId == null && other.analysisFileId != null) || (this.analysisFileId != null && !this.analysisFileId.equals(other.analysisFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_AnalysisFile[ analysisFileId=" + analysisFileId + " ]";
    }
    
}
