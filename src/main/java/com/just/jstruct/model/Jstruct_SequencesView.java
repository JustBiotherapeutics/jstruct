package com.just.jstruct.model;

import com.just.bio.structure.enums.ChainType;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface Jstruct_SequencesView {

    

    //public Jstruct_SequencesView() {
    //}
    
    
    
    public abstract boolean isFromRcsb();
    public abstract void setFromRcsb(boolean fromRcsb);

    //public abstract String getSourceName();
    //public abstract void setSourceName(String sourceName);

    public abstract boolean isObsolete();
    public abstract void setObsolete(boolean isObsolete);
    
    //public abstract Date getObsoleteDate();
    //public abstract void setObsoleteDate(Date obsoleteDate);
    

    public abstract Long getStructureId();
    public abstract void setStructureId(Long structureId);

    public abstract Integer getVersion();
    public abstract void setVersion(Integer version);

    public abstract Long getStructVerId();
    public abstract void setStructVerId(Long structVerId);
    

    public abstract String getTitle();
    public abstract void setTitle(String title);

    public abstract String getPdbIdentifier();
    public abstract void setPdbIdentifier(String pdbIdentifier);

    
    public abstract String getChainName();
    public abstract void setChainName(String chainName);
    
    public abstract ChainType getChainType();
    public abstract void setChainType(ChainType chainType);
    
//    public abstract Integer getResidueCount();
//    public abstract void setResidueCount(Integer residueCount);
    
    
    //Jstruct_SequencesAtom   = seq_with_any_coords
    //Jstruct_SequencesSeqres = seq_theoretical
    public abstract String getPrimaryFastaSequence();

}
