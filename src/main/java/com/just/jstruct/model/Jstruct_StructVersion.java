package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: struct_version
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "struct_version", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_StructVersion.findByStructVerId", 
                                                query = "SELECT sv FROM Jstruct_StructVersion sv "
                                                      + " WHERE sv.structVerId = :structVerId"),
    
    @NamedQuery(name = "Jstruct_StructVersion.findAllStructVerIds", 
                                                query = "SELECT sv.structVerId FROM Jstruct_StructVersion sv "),
    
    @NamedQuery(name = "Jstruct_StructVersion.findManualStructVerIds", 
                                                query = "SELECT sv.structVerId FROM Jstruct_StructVersion sv "
                                                      + " WHERE sv.structureId IN "
                                                      + " ( "
                                                      + "   SELECT s.structureId FROM Jstruct_Structure s "
                                                      + "    WHERE s.fromRcsb LIKE 'N' "
                                                      + " ) "),
  
    @NamedQuery(name = "Jstruct_StructVersion.findActiveRcsbStructVerIds", 
                                                query = "SELECT sv.structVerId FROM Jstruct_StructVersion sv "
                                                      + " WHERE sv.structureId IN "
                                                      + " ( "
                                                      + "   SELECT s.structureId FROM Jstruct_Structure s "
                                                      + "    WHERE s.fromRcsb LIKE 'Y' "
                                                      + "      AND s.isObsolete LIKE 'N' "
                                                      + " ) "),
        
    @NamedQuery(name = "Jstruct_StructVersion.findActiveManualModelStructVerIds", 
                                                query = "SELECT sv.structVerId FROM Jstruct_StructVersion sv "
                                                      + " WHERE sv.structureId IN "
                                                      + " ( "
                                                      + "   SELECT s.structureId FROM Jstruct_Structure s "
                                                      + "    WHERE s.fromRcsb LIKE 'N' "
                                                      + "      AND s.isObsolete LIKE 'N' "
                                                      + " ) "
                                                      + " AND sv.isModel LIKE 'Y' "),
    
    @NamedQuery(name = "Jstruct_StructVersion.findActiveManualNonModelStructVerIds", 
                                                query = "SELECT sv.structVerId FROM Jstruct_StructVersion sv "
                                                      + " WHERE sv.structureId IN "
                                                      + " ( "
                                                      + "   SELECT s.structureId FROM Jstruct_Structure s "
                                                      + "    WHERE s.fromRcsb LIKE 'N' "
                                                      + "      AND s.isObsolete LIKE 'N' "
                                                      + " ) "
                                                      + " AND sv.isModel LIKE 'N' ")
        
        
        
        
    //@NamedQuery(name = "Jstruct_StructVersion.findYoungestByStructureId", 
    //                                            query = "SELECT sv FROM Jstruct_StructVersion sv "
    //                                                  + " WHERE sv.structureId = :structureId "
    //                                                  + "ORDER BY sv.version DESC "
    //                                                  + "LIMIT 1") hibernate not happy with 'LIMIT'
})
public class Jstruct_StructVersion implements Serializable {

        
    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="struct_version_struct_ver_id_seq", sequenceName="struct_version_struct_ver_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="struct_version_struct_ver_id_seq")
    @Basic(optional = false)
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "version")
    private int version;
    
    @Size(max = 255)
    @Column(name = "struct_method")
    private String structMethod;
    
    @Size(max = 255)
    @Column(name = "target")
    private String target;
    
    @Column(name = "is_model")
    private Character isModel;
    
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    
    
    //@JoinColumn(name = "structure_id", referencedColumnName = "structure_id")
    //@ManyToOne(optional = false, fetch = FetchType.LAZY)
    //private Jstruct_Structure structureId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "structure_id")
    private Long structureId;
    

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "program_id")
    private Long programId;
    
    
    
    
    /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    @Column(name = "update_user_id", insertable=false, updatable=false)
    private Long updateUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "update_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User updateUser;
    
    /* --------- end audit fields --------- */
    
    

    
    
    
    
    
    
    public Jstruct_StructVersion() {
    }
    
    
    public Jstruct_StructVersion(Jstruct_User user) {
        this.insertUser = user;
        this.updateUser = user;
        this.insertDate = new Date();
        this.updateDate = new Date();
    }

    

    
    

    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getStructMethod() {
        return structMethod;
    }

    public void setStructMethod(String structMethod) {
        this.structMethod = structMethod;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public boolean isModel() { 
        if(isModel != null){
            return isModel.equals('Y') || isModel.equals('y');
        }
        return false;
    }
    public void setModel(boolean isModel) { this.isModel = isModel ? 'Y' : 'N' ; }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Long getStructureId() {return structureId; }
    public void setStructureId(Long structureId) {this.structureId = structureId; }
    
    public Long getClientId() {return clientId; }
    public void setClientId(Long clientId) {this.clientId = clientId; }
    
    public Long getProgramId() {return programId; }
    public void setProgramId(Long programId) {this.programId = programId; }
    
    
    

    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getUpdateDate() {return updateDate;}
    public void setUpdateDate(Date updateDate) { this.updateDate = updateDate;}

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return updateUserId;}
    public void setUpdateUserId(long updateUserId) {this.updateUserId = updateUserId;}
    
    public Jstruct_User getUpdateUser() {return updateUser;}
    public void setUpdateUser(Jstruct_User updateUser) { this.updateUser = updateUser;}
    
    

    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (structVerId != null ? structVerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_StructVersion)) {
            return false;
        }
        Jstruct_StructVersion other = (Jstruct_StructVersion) object;
        if ((this.structVerId == null && other.structVerId != null) || (this.structVerId != null && !this.structVerId.equals(other.structVerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_StructVersion[ structVerId=" + structVerId + " ]";
    }
    
}
