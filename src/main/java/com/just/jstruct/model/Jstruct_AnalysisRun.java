package com.just.jstruct.model;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: analysis_run
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "analysis_run", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_AnalysisRun.findById", 
                                                query = "SELECT ar FROM Jstruct_AnalysisRun ar "
                                                      + " WHERE ar.analysisRunId = :analysisRunId"),
    
    @NamedQuery(name = "Jstruct_AnalysisRun.findByStatus", 
                                                query = "SELECT ar FROM Jstruct_AnalysisRun ar "
                                                      + " WHERE ar.status = :status"),
    
    @NamedQuery(name = "Jstruct_AnalysisRun.findByAnalysisScriptVerId", 
                                                query = "SELECT ar FROM Jstruct_AnalysisRun ar "
                                                      + " WHERE ar.analysisScriptVerId = :analysisScriptVerId")
})
public class Jstruct_AnalysisRun implements Serializable 
{

    private static final long serialVersionUID = 1L;
    
    public static final String STATUS_NOT_STARTED = "Not Started";
    public static final String STATUS_RUNNING = "Running";
    public static final String STATUS_COMPLETE = "Complete";
    
    
    @Id
    @SequenceGenerator(name="analysis_run_analysis_run_id_seq", sequenceName="analysis_run_analysis_run_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="analysis_run_analysis_run_id_seq")
    @Basic(optional = false)
    @Column(name = "analysis_run_id")
    private Long analysisRunId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "command_pattern")
    private String commandPattern;
    
    @Size(max = 2000)
    @Column(name = "description")
    private String description;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "status")
    private String status;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "total_task_count")
    private int totalTaskCount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "successful_task_count")
    private int successfulTaskCount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "failed_task_count")
    private int failedTaskCount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    
    
    //fk to jstruct_user 
    @Column(name = "user_id", insertable=false, updatable=false)
    private Long userId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Jstruct_User user;
    
    
    //fk to analysis_script_ver
    @Basic(optional = false)
    @NotNull
    @Column(name = "analysis_script_ver_id")
    private Long analysisScriptVerId;
    
    
    
    @JoinColumn(name = "analysis_script_ver_id", referencedColumnName = "analysis_script_ver_id", insertable=false, updatable=false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Jstruct_AnalysisScriptVer analysisScriptVer;
    
    
    
    //transient list of analysis_tasks
    @Transient
    private List<Jstruct_AnalysisTask> analysisTasks = new ArrayList<>(10);
    
    
    
    public Jstruct_AnalysisRun() {
    }

    
   
    
    

    public Long getAnalysisRunId() { return analysisRunId; }
    public void setAnalysisRunId(Long analysisRunId) {  this.analysisRunId = analysisRunId; }

    public String getCommandPattern() { return commandPattern; }
    public void setCommandPattern(String commandPattern) { this.commandPattern = StringUtil.isSet(commandPattern) ? commandPattern.trim() : null; }

    public String getDescription() {  return description; }
    public void setDescription(String description) { this.description = StringUtil.isSet(description) ? description.trim() : null; }

    public String getStatus() { return status; }
    public void setStatus(String status) { this.status = status; }

    public int getTotalTaskCount() { return totalTaskCount; }
    public void setTotalTaskCount(int totalTaskCount) { this.totalTaskCount = totalTaskCount; }

    public int getSuccessfulTaskCount() { return successfulTaskCount; }
    public void setSuccessfulTaskCount(int successfulTaskCount) { this.successfulTaskCount = successfulTaskCount; }

    public int getFailedTaskCount() { return failedTaskCount; }
    public void setFailedTaskCount(int failedTaskCount) { this.failedTaskCount = failedTaskCount; }

    public Date getStartDate() { return startDate; }
    public void setStartDate(Date startDate) { this.startDate = startDate; }

    public Date getEndDate() {  return endDate; }
    public void setEndDate(Date endDate) { this.endDate = endDate; }

    
    public Long getUserId() {return userId;}
    public void setUserId(Long userId) { this.userId = userId;}
    
    public Jstruct_User getUser() {return user;}
    public void setUser(Jstruct_User user) { this.user = user;}

    
    public Long getAnalysisScriptVerId() { return analysisScriptVerId; }
    public void setAnalysisScriptVerId(Long analysisScriptVerId) { this.analysisScriptVerId = analysisScriptVerId; }

    public Jstruct_AnalysisScriptVer getAnalysisScriptVer() { return analysisScriptVer; }
    public void setAnalysisScriptVer(Jstruct_AnalysisScriptVer analysisScriptVer) { this.analysisScriptVer = analysisScriptVer; }
    
    
    public List<Jstruct_AnalysisTask> getAnalysisTasks() {return analysisTasks; }
    public void addAnalysisTask(Jstruct_AnalysisTask analysisTask){
        if(analysisTask != null){
            this.analysisTasks.add(analysisTask);
        }
    }
    public void addAnalysisTasks(List<Jstruct_AnalysisTask> analysisTasks){
        if(CollectionUtil.hasValues(analysisTasks)){
            for(Jstruct_AnalysisTask analysisTask : analysisTasks){
                addAnalysisTask(analysisTask);
            }
        }
    }
    
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (analysisRunId != null ? analysisRunId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_AnalysisRun)) {
            return false;
        }
        Jstruct_AnalysisRun other = (Jstruct_AnalysisRun) object;
        if ((this.analysisRunId == null && other.analysisRunId != null) || (this.analysisRunId != null && !this.analysisRunId.equals(other.analysisRunId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_AnalysisRun[ analysisRunId=" + analysisRunId + " ]";
    }
    
}
