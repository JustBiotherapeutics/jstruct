package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: pdb_lineage
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_lineage", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_PdbLineage.findByPdbLineageId", 
                                                            query = "SELECT pl FROM Jstruct_PdbLineage pl "
                                                                  + "WHERE pl.pdbLineageId = :pdbLineageId "),
    
    @NamedQuery(name = "Jstruct_PdbLineage.findByPdbPredecessorId",
                                                            query = "SELECT pl FROM Jstruct_PdbLineage pl "
                                                                  + "WHERE pl.pdbPredecessorId = :pdbPredecessorId "),
    
    @NamedQuery(name = "Jstruct_PdbLineage.findByPdbDescendantId", 
                                                            query = "SELECT pl FROM Jstruct_PdbLineage pl "
                                                                  + "WHERE pl.pdbDescendantId = :pdbDescendantId "),
    
    @NamedQuery(name = "Jstruct_PdbLineage.findByPdbPredecessorIdAndPdbDescendantId", 
                                                            query = "SELECT pl FROM Jstruct_PdbLineage pl "
                                                                  + " WHERE pl.pdbPredecessorId = :pdbPredecessorId "
                                                                  + "  AND pl.pdbDescendantId = :pdbDescendantId "),
    
    @NamedQuery(name = "Jstruct_PdbLineage.findByPdbPredecessorIdAndNullPdbDescendantId", 
                                                            query = "SELECT pl FROM Jstruct_PdbLineage pl "
                                                                  + " WHERE pl.pdbPredecessorId = :pdbPredecessorId "
                                                                  + "  AND pl.pdbDescendantId IS NULL "),
    
    
    
})
public class Jstruct_PdbLineage implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pdb_lineage_id")
    private Long pdbLineageId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "pdb_predecessor_id")
    private String pdbPredecessorId;
    
    @Size(max = 4)
    @Column(name = "pdb_descendant_id")
    private String pdbDescendantId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "obsolete_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date obsoleteDate;
    
    
    
    
    public Jstruct_PdbLineage() {
    }

    public Jstruct_PdbLineage(String predecessorId, String descendantId, Date obsoleteDate){
        this.pdbPredecessorId = predecessorId;
        this.pdbDescendantId = descendantId;
        this.obsoleteDate = obsoleteDate;
    }
    
    
    

    public Long getPdbLineageId() { return pdbLineageId; }
    public void setPdbLineageId(Long pdbLineageId) { this.pdbLineageId = pdbLineageId;}

    public String getPdbPredecessorId() {return pdbPredecessorId; }
    public void setPdbPredecessorId(String pdbPredecessorId) { this.pdbPredecessorId = pdbPredecessorId;}

    public String getPdbDescendantId() { return pdbDescendantId; }
    public void setPdbDescendantId(String pdbDescendantId) {this.pdbDescendantId = pdbDescendantId;}

    public Date getObsoleteDate() {return obsoleteDate; }
    public void setObsoleteDate(Date obsoleteDate) { this.obsoleteDate = obsoleteDate; }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbLineageId != null ? pdbLineageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbLineage)) {
            return false;
        }
        Jstruct_PdbLineage other = (Jstruct_PdbLineage) object;
        if ((this.pdbLineageId == null && other.pdbLineageId != null) || (this.pdbLineageId != null && !this.pdbLineageId.equals(other.pdbLineageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbLineage[ pdbLineageId=" + pdbLineageId + " ]";
    }
    
}
