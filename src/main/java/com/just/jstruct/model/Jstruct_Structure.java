package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: structure
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "structure", schema = "public")
@NamedQueries({
    //@NamedQuery(name = "Jstruct_Structure.findAll", 
    //                                                query = "SELECT s FROM Jstruct_Structure s"),
    //
    @NamedQuery(name = "Jstruct_Structure.findByStructureId", 
                                                    query = "SELECT s FROM Jstruct_Structure s "
                                                          + " WHERE s.structureId = :structureId"),
    
    @NamedQuery(name = "Jstruct_Structure.getFileCount", 
                                                    query = "SELECT count(st) FROM Jstruct_Structure st "
                                                          + " WHERE st.fromRcsb LIKE :fromRcsb "
                                                          + "  AND st.isObsolete LIKE :isObsolete ")
})
public class Jstruct_Structure implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="structure_structure_id_seq", sequenceName="structure_structure_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="structure_structure_id_seq")
    @Basic(optional = false)
    @Column(name = "structure_id")
    private Long structureId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "from_rcsb")
    private Character fromRcsb;
    
    @Size(max = 255)
    @Column(name = "source_name")
    private String sourceName;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_all_read")
    private Character canAllRead;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_all_write")
    private Character canAllWrite;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_obsolete")
    private Character isObsolete;
    
    @Column(name = "obsolete_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date obsoleteDate;
    
    @Column(name = "owner_id", insertable=false, updatable=false)
    private Long ownerId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "owner_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User owner;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "structureId", fetch = FetchType.EAGER)
    //private List<Jstruct_StructureRole> structureRoles;

    
    
    public Jstruct_Structure() {
    }

    public Jstruct_Structure(Jstruct_User user){
        this.owner = user;
    }
   
    
    
    
    
    
    
    public Long getStructureId() { return structureId; }
    public void setStructureId(Long structureId) { this.structureId = structureId; }

    public boolean isFromRcsb() { return fromRcsb.equals('Y') || fromRcsb.equals('y'); }
    public void setFromRcsb(boolean fromRcsb) { this.fromRcsb = fromRcsb ? 'Y' : 'N' ; }

    public String getSourceName() { return sourceName; }
    public void setSourceName(String sourceName) { this.sourceName = sourceName; }

    public boolean isCanAllRead() { return canAllRead.equals('Y') || canAllRead.equals('y'); }
    public void setCanAllRead(boolean CanAllRead) { this.canAllRead = CanAllRead ? 'Y' : 'N' ; }

    public boolean isCanAllWrite() { return canAllWrite.equals('Y') || canAllWrite.equals('y'); }
    public void setCanAllWrite(boolean canAllWrite) { this.canAllWrite = canAllWrite ? 'Y' : 'N' ; }

    public boolean isObsolete() { return isObsolete.equals('Y') || isObsolete.equals('y'); }
    public void setObsolete(boolean isObsolete) { this.isObsolete = isObsolete ? 'Y' : 'N' ; }
    
    public Date getObsoleteDate() {  return obsoleteDate; }
    public void setObsoleteDate(Date obsoleteDate) { this.obsoleteDate = obsoleteDate; }
    
    
    public long getOwnerId() {return ownerId;}
    public void setOwnerId(long ownerId) { this.ownerId = ownerId;}
    
    public Jstruct_User getOwner() {return owner;}
    public void setOwner(Jstruct_User owner) { this.owner = owner;}
    

    //public List<Jstruct_StructVersion> getJstructStructVersionList() {
    //    return jstructStructVersionList;
    //}

    //public void setJstructStructVersionList(List<Jstruct_StructVersion> jstructStructVersionList) {
    //    this.jstructStructVersionList = jstructStructVersionList;
    //}

    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (structureId != null ? structureId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_Structure)) {
            return false;
        }
        Jstruct_Structure other = (Jstruct_Structure) object;
        if ((this.structureId == null && other.structureId != null) || (this.structureId != null && !this.structureId.equals(other.structureId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_Structure[ structureId=" + structureId + " ]";
    }
    
}
