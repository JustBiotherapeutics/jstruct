package com.just.jstruct.model;

import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.Chain;
import com.just.bio.structure.SeqResidue;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: chain
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "chain", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_Chain.findByChainId", 
                                                    query = "SELECT c FROM Jstruct_Chain c "
                                                          + " WHERE c.chainId = :chainId "),
    
    @NamedQuery(name = "Jstruct_Chain.findCountByStructVerId",
                                                    query = "SELECT count(c) FROM Jstruct_Chain c "
                                                          + " WHERE c.structVerId = :structVerId "),
    
    @NamedQuery(name = "Jstruct_Chain.findByStructVerId", 
                                                    query = "SELECT c FROM Jstruct_Chain c "
                                                          + " WHERE c.structVerId = :structVerId "
                                                          + "ORDER BY c.name ASC, "
                                                          + "         c.chainId ASC "),
    
    @NamedQuery(name = "Jstruct_Chain.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_Chain c "
                                                          + " WHERE c.structVerId = :structVerId ")
})
public class Jstruct_Chain implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="chain_chain_id_seq", sequenceName="chain_chain_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="chain_chain_id_seq")
    @Basic(optional = false)
    @Column(name = "chain_id")
    private Long chainId;
    
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @Size(max = 255)
    @Column(name = "type")
    private String type;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "residue_count")
    private int residueCount;
    
    @Size(max = 2147483647)
    @Column(name = "seq_theoretical")
    private String seqTheoretical;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Transient private List<Jstruct_SeqResidue> seqResidues;
    @Transient private List<Jstruct_AtomGrouping> atomGroupings;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "chainId")
    //private List<Jstruct_AtomGrouping> jstructAtomGroupingList;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "chainId")
    //private List<Jstruct_SeqResidue> jstructSeqResidueList;

    
    
    public Jstruct_Chain() {
    }

    
    
    /**
     * create a new Chain based on a ChainObject (see CoreStruct) and it's related structVerId
     * this will also populate all the chains children: AtomGroupings/Residues, and SeqResidues
     * @param chainObj
     * @param structVerId 
     */
    public Jstruct_Chain(Chain chainObj, Long structVerId)
    {
        this.name = chainObj.name();
        this.type = chainObj.getType().name();
        this.seqTheoretical = chainObj.getTheoreticalSeq();
        this.residueCount = CollectionUtil.hasValues(chainObj.getSeqResidues()) ? chainObj.getSeqResidues().size() : 0 ;
        this.structVerId = structVerId;

        addAtomGrouping(new Jstruct_AtomGrouping(chainObj, this.chainId));

        if(CollectionUtil.hasValues(chainObj.getSeqResidues()))
        {
            for(SeqResidue seqResidue : chainObj.getSeqResidues())
            {
                addSeqResidue(new Jstruct_SeqResidue(seqResidue, this.chainId));
            }
        }
        
    }
   
    
    

    public Long getChainId() {
        return chainId;
    }

    public void setChainId(Long chainId) {
        this.chainId = chainId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getResidueCount() {
        return residueCount;
    }

    public void setResidueCount(int residueCount) {
        this.residueCount = residueCount;
    }

    public String getSeqTheoretical() {
        return seqTheoretical;
    }

    public void setSeqTheoretical(String seqTheoretical) {
        this.seqTheoretical = seqTheoretical;
    }

    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }

    public List<Jstruct_SeqResidue> getSeqResidues() {return seqResidues;}
    public void setSeqResidues(List<Jstruct_SeqResidue> seqResidues) {this.seqResidues = seqResidues;}
    public void addSeqResidue(Jstruct_SeqResidue seqResidue){
        if(this.seqResidues == null){
            seqResidues = new ArrayList<>(10);
        }
        seqResidue.setChainId(this.chainId);
        seqResidues.add(seqResidue);
    }

    public List<Jstruct_AtomGrouping> getAtomGroupings() { return atomGroupings; }
    public void setAtomGroupings(List<Jstruct_AtomGrouping> atomGroupings) { this.atomGroupings = atomGroupings;}
    public void addAtomGrouping(Jstruct_AtomGrouping atomGrouping){
        if(this.atomGroupings == null){
            atomGroupings = new ArrayList<>(10);
        }
        atomGrouping.setChainId(this.chainId);
        atomGroupings.add(atomGrouping);
    }
   
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (chainId != null ? chainId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_Chain)) {
            return false;
        }
        Jstruct_Chain other = (Jstruct_Chain) object;
        if ((this.chainId == null && other.chainId != null) || (this.chainId != null && !this.chainId.equals(other.chainId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_Chain[ chainId=" + chainId + " ]";
    }
    
}
