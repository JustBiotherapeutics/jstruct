package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing Revision Dates/Data (REVDAT) PDB Header information
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#REVDAT
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_revdat", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbRevdat.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbRevdat p "
                                                          + " WHERE p.structVerId = :structVerId "
                                                          + "ORDER BY p.moddate ASC "),
    
    @NamedQuery(name = "Jstruct_PdbRevdat.findInitialReleaseByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbRevdat p "
                                                          + " WHERE p.structVerId = :structVerId "
                                                          + "   AND p.modtype = 0 "),
    
    @NamedQuery(name = "Jstruct_PdbRevdat.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbRevdat p "
                                                          + " WHERE p.structVerId = :structVerId ")
        
})
public class Jstruct_PdbRevdat implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_revdat_pdb_revdat_id_seq", sequenceName="pdb_revdat_pdb_revdat_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_revdat_pdb_revdat_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_revdat_id")
    private Long pdbRevdatId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "modnum")
    private int modnum;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "moddate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date moddate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "modtype")
    private int modtype;
    
    @Size(max = 1000)
    @Column(name = "detail")
    private String detail;
    

    
    
    public Jstruct_PdbRevdat() {
    }

    public Jstruct_PdbRevdat(Long structVerId, int modnum, Date moddate, int modtype, String detail) {
        this.structVerId = structVerId;
        this.modnum = modnum;
        this.moddate = moddate;
        this.modtype = modtype;
        this.detail = detail;
    }

    
   
    
    

    public Long getPdbRevdatId() {
        return pdbRevdatId;
    }

    public void setPdbRevdatId(Long pdbRevdatId) {
        this.pdbRevdatId = pdbRevdatId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public int getModnum() {
        return modnum;
    }

    public void setModnum(int modnum) {
        this.modnum = modnum;
    }

    public Date getModdate() {
        return moddate;
    }

    public void setModdate(Date moddate) {
        this.moddate = moddate;
    }

    public int getModtype() {
        return modtype;
    }

    public void setModtype(int modtype) {
        this.modtype = modtype;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbRevdatId != null ? pdbRevdatId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbRevdat)) {
            return false;
        }
        Jstruct_PdbRevdat other = (Jstruct_PdbRevdat) object;
        if ((this.pdbRevdatId == null && other.pdbRevdatId != null) || (this.pdbRevdatId != null && !this.pdbRevdatId.equals(other.pdbRevdatId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbRevdat[ pdbRevdatId=" + pdbRevdatId + " ]";
    }
    
}
