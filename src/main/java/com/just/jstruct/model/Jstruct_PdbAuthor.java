package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing author (AUTHOR) PDB Header information (related pdb entries)
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#AUTHOR
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_author", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbAuthor.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbAuthor p "
                                                          + "WHERE p.structVerId = :structVerId"),
    
    @NamedQuery(name = "Jstruct_PdbAuthor.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbAuthor p "
                                                          + "WHERE p.structVerId = :structVerId")
        
})
public class Jstruct_PdbAuthor implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_author_pdb_author_id_seq", sequenceName="pdb_author_pdb_author_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_author_pdb_author_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_author_id")
    private Long pdbAuthorId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "value")
    private String value;
    

    public Jstruct_PdbAuthor() {
    }

    public Jstruct_PdbAuthor(Long structVerId, String value) {
        this.structVerId = structVerId;
        this.value = value;
    }

    
    

    public Long getPdbAuthorId() {
        return pdbAuthorId;
    }

    public void setPdbAuthorId(Long pdbAuthorId) {
        this.pdbAuthorId = pdbAuthorId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbAuthorId != null ? pdbAuthorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbAuthor)) {
            return false;
        }
        Jstruct_PdbAuthor other = (Jstruct_PdbAuthor) object;
        if ((this.pdbAuthorId == null && other.pdbAuthorId != null) || (this.pdbAuthorId != null && !this.pdbAuthorId.equals(other.pdbAuthorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbAuthor[ pdbAuthorId=" + pdbAuthorId + " ]";
    }
    
}
