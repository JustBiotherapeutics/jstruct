package com.just.jstruct.model;

import com.just.bio.structure.enums.ChainType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * Entity class for: SEQUENCES_SEQRES_VIEW
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "sequences_seqres_view", schema = "public")
@NamedQueries({

    @NamedQuery(name = "Jstruct_SequencesSeqres.findByStructVerIds", 
                                                query = "SELECT ss FROM Jstruct_SequencesSeqres ss "
                                                      + " WHERE ss.structVerId IN (:structVerIds) "
                                                      + " ORDER BY ss.structureId, ss.structVerId, ss.chainName "),
    
    @NamedQuery(name = "Jstruct_SequencesSeqres.findAllStructVerIds", 
                                                query = "SELECT distinct ss.structVerId FROM Jstruct_SequencesSeqres ss ")
    
    
})
public class Jstruct_SequencesSeqres implements Serializable, Jstruct_SequencesView {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @Column(name = "chain_id")
    private Long chainId;
    
    @Size(max = 255)
    @Column(name = "chain_name")
    private String chainName;
    
    @Size(max = 255)
    @Column(name = "chain_type")
    private String chainType;
    
   // @Column(name = "residue_count")
   // private Integer residueCount;
    
    @Column(name = "seq_theoretical")
    private String seqTheoretical;
    
    
    @Column(name = "from_rcsb")
    private Character fromRcsb;
    
    //@Size(max = 255)
    //@Column(name = "source_name")
    //private String sourceName;
    
    @Column(name = "is_obsolete")
    private Character isObsolete;
    
    //@Column(name = "obsolete_date")
    //@Temporal(TemporalType.TIMESTAMP)
    //private Date obsoleteDate;
    
    
    @Column(name = "structure_id")
    private Long structureId;
    
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    @Column(name = "version")
    private Integer version;
    
    @Column(name = "title")
    private String title;
    
    @Size(max = 4)
    @Column(name = "pdb_identifier")
    private String pdbIdentifier;
    
    
    

    public Jstruct_SequencesSeqres() {
    }

    
    
    
    @Override
    public boolean isFromRcsb() { return fromRcsb.equals('Y') || fromRcsb.equals('y'); }
    @Override
    public void setFromRcsb(boolean fromRcsb) { this.fromRcsb = fromRcsb ? 'Y' : 'N' ; }

    //@Override
    //public String getSourceName() { return sourceName; }
    //@Override
    //public void setSourceName(String sourceName) { this.sourceName = sourceName; }

    @Override
    public boolean isObsolete() { return isObsolete.equals('Y') || isObsolete.equals('y'); }
    @Override
    public void setObsolete(boolean isObsolete) { this.isObsolete = isObsolete ? 'Y' : 'N' ; }
    
    //@Override
    //public Date getObsoleteDate() {  return obsoleteDate; }
    //@Override
    //public void setObsoleteDate(Date obsoleteDate) { this.obsoleteDate = obsoleteDate; }
    
    @Override
    public Long getStructureId() {
        return structureId;
    }

    @Override
    public void setStructureId(Long structureId) {
        this.structureId = structureId;
    }

    @Override
    public Long getStructVerId() {
        return structVerId;
    }

    @Override
    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getPdbIdentifier() {
        return pdbIdentifier;
    }

    @Override
    public void setPdbIdentifier(String pdbIdentifier) {
        this.pdbIdentifier = pdbIdentifier;
    }

    public Long getChainId() {
        return chainId;
    }

    public void setChainId(Long chainId) {
        this.chainId = chainId;
    }

    @Override
    public String getChainName() {
        return chainName;
    }

    @Override
    public void setChainName(String chainName) {
        this.chainName = chainName;
    }
    
    @Override
    public ChainType getChainType() {
        return ChainType.valueOf(chainType);
    }

    @Override
    public void setChainType(ChainType chainType) {
        this.chainType = chainType.name();
    }

    //@Override
    //public Integer getResidueCount() {
    //    return residueCount;
    //}

    //@Override
    //public void setResidueCount(Integer residueCount) {
    //    this.residueCount = residueCount;
    //}

    public String getSeqTheoretical() {
        return seqTheoretical;
    }

    public void setSeqTheoretical(String seqTheoretical) {
        this.seqTheoretical = seqTheoretical;
    }

    @Override
    public String getPrimaryFastaSequence() {
        return seqTheoretical;
    }
    
}
