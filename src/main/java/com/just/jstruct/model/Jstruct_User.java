package com.just.jstruct.model;

import com.hfg.util.StringUtil;
import com.just.jstruct.enums.UserStatus;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: Jstruct_User
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "jstruct_user", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_User.findAll", 
                                            query = "SELECT u FROM Jstruct_User u"),
    
    @NamedQuery(name = "Jstruct_User.findByUserId", 
                                            query = "SELECT u FROM Jstruct_User u "
                                                  + "WHERE u.userId = :userId"),
    
    @NamedQuery(name = "Jstruct_User.findByUid", 
                                            query = "SELECT u FROM Jstruct_User u "
                                                  + "WHERE UPPER(u.uid) = :uid"),
    
    @NamedQuery(name = "Jstruct_User.findUsersByGeneralCriteria", query = "SELECT u FROM Jstruct_User u "
                                                                        + " WHERE "
                                                                        + "  (    UPPER(u.uid)      LIKE :criteria "
                                                                        + "    OR UPPER(u.name)     LIKE :criteria "
                                                                        + "  ) "
                                                                        + "  AND ( u.uid NOT LIKE '" + Jstruct_User.SYSTEM_UID + "') "
                                                                        + "  AND ( u.uid NOT LIKE '" + Jstruct_User.GUEST_UID + "') "
                                                                        + "ORDER BY u.name"),
    
    @NamedQuery(name = "Jstruct_User.findActiveRoleDevelopers", query = "SELECT u FROM Jstruct_User u "
                                                                     + " WHERE u.roleDeveloper LIKE 'Y' "
                                                                     + "   AND u.status LIKE 'ACTIVE' "),
    
    @NamedQuery(name = "Jstruct_User.findActiveRoleAdmins", query = "SELECT u FROM Jstruct_User u "
                                                                  + " WHERE u.roleAdmin LIKE 'Y' "
                                                                  + "   AND u.status LIKE 'ACTIVE' "),
    
    @NamedQuery(name = "Jstruct_User.findActiveRoleContributers", query = "SELECT u FROM Jstruct_User u "
                                                                        + " WHERE u.roleContribute LIKE 'Y' "
                                                                        + "   AND u.status LIKE 'ACTIVE' ")
})  
public class Jstruct_User implements Serializable {
    
    
    public static final String SYSTEM_UID = "JSTRUCT_SYSTEM";
    public static final String GUEST_UID = "JSTRUCT_GUEST";

    
    private static final long serialVersionUID = 1L;
    
    
    
    @Id
    @SequenceGenerator(name="jstruct_user_user_id_seq", sequenceName="jstruct_user_user_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="jstruct_user_user_id_seq")
    @Basic(optional = false)
    @Column(name = "user_id", updatable=false)
    private Long userId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "uid")
    private String uid;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email")
    private String email;
    
    @Size(max = 256)
    @Column(name = "img_url")
    private String imgUrl;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "status")
    private String status;
    
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "role_developer")
    private Character roleDeveloper;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "role_admin")
    private Character roleAdmin;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "role_contribute")
    private Character roleContribute;
    
    
    
    
    /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    @Column(name = "update_user_id", insertable=false, updatable=false)
    private Long updateUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "update_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User updateUser;
    
    /* --------- end audit fields --------- */
    
    
    
    
    
    public Jstruct_User() {
    }

    
    
    
    
    public Jstruct_User(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    
    
    public UserStatus getStatus(){
        UserStatus userStatus = null;
        if (StringUtil.isSet(this.status)){
            userStatus = UserStatus.valueOf(this.status);
        }
        return userStatus;
    }
    public void setStatus(UserStatus inValue) {
        this.status = inValue != null ? inValue.name() : null;
    }
    
    //public String getStatus() { return status; }
    //public void setStatus(String status) { this.status = status; }

    
    public boolean getIsRoleDeveloper() { return roleDeveloper.equals('Y') || roleDeveloper.equals('y'); }
    public void setRoleDeveloper(boolean roleDeveloper) { this.roleDeveloper = roleDeveloper ? 'Y' : 'N' ; }
    
    public boolean getIsRoleAdmin() { return roleAdmin.equals('Y') || roleAdmin.equals('y'); }
    public void setRoleAdmin(boolean roleAdmin) { this.roleAdmin = roleAdmin ? 'Y' : 'N' ; }
    
    public boolean getIsRoleContributor() { return roleContribute.equals('Y') || roleContribute.equals('y'); }
    public void setRoleContributor(boolean roleContribute) { this.roleContribute = roleContribute ? 'Y' : 'N' ; }
    
   

    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getUpdateDate() {return updateDate;}
    public void setUpdateDate(Date updateDate) { this.updateDate = updateDate;}

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return updateUserId;}
    public void setUpdateUserId(long updateUserId) {this.updateUserId = updateUserId;}
    
    public Jstruct_User getUpdateUser() {return updateUser;}
    public void setUpdateUser(Jstruct_User updateUser) { this.updateUser = updateUser;}
    
    
    
    /**
     * determine if this is the 'guest' user
     * @return 
     */
    public boolean isGuest(){
        return getUid().equals(GUEST_UID);
    }
    
    
    /**
     * determine if this is the 'system' user
     * @return 
     */
    public boolean isSystem(){
        return getUid().equals(SYSTEM_UID);
    }

   
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_User)) {
            return false;
        }
        Jstruct_User other = (Jstruct_User) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_User[ userId=" + userId + " ]";
    }
    
}
