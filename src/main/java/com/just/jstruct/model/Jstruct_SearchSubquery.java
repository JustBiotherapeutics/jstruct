package com.just.jstruct.model;

import com.just.jstruct.structureSearch.enums.SubQueryType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * Entity class for: search_subquery
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "search_subquery", schema = "public")
@NamedQueries({

    @NamedQuery(name = "Jstruct_SearchSubquery.findBySearchSubqueryId", 
                                                    query = "SELECT us FROM Jstruct_SearchSubquery us "
                                                          + " WHERE us.searchSubqueryId = :searchSubqueryId"),
    
    @NamedQuery(name = "Jstruct_SearchSubquery.findByUserSearchId", 
                                                    query = "SELECT us FROM Jstruct_SearchSubquery us "
                                                          + " WHERE us.userSearchId = :userSearchId"),
})
public class Jstruct_SearchSubquery implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="search_subquery_search_subquery_id_seq", sequenceName="search_subquery_search_subquery_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="search_subquery_search_subquery_id_seq")
    @Basic(optional = false)
    @Column(name = "search_subquery_id")
    private Long searchSubqueryId;

    
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_search_id")
    private Long userSearchId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "subquery_type")
    private String subqueryType;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "row_index")
    private Integer rowIndex;
    
    
    
    @Transient
    private List<Jstruct_SearchParam> searchParams;
    
    
    
    
    public Jstruct_SearchSubquery() {
    }
    
    
    public Jstruct_SearchSubquery(Long userSearchId, SubQueryType subQueryType, Integer rowIndex) {
        this.userSearchId = userSearchId;
        this.subqueryType = subQueryType.name();
        this.rowIndex = rowIndex;
    }

    
    
   
    public Long getSearchSubqueryId() { return searchSubqueryId;}
    public void setSearchSubqueryId(Long searchSubqueryId) { this.searchSubqueryId = searchSubqueryId;}

    public Long getUserSearchId() { return userSearchId;}
    public void setUserSearchId(Long userSearchId) {this.userSearchId = userSearchId; }

    public SubQueryType getSubqueryType() { 
        return SubQueryType.valueOf(subqueryType);
    }
    public void setSubqueryType(SubQueryType subqueryType) { 
        this.subqueryType = subqueryType.name();
    }

    public Integer getRowIndex() {return rowIndex;}
    public void setRowIndex(Integer rowIndex) { this.rowIndex = rowIndex;}

    
     
    public List<Jstruct_SearchParam> getSearchParams() { return searchParams;}
    public void setSearchParams(List<Jstruct_SearchParam> searchParams) { this.searchParams = searchParams; }
   
    public void addSearchParam(Jstruct_SearchParam searchParam){
        if(this.searchParams == null){
            searchParams = new ArrayList<>();
        }
        searchParam.setSearchSubqueryId(this.searchSubqueryId);
        searchParams.add(searchParam);
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (searchSubqueryId != null ? searchSubqueryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_SearchSubquery)) {
            return false;
        }
        Jstruct_SearchSubquery other = (Jstruct_SearchSubquery) object;
        if ((this.searchSubqueryId == null && other.searchSubqueryId != null) || (this.searchSubqueryId != null && !this.searchSubqueryId.equals(other.searchSubqueryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_SearchSubquery[ searchSubqueryId=" + searchSubqueryId + " ]";
    }
    
}
