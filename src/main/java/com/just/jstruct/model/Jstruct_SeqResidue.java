package com.just.jstruct.model;

import com.just.bio.structure.SeqResidue;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: seq_residue
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "seq_residue", schema = "public")
@NamedQueries({    
    
    @NamedQuery(name = "Jstruct_SeqResidue.findBySeqResidueId", 
                                                            query = "SELECT sr FROM Jstruct_SeqResidue sr "
                                                                  + "WHERE sr.seqResidueId = :seqResidueId "),
    
    @NamedQuery(name = "Jstruct_SeqResidue.findByChainId", 
                                                            query = "SELECT sr FROM Jstruct_SeqResidue sr "
                                                                  + "WHERE sr.chainId = :chainId "
                                                                  + "ORDER BY sr.ordinal ASC, "
                                                                  + "         sr.seqResidueId ASC "),
    
    @NamedQuery(name = "Jstruct_Residue.deleteByChainId", 
                                                        query = "DELETE FROM Jstruct_SeqResidue sr "
                                                              + " WHERE sr.chainId = :chainId ")
})
public class Jstruct_SeqResidue implements Serializable {

    
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="seq_residue_seq_residue_id_seq", sequenceName="seq_residue_seq_residue_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_residue_seq_residue_id_seq")
    @Basic(optional = false)
    @Column(name = "seq_residue_id")
    private Long seqResidueId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "abbrev")
    private String abbrev;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "residue_type")
    private String residueType;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ordinal")
    private int ordinal;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "chain_id")
    private Long chainId;
    
    
    

    public Jstruct_SeqResidue() {
    }

    
    /**
     * create a new Jstruct_SeqResidue based on a SeqResidueObject (see CoreStruct) and it's related chainId
     * @param seqResidueObj 
     * @param chainId 
     */
    public Jstruct_SeqResidue(SeqResidue seqResidueObj, Long chainId){
        this.chainId = chainId;
        this.residueType = seqResidueObj.getResidueType().name();
        this.abbrev = seqResidueObj.name();
        this.ordinal = seqResidueObj.getOrdinal();
    }
    
    
    

    public Long getSeqResidueId() {
        return seqResidueId;
    }

    public void setSeqResidueId(Long seqResidueId) {
        this.seqResidueId = seqResidueId;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    public String getResidueType() {
        return residueType;
    }

    public void setResidueType(String residueType) {
        this.residueType = residueType;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public Long getChainId() {
        return chainId;
    }

    public void setChainId(Long chainId) {
        this.chainId = chainId;
    }

  
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seqResidueId != null ? seqResidueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_SeqResidue)) {
            return false;
        }
        Jstruct_SeqResidue other = (Jstruct_SeqResidue) object;
        if ((this.seqResidueId == null && other.seqResidueId != null) || (this.seqResidueId != null && !this.seqResidueId.equals(other.seqResidueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_SeqResidue[ seqResidueId=" + seqResidueId + " ]";
    }
    
}
