package com.just.jstruct.model;

import com.hfg.html.attribute.HTMLColor;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: user_label
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "user_label", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_UserLabel.findByUserLabelId", 
                                                            query = "SELECT ul FROM Jstruct_UserLabel ul "
                                                                  + " WHERE ul.userLabelId = :userLabelId "),
    
    @NamedQuery(name = "Jstruct_UserLabel.findByUserId", 
                                                            query = "SELECT ul FROM Jstruct_UserLabel ul "
                                                                  + " WHERE ul.userId = :userId "
                                                                  + "ORDER BY ul.ordinal, ul.labelName "),
    
    @NamedQuery(name = "Jstruct_UserLabel.findByUserIdStructureId", 
                                                            query = "SELECT ul FROM Jstruct_UserLabel ul "
                                                                  + " WHERE ul.userLabelId IN  "
                                                                  + "   ( "
                                                                  + "     SELECT usl.userLabelId FROM Jstruct_UserStructureLabel usl "
                                                                  + "      WHERE usl.userId = :userId "
                                                                  + "        AND usl.structureId = :structureId "
                                                                  + "   ) "
                                                                  + "ORDER BY ul.ordinal, ul.labelName "),
    
    @NamedQuery(name = "Jstruct_UserLabel.findUnusedByUserId", 
                                                            query = "SELECT ul FROM Jstruct_UserLabel ul "
                                                                  + " WHERE ul.userId = :userId "
                                                                  + "   AND ul.userLabelId NOT IN "
                                                                  + "   ( "
                                                                  + "     SELECT usl.userLabelId FROM Jstruct_UserStructureLabel usl "
                                                                  + "      WHERE usl.userId = :userId "
                                                                  + "   ) "
                                                                  + "   ORDER BY ul.ordinal, ul.labelName ")
    
})
public class Jstruct_UserLabel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static String DEFAULT_BACKGROUND_COLOR = "FFFFCC";
    public static Integer DEFAULT_ORDINAL = 999;
    
    
    @Id
    @SequenceGenerator(name="user_label_user_label_id_seq", sequenceName="user_label_user_label_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_label_user_label_id_seq")
    @Basic(optional = false)
    @Column(name = "user_label_id")
    private Long userLabelId;
    
    
    @Column(name = "user_id", insertable=false, updatable=false)
    private Long userId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User user;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "label_name")
    private String labelName;
    
    @Size(max = 2000)
    @Column(name = "description")
    private String description;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "background_color")
    private String backgroundColor;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "text_color")
    private String textColor;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ordinal")
    private int ordinal;
    
    
    
    
    /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    @Column(name = "update_user_id", insertable=false, updatable=false)
    private Long updateUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "update_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User updateUser;
    
    /* --------- end audit fields --------- */
    
    
    
    
    public Jstruct_UserLabel() {
        this.backgroundColor = DEFAULT_BACKGROUND_COLOR;
        String contrastingColor = HTMLColor.valueOf(backgroundColor).getContrastingColor().toHex();
        this.textColor = contrastingColor.startsWith("#") ? contrastingColor.substring(1) : contrastingColor;
        this.ordinal = DEFAULT_ORDINAL;
    }

    public Jstruct_UserLabel(Jstruct_User user) {
        this();
        this.user = user;
    }
    
    //only used to create temporary UserLabel objects; not to be used for persisting!
    public Jstruct_UserLabel(Jstruct_UserStructureLabel usl){
        
        this.userLabelId = usl.getUserLabelId();
        this.userId = usl.getUserId();
        this.labelName = usl.getLabelName();
        this.description = usl.getDescription();
        this.backgroundColor = usl.getBackgroundColor();
        String contrastingColor = HTMLColor.valueOf(backgroundColor).getContrastingColor().toHex();
        this.textColor = contrastingColor.startsWith("#") ? contrastingColor.substring(1) : contrastingColor;
        this.ordinal = usl.getOrdinal();
        
        
    }

    
    
    

    public Long getUserLabelId() {
        return userLabelId;
    }

    public void setUserLabelId(Long userLabelId) {
        this.userLabelId = userLabelId;
    }
    
    public long getUserId() {return userId;}
    public void setUserId(long userId) { this.userId = userId;}
    
    public Jstruct_User getUser() {return user;}
    public void setUser(Jstruct_User user) { this.user = user;}

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }

    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getUpdateDate() {return updateDate;}
    public void setUpdateDate(Date updateDate) { this.updateDate = updateDate;}

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return updateUserId;}
    public void setUpdateUserId(long updateUserId) {this.updateUserId = updateUserId;}
    
    public Jstruct_User getUpdateUser() {return updateUser;}
    public void setUpdateUser(Jstruct_User updateUser) { this.updateUser = updateUser;}
    
    

    //public List<Jstruct_StructureLabel> getJstructStructureLabelList() {
    //    return jstructStructureLabelList;
    //}

    //public void setJstructStructureLabelList(List<Jstruct_StructureLabel> jstructStructureLabelList) {
    //    this.jstructStructureLabelList = jstructStructureLabelList;
    //}

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userLabelId != null ? userLabelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_UserLabel)) {
            return false;
        }
        Jstruct_UserLabel other = (Jstruct_UserLabel) object;
        if ((this.userLabelId == null && other.userLabelId != null) || (this.userLabelId != null && !this.userLabelId.equals(other.userLabelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_UserLabel[ userLabelId=" + userLabelId + " ]";
    }
    
}
