package com.just.jstruct.model;

import com.hfg.util.StringUtil;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: value_map
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "value_map", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_ValueMap.findAll", 
                                                    query = "SELECT vm FROM Jstruct_ValueMap vm "
                                                          + "ORDER BY vm.itemGroup, vm.itemKey"),
    
    @NamedQuery(name = "Jstruct_ValueMap.findByValueMapId", 
                                                    query = "SELECT v FROM Jstruct_ValueMap v "
                                                          + "WHERE v.valueMapId = :valueMapId"),
    
    @NamedQuery(name = "Jstruct_ValueMap.findByItemGroup", 
                                                    query = "SELECT v FROM Jstruct_ValueMap v "
                                                          + "WHERE v.itemGroup = :itemGroup "
                                                          + "ORDER BY v.itemGroup, v.itemKey"),
    
    @NamedQuery(name = "Jstruct_ValueMap.findByItemKey", 
                                                    query = "SELECT v FROM Jstruct_ValueMap v "
                                                          + "WHERE v.itemKey = :itemKey"),
    
    @NamedQuery(name = "Jstruct_ValueMap.distinctGroups", 
                                                    query = "SELECT DISTINCT valueMap.itemGroup FROM Jstruct_ValueMap valueMap "
                                                          + "ORDER BY valueMap.itemGroup")
})
public class Jstruct_ValueMap implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @NotNull
    @Basic(optional = false)
    @Column(name = "value_map_id")
    private Long valueMapId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "item_group")
    private String itemGroup;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "item_key")
    private String itemKey;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "item_value")
    private String itemValue;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "description")
    private String description;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "valid_url")
    private Character validUrl;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "valid_email")
    private Character validEmail;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "valid_emails")
    private Character validEmails;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "valid_int")
    private Character validInt;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "initialized")
    private Character initialized;

    
    public Jstruct_ValueMap() {
    }



    public Long getValueMapId() { return valueMapId; }
    public void setValueMapId(Long valueMapId) { this.valueMapId = valueMapId; }

    public String getItemGroup() { return itemGroup;}
    public void setItemGroup(String itemGroup) { this.itemGroup = itemGroup; }

    public String getItemKey() { return itemKey; }
    public void setItemKey(String itemKey) { this.itemKey = itemKey; }

    public String getItemValue() { return itemValue; }
    public void setItemValue(String itemValue) { this.itemValue = itemValue; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    
    public boolean getIsValidUrl() { return validUrl.equals('Y') || validUrl.equals('y'); }
    public void setValidUrl(boolean isValidUrlB) { this.validUrl = isValidUrlB ? 'Y' : 'N' ; }
    
    public boolean getIsValidEmail() { return validEmail.equals('Y') || validEmail.equals('y'); }
    public void setIsValidEmail(boolean isValidEmailB) { this.validEmail = isValidEmailB ? 'Y' : 'N' ; }
    
    public boolean getIsValidEmails() { return validEmails.equals('Y') || validEmails.equals('y'); }
    public void setIsValidEmails(boolean isValidEmailsB) { this.validEmails = isValidEmailsB ? 'Y' : 'N' ; }
    
    public boolean getIsValidInt() { return validInt.equals('Y') || validInt.equals('y'); }
    public void setIsValidInt(boolean isValidIntB) { this.validInt = isValidIntB ? 'Y' : 'N' ; }
    

    public boolean isInitialized() {return initialized.equals('Y') || initialized.equals('y');}
    public void setInitialized(boolean initialized) { this.initialized = initialized ? 'Y' : 'N' ; }

    
    
    public Boolean getIsUrl()
    {
        if(!StringUtil.isSet(itemValue))
        {
            return false;
        }
        return itemValue.startsWith("http");
    }



    public boolean isTestableEmailAddress()
    {
        if(!StringUtil.isSet(itemKey))
        {
            return false;
        }
        return itemKey.endsWith("Recipients");
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (valueMapId != null ? valueMapId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_ValueMap)) {
            return false;
        }
        Jstruct_ValueMap other = (Jstruct_ValueMap) object;
        if ((this.valueMapId == null && other.valueMapId != null) || (this.valueMapId != null && !this.valueMapId.equals(other.valueMapId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_ValueMap[valueMapId=" + valueMapId + "], [itemKey=" + itemKey + "]";
    }
    
}
