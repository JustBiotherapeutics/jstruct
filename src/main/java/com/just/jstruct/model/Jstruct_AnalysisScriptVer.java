package com.just.jstruct.model;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: analysis_script_ver
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "analysis_script_ver", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_AnalysisScriptVer.findById", 
                                                        query = "SELECT asv FROM Jstruct_AnalysisScriptVer asv "
                                                              + " WHERE asv.analysisScriptVerId = :analysisScriptVerId "),
    
    @NamedQuery(name = "Jstruct_AnalysisScriptVer.findByStatus", 
                                                        query = "SELECT asv FROM Jstruct_AnalysisScriptVer asv "
                                                              + " WHERE asv.status = :status "),
    
    @NamedQuery(name = "Jstruct_AnalysisScriptVer.findByAnalysisScriptId", 
                                                        query = "SELECT asv FROM Jstruct_AnalysisScriptVer asv "
                                                              + " WHERE asv.analysisScriptId = :analysisScriptId "
                                                              + "ORDER BY asv.version DESC ")
})
public class Jstruct_AnalysisScriptVer implements Serializable 
{
    private static final long serialVersionUID = 1L;
    
    
    public static final String STATUS_CURRENT = "Current";
    public static final String STATUS_ACTIVE = "Active";
    public static final String STATUS_DEFUNCT = "Defunct";
    
    
    @Id
    @SequenceGenerator(name="analysis_script_ver_analysis_script_ver_id_seq", sequenceName="analysis_script_ver_analysis_script_ver_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="analysis_script_ver_analysis_script_ver_id_seq")
    @Basic(optional = false)
    @Column(name = "analysis_script_ver_id")
    private Long analysisScriptVerId;
    
    @Size(max = 2000)
    @Column(name = "comments")
    private String comments;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "default_command")
    private String defaultCommand;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "status")
    private String status;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "version")
    private int version;
    
    
    //fk to analysis_script
    @Basic(optional = false)
    @NotNull
    @Column(name = "analysis_script_id")
    private Long analysisScriptId;
    
    
    @JoinColumn(name = "analysis_script_id", referencedColumnName = "analysis_script_id", insertable=false, updatable=false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Jstruct_AnalysisScript analysisScript;
    
    
    
    
    
    @Column(name = "owner_id", insertable=false, updatable=false)
    private Long ownerId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "owner_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    private Jstruct_User owner;
    
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Column(name = "overwrite_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date overwriteDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    private Jstruct_User insertUser;
    
    @Column(name = "overwrite_user_id", insertable=false, updatable=false)
    private Long overwriteUserId;
    
    @JoinColumn(name = "overwrite_user_id", referencedColumnName = "user_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Jstruct_User overwriteUser;
    
    
    //transient boolean to support UI checkbox
    @Transient
    private boolean selected;
    
    //transient list of analysis_runs
    @Transient
    private List<Jstruct_AnalysisRun> analysisRuns = new ArrayList<>(10);
    
    //transient list of analysis_files
    @Transient
    private List<Jstruct_AnalysisFile> analysisFiles = new ArrayList<>(10);
    

    public Jstruct_AnalysisScriptVer() {
    }

    
    
    

    public Long getAnalysisScriptVerId() {   return analysisScriptVerId; }
    public void setAnalysisScriptVerId(Long analysisScriptVerId) { this.analysisScriptVerId = analysisScriptVerId; }

    public String getComments() { return comments; }
    public void setComments(String comments) { this.comments = StringUtil.isSet(comments) ? comments.trim() : null; }

    public String getDefaultCommand() { return defaultCommand; }
    public void setDefaultCommand(String defaultCommand) { this.defaultCommand = StringUtil.isSet(defaultCommand) ? defaultCommand.trim() : null; }

    public String getStatus() { return status; }
    public void setStatus(String status) {this.status = status; }

    public boolean isCurrent(){ return this.status.equalsIgnoreCase(STATUS_CURRENT);}
    public boolean isActive(){ return this.status.equalsIgnoreCase(STATUS_ACTIVE);}
    public boolean isDefunct(){ return this.status.equalsIgnoreCase(STATUS_DEFUNCT);}
    
    
    public int getVersion() {return version; }
    public void setVersion(int version) {this.version = version; }

    
    public Long getAnalysisScriptId() { return analysisScriptId;}
    public void setAnalysisScriptId(Long analysisScriptId) {  this.analysisScriptId = analysisScriptId; }

    public Jstruct_AnalysisScript getAnalysisScript() { return analysisScript;}
    public void setAnalysisScript(Jstruct_AnalysisScript analysisScript) { this.analysisScript = analysisScript;}

    
    public long getOwnerId() {return ownerId;}
    public void setOwnerId(long ownerId) { this.ownerId = ownerId;}
    
    public Jstruct_User getOwner() {return owner;}
    public void setOwner(Jstruct_User owner) { this.owner = owner;}
    
    
    public Date getInsertDate() {  return insertDate; }
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getOverwriteDate() { return overwriteDate; }
    public void setOverwriteDate(Date overwriteDate) { this.overwriteDate = overwriteDate;  }

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return overwriteUserId;}
    public void setUpdateUserId(long overwriteUserId) {this.overwriteUserId = overwriteUserId;}
    
    public Jstruct_User getOverwriteUser() {return overwriteUser;}
    public void setOverwriteUser(Jstruct_User overwriteUser) { this.overwriteUser = overwriteUser;}

    //transient getters/setters

    public boolean isSelected() {  return selected; }
    public void setSelected(boolean selected) { this.selected = selected;}
    
    public List<Jstruct_AnalysisRun> getAnalysisRuns() {return analysisRuns; }
    public void addAnalysisRun(Jstruct_AnalysisRun analysisRun){
        if(analysisRun != null){
            this.analysisRuns.add(analysisRun);
        }
    }
    public void addAnalysisRuns(List<Jstruct_AnalysisRun> analysisRuns){
        if(CollectionUtil.hasValues(analysisRuns)){
            for(Jstruct_AnalysisRun analysisRun : analysisRuns){
                addAnalysisRun(analysisRun);
            }
        }
    }
    
    public List<Jstruct_AnalysisFile> getAnalysisFiles() {return analysisFiles; }
    public void addAnalysisFile(Jstruct_AnalysisFile analysisFile){
        if(analysisFile != null){
            this.analysisFiles.add(analysisFile);
        }
    }
    public void addAnalysisFiles(List<Jstruct_AnalysisFile> analysisFiles){
        if(CollectionUtil.hasValues(analysisFiles)){
            for(Jstruct_AnalysisFile analysisFile : analysisFiles){
                addAnalysisFile(analysisFile);
            }
        }
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (analysisScriptVerId != null ? analysisScriptVerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_AnalysisScriptVer)) {
            return false;
        }
        Jstruct_AnalysisScriptVer other = (Jstruct_AnalysisScriptVer) object;
        if ((this.analysisScriptVerId == null && other.analysisScriptVerId != null) || (this.analysisScriptVerId != null && !this.analysisScriptVerId.equals(other.analysisScriptVerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_AnalysisScriptVer[ analysisScriptVerId=" + analysisScriptVerId + " ]";
    }
    
}
