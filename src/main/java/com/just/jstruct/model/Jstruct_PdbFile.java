package com.just.jstruct.model;

import com.just.bio.structure.Structure;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: pdb_file
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_file", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbFile.findByStructVerId", 
                                                        query = "SELECT pf FROM Jstruct_PdbFile pf "
                                                              + "WHERE pf.structVerId = :structVerId"),
    
        
})
public class Jstruct_PdbFile implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_file_pdb_file_id_seq", sequenceName="pdb_file_pdb_file_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_file_pdb_file_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_file_id")
    private Long pdbFileId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    @Size(max = 4)
    @Column(name = "pdb_identifier")
    private String pdbIdentifier;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "title")
    private String title;
    
    @Size(max = 100)
    @Column(name = "classification")
    private String classification;
    
    @Column(name = "deposition_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date depositionDate;
    
    @Size(max = 2147483647)
    @Column(name = "caveat")
    private String caveat;
    
    @Size(max = 2147483647)
    @Column(name = "expdta")
    private String expdta;
    
    @Column(name = "nummdl")
    private Long nummdl;
    
    @Size(max = 2147483647)
    @Column(name = "mdltyp")
    private String mdltyp;
    
    @Size(max = 100)
    @Column(name = "resolution_line")
    private String resolutionLine;
    
    @Size(max = 100)
    @Column(name = "resolution_unit")
    private String resolutionUnit;
    
    @Column(name = "resolution_measure")
    private Double resolutionMeasure;
    
    
    @Transient private List<Jstruct_PdbSplit> pdbSplits;
    @Transient private List<Jstruct_PdbKeyword> pdbKeywords;
    @Transient private List<Jstruct_PdbAuthor> pdbAuthors;
    @Transient private List<Jstruct_PdbJrnl> pdbJrnls;
    @Transient private List<Jstruct_PdbCompnd> pdbCompnds;
    @Transient private List<Jstruct_PdbSource> pdbSources;
    @Transient private List<Jstruct_PdbRevdat> pdbRevdats;
    @Transient private List<Jstruct_PdbRemark> pdbRemarks;
    @Transient private List<Jstruct_PdbSsbond> pdbSsbonds;
    @Transient private List<Jstruct_PdbSeqres> pdbSeqress;
    
    @Transient private String keywordString;
    @Transient private String authorString;
    

    public Jstruct_PdbFile() {
    }

    
    public Jstruct_PdbFile(Structure structure){
        if(structure.getMetadata()!= null){
            this.pdbIdentifier  = structure.getMetadata().getPdbIdentifier();
            this.title          = structure.getMetadata().getTitle();
            this.classification = structure.getMetadata().getClassification();
            this.depositionDate = structure.getMetadata().getDepositionDate();
            this.caveat         = structure.getMetadata().getCaveat();
            this.expdta         = structure.getMetadata().getExpdta();
            this.nummdl         = structure.getMetadata().getNummdl();
            this.mdltyp         = structure.getMetadata().getMdltyp();
        }
        
    }
    
    
    
    

    public Long getPdbFileId() {
        return pdbFileId;
    }

    public void setPdbFileId(Long pdbFileId) {
        this.pdbFileId = pdbFileId;
    }

    
    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    
    
    
    public String getPdbIdentifier() {
        return pdbIdentifier;
    }

    public void setPdbIdentifier(String pdbIdentifier) {
        this.pdbIdentifier = pdbIdentifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public Date getDepositionDate() {
        return depositionDate;
    }

    public void setDepositionDate(Date depositionDate) {
        this.depositionDate = depositionDate;
    }

    public String getCaveat() {
        return caveat;
    }

    public void setCaveat(String caveat) {
        this.caveat = caveat;
    }

    public String getExpdta() {
        return expdta;
    }

    public void setExpdta(String expdta) {
        this.expdta = expdta;
    }

    public Long getNummdl() {
        return nummdl;
    }

    public void setNummdl(Long nummdl) {
        this.nummdl = nummdl;
    }

    public String getMdltyp() {
        return mdltyp;
    }

    public void setMdltyp(String mdltyp) {
        this.mdltyp = mdltyp;
    }

    public String getResolutionLine() {
        return resolutionLine;
    }

    public void setResolutionLine(String resolutionLine) {
        this.resolutionLine = resolutionLine;
    }

    public String getResolutionUnit() {
        return resolutionUnit;
    }

    public void setResolutionUnit(String resolutionUnit) {
        this.resolutionUnit = resolutionUnit;
    }

    public Double getResolutionMeasure() {
        return resolutionMeasure;
    }

    public void setResolutionMeasure(Double resolutionMeasure) {
        this.resolutionMeasure = resolutionMeasure;
    }

    public List<Jstruct_PdbSplit> getPdbSplits() {
        return pdbSplits;
    }

    public void setPdbSplits(List<Jstruct_PdbSplit> pdbSplits) {
        this.pdbSplits = pdbSplits;
    }

    public List<Jstruct_PdbKeyword> getPdbKeywords() {
        return pdbKeywords;
    }

    public void setPdbKeywords(List<Jstruct_PdbKeyword> pdbKeywords) {
        this.pdbKeywords = pdbKeywords;
    }

    public List<Jstruct_PdbAuthor> getPdbAuthors() {
        return pdbAuthors;
    }

    public void setPdbAuthors(List<Jstruct_PdbAuthor> pdbAuthors) {
        this.pdbAuthors = pdbAuthors;
    }

    public List<Jstruct_PdbJrnl> getPdbJrnls() {
        return pdbJrnls;
    }

    public void setPdbJrnls(List<Jstruct_PdbJrnl> pdbJrnls) {
        this.pdbJrnls = pdbJrnls;
    }

    public List<Jstruct_PdbCompnd> getPdbCompnds() {
        return pdbCompnds;
    }

    public void setPdbCompnds(List<Jstruct_PdbCompnd> pdbCompnds) {
        this.pdbCompnds = pdbCompnds;
    }

    public List<Jstruct_PdbSource> getPdbSources() {
        return pdbSources;
    }

    public void setPdbSources(List<Jstruct_PdbSource> pdbSources) {
        this.pdbSources = pdbSources;
    }

    public List<Jstruct_PdbRevdat> getPdbRevdats() {
        return pdbRevdats;
    }

    public void setPdbRevdats(List<Jstruct_PdbRevdat> pdbRevdats) {
        this.pdbRevdats = pdbRevdats;
    }

    public List<Jstruct_PdbRemark> getPdbRemarks() {
        return pdbRemarks;
    }

    public void setPdbRemarks(List<Jstruct_PdbRemark> pdbRemarks) {
        this.pdbRemarks = pdbRemarks;
    }

    public List<Jstruct_PdbSsbond> getPdbSsbonds() {
        return pdbSsbonds;
    }

    public void setPdbSsbonds(List<Jstruct_PdbSsbond> pdbSsbonds) {
        this.pdbSsbonds = pdbSsbonds;
    }

    public List<Jstruct_PdbSeqres> getPdbSeqress() {
        return pdbSeqress;
    }

    public void setPdbSeqress(List<Jstruct_PdbSeqres> pdbSeqress) {
        this.pdbSeqress = pdbSeqress;
    }

    public String getKeywordString() {
        return keywordString;
    }

    public void setKeywordString(String keywordString) {
        this.keywordString = keywordString;
    }

    public String getAuthorString() {
        return authorString;
    }

    public void setAuthorString(String authorString) {
        this.authorString = authorString;
    }

    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbFileId != null ? pdbFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbFile)) {
            return false;
        }
        Jstruct_PdbFile other = (Jstruct_PdbFile) object;
        if ((this.pdbFileId == null && other.pdbFileId != null) || (this.pdbFileId != null && !this.pdbFileId.equals(other.pdbFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbFile[ pdbFileId=" + pdbFileId + " ]";
    }
    
    
    
}
