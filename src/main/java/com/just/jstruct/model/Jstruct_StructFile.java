package com.just.jstruct.model;

import com.hfg.util.HexUtil;
import com.hfg.util.StringUtil;
import com.just.bio.structure.Structure;
import com.just.jstruct.enums.ProcessChainStatus;
import com.just.jstruct.utilities.JFileUtils;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: struct_file
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "struct_file", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_StructFile.findByStructVerId", 
                                                            query = "SELECT sf FROM Jstruct_StructFile sf "
                                                                  + "WHERE sf.structVerId = :structVerId"),
    
    @NamedQuery(name = "Jstruct_StructFile.findByStructVerIds", 
                                                            query = "SELECT sf FROM Jstruct_StructFile sf "
                                                                  + "WHERE sf.structVerId IN (:structVerIds)")
    

})
public class Jstruct_StructFile implements Serializable 
{
    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="struct_file_struct_file_id_seq", sequenceName="struct_file_struct_file_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="struct_file_struct_file_id_seq")
    @Basic(optional = false)
    @Column(name = "struct_file_id")
    private Long structFileId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    @Size(max = 4)
    @Column(name = "pdb_identifier")
    private String pdbIdentifier;
    
//    @Lob
//    @Column(name = "data")
//    private byte[] data;
    
    @Size(max = 50)
    @Column(name = "checksum")
    private String checksum;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "uncompressed_size")
    private long uncompressedSize;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "from_location")
    private String fromLocation;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "upload_file_name")
    private String uploadFileName;
    
    @Size(max = 255)
    @Column(name = "upload_file_ext")
    private String uploadFileExt;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "actual_file_name")
    private String actualFileName;
    
    @Size(max = 255)
    @Column(name = "actual_file_ext")
    private String actualFileExt;
    
    @Column(name = "file_creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fileCreationDate;
    
    @Column(name = "file_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fileModifiedDate;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "process_chain_status")
    private String processChainStatus;
    
    
    
    
    
    /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    @Column(name = "update_user_id", insertable=false, updatable=false)
    private Long updateUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "update_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User updateUser;
    
    /* --------- end audit fields --------- */
    
    
    

    public Jstruct_StructFile(){}

    public Jstruct_StructFile(Jstruct_User user) 
    {
        this.insertUser = user;
        this.updateUser = user;
        this.insertDate = new Date();
        this.updateDate = new Date();
    }

    
    
    public Jstruct_StructFile(Jstruct_User user, Structure structureObj)
    {
        this(user);
        
        this.pdbIdentifier = structureObj.getPdbIdentifier();
        this.uncompressedSize = structureObj.getUncompressedSizeInBytes();
        this.checksum = HexUtil.byteArrayToHexString(structureObj.getMd5Checksum());

        if(StringUtil.isSet(structureObj.getFileRestUrl())) 
        {

            //this originally came from a URL
            this.fromLocation = structureObj.getFileRestUrl();

            String fileNameFromUrl = JFileUtils.getFileNameFromUrl(structureObj.getFileRestUrl());

            this.uploadFileName = fileNameFromUrl;
            this.uploadFileExt = JFileUtils.getExtension(fileNameFromUrl);

            this.actualFileName = JFileUtils.getFileNameWithoutGz(fileNameFromUrl);
            this.actualFileExt = JFileUtils.getExtensionWithoutGz(fileNameFromUrl);

        } 
        else 
        {

            //this originally came from a file
            this.fromLocation = structureObj.getFilePath();

            //String originalPath = structureObj.getFilePath();
            String originalName = JFileUtils.getFileName(structureObj.getFilePath());


            this.uploadFileName = originalName;
            this.uploadFileExt = JFileUtils.getExtension(originalName);

            this.actualFileName = JFileUtils.getFileNameWithoutGz(originalName);
            this.actualFileExt = JFileUtils.getExtensionWithoutGz(originalName);

        }
        
    }
    
    
    
    
    
    

    public Long getStructFileId() {
        return structFileId;
    }

    public void setStructFileId(Long structFileId) {
        this.structFileId = structFileId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    
    
    public String getPdbIdentifier() {
        return pdbIdentifier;
    }

    public void setPdbIdentifier(String pdbIdentifier) {
        this.pdbIdentifier = pdbIdentifier;
    }

 //   public byte[] getData() {
 //       return data;
 //   }

 //   public void setData(byte[] data) {
 //       this.data = data;
 //   }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }
    
    public void setChecksum(byte[] checksumBytes){
        this.checksum = HexUtil.byteArrayToHexString(checksumBytes);
    }
    
    
    

    public long getUncompressedSize() {
        return uncompressedSize;
    }

    public void setUncompressedSize(long uncompressedSize) {
        this.uncompressedSize = uncompressedSize;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String getUploadFileExt() {
        return uploadFileExt;
    }

    public void setUploadFileExt(String uploadFileExt) {
        this.uploadFileExt = uploadFileExt;
    }

    public String getActualFileName() {
        return actualFileName;
    }

    public void setActualFileName(String actualFileName) {
        this.actualFileName = actualFileName;
    }

    public String getActualFileExt() {
        return actualFileExt;
    }

    public void setActualFileExt(String actualFileExt) {
        this.actualFileExt = actualFileExt;
    }

    public Date getFileCreationDate() {
        return fileCreationDate;
    }

    public void setFileCreationDate(Date fileCreationDate) {
        this.fileCreationDate = fileCreationDate;
    }

    public Date getFileModifiedDate() {
        return fileModifiedDate;
    }

    public void setFileModifiedDate(Date fileModifiedDate) {
        this.fileModifiedDate = fileModifiedDate;
    }

    public ProcessChainStatus getProcessChainStatus() {
        return ProcessChainStatus.valueOf(processChainStatus);
    }

    public void setProcessChainStatus(ProcessChainStatus processChainStatus) {
        this.processChainStatus = processChainStatus.name();
    }
    
    
    

    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getUpdateDate() {return updateDate;}
    public void setUpdateDate(Date updateDate) { this.updateDate = updateDate;}

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return updateUserId;}
    public void setUpdateUserId(long updateUserId) {this.updateUserId = updateUserId;}
    
    public Jstruct_User getUpdateUser() {return updateUser;}
    public void setUpdateUser(Jstruct_User updateUser) { this.updateUser = updateUser;}
    
     

    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (structFileId != null ? structFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_StructFile)) {
            return false;
        }
        Jstruct_StructFile other = (Jstruct_StructFile) object;
        if ((this.structFileId == null && other.structFileId != null) || (this.structFileId != null && !this.structFileId.equals(other.structFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_StructFile[ structFileId=" + structFileId + " ]";
    }
    
}
