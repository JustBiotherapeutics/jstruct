package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing split (SPLIT) PDB Header information (related pdb entries)
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#SPLIT
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_split", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbSplit.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbSplit p "
                                                          + "WHERE p.structVerId = :structVerId"),
    
    @NamedQuery(name = "Jstruct_PdbSplit.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbSplit p "
                                                          + "WHERE p.structVerId = :structVerId")
        
})
public class Jstruct_PdbSplit implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_split_pdb_split_id_seq", sequenceName="pdb_split_pdb_split_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_split_pdb_split_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_split_id")
    private Long pdbSplitId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "pdb_identifier")
    private String pdbIdentifier;
    
    @Size(max = 100)
    @Column(name = "db_name")
    private String dbName;
    
    @Size(max = 100)
    @Column(name = "content_type")
    private String contentType;
    
    @Size(max = 2000)
    @Column(name = "detail")
    private String detail;
    

    public Jstruct_PdbSplit() {
    }

    
    
    public Jstruct_PdbSplit(Long structVerId, String pdbIdentifier, String dbName, String contentType, String detail) {
        this.structVerId = structVerId;
        this.pdbIdentifier = pdbIdentifier;
        this.dbName = dbName;
        this.contentType = contentType;
        this.detail = detail;
    }


    
    

    public Long getPdbSplitId() {
        return pdbSplitId;
    }

    public void setPdbSplitId(Long pdbSplitId) {
        this.pdbSplitId = pdbSplitId;
    }

    
    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public String getPdbIdentifier() {
        return pdbIdentifier;
    }

    public void setPdbIdentifier(String pdbIdentifier) {
        this.pdbIdentifier = pdbIdentifier;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbSplitId != null ? pdbSplitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbSplit)) {
            return false;
        }
        Jstruct_PdbSplit other = (Jstruct_PdbSplit) object;
        if ((this.pdbSplitId == null && other.pdbSplitId != null) || (this.pdbSplitId != null && !this.pdbSplitId.equals(other.pdbSplitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbSplit[ pdbSplitId=" + pdbSplitId + " ]";
    }
    
}
