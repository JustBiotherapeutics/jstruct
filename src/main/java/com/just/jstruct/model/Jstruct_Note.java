package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * Entity class for: note
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "note", schema = "public")
@NamedQueries({

    @NamedQuery(name = "Jstruct_Note.getByNoteId", 
                                                query = "SELECT n FROM Jstruct_Note n "
                                                      + " WHERE n.noteId = :noteId"),

    @NamedQuery(name = "Jstruct_Note.getByStructureIdForUser", 
                                                query = "SELECT n FROM Jstruct_Note n "
                                                      + " WHERE n.structureId = :structureId "
                                                      + "   AND ( n.isPublic LIKE 'Y' OR  n.insertUserId = :userId ) "
                                                      + " ORDER BY n.noteId"),

    @NamedQuery(name = "Jstruct_Note.getByStructVerIdForUser", 
                                                query = "SELECT n FROM Jstruct_Note n "
                                                      + " WHERE n.structVerId = :structVerId "
                                                      + "   AND ( n.isPublic LIKE 'Y' OR  n.insertUserId = :userId ) "
                                                      + " ORDER BY n.noteId")

})
public class Jstruct_Note implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "note_id")
    private Long noteId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "structure_id")
    private Long structureId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "lower_comment")
    private String lowerComment;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "formatted_comment")
    private String formattedComment;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_public")
    private Character isPublic;
    
    
    
     /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.EAGER)
    private Jstruct_User insertUser;
    
    @Column(name = "update_user_id", insertable=false, updatable=false)
    private Long updateUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "update_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Jstruct_User updateUser;
    
    /* --------- end audit fields --------- */
    
    
    
    
    
    

    public Jstruct_Note() {
    }
    
    public Jstruct_Note(boolean isNotePublic) {
        this.isPublic = isNotePublic ? 'Y' : 'N' ;
    }



    public Long getNoteId() { return noteId;}
    public void setNoteId(Long noteId) {  this.noteId = noteId;}
    
    
    
    public Long getStructureId() {return structureId;}
    public void setStructureId(Long structureId) {this.structureId = structureId;}

    public Long getStructVerId() {return structVerId; }
    public void setStructVerId(Long structVerId) {this.structVerId = structVerId;}
    
    
    
    
    public String getLowerComment() {return lowerComment;}
    public void setLowerComment(String lowerComment) {this.lowerComment = lowerComment;}
    
    public String getFormattedComment() {return formattedComment;}
    public void setFormattedComment(String formattedComment) {this.formattedComment = formattedComment;}
    
    public boolean isNotePublic() { return isPublic.equals('Y') || isPublic.equals('y'); }
    public void setNotePublic(boolean isPublic) { this.isPublic = isPublic ? 'Y' : 'N' ; }
    
    
    
    
    
    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getUpdateDate() {return updateDate;}
    public void setUpdateDate(Date updateDate) { this.updateDate = updateDate;}

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return updateUserId;}
    public void setUpdateUserId(long updateUserId) {this.updateUserId = updateUserId;}
    
    public Jstruct_User getUpdateUser() {return updateUser;}
    public void setUpdateUser(Jstruct_User updateUser) { this.updateUser = updateUser;}
    
    
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noteId != null ? noteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_Note)) {
            return false;
        }
        Jstruct_Note other = (Jstruct_Note) object;
        if ((this.noteId == null && other.noteId != null) || (this.noteId != null && !this.noteId.equals(other.noteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_Note[ noteId=" + noteId + " ]";
    }
    
}
