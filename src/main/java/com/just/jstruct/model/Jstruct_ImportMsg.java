package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: import_msg
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "import_msg", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_ImportMsg.findByImportMsgId", 
                                                query = "SELECT im FROM Jstruct_ImportMsg im "
                                                      + " WHERE im.importMsgId = :importMsgId "),
    
    @NamedQuery(name = "Jstruct_ImportMsg.findByImportJobId", 
                                                query = "SELECT im FROM Jstruct_ImportMsg im "
                                                      + " WHERE im.importJobId = :importJobId ")
        
})
public class Jstruct_ImportMsg implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    
    
    @Id
    @SequenceGenerator(name="import_msg_import_msg_id_seq", sequenceName="import_msg_import_msg_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="import_msg_import_msg_id_seq")
    @Basic(optional = false)
    @Column(name = "import_msg_id")
    private Long importMsgId;
    
    
    @Size(max = 2000)
    @Column(name = "file_info")
    private String fileInfo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "message")
    private String message;
    
    @Size(max = 2147483647)
    @Column(name = "stack_trace")
    private String stackTrace;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "import_job_id")
    private Long importJobId;
    
    
    //@JoinColumn(name = "import_job_id", referencedColumnName = "import_job_id")
    //@ManyToOne
    //private Jstruct_ImportJob importJobId;
    
    
     /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    /* --------- end audit fields --------- */
    

    public Jstruct_ImportMsg() {
    }

   

    public Jstruct_ImportMsg(Jstruct_User user) {
        this.insertUser = user;
        this.insertDate = new Date();
    }

   
    
    
    
    
    public Long getImportMsgId() {
        return importMsgId;
    }

    public void setImportMsgId(Long importMsgId) {
        this.importMsgId = importMsgId;
    }

    public String getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(String fileInfo) {
        this.fileInfo = fileInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }


    public Long getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(Long importJobId) {
        this.importJobId = importJobId;
    }
    
    
    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}
 
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (importMsgId != null ? importMsgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_ImportMsg)) {
            return false;
        }
        Jstruct_ImportMsg other = (Jstruct_ImportMsg) object;
        if ((this.importMsgId == null && other.importMsgId != null) || (this.importMsgId != null && !this.importMsgId.equals(other.importMsgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_ImportMsg[ importMsgId=" + importMsgId + " ]";
    }
    
}
