package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: import_pdb
 */
@Entity
@Table(name = "import_pdb", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_ImportPdb.findByImportPdbId", 
                                                        query = "SELECT ip FROM Jstruct_ImportPdb ip "
                                                              + "WHERE ip.importPdbId = :importPdbId "),
    
    @NamedQuery(name = "Jstruct_ImportPdb.findByImportRestId", 
                                                        query = "SELECT ip FROM Jstruct_ImportPdb ip "
                                                              + "WHERE ip.importRestId = :importRestId "),
    
    @NamedQuery(name = "Jstruct_ImportPdb.findByImportJobId", 
                                                        query = "SELECT ip FROM Jstruct_ImportPdb ip "
                                                              + "WHERE ip.importRestId IN "
                                                              + "   ( "
                                                              + "     SELECT ir.importRestId FROM Jstruct_ImportRest ir "
                                                              + "      WHERE ir.importJobId = :importJobId "
                                                              + "   ) ")
})
public class Jstruct_ImportPdb implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    
    @Id
    @SequenceGenerator(name="import_pdb_import_pdb_id_seq", sequenceName="import_pdb_import_pdb_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="import_pdb_import_pdb_id_seq")
    @Basic(optional = false)
    @Column(name = "import_pdb_id")
    private Long importPdbId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "pdb_identifier")
    private String pdbIdentifier;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "import_rest_id")
    private Long importRestId;
    
    
    //@JoinColumn(name = "import_rest_id", referencedColumnName = "import_rest_id")
    //@ManyToOne(optional = false)
    //private Jstruct_ImportRest importRestId;
    
    

    public Jstruct_ImportPdb() {
    }

    
    
    

    public Long getImportPdbId() {
        return importPdbId;
    }

    public void setImportPdbId(Long importPdbId) {
        this.importPdbId = importPdbId;
    }

    public String getPdbIdentifier() {
        return pdbIdentifier;
    }

    public void setPdbIdentifier(String pdbIdentifier) {
        this.pdbIdentifier = pdbIdentifier;
    }

    public Long getImportRestId() {
        return importRestId;
    }

    public void setImportRestId(Long importRestId) {
        this.importRestId = importRestId;
    }

    
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (importPdbId != null ? importPdbId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_ImportPdb)) {
            return false;
        }
        Jstruct_ImportPdb other = (Jstruct_ImportPdb) object;
        if ((this.importPdbId == null && other.importPdbId != null) || (this.importPdbId != null && !this.importPdbId.equals(other.importPdbId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_ImportPdb[ importPdbId=" + importPdbId + " ]";
    }
    
}
