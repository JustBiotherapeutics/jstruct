package com.just.jstruct.model;

import com.just.jstruct.enums.JobStatus;
import com.just.jstruct.enums.JobType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: import_job
 */
@Entity
@Table(name = "import_job", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_ImportJob.findAll", query = "SELECT ij FROM Jstruct_ImportJob ij"),
    
    @NamedQuery(name = "Jstruct_ImportJob.findByImportJobId", 
                                                    query = "SELECT ij FROM Jstruct_ImportJob ij "
                                                          + " WHERE ij.importJobId = :importJobId"),
    
    @NamedQuery(name = "Jstruct_ImportJob.findAllRcsbImportJobs", 
                                                    query = "SELECT ij FROM Jstruct_ImportJob ij "
                                                            + " WHERE  UPPER(ij.importType) LIKE 'INITIAL_RCSB_RUN' "
                                                            + "    OR  UPPER(ij.importType) LIKE 'INITIAL_RCSB_RUN_RECOVER' "
                                                            + "    OR  UPPER(ij.importType) LIKE 'INCREMENTAL_RCSB_RUN' "
                                                            + "    OR  UPPER(ij.importType) LIKE 'INITIAL_RCSB_RERUN' "
                                                            + "    OR  UPPER(ij.importType) LIKE 'SELECTED_RCSB_FILES' "
                                                            + "    OR  UPPER(ij.importType) LIKE 'UPDATE_ALL_CHAINS' "
                                                            + "    OR  UPPER(ij.importType) LIKE 'UPDATE_MANUAL_CHAINS' "
                                                            + "    OR  UPPER(ij.importType) LIKE 'UPDATE_MISSING_CHAINS' "
                                                            + "    OR  UPPER(ij.importType) LIKE 'RCSB_HARMONIZE' "
                                                            + "ORDER BY ij.importJobId DESC " ),
     
    @NamedQuery(name = "Jstruct_ImportJob.findRecentSuccessfulRcsbRuns", 
                                                    query = "SELECT ij FROM Jstruct_ImportJob ij "
                                                          + " WHERE UPPER(ij.importStatus) LIKE 'SUCCESS' "
                                                          + "   AND ( "
                                                          + "            UPPER(ij.importType) LIKE 'INITIAL_RCSB_RUN' "
                                                          + "        OR  UPPER(ij.importType) LIKE 'INITIAL_RCSB_RUN_RECOVER' "
                                                          + "        OR  UPPER(ij.importType) LIKE 'INCREMENTAL_RCSB_RUN' "
                                                          + "        OR  UPPER(ij.importType) LIKE 'INITIAL_RCSB_RERUN' "
                                                          + "       ) "
                                                          + "ORDER BY ij.runStart DESC "),         
    
    @NamedQuery(name = "Jstruct_ImportJob.findOnlyInitialRcsbRuns", 
                                                    query = "SELECT ij FROM Jstruct_ImportJob ij "
                                                          + " WHERE UPPER(ij.importType) LIKE 'INITIAL_RCSB_RUN' "
                                                          + "ORDER BY ij.runStart DESC "),         
    
    @NamedQuery(name = "Jstruct_ImportJob.findOnlySuccessfulInitialRcsbRuns", 
                                                    query = "SELECT ij FROM Jstruct_ImportJob ij "
                                                          + " WHERE UPPER(ij.importStatus) LIKE 'SUCCESS' "
                                                          + "   AND ( "
                                                          + "            UPPER(ij.importType) LIKE 'INITIAL_RCSB_RUN' "
                                                          + "        OR  UPPER(ij.importType) LIKE 'INITIAL_RCSB_RUN_RECOVER' "
                                                          + "        OR  UPPER(ij.importType) LIKE 'INITIAL_RCSB_RERUN' "
                                                          + "       ) "
                                                          + "ORDER BY ij.runStart DESC "),          
    

})
public class Jstruct_ImportJob implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    
    
    @Id
    @SequenceGenerator(name="import_job_import_job_id_seq", sequenceName="import_job_import_job_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="import_job_import_job_id_seq")
    @Basic(optional = false)
    @Column(name = "import_job_id")
    private Long importJobId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "run_start")
    @Temporal(TemporalType.TIMESTAMP)
    private Date runStart;
    
    
    @Column(name = "import_end")
    @Temporal(TemporalType.TIMESTAMP)
    private Date importEnd;
    
    
    @Column(name = "run_end")
    @Temporal(TemporalType.TIMESTAMP)
    private Date runEnd;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "import_type")
    private String importType;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "import_status")
    private String importStatus;
    
    
    @Size(max = 2147483647)
    @Column(name = "failure_message")
    private String failureMessage;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "count_unique_source")
    private int countUniqueSource;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "count_imported")
    private int countImported;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "count_added")
    private int countAdded;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "count_updated")
    private int countUpdated;
    
    
    @Size(max = 2000)
    @Column(name = "pdb_rest_url")
    private String pdbRestUrl;
    
    
    @Size(max = 2147483647)
    @Column(name = "pdb_rest_criteria")
    private String pdbRestCriteria;
    
    
    @Column(name = "pdb_search_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pdbSearchDate;
    
    @Column(name = "run_by_user_id", insertable=false, updatable=false)
    private Long runByUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "run_by_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Jstruct_User runByUser;
    
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "importJobId")
    //private List<Jstruct_ImportRest> jstructImportRestList;
    
    
    //@OneToMany(mappedBy = "importJobId")
    //private List<Jstruct_ImportMsg> jstructImportMsgList;
    
    @Transient List<Jstruct_ImportRest> importRests;
    @Transient List<Jstruct_ImportMsg> importMsgs;
    
    
    
    

    public Jstruct_ImportJob() { }

    public Jstruct_ImportJob(Jstruct_User user, JobType importJobType, JobStatus importJobStatus)
    {
        this.runByUser = user;
        this.runStart = new Date();
        this.countUniqueSource = 0;
        this.countImported = 0;
        this.countAdded = 0;
        this.countUpdated = 0;
        
        this.importType = importJobType.name();
        if(importJobStatus == null){
            importJobStatus = JobStatus.IMPORTING;
        }
        this.importStatus = importJobStatus.name();
    }
    
    
    
    

    public Long getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(Long importJobId) {
        this.importJobId = importJobId;
    }

    public Date getRunStart() {
        return runStart;
    }

    public void setRunStart(Date runStart) {
        this.runStart = runStart;
    }

    public Date getImportEnd() {
        return importEnd;
    }

    public void setImportEnd(Date importEnd) {
        this.importEnd = importEnd;
    }

    public Date getRunEnd() {
        return runEnd;
    }

    public void setRunEnd(Date runEnd) {
        this.runEnd = runEnd;
    }

    
    public JobType getImportType() {return JobType.valueOf(importType);}
    public String getImportTypeAsString() {return importType;}
    public void setImportType(JobType importJobType) {this.importType = importJobType.name(); }

    public JobStatus getImportStatus() {return  JobStatus.valueOf(importStatus);}
    public void setImportStatus(JobStatus importJobStatus) {this.importStatus = importJobStatus.name();}

    
    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public int getCountUniqueSource() {
        return countUniqueSource;
    }

    public void setCountUniqueSource(int countUniqueSource) {
        this.countUniqueSource = countUniqueSource;
    }

    public int getCountImported() {
        return countImported;
    }

    public void setCountImported(int countImported) {
        this.countImported = countImported;
    }

    public int getCountAdded() {
        return countAdded;
    }

    public void setCountAdded(int countAdded) {
        this.countAdded = countAdded;
    }

    public int getCountUpdated() {
        return countUpdated;
    }

    public void setCountUpdated(int countUpdated) {
        this.countUpdated = countUpdated;
    }

    public String getPdbRestUrl() {
        return pdbRestUrl;
    }

    public void setPdbRestUrl(String pdbRestUrl) {
        this.pdbRestUrl = pdbRestUrl;
    }

    public String getPdbRestCriteria() {
        return pdbRestCriteria;
    }

    public void setPdbRestCriteria(String pdbRestCriteria) {
        this.pdbRestCriteria = pdbRestCriteria;
    }

    public Date getPdbSearchDate() {
        return pdbSearchDate;
    }

    public void setPdbSearchDate(Date pdbSearchDate) {
        this.pdbSearchDate = pdbSearchDate;
    }

   
    public long getRunByUserId() {return runByUserId;}
    public void setRunByUserId(long runByUserId) { this.runByUserId = runByUserId;}
    
    public Jstruct_User getRunByUser() {return runByUser;}
    public void setRunByUser(Jstruct_User runByUser) { this.runByUser = runByUser;}
    
    
    
    public List<Jstruct_ImportRest> getImportRests() {return importRests;}
    public void setImportRests(List<Jstruct_ImportRest> importRests) {this.importRests = importRests;}
    
    
    
    public List<Jstruct_ImportMsg> getImportMsgs() {return importMsgs;}
    public void setImportMsgs(List<Jstruct_ImportMsg> importMsgs) {this.importMsgs = importMsgs;}
    
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (importJobId != null ? importJobId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_ImportJob)) {
            return false;
        }
        Jstruct_ImportJob other = (Jstruct_ImportJob) object;
        if ((this.importJobId == null && other.importJobId != null) || (this.importJobId != null && !this.importJobId.equals(other.importJobId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_ImportJob[ importJobId=" + importJobId + " ]";
    }
    
}
