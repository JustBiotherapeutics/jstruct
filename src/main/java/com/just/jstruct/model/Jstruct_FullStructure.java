package com.just.jstruct.model;

import com.just.jstruct.utilities.ValueMapUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

/**
 * Entity class for: full_structure_view
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "full_structure_view", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_FullStructure.findByStructVerId", 
                                                query = "SELECT fs FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.structVerId = :structVerId"),
        
    @NamedQuery(name = "Jstruct_FullStructure.findByStructVerIds", 
                                                query = "SELECT fs FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.structVerId IN (:structVerIds) "),
    
    @NamedQuery(name = "Jstruct_FullStructure.findByStructureId", 
                                                query = "SELECT fs FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.structureId = :structureId "
                                                      + "ORDER BY fs.version DESC"),
    
    @NamedQuery(name = "Jstruct_FullStructure.findByStructureIdAndVersion", 
                                                query = "SELECT fs FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.structureId = :structureId "
                                                      + "  AND fs.version = :version"),
    
    
    @NamedQuery(name = "Jstruct_FullStructure.findByPdbIdentifierFromRcsb", 
                                                query = "SELECT fs FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.pdbIdentifier = :pdbIdentifier "
                                                      + "   AND fs.fromRcsb LIKE 'Y'"),
    
    @NamedQuery(name = "Jstruct_FullStructure.findByPdbIdentifiersFromRcsb", 
                                                query = "SELECT fs FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.pdbIdentifier IN (:pdbIdentifiers) "
                                                      + "  AND fs.fromRcsb LIKE 'Y'"),
    
    @NamedQuery(name = "Jstruct_FullStructure.findByPdbIdentifiers", 
                                                query = "SELECT fs FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.pdbIdentifier IN (:pdbIdentifiers) "),
    
    
    
    @NamedQuery(name = "Jstruct_FullStructure.findObsoletePdbIdsFromRcsbRun", 
                                                query = "SELECT fs.pdbIdentifier FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.fromRcsb LIKE 'Y' "
                                                      + "  AND fs.isObsolete LIKE 'Y' "),
    
    @NamedQuery(name = "Jstruct_FullStructure.findActivePdbIdsFromRcsbRun", 
                                                query = "SELECT fs.pdbIdentifier FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.fromRcsb LIKE 'Y' "
                                                      + "   AND fs.isObsolete LIKE 'N' "),
    
    @NamedQuery(name = "Jstruct_FullStructure.findAllPdbIdsFromRcsbRun", 
                                                query = "SELECT fs.pdbIdentifier FROM Jstruct_FullStructure fs "
                                                      + " WHERE fs.fromRcsb LIKE 'Y' "),
})
public class Jstruct_FullStructure implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Column(name = "max_version")
    private Integer maxVersion;
    
    @Column(name = "max_struct_ver_id")
    private Long maxStructVerId;
    
    @Size(max = 2147483647)
    @Column(name = "most_current")
    private String mostCurrent;
    
    
    
    
    @Column(name = "structure_id")
    private Long structureId;
    
    @Column(name = "owner_id")
    private Long ownerId;
    
    @Size(max = 100)
    @Column(name = "owner_name")
    private String ownerName;
    
    @Column(name = "from_rcsb")
    private Character fromRcsb;
    
    @Size(max = 255)
    @Column(name = "source_name")
    private String sourceName;
    
    @Column(name = "can_all_read")
    private Character canAllRead;
    
    @Column(name = "can_all_write")
    private Character canAllWrite;
    
    @Column(name = "is_obsolete")
    private Character isObsolete;
    
    @Column(name = "obsolete_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date obsoleteDate;
    
    
    
    
    
    @Id
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    @Column(name = "version")
    private Integer version;
    
    @Size(max = 255)
    @Column(name = "struct_method")
    private String structMethod;
    
    @Size(max = 255)
    @Column(name = "target")
    private String target;
    
    @Column(name = "is_model")
    private Character isModel;
    
    @Size(max = 2147483647)
    @Column(name = "description")
    private String description;
    
    @Column(name = "struct_ver_insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date structVerInsertDate;
    
    @Column(name = "struct_ver_insert_user_id")
    private Long structVerInsertUserId;
    
    @Size(max = 100)
    @Column(name = "struct_ver_insert_user_name")
    private String structVerInsertUserName;
    
    @Column(name = "struct_ver_update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date structVerUpdateDate;
    
    @Column(name = "struct_ver_update_user_id")
    private Long structVerUpdateUserId;
    
    @Size(max = 100)
    @Column(name = "struct_ver_update_user_name")
    private String structVerUpdateUserName;
    
    @Size(max = 255)
    @Column(name = "client_name")
    private String clientName;
    
    @Column(name = "client_id")
    private Long clientId;
    
    @Size(max = 255)
    @Column(name = "program_name")
    private String programName;
    
    @Column(name = "program_id")
    private Long programId;
    
    
    
    
    @Size(max = 50)
    @Column(name = "checksum")
    private String checksum;
    
    @Column(name = "uncompressed_size")
    private Long uncompressedSize;
    
    @Column(name = "compressed_size")
    private Integer compressedSize;
    
    @Size(max = 2000)
    @Column(name = "from_location")
    private String fromLocation;
    
    @Size(max = 255)
    @Column(name = "upload_file_name")
    private String uploadFileName;
    
    @Size(max = 255)
    @Column(name = "upload_file_ext")
    private String uploadFileExt;
    
    @Size(max = 255)
    @Column(name = "actual_file_name")
    private String actualFileName;
    
    @Size(max = 255)
    @Column(name = "actual_file_ext")
    private String actualFileExt;
    
    @Column(name = "file_creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fileCreationDate;
    
    @Column(name = "file_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fileModifiedDate;
    
    @Size(max = 10)
    @Column(name = "process_chain_status")
    private String processChainStatus;
    
    @Column(name = "struct_file_insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date structFileInsertDate;
    
    @Column(name = "struct_file_insert_user_id")
    private Long structFileInsertUserId;
    
    @Size(max = 100)
    @Column(name = "struct_file_insert_user_name")
    private String structFileInsertUserName;
    
    @Column(name = "struct_file_update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date structFileUpdateDate;
    
    @Column(name = "struct_file_update_user_id")
    private Long structFileUpdateUserId;
    
    @Size(max = 100)
    @Column(name = "struct_file_update_user_name")
    private String structFileUpdateUserName;
    
    
    
    
    
    @Size(max = 4)
    @Column(name = "pdb_identifier")
    private String pdbIdentifier;
    
    @Size(max = 100)
    @Column(name = "pdb_classification")
    private String pdbClassification;
    
    @Column(name = "pdb_deposition_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pdbDepositionDate;
    
    @Size(max = 2147483647)
    @Column(name = "pdb_title")
    private String pdbTitle;
    
    @Size(max = 2147483647)
    @Column(name = "pdb_caveat")
    private String pdbCaveat;
    
    @Size(max = 2147483647)
    @Column(name = "pdb_expdta")
    private String pdbExpdta;
    
    @Column(name = "pdb_nummdl")
    private Long pdbNummdl;
    
    @Size(max = 2147483647)
    @Column(name = "pdb_mdltyp")
    private String pdbMdltyp;
    
    @Size(max = 100)
    @Column(name = "pdb_resolution_line")
    private String pdbResolutionLine;
    
    @Size(max = 100)
    @Column(name = "pdb_resolution_unit")
    private String pdbResolutionUnit;
    
    @Column(name = "pdb_resolution_measure")
    private Double pdbResolutionMeasure;
    
    
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // transient fields for permissions
    
    @Transient
    private List<Jstruct_StructureRole> structureRoles;
    
    @Transient
    private boolean currentUserCanRead = false;
    
    @Transient
    private boolean currentUserCanWrite = false;
    
    @Transient
    private boolean currentUserCanGrant = false;
    
    

    
    public Jstruct_FullStructure() {
    }

    
    
    /**
     * used to create a temporary transient FullStructure object.
     * @param structure
     * @param structVersion
     * @param structFile
     * @param pdbFile 
     */
    public Jstruct_FullStructure(Jstruct_Structure structure, Jstruct_StructVersion structVersion, Jstruct_StructFile structFile, Jstruct_PdbFile pdbFile){
        
        //structure
        structureId = structure.getStructureId();
        ownerId = structure.getOwnerId();
        //ownerName = structure.getOwner().getName();
        ownerId = structure.getOwnerId();
        fromRcsb = structure.isFromRcsb() ? 'Y' : 'N';
        sourceName = structure.getSourceName();
        
        if(canAllRead!=null){
            canAllRead = structure.isCanAllRead() ? 'Y' : 'N';
        } else {
            canAllRead = 'N';
        }
        
        if(canAllWrite!=null){
            canAllWrite = structure.isCanAllWrite() ? 'Y' : 'N';
        } else {
            canAllWrite = 'N';
        }
        
        isObsolete = structure.isObsolete() ? 'Y' : 'N';
        obsoleteDate = structure.getObsoleteDate();
        
        //struct version
        structVerId = structVersion.getStructVerId();
        version = structVersion.getVersion();
        structMethod = structVersion.getStructMethod();
        target = structVersion.getTarget();
        isModel = structVersion.isModel() ? 'Y' : 'N';
        description = structVersion.getDescription();
        //structVerInsertDate = structVersion.getInsertDate();
        //structVerInsertUserId = structVersion.getInsertUserId();
        //structVerInsertUserName = structVersion.getInsertUser().getName();
        //structVerUpdateDate = structVersion.getUpdateDate();
        //structVerUpdateUserId = structVersion.getUpdateUserId();
        //structVerUpdateUserName = structVersion.getUpdateUser().getName();
        //clientName = structVersion.;
        clientId = structVersion.getClientId();
        //programName = structVersion.;
        programId = structVersion.getProgramId();
    
        //struct file
        checksum = structFile.getChecksum();
        uncompressedSize = structFile.getUncompressedSize();
        //compressedSize = structFile.;
        fromLocation = structFile.getFromLocation();
        uploadFileName = structFile.getUploadFileName();
        uploadFileExt = structFile.getUploadFileExt();
        actualFileName = structFile.getActualFileName();
        actualFileExt = structFile.getActualFileExt();
        fileCreationDate = structFile.getFileCreationDate();
        fileModifiedDate = structFile.getFileModifiedDate();
        //processChainStatus = structFile.getProcessChainStatus();
        //structFileInsertDate = structFile.getInsertDate();
        //structFileInsertUserId = structFile.getInsertUserId();
        //structFileInsertUserName = structFile.getInsertUser().getName();
        //structFileUpdateDate = structFile.getUpdateDate();
        //structFileUpdateUserId = structFile.getUpdateUserId();
        //structFileUpdateUserName = structFile.getUpdateUser().getName();
        
        //pdbFile
        pdbIdentifier = pdbFile.getPdbIdentifier();
        pdbClassification = pdbFile.getClassification();
        pdbDepositionDate = pdbFile.getDepositionDate();
        pdbTitle = pdbFile.getTitle();
        pdbCaveat = pdbFile.getCaveat();
        pdbExpdta = pdbFile.getExpdta();
        pdbNummdl = pdbFile.getNummdl();
        pdbMdltyp = pdbFile.getMdltyp();
        pdbResolutionLine = pdbFile.getResolutionLine();
        pdbResolutionUnit = pdbFile.getResolutionUnit();
        pdbResolutionMeasure = pdbFile.getResolutionMeasure();
 
    }
    
    
    /**
     * standard link used throughout the application
     * uses request.getServerName() to create so links stay in the same context as the current URL
     * @return 
     */ 
    public String getLink() {
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        StringBuilder url = new StringBuilder();
        
        url.append(vmu.getBaseUrl());
        url.append("structure.xhtml?id=");
        url.append(structureId);
            
        if(fromRcsb=='N'){
            url.append(".");
            url.append(version);
        }
        
        return url.toString();
    }
            
    
    public String getLinkPermalink(){ return getLinkPermalink(false); }
    public String getLinkPermalinkCurrent(){ return getLinkPermalink(true); }
    
    public String getLinkDownload(){ return getLinkDownload(false, false); }
    public String getLinkDownloadCurrent(){ return getLinkDownload(true, false); }
    public String getLinkDownloadCompressed(){ return getLinkDownload(false, true); }
    
    public String getLinkDisplay(){ return getLinkDisplay(false); }
    public String getLinkDisplayCurrent(){ return getLinkDisplay(true); }
    
    /**
     * get the permalink for this structure
     * @param current
     * @return 
     */
    private String getLinkPermalink(boolean current){
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        StringBuilder url = new StringBuilder();
        
        url.append(vmu.getPermalinkBaseUrl());
        url.append("structure.xhtml?id=");
        url.append(structureId);
            
        if(fromRcsb=='Y' && current){
            url.append("&amp;current");
        } else if(!current){
            url.append(".");
            url.append(version);
        }
        
        return url.toString();
    }
    
    /**
     * get the download link for this structure
     * @param current
     * @param compressed
     * @return 
     */
    public String getLinkDownload(boolean current, boolean compressed) {

        ValueMapUtils vmu = ValueMapUtils.getInstance();
        StringBuilder url = new StringBuilder();

        url.append(vmu.getPermalinkBaseUrl());
        url.append(vmu.getApiStructureBase());
        url.append(vmu.getStructureIdPrefix());
        url.append(structureId);

        if (fromRcsb == 'Y') {
            url.append("/file");
            if (current) {
                url.append("?current");
            }
            if (compressed) {
                url.append(current ? "&amp;compressed" : "?compressed");
            }
        } 
        else 
        {
            if (!current) {
                url.append(".");
                url.append(version);
            }
            url.append("/file");
            if (compressed) {
                url.append("?compressed");
            }
        }
        return url.toString();
    }
    
    /**
     * get the display contents link for this structure
     * @param current
     * @return 
     */
    public String getLinkDisplay(boolean current) {

        ValueMapUtils vmu = ValueMapUtils.getInstance();
        StringBuilder url = new StringBuilder();

        url.append(vmu.getPermalinkBaseUrl());
        url.append(vmu.getApiStructureBase());
        url.append(vmu.getStructureIdPrefix());
        url.append(structureId);

        if (fromRcsb == 'Y') {
            url.append("/display");
            if (current) {
                url.append("?current");
            }
        } 
        else 
        {
            if (!current) {
                url.append(".");
                url.append(version);
            }
            url.append("/display");
        }
        return url.toString();
    }
    
    
    /**
     * 
     * @return 
     */
    public String getLinkMoeAsPdb(){
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        StringBuilder url = new StringBuilder();

        url.append(vmu.getPermalinkBaseUrl());
        url.append(vmu.getApiStructureBase());
        url.append(structureId);
        url.append(".");
        url.append(version);
        url.append(".pdb?transform_from=moe");

        return url.toString();
    }
            

    
    
            
    
    
    /**
     * 
     * @return 
     */
    public String getJstructDisplayId(){
        ValueMapUtils vm = ValueMapUtils.getInstance();
        StringBuilder id = new StringBuilder();
        id.append(vm.getStructureIdPrefix());
        id.append(structureId);
        if(fromRcsb=='N'){
            id.append(".");
            id.append(version);
        } else {
            id.append(" [");
            id.append(pdbIdentifier);
            id.append("]");
        }
        return id.toString();
    }
    
    
    
    
    public Integer getMaxVersion() {
        return maxVersion;
    }

    public void setMaxVersion(Integer maxVersion) {
        this.maxVersion = maxVersion;
    }

    public Long getMaxStructVerId() {
        return maxStructVerId;
    }

    public void setMaxStructVerId(Long maxStructVerId) {
        this.maxStructVerId = maxStructVerId;
    }

    
    public boolean isMostCurrent() { return mostCurrent.equalsIgnoreCase("Y"); }
    public void setMostCurrent(boolean mostCurrent) { this.mostCurrent = mostCurrent ? "Y" : "N" ; }
    
    


    public Long getStructureId() {
        return structureId;
    }

    public void setStructureId(Long structureId) {
        this.structureId = structureId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }


    public boolean isFromRcsb() { return fromRcsb.equals('Y') || fromRcsb.equals('y'); }
    public void setFromRcsb(boolean fromRcsb) { this.fromRcsb = fromRcsb ? 'Y' : 'N' ; }
    
    

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
    
    
    public boolean isObsolete() { return isObsolete.equals('Y') || isObsolete.equals('y'); }
    public void setObsolete(boolean isObsolete) { this.isObsolete = isObsolete ? 'Y' : 'N' ; }
    

    public Date getObsoleteDate() {
        return obsoleteDate;
    }

    public void setObsoleteDate(Date obsoleteDate) {
        this.obsoleteDate = obsoleteDate;
    }

    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getStructMethod() {
        return structMethod;
    }

    public void setStructMethod(String structMethod) {
        this.structMethod = structMethod;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }


    public boolean isModel() { 
        if(isModel == null){
            return false;
        }
        return isModel.equals('Y') || isModel.equals('y'); 
    }
    public void setModel(boolean isModel) { 
        this.isModel = isModel ? 'Y' : 'N' ; 
    }
    
    
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStructVerInsertDate() {
        return structVerInsertDate;
    }

    public void setStructVerInsertDate(Date structVerInsertDate) {
        this.structVerInsertDate = structVerInsertDate;
    }

    public Long getStructVerInsertUserId() {
        return structVerInsertUserId;
    }

    public void setStructVerInsertUserId(Long structVerInsertUserId) {
        this.structVerInsertUserId = structVerInsertUserId;
    }

    public String getStructVerInsertUserName() {
        return structVerInsertUserName;
    }

    public void setStructVerInsertUserName(String structVerInsertUserName) {
        this.structVerInsertUserName = structVerInsertUserName;
    }

    public Date getStructVerUpdateDate() {
        return structVerUpdateDate;
    }

    public void setStructVerUpdateDate(Date structVerUpdateDate) {
        this.structVerUpdateDate = structVerUpdateDate;
    }

    public Long getStructVerUpdateUserId() {
        return structVerUpdateUserId;
    }

    public void setStructVerUpdateUserId(Long structVerUpdateUserId) {
        this.structVerUpdateUserId = structVerUpdateUserId;
    }

    public String getStructVerUpdateUserName() {
        return structVerUpdateUserName;
    }

    public void setStructVerUpdateUserName(String structVerUpdateUserName) {
        this.structVerUpdateUserName = structVerUpdateUserName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public Long getUncompressedSize() {
        return uncompressedSize;
    }

    public void setUncompressedSize(Long uncompressedSize) {
        this.uncompressedSize = uncompressedSize;
    }

    public Integer getCompressedSize() {
        return compressedSize;
    }

    public void setCompressedSize(Integer compressedSize) {
        this.compressedSize = compressedSize;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String getUploadFileExt() {
        return uploadFileExt;
    }

    public void setUploadFileExt(String uploadFileExt) {
        this.uploadFileExt = uploadFileExt;
    }

    public String getActualFileName() {
        return actualFileName;
    }

    public void setActualFileName(String actualFileName) {
        this.actualFileName = actualFileName;
    }

    public String getActualFileExt() {
        return actualFileExt;
    }

    public void setActualFileExt(String actualFileExt) {
        this.actualFileExt = actualFileExt;
    }

    public Date getFileCreationDate() {
        return fileCreationDate;
    }

    public void setFileCreationDate(Date fileCreationDate) {
        this.fileCreationDate = fileCreationDate;
    }

    public Date getFileModifiedDate() {
        return fileModifiedDate;
    }

    public void setFileModifiedDate(Date fileModifiedDate) {
        this.fileModifiedDate = fileModifiedDate;
    }

    public String getProcessChainStatus() {
        return processChainStatus;
    }

    public void setProcessChainStatus(String processChainStatus) {
        this.processChainStatus = processChainStatus;
    }

    public Date getStructFileInsertDate() {
        return structFileInsertDate;
    }

    public void setStructFileInsertDate(Date structFileInsertDate) {
        this.structFileInsertDate = structFileInsertDate;
    }

    public Long getStructFileInsertUserId() {
        return structFileInsertUserId;
    }

    public void setStructFileInsertUserId(Long structFileInsertUserId) {
        this.structFileInsertUserId = structFileInsertUserId;
    }

    public String getStructFileInsertUserName() {
        return structFileInsertUserName;
    }

    public void setStructFileInsertUserName(String structFileInsertUserName) {
        this.structFileInsertUserName = structFileInsertUserName;
    }

    public Date getStructFileUpdateDate() {
        return structFileUpdateDate;
    }

    public void setStructFileUpdateDate(Date structFileUpdateDate) {
        this.structFileUpdateDate = structFileUpdateDate;
    }

    public Long getStructFileUpdateUserId() {
        return structFileUpdateUserId;
    }

    public void setStructFileUpdateUserId(Long structFileUpdateUserId) {
        this.structFileUpdateUserId = structFileUpdateUserId;
    }

    public String getStructFileUpdateUserName() {
        return structFileUpdateUserName;
    }

    public void setStructFileUpdateUserName(String structFileUpdateUserName) {
        this.structFileUpdateUserName = structFileUpdateUserName;
    }

    public String getPdbIdentifier() {
        return pdbIdentifier;
    }

    public void setPdbIdentifier(String pdbIdentifier) {
        this.pdbIdentifier = pdbIdentifier;
    }

    public String getPdbClassification() {
        return pdbClassification;
    }

    public void setPdbClassification(String pdbClassification) {
        this.pdbClassification = pdbClassification;
    }

    public Date getPdbDepositionDate() {
        return pdbDepositionDate;
    }

    public void setPdbDepositionDate(Date pdbDepositionDate) {
        this.pdbDepositionDate = pdbDepositionDate;
    }

    public String getPdbTitle() {
        return pdbTitle;
    }

    public void setPdbTitle(String pdbTitle) {
        this.pdbTitle = pdbTitle;
    }

    public String getPdbCaveat() {
        return pdbCaveat;
    }

    public void setPdbCaveat(String pdbCaveat) {
        this.pdbCaveat = pdbCaveat;
    }

    public String getPdbExpdta() {
        return pdbExpdta;
    }

    public void setPdbExpdta(String pdbExpdta) {
        this.pdbExpdta = pdbExpdta;
    }

    public Long getPdbNummdl() {
        return pdbNummdl;
    }

    public void setPdbNummdl(Long pdbNummdl) {
        this.pdbNummdl = pdbNummdl;
    }

    public String getPdbMdltyp() {
        return pdbMdltyp;
    }

    public void setPdbMdltyp(String pdbMdltyp) {
        this.pdbMdltyp = pdbMdltyp;
    }

    public String getPdbResolutionLine() {
        return pdbResolutionLine;
    }

    public void setPdbResolutionLine(String pdbResolutionLine) {
        this.pdbResolutionLine = pdbResolutionLine;
    }

    public String getPdbResolutionUnit() {
        return pdbResolutionUnit;
    }

    public void setPdbResolutionUnit(String pdbResolutionUnit) {
        this.pdbResolutionUnit = pdbResolutionUnit;
    }

    public Double getPdbResolutionMeasure() {
        return pdbResolutionMeasure;
    }

    public void setPdbResolutionMeasure(Double pdbResolutionMeasure) {
        this.pdbResolutionMeasure = pdbResolutionMeasure;
    }
    
    
    
    
    public List<Jstruct_StructureRole> getStructureRoles() {return structureRoles;}
    public void setStructureRoles(List<Jstruct_StructureRole> structureRoles) {this.structureRoles = structureRoles; }
    
    
    
    
    public boolean isCanAllRead() { return canAllRead.equals('Y') || canAllRead.equals('y'); }
    public void setCanAllRead(boolean canAllRead) { this.canAllRead = canAllRead ? 'Y' : 'N' ; }

    public boolean isCanAllWrite() { return canAllWrite.equals('Y') || canAllWrite.equals('y'); }
    public void setCanAllWrite(boolean canAllWrite) { this.canAllWrite = canAllWrite ? 'Y' : 'N' ; }

    
    public void setCurrentUserCanRead(boolean currentUserCanRead) { this.currentUserCanRead = currentUserCanRead;}
    public boolean isCurrentUserCanRead() {return currentUserCanRead;}

    public boolean isCurrentUserCanWrite() {return currentUserCanWrite;}
    public void setCurrentUserCanWrite(boolean currentUserCanWrite) {this.currentUserCanWrite = currentUserCanWrite;}

    public boolean isCurrentUserCanGrant() {return currentUserCanGrant;}
    public void setCurrentUserCanGrant(boolean currentUserCanGrant) { this.currentUserCanGrant = currentUserCanGrant;}

    
    
    
    
    
    
    
    
}
