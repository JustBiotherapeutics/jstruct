package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * Entity class for: structure_label
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "structure_label", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_StructureLabel.findByStructureLabelId", 
                                                    query = "SELECT sl FROM Jstruct_StructureLabel sl "
                                                          + " WHERE sl.structureLabelId = :structureLabelId"),
    
    @NamedQuery(name = "Jstruct_StructureLabel.findByUserLabelIdStructureId", 
                                                    query = "SELECT sl FROM Jstruct_StructureLabel sl "
                                                          + " WHERE sl.userLabelId = :userLabelId "
                                                          + "   AND sl.structureId = :structureId "),
                                                    
    @NamedQuery(name = "Jstruct_StructureLabel.deleteByUserAndStructure", 
                                                    query = "DELETE FROM Jstruct_StructureLabel sl "
                                                          + " WHERE sl.structureLabelId IN "
                                                          + "   ( "
                                                          + "     SELECT usl.structureLabelId FROM Jstruct_UserStructureLabel usl "
                                                          + "      WHERE usl.userId = :userId "
                                                          + "        AND usl.structureId = :structureId "
                                                          + "   ) ")
                                                    
        
})
public class Jstruct_StructureLabel implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="chain_chain_id_seq", sequenceName="chain_chain_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="chain_chain_id_seq")
    @Basic(optional = false)
    @Column(name = "structure_label_id")
    private Long structureLabelId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_label_id")
    private Long userLabelId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "structure_id")
    private Long structureId;
    
    ///@JoinColumn(name = "user_label_id", referencedColumnName = "user_label_id")
    ///@ManyToOne(optional = false)
    ///private Jstruct_UserLabel userLabelId;
    
    
    /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;

    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    /* --------- end audit fields --------- */
    

    public Jstruct_StructureLabel() {
        
    }

    
    
    

    public Long getStructureLabelId() {return structureLabelId;}
    public void setStructureLabelId(Long structureLabelId) { this.structureLabelId = structureLabelId;}

    public Long getUserLabelId() { return userLabelId;}
    public void setUserLabelId(Long userLabelId) { this.userLabelId = userLabelId;}
   
    public Long getStructureId() { return structureId;}
    public void setStructureId(Long structureId) { this.structureId = structureId;}
    
    
    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (structureLabelId != null ? structureLabelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_StructureLabel)) {
            return false;
        }
        Jstruct_StructureLabel other = (Jstruct_StructureLabel) object;
        if ((this.structureLabelId == null && other.structureLabelId != null) || (this.structureLabelId != null && !this.structureLabelId.equals(other.structureLabelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_StructureLabel[ structureLabelId=" + structureLabelId + " ]";
    }
    
}
