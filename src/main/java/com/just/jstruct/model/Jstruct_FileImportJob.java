/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Entity class for: chain
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "file_import_job", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_FileImportJob.findByFileImportJobId", 
                                                        query = "SELECT fij FROM Jstruct_FileImportJob fij "
                                                              + " WHERE fij.fileImportJobId = :fileImportJobId"),
    
    @NamedQuery(name = "Jstruct_FileImportJob.findByImportJobId", 
                                                        query = "SELECT fij FROM Jstruct_FileImportJob fij "
                                                              + " WHERE fij.importJobId = :importJobId")
        
})
public class Jstruct_FileImportJob implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="file_import_job_file_import_job_id_seq", sequenceName="file_import_job_file_import_job_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="file_import_job_file_import_job_id_seq")
    @Basic(optional = false)
    @Column(name = "file_import_job_id")
    private Long fileImportJobId;

    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "import_job_id")
    private Long importJobId;
    
    
    
    
    public Jstruct_FileImportJob() {
    }

    
    public Jstruct_FileImportJob(Long structVerId, Long importJobId) {
        this.structVerId = structVerId;
        this.importJobId = importJobId;
    }

    
    
    
    
    public Long getFileImportJobId() {return fileImportJobId;}
    public void setFileImportJobId(Long fileImportJobId) {this.fileImportJobId = fileImportJobId;}
    
    public Long getStructVerId() {return structVerId;}
    public void setStructVerId(Long structVerId) {this.structVerId = structVerId;}
    
    public Long getImportJobId() {return importJobId;}
    public void setImportJobId(Long importJobId) {this.importJobId = importJobId;}
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fileImportJobId != null ? fileImportJobId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_FileImportJob)) {
            return false;
        }
        Jstruct_FileImportJob other = (Jstruct_FileImportJob) object;
        if ((this.fileImportJobId == null && other.fileImportJobId != null) || (this.fileImportJobId != null && !this.fileImportJobId.equals(other.fileImportJobId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_FileImportJob[ fileImportJobId=" + fileImportJobId + " ]";
    }
    
}
