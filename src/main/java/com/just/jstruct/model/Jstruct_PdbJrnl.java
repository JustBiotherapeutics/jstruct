package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing Journal (JRNL) PDB Header information
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#JRNL
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_jrnl", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbJrnl.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbJrnl p "
                                                          + "WHERE p.structVerId = :structVerId "),
    
    @NamedQuery(name = "Jstruct_PdbJrnl.findPrimaryByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbJrnl p "
                                                          + " WHERE p.structVerId = :structVerId "
                                                          + "   AND p.isPrimary LIKE 'Y' "),
    
    @NamedQuery(name = "Jstruct_PdbJrnl.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbJrnl p "
                                                          + "WHERE p.structVerId = :structVerId")
        
})
public class Jstruct_PdbJrnl implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_jrnl_pdb_jrnl_id_seq", sequenceName="pdb_jrnl_pdb_jrnl_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_jrnl_pdb_jrnl_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_jrnl_id")
    private Long pdbJrnlId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Size(max = 2147483647)
    @Column(name = "auth")
    private String auth;
    
    @Size(max = 2147483647)
    @Column(name = "titl")
    private String titl;
    
    @Size(max = 2147483647)
    @Column(name = "edit")
    private String edit;
    
    @Size(max = 2147483647)
    @Column(name = "ref")
    private String ref;
    
    @Size(max = 2147483647)
    @Column(name = "publ")
    private String publ;
    
    @Size(max = 2147483647)
    @Column(name = "refn")
    private String refn;
    
    @Size(max = 2147483647)
    @Column(name = "pmid")
    private String pmid;
    
    @Size(max = 2147483647)
    @Column(name = "doi")
    private String doi;
    
    @Column(name = "is_primary")
    private Character isPrimary;
    
    @Size(max = 100)
    @Column(name = "section")
    private String section;
    
    

    public Jstruct_PdbJrnl() {
    }

    
    public Jstruct_PdbJrnl(Long inStructFileId, String inAuth, String inTitl, boolean isPrimary) {
        this.structVerId = inStructFileId;
        this.auth = inAuth;
        this.titl = inTitl;
        this.isPrimary = isPrimary ? 'Y' : 'N';
    }

    public Jstruct_PdbJrnl(Long structVerId, String auth, String titl, String edit, String ref, String publ, String refn, String pmid, String doi, boolean isPrimary, String section) {
        this.structVerId = structVerId;
        this.auth = auth;
        this.titl = titl;
        this.edit = edit;
        this.ref = ref;
        this.publ = publ;
        this.refn = refn;
        this.pmid = pmid;
        this.doi = doi;
        this.isPrimary = isPrimary ? 'Y' : 'N';
        this.section = section;
    }
    
    
    
    
    
    
    
    public Long getPdbJrnlId() {
        return pdbJrnlId;
    }

    public void setPdbJrnlId(Long pdbJrnlId) {
        this.pdbJrnlId = pdbJrnlId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getTitl() {
        return titl;
    }

    public void setTitl(String titl) {
        this.titl = titl;
    }

    public String getEdit() {
        return edit;
    }

    public void setEdit(String edit) {
        this.edit = edit;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getPubl() {
        return publ;
    }

    public void setPubl(String publ) {
        this.publ = publ;
    }

    public String getRefn() {
        return refn;
    }

    public void setRefn(String refn) {
        this.refn = refn;
    }

    public String getPmid() {
        return pmid;
    }

    public void setPmid(String pmid) {
        this.pmid = pmid;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    
    public boolean isPrimary() { return isPrimary.equals('Y') || isPrimary.equals('y'); }
    public void setPrimary(boolean isPrimary) { this.isPrimary = isPrimary ? 'Y' : 'N' ; }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbJrnlId != null ? pdbJrnlId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbJrnl)) {
            return false;
        }
        Jstruct_PdbJrnl other = (Jstruct_PdbJrnl) object;
        if ((this.pdbJrnlId == null && other.pdbJrnlId != null) || (this.pdbJrnlId != null && !this.pdbJrnlId.equals(other.pdbJrnlId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbJrnl[ pdbJrnlId=" + pdbJrnlId + " ]";
    }
    
}
