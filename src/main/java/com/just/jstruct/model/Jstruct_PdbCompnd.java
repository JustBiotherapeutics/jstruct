package com.just.jstruct.model;

import com.just.jstruct.utilities.JStringUtil;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *   Database table containing compound (COMPND) PDB Header information
 *   http://www.wwpdb.org/documentation/file-format-content/format33/sect2.html#COMPND
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "pdb_compnd", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_PdbCompnd.findByStructVerId", 
                                                    query = "SELECT p FROM Jstruct_PdbCompnd p "
                                                          + " WHERE p.structVerId = :structVerId "
                                                          + "ORDER BY p.molId ASC"),
    
    @NamedQuery(name = "Jstruct_PdbCompnd.deleteByStructVerId", 
                                                    query = "DELETE FROM Jstruct_PdbCompnd p "
                                                          + " WHERE p.structVerId = :structVerId ")
        
})
public class Jstruct_PdbCompnd implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="pdb_compnd_pdb_compnd_id_seq", sequenceName="pdb_compnd_pdb_compnd_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="pdb_compnd_pdb_compnd_id_seq")
    @Basic(optional = false)
    @Column(name = "pdb_compnd_id")
    private Long pdbCompndId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "mol_id")
    private long molId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "value")
    private String value;
    

    public Jstruct_PdbCompnd() {
    }

    public Jstruct_PdbCompnd(Long structVerId, long molId, String name, String value) {
        this.structVerId = structVerId;
        this.molId = molId;
        this.name = name;
        if(value!=null && value.length()>2500){
            this.value = JStringUtil.cleaveString(value, 2500);
        } else {
            this.value = value;
        }
    }


    
    
    

    public Long getPdbCompndId() {
        return pdbCompndId;
    }

    public void setPdbCompndId(Long pdbCompndId) {
        this.pdbCompndId = pdbCompndId;
    }

    
    public Long getStructVerId() {
        return structVerId;
    }

    public void setStructVerId(Long structVerId) {
        this.structVerId = structVerId;
    }
    

    public long getMolId() {
        return molId;
    }

    public void setMolId(long molId) {
        this.molId = molId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        if(value!=null && value.length()>2500){
            this.value = JStringUtil.cleaveString(value, 2500);
        } else {
            this.value = value;
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdbCompndId != null ? pdbCompndId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_PdbCompnd)) {
            return false;
        }
        Jstruct_PdbCompnd other = (Jstruct_PdbCompnd) object;
        if ((this.pdbCompndId == null && other.pdbCompndId != null) || (this.pdbCompndId != null && !this.pdbCompndId.equals(other.pdbCompndId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_PdbCompnd[ pdbCompndId=" + pdbCompndId + " ]";
    }
    
}
