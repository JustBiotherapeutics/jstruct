package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: amino_acid_def
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "amino_acid_def", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_AminoAcid.findAll", 
                                                        query = "SELECT aa FROM Jstruct_AminoAcid aa"),
    
    @NamedQuery(name = "Jstruct_AminoAcid.findByAminoAcidDefId", 
                                                        query = "SELECT aa FROM Jstruct_AminoAcid aa "
                                                              + "WHERE aa.aminoAcidDefId = :aminoAcidDefId"),
    
    @NamedQuery(name = "Jstruct_AminoAcid.findByName", 
                                                        query = "SELECT aa FROM Jstruct_AminoAcid aa "
                                                              + "WHERE aa.name = :name"),
    
    @NamedQuery(name = "Jstruct_AminoAcid.findByAbbrev", 
                                                        query = "SELECT aa FROM Jstruct_AminoAcid aa "
                                                              + "WHERE aa.abbrev = :abbrev"),
    
    @NamedQuery(name = "Jstruct_AminoAcid.findByOneLetter", 
                                                        query = "SELECT aa FROM Jstruct_AminoAcid aa "
                                                              + "WHERE aa.oneLetter = :oneLetter"),
    
    @NamedQuery(name = "Jstruct_AminoAcid.findAllByAminoAcidIds", 
                                                        query = "SELECT aa FROM Jstruct_AminoAcid aa "
                                                              + "WHERE aa.aminoAcidDefId in (:aminoAcidIds)")
})
public class Jstruct_AminoAcid implements Serializable {

    
    private static final long serialVersionUID = 1L;
    @Id
    
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name="amino_acid_def_amino_acid_def_id_seq", sequenceName="amino_acid_def_amino_acid_def_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="amino_acid_def_amino_acid_def_id_seq")
    
    @Basic(optional = false)
    @Column(name = "amino_acid_def_id", updatable=false)
    private Long aminoAcidDefId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "abbrev")
    private String abbrev;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "one_letter")
    private Character oneLetter;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_standard")
    private Character isStandard;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "atoms")
    private String atoms;
    
    /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    @Column(name = "update_user_id", insertable=false, updatable=false)
    private Long updateUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "update_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User updateUser;
    
    /* --------- end audit fields --------- */
    
    
    
    

    public Jstruct_AminoAcid() {
        isStandard = 'N';
    }

    


    
    
    

    public Long getAminoAcidDefId() {
        return aminoAcidDefId;
    }

    public void setAminoAcidDefId(Long aminoAcidDefId) {
        this.aminoAcidDefId = aminoAcidDefId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    public Character getOneLetter() {
        return oneLetter;
    }

    public void setOneLetter(Character oneLetter) {
        this.oneLetter = oneLetter;
    }

    public boolean isStandard() { return isStandard.equals('Y') || isStandard.equals('y'); }
    public void setStandard(boolean isStandard) { this.isStandard = isStandard ? 'Y' : 'N' ; }
    

    public String getAtoms() {
        return atoms;
    }

    public void setAtoms(String atoms) {
        this.atoms = atoms;
    }

    
    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getUpdateDate() {return updateDate;}
    public void setUpdateDate(Date updateDate) { this.updateDate = updateDate;}

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return updateUserId;}
    public void setUpdateUserId(long updateUserId) {this.updateUserId = updateUserId;}
    
    public Jstruct_User getUpdateUser() {return updateUser;}
    public void setUpdateUser(Jstruct_User updateUser) { this.updateUser = updateUser;}

    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aminoAcidDefId != null ? aminoAcidDefId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_AminoAcid)) {
            return false;
        }
        Jstruct_AminoAcid other = (Jstruct_AminoAcid) object;
        if ((this.aminoAcidDefId == null && other.aminoAcidDefId != null) || (this.aminoAcidDefId != null && !this.aminoAcidDefId.equals(other.aminoAcidDefId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_AminoAcid[ aminoAcidDefId=" + aminoAcidDefId + " ]";
    }
    
}
