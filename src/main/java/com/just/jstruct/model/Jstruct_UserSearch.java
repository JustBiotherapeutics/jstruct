package com.just.jstruct.model;

import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * Entity class for: user_search
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "user_search", schema = "public")
@NamedQueries({

    @NamedQuery(name = "Jstruct_UserSearch.findByUserSearchId", 
                                                    query = "SELECT us FROM Jstruct_UserSearch us "
                                                          + " WHERE us.userSearchId = :userSearchId"),
    
    @NamedQuery(name = "Jstruct_UserSearch.findByUserId", 
                                                    query = "SELECT us FROM Jstruct_UserSearch us "
                                                          + " WHERE us.userId = :userId "
                                                          + " ORDER BY us.searchName ASC "),
})
public class Jstruct_UserSearch implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="user_search_user_search_id_seq", sequenceName="user_search_user_search_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_search_user_search_id_seq")
    @Basic(optional = false)
    @Column(name = "user_search_id")
    private Long userSearchId;
    
    
    @Column(name = "user_id", insertable=false, updatable=false)
    private Long userId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User user;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "search_name")
    private String searchName;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "file_status_rcsb")
    private String fileStatusRcsb;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "file_status_manual")
    private String fileStatusManual;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "query_mode")
    private String queryMode;
    
    
    @Transient
    private List<Jstruct_SearchSubquery> searchSubquerys;
    
    @Transient
    private FullQueryInput fullQueryInput;
    
    
    /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    @Column(name = "update_user_id", insertable=false, updatable=false)
    private Long updateUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "update_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User updateUser;
    
    /* --------- end audit fields --------- */

    
    
    public Jstruct_UserSearch() {
    }

    public Jstruct_UserSearch(Jstruct_User user, String searchName){
        this.user = user;
        this.searchName = searchName;
    }

    
   
    public Long getUserSearchId() { return userSearchId;}
    public void setUserSearchId(Long userSearchId) { this.userSearchId = userSearchId;}

    
    public Long getUserId() {return userId;}
    public void setUserId(Long userId) { this.userId = userId;}

    public Jstruct_User getUser() {return user;}
    public void setUser(Jstruct_User user) {this.user = user;}

    
    
    public String getSearchName() {return searchName; }
    public void setSearchName(String searchName) {this.searchName = searchName;}

    
    
    public FileStatus getFileStatusRcsb() { 
        return FileStatus.valueOf(fileStatusRcsb); 
    }
    public void setFileStatusRcsb(FileStatus fileStatusRcsb) {
        this.fileStatusRcsb = fileStatusRcsb.name();
    }

    
    
    public FileStatus getFileStatusManual() { 
        return FileStatus.valueOf(fileStatusManual); 
    }
    public void setFileStatusManual(FileStatus fileStatusManual) {
        this.fileStatusManual = fileStatusManual.name();
    }

    
    
    public QueryMode getQueryMode() { 
        return QueryMode.valueOf(queryMode);
    }
    public void setQueryMode(QueryMode queryMode) {
        this.queryMode = queryMode.name();
    }

    
    public List<Jstruct_SearchSubquery> getSearchSubquerys() { return searchSubquerys;}
    public void setSearchSubquerys(List<Jstruct_SearchSubquery> searchSubquerys) { this.searchSubquerys = searchSubquerys; }
   
    public void addSearchSubquery(Jstruct_SearchSubquery searchSubquery){
        if(this.searchSubquerys == null){
            searchSubquerys = new ArrayList<>();
        }
        searchSubquery.setUserSearchId(this.userSearchId);
        searchSubquerys.add(searchSubquery);
    }

    
    
    public FullQueryInput getFullQueryInput() {
        return fullQueryInput;
    }

    public void setFullQueryInput(FullQueryInput fullQueryInput) {
        this.fullQueryInput = fullQueryInput;
    }
    
    
    
    
    
    
    
    
    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getUpdateDate() {return updateDate;}
    public void setUpdateDate(Date updateDate) { this.updateDate = updateDate;}

    public Long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(Long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public Long getUpdateUserId() { return updateUserId;}
    public void setUpdateUserId(Long updateUserId) {this.updateUserId = updateUserId;}
    
    public Jstruct_User getUpdateUser() {return updateUser;}
    public void setUpdateUser(Jstruct_User updateUser) { this.updateUser = updateUser;}

    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userSearchId != null ? userSearchId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_UserSearch)) {
            return false;
        }
        Jstruct_UserSearch other = (Jstruct_UserSearch) object;
        if ((this.userSearchId == null && other.userSearchId != null) || (this.userSearchId != null && !this.userSearchId.equals(other.userSearchId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_UserSearch[ userSearchId=" + userSearchId + " ]";
    }
    
}
