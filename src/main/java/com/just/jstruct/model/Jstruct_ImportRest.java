package com.just.jstruct.model;

import com.hfg.util.collection.CollectionUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: import_rest
 */
@Entity
@Table(name = "import_rest", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_ImportRest.findByImportRestId", 
                                                        query = "SELECT ir FROM Jstruct_ImportRest ir "
                                                              + "WHERE ir.importRestId = :importRestId"),
    
    @NamedQuery(name = "Jstruct_ImportRest.findByImportJobId", 
                                                        query = "SELECT ir FROM Jstruct_ImportRest ir "
                                                              + "WHERE ir.importJobId = :importJobId"),
    
        
})
public class Jstruct_ImportRest implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    
    @Id
    @SequenceGenerator(name="import_rest_import_rest_id_seq", sequenceName="import_rest_import_rest_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="import_rest_import_rest_id_seq")
    @Basic(optional = false)
    @Column(name = "import_rest_id")
    private Long importRestId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "search_desc")
    private String searchDesc;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "pdb_rest_url")
    private String pdbRestUrl;
    
    @Size(max = 2147483647)
    @Column(name = "pdb_rest_criteria")
    private String pdbRestCriteria;
    
    @Column(name = "search_date_start")
    @Temporal(TemporalType.TIMESTAMP)
    private Date searchDateStart;
    
    @Column(name = "search_date_end")
    @Temporal(TemporalType.TIMESTAMP)
    private Date searchDateEnd;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "response_pdb_count")
    private long responsePdbCount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "import_job_id")
    private Long importJobId;
    
    
    @Transient private List<Jstruct_ImportPdb> importPdbs;
    
    //@JoinColumn(name = "import_job_id", referencedColumnName = "import_job_id")
    //@ManyToOne(optional = false)
    //private Jstruct_ImportJob importJobId;
    
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "importRestId")
    //private List<Jstruct_ImportPdb> jstructImportPdbList;
    
    
    

    public Jstruct_ImportRest() {
    }

   
    
    
    

    public Long getImportRestId() {
        return importRestId;
    }

    public void setImportRestId(Long importRestId) {
        this.importRestId = importRestId;
    }

    public String getSearchDesc() {
        return searchDesc;
    }

    public void setSearchDesc(String searchDesc) {
        this.searchDesc = searchDesc;
    }

    public String getPdbRestUrl() {
        return pdbRestUrl;
    }

    public void setPdbRestUrl(String pdbRestUrl) {
        this.pdbRestUrl = pdbRestUrl;
    }

    public String getPdbRestCriteria() {
        return pdbRestCriteria;
    }

    public void setPdbRestCriteria(String pdbRestCriteria) {
        this.pdbRestCriteria = pdbRestCriteria;
    }

    public Date getSearchDateStart() {
        return searchDateStart;
    }

    public void setSearchDateStart(Date searchDateStart) {
        this.searchDateStart = searchDateStart;
    }

    public Date getSearchDateEnd() {
        return searchDateEnd;
    }

    public void setSearchDateEnd(Date searchDateEnd) {
        this.searchDateEnd = searchDateEnd;
    }

    public long getResponsePdbCount() {
        return responsePdbCount;
    }

    public void setResponsePdbCount(long responsePdbCount) {
        this.responsePdbCount = responsePdbCount;
    }

    public Long getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(Long importJobId) {
        this.importJobId = importJobId;
    }

    
    
    public List<Jstruct_ImportPdb> getImportPdbs() { return importPdbs; }
    public void setImportPdbs(List<Jstruct_ImportPdb> importPdbs) { this.importPdbs = importPdbs;}
    public void addImportPdb(Jstruct_ImportPdb importPdb){
        if(this.importPdbs == null){
            importPdbs = new ArrayList<>(10);
        }
        importPdb.setImportRestId(this.importRestId);
        importPdbs.add(importPdb);
    }
    public void addImportPdb(String pdbIdentifier){
        if(this.importPdbs == null){
            importPdbs = new ArrayList<>(10);
        }
        Jstruct_ImportPdb importPdb = new Jstruct_ImportPdb();
        importPdb.setPdbIdentifier(pdbIdentifier);
        importPdb.setImportRestId(importRestId);
        importPdbs.add(importPdb);
    }
   
    public List<String> getImportPdbIds(){
        if(CollectionUtil.hasValues(importPdbs)){
            List<String> pdbIds = new ArrayList<>(importPdbs.size());
            for(Jstruct_ImportPdb pdb : importPdbs){
                pdbIds.add(pdb.getPdbIdentifier());
            }
            return pdbIds;
        }
        return null;
    }
    
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (importRestId != null ? importRestId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_ImportRest)) {
            return false;
        }
        Jstruct_ImportRest other = (Jstruct_ImportRest) object;
        if ((this.importRestId == null && other.importRestId != null) || (this.importRestId != null && !this.importRestId.equals(other.importRestId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_ImportRest[ importRestId=" + importRestId + " ]";
    }
    
}
