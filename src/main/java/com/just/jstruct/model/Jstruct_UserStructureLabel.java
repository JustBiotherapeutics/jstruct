package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * Entity class for: user_structure_label_view (database view)
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "user_structure_label_view", schema = "public")
@NamedQueries({
    
    @NamedQuery(name = "Jstruct_UserStructureLabel.findByUserIdAndStructureId", 
                                                query = "SELECT usl FROM Jstruct_UserStructureLabel usl "
                                                      + " WHERE usl.userId = :userId "
                                                      + "   AND usl.structureId = :structureId "
                                                      + "ORDER BY usl.ordinal, usl.labelName "),
    
    @NamedQuery(name = "Jstruct_UserStructureLabel.findByUserId", 
                                                query = "SELECT usl FROM Jstruct_UserStructureLabel usl "
                                                      + " WHERE usl.userId = :userId "
                                                      + "ORDER BY usl.ordinal, usl.labelName ")
})
public class Jstruct_UserStructureLabel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @Column(name = "structure_label_id")
    private Long structureLabelId;
    
    @Column(name = "structure_id")
    private Long structureId;
    
    
    
    
    @Column(name = "user_label_id")
    private Long userLabelId;
    
    @Column(name = "user_id")
    private Long userId;
    
    @Size(max = 255)
    @Column(name = "label_name")
    private String labelName;
    
    @Size(max = 2000)
    @Column(name = "description")
    private String description;
    
    @Size(max = 6)
    @Column(name = "background_color")
    private String backgroundColor;
    
    @Size(max = 6)
    @Column(name = "text_color")
    private String textColor;
    
    @Column(name = "ordinal")
    private Integer ordinal;
    
    

    
    public Jstruct_UserStructureLabel() {
    }
    
    
    
    
    

    public Long getStructureLabelId() {
        return structureLabelId;
    }

    public void setStructureLabelId(Long structureLabelId) {
        this.structureLabelId = structureLabelId;
    }

    public Long getStructureId() {
        return structureId;
    }

    public void setStructureId(Long structureId) {
        this.structureId = structureId;
    }

    public Long getUserLabelId() {
        return userLabelId;
    }

    public void setUserLabelId(Long userLabelId) {
        this.userLabelId = userLabelId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }
    
}
