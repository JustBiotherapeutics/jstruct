package com.just.jstruct.model;

import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.AtomGroup;
import com.just.bio.structure.Chain;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: atom_grouping
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "atom_grouping", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_AtomGrouping.findByAtomGroupingId", 
                                                        query = "SELECT ag FROM Jstruct_AtomGrouping ag "
                                                              + " WHERE ag.atomGroupingId = :atomGroupingId "),
    
    @NamedQuery(name = "Jstruct_AtomGrouping.findByChainId", 
                                                        query = "SELECT ag FROM Jstruct_AtomGrouping ag "
                                                              + " WHERE ag.chainId = :chainId "),
    
    @NamedQuery(name = "Jstruct_AtomGrouping.deleteByChainId", 
                                                        query = "DELETE FROM Jstruct_AtomGrouping ag "
                                                              + " WHERE ag.chainId = :chainId ")

})
public class Jstruct_AtomGrouping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="atom_grouping_atom_grouping_id_seq", sequenceName="atom_grouping_atom_grouping_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="atom_grouping_atom_grouping_id_seq")
    @Basic(optional = false)
    @Column(name = "atom_grouping_id")
    private Long atomGroupingId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "type")
    private String type;
    
    @Size(max = 2147483647)
    @Column(name = "seq_with_all_coords")
    private String seqWithAllCoords;
    
    @Size(max = 2147483647)
    @Column(name = "seq_with_any_coords")
    private String seqWithAnyCoords;
    
    @Size(max = 2147483647)
    @Column(name = "seq_with_ca_coords")
    private String seqWithCaCoords;
    
    @Size(max = 2147483647)
    @Column(name = "seq_with_bkbn_coords")
    private String seqWithBkbnCoords;
    
    @Size(max = 2147483647)
    @Column(name = "seq_with_bkbn_cb_coords")
    private String seqWithBkbnCbCoords;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "chain_id")
    private Long chainId;
    
  
    @Transient private List<Jstruct_Residue> residues;
    
    
    

    public Jstruct_AtomGrouping() {
    }

    
    /**
     * create a new Jstruct_AtomGrouping based on a AtomGroupingObject (see CoreStruct) and it's related chainId
     * @param inChain
     * @param chainId 
     */
    public Jstruct_AtomGrouping(Chain inChain, Long chainId){
        
        this.chainId = chainId;
        
        this.seqWithAllCoords = inChain.getSequenceWithAllCoordinates();
        this.seqWithAnyCoords = inChain.getSequenceWithAnyCoordinates();
        this.seqWithCaCoords = inChain.getSequenceWithCaCoordinates();
        this.seqWithBkbnCoords = inChain.getSequenceWithBackboneCoordinates();
        this.seqWithBkbnCbCoords = inChain.getSequenceWithBackboneCbCoordinates();
        
        this.type = inChain.getType().name();
        
        if(CollectionUtil.hasValues(inChain.getAtomGroups())){
            for(AtomGroup residue : inChain.getAtomGroups()){
                addResidue(new Jstruct_Residue(residue, this.atomGroupingId));
            }
        }
        
        
    }
    
    
    
    

    public Long getAtomGroupingId() {
        return atomGroupingId;
    }

    public void setAtomGroupingId(Long atomGroupingId) {
        this.atomGroupingId = atomGroupingId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSeqWithAllCoords() {
        return seqWithAllCoords;
    }

    public void setSeqWithAllCoords(String seqWithAllCoords) {
        this.seqWithAllCoords = seqWithAllCoords;
    }

    public String getSeqWithAnyCoords() {
        return seqWithAnyCoords;
    }

    public void setSeqWithAnyCoords(String seqWithAnyCoords) {
        this.seqWithAnyCoords = seqWithAnyCoords;
    }

    public String getSeqWithCaCoords() {
        return seqWithCaCoords;
    }

    public void setSeqWithCaCoords(String seqWithCaCoords) {
        this.seqWithCaCoords = seqWithCaCoords;
    }

    public String getSeqWithBkbnCoords() {
        return seqWithBkbnCoords;
    }

    public void setSeqWithBkbnCoords(String seqWithBkbnCoords) {
        this.seqWithBkbnCoords = seqWithBkbnCoords;
    }

    public String getSeqWithBkbnCbCoords() {
        return seqWithBkbnCbCoords;
    }

    public void setSeqWithBkbnCbCoords(String seqWithBkbnCbCoords) {
        this.seqWithBkbnCbCoords = seqWithBkbnCbCoords;
    }

    public Long getChainId() {
        return chainId;
    }

    public void setChainId(Long chainId) {
        this.chainId = chainId;
    }

    
    
    public List<Jstruct_Residue> getResidues() { return residues; }
    public void setResidues(List<Jstruct_Residue> residues) { this.residues = residues; }
    public void addResidue(Jstruct_Residue residue){
        if(this.residues == null){
            residues = new ArrayList<>(10);
        }
        this.residues.add(residue);
    }
    
    
   
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (atomGroupingId != null ? atomGroupingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_AtomGrouping)) {
            return false;
        }
        Jstruct_AtomGrouping other = (Jstruct_AtomGrouping) object;
        if ((this.atomGroupingId == null && other.atomGroupingId != null) || (this.atomGroupingId != null && !this.atomGroupingId.equals(other.atomGroupingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_AtomGrouping[ atomGroupingId=" + atomGroupingId + " ]";
    }
    
}
