package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: client
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "client", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Jstruct_Client.findAll",
                                        query = "SELECT c FROM Jstruct_Client c "
                                              + "ORDER BY c.name "),

    @NamedQuery(name = "Jstruct_Client.findByClientId",
                                        query = "SELECT c FROM Jstruct_Client c "
                                              + " WHERE c.clientId = :clientId "),

    @NamedQuery(name = "Jstruct_Client.findByName",
                                        query = "SELECT c FROM Jstruct_Client c "
                                              + " WHERE c.name = :name "),

    @NamedQuery(name = "Jstruct_Client.doesClientNameExist",
                                        query = "SELECT count(c) FROM Jstruct_Client c "
                                              + " WHERE UPPER(c.name) = :name "),

    @NamedQuery(name = "Jstruct_Client.doesClientNameExistIgnoreId",
                                        query = "SELECT count(c) FROM Jstruct_Client c "
                                              + " WHERE UPPER(c.name) LIKE :name "
                                              + "   AND c.clientId != :clientId ")
})


public class Jstruct_Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="client_client_id_seq", sequenceName="client_client_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="client_client_id_seq")
    @Basic(optional = false)
    @Column(name = "client_id")
    private Long clientId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "deprecated")
    private Character deprecated;
    
    
    
    /* ------------ audit fields ------------ */
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Column(name = "insert_user_id", insertable=false, updatable=false)
    private Long insertUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "insert_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Jstruct_User insertUser;
    
    @Column(name = "update_user_id", insertable=false, updatable=false)
    private Long updateUserId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "update_user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Jstruct_User updateUser;
    
    /* --------- end audit fields --------- */
    
    
    

    public Jstruct_Client() {
    }

   
    
    
    
    
    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public boolean isDeprecated() { return deprecated.equals('Y') || deprecated.equals('y'); }
    public void setDeprecated(boolean deprecated) { this.deprecated = deprecated ? 'Y' : 'N' ; }
    
   
    public Date getInsertDate() {return insertDate;}
    public void setInsertDate(Date insertDate) {this.insertDate = insertDate; }

    public Date getUpdateDate() {return updateDate;}
    public void setUpdateDate(Date updateDate) { this.updateDate = updateDate;}

    public long getInsertUserId() {return insertUserId;}
    public void setInsertUserId(long insertUserId) { this.insertUserId = insertUserId;}
    
    public Jstruct_User getInsertUser() {return insertUser;}
    public void setInsertUser(Jstruct_User insertUser) { this.insertUser = insertUser;}

    public long getUpdateUserId() { return updateUserId;}
    public void setUpdateUserId(long updateUserId) {this.updateUserId = updateUserId;}
    
    public Jstruct_User getUpdateUser() {return updateUser;}
    public void setUpdateUser(Jstruct_User updateUser) { this.updateUser = updateUser;}




    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientId != null ? clientId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_Client)) {
            return false;
        }
        Jstruct_Client other = (Jstruct_Client) object;
        if ((this.clientId == null && other.clientId != null) || (this.clientId != null && !this.clientId.equals(other.clientId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_Client[ clientId=" + clientId + " ]";
    }
    
}
