package com.just.jstruct.model;

import com.hfg.util.StringUtil;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity class for: analysis_task
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "analysis_task", schema = "public")
@NamedQueries({
     @NamedQuery(name = "Jstruct_AnalysisTask.findById", 
                                                query = "SELECT a FROM Jstruct_AnalysisTask a "
                                                     + " WHERE a.analysisTaskId = :analysisTaskId"),
    
    @NamedQuery(name = "Jstruct_AnalysisTask.findByStatus", 
                                                query = "SELECT a FROM Jstruct_AnalysisTask a "
                                                      + " WHERE a.status = :status"),
    
    @NamedQuery(name = "Jstruct_AnalysisTask.findByAnalysisRunId", 
                                                query = "SELECT a FROM Jstruct_AnalysisTask a "
                                                      + " WHERE a.analysisRunId = :analysisRunId")
})
public class Jstruct_AnalysisTask implements Serializable 
{

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name="analysis_task_analysis_task_id_seq", sequenceName="analysis_task_analysis_task_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="analysis_task_analysis_task_id_seq")
    @Basic(optional = false)
    @Column(name = "analysis_task_id")
    private Long analysisTaskId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "command")
    private String command;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "status")
    private String status;
    
    @Size(max = 2147483647)
    @Column(name = "error")
    private String error;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    
    //fk to struct_version
    @Basic(optional = false)
    @NotNull
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    //fk to analysis_run
    @Basic(optional = false)
    @NotNull
    @Column(name = "analysis_run_id")
    private Long analysisRunId;
    
    
    
    
    
    
    public Jstruct_AnalysisTask() {
    }

    
    
    
    

    public Long getAnalysisTaskId() {  return analysisTaskId; }
    public void setAnalysisTaskId(Long analysisTaskId) { this.analysisTaskId = analysisTaskId; }

    public String getCommand() { return command; }
    public void setCommand(String command) { this.command = StringUtil.isSet(command) ? command.trim() : null; }

    public String getStatus() { return status; }
    public void setStatus(String status) { this.status = status; }

    public String getError() { return error; }
    public void setError(String error) { this.error = error; }

    public Date getStartDate() { return startDate; }
    public void setStartDate(Date startDate) { this.startDate = startDate; }

    public Date getEndDate() { return endDate; }
    public void setEndDate(Date endDate) { this.endDate = endDate; }
    
    
    public Long getStructVerId() { return structVerId; }
    public void setStructVerId(Long structVerId) { this.structVerId = structVerId; }

    public Long getAnalysisRunId() { return analysisRunId; }
    public void setAnalysisRunId(Long analysisRunId) {  this.analysisRunId = analysisRunId; }
    
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (analysisTaskId != null ? analysisTaskId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_AnalysisTask)) {
            return false;
        }
        Jstruct_AnalysisTask other = (Jstruct_AnalysisTask) object;
        if ((this.analysisTaskId == null && other.analysisTaskId != null) || (this.analysisTaskId != null && !this.analysisTaskId.equals(other.analysisTaskId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.AnalysisTask[ analysisTaskId=" + analysisTaskId + " ]";
    }
    
}
