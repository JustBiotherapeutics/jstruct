package com.just.jstruct.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * Entity class for: structure_user
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "structure_user", schema = "public")
@NamedQueries({

    @NamedQuery(name = "Jstruct_StructureUser.getByStructureUserId", 
                                                            query = "SELECT su FROM Jstruct_StructureUser su "
                                                                  + "WHERE su.structureUserId = :structureUserId"),

    
    @NamedQuery(name = "Jstruct_StructureUser.getFavoritesByUserId", 
                                                            query = "SELECT su FROM Jstruct_StructureUser su "
                                                                  + " WHERE su.userId = :userId "
                                                                  + "  AND su.isFavorite LIKE 'Y' "
                                                                  + "ORDER BY su.insertDate DESC"),

    @NamedQuery(name = "Jstruct_StructureUser.getRecentsByUserId", 
                                                            query = "SELECT su FROM Jstruct_StructureUser su "
                                                                  + " WHERE su.userId = :userId "
                                                                  + "  AND su.isRecent LIKE 'Y' "
                                                                  + "ORDER BY su.insertDate DESC"),

    
    @NamedQuery(name = "Jstruct_StructureUser.getFavoriteByStructureIdAndUserId", 
                                                            query = "SELECT su FROM Jstruct_StructureUser su "
                                                                  + " WHERE su.structureId = :structureId "
                                                                  + "  AND su.userId = :userId "
                                                                  + "  AND su.isFavorite LIKE 'Y' "),

    @NamedQuery(name = "Jstruct_StructureUser.getRecentByStructVersionIdAndUserId", 
                                                            query = "SELECT su FROM Jstruct_StructureUser su "
                                                                  + " WHERE su.structVerId = :structVerId "
                                                                  + "  AND su.userId = :userId "
                                                                  + "  AND su.isRecent LIKE 'Y' "),
        
      


})
public class Jstruct_StructureUser implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "structure_user_id")
    private Long structureUserId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_favorite")
    private Character isFavorite;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_recent")
    private Character isRecent;
    
    
    @Column(name = "recent_count")
    private Integer recentCount;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    
    
    @Column(name = "structure_id")
    private Long structureId;
    
    @Column(name = "struct_ver_id")
    private Long structVerId;
    
    @Column(name = "user_id")
    private Long userId;
    
    
    
    

    public Jstruct_StructureUser() {
    }


    
    

    public Long getStructureUserId() { return structureUserId;}
    public void setStructureUserId(Long structureUserId) {  this.structureUserId = structureUserId;}

    
    
    public boolean isFavorite() { return isFavorite.equals('Y') || isFavorite.equals('y'); }
    public void setFavorite(boolean isFavorite) { this.isFavorite = isFavorite ? 'Y' : 'N' ; }
    
    public boolean isRecent() { return isRecent.equals('Y') || isRecent.equals('y'); }
    public void setRecent(boolean isRecent) { this.isRecent = isRecent ? 'Y' : 'N' ; }
    
    
    
    public Integer getRecentCount() {return recentCount;}
    public void setRecentCount(Integer recentCount) {this.recentCount = recentCount;}

    
    
    public Date getInsertDate() { return insertDate; }
    public void setInsertDate(Date insertDate) { this.insertDate = insertDate; }

    
    
    public Long getStructureId() {return structureId;}
    public void setStructureId(Long structureId) {this.structureId = structureId;}

    public Long getStructVerId() {return structVerId; }
    public void setStructVerId(Long structVerId) {this.structVerId = structVerId;}

    public Long getUserId() { return userId; }
    public void setUserId(Long userId) { this.userId = userId;}

    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (structureUserId != null ? structureUserId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_StructureUser)) {
            return false;
        }
        Jstruct_StructureUser other = (Jstruct_StructureUser) object;
        if ((this.structureUserId == null && other.structureUserId != null) || (this.structureUserId != null && !this.structureUserId.equals(other.structureUserId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_StructureUser[ structureUserId=" + structureUserId + " ]";
    }
    
}
