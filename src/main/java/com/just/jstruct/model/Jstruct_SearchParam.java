package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Entity class for: search_param
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "search_param", schema = "public")
@NamedQueries({

    @NamedQuery(name = "Jstruct_SearchParam.findBySearchParamId", 
                                                    query = "SELECT us FROM Jstruct_SearchParam us "
                                                          + " WHERE us.searchParamId = :searchParamId "),
    
    @NamedQuery(name = "Jstruct_SearchParam.findBySearchSubqueryId", 
                                                    query = "SELECT us FROM Jstruct_SearchParam us "
                                                          + " WHERE us.searchSubqueryId = :searchSubqueryId "),
})
public class Jstruct_SearchParam implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @SequenceGenerator(name="search_param_search_param_id_seq", sequenceName="search_param_search_param_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="search_param_search_param_id_seq")
    @Basic(optional = false)
    @Column(name = "search_param_id")
    private Long searchParamId;

    
    @Basic(optional = false)
    @NotNull
    @Column(name = "search_subquery_id")
    private Long searchSubqueryId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "param_key")
    private String paramKey;
    

    @Column(name = "param_value")
    private String paramValue;
    
    
    
    
    public Jstruct_SearchParam() {
    }
    
    
    
    public Jstruct_SearchParam(Long searchSubqueryId, String paramKey, String paramValue) {
        this.searchSubqueryId = searchSubqueryId;
        this.paramKey = paramKey;
        this.paramValue = paramValue;
    }

    
    
   
    public Long getSearchParamId() { return searchParamId;}
    public void setSearchParamId(Long searchParamId) { this.searchParamId = searchParamId;}

    public Long getSearchSubqueryId() { return searchSubqueryId;}
    public void setSearchSubqueryId(Long searchSubqueryId) {this.searchSubqueryId = searchSubqueryId; }

    public String getParamKey() { return paramKey; }
    public void setParamKey(String paramKey) { this.paramKey = paramKey;}

    public String getParamValue() { return paramValue; }
    public void setParamValue(String paramValue) {this.paramValue = paramValue;}

    
    
    
    
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (searchParamId != null ? searchParamId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_SearchParam)) {
            return false;
        }
        Jstruct_SearchParam other = (Jstruct_SearchParam) object;
        if ((this.searchParamId == null && other.searchParamId != null) || (this.searchParamId != null && !this.searchParamId.equals(other.searchParamId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_SearchParam[ searchParamId=" + searchParamId + " ]";
    }
    
}
