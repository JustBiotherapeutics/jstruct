package com.just.jstruct.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Entity class for: structure_role
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@Entity
@Table(name = "structure_role", schema = "public")
@NamedQueries({

    @NamedQuery(name = "Jstruct_StructureRole.getByStructureRoleId", 
                                                            query = "SELECT sr FROM Jstruct_StructureRole sr "
                                                                  + " WHERE sr.structureRoleId = :structureRoleId"),

    @NamedQuery(name = "Jstruct_StructureRole.getByStructureIdAndUserId", 
                                                            query = "SELECT sr FROM Jstruct_StructureRole sr "
                                                                  + " WHERE sr.structureId = :structureId "
                                                                  + "   AND sr.userId = :userId "),

    @NamedQuery(name = "Jstruct_StructureRole.getByStructureId", 
                                                            query = "SELECT sr FROM Jstruct_StructureRole sr "
                                                                  + " WHERE sr.structureId = :structureId "
                                                                  + " ORDER BY sr.structureRoleId "),
    
    @NamedQuery(name = "Jstruct_StructureRole.findReadStructureIdsForUser", 
                                                            query = "SELECT sr.structureId FROM Jstruct_StructureRole sr "
                                                                  + " WHERE sr.userId = :userId "
                                                                  + "   AND sr.canRead LIKE 'Y' "),
    
    @NamedQuery(name = "Jstruct_StructureRole.findWriteStructureIdsForUser", 
                                                            query = "SELECT sr.structureId FROM Jstruct_StructureRole sr "
                                                                  + " WHERE sr.userId = :userId "
                                                                  + "   AND sr.canWrite LIKE 'Y' "),
    
    @NamedQuery(name = "Jstruct_StructureRole.findGrantStructureIdsForUser", 
                                                            query = "SELECT sr.structureId FROM Jstruct_StructureRole sr "
                                                                  + " WHERE sr.userId = :userId "
                                                                  + "   AND sr.canGrant LIKE 'Y' "),
    
    
    
})
public class Jstruct_StructureRole implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "structure_role_id")
    private Long structureRoleId;
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "structure_id")
    private Long structureId;
    
    
    
    @Column(name = "user_id", insertable=false, updatable=false)
    private Long userId;
    
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Jstruct_User user;
    
    
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_read")
    private Character canRead;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_write")
    private Character canWrite;

    @Basic(optional = false)
    @NotNull
    @Column(name = "can_grant")
    private Character canGrant;
    

    
    
    
    

    public Jstruct_StructureRole() {
    }

    public Jstruct_StructureRole(Long structureId, Jstruct_User user, boolean canRead, boolean canWrite, boolean canGrant) {
        this.structureId = structureId;
        this.user = user;
        this.canRead = canRead ? 'Y' : 'N' ;
        this.canWrite = canWrite ? 'Y' : 'N' ;
        this.canGrant = canGrant ? 'Y' : 'N' ;
    }
    
    
    
    
     


    
    

    public Long getStructureRoleId() { return structureRoleId;}
    public void setStructureRoleId(Long structureUserId) {  this.structureRoleId = structureUserId;}

    
    
    public Long getStructureId() {return structureId;}
    public void setStructureId(Long structureId) {this.structureId = structureId;}

    public Long getUserId() {return userId;}
    public void setUserId(Long userId) { this.userId = userId;}

    public Jstruct_User getUser() {return user;}
    public void setUser(Jstruct_User user) {this.user = user;}
    
    
    
    public boolean isCanRead() { return canRead.equals('Y') || canRead.equals('y'); }
    public void setCanRead(boolean canRead) { this.canRead = canRead ? 'Y' : 'N' ; }
    
    public boolean isCanWrite() { return canWrite.equals('Y') || canWrite.equals('y'); }
    public void setCanWrite(boolean canWrite) { this.canWrite = canWrite ? 'Y' : 'N' ; }
    
    public boolean isCanGrant() { return canGrant.equals('Y') || canGrant.equals('y'); }
    public void setCanGrant(boolean canGrant) { this.canGrant = canGrant ? 'Y' : 'N' ; }
    


    

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (structureRoleId != null ? structureRoleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jstruct_StructureRole)) {
            return false;
        }
        Jstruct_StructureRole other = (Jstruct_StructureRole) object;
        if ((this.structureRoleId == null && other.structureRoleId != null) || (this.structureRoleId != null && !this.structureRoleId.equals(other.structureRoleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.just.jstruct.model.Jstruct_StructureRole[ structureRoleId=" + structureRoleId + " ]";
    }
    
}
