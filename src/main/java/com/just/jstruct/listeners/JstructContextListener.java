package com.just.jstruct.listeners;

import com.just.jstruct.exception.JException;
import com.just.jstruct.jobScheduling.JScheduler;
import com.just.jstruct.jobScheduling.JSoloScheduler;
import com.just.jstruct.utilities.HibernateUtil;
import com.just.jstruct.utilities.JEmailUtil;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;



/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JstructContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        
        // addressing this issue:
        //     http://stackoverflow.com/questions/3116517/hinputtext-which-is-bound-to-integer-property-is-submitting-value-0-instead-of
        //     http://jdevelopment.nl/passing-null-model-jsf/ (this option is pretty involved)
        // using solution referenced here:
        //     http://stackoverflow.com/questions/5225013/how-to-set-dorg-apache-el-parser-coerce-to-zero-false-programmatically
        System.setProperty("org.apache.el.parser.COERCE_TO_ZERO", "false");
        
        JEmailUtil.sendJstructStartupEmail();
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        
        
        System.out.println("--JstructContextListener.contextDestroyed called");

        try {
            
            //upon shutdown, ensure the Scheduler is stopped. 
            //better plan is to manually stop it first from the UI, but if forgotton this outta shut it down progrmatically

            System.out.println("---Stopping JImportScheduler...");
            Scheduler importScheduler = JScheduler.getImportScheduler();
            importScheduler.shutdown();

            System.out.println("---Stopping JImportSoloScheduler...");
            Scheduler soloScheduler = JSoloScheduler.getSoloScheduler();
            soloScheduler.shutdown();
            
            
            //attempt to close hibernate entity manager factory
            System.out.println("---Closing EntityManagerFactory...");
            EntityManagerFactory emf = HibernateUtil.getEntityManagerFactory();
            emf.close();
            

        }catch (SchedulerException | JException ex) {
            ex.printStackTrace();
        }

        System.out.println("--JstructContextListener.contextDestroyed finished");
    }

}