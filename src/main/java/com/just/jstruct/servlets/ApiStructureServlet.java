package com.just.jstruct.servlets;

import com.hfg.exception.ProgrammingException;
import com.hfg.util.StringUtil;
import com.just.bio.structure.Structure;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.dao.service.StructFileService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_StructFile;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * API servlet handling file download requests and display of file contents
 * 
 * 
 * 
 * FILE DOWNLOAD url looks like one of these:
 * 
 *      manually uploaded
 *         /api/structure/JSTRUCT-12345.2/file             - get file based on structure id (exact version)
 *         /api/structure/JSTRUCT-12345/file               - get file based on structure id (current version)
 *         /api/structure/JSTRUCT-12345.2/file?current     - get file based on structure id (current version)
 *         /api/structure/JSTRUCT-12345/file?current       - get file based on structure id (current version)
 *             note: JSTRUCT-12345 may be replaced with 12345
 * 
 *      RCSB only (pdb/cif)
 *         /api/structure/A12B/file               - get PDB file based on pdb identifier (current version)
 *         /api/structure/A12B/file?current       - get PDB file based on pdb identifier (traverse descendants to get current version)
 * 
 * 
 * STRUCTURE CONTENT DISPLAY looks like one of these:
 *      same as above, but replace 'file' with 'display'
 * 
 * 
 * FILE for use with NGL viewer
 *         PDB files:  /api/structure/12345.0.pdb 
 *         CIF files:  /api/structure/12345.0.cif     
 *         MOE files:  /api/structure/12345.1.pdb?transform_from=moe   
 * 
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ApiStructureServlet extends HttpServlet {
    
    private final static String API_STRUCTURE_BASE = "api/structure/";
    
    private final static Pattern NGL_ID = Pattern.compile(API_STRUCTURE_BASE + "([\\S]+)\\.([\\S]+)\\.(pdb|cif|moe)");  //structVerId.fileExt
    
    private final static Pattern JSTRUCT_ID = Pattern.compile(API_STRUCTURE_BASE + "([\\S]+)/");
    private final static Pattern JSTRUCT_ACTION = Pattern.compile(API_STRUCTURE_BASE + "[\\S]+/([\\S]+)");
    
    private FullStructureService fullStructureService;
    private final StructFileService structFileService = new StructFileService();
    private Jstruct_User currentUser;
    
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
          currentUser = SessionBean.getUser(request);
          fullStructureService = new FullStructureService(currentUser);
        } catch (JDAOException ex) {
            throw new ServletException("todo fix error msg! [" + request.getRequestURL().toString() + "]", ex);
        }
        
        
        String requestURI = request.getRequestURI();
        System.out.println(" - ApiStructureServlet called with URI: " + requestURI + "  ---  by user: " + currentUser.getName());
        
        
        Matcher nglIdMatcher = NGL_ID.matcher(requestURI);
        
        if(nglIdMatcher.find()){
            
            //if we found something matching an ID for the NGL viewer, call function that deals with that.
            String structureIdParam = nglIdMatcher.group(1) + "." + nglIdMatcher.group(2);
            //String requestedExtension = nglIdMatcher.group(3);
            String originalExt = request.getParameter("transform_from");
            
            
            if(originalExt!=null && originalExt.equalsIgnoreCase("moe")){
                writeTransformedStructureForNglViewer(request, response, structureIdParam);
            } else {
                writeStructureForNglViewer(request, response, structureIdParam);
            }
            
        } 
        else 
        {
            Matcher actionMatcher = JSTRUCT_ACTION.matcher(requestURI);
            String structureAction = actionMatcher.find() ? actionMatcher.group(1) : "";
        
            boolean downloadAsAttachment = false;
            switch (structureAction) {   //todo add xml or content display response...
                case "file":
                    downloadAsAttachment = true;
                    break;
                case "display":
                    downloadAsAttachment = false;
                    break;
                default:
                    throw new ProgrammingException(StringUtil.singleQuote(requestURI) + " is not a recognized URI!");
            }
        
            Matcher jstructIdMatcher = JSTRUCT_ID.matcher(requestURI);
            String structureIdParam = jstructIdMatcher.find() ? jstructIdMatcher.group(1) : "";

            
            structureIdParam = structureIdParam.toUpperCase();

            boolean getCurrent = request.getParameterMap().containsKey("current");
            boolean getCompressed = request.getParameterMap().containsKey("compressed");

            OutputStream stream = null;

            try {

                Jstruct_FullStructure fullStruture;

                //if the structureIdParam(eter) is only 4 long, it must be a PDB Identifier
                if(structureIdParam.length() == 4){
                    fullStruture = getFullStructureByPdbIdentifier(structureIdParam, getCurrent);
                } else {
                    fullStruture = getFullStructureByStructureIdParam(structureIdParam, getCurrent);
                }
                
               
                //create the response
                stream = response.getOutputStream();

                
            //    if (currentUser == null) {
            //        System.out.println(" - currentUser is null");
            //    } else {
            //       System.out.println(" - currentUser: " + currentUser.getName());
            //    }
            //
            //    HfgCookie cookie = AuthenticationFilter.getBrowserSessionCookie(request);
            //    if (cookie == null) {
            //        System.out.println(" - cookie is null");
            //    } else {
            //        System.out.println(" - cookie: " + cookie.getName() + "  -  value: " + cookie.getDecodedValue());
            //    }
                
               
                
                //and get the file data if user has read access
                if(fullStruture.isCurrentUserCanRead()){
                    
                    String disposition = "text/plain";
                    if(downloadAsAttachment){
                        
                        Jstruct_StructFile structFile = structFileService.getByStructVerId(fullStruture.getStructVerId()); 
                        
                        if(getCompressed){
                            disposition = "attachment; filename=\"" + structFile.getActualFileName() + ".gz\"";
                        } else {
                            disposition = "attachment; filename=\"" + structFile.getActualFileName() + "\"";
                        }
                    }
                    response.setHeader("Content-disposition", disposition);
                
                    if(getCompressed){
                        structFileService.getCompressedData(fullStruture.getStructVerId(), stream);
                    } else {
                        structFileService.getData(fullStruture.getStructVerId(), stream);
                    }
                    
                } 
                else 
                {
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); 
                    stream.write((currentUser.getName() + " does not have access to structure file").getBytes()); 
                }
                

            } catch (JDAOException | JException  ex) {
                throw new ServletException("Error downloading Structure file! [structureIdParam=" + structureIdParam + "]", ex);
            } finally {
                if(stream!=null){
                    stream.close();
                }
            }

        }

    }
    
    
    
    private void writeStructureForNglViewer(HttpServletRequest request, HttpServletResponse response, String structureIdParam) throws ServletException, IOException {

        //Matcher nglIdMatcher = NGL_ID.matcher(requestURI);

        //String structureIdParam = nglIdMatcher.group(1) + "." + nglIdMatcher.group(2);
        //String requestedExtension = nglIdMatcher.group(3);  (pdb | cif | moe)

        OutputStream stream = null; 
        
        try {

            //get the structFile object
            Jstruct_FullStructure fullStruture = getFullStructureByStructureIdParam(structureIdParam, false);
            Jstruct_StructFile structFile = structFileService.getByStructVerId(fullStruture.getStructVerId());

            //create the response
            String disposition = "attachment; filename=\"" + structFile.getActualFileName() + "\"";
            response.setHeader("Content-disposition", disposition);

            //and send the file data
            stream = response.getOutputStream();
            if(fullStruture.isCurrentUserCanRead()){
                structFileService.getData(structFile.getStructVerId(), stream);
            }
           
        } catch (JDAOException | JException | IOException ex) {
            throw new ServletException("Error downloading Structure file! [structureIdParam=" + structureIdParam + "]", ex);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }

    }
    
    
    
    
    private void writeTransformedStructureForNglViewer(HttpServletRequest request, HttpServletResponse response, String structureIdParam) throws ServletException, IOException {

        //Matcher nglIdMatcher = NGL_ID.matcher(requestURI);

        //String structureIdParam = nglIdMatcher.group(1) + "." + nglIdMatcher.group(2);
        //String requestedExtension = nglIdMatcher.group(3);  (pdb | cif | moe)

        BufferedWriter writer = null;
        
        try {
            
            //get the file data
            Jstruct_FullStructure fullStructure = getFullStructureByStructureIdParam(structureIdParam, false);
            Structure structure = structFileService.getAsStructure(fullStructure.getStructVerId());
            
            //create the response
            String newFileName = JStringUtil.onlyTextBeforeLastOccurance(fullStructure.getActualFileName(), ".");
            String disposition = "attachment; filename=\"" + newFileName + ".pdb\"";
            response.setHeader("Content-disposition", disposition);

            //and write it to the writer
            if(fullStructure.isCurrentUserCanRead())
            {
                MoeToPdbTransofmer moeToPdbTransofmer = new MoeToPdbTransofmer(structure, fullStructure);
                writer = new BufferedWriter(response.getWriter()); 
                moeToPdbTransofmer.createCoordinatesOnlyPdb(writer);
            }


        } 
        catch (JDAOException | JException | IOException ex) 
        {
            throw new ServletException("Error downloading Structure file! [structureIdParam=" + structureIdParam + "]", ex);
        } 
        finally 
        {
            if(writer != null) {
                writer.close();
            }
        }

    }
    
    
    
    
    
    
    /**
     * 
     * @param pdbIdentifier
     * @param getCurrent
     * @return
     * @throws JDAOException 
     */
    private Jstruct_FullStructure getFullStructureByPdbIdentifier(String pdbIdentifier, boolean getCurrent) throws JDAOException {
        Jstruct_FullStructure fullStructure = fullStructureService.getRcsbImportedRowByPbdIdentifier(pdbIdentifier, getCurrent);
        return fullStructure;
    }


    /**
     * 
     * @param structureIdParam
     * @param getCurrent
     * @return
     * @throws JDAOException 
     */
    private Jstruct_FullStructure getFullStructureByStructureIdParam(String structureIdParam, boolean getCurrent) throws JDAOException, JException {
        
        //first parse out the structureIdParam.. could be in formats: JSTRUCT-12345, JSTRUCT-12345.3, 12345, 12345.3
        Long structureId = null;
        Integer version = null;

        String structureIdPrefix = ValueMapUtils.getInstance().getStructureIdPrefix();
        if(structureIdParam.startsWith(structureIdPrefix)){
            //strip the JSTRUCT- prefix
            structureIdParam = structureIdParam.substring(structureIdPrefix.length());
        }

        if(structureIdParam.contains(".")){
            //get version if it's there
            String[] splitId = structureIdParam.split("\\.");
            structureIdParam = splitId[0];
            String versionParam = splitId[1];
            if(JStringUtil.isPositiveInteger(versionParam)){
                version = JStringUtil.stringToInteger(versionParam);
            } else {
                throw new JException("Error - unable to retrieve Structure! Version ("+versionParam+") in invalid.");
            }
        }

        if(!JStringUtil.isPositiveInteger(structureIdParam)){
            throw new JException("Error - unable to retrieve Structure! Structure Id ("+structureIdParam+") in invalid.");
        } else {
            structureId = JStringUtil.stringToLong(structureIdParam);
        }

        Jstruct_FullStructure fullStructure = fullStructureService.getByStructureIdAndVersion(structureId, version, getCurrent);
    
        return fullStructure;

    }
    
    
}
