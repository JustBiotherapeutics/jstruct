package com.just.jstruct.servlets;

import com.hfg.bio.seq.BioSequence;
import com.hfg.bio.seq.BioSequenceImpl;
import com.hfg.bio.seq.BioSequenceType;
import com.hfg.bio.seq.NucleicAcid;
import com.hfg.bio.seq.NucleicAcidFactory;
import com.hfg.bio.seq.Protein;
import com.hfg.bio.seq.ProteinFactory;
import com.hfg.bio.seq.format.FASTA;
import com.hfg.exception.ProgrammingException;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.enums.ChainType;
import com.just.jstruct.dao.service.SequencesAtomService;
import com.just.jstruct.dao.service.SequencesSeqresService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_SearchParam;
import com.just.jstruct.model.Jstruct_SearchSubquery;
import com.just.jstruct.model.Jstruct_SequencesView;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.PasSearch;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.JDateUtil;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

/**
 *
 * API servlet handling requests for sequence data 
 * 
 * such as FASTA files that are dynamically created from sequences/chains SEQRES and/or atomic coords.
 * 
 * example URL:
 * 
 *      manually uploaded
 *         /api/sequence/fastafile?
 * 
 *              sequence_from=          SEQRES | ATOM 
 *              chain_types=            PROTEIN | NUCLEIC_ACID | ... (one or more of)
 *              output_file_name=       anyname.fasta 
 *              file_status_rcsb=       FILE_STATUS_ACTIVE | FILE_STATUS_OBSOLETE | FILE_STATUS_ALL | FILE_STATUS_NONE 
 *              file_status_manual=     FILE_STATUS_ACTIVE | FILE_STATUS_OBSOLETE | FILE_STATUS_ALL | FILE_STATUS_NONE 
 *              query_mode=             AND | OR 
 * 
 *              subquery=               TITLE;foo1:bar1;foo2:bar2,bar22;foo3:bar3 
 *              subquery=               OWNER;foo1:bar1
 *                                      FULL_TEXT | IDENTIFIER | SEQUENCE | LABEL | EVERYTHING | ANY_HEADER | TITLE | AUTHOR | OWNER | MODEL | DEPOSIT_DATES | PDB_REMARK | JRNL | COMPND | SOURCE | etc
 * 
 *       http://localhost:8080/jstruct/api/sequence/fastafile?sequence_from=SEQRES&chain_type=PROTEIN&output_file_name=anyname.fasta&file_status_rcsb=FILE_STATUS_NONE&file_status_manual=FILE_STATUS_ALL&query_mode=AND
 *              
 *       http://localhost:8080/jstruct/api/sequence/testfakefile?size_mb=100&start_delay_seconds=120&end_delay_seconds=120
 *       http://johndeere.justbiotherapeutics.com:5501/jstruct/api/sequence/testfakefile?size_mb=100&start_delay_seconds=120&end_delay_seconds=120
 *       http://jstruct.evotec.com/jstruct/api/sequence/testfakefile?size_mb=100&start_delay_seconds=120&end_delay_seconds=120
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ApiSequenceServlet extends HttpServlet {
    
    public final static String API_SEQUENCE_BASE = "api/sequence/";
    public final static String FASTA_FILE_ACTION = "fastafile";
    
    public Jstruct_User currentUser;
    
    private Map<String, HashSet<String>> chainIdMap; //a map of all structureIds.version and the existing chainIds. used to ensure unique chainIds are created for each sequence.

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try {
            
            currentUser = SessionBean.getUser(request);
            
            //reset chainIdMap
            chainIdMap = new HashMap<>();
            
            //determine the action (currently only fastafile is allowed)
            String requestURI = request.getRequestURI();
            String sequenceAction = requestURI.substring(requestURI.lastIndexOf('/') + 1);

            if(FASTA_FILE_ACTION.equalsIgnoreCase(sequenceAction))
            {
                requestFasta(request, response);
            }
            else if ("testfakefile".equalsIgnoreCase(sequenceAction))
            {
                testFakeFile(request, response);
            }
            //else if ("anotheractionhere".equalsIgnoreCase(sequenceAction))
            //{
            //    
            //}
            else
            {
                throw new ProgrammingException(StringUtil.singleQuote(requestURI) + " is not a recognized URI!");
            }
            
          
        } catch (ProgrammingException ex) {
            throw new ServletException("Error creating FASTA file!", ex);
        } 

    }
    
    private void testFakeFile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        OutputStream stream = null;
        String sizeMbParam = "";
        
        try {
            System.out.println("----------------------------------------------------------------------------------------");
            System.out.println("      testFakefile start: " + JDateUtil.defaultDateAndTimeFormat(new Date(), true));
            System.out.println("testFakefile queryString: " + request.getQueryString());
            
            
            Map<String, List<String>> queryParams = FacesUtil.getQueryParams(request.getQueryString());
            sizeMbParam = queryParams.get("size_mb").get(0);
            double fileSizeMb = JStringUtil.stringToDouble(sizeMbParam);

            
            
            String disposition = "attachment; filename=\"fake_file_of_size_" + sizeMbParam + "_mb\"";   //String disposition = "text/plain"; //use if not wanting as attachment
            response.setHeader("Content-disposition", disposition);
            stream = new BufferedOutputStream(response.getOutputStream(), (32 * 1024 * 1024));
            
            
            //start delay
            if(queryParams.containsKey("start_delay_seconds")){
                System.out.println(" * start delay, beginning...");
                TimeUnit.SECONDS.sleep(JStringUtil.stringToInteger(queryParams.get("start_delay_seconds").get(0)));
                System.out.println("   - start delay, finished.");
            }
            
            //write file to stream
            System.out.println(" * writing to stream, beginning...");
            String str = "1 2 3 4 5 6 7 8 9 0\n";
            double fileSize = (fileSizeMb * ((1024 * 1024) / str.length()));
            System.out.println("   - ...");
            for(long i=0 ; i<fileSize ; i++){
                stream.write(str.getBytes(Charset.forName("UTF-8")));
            }
            System.out.println("   - writing to stream, finished.");
              
            //end delay
            if(queryParams.containsKey("end_delay_seconds")){
                System.out.println(" * end delay, beginning...");
                TimeUnit.SECONDS.sleep(JStringUtil.stringToInteger(queryParams.get("end_delay_seconds").get(0)));
                System.out.println("   - end delay, finished.");
            }
            
        
            // http://localhost:8080/jstruct/api/sequence/testfakefile?size_mb=100&start_delay_seconds=120&end_delay_seconds=120
            // http://localhost:8080/jstruct/api/sequence/testfakefile?size_mb=10&start_delay_seconds=10&end_delay_seconds=10
            
            // http://jstruct.evotec.com/jstruct/api/sequence/testfakefile?size_mb=100&start_delay_seconds=120&end_delay_seconds=120
            // http://jstruct.evotec.com/jstruct/api/sequence/testfakefile?size_mb=10&start_delay_seconds=10&end_delay_seconds=10
            
        } catch (Throwable ex) {
            ex.printStackTrace();
            throw new ServletException("Error creating 'testfakefile' file!", ex);
        } finally {
            
            System.out.println(" * closing stream...");
            IOUtils.closeQuietly(stream);
            System.out.println("   - stream closed.");
        }
        
        
    }
    
    
   
    
    
    
    
    
    private void requestFasta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
         try {
            
            //get and validate all the URL parameters
            Map<String, List<String>> queryParams = FacesUtil.getQueryParams(request.getQueryString());
            List<String> missingKeys = apiSequenceUrlIsMissingTheseKeys(queryParams);
            if(CollectionUtil.hasValues(missingKeys)){
                throw new ServletException("URL is missing required parameters: " + StringUtil.join(missingKeys, ", "));
            }
            
            //create the fullQueryInput object based on the url params, then search (useing the PasSearch object) to retrieva all matching structurs
            FullQueryInput fullQueryInput = createFullQueryInputBasedOnParams(queryParams);
            PasSearch pasSearch = new PasSearch(fullQueryInput);
          
            List<Jstruct_FullStructure> fullStructures = pasSearch.execute(currentUser);
            
            List<Long> structVerIds = new ArrayList<>(fullStructures.size());
            
            //filter out any user doesn't have read access to while gettin the structVerIds
            for(Jstruct_FullStructure fullStructure : fullStructures){
                if(fullStructure.isCurrentUserCanRead()){
                    structVerIds.add(fullStructure.getStructVerId());
                }
            }

            //create download response
            respondFastaUsingSeqres(structVerIds, queryParams, response);
            
        } catch (ProgrammingException | JDAOException ex) {
            throw new ServletException("Error creating FASTA file!", ex);
        } 
         
    }
    
    
    
    
    /**
     * 
     * @param structVerIds
     * @param queryParams
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void respondFastaUsingSeqres(List<Long> structVerIds, Map<String, List<String>> queryParams, HttpServletResponse response) throws ServletException, IOException {
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        OutputStream stream = null;
        
        List<String> chainTypesParam = JStringUtil.stringToList(queryParams.get("chain_types").get(0), ",");
        String sequenceFromParam = queryParams.get("sequence_from").get(0);
        String outputFileNameParam = queryParams.get("output_file_name").get(0);
        
        try {
            
            //determine what chaintypes were requested (typically protein or nucleic acid, but sometimes can be a combination
            List<ChainType> chainTypes = new ArrayList<>(1);
            for(String s : chainTypesParam){
                chainTypes.add(ChainType.valueOf(s));
            } 
        
            
            
            //get the list of sequences based on either SEQRES or ATOM sequenceFromParam
            List<? extends Jstruct_SequencesView> seqs = null;
            switch (sequenceFromParam.toUpperCase()) {
                case "SEQRES": {
                    SequencesSeqresService sequencesSeqresService = new SequencesSeqresService(); 
                    seqs = sequencesSeqresService.getByStructVerIds(structVerIds);
                    break;
                }
                case "ATOM": {
                    SequencesAtomService sequencesAtomService = new SequencesAtomService();
                    seqs = sequencesAtomService.getByStructVerIds(structVerIds);
                    break;
                }
            }
            
            
            
            //prep the stream/response
            stream = new BufferedOutputStream(response.getOutputStream(), (32 * 1024 * 1024));
            String disposition = "attachment; filename=\"" + outputFileNameParam + "\"";   //String disposition = "text/plain"; //use if not wanting as attachment
            response.setHeader("Content-disposition", disposition);
            
            
            
            //iterate through list, writing creating bioSequences as we go 
            //we'll pass over groups of sequences (by structVerId) which will collapse any duplicated sequences
            
            List<BioSequence> bioSequences = new ArrayList<>();
            Long prevStructVerId = getFirstStructVerId(chainTypes, seqs);
            
            for(Jstruct_SequencesView seq : seqs){
               
                if(chainTypes.contains(seq.getChainType())){
                    
                    BioSequenceImpl bioSequence;
                    if(seq.getChainType().equals(ChainType.NUCLEIC_ACID)){
                        bioSequence = new NucleicAcid();
                    } else {
                        bioSequence = new Protein();
                    }
                    
                    bioSequence.setID(createId(vmu, seq.getStructureId(), seq.getVersion(), seq.getChainName()));
                    bioSequence.setDescription(createDesc(seq.getChainName(), seq.isFromRcsb(), seq.getPdbIdentifier(), seq.getTitle()));
                    bioSequence.setSequence(seq.getPrimaryFastaSequence());
                    
                    //if this is a new structVerID...
                    if(!seq.getStructVerId().equals(prevStructVerId)){
                        //..write out the existing list, then clear it
                        writeBioSequencesToStream(bioSequences, stream);
                        bioSequences = new ArrayList<>(); 
                        prevStructVerId = seq.getStructVerId();
                    }
                    bioSequences.add(bioSequence); 
                }
            }
            //write final list
            writeBioSequencesToStream(bioSequences, stream);
            
        } catch (Exception ex) {
            throw new ServletException("Error creating FASTA file!", ex);
        } finally {
            IOUtils.closeQuietly(stream);
        }
        
        
    }
    
    
    
    
    /*
    private void respondFastaUsingSeqres_OLD(List<Long> structVerIds, Map<String, List<String>> queryParams, HttpServletResponse response) throws ServletException, IOException {
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        OutputStream stream = null;
        
        List<String> chainTypesParam = JStringUtil.stringToList(queryParams.get("chain_types").get(0), ",");
        String sequenceFromParam = queryParams.get("sequence_from").get(0);
        String outputFileNameParam = queryParams.get("output_file_name").get(0);
        
        try {
            
            //determine what chaintypes were requested (typically protein or nucleic acid, but sometimes can be a combination
            List<ChainType> chainTypes = new ArrayList<>(1);
            for(String s : chainTypesParam){
                chainTypes.add(ChainType.valueOf(s));
            } 
        
        
            
            //get the list of sequences based on either SEQRES or ATOM sequenceFromParam
            List<? extends Jstruct_SequencesView> seqs = null;
            switch (sequenceFromParam.toUpperCase()) {
                case "SEQRES": {
                    SequencesSeqresService sequencesSeqresService = new SequencesSeqresService(); 
                    seqs = sequencesSeqresService.getByStructVerIds(structVerIds);
                    break;
                }
                case "ATOM": {
                    SequencesAtomService sequencesAtomService = new SequencesAtomService();
                    seqs = sequencesAtomService.getByStructVerIds(structVerIds);
                    break;
                }
            }
            
            
            
            //prep the stream/response
            stream = response.getOutputStream();
            String disposition = "attachment; filename=\"" + outputFileNameParam + "\"";   //String disposition = "text/plain"; //use if not wanting as attachment
            response.setHeader("Content-disposition", disposition);
            
            
            
            //iterate through list, writing creating bioSequences as we go 
            //we'll pass over groups of sequences (by structVerId) which will collapse any duplicated sequences
            
            List<BioSequence> bioSequences = new ArrayList<>();
            Long prevStructVerId = getFirstStructVerId(chainTypes, seqs);
            
            for(Jstruct_SequencesView seq : seqs){
               
                if(chainTypes.contains(seq.getChainType())){
                    
                    BioSequenceImpl bioSequence;
                    if(seq.getChainType().equals(ChainType.NUCLEIC_ACID)){
                        bioSequence = new NucleicAcid();
                    } else {
                        bioSequence = new Protein();
                    }
                    
                    bioSequence.setID(createId(vmu, seq.getStructureId(), seq.getVersion(), seq.getChainName()));
                    bioSequence.setDescription(createDesc(seq.getChainName(), seq.isFromRcsb(), seq.getPdbIdentifier(), seq.getTitle()));
                    bioSequence.setSequence(seq.getPrimaryFastaSequence());
                    
                    //if this is a new structVerID...
                    if(!seq.getStructVerId().equals(prevStructVerId)){
                        //..write out the existing list, then clear it
                        writeBioSequencesToStream(bioSequences, stream);
                        bioSequences = new ArrayList<>(); 
                        prevStructVerId = seq.getStructVerId();
                    }
                    bioSequences.add(bioSequence); 
                }
            }
            //write final list
            writeBioSequencesToStream(bioSequences, stream);
            
            
        } catch (JDAOException ex) {
            throw new ServletException("Error creating FASTA file!", ex);
        } finally {
            if(stream!=null){
                stream.close();
            }
        }
        
    } */

    
    
    /**
     * get the first structVerId in a list that matches one of the chain types we're lookin for
     * @param chainTypes
     * @param seqs
     * @return 
     */
    private Long getFirstStructVerId(List<ChainType> chainTypes, List<? extends Jstruct_SequencesView> seqs){
        for(Jstruct_SequencesView seq : seqs){
            if(chainTypes.contains(seq.getChainType())){
                return seq.getStructVerId();
            }
        }
        return null;
    }
    
    
    /**
     * create the chain id - must be unique across all chains in this fasta file
     * examples:
     *      JSTRUCT-12345.A
     *      JSTRUCT-12345.2.A
     * @param vmu
     * @param structureId
     * @param version
     * @param chainId
     * @return 
     */
    private String createId(ValueMapUtils vmu, Long structureId, Integer version, String chainId){
        
       String jstructId = vmu.getStructureIdPrefix() + structureId + (version < 1 ? "" : "." + version);
       String upperChainId = chainId.toUpperCase();
       
       // chain id must be unique and is not case sensitive. unfortunatly some files has chains with same identifier (aka: 'M', 'm')
       // we'll keep a map of all used strutures, and a set of all their chainids (upper). if we find a duplicate we follow ncbi 
       // example of just doubling the chain id (aka: 'm' becomes 'MM')
       if(chainIdMap.containsKey(jstructId)){
           HashSet<String> chainIds = chainIdMap.get(jstructId);
           
           while(chainIds.contains(upperChainId)){
               upperChainId = upperChainId + chainId.toUpperCase();
           }
           chainIds.add(upperChainId);
           
       } else {
           HashSet firstChainId = new HashSet<>();
           firstChainId.add(upperChainId);
           chainIdMap.put(jstructId, firstChainId);
       }
       
       return jstructId + ":" + upperChainId;
       
    }
    
    /**
     * create the description of the chain; the description follows the chainId in the header line
     * examples:
     *      pdb|3TFC|B CRYSTAL STRUCTURE OF NEUTR-
     *      CHAIN:B; CRYSTAL STRUCTURE OF NEUTR-
     * @param chainName
     * @param isFromRcsb
     * @param pdbIdentifier
     * @param title
     * @return 
     */
    private String createDesc(String chainName, boolean isFromRcsb, String pdbIdentifier, String title){
        StringBuilder desc = new StringBuilder();
        if(isFromRcsb){
            desc.append("pdb|").append(pdbIdentifier).append("|").append(chainName);
        } else {
            desc.append("CHAIN:").append(chainName).append(";");
        }
        desc.append(" ").append(title);
        
        return desc.toString();
    }
    
    
    
    
    /**
     * for a group of bioSequences (all related to the same structVerId), first collapse
     * all duplicated sequences, then write it to the stream
     * @param bioSequences
     * @param stream 
     */
    private void writeBioSequencesToStream(List<BioSequence> bioSequences, OutputStream stream) throws UnsupportedEncodingException {
        
        //first split the bioSequences into Protein and Nucleic Acid
        
        List<BioSequence> proteinBioSequence = new ArrayList<>();
        List<BioSequence> nucleicBioSequence = new ArrayList<>();
        
        for(BioSequence bioSequence : bioSequences){
            if(bioSequence.getType().equals(BioSequenceType.NUCLEIC_ACID)){
                nucleicBioSequence.add(bioSequence);
            } else {
                proteinBioSequence.add(bioSequence);
            }
        }
        
        if(CollectionUtil.hasValues(nucleicBioSequence)){
            writeBioSequencesToStream(nucleicBioSequence, new FASTA<>(new NucleicAcidFactory()), stream);
        }
        
        if(CollectionUtil.hasValues(proteinBioSequence)){
            writeBioSequencesToStream(proteinBioSequence, new FASTA<>(new ProteinFactory()), stream);
        }
        
    }
    
    /**
     * for a group of bioSequences (all related to the same structVerId), first collapse
     * all duplicated sequences, then write it to the stream
     * @param bioSequences
     * @param fasta
     * @param stream
     * @throws UnsupportedEncodingException 
     */
    private void writeBioSequencesToStream(List<BioSequence> bioSequences, FASTA fasta, OutputStream stream) throws UnsupportedEncodingException {
        
        //if only one sequence 
        if(bioSequences.size()==1){
            fasta.write(bioSequences.get(0), stream);
            return;
        }
        
        Map<String, List<BioSequence>> groupedBioSequences = new HashMap<>();
        
        //collapse biosequences by finding duplicate sequences
        for(BioSequence bs : bioSequences){
            
            if(groupedBioSequences.containsKey(bs.getSequence())){
                List<BioSequence> existingBioSequences = groupedBioSequences.get(bs.getSequence());
                existingBioSequences.add(bs);

            } else {
                List<BioSequence> firstBs = new ArrayList<>();
                firstBs.add(bs);
                groupedBioSequences.put(bs.getSequence(), firstBs);
            }
        }
        
        //write each group to stream
        for (Map.Entry<String, List<BioSequence>> entry : groupedBioSequences.entrySet()) {
            
            String sequence = entry.getKey();
            List<BioSequence> duplicatedBioSequences = entry.getValue();
            
            
            if(duplicatedBioSequences.size() == 1){
                //if only one biosequence for this sequence, write it to the stream
                fasta.write(duplicatedBioSequences.get(0), stream);
            }
            else 
            {
                //multiple duplicateSequences. need to alter the header a bit (to include all id/desc for the same sequence)
                BioSequence firstBioSequence = duplicatedBioSequences.get(0);
                int i=1;
                int size = duplicatedBioSequences.size();
                StringBuilder header = new StringBuilder();
                for( ; i<size ; i++){
                    header.append((char) 1); //this is the fancypants character determined (by the NCBI?) to separate chain headers, but not initiate a new line
                    header.append(duplicatedBioSequences.get(i).getID());
                    header.append(" ").append(duplicatedBioSequences.get(i).getDescription());
                }
                firstBioSequence.setDescription(firstBioSequence.getDescription() + header.toString());

                fasta.write(firstBioSequence, stream);
            }
            
        }
        
    }      
    
    
    
    
    
    
    
    
    
    
    
    /**
     * 
     * @param queryParams
     * @return
     * @throws ServletException
     * @throws JDAOException 
     */
    private FullQueryInput createFullQueryInputBasedOnParams(Map<String, List<String>> queryParams) throws ServletException, JDAOException {
        
        //get all the parameters; turn 'em into objects
           
        FileStatus fileStatusRcsb = FileStatus.valueOf(queryParams.get("file_status_rcsb").get(0));
        FileStatus fileStatusManual = FileStatus.valueOf(queryParams.get("file_status_manual").get(0));
        QueryMode queryMode  = QueryMode.valueOf(queryParams.get("query_mode").get(0));

        
        List<String> subqueryParams = queryParams.get("subquery");
        if(!CollectionUtil.hasValues(subqueryParams)) {
            //if there are no subquerys on the url, make up an 'everything' one
            subqueryParams = new ArrayList<>(1);
            subqueryParams.add("EVERYTHING");
        }

        //create the full query input object...
        FullQueryInput fullQueryInput = new FullQueryInput(fileStatusRcsb, fileStatusManual, queryMode);

        //...then populate all it's subquerys with the parsed subquery parameters 
        if(CollectionUtil.hasValues(subqueryParams)){ 
            for(String subqueryParam : subqueryParams){
                Jstruct_SearchSubquery searchSubquery = createSearchSubqueryFromParam(subqueryParam);
                SubQueryInput subqueryInput = PostgresDao.createSubQueryInputFromSearchSubquery(searchSubquery, currentUser);
                fullQueryInput.addSubQueryInput(subqueryInput);
            }
        }
        
        return fullQueryInput;
    }
    
    
    
    public List<String> apiSequenceUrlIsMissingTheseKeys(Map<String, List<String>> queryParams){
        List<String> requiredKeys = Arrays.asList("sequence_from", "chain_types", "output_file_name", "file_status_rcsb", "file_status_manual", "query_mode");
        List<String> missingKeys = new ArrayList<>();
        for(String requiredKey : requiredKeys){
            List<String> foundVals = queryParams.get(requiredKey);
            if(!CollectionUtil.hasValues(foundVals)){
                missingKeys.add(requiredKey);
            }
        }
        return missingKeys;
    }
    
    
    
    private Jstruct_SearchSubquery createSearchSubqueryFromParam(String subQueryParam){
        
        String[] param = subQueryParam.split(";"); 
        
        SubQueryType subQueryType = SubQueryType.valueOf(param[0]);
        
        //create the searchSubquery..
        Jstruct_SearchSubquery searchSubquery = new Jstruct_SearchSubquery();
        searchSubquery.setSubqueryType(subQueryType);
        
        //..and add all the searchParams
        if(!subQueryType.equals(SubQueryType.EVERYTHING)){
            int i;
            for(i=1 ; i<param.length ; i++){

                Jstruct_SearchParam searchParam = new Jstruct_SearchParam();

                String[] keyValue = param[i].split(":");

                searchParam.setParamKey(keyValue[0]);
                searchParam.setParamValue(keyValue[1]);

                searchSubquery.addSearchParam(searchParam); 

            }
        }
        
        return searchSubquery;
        
    }

   
    
    
    
    
    
}
