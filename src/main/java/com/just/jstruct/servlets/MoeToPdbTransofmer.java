package com.just.jstruct.servlets;

import com.hfg.util.StringUtil;
import com.just.bio.structure.Chain;
import com.just.bio.structure.Model;
import com.just.bio.structure.Structure;
import com.just.bio.structure.format.StructureFactoryImpl;
import com.just.bio.structure.format.pdb.PDB_Format;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.utilities.JDateUtil;
import com.just.jstruct.utilities.JStringUtil;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class MoeToPdbTransofmer {
    
    
    
    // # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    // service classes and datamembers

    private final Structure structure ;
    private final Jstruct_FullStructure fullStructure;
    
    private Map<String, Character> chainNameMap;
    
    
    
    // # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    // constructors
    
    public MoeToPdbTransofmer(Structure structure, Jstruct_FullStructure fullStructure) {
        this.structure = structure;
        this.fullStructure = fullStructure;
    }
    
    
    
    
    // # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    // getters/setters
    
    public Structure getStructure() {return structure; }

    public Jstruct_FullStructure getFullStructure() {return fullStructure;}
   
    private Character getChainCharFromMap(String originalName){
        return chainNameMap.get(originalName);
    }
    
    
    // # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    // public methods

    
    public void createCoordinatesOnlyPdb(BufferedWriter writer)
          throws IOException
    {
        createChainMap(structure);

        PDB_Format formatObj = new PDB_Format<>(new StructureFactoryImpl());

        generateTitle(fullStructure, writer);
        generateRemark(fullStructure, writer);

        formatObj.writeSeqResSection(structure, writer);
        formatObj.writeCoordinateSection(structure, writer);
    }

        
    // #############################################################################################################
    // private methods
    
    private final static Pattern CHAIN_NAME_DOT_CHAR = Pattern.compile("\\..$");
        
    private void createChainMap(Structure structure){

        Model model = structure.getAtomicCoordinates().getModels().get(0);
        //if chainName is one char long, use it.
        //if chain name ends with ****.X (dot followed by character) use the final character
        //else chain name is just a space
        chainNameMap = new HashMap<>(model.getChains().size()+2);
        chainNameMap.put(null, ' ');
        
        for (Chain chain : model.getChains()) {
            
            String chainId = chain.getId();
            String chainName = chain.name();
            
            if(StringUtil.isSet(chainName)){
                
                if(chainName.length()==1)
                {
                    chainNameMap.put(chainId, chainName.toCharArray()[0]);
                } 
                else 
                {
                    Matcher chainNameDotChar = CHAIN_NAME_DOT_CHAR.matcher(chainName);
                    if(chainNameDotChar.find()){
                        chainNameMap.put(chainId, chainNameDotChar.group(0).toCharArray()[1]);
                    } else {
                        chainNameMap.put(chainId, ' ');
                    }
                    
                }
            }
            
           
            
        }
    }
    
    
    private void generateRemark(Jstruct_FullStructure fullStructure, BufferedWriter writer) throws IOException {
        writer.write("REMARK  99 \n");
        
        writer.write("REMARK  99 ");
        writer.write("generated from JStruct as a 'coordinates only PDB version' \n");
        
        writer.write("REMARK  99 ");
        writer.write("        JStruct ID: " + fullStructure.getJstructDisplayId() + " \n");
        
        writer.write("REMARK  99 ");
        writer.write("     from MOE file: " + fullStructure.getActualFileName() + " \n");
        
        writer.write("REMARK  99 ");
        writer.write("              date: " + JDateUtil.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss") + " \n");
        
        writer.write("REMARK  99 \n");
        
    }
            
        
        

    
    
    private void generateTitle(Jstruct_FullStructure fullStructure, BufferedWriter writer) throws IOException {

        List<String> titleLines = JStringUtil.splitByChunkSize(fullStructure.getPdbTitle(), 70);
        
        int rowIndex=1;
        for(String s : titleLines){
            writer.write("TITLE   "); //1-6 (7-8 not used)
            if(rowIndex==1){
                writer.write("  ");
            } else {
                writer.write(rightAlign(rowIndex, 2));  //9-10
            }
            writer.write(s); //11-80 
            writer.write("\n");
            rowIndex++;
        }
        
    }

    
 
    
    
    
    private String insertChar(Character c){
        if(c==null){
            return " ";
        }
        return c.toString();
    }
    
    private String rightAlign(String s, int size){     
        return StringUtils.repeat(" ", size - s.length()) + s;      
    }
    private String rightAlign(int i, int size){
        String s = JStringUtil.integerToString(i);
        return rightAlign(s, size);
    }
    private String leftAlign(String s, int size){
        return s + StringUtils.repeat(" ", size - s.length());      
    }
    
    private String rightAlignCoord(float d, int size){
        String s = String.format("%.3f", d);
        return rightAlign(s, size);
    }
    
    //4 available spaces, if atom name size == 1-3 it starts with 2nd space, else with 1st space.
    private String leftAlignAtomName(String s){
        if(!StringUtil.isSet(s)){
            return "    ";
        }
        if(s.length()<4){
            return " " + leftAlign(s, 3);
        } else {
            return s.substring(0,4);
        } 
    }
    
    
}



