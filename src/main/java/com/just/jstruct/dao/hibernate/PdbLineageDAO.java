package com.just.jstruct.dao.hibernate;

import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.IPdbLineageDAO;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_PdbLineage;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbLineageDAO  extends GenericDAO<Jstruct_PdbLineage, Long> implements IPdbLineageDAO, Serializable {

    
    
    /**
     * get all rows by a PdbPredecessorId (parent id)
     * @param pdbParentId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_PdbLineage> getAllByPdbPredecessorId(String pdbParentId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_PdbLineage> pdbLineages = null;
        try{
            pdbLineages = em.createNamedQuery("Jstruct_PdbLineage.findByPdbPredecessorId")
                                    .setParameter("pdbPredecessorId", pdbParentId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbLineages;
    }

    
    /**
     * get all rows by a PdbDescendantId (child id)
     * @param pdbChildId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_PdbLineage> getAllByPdbDescendantId(String pdbChildId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_PdbLineage> pdbLineages = null;
        try{
            pdbLineages = em.createNamedQuery("Jstruct_PdbLineage.findByPdbDescendantId")
                                    .setParameter("pdbDescendantId", pdbChildId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbLineages;
    }

    
    /**
     * get a row with a specific predecessor and descendant (descendant can be null)
     * @param pdbPredecessorId
     * @param pdbDescendantId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_PdbLineage getByPdbPredecessorIdAndPdbDescendantId(String pdbPredecessorId, String pdbDescendantId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_PdbLineage pdbLineage = null;
        try{
            
            if(pdbDescendantId==null){
                pdbLineage = (Jstruct_PdbLineage) em.createNamedQuery("Jstruct_PdbLineage.findByPdbPredecessorIdAndNullPdbDescendantId")
                                                    .setParameter("pdbPredecessorId", pdbPredecessorId)
                                                    .getSingleResult();
            } else {
                pdbLineage = (Jstruct_PdbLineage) em.createNamedQuery("Jstruct_PdbLineage.findByPdbPredecessorIdAndPdbDescendantId")
                                                    .setParameter("pdbPredecessorId", pdbPredecessorId)
                                                    .setParameter("pdbDescendantId", pdbDescendantId)
                                                    .getSingleResult();
            }
            
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbLineage;
    }
    
    
    /**
     * based on a pdbId, get the 'most current' pdb in the lineage
     * @param pbdIdentifier
     * @param ifCurrentIsObsoleteGetMostRecent
     * @return
     * @throws JDAOException 
     */
    @Override
    public String getCurrentBasedOnPdbIdentifier(String pbdIdentifier, boolean ifCurrentIsObsoleteGetMostRecent) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        
        String questionPdbId = pbdIdentifier.toUpperCase();
        String answerPdbId = null;
        
        try {
            StringBuilderPlus sql = new StringBuilderPlus();

            //todo: turn into a NamedNativeQuery
            //https://stackoverflow.com/questions/17261792/postgresql-recursive-self-join
            
            //this first recursive query stopped working sometime in june/uly 2018 on prod
            /*
            sql.appendln("with recursive tr(pdb_predecessor_id, pdb_descendant_id, level) as ");
            sql.appendln("    ( ");
            sql.appendln("      select pdb_lineage.pdb_predecessor_id, pdb_lineage.pdb_descendant_id, 1 as level ");
            sql.appendln("      from pdb_lineage ");
            if (ifCurrentIsObsoleteGetMostRecent) {
                sql.appendln("    where pdb_descendant_id is not null ");
            }
            sql.appendln("      union all ");
            sql.appendln("      select pdb_lineage.pdb_predecessor_id, tr.pdb_descendant_id, tr.level + 1 ");
            sql.appendln("      from pdb_lineage join ");
            sql.appendln("          tr ");
            sql.appendln("          on pdb_lineage.pdb_descendant_id = tr.pdb_predecessor_id ");
            sql.appendln("    ) ");
            sql.appendln("select * ");
            sql.appendln("from (select tr.*, ");
            sql.appendln("             max(level) over (partition by pdb_predecessor_id) as maxlevel ");
            sql.appendln("      from tr ");
            sql.appendln("     ) tr ");
            sql.appendln("where level = maxlevel ");
            sql.appendln("  and pdb_predecessor_id like :pdbPredecessorId ");
            sql.appendln("order by pdb_descendant_id desc "); 
            
            Query q = em.createNativeQuery(sql.toString());
            q.setParameter("pdbPredecessorId", questionPdbId);
            
            List<Object[]> rows = q.getResultList();
            
            if(CollectionUtil.hasValues(rows))
            {
                answerPdbId = rows.get(0)[1].toString(); 
            } 
            else 
            {
                answerPdbId = questionPdbId; //if nothing was found for the passed in pdbId, then it is our current.
            }
            */
            
            sql.appendln("WITH RECURSIVE chain(from_id, to_id) AS ( ");
            sql.append("    SELECT NULL, '").append(questionPdbId).appendln("' ");
            sql.appendln("    UNION ");
            sql.appendln("    SELECT c.to_id\\:\\:text, t.pdb_descendant_id\\:\\:text "); 
            sql.appendln("    FROM chain c ");
            sql.appendln("    LEFT OUTER JOIN pdb_lineage t ON (t.pdb_predecessor_id = to_id) ");
            sql.appendln("    WHERE c.to_id IS NOT NULL ");
            sql.appendln(") ");
            sql.appendln("SELECT from_id  ");
            sql.appendln("  FROM chain ");
            sql.appendln(" WHERE to_id IS NULL "); 
            
            Query q = em.createNativeQuery(sql.toString());
            
            Object singleResult = q.getSingleResult();
            
            if(singleResult != null)
            {
                answerPdbId = (String) singleResult; 
            } 
            else 
            {
                answerPdbId = questionPdbId; //if nothing was found for the passed in pdbId, then it is our current.
            }
            
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return answerPdbId.toUpperCase();
    }

        
    
    
    /**
     * get all FullStructure objects related to a pdb id (all predecessors and descendants)
     * @param pbdIdentifier
     * @param currentUser
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_FullStructure> getFullStructureLineageBasedOnPdbIdentifier(String pbdIdentifier, Jstruct_User currentUser) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();

        pbdIdentifier = pbdIdentifier.toUpperCase();
        
        Set<String> level1PdbIds = new HashSet<>();
        Set<String> level2PdbIds = new HashSet<>();
        level2PdbIds.add(StringUtil.singleQuote(pbdIdentifier));
        
        List<Jstruct_FullStructure> fullStructures = null;

        try {
            
            //first get all predecessors/descendants
            
            List<Jstruct_PdbLineage> firstFamily = getFamily(em, pbdIdentifier);

            if(CollectionUtil.hasValues(firstFamily)){
                for(Jstruct_PdbLineage pl : firstFamily){
                    
                    String predecessorId = pl.getPdbPredecessorId();
                    String descendantId  = pl.getPdbDescendantId();
                    
                    if (StringUtil.isSet(predecessorId)) {
                        level1PdbIds.add(predecessorId);
                        level2PdbIds.add(predecessorId);
                    }
                    if (StringUtil.isSet(descendantId)) {
                        level1PdbIds.add(descendantId);
                        level2PdbIds.add(descendantId);
                    }
                }
            }
            
            //at this point we have all parents/grandparents... and all children/grandchildren..., however there are no 'cousins' in the mix..
            //if we want to see the complete picture, we'll need to get all children/grandchildren of the oldest parent leaves, and all
            //parents/grandparents of the youngest child leaves... the easiest way to do this is to go thru our initial list, and
            //make database calls for predecessors & descendants for each of the pdbids. (todo - can we get this 'full picture' in one SQL statement?)
            
            for(String pdbId : level1PdbIds){
                List<Jstruct_PdbLineage> moreFamily = getFamily(em, pdbId);
                
                if(CollectionUtil.hasValues(moreFamily)){
                    for(Jstruct_PdbLineage pl : moreFamily){
                        
                        String predecessorId = pl.getPdbPredecessorId();
                        String descendantId  = pl.getPdbDescendantId();
                        
                        if (StringUtil.isSet(predecessorId)) {
                            level2PdbIds.add(predecessorId);
                        }
                        if (StringUtil.isSet(descendantId)) {
                            level2PdbIds.add(descendantId);
                        }
                    }
                }
            }
            
            
            //get all 'full structure' objects based on our list of pdbIds... then sort 'em by deposit date
            FullStructureService fullStructureService = new FullStructureService(currentUser);
            fullStructures = fullStructureService.getByPdbIds(new ArrayList<>(level2PdbIds), true);
            Collections.sort(fullStructures, new Comparator<Jstruct_FullStructure>() {
                @Override
                public int compare(Jstruct_FullStructure o1, Jstruct_FullStructure o2) {
                    if(null==o1 || null==o1.getPdbDepositionDate()){
                        return -1;
                    }
                    if(null==o2 || null==o2.getPdbDepositionDate()){
                        return 1;
                    }
                    if(o1.getPdbDepositionDate().equals(o2.getPdbDepositionDate())){
                        return 0;
                    } else {
                        return o1.getPdbDepositionDate().before(o2.getPdbDepositionDate()) ? -1 : 1;
                    }
                }
            });
            
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }

        return fullStructures;
    }
    
    
    /**
     * this fancy li'l helper class will return the parents/grandparents of a pdbId. (but not cousins, eh)
     * @param em
     * @param pbdIdentifier
     * @return
     * @throws Exception 
     */
    private List<Jstruct_PdbLineage> getFamily(EntityManager em, String pbdIdentifier) throws Exception 
    {
        StringBuilderPlus sql = new StringBuilderPlus();

        sql.appendln("( ");
        sql.appendln("  with recursive fullLineage as ( ");
        sql.appendln("       select pl1.* ");
        sql.appendln("         from pdb_lineage pl1 ");
        sql.appendln("        where UPPER(pl1.pdb_descendant_id) like :pbdDescendantId ");
        sql.appendln("            UNION ");
        sql.appendln("       select pl2.* ");
        sql.appendln("         from pdb_lineage pl2, fullLineage fl ");
        sql.appendln("        where pl2.pdb_descendant_id = fl.pdb_predecessor_id ");
        sql.appendln("  ) ");
        sql.appendln("  select * from fullLineage ");
        sql.appendln(") ");
        sql.appendln("  UNION  ");
        sql.appendln("( ");
        sql.appendln("  with recursive fullLineage as ( ");
        sql.appendln("       select pl1.* ");
        sql.appendln("         from pdb_lineage pl1 ");
        sql.appendln("        where UPPER(pl1.pdb_predecessor_id) like :pbdPredecessorId ");
        sql.appendln("            UNION ");
        sql.appendln("       select pl2.* ");
        sql.appendln("         from pdb_lineage pl2, fullLineage fl ");
        sql.appendln("        where pl2.pdb_predecessor_id = fl.pdb_descendant_id ");
        sql.appendln("  ) ");
        sql.appendln("  select * from fullLineage ");
        sql.appendln(") ");
            
        Query q = em.createNativeQuery(sql.toString(), Jstruct_PdbLineage.class);
        q.setParameter("pbdDescendantId", pbdIdentifier);
        q.setParameter("pbdPredecessorId", pbdIdentifier);
        
        List<Jstruct_PdbLineage> familyLineage = q.getResultList();
        return familyLineage;
        
    }
    

    

    
}
