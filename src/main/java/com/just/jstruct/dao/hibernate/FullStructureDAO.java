package com.just.jstruct.dao.hibernate;

import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.IFullStructureDAO;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.PdbLineageService;
import com.just.jstruct.dao.service.StructureRoleService;
import com.just.jstruct.enums.UserStatus;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_StructureRole;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;


/**
 * DAO for full_structure_view
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class FullStructureDAO implements IFullStructureDAO, Serializable {

    
    
    
    
    //##################################################################################################################
    // data members
    //##################################################################################################################
    
    private final Jstruct_User currentUser;
    
    private final StructureRoleService structureRoleService = new StructureRoleService();
    
    
    
    
    
    //##################################################################################################################
    // constructor
    //##################################################################################################################
    
    /**
     * constructor takes a user so we can figure out permissions for the structure
     * @param currentUser
     * @throws JDAOException 
     */
    public FullStructureDAO(Jstruct_User currentUser) throws JDAOException {
        if(currentUser != null){
            this.currentUser = currentUser;
        } else {
            JstructUserService jstructUserService = new JstructUserService();
            this.currentUser = jstructUserService.getGuestUser();
        }
    }
    
    
    
    
    
    
    //##################################################################################################################
    // functions to setup permissions 
    //##################################################################################################################
    
    
    /**
     * 
     * @param fullStructures
     * @return 
     * @throws com.just.jstruct.exception.JDAOException 
     */
    public List<Jstruct_FullStructure> setupFullStructureRoles(List<Jstruct_FullStructure> fullStructures) throws JDAOException {
        if(CollectionUtil.hasValues(fullStructures)){
            for(Jstruct_FullStructure fs : fullStructures){
                fs = setupFullStructureRole(fs);
            }
        }
        return fullStructures; 
    }
    
    
    
    
    /**
     * for a fullStructure / currentUser update all the currentUserCan* permission fields
     * (prior to calling this function, a all fullStructure currentUserCan* fields are set to false)
     * @param fullStructure
     * @return 
     * @throws com.just.jstruct.exception.JDAOException 
     */
    public Jstruct_FullStructure setupFullStructureRole(Jstruct_FullStructure fullStructure) throws JDAOException {
         
        if(fullStructure != null){
            
            //get all roles for this structure. add them to the structure.
            List<Jstruct_StructureRole> strutureRoles = structureRoleService.getByStructureId(fullStructure.getStructureId());
            fullStructure.setStructureRoles(strutureRoles);
            
            fullStructure.setCurrentUserCanRead(canRead(fullStructure));
            fullStructure.setCurrentUserCanWrite(canWrite(fullStructure));
            fullStructure.setCurrentUserCanGrant(canGrant(fullStructure));
        }
        
        return fullStructure;
    }
    
    
    
    
    /**
     * true if the currentUser can read (view) this structure
     * @param fullStructure
     * @return 
     */
    public boolean canRead(Jstruct_FullStructure fullStructure){
        
        if(fullStructure.isFromRcsb() || fullStructure.isCanAllRead() || Objects.equals(fullStructure.getOwnerId(), currentUser.getUserId())){
            return true;
        }
        
        for(Jstruct_StructureRole sr : fullStructure.getStructureRoles()){
            if(sr.getStructureId().equals(fullStructure.getStructureId()) && sr.getUserId().equals(currentUser.getUserId()) && sr.isCanRead()){
                return true;
            }
        }

        return false;

    }
    
    
    /**
     * true if the currentUser can write (edit) this structure
     * @param fullStructure
     * @return 
     */
    public boolean canWrite(Jstruct_FullStructure fullStructure){
        
        if(fullStructure.isFromRcsb()){
            return false;
        }
        
        if(currentUser.getIsRoleContributor() && currentUser.getStatus().equals(UserStatus.ACTIVE)){
            
            if(fullStructure.isCanAllWrite() || Objects.equals(fullStructure.getOwnerId(), currentUser.getUserId())){
                return true;
            }
            
            for(Jstruct_StructureRole sr : fullStructure.getStructureRoles()){
                if(sr.getStructureId().equals(fullStructure.getStructureId()) && sr.getUserId().equals(currentUser.getUserId()) && sr.isCanWrite()){
                    return true;
                }
            }
        }
        
        return false;

    }
    
    
    /**
     * true if the currentUser can grant (aka ability to also grant other users permissions) on this structure
     * @param fullStructure
     * @return 
     */
    public boolean canGrant(Jstruct_FullStructure fullStructure){
        
        if(fullStructure.isFromRcsb()){
            return false;
        }
        
        if(currentUser.getIsRoleContributor() && currentUser.getStatus().equals(UserStatus.ACTIVE)){
            
            if(Objects.equals(fullStructure.getOwnerId(), currentUser.getUserId())){
                return true;
            }
            
            for(Jstruct_StructureRole sr : fullStructure.getStructureRoles()){
                if(sr.getStructureId().equals(fullStructure.getStructureId()) && sr.getUserId().equals(currentUser.getUserId()) && sr.isCanGrant()){
                    return true;
                }
            }
            
        }
        
        return false;

    }
    
    
    
    
    
    //##################################################################################################################
    // public Hibernate DAO functions
    //##################################################################################################################
    
    
    

    /**
     * get a FullStructure based on it's primary key
     * @param structVerId of FullStructure to find
     * @return FullStructure that matches structVerId
     * @throws JDAOException
     */
    @Override
    public Jstruct_FullStructure getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_FullStructure fullStructure = null;
        try{
            fullStructure = (Jstruct_FullStructure) em.createNamedQuery("Jstruct_FullStructure.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getSingleResult();
        } catch (NoResultException nre){
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return setupFullStructureRole(fullStructure);
    }

    
    
    
    /**
     * 
     * @param structVerIds
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_FullStructure> getByStructVerIds(List<Long> structVerIds) throws JDAOException {

        if(!CollectionUtil.hasValues(structVerIds)){
            return new ArrayList<>();
        }
        
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_FullStructure> fullStructures = new ArrayList<>(structVerIds.size());
        
        try {
            
            List<List<Long>> sublists = CollectionUtil.chunkList(structVerIds, 999); //postgresql doesnt have a hard limit, but above 1000 performace may get ugly

            for (List<Long> sublist : sublists) {
                
                Query q = em.createNamedQuery("Jstruct_FullStructure.findByStructVerIds");
                q.setParameter("structVerIds", sublist);
                List<Jstruct_FullStructure> subFullStructures = q.getResultList();
                
                if (CollectionUtil.hasValues(subFullStructures)) {
                    fullStructures.addAll(subFullStructures);
                }
                
            }
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return setupFullStructureRoles(fullStructures);

    }

    
    
    /**
     * 
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_FullStructure> getByStructureId(Long structureId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_FullStructure> fullStructures = null;
        try{
            fullStructures =  em.createNamedQuery("Jstruct_FullStructure.findByStructureId")
                                .setParameter("structureId", structureId)
                                .getResultList();
           
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return setupFullStructureRoles(fullStructures);
    }

    
   

    
    /**
     * 
     * @param structureId
     * @param version
     * @param getCurrent
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_FullStructure getByStructureIdAndVersion(Long structureId, Integer version, boolean getCurrent) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        
        Jstruct_FullStructure fullStructure = null;
        
        try{
           
            //if version is not null, and getCurrent = false, then get the specificed version
            if (null!=version && !getCurrent) {
                fullStructure = (Jstruct_FullStructure) em.createNamedQuery("Jstruct_FullStructure.findByStructureIdAndVersion")
                                    .setParameter("structureId", structureId)
                                    .setParameter("version", version)
                                    .getSingleResult();
                if(null != fullStructure){
                    return setupFullStructureRole(fullStructure);
                }
            }
            
            //if skipped above section, then get the most recent version (form manual upload) of the structureId
            //todo test this SQL gets the largest version number
            fullStructure = (Jstruct_FullStructure) em.createNamedQuery("Jstruct_FullStructure.findByStructureId")
                                                      .setParameter("structureId", structureId)
                                                      .setFirstResult(0)
                                                      .setMaxResults(1)
                                                      .getSingleResult();
            
            //if current is requested, AND if this is from the RCSB, then we need to follow the lineage to get the current
            if(fullStructure.isFromRcsb() && getCurrent){
                fullStructure = getRcsbImportedRowByPbdIdentifier(fullStructure.getPdbIdentifier(), true);
            }
            
        } catch (NoResultException nre){
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return setupFullStructureRole(fullStructure);
    }

    
    
    
    /**
     * 
     * @param pdbIdentifier
     * @param jumpToCurrent
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_FullStructure getRcsbImportedRowByPbdIdentifier(String pdbIdentifier, boolean jumpToCurrent) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        
        Jstruct_FullStructure fullStructure = null;
        
        try{
            
            pdbIdentifier = pdbIdentifier.toUpperCase();

            //if jumping to current, we'll get the most recent pdbidentifier from the lineage table
            if(jumpToCurrent) {
                PdbLineageService pdbLineageService = new PdbLineageService();
                pdbIdentifier = pdbLineageService.getCurrentBasedOnPdbIdentifier(pdbIdentifier, false);
            }
        
            fullStructure = (Jstruct_FullStructure) em.createNamedQuery("Jstruct_FullStructure.findByPdbIdentifierFromRcsb")
                                    .setParameter("pdbIdentifier", pdbIdentifier)
                                    .getSingleResult();
            
        } catch (NoResultException nre){
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return setupFullStructureRole(fullStructure);
    }
    
    
    
    /**
     * 
     * @param includeActive
     * @param includeObsolete
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<String> getPdbIdsFromRcsbRun(boolean includeActive, boolean includeObsolete) throws JDAOException {
        
        //if passed in false/false (shoule never happen) return nuttin.
        if(!includeActive && !includeObsolete){
            return null;
        }
        
        EntityManager em = HibernateUtil.getEntityManager();
        List<String> pdbIds = new ArrayList<>();
        
        try {
            
            String namedQuery = "";
            if(includeActive && includeObsolete){
                //active and obsolete (aka all)
                namedQuery = "Jstruct_FullStructure.findAllPdbIdsFromRcsbRun";
            } else if (includeActive){
                //active, no obsolete
                namedQuery = "Jstruct_FullStructure.findActivePdbIdsFromRcsbRun";
            } else {
                //obsolete, no active
                namedQuery = "Jstruct_FullStructure.findObsoletePdbIdsFromRcsbRun";
            }

            pdbIds = em.createNamedQuery(namedQuery).getResultList();
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbIds;
    }

    
    
    /**
     * 
     * @param pdbIds
     * @param onlyFromRcsb
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_FullStructure> getByPdbIds(List<String> pdbIds, boolean onlyFromRcsb) throws JDAOException
    {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_FullStructure> fullStructures = null;
        
        String namedQuery = "Jstruct_FullStructure.findByPdbIdentifiers";
        if(onlyFromRcsb)
        {
            namedQuery = "Jstruct_FullStructure.findByPdbIdentifiersFromRcsb";
        }
        
        try
        {
            List<List<String>> sublists = CollectionUtil.chunkList(pdbIds, 999); //postgresql doesnt have a hard limit, but above 1000 performace may get ugly

            for (List<String> sublist : sublists)
            {
                List<Jstruct_FullStructure> subFullStructures = em.createNamedQuery(namedQuery)
                                                                  .setParameter("pdbIdentifiers", sublist)
                                                                  .getResultList();

                if (CollectionUtil.hasValues(subFullStructures))
                {
                    if (null == fullStructures)
                    {
                        fullStructures = new ArrayList<>();
                    }
                    fullStructures.addAll(subFullStructures);
                }
                
            }
            
        } 
        catch (Exception e)
        {
            throw new JDAOException(e);
        }
        finally
        {
            em.close();
        }
        return setupFullStructureRoles(fullStructures);      
    }
    
    
    
    
    
    
    /**
     * 
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_FullStructure getRandomActiveRcsbFullStructure() throws JDAOException {
        
        EntityManager em = HibernateUtil.getEntityManager();
        
        Jstruct_FullStructure fullStructure = null;
        
        try {
            StringBuilderPlus sql = new StringBuilderPlus();

            sql.appendln(" SELECT s.structure_id ");
            sql.appendln("   FROM structure s ");
            sql.appendln("  WHERE s.from_rcsb = 'Y' AND s.is_obsolete like 'N' ");
            sql.appendln(" OFFSET floor(random()*(select count(*) from structure where from_rcsb = 'Y' and is_obsolete like 'N')) ");
            sql.appendln("  LIMIT 1 ");
            
            Query q = em.createNativeQuery(sql.toString());

            List<BigInteger> rows = q.getResultList();
            
            if(CollectionUtil.hasValues(rows)){
                BigInteger structureId = (BigInteger) rows.get(0);
                fullStructure = getByStructureIdAndVersion(structureId.longValue(), 0, false);
            } 
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        
        return setupFullStructureRole(fullStructure);
        
    }
    
    
    
    
    
    
    /**
     * returns all structures (as full_structures) that are parents/grandparents/etc and children/grandchildren/etc based on a pdb identifier.
     * @param inPbdIdentifier
     * @return
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_FullStructure> getFullStructureLineageBasedOnPdbIdentifier(String inPbdIdentifier) throws JDAOException {

        StringBuilderPlus sql = new StringBuilderPlus();
        sql.appendln("select * from ");
        sql.appendln("( ");
        sql.appendln("  with recursive fullLineage as ( ");
        sql.appendln("       select pl1.pdb_lineage_id, pl1.pdb_predecessor_id, pl1.pdb_descendant_id, pl1.obsolete_date ");
        sql.appendln("         from pdb_lineage pl1 ");
        sql.appendln("        where UPPER(pl1.pdb_descendant_id) like :pdbIdOne ");
        sql.appendln("            UNION ");
        sql.appendln("       select pl2.pdb_lineage_id, pl2.pdb_predecessor_id, pl2.pdb_descendant_id, pl2.obsolete_date ");
        sql.appendln("         from pdb_lineage pl2, fullLineage fl ");
        sql.appendln("        where pl2.pdb_descendant_id = fl.pdb_predecessor_id ");
        sql.appendln("  ) ");
        sql.appendln("  select * from fullLineage ");
        sql.appendln(") ");
        sql.appendln("AS fullLineage ");
        
        sql.appendln("  UNION  ");
        
        sql.appendln("select * from ");
        sql.appendln("( ");
        sql.appendln("  with recursive fullLineage as ( ");
        sql.appendln("       select pl1.pdb_lineage_id, pl1.pdb_predecessor_id, pl1.pdb_descendant_id, pl1.obsolete_date ");
        sql.appendln("         from pdb_lineage pl1 ");
        sql.appendln("        where UPPER(pl1.pdb_predecessor_id) like :pdbIdTwo ");
        sql.appendln("            UNION ");
        sql.appendln("       select pl2.pdb_lineage_id, pl2.pdb_predecessor_id, pl2.pdb_descendant_id, pl2.obsolete_date ");
        sql.appendln("         from pdb_lineage pl2, fullLineage fl ");
        sql.appendln("        where pl2.pdb_predecessor_id = fl.pdb_descendant_id ");
        sql.appendln("  ) ");
        sql.appendln("  select * from fullLineage ");
        sql.appendln(") ");
        sql.appendln("AS fullLineage ");


        inPbdIdentifier = inPbdIdentifier.toUpperCase();

        List<Jstruct_FullStructure> fullStructures = null;


        Set<String> level1PdbIds = new HashSet<>();

        Set<String> level2PdbIds = new HashSet<>();
        level2PdbIds.add(inPbdIdentifier);
        
        
        EntityManager em = HibernateUtil.getEntityManager();

        try {
            
            //first get all predecessors/descendants
            Query q = em.createNativeQuery(sql.toString());
            q.setParameter("pdbIdOne", inPbdIdentifier);
            q.setParameter("pdbIdTwo", inPbdIdentifier);
            List<Object[]> rows = q.getResultList();
    
            if(CollectionUtil.hasValues(rows)){ 
                for(Object[] row : rows){
                    
                    String predecessorId = (String) row[1]; //pdb_predecessor_id
                    String descendantId  = (String) row[2]; //pdb_descendant_id
            
                    if (StringUtil.isSet(predecessorId)) {
                        level1PdbIds.add(predecessorId);
                        level2PdbIds.add(predecessorId);
                    }
                    if (StringUtil.isSet(descendantId)) {
                        level1PdbIds.add(descendantId);
                        level2PdbIds.add(descendantId);
                    }
                }
            }

            //at this point we have all parents/grandparents... and all children/grandchildren..., however there are no 'cousins' in the mix..
            //if we want to see the complete picture, we'll need to get all children/grandchildren of the oldest parent leaves, and all
            //parents/grandparents of the youngest child leaves... the easiest way to do this is to go thru our initial list, and
            //make database calls for predecessors & descendants for each of the pdbids. (todo - can we get this 'full picture' in one SQL statement?)

            for(String pdbId : level1PdbIds){
                Query q2 = em.createNativeQuery(sql.toString());
                q2.setParameter("pdbIdOne", inPbdIdentifier);
                q2.setParameter("pdbIdTwo", inPbdIdentifier);
                rows = q2.getResultList();

                if(CollectionUtil.hasValues(rows)){ 
                    for(Object[] row : rows){
                        String predecessorId = (String) row[1]; //pdb_predecessor_id
                        String descendantId  = (String) row[2]; //pdb_descendant_id
                        if (StringUtil.isSet(predecessorId)) {
                            level2PdbIds.add(predecessorId);
                            //level2PdbIds.add(StringUtil.singleQuote(predecessorId));
                        }
                        if (StringUtil.isSet(descendantId)) {
                            level2PdbIds.add(descendantId);
                           // level2PdbIds.add(StringUtil.singleQuote(descendantId));
                        }
                    }
                }
            }


            //get all 'full structure' objects based on our list of pdbIds... then sort 'em by deposit date
            fullStructures = getByPdbIds(new ArrayList<>(level2PdbIds), true);
            //fullStructures = Jstruct_FullStructure.getByPdbIds(inConn, new ArrayList<>(level2PdbIds), true);
            
            Collections.sort(fullStructures, new Comparator<Jstruct_FullStructure>() {
                @Override
                public int compare(Jstruct_FullStructure o1, Jstruct_FullStructure o2) {
                    if(null==o1 || null==o1.getPdbDepositionDate()){
                        return -1;
                    }
                    if(null==o2 || null==o2.getPdbDepositionDate()){
                        return 1;
                    }
                    if(o1.getPdbDepositionDate().equals(o2.getPdbDepositionDate())){
                        return 0;
                    } else {
                        return o1.getPdbDepositionDate().before(o2.getPdbDepositionDate()) ? -1 : 1;
                    }
                }
            });


        } catch (Exception e) {
            throw new JDAOException("Error gettin' full structure lineage for Structure with PDB Identifier: " + inPbdIdentifier, e);
        } finally {
            em.close();
        }

        return setupFullStructureRoles(fullStructures);

    }

    
    
    

    
}
