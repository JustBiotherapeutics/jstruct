package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IPdbSsbondDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbSsbond;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbSsbondDAO  extends GenericDAO<Jstruct_PdbSsbond, Long> implements IPdbSsbondDAO, Serializable {
    
    
    /**
     * get all PdbSsbonds by their structVerId
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_PdbSsbond> getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_PdbSsbond> pdbSsbonds = null;
        try{
            pdbSsbonds = em.createNamedQuery("Jstruct_PdbSsbond.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbSsbonds;
    }

    
    /**
     * delete all PdbSsbonds based on their structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_PdbSsbond.deleteByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }
    
    
    
    
    
}
