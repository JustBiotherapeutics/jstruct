package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IFileImportJobDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FileImportJob;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class FileImportJobDAO  extends GenericDAO<Jstruct_FileImportJob, Long> implements IFileImportJobDAO, Serializable {
    
    


    /**
     * get a Jstruct_FileImportJob based on it's primary key
     * @param fileImportJobId of Jstruct_FileImportJob to find
     * @return Jstruct_FileImportJob that matches fileImportJobId
     * @throws JDAOException
     */
    @Override
    public Jstruct_FileImportJob getByFileImportJobId(Long fileImportJobId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_FileImportJob fileImportJob = null;
        try{
            fileImportJob = (Jstruct_FileImportJob) em.createNamedQuery("Jstruct_FileImportJob.findByFileImportJobId")
                                    .setParameter("fileImportJobId", fileImportJobId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return fileImportJob;
    }
    
    
    
    
    /**
     * get ALL the FileImportJobs for a importJob
     * @param importJobId
     * @return 
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_FileImportJob> getByImportJobId(Long importJobId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_FileImportJob> fileImportJobs = new ArrayList<>();
        try {
            fileImportJobs = em.createNamedQuery("Jstruct_FileImportJob.findByImportJobId")
                             .setParameter("importJobId", importJobId)
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return fileImportJobs;
    }


    
}
