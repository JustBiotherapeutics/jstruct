package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IAnalysisFileDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisFile;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisFileDAO extends GenericDAO<Jstruct_AnalysisFile, Long> implements IAnalysisFileDAO, Serializable {
    
    

    /**
     * get a Jstruct_AnalysisFile based on it's primary key
     * @param id of Jstruct_AnalysisFile to find
     * @return Jstruct_AnalysisFile that matches id
     * @throws JDAOException
     */
    @Override
    public Jstruct_AnalysisFile getById(Long id) throws JDAOException 
    {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_AnalysisFile analysisFile = null;
        try
        {
            analysisFile = (Jstruct_AnalysisFile) em.createNamedQuery("Jstruct_AnalysisFile.findById")
                                    .setParameter("analysisFileId", id)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return analysisFile;
    }

    
    
    
    
    
    /**
     * get all Jstruct_AnalysisFile objects by analysisScriptVerId
     * @param analysisScriptVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisFile> getByAnalysisScriptVerId(Long analysisScriptVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisFile> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_AnalysisFile.findByAnalysisScriptVerId")
                    .setParameter("analysisScriptVerId", analysisScriptVerId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }
    


    
}
