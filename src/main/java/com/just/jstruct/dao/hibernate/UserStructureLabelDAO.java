package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IUserStructureLabelDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_UserStructureLabel;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class UserStructureLabelDAO implements IUserStructureLabelDAO, Serializable {

    
    
    /**
     * 
     * @param userId
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_UserStructureLabel> getByUserIdAndStructureId(Long userId, Long structureId) throws JDAOException {
        
        EntityManager em = HibernateUtil.getEntityManager();
        
        List<Jstruct_UserStructureLabel> userStructureLabels = new ArrayList<>();
        try {
            userStructureLabels = em.createNamedQuery("Jstruct_UserStructureLabel.findByUserIdAndStructureId")
                    .setParameter("userId", userId)
                    .setParameter("structureId", structureId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return userStructureLabels;
    }

    
    
    
    
    /**
     * return collection of all Jstruct_UserStructureLabels for a user
     * @param userId
     * @return all Jstruct_UserStructureLabels based on the userId
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_UserStructureLabel> getByUserId(Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_UserStructureLabel> userStructureLabels = new ArrayList<>();
        try {
            userStructureLabels = em.createNamedQuery("Jstruct_UserStructureLabel.findByUserId")
                    .setParameter("userId", userId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return userStructureLabels;
    }
    
    
    
    
    

}
