package com.just.jstruct.dao.hibernate;

import com.hfg.sql.SQLUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.IImportPdbDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportPdb;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportPdbDAO  extends GenericDAO<Jstruct_ImportPdb, Long> implements IImportPdbDAO, Serializable {
    
   

    /**
     * get a Jstruct_ImportPdb based on it's primary key
     * @param importPdbId of Jstruct_ImportPdb to find
     * @return Jstruct_ImportPdb that matches importPdbId
     * @throws JDAOException
     */
    @Override
    public Jstruct_ImportPdb getByImportPdbId(Long importPdbId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_ImportPdb importPdb = null;
        try{
            importPdb = (Jstruct_ImportPdb) em.createNamedQuery("Jstruct_ImportPdb.findByImportPdbId")
                                    .setParameter("importPdbId", importPdbId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return importPdb;
    }

    
    
    
    
    /**
     * 
     * @param importRestId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_ImportPdb> getByImportRestId(Long importRestId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ImportPdb> importPdbs = null;
        try{
            importPdbs =  em.createNamedQuery("Jstruct_ImportPdb.findByImportRestId")
                                .setParameter("importRestId", importRestId)
                                .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return importPdbs;
    }
    
    
    
    
     @Override
    public List<Jstruct_ImportPdb> getByImportJobId(Long importJobId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ImportPdb> importPdbs = null;
        try{
            importPdbs =  em.createNamedQuery("Jstruct_ImportPdb.findByImportJobId")
                                .setParameter("importJobId", importJobId)
                                .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return importPdbs;
    }


    
    
    /**
     * 
     * @param importPdbs
     * @param importRestId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_ImportPdb> bulkInsert(List<Jstruct_ImportPdb> importPdbs, Long importRestId) throws JDAOException {
        
        if(!CollectionUtil.hasValues(importPdbs)){
            return null;
        }
        
        List<Jstruct_ImportPdb> postInsertedImportPdbs = new ArrayList<>(importPdbs.size()) ;
        
        EntityManager em = HibernateUtil.getEntityManager();
        Session hibernateSession = em.unwrap(Session.class);
        
        try {
            //em.getTransaction().begin();
            hibernateSession.getTransaction().begin();

            hibernateSession.doWork(new Work() {

                @Override
                public void execute(Connection conn) throws SQLException {
                    
                    PreparedStatement ps = conn.prepareStatement("INSERT INTO import_pdb ( "
                                                               + "   pdb_identifier, "
                                                               + "   import_rest_id ) "
                                                               + " VALUES (?,?)");

                    for (Jstruct_ImportPdb importPdb : importPdbs) {
                        SQLUtil.setString(ps, 1, importPdb.getPdbIdentifier());
                        SQLUtil.setLong(ps, 2, importRestId);
                        ps.addBatch();
                    }
                    ps.executeBatch();
                    ps.close();
                }

            });
            hibernateSession.getTransaction().commit();
            //em.getTransaction().commit();
    

            //get the just inserted residues
            postInsertedImportPdbs = getByImportRestId(importRestId);

            
            
        } catch (HibernateException | JDAOException ex) {
            ex.printStackTrace();
            throw new JDAOException("Error bulk-inserting ImportPdbs for ImportRest with id: " + importRestId, ex);

        } finally {
            hibernateSession.close();
        }
        
        return postInsertedImportPdbs;
    }

   

    
}
