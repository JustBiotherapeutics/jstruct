package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IAminoAcidDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AminoAcid;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AminoAcidDAO  extends GenericDAO<Jstruct_AminoAcid, Long> implements IAminoAcidDAO, Serializable {
    
    
    
    
    
    
    /**
     * return collection of all AminoAcids
     * @return all AminoAcids
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_AminoAcid> getAll() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AminoAcid> roles = new ArrayList<>();
        try{
            roles = em.createNamedQuery("Jstruct_AminoAcid.findAll").getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return roles;
    }
    
    
    
    /**
     * return all amino acids that have an id in the passed in list
     * @param aminoAcidIds 
     * @return all AminoAcids that match
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_AminoAcid> getByAminoAcidIds(List<Integer> aminoAcidIds) throws JDAOException{
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AminoAcid> roles = new ArrayList<>();
        try{
            roles = em.createNamedQuery("Jstruct_AminoAcid.findAllByAminoAcidIds")
                                        .setParameter("aminoAcidIds", aminoAcidIds)
                                        .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return roles;
    }


    
    
    /**
     * get a AminoAcid based on it's primary key
     * @param aminoAcidId of AminoAcid to find
     * @return AminoAcid that matches id
     * @throws JDAOException
     */
    @Override
    public Jstruct_AminoAcid getByAminoAcidId(Long aminoAcidId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_AminoAcid aminoAcid = null;
        try{
            aminoAcid = (Jstruct_AminoAcid) em.createNamedQuery("Jstruct_AminoAcid.findByAminoAcidId")
                           .setParameter("aminoAcidId", aminoAcidId)
                           .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return aminoAcid;
    }


   


    /**
     * get a AminoAcid record based on it's threeLetterAbbrev
     * @param threeLetterAbbrev of AminoAcid to find
     * @return AminoAcid that matches threeLetterAbbrev
     * @throws JDAOException
     */
    @Override
    public Jstruct_AminoAcid getByThreeLetter(String threeLetterAbbrev) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_AminoAcid aminoAcid = null;
        try{
            aminoAcid = (Jstruct_AminoAcid) em.createNamedQuery("Jstruct_AminoAcid.findByAbbrev")
                                    .setParameter("abbrev", threeLetterAbbrev)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return aminoAcid;
    }


    
    



    
}
