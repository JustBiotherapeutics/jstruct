package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IStructureDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Structure;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureDAO  extends GenericDAO<Jstruct_Structure, Long> implements IStructureDAO, Serializable {
    
    
    
    
    /**
     * return collection of all Jstruct_Structures
     * @return all Jstruct_Structures
     * @throws JDAOException
     
    @Override
    public List<Jstruct_Structure> getAll() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_Structure> structures = new ArrayList<>();
        try{
            structures = em.createNamedQuery("Jstruct_Structure.findAll").getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return structures;
    }*/


    /**
     * get a Jstruct_Structure based on it's primary key
     * @param structureId of Jstruct_Structure to find
     * @return Jstruct_Structure that matches structureId
     * @throws JDAOException
     */
    @Override
    public Jstruct_Structure getByStructureId(Long structureId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_Structure structure = null;
        try{
            structure = (Jstruct_Structure) em.createNamedQuery("Jstruct_Structure.findByStructureId")
                                    .setParameter("structureId", structureId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structure;
    }
    
    
    
    /**
     * get a count of files based on the incoming boolean parameters
     * @param fromRcsb
     * @param isObsolete
     * @return
     * @throws JDAOException 
     */
    @Override
    public Integer getFileCount(boolean fromRcsb, boolean isObsolete) throws JDAOException {
        
        int fileCount = 0; 
        EntityManager em = HibernateUtil.getEntityManager();
        
        try{
            
            Query q = em.createNamedQuery("Jstruct_Structure.getFileCount")
                        .setParameter("fromRcsb", fromRcsb ? 'Y' : 'N')
                        .setParameter("isObsolete", isObsolete ? 'Y' : 'N');
            
            fileCount = ((Long) q.getSingleResult()).intValue();
            
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return fileCount;
    }

    
    
    
    
 
    
}
