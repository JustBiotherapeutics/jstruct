package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IAnalysisScriptDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisScript;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisScriptDAO extends GenericDAO<Jstruct_AnalysisScript, Long> implements IAnalysisScriptDAO, Serializable {
    
    

    /**
     * get a Jstruct_AnalysisScript based on it's primary key
     * @param id of Jstruct_AnalysisScript to find
     * @return Jstruct_AnalysisScript that matches id
     * @throws JDAOException
     */
    @Override
    public Jstruct_AnalysisScript getById(Long id) throws JDAOException 
    {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_AnalysisScript analysisScript = null;
        try
        {
            analysisScript = (Jstruct_AnalysisScript) em.createNamedQuery("Jstruct_AnalysisScript.findById")
                                    .setParameter("analysisScriptId", id)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return analysisScript;
    }

    
    
  
    
    
    
    /**
     * get all Jstruct_AnalysisScript objects by ownerId
     * @param ownerId
     * @param includeDeleted
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisScript> getByOwnerId(Long ownerId, boolean includeDeleted) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisScript> scripts = new ArrayList<>();
        try
        {
            if(includeDeleted)
            {
                scripts = em.createNamedQuery("Jstruct_AnalysisScript.findByOwnerId")
                            .setParameter("ownerId", ownerId)
                            .getResultList();
            }
            else 
            {
                scripts = em.createNamedQuery("Jstruct_AnalysisScript.findByOwnerIdNotDeleted")
                            .setParameter("ownerId", ownerId)
                            .getResultList();
            }
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return scripts;
    }
    
    
    
    /**
     * get all Jstruct_AnalysisScript objects that are marked as global
     * @param includeDeleted
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisScript> getAllMarkedAsGlobal(boolean includeDeleted) throws JDAOException
    {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisScript> scripts = new ArrayList<>();
        try
        {
            if(includeDeleted)
            {
                scripts = em.createNamedQuery("Jstruct_AnalysisScript.findAllMarkedAsGlobal")
                         .getResultList();
            }
            else 
            {
                scripts = em.createNamedQuery("Jstruct_AnalysisScript.findAllMarkedAsGlobalNotDeleted")
                         .getResultList();
            }
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return scripts;
    }


    
}
