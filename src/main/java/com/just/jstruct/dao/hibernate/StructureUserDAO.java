package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IStructureUserDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureUser;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureUserDAO  extends GenericDAO<Jstruct_StructureUser, Long> implements IStructureUserDAO, Serializable {
    
    
    
    // ======================================================================================================
    // FAVORITE 
    // ======================================================================================================

    
    

    /**
     * get 'favorite structures' for a user 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_StructureUser> getFavoritesByUserId(Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_StructureUser> structures = null;
        try{
            structures = em.createNamedQuery("Jstruct_StructureUser.getFavoritesByUserId")
                                    .setParameter("userId", userId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structures;
    }

    
    /**
     * get a 'favorite structure' for a user and structureId
     * @param structureId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_StructureUser getFavoriteByStructureIdUserId(Long structureId, Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_StructureUser structure = null;
        try{
            structure = (Jstruct_StructureUser) em.createNamedQuery("Jstruct_StructureUser.getFavoriteByStructureIdAndUserId")
                                                  .setParameter("structureId", structureId)
                                                  .setParameter("userId", userId)
                                                  .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structure;
    }
    

    
    
    
    // ======================================================================================================
    // RECENT 
    // ======================================================================================================

    
    
    
    /**
     * get 'recent structures' for a user (up to a certain amount)
     * @param userId
     * @param maxCountReturned
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_StructureUser> getRecentsByUserId(Long userId, Integer maxCountReturned) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_StructureUser> structures = null;
        try {
            structures = em.createNamedQuery("Jstruct_StructureUser.getRecentsByUserId")
                           .setParameter("userId", userId)
                           .setFirstResult(0)
                           .setMaxResults(maxCountReturned)
                           .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structures;
    }

    
    /**
     * get a 'recent structure' for a user and structVerID
     * @param structVerId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_StructureUser getRecentByStructVerIdUserId(Long structVerId, Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_StructureUser structure = null;
        try{
            structure = (Jstruct_StructureUser) em.createNamedQuery("Jstruct_StructureUser.getRecentByStructVersionIdAndUserId")
                                                  .setParameter("structVerId", structVerId)
                                                  .setParameter("userId", userId)
                                                  .getSingleResult();
            
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structure;
    }

    
    
    
    
    
}
