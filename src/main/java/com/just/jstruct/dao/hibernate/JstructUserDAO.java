package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IJstructUserDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JstructUserDAO extends GenericDAO<Jstruct_User, Long> implements IJstructUserDAO, Serializable {

    /**
     * return collection of all Jstruct_Users
     *
     * @return all Jstruct_Users
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_User> getAll() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_User> users = new ArrayList<>();
        try {
            users = em.createNamedQuery("Jstruct_User.findAll").getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return users;
    }

    /**
     * get a Jstruct_User based on it's primary key
     *
     * @param userId of Jstruct_User to find
     * @return Jstruct_User that matches userId
     * @throws JDAOException
     */
    @Override
    public Jstruct_User getByUserId(Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_User user = null;
        try {
            user = (Jstruct_User) em.createNamedQuery("Jstruct_User.findByUserId")
                    .setParameter("userId", userId)
                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return user;
    }

    /**
     * get a Jstruct_User based on it's UID
     *
     * @param uid of Jstruct_User to find
     * @return Jstruct_User that matches uid
     * @throws JDAOException
     */
    @Override
    public Jstruct_User getByUid(String uid) throws JDAOException
    {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_User user = null;
        try
        {
            user = (Jstruct_User) em.createNamedQuery("Jstruct_User.findByUid")
                    .setParameter("uid", uid.toUpperCase())
                    .getSingleResult();
        } 
        catch (Exception e)
        {
            throw new JDAOException(e);
        } 
        finally
        {
            em.close();
        }
        return user;
    }

    /**
     * get the System user
     *
     * @return system user
     * @throws JDAOException
     */
    @Override
    public Jstruct_User getSystemUser() throws JDAOException {
        return getByUid(Jstruct_User.SYSTEM_UID);
    }

    /**
     * get the System user
     *
     * @return system user
     * @throws JDAOException
     */
    @Override
    public Jstruct_User getGuestUser() throws JDAOException {
        return getByUid(Jstruct_User.GUEST_UID);
    }

    /**
     * return a list of all persons that match the inputed criteria
     *
     * @param criteria may be first name, familiar name, last name
     * @return list of all users that match criteria
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_User> getUsersByGeneralCriteria(String criteria) throws JDAOException {

        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_User> users = new ArrayList<>();
        try {
            
            Query q = em.createNamedQuery("Jstruct_User.findUsersByGeneralCriteria");
            q.setParameter("criteria", "%" + criteria.toUpperCase() + "%");
            users = (List<Jstruct_User>) q.getResultList();
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return users;

    }

    /***
     * get a list of active users with developer role
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_User> getActiveRoleDevelopers() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_User> users = new ArrayList<>();
        try {
            
            Query q = em.createNamedQuery("Jstruct_User.findActiveRoleDevelopers");
            users = (List<Jstruct_User>) q.getResultList();
            
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return users;
    }
    
    /***
     * get a list of active users with admin role
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_User> getActiveRoleAdmins() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_User> users = new ArrayList<>();
        try {
            
            Query q = em.createNamedQuery("Jstruct_User.findActiveRoleAdmins");
            users = (List<Jstruct_User>) q.getResultList();
            
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return users;
    }
    
    /***
     * get a list of active users with contributer role
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_User> getActiveRoleContributers() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_User> users = new ArrayList<>();
        try {
            
            Query q = em.createNamedQuery("Jstruct_User.findActiveRoleContributers");
            users = (List<Jstruct_User>) q.getResultList();
            
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return users;
    }

}
