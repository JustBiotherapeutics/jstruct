package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IPdbFileDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbFile;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbFileDAO extends GenericDAO<Jstruct_PdbFile, Long> implements IPdbFileDAO, Serializable {
    
    
    /**
     * get all PdbFiles by their structVerId
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_PdbFile getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_PdbFile pdbFile = null;
        try{
            pdbFile = (Jstruct_PdbFile) em.createNamedQuery("Jstruct_PdbFile.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getSingleResult();
        } catch (NoResultException nre){
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbFile;
    }

   
    
    
    
    
    
}
