package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IAnalysisScriptVerDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisScriptVer;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisScriptVerDAO extends GenericDAO<Jstruct_AnalysisScriptVer, Long> implements IAnalysisScriptVerDAO, Serializable {
    
    

    /**
     * get a Jstruct_AnalysisScriptVer based on it's primary key
     * @param id of Jstruct_AnalysisScriptVer to find
     * @return Jstruct_AnalysisScriptVer that matches id
     * @throws JDAOException
     */
    @Override
    public Jstruct_AnalysisScriptVer getById(Long id) throws JDAOException 
    {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_AnalysisScriptVer analysisScriptVer = null;
        try
        {
            analysisScriptVer = (Jstruct_AnalysisScriptVer) em.createNamedQuery("Jstruct_AnalysisScriptVer.findById")
                                    .setParameter("analysisScriptVerId", id)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return analysisScriptVer;
    }

    
    
    
    /**
     * get all Jstruct_AnalysisScriptVer objects by status
     * @param status
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisScriptVer> getByStatus(String status) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisScriptVer> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_AnalysisScriptVer.findByStatus")
                    .setParameter("status", status)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }
    
    
    
    /**
     * get all Jstruct_AnalysisScript objects by ownerId
     * @param ownerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisScriptVer> getByOwnerId(Long ownerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisScriptVer> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_AnalysisScriptVer.findByOwnerId")
                    .setParameter("ownerId", ownerId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }
    
    
    
    
    
    /**
     * get all Jstruct_AnalysisScriptVer objects by analysisScriptId
     * @param analysisScriptId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisScriptVer> getByAnalysisScriptId(Long analysisScriptId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisScriptVer> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_AnalysisScriptVer.findByAnalysisScriptId")
                    .setParameter("analysisScriptId", analysisScriptId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }
    


    
}
