package com.just.jstruct.dao.hibernate;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.IPdbRevdatDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbRevdat;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbRevdatDAO  extends GenericDAO<Jstruct_PdbRevdat, Long> implements IPdbRevdatDAO, Serializable {
    
    
    /**
     * get all PdbRevdats by their structVerId
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_PdbRevdat> getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_PdbRevdat> pdbRevdats = null;
        try{
            pdbRevdats = em.createNamedQuery("Jstruct_PdbRevdat.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbRevdats;
    }

    
    /**
     * delete all PdbRevdats based on their structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_PdbRevdat.deleteByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }

    @Override
    public Jstruct_PdbRevdat getInitialReleaseByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_PdbRevdat pdbRevdat = null;
        try{
            
            List<Jstruct_PdbRevdat> pdbRevdats = em.createNamedQuery("Jstruct_PdbRevdat.findInitialReleaseByStructVerId")
                                                   .setParameter("structVerId", structVerId)
                                                   .getResultList();
            
            if(CollectionUtil.hasValues(pdbRevdats)){
                pdbRevdat = pdbRevdats.get(0);
            }
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbRevdat;
    }
    
    
    
    
    
}
