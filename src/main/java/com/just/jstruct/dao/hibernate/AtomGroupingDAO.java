package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IAtomGroupingDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AtomGrouping;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AtomGroupingDAO implements IAtomGroupingDAO, Serializable {
    
    
    
   

    /**
     * get a Jstruct_AtomGrouping based on it's primary key
     * @param atomGroupingId of Jstruct_AtomGrouping to find
     * @return Jstruct_AtomGrouping that matches atomGroupingId
     * @throws JDAOException
     */
    @Override
    public Jstruct_AtomGrouping getByAtomGroupingId(Long atomGroupingId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_AtomGrouping atomGrouping = null;
        try{
            atomGrouping = (Jstruct_AtomGrouping) em.createNamedQuery("Jstruct_AtomGrouping.findByAtomGroupingId")
                                    .setParameter("atomGroupingId", atomGroupingId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return atomGrouping;
    }

    
    
    
    
    /**
     * get all JstructAtomGrouping objects by chainId
     * @param chainId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AtomGrouping> getByChainId(Long chainId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AtomGrouping> programs = new ArrayList<>();
        try{
            programs = em.createNamedQuery("Jstruct_AtomGrouping.findByChainId")
                    .setParameter("chainId", chainId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return programs;
    }


    /**
     * delete all Jstruct_AtomGrouping based on their chainId
     * @param chainId
     * @throws JDAOException 
     */
    @Override
    public void deleteByChainId(Long chainId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_AtomGrouping.deleteByChainId")
                                    .setParameter("chainId", chainId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }
    
    
    
    /**
     * delete all Jstruct_AtomGroupings related to a struct_ver_id
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException{
        EntityManager em = HibernateUtil.getEntityManager();
        
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM atom_grouping ag ");
        sql.append(" WHERE ag.chain_id IN ");
        sql.append("    ( ");
        sql.append("      SELECT c.chain_id FROM chain c ");
        sql.append("       WHERE c.struct_ver_id = :structVerId ");
        sql.append("    ) ");
        
        
        try {
 
            em.getTransaction().begin();
            em.createNativeQuery(sql.toString())
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
                   
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
    }

    @Override
    public Jstruct_AtomGrouping insert(Jstruct_AtomGrouping atomGrouping) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(atomGrouping);
            em.getTransaction().commit();
            em.refresh(atomGrouping);
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return atomGrouping;
    }
    
    
    
    
}
