package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IProgramDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Program;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ProgramDAO  extends GenericDAO<Jstruct_Program, Long> implements IProgramDAO, Serializable {
    
    
    
    
    /**
     * return collection of all Jstruct_Programs
     * @return all Jstruct_Programs
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_Program> getAll() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_Program> programs = new ArrayList<>();
        try{
            programs = em.createNamedQuery("Jstruct_Program.findAll").getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return programs;
    }


    /**
     * get a Jstruct_Program based on it's primary key
     * @param programId of Jstruct_Program to find
     * @return Jstruct_Program that matches programId
     * @throws JDAOException
     */
    @Override
    public Jstruct_Program getByProgramId(Long programId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_Program program = null;
        try{
            program = (Jstruct_Program) em.createNamedQuery("Jstruct_Program.findByProgramId")
                                    .setParameter("programId", programId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return program;
    }


    /**
     * get a Jstruct_Program based on it's name
     * @param name of Jstruct_Program to find
     * @return Jstruct_Program that matches name
     * @throws JDAOException
     */
    @Override
    public Jstruct_Program getByName(String name) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_Program program = null;
        try{
            program = (Jstruct_Program) em.createNamedQuery("Jstruct_Program.findByName")
                                    .setParameter("name", name)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return program;
    }
    
    
    
    /**
     * determine if a program name already exists; use programIdToIgnore if editing (or pass in null if new)
     * @param name
     * @param programIdToIgnore
     * @return
     * @throws JDAOException 
     */
    @Override
    public boolean programExists(String name, Long programIdToIgnore) throws JDAOException {
        
        int matchesFound = 0; 
        EntityManager em = HibernateUtil.getEntityManager();
        
        try{
            
            Query q;
            if(programIdToIgnore==null){
                q = em.createNamedQuery("Jstruct_Program.doesProgramNameExist");
                q.setParameter("name", name.toUpperCase());
            } else {
                
                q = em.createNamedQuery("Jstruct_Program.doesProgramNameExistIgnoreId");
                q.setParameter("name", name.toUpperCase());
                q.setParameter("programId", programIdToIgnore);
            }
            
            matchesFound = ((Long) q.getSingleResult()).intValue();
            
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return matchesFound > 0;
    }
    
    
    
    
    
    
    /**
     * determine if a program is used (e.g in the struct_version table)
     * @param programId
     * @return
     * @throws JDAOException 
     */
    @Override
    public boolean isUsed(Long programId) throws JDAOException{
        return true; //TODO update once struct_version table is implemented
    }




    
}
