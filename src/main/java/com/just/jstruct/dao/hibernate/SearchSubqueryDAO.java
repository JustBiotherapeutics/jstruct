package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.ISearchSubqueryDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SearchSubquery;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SearchSubqueryDAO  extends GenericDAO<Jstruct_SearchSubquery, Long> implements ISearchSubqueryDAO, Serializable {
    
    
    
    
    /**
     * @param searchSubqueryId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_SearchSubquery getBySearchSubqueryId(Long searchSubqueryId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_SearchSubquery searchSubquery = null;
        try{
            searchSubquery = (Jstruct_SearchSubquery) em.createNamedQuery("Jstruct_SearchSubquery.findBySearchSubqueryId")
                                          .setParameter("searchSubqueryId", searchSubqueryId)
                                          .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return searchSubquery;
    }
    
    
    /**
     * @param userSearchId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_SearchSubquery> getByUserSearchId(Long userSearchId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_SearchSubquery> searchSubquerys = null;
        try{
            searchSubquerys = em.createNamedQuery("Jstruct_SearchSubquery.findByUserSearchId")
                                    .setParameter("userSearchId", userSearchId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return searchSubquerys;
    }

    
    

    
    
    
    
    
    
}
