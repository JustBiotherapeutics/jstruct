package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IPdbCompndDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbCompnd;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbCompndDAO  extends GenericDAO<Jstruct_PdbCompnd, Long> implements IPdbCompndDAO, Serializable {
    
    
    /**
     * get all PdbCompnds by their structVerId
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_PdbCompnd> getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_PdbCompnd> pdbCompnds = null;
        try{
            pdbCompnds = em.createNamedQuery("Jstruct_PdbCompnd.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbCompnds;
    }

    
    /**
     * delete all PdbCompnds based on their structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_PdbCompnd.deleteByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }
    
    
    
    
    
}
