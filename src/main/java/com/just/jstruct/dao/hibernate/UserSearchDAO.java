package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IUserSearchDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_UserSearch;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class UserSearchDAO  extends GenericDAO<Jstruct_UserSearch, Long> implements IUserSearchDAO, Serializable {
    
    
    
    
    /**
     * @param userSearchId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_UserSearch getByUserSearchId(Long userSearchId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_UserSearch userSearch = null;
        try{
            userSearch = (Jstruct_UserSearch) em.createNamedQuery("Jstruct_UserSearch.findByUserSearchId")
                                          .setParameter("userSearchId", userSearchId)
                                          .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return userSearch;
    }
    
    
    /**
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_UserSearch> getByUserId(Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_UserSearch> userSearchs = null;
        try{
            userSearchs = em.createNamedQuery("Jstruct_UserSearch.findByUserId")
                                    .setParameter("userId", userId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return userSearchs;
    }

    
    

    
    
    
    
    
    
}
