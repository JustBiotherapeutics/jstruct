package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IChainDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Chain;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ChainDAO implements IChainDAO, Serializable {
    
    
    
   

    /**
     * get a Jstruct_Chain based on it's primary key
     * @param chainId of Jstruct_Chain to find
     * @return Jstruct_Chain that matches chainId
     * @throws JDAOException
     */
    @Override
    public Jstruct_Chain getByChainId(Long chainId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_Chain chain = null;
        try{
            chain = (Jstruct_Chain) em.createNamedQuery("Jstruct_Chain.findByChainId")
                                    .setParameter("chainId", chainId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return chain;
    }

    
    
    
    
    /**
     * get all JstructChain objects by struct_ver_id
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_Chain> getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_Chain> chains = new ArrayList<>();
        try{
            chains = em.createNamedQuery("Jstruct_Chain.findByStructVerId")
                       .setParameter("structVerId", structVerId)
                       .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return chains;
    }
    
    
    /**
     * 
     * @param structVerIds
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_Chain> getByStructVerIds(List<Long> structVerIds) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_Chain> chains = new ArrayList<>();
        try{
            chains = em.createNamedQuery("Jstruct_Chain.findByStructVerId")
                       .setParameter("structVerIds", structVerIds)
                       .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return chains;
    }

    
    
    @Override
    public Integer getCountByStructVerId(Long structVerId) throws JDAOException {
        int countOfChains = 0; 
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            
            Query q = em.createNamedQuery("Jstruct_Chain.findCountByStructVerId")
                        .setParameter("structVerId", structVerId);
            
            countOfChains = ((Long) q.getSingleResult()).intValue();
            
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return countOfChains;
    }
    
    
    
    
    
    /**
     * delete all chains related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException{
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_Chain.deleteByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }
    
    
    /**
     * add a new chain to the database
     * @param chain to add
     * @return the chain after it's saved
     * @throws JDAOException 
     */
    @Override
    public Jstruct_Chain insert(Jstruct_Chain chain) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(chain);
            em.getTransaction().commit();
            em.refresh(chain);
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return chain;
    }

    

    
    
    

    
}
