package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IImportJobDAO;
import com.just.jstruct.dao.service.ImportMsgService;
import com.just.jstruct.dao.service.ImportRestService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_ImportMsg;
import com.just.jstruct.model.Jstruct_ImportRest;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportJobDAO  extends GenericDAO<Jstruct_ImportJob, Long> implements IImportJobDAO, Serializable {
    
    
    /**
     * return collection of all Jstruct_ImportJobs
     * @return all Jstruct_ImportJob
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_ImportJob> getAll() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ImportJob> importJobs = new ArrayList<>();
        try{
            importJobs = em.createNamedQuery("Jstruct_ImportJob.findAll").getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return importJobs;
    }
   

    /**
     * get a Jstruct_ImportJob based on it's primary key
     * @param importJobId of Jstruct_ImportJob to find
     * @param includeImportRests
     * @param includeImportMsgs
     * @return Jstruct_ImportJob that matches importJobId
     * @throws JDAOException
     */
    @Override
    public Jstruct_ImportJob getByImportJobId(Long importJobId, boolean includeImportRests, boolean includeImportMsgs) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_ImportJob importJob = null;
        try{
            importJob = (Jstruct_ImportJob) em.createNamedQuery("Jstruct_ImportJob.findByImportJobId")
                                    .setParameter("importJobId", importJobId)
                                    .getSingleResult();
            
            if(includeImportRests){
                ImportRestService importRestService = new ImportRestService();
                List<Jstruct_ImportRest> importRests = importRestService.getByImportJobId(importJobId);
                importJob.setImportRests(importRests);
            }
            
            if(includeImportMsgs){
                ImportMsgService importMsgService = new ImportMsgService();
                List<Jstruct_ImportMsg> importMsgs = importMsgService.getByImportJobId(importJobId);
                importJob.setImportMsgs(importMsgs);
            }
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return importJob;
    }

    
    
    
    
    /**
     * all importJobs that are related to set of rcsb ImportJobType (see named query)
     * @return 
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_ImportJob> getAllRcsbImportJobs() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ImportJob> importJobs = new ArrayList<>();
        try{
            importJobs = em.createNamedQuery("Jstruct_ImportJob.findAllRcsbImportJobs")
                           .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return importJobs;
    }

    

    /**
     * all importJobs that are successful and related to a set of rcsb ImportJobType (see named query)
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_ImportJob getMostRecentSuccessfulRcsbRun() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_ImportJob importJob = null;
        try{
            importJob = (Jstruct_ImportJob) em.createNamedQuery("Jstruct_ImportJob.findRecentSuccessfulRcsbRuns")
                                              .setFirstResult(0)
                                              .setMaxResults(1)
                                              .getSingleResult();
        } catch (NoResultException nre){
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return importJob;
    }

    
    /**
     * 
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_ImportJob> getOnlyInitialRcsbRuns() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ImportJob> importJobs = new ArrayList<>();
        try{
            importJobs = em.createNamedQuery("Jstruct_ImportJob.findOnlyInitialRcsbRuns")
                           .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return importJobs;
    }
    
    

    /**
     * 
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_ImportJob> getOnlySuccessfulInitialRcsbRuns() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ImportJob> importJobs = new ArrayList<>();
        try{
            importJobs = em.createNamedQuery("Jstruct_ImportJob.findOnlySuccessfulInitialRcsbRuns")
                           .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return importJobs;
    }



    
}
