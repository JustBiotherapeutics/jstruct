package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IAnalysisRunDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisRun;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisRunDAO extends GenericDAO<Jstruct_AnalysisRun, Long> implements IAnalysisRunDAO, Serializable {
    
    

    /**
     * get a Jstruct_AnalysisRun based on it's primary key
     * @param id of Jstruct_AnalysisRun to find
     * @return Jstruct_AnalysisRun that matches id
     * @throws JDAOException
     */
    @Override
    public Jstruct_AnalysisRun getById(Long id) throws JDAOException 
    {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_AnalysisRun analysisRun = null;
        try
        {
            analysisRun = (Jstruct_AnalysisRun) em.createNamedQuery("Jstruct_AnalysisRun.findById")
                                    .setParameter("analysisRunId", id)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return analysisRun;
    }

    
    
    
    /**
     * get all Jstruct_AnalysisRun objects by status
     * @param status
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisRun> getByStatus(String status) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisRun> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_AnalysisRun.findByStatus")
                    .setParameter("status", status)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }
    
    
    
    /**
     * get all Jstruct_AnalysisRun objects by analysisScriptVerId
     * @param analysisScriptVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisRun> getByAnalysisScriptVerId(Long analysisScriptVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisRun> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_AnalysisRun.findByAnalysisScriptVerId")
                    .setParameter("analysisScriptVerId", analysisScriptVerId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }
    


    
}
