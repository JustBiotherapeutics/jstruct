package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.INoteDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Note;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class NoteDAO  extends GenericDAO<Jstruct_Note, Long> implements INoteDAO, Serializable {
    
    
    
    
    /**
     * get note by Id 
     * @param noteId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_Note getByNoteId(Long noteId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_Note note = null;
        try{
            note = (Jstruct_Note) em.createNamedQuery("Jstruct_Note.getByNoteId")
                                                  .setParameter("noteId", noteId)
                                                  .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return note;
    }
    

    
    
    
    /**
     * get all notes for a structureId and insertUserId 
     * @param structureId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_Note> getByStructureIdForUser(Long structureId, Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_Note> notes = null;
        try{
            notes = em.createNamedQuery("Jstruct_Note.getByStructureIdForUser")
                                    .setParameter("structureId", structureId)
                                    .setParameter("userId", userId)
                                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return notes;
    }
    
    
   
    /**
     * get all notes for a structVerId and insertUserId 
     * @param structVerId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_Note> getByStructVerIdForUser(Long structVerId, Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_Note> notes = null;
        try{
            notes = em.createNamedQuery("Jstruct_Note.getByStructVerIdForUser")
                                    .setParameter("structVerId", structVerId)
                                    .setParameter("userId", userId)
                                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return notes;
    }

    
    
    
}
