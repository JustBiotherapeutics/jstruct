package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.ISearchParamDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SearchParam;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SearchParamDAO  extends GenericDAO<Jstruct_SearchParam, Long> implements ISearchParamDAO, Serializable {
    
    
    
    
    /**
     * @param searchParamId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_SearchParam getBySearchParamId(Long searchParamId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_SearchParam searchParam = null;
        try{
            searchParam = (Jstruct_SearchParam) em.createNamedQuery("Jstruct_SearchParam.findBySearchParamId")
                                          .setParameter("searchParamId", searchParamId)
                                          .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return searchParam;
    }
    
    
    /**
     * @param searchSubqueryId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_SearchParam> getBySearchSubqueryId(Long searchSubqueryId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_SearchParam> searchParams = null;
        try{
            searchParams = em.createNamedQuery("Jstruct_SearchParam.findBySearchSubqueryId")
                                    .setParameter("searchSubqueryId", searchSubqueryId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return searchParams;
    }

    
    

    
    
    
    
    
    
}
