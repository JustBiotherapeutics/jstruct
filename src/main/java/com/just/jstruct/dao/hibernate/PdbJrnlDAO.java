package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IPdbJrnlDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbJrnl;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbJrnlDAO  extends GenericDAO<Jstruct_PdbJrnl, Long> implements IPdbJrnlDAO, Serializable {
    
    
    /**
     * get all PdbJrnls by their structVerId
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_PdbJrnl> getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_PdbJrnl> pdbJrnls = null;
        try{
            pdbJrnls = em.createNamedQuery("Jstruct_PdbJrnl.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbJrnls;
    }

    
    /**
     * delete all PdbJrnls based on their structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_PdbJrnl.deleteByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }

    
    /**
     * get the primary PdbJrnl for this structVerId
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_PdbJrnl getPrimaryByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_PdbJrnl pdbJrnl = null;
        try{
            pdbJrnl = (Jstruct_PdbJrnl) em.createNamedQuery("Jstruct_PdbJrnl.findPrimaryByStructVerId")
                                          .setParameter("structVerId", structVerId)
                                          .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbJrnl;
    }
    
    
    
    
    
}
