package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IStructureLabelDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureLabel;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureLabelDAO  extends GenericDAO<Jstruct_StructureLabel, Long> implements IStructureLabelDAO, Serializable {
    
    
    
    /**
     * get a Jstruct_StructureLabel based on it's primary key
     * @param structureLabelId of Jstruct_StructureLabel to find
     * @return Jstruct_StructureLabel that matches structureLabelId
     * @throws JDAOException
     */
    @Override
    public Jstruct_StructureLabel getByStructureLabelId(Long structureLabelId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_StructureLabel structureLabel = null;
        try{
            structureLabel = (Jstruct_StructureLabel) em.createNamedQuery("Jstruct_StructureLabel.findByStructureLabelId")
                                    .setParameter("structureLabelId", structureLabelId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structureLabel;
    }
        
    
    
    /**
     * 
     * @param userLabelId
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_StructureLabel getByUserLabelIdStructureId(Long userLabelId, Long structureId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_StructureLabel structureLabel = null;
        try{
            structureLabel = (Jstruct_StructureLabel) em.createNamedQuery("Jstruct_StructureLabel.findByUserLabelIdStructureId")
                                    .setParameter("userLabelId", userLabelId)
                                    .setParameter("structureId", structureId)
                                    .getSingleResult();
        } catch (NoResultException nre){
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structureLabel;
    }
              
                
    
    /**
     * 
     * @param userId
     * @param structureId
     * @throws JDAOException 
     */
    @Override
    public void deleteByUserAndStructure(Long userId, Long structureId) throws JDAOException{
     EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_StructureLabel.deleteByUserAndStructure")
                                    .setParameter("userId", userId)
                                    .setParameter("structureId", structureId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }
   



    
}
