package com.just.jstruct.dao.hibernate;

import com.hfg.sql.SQLQuery;
import com.hfg.sql.SQLUtil;
import com.hfg.util.io.GZIPCompressInputStream;
import com.just.bio.structure.Structure;
import com.just.bio.structure.enums.StructureFormatType;
import com.just.bio.structure.exceptions.StructureParseException;
import com.just.bio.structure.format.ReadableStructureFormat;
import com.just.bio.structure.pdb.PdbFileParse;
import com.just.bio.structure.pdb.StructureUtils;
import com.just.jstruct.dao.interfaces.IStructFileDataDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructFile;
import com.just.jstruct.utilities.HibernateUtil;
import com.just.jstruct.utilities.JFileUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructFileDataDAO implements IStructFileDataDAO, Serializable {
    
    
    
    
    
    // kindasorta helpful urls for doWork()
    //    https://justonedeveloper.wordpress.com/2013/05/29/convert-hibernate-session-to-jdbc-connection/
    //    http://stackoverflow.com/questions/3493495/getting-database-connection-in-pure-jpa-setup
    //    http://www.concretepage.com/hibernate-4/hibernate-4-returningwork-and-session-doreturningwork-example-for-jdbc (?)
    
    /**
     * 
     * @param file
     * @param structVerId
     * @throws java.io.FileNotFoundException
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public void saveDataFromFile(File file, Long structVerId) throws SQLException, IOException {
        
        EntityManager em = HibernateUtil.getEntityManager();
        Session hibernateSession = em.unwrap(Session.class);
        
                    
        try{
            hibernateSession.getTransaction().begin();
            hibernateSession.doWork(new Work() {
            @Override
                public void execute(Connection conn) throws SQLException {
                    
                    InputStream inputStream = null;
                    try {

                        inputStream = new FileInputStream(file);

                        if(!JFileUtils.isFileGZipped(file)){
                            inputStream = new GZIPCompressInputStream(inputStream);
                        }

                        PreparedStatement ps = conn.prepareStatement("UPDATE struct_file "
                                                                   + "   SET data = ? "
                                                                   + " WHERE struct_ver_id = " + structVerId);

                        ps.setBinaryStream(1, inputStream);
                        //ps.setBlob(1, inputStream);
                        ps.executeUpdate();
                        ps.close();

                    } catch (Exception ex) {
                        Logger.getLogger(StructFileDataDAO.class.getName()).log(Level.SEVERE, null, ex);
                        
                    } finally {
                        try {
                            inputStream.close();
                        } catch (IOException ex) {
                            Logger.getLogger(StructFileDataDAO.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                }
            });
            hibernateSession.getTransaction().commit();
        
        } catch (Exception ex) {
            
            ex.printStackTrace();

            //preparedStatement = conn.prepareStatement(sql.toString());
            //preparedStatement.setBinaryStream(1, inputStream);
            //preparedStatement.executeUpdate();

        } finally {

            if(hibernateSession != null){
                hibernateSession.close();
            }

        }
        
    }


    
    
   

    /**
     * 
     * @param structVerId
     * @return
     * @throws JDAOException
     * @throws SQLException
     * @throws IOException
     * @throws StructureParseException 
     */
    @Override
    public Structure getAsStructure(Long structVerId) throws JDAOException, SQLException, IOException, StructureParseException {
        
        Structure structure = null;

        Connection conn = null;
        ResultSet rs = null;

        try 
        {
            //get the metadata for the file (aka everything but the data)
            StructFileDAO structFileDAO = new StructFileDAO();
            Jstruct_StructFile structFile = structFileDAO.getByStructVerId(structVerId);

            SQLQuery query = new SQLQuery()
                .addSelect("data")
                .addFrom("struct_file")
                .addWhereClause("struct_ver_id = " + structVerId);
            
            conn = getJdbcConnectionFromEntityManagerFactory();
            rs = query.execute(conn);
            
            if (rs.next()) 
            {
                InputStream stream = rs.getBinaryStream("data");
                GZIPInputStream gzipStream = new GZIPInputStream(stream);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(gzipStream));

                PdbFileParse pdbFileParse = new PdbFileParse();
                StructureFormatType structureFormatType = StructureUtils.determineFileFormatByExtension(structFile.getActualFileExt());

                structure = pdbFileParse.parseStructureFromReader(ReadableStructureFormat.allocateReader(structureFormatType), bufferedReader);

            }

        } 
        finally 
        {
            SQLUtil.closeResultSetAndStatement(rs);
            SQLUtil.close(conn);
        }

        return structure;
        
    }
    
    
    
    /**
     * 
     * @return
     * @throws SQLException 
     */
    private Connection getJdbcConnectionFromEntityManagerFactory() throws SQLException {
        
        //fallback to jdbc (no hibernate) to get the data as a stream
        
        EntityManagerFactory emf = HibernateUtil.getEntityManagerFactory();
        Map<String, Object> emfProperties = emf.getProperties();
        
        String dataSourceUrl = (String) emfProperties.get("hibernate.hikari.dataSource.url");
        String dataSourceUser = (String) emfProperties.get("hibernate.hikari.dataSource.user");
        String dataSourcePass = (String) emfProperties.get("hibernate.hikari.dataSource.password");
        
        //register the database driver
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            Logger.getLogger(StructFileDataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Connection conn = DriverManager.getConnection(dataSourceUrl, dataSourceUser, dataSourcePass);
        return conn;
        
    }
    
    
    
    
    /**
     * 
     * @param structVerId
     * @param outputStream
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public void getData(Long structVerId, OutputStream outputStream) throws SQLException, IOException {
  
        Connection conn = null;
        ResultSet rs = null;
        
        try {
            
            SQLQuery query = new SQLQuery()
                .addSelect("data")
                .addFrom("struct_file")
                .addWhereClause("struct_ver_id = " + structVerId);
            
            conn = getJdbcConnectionFromEntityManagerFactory();
            rs = query.execute(conn);
            
            if (rs.next())
            {
                InputStream stream = rs.getBinaryStream("data");
                GZIPInputStream gzipStream = new GZIPInputStream(stream);

                byte[] transferBuffer = new byte[32 * 1024];
                int readSize;
                while ((readSize = gzipStream.read(transferBuffer)) != -1)
                {
                    outputStream.write(transferBuffer, 0, readSize);
                }

                gzipStream.close();
                outputStream.close();
            }
        }
        finally
        {
            SQLUtil.closeResultSetAndStatement(rs);
            SQLUtil.close(conn);
        }
        
    }
    
    
    
    
    
    
    

    /**
     * 
     * @param structVerId
     * @param outputStream
     * @throws SQLException
     * @throws IOException 
     */
    @Override
    public void getCompressedData(Long structVerId, OutputStream outputStream) throws SQLException, IOException {
      
        Connection conn = null;
        ResultSet rs = null;
        
        try {
            
            SQLQuery query = new SQLQuery()
                .addSelect("data")
                .addFrom("struct_file")
                .addWhereClause("struct_ver_id = " + structVerId);
            
            conn = getJdbcConnectionFromEntityManagerFactory();
            rs = query.execute(conn);
            
            if (rs.next())
            {
                InputStream stream = rs.getBinaryStream("data");

                byte[] transferBuffer = new byte[32 * 1024];
                int readSize;
                while ((readSize = stream.read(transferBuffer)) != -1)
                {
                    outputStream.write(transferBuffer, 0, readSize);
                }

                stream.close();
                outputStream.close();
            }
        }
        finally
        {
            SQLUtil.closeResultSetAndStatement(rs);
            SQLUtil.close(conn);
        }
    }


    
}
