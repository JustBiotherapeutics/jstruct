package com.just.jstruct.dao.hibernate;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.ISequencesSeqresDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SequencesSeqres;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * DAO for sequences_seqres_view
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SequencesSeqresDAO  implements ISequencesSeqresDAO, Serializable {
    
    


    
    
    
    /**
     * 
     * @param structVerIds
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_SequencesSeqres> getByStructVerIds(List<Long> structVerIds) throws JDAOException {

        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_SequencesSeqres> sequencesSeqres = null;
        
        try {
            
            List<List<Long>> sublists = CollectionUtil.chunkList(structVerIds, 999); //postgresql doesnt have a hard limit, but above 1000 performace may get ugly

            for (List<Long> sublist : sublists) {
                
                
                Query q = em.createNamedQuery("Jstruct_SequencesSeqres.findByStructVerIds");
                q.setParameter("structVerIds", sublist);
                List<Jstruct_SequencesSeqres> subSequencesSeqres = q.getResultList();
                

                if (CollectionUtil.hasValues(subSequencesSeqres)) {
                    if (null == sequencesSeqres) {
                        sequencesSeqres = new ArrayList<>();
                    }
                    sequencesSeqres.addAll(subSequencesSeqres);
                }
                
            }
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return sequencesSeqres;

    }

    
    
    
    
    /**
     * list of all structVerIds for structures that have SEQRES data
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Long> getStructVerIds() throws JDAOException {
        
        EntityManager em = HibernateUtil.getEntityManager();
        
        List<Long> structVerIds = new ArrayList<>();
        try {
            structVerIds = em.createNamedQuery("Jstruct_SequencesSeqres.findAllStructVerIds")
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return structVerIds;
        
    }
    
    
    

    
}
