package com.just.jstruct.dao.hibernate;

import com.hfg.sql.SQLUtil;
import com.hfg.util.StringBuilderPlus;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.IResidueDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Residue;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ResidueDAO implements IResidueDAO, Serializable {
    
    
    
   

    /**
     * get a Jstruct_Residue based on it's primary key
     * @param residueId of Jstruct_Residue to find
     * @return Jstruct_Residue that matches residueId
     * @throws JDAOException
     */
    @Override
    public Jstruct_Residue getByResidueId(Long residueId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_Residue residue = null;
        try{
            residue = (Jstruct_Residue) em.createNamedQuery("Jstruct_Residue.findByResidueId")
                                    .setParameter("residueId", residueId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return residue;
    }

    
    
    
    
    /**
     * get all JstructResidue objects by atomGroupingId
     * @param atomGroupingId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_Residue> getByAtomGroupingId(Long atomGroupingId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_Residue> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_Residue.findByAtomGroupingId")
                    .setParameter("atomGroupingId", atomGroupingId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }

    
/**
     * delete all Jstruct_Residues based on their atomGroupingId
     * @param atomGroupingId
     * @throws JDAOException 
     */
    @Override
    public void deleteByAtomGroupingId(Long atomGroupingId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_Residue.deleteByAtomGroupingId")
                                    .setParameter("atomGroupingId", atomGroupingId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }

    
    
    
    /**
     * delete all Jstruct_Residues related to a struct_ver_id
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException{
        EntityManager em = HibernateUtil.getEntityManager();
        
        StringBuilderPlus sql = new StringBuilderPlus();
        sql.appendln("DELETE FROM residue res ");
        sql.appendln(" WHERE res.atom_grouping_id IN ");
        sql.appendln("    ( ");
        sql.appendln("      SELECT ag.atom_grouping_id FROM atom_grouping ag ");
        sql.appendln("       WHERE ag.chain_id IN ");
        sql.appendln("          ( ");
        sql.appendln("            SELECT c.chain_id FROM chain c ");
        sql.appendln("             WHERE c.struct_ver_id = :structVerId ");
        sql.appendln("          ) ");
        sql.appendln("    ) ");
        
        
        try {
 
            em.getTransaction().begin();
            em.createNativeQuery(sql.toString())
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
                   
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
    }

    @Override
    public List<Jstruct_Residue> bulkInsert(List<Jstruct_Residue> residues, Long atomGroupingId) throws JDAOException {
        
        if (!CollectionUtil.hasValues(residues)) {
            return null;
        }

        List<Jstruct_Residue> postInsertedResidues = new ArrayList<>(residues.size());

        EntityManager em = HibernateUtil.getEntityManager();
        Session hibernateSession = em.unwrap(Session.class);;

        try {
            //em.getTransaction().begin();
            hibernateSession.getTransaction().begin();

            hibernateSession.doWork(new Work() {

                @Override
                public void execute(Connection conn) throws SQLException {

                    PreparedStatement ps = conn.prepareStatement("INSERT INTO residue ( "
                                                               + "  ATOM_GROUPING_ID, "
                                                               + "  RES_SEQ_NUMBER, "
                                                               + "  ICODE, "
                                                               + "  ABBREV, "
                                                               + "  RESIDUE_TYPE, "
                                                               + "  ATOMS, "
                                                               + "  HAS_ALL_COORDS, "
                                                               + "  HAS_ANY_COORDS, "
                                                               + "  HAS_CA_COORDS, "
                                                               + "  HAS_BKBN_COORDS, "
                                                               + "  HAS_BKBN_CB_COORDS ) "
                                                               + "VALUES (?,?,?,?,?,?,?,?,?,?,?)");

                    for (Jstruct_Residue residue : residues) {
                        
                        String iCode = residue.getIcode() != null ? residue.getIcode().toString() : null;
                        
                        SQLUtil.setLong(ps, 1, atomGroupingId);
                        SQLUtil.setInt(ps, 2, residue.getResSeqNumber());
                        SQLUtil.setString(ps, 3, iCode);
                        SQLUtil.setString(ps, 4, residue.getAbbrev());
                        SQLUtil.setString(ps, 5, residue.getResidueType());
                        SQLUtil.setString(ps, 6, residue.getAtoms());
                        SQLUtil.setString(ps, 7, residue.hasAllCoords() ? "Y" : "N");
                        SQLUtil.setString(ps, 8, residue.hasAnyCoords() ? "Y" : "N");
                        SQLUtil.setString(ps, 9, residue.hasCaCoords() ? "Y" : "N");
                        SQLUtil.setString(ps, 10, residue.hasBkbnCoords() ? "Y" : "N");
                        SQLUtil.setString(ps, 11, residue.hasBkbnCbCoords() ? "Y" : "N");
                        ps.addBatch();
                    }
                    ps.executeBatch();
                    ps.close();
                }

            });
            hibernateSession.getTransaction().commit();

            //get the just inserted residues
            postInsertedResidues = getByAtomGroupingId(atomGroupingId);

        } catch (HibernateException | JDAOException ex) {
            ex.printStackTrace();
            throw new JDAOException("Error bulk-inserting Residues for Atom Grouping with id: " + atomGroupingId + " (related to a Chain)", ex);

        } finally {
            hibernateSession.close();
        }

        return postInsertedResidues;

    }

    
}
