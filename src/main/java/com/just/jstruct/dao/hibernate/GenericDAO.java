package com.just.jstruct.dao.hibernate;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.IGenericDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * generic add, delete, and update functionality for all DAO classes
 * @param <T>
 * @param <ID>
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public abstract class GenericDAO<T, ID extends Serializable> implements IGenericDAO<T, ID>, Serializable {
 
    
    /**
     * add a new entity to the database of type T
     * @param entity to add
     * @return the entity after it's saved
     * @throws JDAOException 
     */
    @Override
    public T add(T entity) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
            em.refresh(entity);
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return entity;
    }

    
    
    /**
     * insert a batch/list of entities of type T
     * @param entities to add
     * @return
     * @throws JDAOException 
     
    @Override
    public List<T> batchInsert(List<T> entities) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try {
            
            if(CollectionUtil.hasValues(entities)){
                em.getTransaction().begin();
                
                for(T entity : entities){
                    em.persist(entity);
                    em.getTransaction().commit();
                    em.refresh(entity);
                }
                
            }
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return entities;
    }*/
    
    
    /**
     * insert a batch/list of entities of type T
     * @param entities to add
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<T> batchInsert(List<T> entities) throws JDAOException {
        
        if(!CollectionUtil.hasValues(entities)){
            return null;
        }
        
        EntityManager em = HibernateUtil.getEntityManager();
        
        List<T> savedEntities = new ArrayList<>(entities.size());
        int i=0;
        
        try {
            
                em.getTransaction().begin();
                
                for(T entity : entities){
                    
                    em.persist(entity);
                    savedEntities.add(entity);
                    
                    if(i % 20 == 0){
                        em.flush();
                        em.clear();
                    }
                    
                }
                
                em.getTransaction().commit();
            
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return savedEntities;
    }
    
    
    

    /**
     * delete an entity of type T from the database
     * @param entity to delete
     * @throws JDAOException
     */
    @Override
    public void delete(T entity) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try {
            em.getTransaction().begin();
            entity = em.merge(entity);
            em.remove(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }


    /**
     * update an entity of type T in the database
     * @param entity to update
     * @return the entity after it's updated
     * @throws JDAOException
     */
    @Override
    public T update(T entity) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try {
            em.getTransaction().begin();
            entity = em.merge(entity);
            em.getTransaction().commit();
            em.refresh(entity); //this may be necessary if we're looking for trigger updated data such as audit info
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return entity;
    }




}
