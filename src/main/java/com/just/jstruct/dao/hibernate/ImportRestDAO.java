package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IImportRestDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportRest;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportRestDAO  extends GenericDAO<Jstruct_ImportRest, Long> implements IImportRestDAO, Serializable {
    
   

    /**
     * get a Jstruct_ImportRest based on it's primary key
     * @param importRestId of Jstruct_ImportRest to find
     * @return Jstruct_ImportRest that matches importRestId
     * @throws JDAOException
     */
    @Override
    public Jstruct_ImportRest getByImportRestId(Long importRestId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_ImportRest importRest = null;
        try{
            importRest = (Jstruct_ImportRest) em.createNamedQuery("Jstruct_ImportRest.findByImportRestId")
                                    .setParameter("importRestId", importRestId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return importRest;
    }

    
    
    
    
    /**
     * 
     * @param importJobId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_ImportRest> getByImportJobId(Long importJobId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ImportRest> importRests = null;
        try{
            importRests =  em.createNamedQuery("Jstruct_ImportRest.findByImportJobId")
                                .setParameter("importJobId", importJobId)
                                .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return importRests;
    }

    
    
    
    


    
}
