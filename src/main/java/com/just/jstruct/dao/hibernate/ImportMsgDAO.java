package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IImportMsgDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportMsg;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportMsgDAO  extends GenericDAO<Jstruct_ImportMsg, Long> implements IImportMsgDAO, Serializable {
    
   

    /**
     * get a Jstruct_ImportMsg based on it's primary key
     * @param importMsgId of Jstruct_ImportMsg to find
     * @return Jstruct_ImportMsg that matches importMsgId
     * @throws JDAOException
     */
    @Override
    public Jstruct_ImportMsg getByImportMsgId(Long importMsgId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_ImportMsg importMsg = null;
        try{
            importMsg = (Jstruct_ImportMsg) em.createNamedQuery("Jstruct_ImportMsg.findByImportMsgId")
                                    .setParameter("importMsgId", importMsgId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return importMsg;
    }

    
    
    
    
    /**
     * 
     * @param importJobId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_ImportMsg> getByImportJobId(Long importJobId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ImportMsg> importMsgs = null;
        try{
            importMsgs =  em.createNamedQuery("Jstruct_ImportMsg.findByImportJobId")
                                .setParameter("importJobId", importJobId)
                                .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return importMsgs;
    }

    
    
    
    


    
}
