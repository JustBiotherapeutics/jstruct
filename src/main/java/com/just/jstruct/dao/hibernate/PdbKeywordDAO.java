package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IPdbKeywordDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbKeyword;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbKeywordDAO  extends GenericDAO<Jstruct_PdbKeyword, Long> implements IPdbKeywordDAO, Serializable {
    
    
    /**
     * get all PdbKeywords by their structVerId
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_PdbKeyword> getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_PdbKeyword> pdbKeywords = null;
        try{
            pdbKeywords = em.createNamedQuery("Jstruct_PdbKeyword.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return pdbKeywords;
    }

    
    /**
     * delete all PdbKeywords based on their structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_PdbKeyword.deleteByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }
    
    
    
    
    
}
