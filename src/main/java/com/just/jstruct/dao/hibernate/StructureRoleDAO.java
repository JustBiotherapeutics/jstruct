package com.just.jstruct.dao.hibernate;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.IStructureRoleDAO;
import com.just.jstruct.dao.service.StructureRoleService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureRole;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureRoleDAO  extends GenericDAO<Jstruct_StructureRole, Long> implements IStructureRoleDAO, Serializable {
    
    
    
    // ======================================================================================================
    // FAVORITE 
    // ======================================================================================================

    
    


    /**
     * get based on a structureRoleId
     * @param structureRoleId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_StructureRole getByStructureRoleId(Long structureRoleId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_StructureRole structure = null;
        try{
            structure = (Jstruct_StructureRole) em.createNamedQuery("Jstruct_StructureRole.getByStructureRoleId")
                                                  .setParameter("structureId", structureRoleId)
                                                  .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structure;
    }
    

    
    
    
   /**
     * get based on a userId and structureId
     * @param structureId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public Jstruct_StructureRole getByStructureIdUserId(Long structureId, Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        
        try{
            List<Jstruct_StructureRole> structures = em.createNamedQuery("Jstruct_StructureRole.getByStructureIdAndUserId")
                                                       .setParameter("structureId", structureId)
                                                       .setParameter("userId", userId)
                                                       .getResultList();
            if(structures.size() == 1)
            {
                return structures.get(0);
            }
            else 
            {
                return combineAndFixDuplicateRoles(structures);
            }
            
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }
    
    private Jstruct_StructureRole combineAndFixDuplicateRoles(List<Jstruct_StructureRole> roles) throws JDAOException {
        if(CollectionUtil.hasValues(roles)){
            
            StructureRoleService structureRoleService = new StructureRoleService();
            
            Jstruct_StructureRole singleRole = new Jstruct_StructureRole(roles.get(0).getStructureId(), roles.get(0).getUser(), false, false, false);
            
            for(Jstruct_StructureRole role : roles)
            {
                singleRole.setCanRead(singleRole.isCanRead() || role.isCanRead());
                singleRole.setCanWrite(singleRole.isCanWrite() || role.isCanWrite());
                singleRole.setCanGrant(singleRole.isCanGrant() || role.isCanGrant());
                
                structureRoleService.delete(role);
                
            }
            structureRoleService.add(singleRole);
            return singleRole;
        }
        return null;
    }
    
    
    
    
    
    
    
   /**
     * get a list of roles for a structure
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_StructureRole> getByStructureId(Long structureId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_StructureRole> structures = null;
        try{
            structures = em.createNamedQuery("Jstruct_StructureRole.getByStructureId")
                           .setParameter("structureId", structureId)
                           .getResultList();
            
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structures;
    }
    

    
    
    
        
    /**
     * 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Long> getReadStructureIdsForUser(Long userId) throws JDAOException {
       
        EntityManager em = HibernateUtil.getEntityManager();
        
        List<Long> structureId = new ArrayList<>();
        try {
            structureId = em.createNamedQuery("Jstruct_StructureRole.findReadStructureIdsForUser")
                            .setParameter("userId", userId)
                            .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return structureId;
    }
    
    /**
     * 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Long> getWriteStructureIdsForUser(Long userId) throws JDAOException {
       
        EntityManager em = HibernateUtil.getEntityManager();
        
        List<Long> structureId = new ArrayList<>();
        try {
            structureId = em.createNamedQuery("Jstruct_StructureRole.findWriteStructureIdsForUser")
                            .setParameter("userId", userId)
                            .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return structureId;
    }
    
    /**
     * 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Long> getGrantStructureIdsForUser(Long userId) throws JDAOException {
       
        EntityManager em = HibernateUtil.getEntityManager();
        
        List<Long> structureId = new ArrayList<>();
        try {
            structureId = em.createNamedQuery("Jstruct_StructureRole.findGrantStructureIdsForUser")
                            .setParameter("userId", userId)
                            .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return structureId;
    }
    
    
    
    
}
