package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IStructFileDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructFile;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructFileDAO  extends GenericDAO<Jstruct_StructFile, Long> implements IStructFileDAO, Serializable {
    
    


    /**
     * get a Jstruct_StructFile based on the (unique) struct_ver_id
     * @param structVerId of Jstruct_StructFile to find
     * @return Jstruct_StructFile that matches structVerId
     * @throws JDAOException
     */
    @Override
    public Jstruct_StructFile getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_StructFile structFile = null;
        try{
            structFile = (Jstruct_StructFile) em.createNamedQuery("Jstruct_StructFile.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structFile;
    }
    
    
    
    
    /**
     * get all structFiles that match the structVerId list
     * @param structVerIds
     * @return 
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_StructFile> getByStructVerIds(List<Long> structVerIds) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_StructFile> structFiles = new ArrayList<>();
        try {
            structFiles = em.createNamedQuery("Jstruct_StructFile.findByStructVerIds")
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structFiles;
    }
    
    
    

   

    
    
   
    
}
