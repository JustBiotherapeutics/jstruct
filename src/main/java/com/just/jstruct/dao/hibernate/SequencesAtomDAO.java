package com.just.jstruct.dao.hibernate;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.ISequencesAtomDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SequencesAtom;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * DAO for sequences_atom_view
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SequencesAtomDAO  implements ISequencesAtomDAO, Serializable {
    
    


    
    
    
    /**
     * 
     * @param structVerIds
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_SequencesAtom> getByStructVerIds(List<Long> structVerIds) throws JDAOException {

        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_SequencesAtom> sequencesAtom = null;
        
        try {
            
            List<List<Long>> sublists = CollectionUtil.chunkList(structVerIds, 999); //postgresql doesnt have a hard limit, but above 1000 performace may get ugly

            for (List<Long> sublist : sublists) {
                
                
                Query q = em.createNamedQuery("Jstruct_SequencesAtom.findByStructVerIds");
                q.setParameter("structVerIds", sublist);
                List<Jstruct_SequencesAtom> subSequencesAtom = q.getResultList();
                

                if (CollectionUtil.hasValues(subSequencesAtom)) {
                    if (null == sequencesAtom) {
                        sequencesAtom = new ArrayList<>();
                    }
                    sequencesAtom.addAll(subSequencesAtom);
                }
                
            }
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return sequencesAtom;

    }

    
    
    /**
     * 
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Long> getStructVerIds() throws JDAOException {
        
        EntityManager em = HibernateUtil.getEntityManager();
        
        List<Long> structVerIds = new ArrayList<>();
        try {
            structVerIds = em.createNamedQuery("Jstruct_SequencesAtom.findAllStructVerIds")
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return structVerIds;
        
    }
    

    
}
