package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IUserLabelDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_UserLabel;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class UserLabelDAO extends GenericDAO<Jstruct_UserLabel, Long> implements IUserLabelDAO, Serializable {
    
    
    
    /**
     * get a Jstruct_UserLabel based on it's primary key
     * @param userLabelId of Jstruct_UserLabel to find
     * @return Jstruct_UserLabel that matches userLabelId
     * @throws JDAOException
     */
    @Override
    public Jstruct_UserLabel getByUserLabelId(Long userLabelId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_UserLabel userLabel = null;
        try{
            userLabel = (Jstruct_UserLabel) em.createNamedQuery("Jstruct_UserLabel.findByUserLabelId")
                                    .setParameter("userLabelId", userLabelId)
                                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return userLabel;
    }
               
                
                
    
    
    /**
     * return collection of all Jstruct_UserLabels for a user
     * @param userId
     * @return all Jstruct_UserLabels
     * @throws JDAOException
     */
    @Override
     public List<Jstruct_UserLabel> getByUserId(Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_UserLabel> userLabels = new ArrayList<>();
        try{
            userLabels = em.createNamedQuery("Jstruct_UserLabel.findByUserId")
                           .setParameter("userId", userId)
                           .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return userLabels;
    }

     
                
    
    
    /**
     * return collection of all Jstruct_UserLabels for a user and structure
     * @param userId
     * @param structureId
     * @return all Jstruct_UserLabels
     * @throws JDAOException
     */
    @Override
     public List<Jstruct_UserLabel> getByUserIdStructureId(Long userId, Long structureId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_UserLabel> userLabels = new ArrayList<>();
        try{
            userLabels = em.createNamedQuery("Jstruct_UserLabel.findByUserIdStructureId")
                           .setParameter("userId", userId)
                           .setParameter("structureId", structureId)
                           .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return userLabels;
    }

     
     
    
    /**
     * return collection of all Jstruct_UserLabels for a user
     * @param userId
     * @return all Jstruct_UserLabels
     * @throws JDAOException
     */
    @Override
     public List<Jstruct_UserLabel> getUnusedByUserId(Long userId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_UserLabel> userLabels = new ArrayList<>();
        try{
            userLabels = em.createNamedQuery("Jstruct_UserLabel.findUnusedByUserId")
                           .setParameter("userId", userId)
                           .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return userLabels;
    }


   



    
}
