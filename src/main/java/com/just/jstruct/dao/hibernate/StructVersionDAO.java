package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IStructVersionDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructVersion;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructVersionDAO  extends GenericDAO<Jstruct_StructVersion, Long> implements IStructVersionDAO, Serializable {
    
    


    /**
     * get a Jstruct_StructVersion based on it's primary key
     * @param structVerId of Jstruct_StructVersion to find
     * @return Jstruct_StructVersion that matches structVerId
     * @throws JDAOException
     */
    @Override
    public Jstruct_StructVersion getByStructVerId(Long structVerId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_StructVersion structVersion = null;
        try{
            structVersion = (Jstruct_StructVersion) em.createNamedQuery("Jstruct_StructVersion.findByStructVerId")
                                    .setParameter("structVerId", structVerId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structVersion;
    }
    
    
    
    
    
    /**
     * get the most recent (youngest, aka newest) struct_version for a structure id
     * @param structureId 
     * @return structVersion that is youngest for this structure
     * @throws JDAOException
     */
    @Override
    public Jstruct_StructVersion getYoungestByStructureId(Long structureId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_StructVersion structVersion = null;
        try{
           
            //todo test this.
            //todo move to a named query.
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT sv ");
            sb.append("  FROM Jstruct_StructVersion sv ");
            sb.append(" WHERE sv.structureId = :structureId ");
            sb.append("ORDER BY sv.version DESC ");

            structVersion = (Jstruct_StructVersion) em.createQuery(sb.toString())
                                    .setParameter("structureId", structureId)
                                    .setFirstResult(0)
                                    .setMaxResults(1)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structVersion;
    }
    
    
    
    
    
    /**
     * get ALL the structVerIds in the database
     * @return 
     * @throws JDAOException 
     */
    @Override
    public List<Long> getAllStructVerIds() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Long> structVerIds = new ArrayList<>();
        try {
            structVerIds = em.createNamedQuery("Jstruct_StructVersion.findAllStructVerIds")
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structVerIds;
    }
    
    
    /**
     * get struct version ids for ALL manually uploaded files
     * @return 
     * @throws JDAOException 
     */
    @Override
    public List<Long> getManualStructVerIds() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Long> structVerIds = new ArrayList<>();
        try {
            structVerIds = em.createNamedQuery("Jstruct_StructVersion.findManualStructVerIds")
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structVerIds;
    }
    
    
    
    
    
    
    
    /**
     * get struct version ids for ALL active RCSB files
     * @return 
     * @throws JDAOException 
     */
    @Override
    public List<Long> getActiveRcsbStructVerIds() throws JDAOException{
        EntityManager em = HibernateUtil.getEntityManager();
        List<Long> structVerIds = new ArrayList<>();
        try {
            structVerIds = em.createNamedQuery("Jstruct_StructVersion.findActiveRcsbStructVerIds")
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structVerIds;
    }
    
    /**
     * get struct version ids for ALL active manually uploaded 'model' files
     * @return 
     * @throws JDAOException 
     */
    @Override
    public List<Long> getActiveManualModelStructVerIds() throws JDAOException{
        EntityManager em = HibernateUtil.getEntityManager();
        List<Long> structVerIds = new ArrayList<>();
        try {
            structVerIds = em.createNamedQuery("Jstruct_StructVersion.findActiveManualModelStructVerIds")
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structVerIds;
    }
    
    /**
     * get struct version ids for ALL active manually uploaded 'NON-model' files
     * @return 
     * @throws JDAOException 
     */
    @Override
    public List<Long> getActiveManualNonModelStructVerIds() throws JDAOException{
        EntityManager em = HibernateUtil.getEntityManager();
        List<Long> structVerIds = new ArrayList<>();
        try {
            structVerIds = em.createNamedQuery("Jstruct_StructVersion.findActiveManualNonModelStructVerIds")
                             .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return structVerIds;
    }
    
    


    
}
