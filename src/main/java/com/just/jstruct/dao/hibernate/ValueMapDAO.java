package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IValueMapDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ValueMap;
import com.just.jstruct.pojos.ValueMapGroup;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ValueMapDAO  extends GenericDAO<Jstruct_ValueMap, Long> implements IValueMapDAO, Serializable {
    
    
    
    /**
     * return collection of all ValueMaps
     * @return all ValueMaps
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_ValueMap> getAll() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ValueMap> roles = new ArrayList<>();
        try{
            roles = em.createNamedQuery("Jstruct_ValueMap.findAll").getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return roles;
    }


    /**
     * get a ValueMap based on it's primary key
     * @param valueMapId of ValueMap to find
     * @return ValueMap that matches id
     * @throws JDAOException
     */
    @Override
    public Jstruct_ValueMap getByValueMapId(Long valueMapId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_ValueMap valueMap = null;
        try{
            valueMap = (Jstruct_ValueMap) em.createNamedQuery("Jstruct_ValueMap.findByValueMapId")
                           .setParameter("valueMapId", valueMapId)
                           .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return valueMap;
    }


    /**
     * return collection of all ValueMaps for a particular group
     * @param itemGroup of ValueMaps to find
     * @return all ValueMaps for a particular group
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_ValueMap> getbyGroup(String itemGroup) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_ValueMap> roles = new ArrayList<>();
        try{
            roles = em.createNamedQuery("Jstruct_ValueMap.findByItemGroup")
                      .setParameter("itemGroup", itemGroup)
                      .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return roles;
    }


    /**
     * get a ValueMap record based on it's itemKey
     * @param itemKey of ValueMap to find
     * @return ValueMap that matches itemKey
     * @throws JDAOException
     */
    @Override
    public Jstruct_ValueMap getByItemKey(String itemKey) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_ValueMap valueMap = null;
        try{
            valueMap = (Jstruct_ValueMap) em.createNamedQuery("Jstruct_ValueMap.findByItemKey")
                                    .setParameter("itemKey", itemKey)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return valueMap;
    }


    /**
     * get a collection of ValueMapGroup objects (each vmg object contains all ValueMap records for a group)
     * @return collection of ValueMapGroup objects
     * @throws JDAOException
     */
    @Override
    public List<ValueMapGroup> getValueMapGroups() throws JDAOException{
        
        List<ValueMapGroup> valueMapGroups = new ArrayList<>();

        List<String> groups = getDistinctGroups();
        for (String group: groups) {
            List<Jstruct_ValueMap> valueMaps = getbyGroup(group);
            ValueMapGroup vmg = new ValueMapGroup(valueMaps, group);
            valueMapGroups.add(vmg);
        }
        return valueMapGroups;
    }
    

    /**
     * get a map of ValueMap objects (key: valueMap.itemKey)
     * @return map of all valueMaps with itemKey as map key
     * @throws JDAOException
     
    @Override
    public Map<String, List<Jstruct_ValueMap>> getMapOfValueMaps() throws JDAOException {

        Map<String, List<Jstruct_ValueMap>> mapOfValueMaps = new LinkedHashMap<>();
       
        List<Jstruct_ValueMap> valueMaps = getAll();
        for(Jstruct_ValueMap valueMap : valueMaps){

            String itemGroup = valueMap.getItemGroup();

            if (mapOfValueMaps.containsKey(itemGroup)) {
                mapOfValueMaps.get(itemGroup).add(valueMap);
            } else {
                List<Jstruct_ValueMap> first = new ArrayList<>();
                first.add(valueMap);
                mapOfValueMaps.put(itemGroup, first);
            }
        }
        
        return mapOfValueMaps;
    }
    */
    


    /**
     * find a distinct list of groups in the ValueMap table
     * @return collection of groups as string (distinct values only)
     * @throws JDAOException
     */
    private List<String> getDistinctGroups() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<String> groups = new ArrayList<>();
        try {
            groups = em.createNamedQuery("Jstruct_ValueMap.distinctGroups")
                       .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return groups;
    }

    
}
