package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IClientDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Client;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ClientDAO  extends GenericDAO<Jstruct_Client, Long> implements IClientDAO, Serializable {
    
    
    
    
    /**
     * return collection of all Jstruct_Clients
     * @return all Jstruct_Clients
     * @throws JDAOException
     */
    @Override
    public List<Jstruct_Client> getAll() throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_Client> clients = new ArrayList<>();
        try{
            clients = em.createNamedQuery("Jstruct_Client.findAll").getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return clients;
    }


    /**
     * get a Jstruct_Client based on it's primary key
     * @param clientId of Jstruct_Client to find
     * @return Jstruct_Client that matches clientId
     * @throws JDAOException
     */
    @Override
    public Jstruct_Client getByClientId(Long clientId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_Client client = null;
        try{
            client = (Jstruct_Client) em.createNamedQuery("Jstruct_Client.findByClientId")
                                    .setParameter("clientId", clientId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return client;
    }


    /**
     * get a Jstruct_Client based on it's name
     * @param name of Jstruct_Client to find
     * @return Jstruct_Client that matches name
     * @throws JDAOException
     */
    @Override
    public Jstruct_Client getByName(String name) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_Client client = null;
        try{
            client = (Jstruct_Client) em.createNamedQuery("Jstruct_Client.findByName")
                                    .setParameter("name", name)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return client;
    }
    
    
    
    /**
     * determine if a client name already exists; use clientIdToIgnore if editing (or pass in null if new)
     * @param name
     * @param clientIdToIgnore
     * @return
     * @throws JDAOException 
     */
    @Override
    public boolean clientExists(String name, Long clientIdToIgnore) throws JDAOException {
        
        int matchesFound = 0; 
        EntityManager em = HibernateUtil.getEntityManager();
        
        try{
            
            Query q;
            if(clientIdToIgnore==null){
                q = em.createNamedQuery("Jstruct_Client.doesClientNameExist");
                q.setParameter("name", name.toUpperCase());
            } else {
                
                q = em.createNamedQuery("Jstruct_Client.doesClientNameExistIgnoreId");
                q.setParameter("name", name.toUpperCase());
                q.setParameter("clientId", clientIdToIgnore);
            }
            
            matchesFound = ((Long) q.getSingleResult()).intValue();
            
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return matchesFound > 0;
    }
    
    
    
    
    
    
    /**
     * determine if a client is used (e.g in the struct_version table)
     * @param clientId
     * @return
     * @throws JDAOException 
     */
    @Override
    public boolean isUsed(Long clientId) throws JDAOException{
        return true; //TODO update once struct_version table is implemented
    }




    
}
