package com.just.jstruct.dao.hibernate;

import com.hfg.sql.SQLUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.interfaces.ISeqResidueDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SeqResidue;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SeqResidueDAO implements ISeqResidueDAO, Serializable {
    
    
    
   

    /**
     * get a Jstruct_SeqResidue based on it's primary key
     * @param seqResidueId of Jstruct_SeqResidue to find
     * @return Jstruct_SeqResidue that matches seqResidueId
     * @throws JDAOException
     */
    @Override
    public Jstruct_SeqResidue getBySeqResidueId(Long seqResidueId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_SeqResidue seqResidue = null;
        try{
            seqResidue = (Jstruct_SeqResidue) em.createNamedQuery("Jstruct_SeqResidue.findBySeqResidueId")
                                    .setParameter("seqResidueId", seqResidueId)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return seqResidue;
    }

    
    
    
    
    /**
     * get all JstructSeqResidue objects by chainId
     * @param chainId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_SeqResidue> getByChainId(Long chainId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_SeqResidue> programs = new ArrayList<>();
        try{
            programs = em.createNamedQuery("Jstruct_SeqResidue.findByChainId")
                    .setParameter("chainId", chainId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return programs;
    }

    
    
    /**
     * delete all Jstruct_SeqResidue based on their chainId
     * @param chainId
     * @throws JDAOException 
     */
    @Override
    public void deleteByChainId(Long chainId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        try{
            em.getTransaction().begin();
            em.createNamedQuery("Jstruct_SeqResidue.deleteByChainId")
                                    .setParameter("chainId", chainId)
                                    .executeUpdate();
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
    }


    
    /**
     * delete all SeqResidues related to a struct_ver_id
     * @param structVerId
     * @throws JDAOException 
     */
    @Override
    public void deleteByStructVerId(Long structVerId) throws JDAOException{
        
        EntityManager em = HibernateUtil.getEntityManager();
        
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM seq_residue sr ");
        sql.append(" WHERE sr.chain_id IN ");
        sql.append("    ( ");
        sql.append("      SELECT c.chain_id FROM chain c ");
        sql.append("       WHERE c.struct_ver_id = :structVerId ");
        sql.append("    ) ");
        
        try {
 
            em.getTransaction().begin();
            em.createNativeQuery(sql.toString())
                                    .setParameter("structVerId", structVerId)
                                    .executeUpdate();
            em.getTransaction().commit();
                   
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
    }

    @Override
    public List<Jstruct_SeqResidue> bulkInsert(List<Jstruct_SeqResidue> seqResidues, Long chainId) throws JDAOException {
        
        if (!CollectionUtil.hasValues(seqResidues)) {
            return null;
        }

        List<Jstruct_SeqResidue> postInsertedSeqResidues = new ArrayList<>(seqResidues.size());

        EntityManager em = HibernateUtil.getEntityManager();
        Session hibernateSession = em.unwrap(Session.class);
        

        try {
            hibernateSession.getTransaction().begin();
            hibernateSession.doWork(new Work() {

                @Override
                public void execute(Connection conn) throws SQLException {
                   
                    PreparedStatement ps = conn.prepareStatement("INSERT INTO seq_residue ( "
                                                               + "  CHAIN_ID, "
                                                               + "  ORDINAL, "
                                                               + "  ABBREV, "
                                                               + "  RESIDUE_TYPE ) "
                                                               + "VALUES (?,?,?,?)");

                    for (Jstruct_SeqResidue seqResidue : seqResidues) {
                        SQLUtil.setLong(ps, 1, chainId);
                        SQLUtil.setInt(ps, 2, seqResidue.getOrdinal());
                        SQLUtil.setString(ps, 3, seqResidue.getAbbrev());
                        SQLUtil.setString(ps, 4, seqResidue.getResidueType());
                        ps.addBatch();
                    }
                    ps.executeBatch();
                    ps.close();
                    
                }

            });
            hibernateSession.getTransaction().commit();

            //get the just inserted residues
            postInsertedSeqResidues = getByChainId(chainId);

        } catch (HibernateException | JDAOException ex) {
            ex.printStackTrace();
            throw new JDAOException("Error bulk-inserting SeqResidues for Chain with id: " + chainId, ex);

        } finally {
            hibernateSession.close();
        }

        return postInsertedSeqResidues;
    }
    
    
    
    
    
}
