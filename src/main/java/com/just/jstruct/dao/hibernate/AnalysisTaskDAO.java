package com.just.jstruct.dao.hibernate;

import com.just.jstruct.dao.interfaces.IAnalysisTaskDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisTask;
import com.just.jstruct.utilities.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisTaskDAO extends GenericDAO<Jstruct_AnalysisTask, Long> implements IAnalysisTaskDAO, Serializable {
    
    

    /**
     * get a Jstruct_AnalysisTask based on it's primary key
     * @param id of Jstruct_AnalysisTask to find
     * @return Jstruct_AnalysisTask that matches id
     * @throws JDAOException
     */
    @Override
    public Jstruct_AnalysisTask getById(Long id) throws JDAOException 
    {
        EntityManager em = HibernateUtil.getEntityManager();
        Jstruct_AnalysisTask analysisTask = null;
        try
        {
            analysisTask = (Jstruct_AnalysisTask) em.createNamedQuery("Jstruct_AnalysisTask.findById")
                                    .setParameter("analysisTaskId", id)
                                    .getSingleResult();
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        return analysisTask;
    }

    
    
    
    /**
     * get all Jstruct_AnalysisTask objects by status
     * @param status
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisTask> getByStatus(String status) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisTask> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_AnalysisTask.findByStatus")
                    .setParameter("status", status)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }
    
    
    
    /**
     * get all Jstruct_AnalysisTask objects by analysisRunId
     * @param analysisRunId
     * @return
     * @throws JDAOException 
     */
    @Override
    public List<Jstruct_AnalysisTask> getByAnalysisRunId(Long analysisRunId) throws JDAOException {
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_AnalysisTask> residues = new ArrayList<>();
        try{
            residues = em.createNamedQuery("Jstruct_AnalysisTask.findByAnalysisRunId")
                    .setParameter("analysisRunId", analysisRunId)
                    .getResultList();
        } catch (Exception e) {
            throw new JDAOException(e);
        }finally{
            em.close();
        }
        return residues;
    }
    


    
}
