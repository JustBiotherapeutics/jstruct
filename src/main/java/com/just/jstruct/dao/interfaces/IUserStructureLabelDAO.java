package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_UserStructureLabel;
import java.util.List;



/**
 * interface for UserStructureLabelDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IUserStructureLabelDAO {

    
    
    public List<Jstruct_UserStructureLabel> getByUserIdAndStructureId(Long userId, Long structureId) throws JDAOException;
    
    public List<Jstruct_UserStructureLabel> getByUserId(Long userId) throws JDAOException;
    
    
    

    
}



