package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ValueMap;
import com.just.jstruct.pojos.ValueMapGroup;
import java.util.List;



/**
 * interface for ValueMapDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IValueMapDAO extends IGenericDAO<Jstruct_ValueMap, Long>{

    public List<Jstruct_ValueMap> getAll() throws JDAOException;
    
    public Jstruct_ValueMap getByValueMapId(Long valueMapId) throws JDAOException;

    public List<Jstruct_ValueMap> getbyGroup(String itemGroup) throws JDAOException;

    public Jstruct_ValueMap getByItemKey(String itemKey) throws JDAOException;

    public List<ValueMapGroup> getValueMapGroups() throws JDAOException;

}