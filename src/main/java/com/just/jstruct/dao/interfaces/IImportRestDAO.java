package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportRest;
import java.util.List;



/**
 * interface for ImportRestDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IImportRestDAO extends IGenericDAO<Jstruct_ImportRest, Long>{

    
    public Jstruct_ImportRest getByImportRestId(Long importRestId) throws JDAOException;
   
    public List<Jstruct_ImportRest> getByImportJobId(Long importJobId) throws JDAOException;
    
    
    
    
}



