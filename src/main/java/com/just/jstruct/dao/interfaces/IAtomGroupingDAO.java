package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AtomGrouping;
import java.util.List;



/**
 * interface for AtomGroupingDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IAtomGroupingDAO {

    
    
    public Jstruct_AtomGrouping getByAtomGroupingId(Long atomGroupingId) throws JDAOException;
    
    public List<Jstruct_AtomGrouping> getByChainId(Long chainId) throws JDAOException;
    
    public void deleteByChainId(Long chainId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
    public Jstruct_AtomGrouping insert(Jstruct_AtomGrouping atomGrouping) throws JDAOException;
    
}



