package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbKeyword;
import java.util.List;



/**
 * interface for PdbKeywordDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IPdbKeywordDAO extends IGenericDAO<Jstruct_PdbKeyword, Long> {

    public List<Jstruct_PdbKeyword> getByStructVerId(Long structVerId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
}



