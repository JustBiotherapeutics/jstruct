package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FileImportJob;
import java.util.List;



/**
 * interface for FileImportJobDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IFileImportJobDAO extends IGenericDAO<Jstruct_FileImportJob, Long> {

    
    public Jstruct_FileImportJob getByFileImportJobId(Long fileImportJobId) throws JDAOException;
    
    public List<Jstruct_FileImportJob> getByImportJobId(Long importJobId) throws JDAOException;
    
    
    
    
}



