package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SequencesAtom;
import java.util.List;



/**
 * interface for SequencesAtomDAO 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface ISequencesAtomDAO {

    
    public List<Jstruct_SequencesAtom> getByStructVerIds(List<Long> structVerIds) throws JDAOException;
    
    public List<Long> getStructVerIds() throws JDAOException;
    
}



