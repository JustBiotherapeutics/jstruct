package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Structure;



/**
 * interface for StructureDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IStructureDAO extends IGenericDAO<Jstruct_Structure, Long>{

    
    //public List<Jstruct_Structure> getAll() throws JDAOException;
    
    public Jstruct_Structure getByStructureId(Long structureId) throws JDAOException;
    
    public Integer getFileCount(boolean fromRcsb, boolean isObsolete) throws JDAOException;
    
    
}



