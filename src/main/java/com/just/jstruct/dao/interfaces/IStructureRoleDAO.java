package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureRole;
import java.util.List;

/**
 * interface for StructureRoleDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IStructureRoleDAO extends IGenericDAO<Jstruct_StructureRole, Long> {

    
    public Jstruct_StructureRole getByStructureRoleId(Long structureRoleId) throws JDAOException;
    
    public Jstruct_StructureRole getByStructureIdUserId(Long structureId, Long userId) throws JDAOException;
    
    public List<Jstruct_StructureRole> getByStructureId(Long structureId) throws JDAOException;
    
    
    
    public List<Long> getReadStructureIdsForUser(Long userId) throws JDAOException;
    
    public List<Long> getWriteStructureIdsForUser(Long userId) throws JDAOException;
    
    public List<Long> getGrantStructureIdsForUser(Long userId) throws JDAOException;
    
    
}
