package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportMsg;
import java.util.List;



/**
 * interface for ImportMsgDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IImportMsgDAO extends IGenericDAO<Jstruct_ImportMsg, Long>{

    
    public Jstruct_ImportMsg getByImportMsgId(Long importMsgId) throws JDAOException;
   
    public List<Jstruct_ImportMsg> getByImportJobId(Long importJobId) throws JDAOException;
    
    
    
    
}



