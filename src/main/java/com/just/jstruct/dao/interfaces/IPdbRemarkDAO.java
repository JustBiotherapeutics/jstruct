package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbRemark;
import java.util.List;



/**
 * interface for PdbRemarkDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IPdbRemarkDAO extends IGenericDAO<Jstruct_PdbRemark, Long> {

    public List<Jstruct_PdbRemark> getByStructVerId(Long structVerId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
}



