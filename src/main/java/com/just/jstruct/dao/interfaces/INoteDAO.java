package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Note;
import java.util.List;

/**
 * interface for StructureUserDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface INoteDAO extends IGenericDAO<Jstruct_Note, Long> {


    public Jstruct_Note getByNoteId(Long noteId) throws JDAOException;

    public List<Jstruct_Note> getByStructureIdForUser(Long structureId, Long userId) throws JDAOException;

    public List<Jstruct_Note> getByStructVerIdForUser(Long structVerId, Long userId) throws JDAOException;

    
}
