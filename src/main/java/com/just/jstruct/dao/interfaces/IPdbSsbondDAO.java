package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbSsbond;
import java.util.List;



/**
 * interface for PdbSsbondDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IPdbSsbondDAO extends IGenericDAO<Jstruct_PdbSsbond, Long> {

    public List<Jstruct_PdbSsbond> getByStructVerId(Long structVerId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
}



