package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbRevdat;
import java.util.List;



/**
 * interface for PdbRevdatDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IPdbRevdatDAO extends IGenericDAO<Jstruct_PdbRevdat, Long> {

    public List<Jstruct_PdbRevdat> getByStructVerId(Long structVerId) throws JDAOException;
    
    public Jstruct_PdbRevdat getInitialReleaseByStructVerId(Long structVerId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
}



