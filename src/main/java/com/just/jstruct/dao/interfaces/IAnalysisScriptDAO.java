package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisScript;
import java.util.List;



/**
 * interface for AnalysisScriptDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IAnalysisScriptDAO extends IGenericDAO<Jstruct_AnalysisScript, Long>
{
    
    public Jstruct_AnalysisScript getById(Long id) throws JDAOException;
    
    public List<Jstruct_AnalysisScript> getByOwnerId(Long ownerId, boolean includeDeleted) throws JDAOException;
    
    public List<Jstruct_AnalysisScript> getAllMarkedAsGlobal(boolean includeDeleted) throws JDAOException;
    
}



