package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Program;
import java.util.List;



/**
 * interface for ProgramDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IProgramDAO extends IGenericDAO<Jstruct_Program, Long>{

    
    public List<Jstruct_Program> getAll() throws JDAOException;
    
    public Jstruct_Program getByProgramId(Long programId) throws JDAOException;
    
    public Jstruct_Program getByName(String name) throws JDAOException;
    
    public boolean programExists(String name, Long programIdToIgnore) throws JDAOException;
    
    public boolean isUsed(Long programId) throws JDAOException;

    
}



