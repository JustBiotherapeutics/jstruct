package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AminoAcid;
import java.util.List;



/**
 * interface for AminoAcidDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IAminoAcidDAO extends IGenericDAO<Jstruct_AminoAcid, Long>{

    public List<Jstruct_AminoAcid> getAll() throws JDAOException;
    
    public List<Jstruct_AminoAcid> getByAminoAcidIds(List<Integer> aminoAcidIds) throws JDAOException;
    
    public Jstruct_AminoAcid getByAminoAcidId(Long aminoAcidId) throws JDAOException;
    
    public Jstruct_AminoAcid getByThreeLetter(String threeLetterAbbrev) throws JDAOException;

}