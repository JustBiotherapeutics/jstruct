package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_PdbLineage;
import com.just.jstruct.model.Jstruct_User;
import java.util.List;

/**
 * interface for PdbLineageDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IPdbLineageDAO extends IGenericDAO<Jstruct_PdbLineage, Long> {
    
    

    public List<Jstruct_PdbLineage> getAllByPdbPredecessorId(String pdbParentId) throws JDAOException;

    public List<Jstruct_PdbLineage> getAllByPdbDescendantId(String pdbChildId) throws JDAOException;

    public Jstruct_PdbLineage getByPdbPredecessorIdAndPdbDescendantId(String pdbPredecessorId, String pdbDescendantId) throws JDAOException;

    public String getCurrentBasedOnPdbIdentifier(String pbdIdentifier, boolean ifCurrentIsObsoleteGetMostRecent) throws JDAOException;

    public List<Jstruct_FullStructure> getFullStructureLineageBasedOnPdbIdentifier(String pbdIdentifier, Jstruct_User currentUser) throws JDAOException;
    
    
    
}
