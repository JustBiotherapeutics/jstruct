package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SearchParam;
import java.util.List;



/**
 * interface for SearchParamDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface ISearchParamDAO extends IGenericDAO<Jstruct_SearchParam, Long> {

    public Jstruct_SearchParam getBySearchParamId(Long searchParamId) throws JDAOException;
    
    public List<Jstruct_SearchParam> getBySearchSubqueryId(Long searchSubqueryId) throws JDAOException;
    
    
}



