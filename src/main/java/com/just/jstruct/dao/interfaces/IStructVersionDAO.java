package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructVersion;
import java.util.List;



/**
 * interface for StructVersionDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IStructVersionDAO extends IGenericDAO<Jstruct_StructVersion, Long>{

    
    public Jstruct_StructVersion getByStructVerId(Long structVerId) throws JDAOException;
    
    public Jstruct_StructVersion getYoungestByStructureId(Long structureId) throws JDAOException;
    
    public List<Long> getAllStructVerIds() throws JDAOException;

    public List<Long> getManualStructVerIds() throws JDAOException;
    
   
    public List<Long> getActiveRcsbStructVerIds() throws JDAOException;
    
    public List<Long> getActiveManualModelStructVerIds() throws JDAOException;
    
    public List<Long> getActiveManualNonModelStructVerIds() throws JDAOException;
    
    
    
    
    
}



