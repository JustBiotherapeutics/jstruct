package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisRun;
import java.util.List;



/**
 * interface for AnalysisRunDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IAnalysisRunDAO extends IGenericDAO<Jstruct_AnalysisRun, Long>
{
    
    public Jstruct_AnalysisRun getById(Long id) throws JDAOException;
    
    public List<Jstruct_AnalysisRun> getByStatus(String status) throws JDAOException;
    
    public List<Jstruct_AnalysisRun> getByAnalysisScriptVerId(Long analysisScriptVerId) throws JDAOException;
    
}



