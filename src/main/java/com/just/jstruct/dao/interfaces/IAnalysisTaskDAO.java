package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisTask;
import java.util.List;



/**
 * interface for AnalysisTaskDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IAnalysisTaskDAO extends IGenericDAO<Jstruct_AnalysisTask, Long>
{
    
    public Jstruct_AnalysisTask getById(Long id) throws JDAOException;
    
    public List<Jstruct_AnalysisTask> getByStatus(String status) throws JDAOException;
    
    public List<Jstruct_AnalysisTask> getByAnalysisRunId(Long analysisRunId) throws JDAOException;
    
}



