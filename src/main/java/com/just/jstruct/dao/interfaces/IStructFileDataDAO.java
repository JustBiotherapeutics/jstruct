package com.just.jstruct.dao.interfaces;

import com.just.bio.structure.Structure;
import com.just.bio.structure.exceptions.StructureParseException;
import com.just.jstruct.exception.JDAOException;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;



/**
 * interface for StructFileDataDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IStructFileDataDAO  {

 
    
    
    
    public Structure getAsStructure(Long structVerId) throws JDAOException, SQLException, IOException, StructureParseException;
    
    
    
    public void saveDataFromFile(File file, Long structVerId) throws SQLException, IOException;
    
    public void getData(Long structVerId, OutputStream outputStream) throws SQLException, IOException;
    
    public void getCompressedData(Long structVerId, OutputStream outputStream) throws SQLException, IOException;
    
            
}



