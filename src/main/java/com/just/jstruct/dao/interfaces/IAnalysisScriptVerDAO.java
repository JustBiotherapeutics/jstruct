package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisScriptVer;
import java.util.List;



/**
 * interface for AnalysisScriptVerDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IAnalysisScriptVerDAO extends IGenericDAO<Jstruct_AnalysisScriptVer, Long>
{
    
    public Jstruct_AnalysisScriptVer getById(Long id) throws JDAOException;
    
    public List<Jstruct_AnalysisScriptVer> getByStatus(String status) throws JDAOException;
    
    public List<Jstruct_AnalysisScriptVer> getByOwnerId(Long ownerId) throws JDAOException;
    
    public List<Jstruct_AnalysisScriptVer> getByAnalysisScriptId(Long analysisScriptId) throws JDAOException;
    
}



