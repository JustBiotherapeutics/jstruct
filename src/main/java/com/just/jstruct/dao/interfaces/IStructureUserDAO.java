package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureUser;
import java.util.List;

/**
 * interface for StructureUserDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IStructureUserDAO extends IGenericDAO<Jstruct_StructureUser, Long> {

    
    public List<Jstruct_StructureUser> getFavoritesByUserId(Long userId) throws JDAOException;

    public Jstruct_StructureUser getFavoriteByStructureIdUserId(Long structureId, Long userId) throws JDAOException;


    
    public List<Jstruct_StructureUser> getRecentsByUserId(Long userId, Integer maxCountReturned) throws JDAOException;
    
    public Jstruct_StructureUser getRecentByStructVerIdUserId(Long structVerId, Long userId) throws JDAOException;
    

   
    
    
}
