package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbFile;



/**
 * interface for PdbFileDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IPdbFileDAO extends IGenericDAO<Jstruct_PdbFile, Long> {

    
    public Jstruct_PdbFile getByStructVerId(Long structVerId) throws JDAOException;
    
    

    
}



