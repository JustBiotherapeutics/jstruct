package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_User;
import java.util.List;



/**
 * interface for JstructUserDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IJstructUserDAO extends IGenericDAO<Jstruct_User, Long>{

    
    public List<Jstruct_User> getAll() throws JDAOException;
    
    public Jstruct_User getByUserId(Long userId) throws JDAOException;
    
    public Jstruct_User getByUid(String uid) throws JDAOException;
    
    public Jstruct_User getSystemUser() throws JDAOException;
    
    public Jstruct_User getGuestUser() throws JDAOException;
    
    public List<Jstruct_User> getUsersByGeneralCriteria(String criteria) throws JDAOException;


    public List<Jstruct_User> getActiveRoleDevelopers() throws JDAOException;

    public List<Jstruct_User> getActiveRoleAdmins() throws JDAOException;

    public List<Jstruct_User> getActiveRoleContributers() throws JDAOException;
    
}



