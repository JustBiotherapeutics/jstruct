package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Chain;
import java.util.List;



/**
 * interface for ChainDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IChainDAO {

    
    public Jstruct_Chain getByChainId(Long chainId) throws JDAOException;
    
    public List<Jstruct_Chain> getByStructVerId(Long structVerId) throws JDAOException;
    
    public List<Jstruct_Chain> getByStructVerIds(List<Long> structVerIds) throws JDAOException;
    
    public Integer getCountByStructVerId(Long structVerId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;

    public Jstruct_Chain insert(Jstruct_Chain chain) throws JDAOException;
    
    
    
}



