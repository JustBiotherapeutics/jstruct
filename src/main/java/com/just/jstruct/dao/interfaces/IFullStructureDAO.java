package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import java.util.List;



/**
 * interface for FullStructureDao 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IFullStructureDAO {
    
    public Jstruct_FullStructure getByStructVerId(Long structVerId) throws JDAOException;
    
    public List<Jstruct_FullStructure> getByStructVerIds(List<Long> structVerIds) throws JDAOException;
    
    public List<Jstruct_FullStructure> getByStructureId(Long structureId) throws JDAOException;
   
    public Jstruct_FullStructure getByStructureIdAndVersion(Long structureId, Integer version, boolean getCurrent) throws JDAOException;
    
    public Jstruct_FullStructure getRcsbImportedRowByPbdIdentifier(String pdbIdentifier, boolean jumpToCurrent) throws JDAOException;
    
    public List<String> getPdbIdsFromRcsbRun(boolean includeActive, boolean includeObsolete) throws JDAOException;
    
    public List<Jstruct_FullStructure> getByPdbIds(List<String> pdbIds, boolean onlyFromRcsb) throws JDAOException;
    
    public Jstruct_FullStructure getRandomActiveRcsbFullStructure() throws JDAOException;
    
    public List<Jstruct_FullStructure> getFullStructureLineageBasedOnPdbIdentifier(String inPbdIdentifier) throws JDAOException;
    
}



