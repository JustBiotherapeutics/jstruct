package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Residue;
import java.util.List;



/**
 * interface for ResidueDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IResidueDAO {

    
    public Jstruct_Residue getByResidueId(Long residueId) throws JDAOException;
    
    public List<Jstruct_Residue> getByAtomGroupingId(Long atomGroupingId) throws JDAOException;
    
    public void deleteByAtomGroupingId(Long atomGroupingId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
    public List<Jstruct_Residue> bulkInsert(List<Jstruct_Residue> residues, Long atomGroupingId) throws JDAOException;
}



