package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Client;
import java.util.List;



/**
 * interface for ClientDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IClientDAO extends IGenericDAO<Jstruct_Client, Long>{

    
    public List<Jstruct_Client> getAll() throws JDAOException;
    
    public Jstruct_Client getByClientId(Long clientId) throws JDAOException;
    
    public Jstruct_Client getByName(String name) throws JDAOException;
    
    public boolean clientExists(String name, Long clientIdToIgnore) throws JDAOException;
    
    public boolean isUsed(Long clientId) throws JDAOException;

    
}



