package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SearchSubquery;
import java.util.List;



/**
 * interface for SearchSubqueryDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface ISearchSubqueryDAO extends IGenericDAO<Jstruct_SearchSubquery, Long> {

    public Jstruct_SearchSubquery getBySearchSubqueryId(Long searchSubqueryId) throws JDAOException;
    
    public List<Jstruct_SearchSubquery> getByUserSearchId(Long userSearchId) throws JDAOException;
    
    
}



