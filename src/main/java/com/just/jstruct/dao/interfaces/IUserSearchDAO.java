package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_UserSearch;
import java.util.List;



/**
 * interface for UserSearchDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IUserSearchDAO extends IGenericDAO<Jstruct_UserSearch, Long> {

    public Jstruct_UserSearch getByUserSearchId(Long userSearchId) throws JDAOException;
    
    public List<Jstruct_UserSearch> getByUserId(Long userId) throws JDAOException;
    
    
}



