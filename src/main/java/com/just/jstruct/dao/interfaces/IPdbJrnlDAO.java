package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbJrnl;
import java.util.List;



/**
 * interface for PdbJrnlDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IPdbJrnlDAO extends IGenericDAO<Jstruct_PdbJrnl, Long> {

    public List<Jstruct_PdbJrnl> getByStructVerId(Long structVerId) throws JDAOException;
    
    public Jstruct_PdbJrnl getPrimaryByStructVerId(Long structVerId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
}



