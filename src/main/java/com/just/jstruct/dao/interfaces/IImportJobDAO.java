package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportJob;
import java.util.List;



/**
 * interface for ImportJobDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IImportJobDAO extends IGenericDAO<Jstruct_ImportJob, Long>{

    
    public List<Jstruct_ImportJob> getAll() throws JDAOException;
    
    public Jstruct_ImportJob getByImportJobId(Long importJobId, boolean includeImportRests, boolean includeImportMsgs) throws JDAOException;
    
    public List<Jstruct_ImportJob> getAllRcsbImportJobs() throws JDAOException;
    
    public Jstruct_ImportJob getMostRecentSuccessfulRcsbRun() throws JDAOException;
    
    public List<Jstruct_ImportJob> getOnlyInitialRcsbRuns() throws JDAOException;
    
    public List<Jstruct_ImportJob> getOnlySuccessfulInitialRcsbRuns() throws JDAOException;
    
    
}



