package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_UserLabel;
import java.util.List;



/**
 * interface for UserLabelDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IUserLabelDAO extends IGenericDAO<Jstruct_UserLabel, Long>{

    
    
    public Jstruct_UserLabel getByUserLabelId(Long userLabelId) throws JDAOException;
    
    public List<Jstruct_UserLabel> getByUserId(Long userId) throws JDAOException;
    
    public List<Jstruct_UserLabel> getByUserIdStructureId(Long userId, Long structureId) throws JDAOException;
    
    public List<Jstruct_UserLabel> getUnusedByUserId(Long userId) throws JDAOException;
    
    

    
}



