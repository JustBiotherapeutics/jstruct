package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbCompnd;
import java.util.List;



/**
 * interface for PdbCompndDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IPdbCompndDAO extends IGenericDAO<Jstruct_PdbCompnd, Long> {

    public List<Jstruct_PdbCompnd> getByStructVerId(Long structVerId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
}



