package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SeqResidue;
import java.util.List;



/**
 * interface for SeqResidueDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface ISeqResidueDAO {

    
    
    public Jstruct_SeqResidue getBySeqResidueId(Long seqResidueId) throws JDAOException;
    
    public List<Jstruct_SeqResidue> getByChainId(Long chainId) throws JDAOException;
    
    public void deleteByChainId(Long chainId) throws JDAOException;
    
    public void deleteByStructVerId(Long structVerId) throws JDAOException;
    
    public List<Jstruct_SeqResidue> bulkInsert(List<Jstruct_SeqResidue> seqResidues, Long chainId) throws JDAOException;
    
}



