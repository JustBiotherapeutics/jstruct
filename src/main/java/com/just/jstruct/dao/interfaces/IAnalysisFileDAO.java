package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisFile;
import java.util.List;



/**
 * interface for AnalysisFileDAO
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IAnalysisFileDAO extends IGenericDAO<Jstruct_AnalysisFile, Long>
{
    
    public Jstruct_AnalysisFile getById(Long id) throws JDAOException;
    
    public List<Jstruct_AnalysisFile> getByAnalysisScriptVerId(Long analysisScriptVerId) throws JDAOException;
    
}



