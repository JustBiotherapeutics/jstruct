package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureLabel;



/**
 * interface for StructureLabelDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IStructureLabelDAO extends IGenericDAO<Jstruct_StructureLabel, Long>{

    
    
    public Jstruct_StructureLabel getByStructureLabelId(Long structureLabelId) throws JDAOException;
    
    public Jstruct_StructureLabel getByUserLabelIdStructureId(Long userLabelId, Long structureId) throws JDAOException;
    
    public void deleteByUserAndStructure(Long userId, Long structureId) throws JDAOException; 
    

    
}



