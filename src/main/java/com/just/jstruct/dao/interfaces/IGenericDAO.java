package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @param <T>
 * @param <ID>
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IGenericDAO <T, ID extends Serializable> {
    
    T add(T entity) throws JDAOException;
    
    public List<T> batchInsert(List<T> entities) throws JDAOException;
    
    T update(T entity) throws JDAOException;
    
    void delete(T entity) throws JDAOException;
    
}
