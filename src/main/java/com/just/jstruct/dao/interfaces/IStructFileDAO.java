package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructFile;
import java.util.List;



/**
 * interface for StructFileDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IStructFileDAO extends IGenericDAO<Jstruct_StructFile, Long> {

    
    public Jstruct_StructFile getByStructVerId(Long structVerId) throws JDAOException;
    
    public List<Jstruct_StructFile> getByStructVerIds(List<Long> structVerIds) throws JDAOException;
    
    
    
 
    
            
}



