package com.just.jstruct.dao.interfaces;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportPdb;
import java.util.List;



/**
 * interface for ImportPdbDao
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface IImportPdbDAO extends IGenericDAO<Jstruct_ImportPdb, Long>{

    
    public Jstruct_ImportPdb getByImportPdbId(Long importPdbId) throws JDAOException;
   
    public List<Jstruct_ImportPdb> getByImportRestId(Long importRestId) throws JDAOException;
   
    public List<Jstruct_ImportPdb> getByImportJobId(Long importJobId) throws JDAOException;
    
    public List<Jstruct_ImportPdb> bulkInsert(List<Jstruct_ImportPdb> importPdbs, Long importRestId) throws JDAOException;
    
    
}



