package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.SequencesSeqresDAO;
import com.just.jstruct.dao.interfaces.ISequencesSeqresDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SequencesSeqres;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SequencesSeqresService implements Serializable {


    /* dao classes */
    
    private final ISequencesSeqresDAO sequencesSeqresDao;


    
    
    /* constructor */
    
    public SequencesSeqresService() {
        sequencesSeqresDao = new SequencesSeqresDAO();
    }


    
    /**
     * 
     * @param structVerIds
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_SequencesSeqres> getByStructVerIds(List<Long> structVerIds) throws JDAOException {
        return sequencesSeqresDao.getByStructVerIds(structVerIds);
    }
    
    
    /**
     * list of all structVerIds for structures that have SEQRES data
     * @return
     * @throws JDAOException 
     */
    public List<Long> getStructVerIds() throws JDAOException {
        return sequencesSeqresDao.getStructVerIds();
    }
    
    
    
}
