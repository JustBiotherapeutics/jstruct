package com.just.jstruct.dao.service;

import com.just.bio.structure.AminoAcid3D;
import com.just.bio.structure.ElementMap;
import com.just.bio.structure.format.pdb.PDB_AtomName;
import com.just.jstruct.dao.hibernate.AminoAcidDAO;
import com.just.jstruct.dao.interfaces.IAminoAcidDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AminoAcidService implements Serializable {


    /* dao classes */
    
    private final IAminoAcidDAO aminoAcidDao;


    
    
    /* constructor */
    
    public AminoAcidService() {
        aminoAcidDao = new AminoAcidDAO();
    }


    private HashMap<String, Character> naMap;
    private List<PDB_AtomName> mBkbnAtoms;
    private List<PDB_AtomName> mBkbnCbAtoms;
    
    
    public List<AminoAcid3D> getAll() {
        ElementMap elementMap = ElementMap.getInstance();
        return new ArrayList(elementMap.getAaMap().values());
    }
    
    public HashMap<String, Character> getNucleicAcidMap(){
        ElementMap elementMap = ElementMap.getInstance();
        return elementMap.getNaMap();
    }
    
    public List<PDB_AtomName> getBkbnAtoms(){
        ElementMap elementMap = ElementMap.getInstance();
        return elementMap.getBkbnAtoms();
    }
    
    public List<PDB_AtomName> getBkbnCbAtoms(){
        ElementMap elementMap = ElementMap.getInstance();
        return elementMap.getBkbnCbAtoms();
    }
    
    
    


    /**
     * return List of all AminoAcids
     * @return all AminoAcids
     * @throws JDAOException
     * /
    public List<Jstruct_AminoAcid> getAll() throws JDAOException {
        return aminoAcidDao.getAll();
    }

    
    /**
     * return all amino acids that have an id in the passed in list
     * @param aminoAcidIds 
     * @return all AminoAcids that match
     * @throws JDAOException
     * /
    public List<Jstruct_AminoAcid> getAllByIds(List<Integer> aminoAcidIds) throws JDAOException {
        return aminoAcidDao.getByAminoAcidIds(aminoAcidIds);
    }


    /**
     * get a AminoAcid based on it's primary key
     * @param aminoAcidId of AminoAcid to find
     * @return AminoAcid that matches aminoAcidId
     * @throws JDAOException
     * /
    public Jstruct_AminoAcid getById(Long aminoAcidId) throws JDAOException {
        return aminoAcidDao.getByAminoAcidId(aminoAcidId);
    }

    
    
    /**
     * get a AminoAcid based on it's threeLetter abbreviation
     * @param threeLetterAbbrev  of AminoAcid to find
     * @return AminoAcid that matches threeLetter
     * @throws JDAOException
     * /
    public Jstruct_AminoAcid getByThreeLetter(String threeLetterAbbrev) throws JDAOException {
        return aminoAcidDao.getByThreeLetter(threeLetterAbbrev);
    }

    
    
    private static HashMap<String, AminoAcid> CACHED_AMINO_ACID_MAP = null;
   
    /**
     * @return a cached map of all the amino acids - key is three letter abbreviation
     * @throws JDAOException 
     * /
    public HashMap<String, AminoAcid> getAminoAcidMap() throws JDAOException {
        
        if(CACHED_AMINO_ACID_MAP == null) {
            
            CACHED_AMINO_ACID_MAP = new HashMap<>();
             List<Jstruct_AminoAcid> aminoAcids = aminoAcidDao.getAll();
             for(Jstruct_AminoAcid aa : aminoAcids){
                 String[] atoms = aa.getAtoms().split(",");
                 CACHED_AMINO_ACID_MAP.put(aa.getAbbrev(), new AminoAcid(aa.getOneLetter(), aa.getAbbrev(), aa.isStandard(), Arrays.asList(atoms)));
             }
            
        }
        
        return CACHED_AMINO_ACID_MAP;
        
    }

  
    /**
     * update a AminoAcid in the database
     * @param aminoAcid to update
     * @param user
     * @return the updated aminoAcid from the database
     * @throws JDAOException
     * /
    public Jstruct_AminoAcid updateAminoAcid(Jstruct_AminoAcid aminoAcid, Jstruct_User user) throws JDAOException {
        
        aminoAcid.setUpdateUser(user);
        aminoAcid.setUpdateDate(new Date());
        
        Jstruct_AminoAcid updatedAminoAcid = aminoAcidDao.update(aminoAcid);
        
        return updatedAminoAcid;
    }
    
    
    /**
     * add a new amino acid to the database
     * @param aminoAcid to add
     * @return the brand new amino acid
     * @throws JDAOException 
     * /
    public Jstruct_AminoAcid addAminoAcid(Jstruct_AminoAcid aminoAcid, Jstruct_User user) throws JDAOException {
        
        aminoAcid.setInsertDate(new Date());
        aminoAcid.setUpdateDate(new Date());
        
        aminoAcid.setInsertUser(user);
        aminoAcid.setUpdateUser(user);
        
        Jstruct_AminoAcid updatedAminoAcid = aminoAcidDao.add(aminoAcid);
        return updatedAminoAcid; 
    }
    */
    
}
