package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.StructureLabelDAO;
import com.just.jstruct.dao.interfaces.IStructureLabelDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureLabel;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserLabel;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureLabelService implements Serializable {


    /* dao classes */
    
    private final IStructureLabelDAO structureLabelDao;


    
    
    /* constructor */
    
    public StructureLabelService() {
        structureLabelDao = new StructureLabelDAO();
    }


    
    /**
     * get a StructureLabel based on it's primary key
     * @param structureLabelId of StructureLabel to find
     * @return structureLabel that matches structureLabelId
     * @throws JDAOException
     */
    public Jstruct_StructureLabel getByStructureLabelId(Long structureLabelId) throws JDAOException {
        return structureLabelDao.getByStructureLabelId(structureLabelId);
    }
    
    
    /**
     * 
     * @param userLabelId
     * @param structureLabelId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructureLabel getByUserLabelIdStructureId(Long userLabelId, Long structureLabelId) throws JDAOException{
        return structureLabelDao.getByUserLabelIdStructureId(userLabelId, structureLabelId);
    }
    
    
   
    /**
     * add a new structureLabel to the database 
     * @param structureLabel to add
     * @param user
     * @return the brand new structureLabel
     * @throws JDAOException 
     */
    public Jstruct_StructureLabel add(Jstruct_StructureLabel structureLabel, Jstruct_User user) throws JDAOException {

        structureLabel.setInsertUser(user);
        structureLabel.setInsertDate(new Date());
        
        Jstruct_StructureLabel updatedStructureLabel = structureLabelDao.add(structureLabel);
        return updatedStructureLabel; 
    }
    
    
    
    /**
     * 
     * @param userId
     * @param structureId
     * @throws JDAOException 
     */
    public void deleteByUserAndStructure(Long userId, Long structureId) throws JDAOException {
        structureLabelDao.deleteByUserAndStructure(userId, structureId);
    }
    
    
    
    
    
    /**
     * 
     * @param user
     * @param structureId
     * @param userLabels
     * @throws JDAOException 
     */
    public void replaceStructureLabelsForUserAndStructure(Jstruct_User user, Long structureId, List<Jstruct_UserLabel> userLabels) throws JDAOException {
        
        //first, delete all the structureLabels for this structure/user
        deleteByUserAndStructure(user.getUserId(), structureId);
        
        //create and save structureLables for each userLabel passed in
        for(Jstruct_UserLabel userLabel : userLabels){
            
            Jstruct_StructureLabel structureLabel = new Jstruct_StructureLabel();
            structureLabel.setInsertUser(user);
            structureLabel.setInsertDate(new Date());
            structureLabel.setStructureId(structureId);
            structureLabel.setUserLabelId(userLabel.getUserLabelId());
            
            structureLabelDao.add(structureLabel);
            
        }
        
    }
    
    
    /**
     * add all user labels to the structure that do not already exist
     * @param user
     * @param structureId
     * @param userLabels
     * @throws JDAOException 
     */
    public void addStructureLabelsForStructure(Jstruct_User user, Long structureId, List<Jstruct_UserLabel> userLabels) throws JDAOException {
        
        if(CollectionUtil.hasValues(userLabels))
        {
            for(Jstruct_UserLabel userLabel : userLabels)
            {
                Jstruct_StructureLabel foundStructureLabel = getByUserLabelIdStructureId(userLabel.getUserLabelId(), structureId);
                if(foundStructureLabel == null)
                {
                    Jstruct_StructureLabel structureLabel = new Jstruct_StructureLabel();
                    structureLabel.setInsertUser(user);
                    structureLabel.setInsertDate(new Date());
                    structureLabel.setStructureId(structureId);
                    structureLabel.setUserLabelId(userLabel.getUserLabelId());

                    structureLabelDao.add(structureLabel);
                }
            }
        }
    }
    
    
    
    
    
    
    
    
 
    
    
}
