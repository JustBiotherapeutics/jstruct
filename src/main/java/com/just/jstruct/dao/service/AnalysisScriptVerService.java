package com.just.jstruct.dao.service;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.AnalysisScriptVerDAO;
import com.just.jstruct.dao.interfaces.IAnalysisScriptVerDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisFile;
import com.just.jstruct.model.Jstruct_AnalysisRun;
import com.just.jstruct.model.Jstruct_AnalysisScriptVer;
import com.just.jstruct.model.Jstruct_User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisScriptVerService implements Serializable {

    /* -------------------------------------------------------------------------
        dao classes , service methods
    ------------------------------------------------------------------------- */
    
    private final IAnalysisScriptVerDAO analysisScriptVerDao;
    
    private final AnalysisRunService analysisRunService = new AnalysisRunService();
    private final AnalysisFileService analysisFileService = new AnalysisFileService();


    /* -------------------------------------------------------------------------
        constructor 
    ------------------------------------------------------------------------- */
    
    public AnalysisScriptVerService() {
        analysisScriptVerDao = new AnalysisScriptVerDAO();
    }


    
    /* -------------------------------------------------------------------------
        methods 
    ------------------------------------------------------------------------- */
    
    /**
     * add a new AnalysisScriptVer to the database
     * @param analysisScriptVer to add
     * @param user
     * @return the newly persisted analysisScriptVer
     * @throws JDAOException 
     */
    public Jstruct_AnalysisScriptVer add(Jstruct_AnalysisScriptVer analysisScriptVer, Jstruct_User user) throws JDAOException 
    {
        analysisScriptVer.setInsertUser(user); 
        analysisScriptVer.setOwner(user); 
        
        if(analysisScriptVer.getInsertDate() == null){
            analysisScriptVer.setInsertDate(new Date()); 
        }
        
        if(!StringUtil.isSet(analysisScriptVer.getStatus())){
            analysisScriptVer.setStatus(Jstruct_AnalysisScriptVer.STATUS_ACTIVE);
        }
        
        if(analysisScriptVer.getVersion() <= 0){
            analysisScriptVer.setVersion(1);
        }
        
        Jstruct_AnalysisScriptVer newAnalysisScriptVer = analysisScriptVerDao.add(analysisScriptVer);
        return newAnalysisScriptVer; 
    }
    
    
    /**
     * update a AnalysisScriptVer in the database
     * @param analysisScriptVer to update
     * @return the updated analysisScriptVer from the database
     * @throws JDAOException
     */
    public Jstruct_AnalysisScriptVer update(Jstruct_AnalysisScriptVer analysisScriptVer) throws JDAOException 
    {
        Jstruct_AnalysisScriptVer updatedAnalysisScriptVer = analysisScriptVerDao.update(analysisScriptVer);
        return updatedAnalysisScriptVer;
    }
    
    
    
    
    /**
     * get a AnalysisScriptVer based on it's primary key
     * @param id of AnalysisScriptVer to find
     * @param includeRuns
     * @param includeFiles
     * @return AnalysisScriptVer that matches id
     * @throws JDAOException
     */
    public Jstruct_AnalysisScriptVer getById(Long id, boolean includeRuns, boolean includeFiles) throws JDAOException 
    {
        Jstruct_AnalysisScriptVer scriptVer = analysisScriptVerDao.getById(id);
        
        if(includeRuns)
        {
            scriptVer = retrieveRuns(scriptVer);
        }
        
        if(includeFiles){
            scriptVer = retrieveFiles(scriptVer);
        }
        
        return scriptVer;
    }
    
    
    
    
    
    
    /**
     * return collection of all AnalysisScriptVer for a ownerId
     * @param ownerId
     * @param includeRuns
     * @param includeFiles
     * @return all AnalysisScriptVer based on ownerId
     * @throws JDAOException
     */
    public List<Jstruct_AnalysisScriptVer> getByOwnerId(Long ownerId, boolean includeRuns, boolean includeFiles) throws JDAOException 
    {
        List<Jstruct_AnalysisScriptVer> scriptVers = analysisScriptVerDao.getByOwnerId(ownerId);
        
        if(includeRuns) 
        {
            scriptVers = retrieveRuns(scriptVers);
        }
        
        if(includeFiles){
            scriptVers = retrieveFiles(scriptVers);
        }
        
        return scriptVers; 
    }


    

    
    
    
    /**
     * return collection of all AnalysisScriptVer for a analysisScriptId
     * @param analysisScriptId
     * @param includeRuns
     * @param includeFiles
     * @return all AnalysisScriptVer based on analysisScriptVerId
     * @throws JDAOException
     */
    public List<Jstruct_AnalysisScriptVer> getByAnalysisScriptId(Long analysisScriptId, boolean includeRuns, boolean includeFiles) throws JDAOException 
    {
        List<Jstruct_AnalysisScriptVer> scriptVers = analysisScriptVerDao.getByAnalysisScriptId(analysisScriptId);
        
        if(includeRuns) 
        {
            scriptVers = retrieveRuns(scriptVers);
        }
        
        if(includeFiles){
            scriptVers = retrieveFiles(scriptVers);
        }
        
        return scriptVers; 
    }
    
    
    
    
    /* -------------------------------------------------------------------------
        private 
    ------------------------------------------------------------------------- */
    
    
    private  List<Jstruct_AnalysisScriptVer> retrieveRuns(List<Jstruct_AnalysisScriptVer> scriptVers) throws JDAOException 
    {
        if(CollectionUtil.hasValues(scriptVers))
        {
            for(Jstruct_AnalysisScriptVer scriptVer : scriptVers)
            {
                List<Jstruct_AnalysisRun> runs = analysisRunService.getByAnalysisScriptVerId(scriptVer.getAnalysisScriptVerId());
                scriptVer.addAnalysisRuns(runs);
            }
        }
        return scriptVers;
    }
    
    
    
    private Jstruct_AnalysisScriptVer retrieveRuns(Jstruct_AnalysisScriptVer scriptVer) throws JDAOException 
    {
        if(scriptVer != null)
        {
            List<Jstruct_AnalysisRun> runs = analysisRunService.getByAnalysisScriptVerId(scriptVer.getAnalysisScriptVerId());
            scriptVer.addAnalysisRuns(runs);
        }
        return scriptVer;
    }
    
    
    
    
    
    private  List<Jstruct_AnalysisScriptVer> retrieveFiles(List<Jstruct_AnalysisScriptVer> scriptVers) throws JDAOException 
    {
        if(CollectionUtil.hasValues(scriptVers))
        {
            for(Jstruct_AnalysisScriptVer scriptVer : scriptVers)
            {
                List<Jstruct_AnalysisFile> files = analysisFileService.getByAnalysisScriptVerId(scriptVer.getAnalysisScriptVerId());
                scriptVer.addAnalysisFiles(files);
            }
        }
        return scriptVers;
    }
    
    
    
    private Jstruct_AnalysisScriptVer retrieveFiles(Jstruct_AnalysisScriptVer scriptVer) throws JDAOException 
    {
        if(scriptVer != null)
        {
            List<Jstruct_AnalysisFile> files = analysisFileService.getByAnalysisScriptVerId(scriptVer.getAnalysisScriptVerId());
            scriptVer.addAnalysisFiles(files);
        }
        return scriptVer;
    }
    
    
    
    
    

    
    
    


    
    
    
    
    
    
    
    
 
    
    
}
