package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.ImportJobDAO;
import com.just.jstruct.dao.interfaces.IImportJobDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportJob;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportJobService implements Serializable {


    /* dao classes */
    
    private final IImportJobDAO importJobDao;


    
    
    /* constructor */
    
    public ImportJobService() {
        importJobDao = new ImportJobDAO();
    }


    
    /**
     * return collection of all ImportJobs
     * @return all ImportJobs
     * @throws JDAOException
     */
    public List<Jstruct_ImportJob> getAll() throws JDAOException {
        return importJobDao.getAll();
    }


    /**
     * get a ImportJob based on it's primary key
     * @param importJobId of ImportJob to find
     * @param includeImportRests
     * @param includeImportMsgs
     * @return importJob that matches importJobId
     * @throws JDAOException
     */
    public Jstruct_ImportJob getByImportJobId(Long importJobId, boolean includeImportRests, boolean includeImportMsgs) throws JDAOException {
        return importJobDao.getByImportJobId(importJobId, includeImportRests, includeImportMsgs);
    }
    
    
    /**
     * all importjobs that are related to set of rcsb ImportJobTypes (see named query: Jstruct_ImportJob.findAllRcsbImportJobs)
     * @return 
     * @throws JDAOException 
     */
    public List<Jstruct_ImportJob> getAllRcsbImportJobs() throws JDAOException {
        return importJobDao.getAllRcsbImportJobs();
    }


    /**
     * get the most recent successful rcsb run
     * @return 
     * @throws JDAOException
     */
    public Jstruct_ImportJob getMostRecentSuccessfulRcsbRun() throws JDAOException {
        return importJobDao.getMostRecentSuccessfulRcsbRun();
    }

    
    
    
    /**
     * return true if any INITIAL_RCSB_RUN runs have been run.
     * @return
     * @throws JDAOException 
     */
    public boolean hasInitialImportRun() throws JDAOException {
        List<Jstruct_ImportJob> onlyInitialRcsbRuns = importJobDao.getOnlyInitialRcsbRuns();
        return CollectionUtil.hasValues(onlyInitialRcsbRuns);
    }
    
    
     
    
    /**
     * return true if any successful initial runs have been run.
     * (INITIAL_RCSB_RUN, INITIAL_RCSB_RUN_RECOVER, INITIAL_RCSB_RERUN)
     * @return
     * @throws JDAOException 
     */
    public boolean wasInitialImportSuccessful() throws JDAOException {
        List<Jstruct_ImportJob> onlySuccessfulInitialRcsbRuns = importJobDao.getOnlySuccessfulInitialRcsbRuns();
        return CollectionUtil.hasValues(onlySuccessfulInitialRcsbRuns);
    }
    
    
    
    
    
    
    /**
     * add a new importJob to the database
     * @param importJob to add
     * @return the brand new importJob
     * @throws JDAOException 
     */
    public Jstruct_ImportJob add(Jstruct_ImportJob importJob) throws JDAOException {
        return importJobDao.add(importJob);
    }
    

    
    
    
    
    /**
     * update an importJob 
     * @param importJob to update
     * @return the updated importJob
     * @throws JDAOException 
     */
    public Jstruct_ImportJob update(Jstruct_ImportJob importJob) throws JDAOException {
        return importJobDao.update(importJob);
    }
    

    
    
    
    
}
