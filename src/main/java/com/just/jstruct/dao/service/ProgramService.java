package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.ProgramDAO;
import com.just.jstruct.dao.interfaces.IProgramDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Program;
import com.just.jstruct.model.Jstruct_User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ProgramService implements Serializable {


    /* dao classes */
    
    private final IProgramDAO programDao;


    
    
    /* constructor */
    
    public ProgramService() {
        programDao = new ProgramDAO();
    }



    /**
     * return collection of all Programs
     * @return all JstructUsers
     * @throws JDAOException
     */
    public List<Jstruct_Program> getAll() throws JDAOException {
        return programDao.getAll();
    }


    /**
     * get a Program based on it's primary key
     * @param programId of Program to find
     * @return program that matches programId
     * @throws JDAOException
     */
    public Jstruct_Program getByProgramId(Long programId) throws JDAOException {
        return programDao.getByProgramId(programId);
    }


    /**
     * get a Program based on it's name
     * @param name of Program to find
     * @return program that matches name
     * @throws JDAOException
     */
    public Jstruct_Program getByName(String name) throws JDAOException {
        return programDao.getByName(name);
    }
    
    
    /**
     * determine if a program name already exists; use programIdToIgnore if editing
     * @param name
     * @param programIdToIgnore
     * @return
     * @throws JDAOException 
     */
    public boolean programExists(String name, Long programIdToIgnore) throws JDAOException{
        return programDao.programExists(name, programIdToIgnore);
    }
    
    
    /**
     * determine if a program name already exists
     * @param name
     * @return
     * @throws JDAOException 
     */
    public boolean programExists(String name) throws JDAOException{
        return programExists(name, null);
    }
    
    
    /**
     * determine if a program is used (e.g in the struct_version table)
     * @param programId
     * @return
     * @throws JDAOException 
     */
    public boolean isUsed(Long programId) throws JDAOException{
        return programDao.isUsed(programId);
    }
    
    
    
    
    
    /**
     * update a Program in the database
     * @param program to update
     * @return the updated program from the database
     * @throws JDAOException
     */
    public Jstruct_Program updateProgram(Jstruct_Program program) throws JDAOException {
        Jstruct_Program updatedProgram = programDao.update(program);
        return updatedProgram;
    }
    
    
    /**
     * add a new program to the database
     * @param program to add
     * @param user
     * @return the brand new program
     * @throws JDAOException 
     */
    public Jstruct_Program addProgram(Jstruct_Program program, Jstruct_User user) throws JDAOException {
        
        program.setInsertDate(new Date());
        program.setUpdateDate(new Date());
        
        program.setInsertUser(user);
        program.setUpdateUser(user);

        Jstruct_Program updatedProgram = programDao.add(program);
        return updatedProgram; 
    }
    
    
    
 
    
    
}
