package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbRevdatDAO;
import com.just.jstruct.dao.interfaces.IPdbRevdatDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbRevdat;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbRevdatService implements Serializable {


    /* dao classes */
    
    private final IPdbRevdatDAO pdbRevdatDao;


    
    
    /* constructor */
    
    public PdbRevdatService() {
        pdbRevdatDao = new PdbRevdatDAO();
    }



    

    /**
     * get a list of PdbRevdats based on a structVerId
     * @param structVerId of PdbRevdats to find
     * @return PdbRevdats that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbRevdat> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbRevdatDao.getByStructVerId(structVerId);
    }
    
    
    
    /**
     * get the initial release PdbRevdat based on a structVerId
     * @param structVerId of PdbRevdat to find
     * @return PdbRevdat that matches structVerId
     * @throws JDAOException
     */
    public Jstruct_PdbRevdat getInitialReleaseByStructVerId(Long structVerId) throws JDAOException {
        return pdbRevdatDao.getInitialReleaseByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbRevdats related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbRevdatDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbRevdats
     * @param pdbRevdats
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbRevdat> batchInsert(List<Jstruct_PdbRevdat> pdbRevdats) throws JDAOException {
        return pdbRevdatDao.batchInsert(pdbRevdats);
    }
    
    
    /**
     * insert a batch/list of PdbRevdats for a specific structVerId
     * @param structVerId
     * @param pdbRevdats
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbRevdat> batchInsert(Long structVerId, List<Jstruct_PdbRevdat> pdbRevdats) throws JDAOException {
        if(CollectionUtil.hasValues(pdbRevdats)){
            pdbRevdats.stream().forEach((pdbRevdat) -> {
                pdbRevdat.setStructVerId(structVerId);
            });
            return batchInsert(pdbRevdats);
        }
        return null;
    }
    
    
    
    
    
}
