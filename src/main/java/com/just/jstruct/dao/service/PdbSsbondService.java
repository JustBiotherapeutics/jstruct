package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbSsbondDAO;
import com.just.jstruct.dao.interfaces.IPdbSsbondDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbSsbond;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbSsbondService implements Serializable {


    /* dao classes */
    
    private final IPdbSsbondDAO pdbSsbondDao;


    
    
    /* constructor */
    
    public PdbSsbondService() {
        pdbSsbondDao = new PdbSsbondDAO();
    }



    

    /**
     * get a list of PdbSsbonds based on a structVerId
     * @param structVerId of PdbSsbonds to find
     * @return PdbSsbonds that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbSsbond> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbSsbondDao.getByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbSsbonds related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbSsbondDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbSsbonds
     * @param pdbSsbonds
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbSsbond> batchInsert(List<Jstruct_PdbSsbond> pdbSsbonds) throws JDAOException {
        return pdbSsbondDao.batchInsert(pdbSsbonds);
    }
    
    
    /**
     * insert a batch/list of PdbSsbonds for a specific structVerId
     * @param structVerId
     * @param pdbSsbonds
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbSsbond> batchInsert(Long structVerId, List<Jstruct_PdbSsbond> pdbSsbonds) throws JDAOException {
        if(CollectionUtil.hasValues(pdbSsbonds)){
            pdbSsbonds.stream().forEach((pdbSsbond) -> {
                pdbSsbond.setStructVerId(structVerId);
            });
            return batchInsert(pdbSsbonds);
        }
        return null;
    }
    
    
    
    
    
}
