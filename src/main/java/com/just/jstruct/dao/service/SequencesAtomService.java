package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.SequencesAtomDAO;
import com.just.jstruct.dao.interfaces.ISequencesAtomDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SequencesAtom;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SequencesAtomService implements Serializable {


    /* dao classes */
    
    private final ISequencesAtomDAO sequencesAtomDao;


    
    
    /* constructor */
    
    public SequencesAtomService() {
        sequencesAtomDao = new SequencesAtomDAO();
    }


    
    /**
     * 
     * @param structVerIds
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_SequencesAtom> getByStructVerIds(List<Long> structVerIds) throws JDAOException {
        return sequencesAtomDao.getByStructVerIds(structVerIds);
    }
    
    
    /**
     * 
     * @return
     * @throws JDAOException 
     */
    public List<Long> getStructVerIds() throws JDAOException {
        return sequencesAtomDao.getStructVerIds();
    }
    
    
    
}
