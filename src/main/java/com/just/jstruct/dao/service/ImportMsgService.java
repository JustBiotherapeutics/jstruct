package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.ImportMsgDAO;
import com.just.jstruct.dao.interfaces.IImportMsgDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportMsg;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportMsgService implements Serializable {


    /* dao classes */
    
    private final IImportMsgDAO importMsgDao;


    
    
    /* constructor */
    
    public ImportMsgService() {
        importMsgDao = new ImportMsgDAO();
    }



    /**
     * get a ImportMsg based on it's primary key
     * @param importMsgId of ImportMsg to find
     * @return importMsg that matches importMsgId
     * @throws JDAOException
     */
    public Jstruct_ImportMsg getByImportMsgId(Long importMsgId) throws JDAOException {
        return importMsgDao.getByImportMsgId(importMsgId);
    }
    
    
    
    /**
     * 
     * @param importJobId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_ImportMsg> getByImportJobId(Long importJobId) throws JDAOException {
        return importMsgDao.getByImportJobId(importJobId);
    }
    
    
    
    /**
     * persist a new importMsg 
     * @param importMsg
     * @return
     * @throws JDAOException 
     */
    public Jstruct_ImportMsg add(Jstruct_ImportMsg importMsg) throws JDAOException {
        return importMsgDao.add(importMsg);
    }
    
    
}
