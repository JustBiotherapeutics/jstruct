package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.StructVersionDAO;
import com.just.jstruct.dao.interfaces.IStructVersionDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructVersion;
import com.just.jstruct.model.Jstruct_User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructVersionService implements Serializable {


    /* dao classes */
    private final IStructVersionDAO structVersionDao;


    /* constructor */
    public StructVersionService() {
        structVersionDao = new StructVersionDAO();
    }



    /**
     * get a StructVersion based on it's primary key
     * @param structVerId of StructVersion to find
     * @return structVersion that matches structVerId
     * @throws JDAOException
     */
    public Jstruct_StructVersion getByStructVerId(Long structVerId) throws JDAOException {
        return structVersionDao.getByStructVerId(structVerId);
    }

    


    /**
     * get the most recent (youngest, aka newest) struct_version for a structure id
     * @param structureId 
     * @return structVersion that is youngest for this structure
     * @throws JDAOException
     */
    public Jstruct_StructVersion getYoungestByStructureId(Long structureId) throws JDAOException{
        return structVersionDao.getYoungestByStructureId(structureId);
    }

    
    
    /**
     * get ALLLLLLL the structVerIds in the database. this will be big.
     * @return 
     * @throws JDAOException 
     */
    public List<Long> getAllStructVerIds() throws JDAOException{
        return structVersionDao.getAllStructVerIds();
    }
    
    
    /**
     * get struct version ids for ALL manually uploaded files
     * @return 
     * @throws JDAOException 
     */
    public List<Long> getManualStructVerIds() throws JDAOException{
        return structVersionDao.getManualStructVerIds();
    }
    
    
    
    /**
     * get struct version ids for ALL active RCSB files
     * @return 
     * @throws JDAOException 
     */
    public List<Long> getActiveRcsbStructVerIds() throws JDAOException{
        return structVersionDao.getActiveRcsbStructVerIds();
    }
    
    /**
     * get struct version ids for ALL active manually uploaded 'model' files
     * @return 
     * @throws JDAOException 
     */
    public List<Long> getActiveManualModelStructVerIds() throws JDAOException{
        return structVersionDao.getActiveManualModelStructVerIds();
    }
    
    /**
     * get struct version ids for ALL active manually uploaded 'NON-model' files
     * @return 
     * @throws JDAOException 
     */
    public List<Long> getActiveManualNonModelStructVerIds() throws JDAOException{
        return structVersionDao.getActiveManualNonModelStructVerIds();
    }
    
    
    
    
    
    
    /**
     * 
     * @param structVersion
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructVersion add(Jstruct_StructVersion structVersion) throws JDAOException {
        if(structVersion.getClientId()!=null && structVersion.getClientId() == 0){
            structVersion.setClientId(null);
        }
        if(structVersion.getProgramId()!=null && structVersion.getProgramId() == 0){
            structVersion.setProgramId(null);
        }
        return structVersionDao.add(structVersion);
    }
    
 
    
    /**
     * 
     * @param structVersion
     * @param user
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructVersion update(Jstruct_StructVersion structVersion, Jstruct_User user) throws JDAOException {
        structVersion.setUpdateUser(user);
        structVersion.setUpdateDate(new Date());
        return structVersionDao.update(structVersion);
    }
    
    
}
