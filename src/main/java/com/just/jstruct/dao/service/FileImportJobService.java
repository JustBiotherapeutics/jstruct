package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.FileImportJobDAO;
import com.just.jstruct.dao.interfaces.IFileImportJobDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FileImportJob;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class FileImportJobService implements Serializable {


    /* dao classes */
    private final IFileImportJobDAO fileImportJobDao;


    /* constructor */
    public FileImportJobService() {
        fileImportJobDao = new FileImportJobDAO();
    }



    /**
     * get a FileImportJob based on it's primary key
     * @param fileImportJobId of FileImportJob to find
     * @return fileImportJob that matches fileImportJobId
     * @throws JDAOException
     */
    public Jstruct_FileImportJob getByFileImportJobId(Long fileImportJobId) throws JDAOException {
        return fileImportJobDao.getByFileImportJobId(fileImportJobId);
    }

    


    
    
    /**
     * get ALL the FileImportJobs for a importJob
     * @param importJobId
     * @return 
     * @throws JDAOException 
     */
    public List<Jstruct_FileImportJob> getByImportJobId(Long importJobId) throws JDAOException {
        return fileImportJobDao.getByImportJobId(importJobId);
    }
    
    
    /**
     * add a new file import job.
     * @param fileImportJob
     * @return
     * @throws JDAOException 
     */
    public Jstruct_FileImportJob add(Jstruct_FileImportJob fileImportJob) throws JDAOException {
        return fileImportJobDao.add(fileImportJob);
    }
    
    
    
}
