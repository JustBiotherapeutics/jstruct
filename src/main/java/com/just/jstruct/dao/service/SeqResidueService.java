package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.SeqResidueDAO;
import com.just.jstruct.dao.interfaces.ISeqResidueDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SeqResidue;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SeqResidueService implements Serializable {


    /* dao classes */
    
    private final ISeqResidueDAO seqResidueDao;


    
    
    /* constructor */
    
    public SeqResidueService() {
        seqResidueDao = new SeqResidueDAO();
    }



    


    /**
     * get a SeqResidue based on it's primary key
     * @param seqResidueId of SeqResidue to find
     * @return seqResidue that matches seqResidueId
     * @throws JDAOException
     */
    public Jstruct_SeqResidue getBySeqResidueId(Long seqResidueId) throws JDAOException {
        return seqResidueDao.getBySeqResidueId(seqResidueId);
    }
    


    /**
     * get a list of SeqResidues based on a chainId
     * @param chainId of SeqResidues to find
     * @return seqResidues that matches chainId
     * @throws JDAOException
     */
    public List<Jstruct_SeqResidue> getByChainId(Long chainId) throws JDAOException {
        return seqResidueDao.getByChainId(chainId);
    }


    
    
    
    
    
    /**
     * delete all Jstruct_SeqResidue for a chain
     * @param chainId
     * @throws JDAOException 
     */
    public void deleteByChainId(Long chainId) throws JDAOException{
        seqResidueDao.deleteByChainId(chainId);
    }
    
    
 
    /**
     * delete all Jstruct_SeqResidue from the database for a struct_ver_id
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        seqResidueDao.deleteByStructVerId(structVerId);
    }
    
    
    
    /**
     * 
     * @param seqResidues
     * @param chainId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_SeqResidue> bulkInsert(List<Jstruct_SeqResidue> seqResidues, Long chainId) throws JDAOException{
        return seqResidueDao.bulkInsert(seqResidues,chainId);
             
    }
    
    
}
