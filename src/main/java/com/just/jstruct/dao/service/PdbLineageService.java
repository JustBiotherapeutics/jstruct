package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbLineageDAO;
import com.just.jstruct.dao.interfaces.IPdbLineageDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_PdbLineage;
import com.just.jstruct.model.Jstruct_User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbLineageService implements Serializable {


    /* dao classes */
    
    private final IPdbLineageDAO pdbLineageDao;


    
    
    /* constructor */
    
    public PdbLineageService() {
        pdbLineageDao = new PdbLineageDAO();
    }

    

    
    // ======================================================================================================
    
    
    /**
     * get all rows by a PdbPredecessorId (parent id)
     * @param pdbParentId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbLineage> getAllByPdbPredecessorId(String pdbParentId) throws JDAOException {
        return pdbLineageDao.getAllByPdbPredecessorId(pdbParentId);
    }

    /**
     * get all rows by a PdbDescendantId (child id)
     * @param pdbChildId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbLineage> getAllByPdbDescendantId(String pdbChildId) throws JDAOException {
        return pdbLineageDao.getAllByPdbDescendantId(pdbChildId);
    }

    /**
     * get a row with a specific predecessor and descendant (descendant can be null)
     * @param pdbPredecessorId
     * @param pdbDescendantId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_PdbLineage getByPdbPredecessorIdAndPdbDescendantId(String pdbPredecessorId, String pdbDescendantId) throws JDAOException {
        return pdbLineageDao.getByPdbPredecessorIdAndPdbDescendantId(pdbPredecessorId, pdbDescendantId);
    }

    /**
     * based on a pdbId, get the 'most current' pdb in the lineage
     * @param pbdIdentifier
     * @param ifCurrentIsObsoleteGetMostRecent
     * @return
     * @throws JDAOException 
     */
    public String getCurrentBasedOnPdbIdentifier(String pbdIdentifier, boolean ifCurrentIsObsoleteGetMostRecent) throws JDAOException {
        return pdbLineageDao.getCurrentBasedOnPdbIdentifier(pbdIdentifier, ifCurrentIsObsoleteGetMostRecent);
    }

    /**
     * get all FullStructure objects related to a pdb id (all predecessors and descendants)
     * @param pbdIdentifier
     * @param currentUser
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_FullStructure> getFullStructureLineageBasedOnPdbIdentifier(String pbdIdentifier, Jstruct_User currentUser) throws JDAOException {
        return pdbLineageDao.getFullStructureLineageBasedOnPdbIdentifier(pbdIdentifier, currentUser);
    }
    
    
    /**
     * insert a new pdbLineage record
     * @param predecessorId
     * @param descendantId
     * @param obsoleteDate
     * @return
     * @throws JDAOException 
     
    public Jstruct_PdbLineage insert(String predecessorId, String descendantId, Date obsoleteDate) throws JDAOException {
        Jstruct_PdbLineage pdbLineage = new Jstruct_PdbLineage(predecessorId, descendantId, obsoleteDate);
        return pdbLineageDao.add(pdbLineage);
    } */
    
    
    /**
     * 
     * @param predecessorId
     * @param descendantIds
     * @param obsoleteDate
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbLineage> updatePdbLineageWithObsltlData(String predecessorId, List<String> descendantIds, Date obsoleteDate) throws JDAOException {
        
        if (null==predecessorId || !CollectionUtil.hasValues(descendantIds) || null==obsoleteDate){
            return null;
        }
        
        List<Jstruct_PdbLineage> pdbLineages = new ArrayList<>(descendantIds.size());
        
        for(String descendantId : descendantIds){
            
            Jstruct_PdbLineage existingRow = pdbLineageDao.getByPdbPredecessorIdAndPdbDescendantId(predecessorId, descendantId);
            if (null != existingRow) {
                pdbLineages.add(existingRow);
            } else {
                Jstruct_PdbLineage p = pdbLineageDao.add(new Jstruct_PdbLineage(predecessorId, descendantId, obsoleteDate));
                pdbLineages.add(p);
            }
        }
        return pdbLineages;
    }
    
    
    
    /**
     * 
     * @param predecessorIds
     * @param descendantId
     * @param obsoleteDate
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbLineage> updatePdbLineageWithSprsdeData(List<String> predecessorIds, String descendantId, Date obsoleteDate) throws JDAOException {
        
        if (!CollectionUtil.hasValues(predecessorIds) ||null==descendantId ||  null==obsoleteDate){
            return null;
        }
        
        List<Jstruct_PdbLineage> pdbLineages = new ArrayList<>(predecessorIds.size());
        
        for(String predecessorId : predecessorIds){
            
            Jstruct_PdbLineage existingRow = pdbLineageDao.getByPdbPredecessorIdAndPdbDescendantId(predecessorId, descendantId);
            if (null != existingRow) {
               pdbLineages.add(existingRow);
            } else { 
                Jstruct_PdbLineage p = pdbLineageDao.add(new Jstruct_PdbLineage(predecessorId, descendantId, obsoleteDate));
                pdbLineages.add(p);
            }
            
            
        }
        return pdbLineages;
    }

    

    
    
    
    
    
 
    
    
}
