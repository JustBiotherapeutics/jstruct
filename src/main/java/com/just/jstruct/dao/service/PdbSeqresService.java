package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbSeqresDAO;
import com.just.jstruct.dao.interfaces.IPdbSeqresDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbSeqres;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbSeqresService implements Serializable {


    /* dao classes */
    
    private final IPdbSeqresDAO pdbSeqresDao;


    
    
    /* constructor */
    
    public PdbSeqresService() {
        pdbSeqresDao = new PdbSeqresDAO();
    }



    

    /**
     * get a list of PdbSeqress based on a structVerId
     * @param structVerId of PdbSeqress to find
     * @return PdbSeqress that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbSeqres> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbSeqresDao.getByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbSeqress related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbSeqresDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbSeqress
     * @param pdbSeqress
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbSeqres> batchInsert(List<Jstruct_PdbSeqres> pdbSeqress) throws JDAOException {
        return pdbSeqresDao.batchInsert(pdbSeqress);
    }
    
    
    /**
     * insert a batch/list of PdbSeqress for a specific structVerId
     * @param structVerId
     * @param pdbSeqress
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbSeqres> batchInsert(Long structVerId, List<Jstruct_PdbSeqres> pdbSeqress) throws JDAOException {
        if(CollectionUtil.hasValues(pdbSeqress)){
            pdbSeqress.stream().forEach((pdbSeqres) -> {
                pdbSeqres.setStructVerId(structVerId);
            });
            return batchInsert(pdbSeqress);
        }
        return null;
    }
    
    
    
    
    
}
