package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbSplitDAO;
import com.just.jstruct.dao.interfaces.IPdbSplitDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbSplit;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbSplitService implements Serializable {


    /* dao classes */
    
    private final IPdbSplitDAO pdbSplitDao;


    
    
    /* constructor */
    
    public PdbSplitService() {
        pdbSplitDao = new PdbSplitDAO();
    }



    

    /**
     * get a list of PdbSplits based on a structVerId
     * @param structVerId of PdbSplits to find
     * @return PdbSplits that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbSplit> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbSplitDao.getByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbSplits related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbSplitDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbSplits
     * @param pdbSplits
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbSplit> batchInsert(List<Jstruct_PdbSplit> pdbSplits) throws JDAOException {
        return pdbSplitDao.batchInsert(pdbSplits);
    }
    
    
    /**
     * insert a batch/list of PdbSplits for a specific structVerId
     * @param structVerId
     * @param pdbSplits
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbSplit> batchInsert(Long structVerId, List<Jstruct_PdbSplit> pdbSplits) throws JDAOException {
        if(CollectionUtil.hasValues(pdbSplits)){
            pdbSplits.stream().forEach((pdbSplit) -> {
                pdbSplit.setStructVerId(structVerId);
            });
            return batchInsert(pdbSplits);
        }
        return null;
    }
    
    
    
    
    
}
