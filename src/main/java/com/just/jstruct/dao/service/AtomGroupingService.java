package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.AtomGroupingDAO;
import com.just.jstruct.dao.interfaces.IAtomGroupingDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AtomGrouping;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AtomGroupingService implements Serializable {


    /* dao classes */
    
    private final IAtomGroupingDAO atomGroupingDao;


    
    
    /* constructor */
    
    public AtomGroupingService() {
        atomGroupingDao = new AtomGroupingDAO();
    }



    


    /**
     * get a AtomGrouping based on it's primary key
     * @param atomGroupingId of AtomGrouping to find
     * @return atomGrouping that matches atomGroupingId
     * @throws JDAOException
     */
    public Jstruct_AtomGrouping getByAtomGroupingId(Long atomGroupingId) throws JDAOException {
        return atomGroupingDao.getByAtomGroupingId(atomGroupingId);
    }
    


    /**
     * get a list of AtomGroupings based on a chainId
     * @param chainId of AtomGroupings to find
     * @param includeResidues
     * @return atomGroupings that matches chainId
     * @throws JDAOException
     */
    public List<Jstruct_AtomGrouping> getByChainId(Long chainId, boolean includeResidues) throws JDAOException {
        
        List<Jstruct_AtomGrouping> atomGroupings = atomGroupingDao.getByChainId(chainId);
        
        if(CollectionUtil.hasValues(atomGroupings) && includeResidues){
            
            ResidueService residueService = new ResidueService();
            
            for(Jstruct_AtomGrouping ag : atomGroupings){
                ag.setResidues(residueService.getByAtomGroupingId(ag.getAtomGroupingId()));
            }
        }
        
        return atomGroupings;
        
    }


    
    
    /**
     * delete all atomGroupings from the database for a chain
     * @param chainId
     * @throws JDAOException 
     */
    public void deleteByChainId(Long chainId) throws JDAOException {
        atomGroupingDao.deleteByChainId(chainId);
    }
    
    
    
    /**
     * delete all atomGroupings from the database for a struct_ver_id
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        atomGroupingDao.deleteByStructVerId(structVerId);
    }
    
    
    
    /**
     * 
     * @param atomGrouping
     * @return
     * @throws JDAOException 
     */
    public Jstruct_AtomGrouping insert(Jstruct_AtomGrouping atomGrouping) throws JDAOException{
        return atomGroupingDao.insert(atomGrouping);
    }
    
    
 
    
    
}
