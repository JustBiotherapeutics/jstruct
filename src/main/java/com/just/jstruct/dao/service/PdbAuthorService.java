package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbAuthorDAO;
import com.just.jstruct.dao.interfaces.IPdbAuthorDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbAuthor;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbAuthorService implements Serializable {


    /* dao classes */
    
    private final IPdbAuthorDAO pdbAuthorDao;


    
    
    /* constructor */
    
    public PdbAuthorService() {
        pdbAuthorDao = new PdbAuthorDAO();
    }



    

    /**
     * get a list of PdbAuthors based on a structVerId
     * @param structVerId of PdbAuthors to find
     * @return PdbAuthors that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbAuthor> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbAuthorDao.getByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbAuthors related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbAuthorDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbAuthors
     * @param pdbAuthors
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbAuthor> batchInsert(List<Jstruct_PdbAuthor> pdbAuthors) throws JDAOException {
        return pdbAuthorDao.batchInsert(pdbAuthors);
    }
    
    
    /**
     * insert a batch/list of PdbAuthors for a specific structVerId
     * @param structVerId
     * @param pdbAuthors
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbAuthor> batchInsert(Long structVerId, List<Jstruct_PdbAuthor> pdbAuthors) throws JDAOException {
        if(CollectionUtil.hasValues(pdbAuthors)){
            pdbAuthors.stream().forEach((pdbAuthor) -> {
                pdbAuthor.setStructVerId(structVerId);
            });
            return batchInsert(pdbAuthors);
        }
        return null;
    }
    
    
    
    
    
}
