package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbRemarkDAO;
import com.just.jstruct.dao.interfaces.IPdbRemarkDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbRemark;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbRemarkService implements Serializable {


    /* dao classes */
    
    private final IPdbRemarkDAO pdbRemarkDao;


    
    
    /* constructor */
    
    public PdbRemarkService() {
        pdbRemarkDao = new PdbRemarkDAO();
    }



    

    /**
     * get a list of PdbRemarks based on a structVerId
     * @param structVerId of PdbRemarks to find
     * @return PdbRemarks that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbRemark> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbRemarkDao.getByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbRemarks related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbRemarkDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbRemarks
     * @param pdbRemarks
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbRemark> batchInsert(List<Jstruct_PdbRemark> pdbRemarks) throws JDAOException {
        return pdbRemarkDao.batchInsert(pdbRemarks);
    }
    
    
    /**
     * insert a batch/list of PdbRemarks for a specific structVerId
     * @param structVerId
     * @param pdbRemarks
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbRemark> batchInsert(Long structVerId, List<Jstruct_PdbRemark> pdbRemarks) throws JDAOException {
        if(CollectionUtil.hasValues(pdbRemarks)){
            pdbRemarks.stream().forEach((pdbRemark) -> {
                pdbRemark.setStructVerId(structVerId);
            });
            return batchInsert(pdbRemarks);
        }
        return null;
    }
    
    
    
    
    
}
