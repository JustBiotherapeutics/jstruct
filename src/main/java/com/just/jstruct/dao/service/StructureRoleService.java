package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.StructureRoleDAO;
import com.just.jstruct.dao.interfaces.IStructureRoleDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureRole;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureRoleService implements Serializable {


    /* dao classes */
    
    private final IStructureRoleDAO structureRoleDao;


    
    
    /* constructor */
    
    public StructureRoleService() {
        structureRoleDao = new StructureRoleDAO();
    }

    

    /**
     * get based on a structureRoleId
     * @param structureRoleId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructureRole getByStructureRoleId(Long structureRoleId) throws JDAOException {
        return structureRoleDao.getByStructureRoleId(structureRoleId);
    }
    
    
    
    /**
     * get based on a userId and structureId
     * @param structureId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructureRole getByStructureIdUserId(Long structureId, Long userId) throws JDAOException {
        return structureRoleDao.getByStructureIdUserId(structureId, userId);
    }

    
    
    /**
     * get a list of roles for a structure
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_StructureRole> getByStructureId(Long structureId) throws JDAOException {
        return structureRoleDao.getByStructureId(structureId);
    }
    
    
    /**
     * 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public List<Long> getReadStructureIdsForUser(Long userId) throws JDAOException {
        return structureRoleDao.getReadStructureIdsForUser(userId);
    }
    
    /**
     * 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public List<Long> getWriteStructureIdsForUser(Long userId) throws JDAOException {
        return structureRoleDao.getWriteStructureIdsForUser(userId);
    }
    
    /**
     * 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public List<Long> getGrantStructureIdsForUser(Long userId) throws JDAOException {
        return structureRoleDao.getGrantStructureIdsForUser(userId);
    }
    
    
    
    
    
    
    public Jstruct_StructureRole add(Jstruct_StructureRole structureRole) throws JDAOException {
        return structureRoleDao.add(structureRole);
    }
    
    
    public Jstruct_StructureRole update(Jstruct_StructureRole structureRole) throws JDAOException {
        return structureRoleDao.update(structureRole);
    }
    
    
    public void delete(Jstruct_StructureRole structureRole) throws JDAOException {
        structureRoleDao.delete(structureRole);
    }
    
    
    
    
    public Jstruct_StructureRole giveRoleUpToRead(Jstruct_StructureRole structureRole) throws JDAOException {
        structureRole.setCanRead(true);
        structureRole.setCanWrite(false);
        structureRole.setCanGrant(false);
        return update(structureRole);
    }
    
    
    
    public Jstruct_StructureRole giveRoleUpToWrite(Jstruct_StructureRole structureRole) throws JDAOException {
        structureRole.setCanRead(true);
        structureRole.setCanWrite(true);
        structureRole.setCanGrant(false);
        return update(structureRole);
    }
    
    
    
    public Jstruct_StructureRole giveRoleUpToGrant(Jstruct_StructureRole structureRole) throws JDAOException {
        structureRole.setCanRead(true);
        structureRole.setCanWrite(true);
        structureRole.setCanGrant(true);
        return update(structureRole);
    }
    
    
    
    
    
    
}
