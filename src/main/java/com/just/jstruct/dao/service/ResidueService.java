package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.ResidueDAO;
import com.just.jstruct.dao.interfaces.IResidueDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Residue;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ResidueService implements Serializable {


    /* dao classes */
    
    private final IResidueDAO residueDao;


    
    
    /* constructor */
    
    public ResidueService() {
        residueDao = new ResidueDAO();
    }



    


    /**
     * get a Residue based on it's primary key
     * @param residueId of Residue to find
     * @return residue that matches residueId
     * @throws JDAOException
     */
    public Jstruct_Residue getByResidueId(Long residueId) throws JDAOException {
        return residueDao.getByResidueId(residueId);
    }
    


    /**
     * get a list of Residues based on a atomGroupingId
     * @param atomGroupingId of Residues to find
     * @return residues that matches atomGroupingId
     * @throws JDAOException
     */
    public List<Jstruct_Residue> getByAtomGroupingId(Long atomGroupingId) throws JDAOException {
        return residueDao.getByAtomGroupingId(atomGroupingId);
    }


    /**
     * delete all Jstruct_Residues for a atomGroup
     * @param atomGroupingId
     * @throws JDAOException 
     */
    public void deleteByAtomGroupingId(Long atomGroupingId) throws JDAOException{
        residueDao.deleteByAtomGroupingId(atomGroupingId);
    }
    
    
    /**
     * delete all Jstruct_Residues from the database for a struct_ver_id
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        residueDao.deleteByStructVerId(structVerId);
    }
    
    
    
    /**
     * 
     * @param residues
     * @param atomGroupingId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_Residue> bulkInsert(List<Jstruct_Residue> residues, Long atomGroupingId) throws JDAOException{
        return residueDao.bulkInsert(residues, atomGroupingId);
    }

 
    
    
}
