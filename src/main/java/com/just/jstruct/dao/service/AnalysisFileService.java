package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.AnalysisFileDAO;
import com.just.jstruct.dao.interfaces.IAnalysisFileDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisFile;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisFileService implements Serializable {

    /* -------------------------------------------------------------------------
        dao classes 
    ------------------------------------------------------------------------- */
    
    private final IAnalysisFileDAO analysisFileDao;


    /* -------------------------------------------------------------------------
        constructor 
    ------------------------------------------------------------------------- */
    
    public AnalysisFileService() {
        analysisFileDao = new AnalysisFileDAO();
    }


    
    /* -------------------------------------------------------------------------
        methods 
    ------------------------------------------------------------------------- */
    
    /**
     * add a new AnalysisFile to the database
     * @param analysisFile to add
     * @return the newly persisted analysisFile
     * @throws JDAOException 
     */
    public Jstruct_AnalysisFile add(Jstruct_AnalysisFile analysisFile) throws JDAOException 
    {
        Jstruct_AnalysisFile newAnalysisFile = analysisFileDao.add(analysisFile);
        return newAnalysisFile; 
    }
    
    
    /**
     * update a AnalysisFile in the database
     * @param analysisFile to update
     * @return the updated analysisFile from the database
     * @throws JDAOException
     */
    public Jstruct_AnalysisFile update(Jstruct_AnalysisFile analysisFile) throws JDAOException 
    {
        Jstruct_AnalysisFile updatedAnalysisFile = analysisFileDao.update(analysisFile);
        return updatedAnalysisFile;
    }
    
    
    
    
    
    
    
    
    
    /**
     * get a AnalysisFile based on it's primary key
     * @param id of AnalysisFile to find
     * @return AnalysisFile that matches id
     * @throws JDAOException
     */
    public Jstruct_AnalysisFile getById(Long id) throws JDAOException 
    {
        return analysisFileDao.getById(id);
    }
    
    
  
    
    
    
    /**
     * return collection of all AnalysisFile for a analysisScriptVerId
     * @param analysisScriptVerId
     * @return all AnalysisFile based on analysisScriptVerId
     * @throws JDAOException
     */
    public List<Jstruct_AnalysisFile> getByAnalysisScriptVerId(Long analysisScriptVerId) throws JDAOException 
    {
        return analysisFileDao.getByAnalysisScriptVerId(analysisScriptVerId);
    }


    


    
    
    
    
    
    
    
    
 
    
    
}
