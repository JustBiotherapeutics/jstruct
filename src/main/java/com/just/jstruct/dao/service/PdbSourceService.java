package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbSourceDAO;
import com.just.jstruct.dao.interfaces.IPdbSourceDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbSource;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbSourceService implements Serializable {


    /* dao classes */
    
    private final IPdbSourceDAO pdbSourceDao;


    
    
    /* constructor */
    
    public PdbSourceService() {
        pdbSourceDao = new PdbSourceDAO();
    }



    

    /**
     * get a list of PdbSources based on a structVerId
     * @param structVerId of PdbSources to find
     * @return PdbSources that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbSource> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbSourceDao.getByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbSources related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbSourceDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbSources
     * @param pdbSources
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbSource> batchInsert(List<Jstruct_PdbSource> pdbSources) throws JDAOException {
        return pdbSourceDao.batchInsert(pdbSources);
    }
    
    
    /**
     * insert a batch/list of PdbSources for a specific structVerId
     * @param structVerId
     * @param pdbSources
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbSource> batchInsert(Long structVerId, List<Jstruct_PdbSource> pdbSources) throws JDAOException {
        if(CollectionUtil.hasValues(pdbSources))
        {
            pdbSources.stream().forEach((pdbSource) -> {
                pdbSource.setStructVerId(structVerId);
            });
            
            return batchInsert(pdbSources);
        }
        return null;
    }
    
    
    
    
    
}
