package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.SearchParamDAO;
import com.just.jstruct.dao.interfaces.ISearchParamDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SearchParam;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SearchParamService implements Serializable {


    /* dao classes */
    
    private final ISearchParamDAO searchParamDao;


    
    
    /* constructor */
    
    public SearchParamService() {
        searchParamDao = new SearchParamDAO();
    }



    
  
    /**
     * 
     * @param searchParamId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_SearchParam getBySearchParamId(Long searchParamId) throws JDAOException {
        return searchParamDao.getBySearchParamId(searchParamId);
    }

    
    
    
    /**
     * 
     * @param searchSubqueryId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_SearchParam> getBySearchSubqueryId(Long searchSubqueryId) throws JDAOException {
        return searchParamDao.getBySearchSubqueryId(searchSubqueryId);
    }

  

    
    /**
     * 
     * @param searchParam
     * @return
     * @throws JDAOException 
     */
    public Jstruct_SearchParam insert(Jstruct_SearchParam searchParam) throws JDAOException {
        Jstruct_SearchParam insertedSearchParam = searchParamDao.add(searchParam);
        return insertedSearchParam; 
    }
    
    
    
    /**
     * 
     * @param doomedSearchParam
     * @throws JDAOException 
     */
    public void delete(Jstruct_SearchParam doomedSearchParam) throws JDAOException {
        searchParamDao.delete(doomedSearchParam);
    }
    
    
   
    
    
  
    
    
}
