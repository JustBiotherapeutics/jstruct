package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.StructureUserDAO;
import com.just.jstruct.dao.interfaces.IStructureUserDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_StructureUser;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureUserService implements Serializable {


    /* dao classes */
    
    private final IStructureUserDAO structureUserDao;


    
    
    /* constructor */
    
    public StructureUserService() {
        structureUserDao = new StructureUserDAO();
    }

    

    
    // ======================================================================================================
    // FAVORITE 
    // ======================================================================================================

    
    /**
     * get 'favorite structures' for a user 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_StructureUser> getFavoritesByUserId(Long userId) throws JDAOException {
        return structureUserDao.getFavoritesByUserId(userId);
    }
    
    /**
     * get a 'favorite structure' for a user and structureId
     * @param structureId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructureUser getFavoriteByStructureIdUserId(Long structureId, Long userId) throws JDAOException {
        return structureUserDao.getFavoriteByStructureIdUserId(structureId, userId);
    }

    /**
     * determine if a 'favorite structure' exists for a user, by it's structure id
     * @param structureId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public boolean favoriteExists(Long structureId, Long userId) throws JDAOException {
        Jstruct_StructureUser structureUser = getFavoriteByStructureIdUserId(structureId, userId);
        return structureUser!=null;
    }

    /**
     * add a 'favorite structure' to a users list
     * @param structureId
     * @param structVerId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructureUser addFavoriteToList(Long structureId, Long structVerId, Long userId) throws JDAOException {
        //first check if it already exists
        if(favoriteExists(structureId, userId)){
            return getFavoriteByStructureIdUserId(structureId, userId);
        } else {
            //else create a new one and return it
            Jstruct_StructureUser newStructureUser = new Jstruct_StructureUser();
            newStructureUser.setFavorite(true);
            newStructureUser.setRecent(false);
            newStructureUser.setStructureId(structureId);
            newStructureUser.setStructVerId(structVerId);
            newStructureUser.setUserId(userId);
            newStructureUser.setInsertDate(new Date());
            newStructureUser = structureUserDao.add(newStructureUser);
            return newStructureUser;
        }
    }

    /**
     * remove a 'favorite structure' from a users list
     * @param structureId
     * @param userId
     * @throws JDAOException 
     */
    public void removeFavoriteFromList(Long structureId, Long userId) throws JDAOException {
        Jstruct_StructureUser doomedStructureUser = getFavoriteByStructureIdUserId(structureId, userId);
        structureUserDao.delete(doomedStructureUser);
    }
    
    
    
    
    // ======================================================================================================
    // RECENT 
    // ======================================================================================================


    
    /**
     * get 'recent structures' for a user (up to a certain amount)
     * @param userId
     * @param maxCountReturned
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_StructureUser> getRecentsByUserId(Long userId, Integer maxCountReturned) throws JDAOException {
        return structureUserDao.getRecentsByUserId(userId, maxCountReturned);
    }
    
    /**
     * get a 'recent structure' for a user and structVerID
     * @param structVerId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructureUser getRecentByStructVerIdUserId(Long structVerId, Long userId) throws JDAOException {
        return structureUserDao.getRecentByStructVerIdUserId(structVerId, userId);
    }
    
    /**
     * return true if a 'recent structure' is already in the database for this user
     * @param structVerId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public boolean recentExists(Long structVerId, Long userId) throws JDAOException {
        Jstruct_StructureUser structureUser = getRecentByStructVerIdUserId(structVerId, userId);
        return structureUser!=null;
    }

    /**
     * add a 'recent structure' to the list, if it's already there increment it's count
     * @param structureId
     * @param structVerId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructureUser addRecentToList(Long structureId, Long structVerId, Long userId) throws JDAOException {
        //first check if it already exists
        if(recentExists(structVerId, userId)){
            //if so, increment by one, resave, return it.
            Jstruct_StructureUser su = getRecentByStructVerIdUserId(structVerId, userId);
            su.setRecentCount(su.getRecentCount()+1);
            su.setInsertDate(new Date());
            su = structureUserDao.update(su);
            return su;
        } else {
            //else create a new one and return it
            Jstruct_StructureUser newSu = new Jstruct_StructureUser();
            newSu.setRecentCount(1);
            newSu.setFavorite(false);
            newSu.setRecent(true);
            newSu.setStructureId(structureId);
            newSu.setStructVerId(structVerId);
            newSu.setUserId(userId);
            newSu.setInsertDate(new Date());
            newSu = structureUserDao.add(newSu);
            return newSu;
        }
    }
    
    
    
    
 
    
    
}
