package com.just.jstruct.dao.service;

import com.just.bio.structure.Structure;
import com.just.bio.structure.exceptions.StructureParseException;
import com.just.jstruct.dao.hibernate.StructFileDAO;
import com.just.jstruct.dao.hibernate.StructFileDataDAO;
import com.just.jstruct.dao.interfaces.IStructFileDAO;
import com.just.jstruct.dao.interfaces.IStructFileDataDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_StructFile;
import com.just.jstruct.model.Jstruct_User;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructFileService implements Serializable {


    /* dao classes */
    private final IStructFileDAO structFileDao;
    private final IStructFileDataDAO structFileDataDao;


    /* constructor */
    public StructFileService() {
        structFileDao = new StructFileDAO();
        structFileDataDao = new StructFileDataDAO();
    }



    /**
     * get a StructFile based on it's struct_ver_id
     * @param structVerId of StructFile to find
     * @return structFile that matches structVerId
     * @throws JDAOException
     */
    public Jstruct_StructFile getByStructVerId(Long structVerId) throws JDAOException {
        return structFileDao.getByStructVerId(structVerId);
    }

    

    /**
     * get all structFiles that match the structVerId list
     * @param structVerIds
     * @return 
     * @throws JDAOException 
     */
    public List<Jstruct_StructFile> getByStructVerIds(List<Long> structVerIds) throws JDAOException {
        return structFileDao.getByStructVerIds(structVerIds);
    }
    
    
    
    /**
     * 
     * @param pdbIdentifier
     * @param currentUser
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructFile getPdbImportedRowByPdbIdentifier(String pdbIdentifier, Jstruct_User currentUser) throws JDAOException 
    {
        FullStructureService fullStructureService = new FullStructureService(currentUser);
        Jstruct_FullStructure fullStructure = fullStructureService.getRcsbImportedRowByPbdIdentifier(pdbIdentifier, false);
        if (null == fullStructure) 
        {
            return null;
        }
        return getByStructVerId(fullStructure.getStructVerId());
    }

    /**
     * persist a new structFile - note: this does not save the file contents into the data field, use saveDataFromFile(..) for that.
     * @param structFile
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructFile add(Jstruct_StructFile structFile) throws JDAOException {
        return structFileDao.add(structFile);
    }
    
    
    /**
     * update an existing structFile - note: this does not save the file contents into the data field, use saveDataFromFile(..) for that.
     * @param structFile
     * @return
     * @throws JDAOException 
     */
    public Jstruct_StructFile update(Jstruct_StructFile structFile) throws JDAOException {
        return structFileDao.update(structFile);
    }
    
    
    
    
    
    /**
     * 
     * @param structVerId
     * @return
     * @throws JDAOException 
     * @throws JException 
     */
    public Structure getAsStructure(Long structVerId) throws JDAOException, JException 
    {
        Structure structure;
        try 
        {
               structure = structFileDataDao.getAsStructure(structVerId);
        } 
        catch (SQLException sqlex){
            throw new JDAOException("Error (Database) retrieving structure for structVerId: " + structVerId, sqlex);
        } catch (IOException | StructureParseException ex){
            throw new JException("Error (IO/Parsing) retrieving structure for structVerId: " + structVerId, ex);
        }
        return structure;
    }
    
    
    
    
    
    /**
     * 
     * @param file
     * @param structVerId
     * @throws JDAOException 
     */
    public void saveDataFromFile(File file, Long structVerId) throws JDAOException {
        try {
               structFileDataDao.saveDataFromFile(file, structVerId);
               
        } catch(SQLException | IOException ex){
            throw new JDAOException("Error saving file data for structVerId: " + structVerId, ex);
        }
    }
    
    
    /**
     * 
     * @param structVerId
     * @param outputStream
     * @throws JDAOException 
     */
    public void getData(Long structVerId, OutputStream outputStream) throws JDAOException {
        try {
               structFileDataDao.getData(structVerId, outputStream);
               
        } catch(SQLException | IOException ex){
            throw new JDAOException("Error retrieving file data for structVerId: " + structVerId, ex);
        }
    }
    
    
    /**
     * 
     * @param structVerId
     * @param outputStream
     * @throws JDAOException 
     */
    public void getCompressedData(Long structVerId, OutputStream outputStream) throws JDAOException {
        try {
               structFileDataDao.getCompressedData(structVerId, outputStream);
               
        } catch(SQLException | IOException ex){
            throw new JDAOException("Error retrieving (compressed) file data for structVerId: " + structVerId, ex);
        }
    }

    
    
}
