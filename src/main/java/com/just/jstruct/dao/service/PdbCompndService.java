package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbCompndDAO;
import com.just.jstruct.dao.interfaces.IPdbCompndDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbCompnd;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbCompndService implements Serializable {


    /* dao classes */
    
    private final IPdbCompndDAO pdbCompndDao;


    
    
    /* constructor */
    
    public PdbCompndService() {
        pdbCompndDao = new PdbCompndDAO();
    }



    

    /**
     * get a list of PdbCompnds based on a structVerId
     * @param structVerId of PdbCompnds to find
     * @return PdbCompnds that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbCompnd> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbCompndDao.getByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbCompnds related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbCompndDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbCompnds
     * @param pdbCompnds
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbCompnd> batchInsert(List<Jstruct_PdbCompnd> pdbCompnds) throws JDAOException {
        return pdbCompndDao.batchInsert(pdbCompnds);
    }
    
    
    /**
     * insert a batch/list of PdbCompnds for a specific structVerId
     * @param structVerId
     * @param pdbCompnds
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbCompnd> batchInsert(Long structVerId, List<Jstruct_PdbCompnd> pdbCompnds) throws JDAOException {
        if(CollectionUtil.hasValues(pdbCompnds)){
            pdbCompnds.stream().forEach((pdbCompnd) -> {
                pdbCompnd.setStructVerId(structVerId);
            });
            return batchInsert(pdbCompnds);
        }
        return null;
    }
    
    
    
    
    
}
