package com.just.jstruct.dao.service;

import com.hfg.util.StringUtil;
import com.just.jstruct.dao.hibernate.AnalysisTaskDAO;
import com.just.jstruct.dao.interfaces.IAnalysisTaskDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisRun;
import com.just.jstruct.model.Jstruct_AnalysisTask;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisTaskService implements Serializable {

    /* -------------------------------------------------------------------------
        dao classes 
    ------------------------------------------------------------------------- */
    
    private final IAnalysisTaskDAO analysisTaskDao;


    /* -------------------------------------------------------------------------
        constructor 
    ------------------------------------------------------------------------- */
    
    public AnalysisTaskService() {
        analysisTaskDao = new AnalysisTaskDAO();
    }


    
    /* -------------------------------------------------------------------------
        methods 
    ------------------------------------------------------------------------- */
    
    /**
     * add a new AnalysisTask to the database
     * @param analysisTask to add
     * @return the newly persisted analysisTask
     * @throws JDAOException 
     */
    public Jstruct_AnalysisTask add(Jstruct_AnalysisTask analysisTask) throws JDAOException 
    {
        if(analysisTask.getStartDate() == null){
            analysisTask.setStartDate(new Date()); 
        }
        
        if(!StringUtil.isSet(analysisTask.getStatus())){
            analysisTask.setStatus(Jstruct_AnalysisRun.STATUS_NOT_STARTED);  
        }
        
        Jstruct_AnalysisTask newAnalysisTask = analysisTaskDao.add(analysisTask);
        return newAnalysisTask; 
    }
    
    
    /**
     * update a AnalysisTask in the database
     * @param analysisTask to update
     * @return the updated analysisTask from the database
     * @throws JDAOException
     */
    public Jstruct_AnalysisTask update(Jstruct_AnalysisTask analysisTask) throws JDAOException 
    {
        Jstruct_AnalysisTask updatedAnalysisTask = analysisTaskDao.update(analysisTask);
        return updatedAnalysisTask;
    }
    
    
    
    
    
    
    
    
    
    /**
     * get a AnalysisTask based on it's primary key
     * @param id of AnalysisTask to find
     * @return AnalysisTask that matches id
     * @throws JDAOException
     */
    public Jstruct_AnalysisTask getById(Long id) throws JDAOException 
    {
        return analysisTaskDao.getById(id);
    }
    
    
  
    /**
     * return collection of all AnalysisTask for a status
     * @param status
     * @return all AnalysisTask based on status
     * @throws JDAOException
     */
    public List<Jstruct_AnalysisTask> getByStatus(String status) throws JDAOException 
    {
        return analysisTaskDao.getByStatus(status);
    }
    
    
    
    /**
     * return collection of all AnalysisTask for a analysisRunId
     * @param analysisRunId
     * @return all AnalysisTask based on analysisRunId
     * @throws JDAOException
     */
    public List<Jstruct_AnalysisTask> getByAnalysisRunId(Long analysisRunId) throws JDAOException 
    {
        return analysisTaskDao.getByAnalysisRunId(analysisRunId);
    }


    


    
    
    
    
    
    
    
    
 
    
    
}
