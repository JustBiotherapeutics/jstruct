package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.FullStructureDAO;
import com.just.jstruct.dao.interfaces.IFullStructureDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_PdbSplit;
import com.just.jstruct.model.Jstruct_StructureUser;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.JstructId;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class FullStructureService implements Serializable
{


    /* dao classes */
    
    private final IFullStructureDAO fullStructureDao;

    
    
    /* constructor */
    
    public FullStructureService(Jstruct_User currentUser) throws JDAOException
    {
        this.fullStructureDao = new FullStructureDAO(currentUser); 
    }



    
    /**
     * 
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_FullStructure getByStructVerId(Long structVerId) throws JDAOException
    {
        return fullStructureDao.getByStructVerId(structVerId);
    }
    
    
    /**
     * 
     * @param structVerIds
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_FullStructure> getByStructVerIds(List<Long> structVerIds) throws JDAOException
    {
        List<Jstruct_FullStructure> fullStructures = fullStructureDao.getByStructVerIds(structVerIds);
        return fullStructures;
    }
    
    
    /**
     * 
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_FullStructure> getByStructureId(Long structureId) throws JDAOException
    {
        List<Jstruct_FullStructure> fullStructures = fullStructureDao.getByStructureId(structureId);
        return fullStructures;
    }
    
    
    
    /**
     * 
     * @param jstructId
     * @param getCurrent
     * @return
     * @throws JDAOException 
     */
    public Jstruct_FullStructure getByJstructId(JstructId jstructId, boolean getCurrent) throws JDAOException
    {
        if(jstructId.isInvalidFormat())
        {
            return null;
        }

        if(jstructId.hasStructureId())
        {
            return getByStructureIdAndVersion(jstructId.getStructureId(), jstructId.getStructureVersion(), getCurrent);
        } 
        else if (jstructId.hasRcsbId())
        {
            return getRcsbImportedRowByPbdIdentifier(jstructId.getRcsbId(), getCurrent);
        }

        return null;
    }
    
    
    /**
     * 
     * @param structureId
     * @param version
     * @param getCurrent
     * @return
     * @throws JDAOException 
     */
    public Jstruct_FullStructure getByStructureIdAndVersion(Long structureId, Integer version, boolean getCurrent) throws JDAOException
    {
        return fullStructureDao.getByStructureIdAndVersion(structureId, version, getCurrent);
    }
    
    
    /**
     * 
     * @param pdbIdentifier
     * @param jumpToCurrent
     * @return
     * @throws JDAOException 
     */
    public Jstruct_FullStructure getRcsbImportedRowByPbdIdentifier(String pdbIdentifier, boolean jumpToCurrent) throws JDAOException
    {
        return fullStructureDao.getRcsbImportedRowByPbdIdentifier(pdbIdentifier, jumpToCurrent);
    }
    
    
    /**
     * 
     * @return
     * @throws JDAOException 
     */
    public List<String> getAllPdbIdsFromRcsbRun() throws JDAOException
    {
        return fullStructureDao.getPdbIdsFromRcsbRun(true, true);
    }
    
    /**
     * 
     * @return
     * @throws JDAOException 
     */
    public List<String> getActivePdbIdsFromRcsbRun() throws JDAOException
    {
        return fullStructureDao.getPdbIdsFromRcsbRun(true, false);
    }
    
    /**
     * 
     * @return
     * @throws JDAOException 
     */
    public List<String> getObsoletePdbIdsFromRcsbRun() throws JDAOException
    {
        return fullStructureDao.getPdbIdsFromRcsbRun(false, true);
    }
    
    
    
    /**
     * 
     * @param pdbIds
     * @param onlyFromRcsb
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_FullStructure> getByPdbIds(List<String> pdbIds, boolean onlyFromRcsb) throws JDAOException
    {
        List<Jstruct_FullStructure> fullStructures = fullStructureDao.getByPdbIds(pdbIds, onlyFromRcsb);
        return fullStructures;
    }
    
    
    
    
    
    /**
     * 
     * @param structureUsers
     * @param onlyCurrent
     * @param keepOrder
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_FullStructure> getByStructureUsers(List<Jstruct_StructureUser> structureUsers, boolean onlyCurrent, boolean keepOrder) throws JDAOException
    {
        
        if(CollectionUtil.hasValues(structureUsers))
        {
            
            //get the structVerIds from our list
            List<Long> structVerIds = new ArrayList<>(structureUsers.size());
            for(Jstruct_StructureUser su : structureUsers)
            {
                structVerIds.add(su.getStructVerId());
            }
            
            //get all the fullStructures based on our list of structVerIds
            List<Jstruct_FullStructure> fullStructures = new ArrayList();
            if(keepOrder)
            {
                //if we want to keep the order that the structVerIds came back in, we gotta add them to our arraylist one at a time. ug.
                for(Long structVerId : structVerIds)
                {
                    fullStructures.add(fullStructureDao.getByStructVerId(structVerId));
                }
            } 
            else
            {
                fullStructures = fullStructureDao.getByStructVerIds(structVerIds);
            }
            
            
            //if only current, 'swap out' previous versions with it's current version
            if(onlyCurrent)
            {
                List<Jstruct_FullStructure> mostRecentFullStructures = new ArrayList<>(fullStructures.size());
                for(Jstruct_FullStructure fs : fullStructures)
                {
                    if(!fs.isMostCurrent())
                    {
                        fs = fullStructureDao.getByStructureIdAndVersion(fs.getStructureId(), fs.getMaxVersion(), true); //the 'true' here flags to get the most current regardless of requested 
                    }
                    mostRecentFullStructures.add(fs);
                }
                fullStructures = mostRecentFullStructures;
            } 
            
            return fullStructures; 
            
        }
        return null;
    }
    
    
    
    public List<Jstruct_FullStructure> getByPdbSplits(List<Jstruct_PdbSplit> pdbSplits) throws JDAOException
    {
        if(CollectionUtil.hasValues(pdbSplits))
        {
            List<String> pdbIds = new ArrayList<>(pdbSplits.size());
            for(Jstruct_PdbSplit pdbSplit : pdbSplits)
            {
                pdbIds.add(pdbSplit.getPdbIdentifier());
            }
            return getByPdbIds(pdbIds, true);
        }
        return null;
    }
    
    
    
    
    public Jstruct_FullStructure getRandomActiveRcsbFullStructure() throws JDAOException
    {
        return fullStructureDao.getRandomActiveRcsbFullStructure();
    }
    
    
    public List<Jstruct_FullStructure> getFullStructureLineageBasedOnPdbIdentifier(String inPbdIdentifier) throws JDAOException
    {
        return fullStructureDao.getFullStructureLineageBasedOnPdbIdentifier(inPbdIdentifier);
    }
    
    
    
}
