package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.ClientDAO;
import com.just.jstruct.dao.interfaces.IClientDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Client;
import com.just.jstruct.model.Jstruct_User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ClientService implements Serializable {


    /* dao classes */
    
    private final IClientDAO clientDao;


    
    
    /* constructor */
    
    public ClientService() {
        clientDao = new ClientDAO();
    }



    /**
     * return collection of all Clients
     * @return all JstructUsers
     * @throws JDAOException
     */
    public List<Jstruct_Client> getAll() throws JDAOException {
        return clientDao.getAll();
    }


    /**
     * get a Client based on it's primary key
     * @param clientId of Client to find
     * @return client that matches clientId
     * @throws JDAOException
     */
    public Jstruct_Client getByClientId(Long clientId) throws JDAOException {
        return clientDao.getByClientId(clientId);
    }


    /**
     * get a Client based on it's name
     * @param name of Client to find
     * @return client that matches name
     * @throws JDAOException
     */
    public Jstruct_Client getByName(String name) throws JDAOException {
        return clientDao.getByName(name);
    }
    
    
    /**
     * determine if a client name already exists; use clientIdToIgnore if editing
     * @param name
     * @param clientIdToIgnore
     * @return
     * @throws JDAOException 
     */
    public boolean clientExists(String name, Long clientIdToIgnore) throws JDAOException{
        return clientDao.clientExists(name, clientIdToIgnore);
    }
    
    
    /**
     * determine if a client name already exists
     * @param name
     * @return
     * @throws JDAOException 
     */
    public boolean clientExists(String name) throws JDAOException{
        return clientExists(name, null);
    }
    
    
    /**
     * determine if a client is used (e.g in the struct_version table)
     * @param clientId
     * @return
     * @throws JDAOException 
     */
    public boolean isUsed(Long clientId) throws JDAOException{
        return clientDao.isUsed(clientId);
    }
    
    
    
    
    
    /**
     * update a Client in the database
     * @param client to update
     * @return the updated client from the database
     * @throws JDAOException
     */
    public Jstruct_Client updateClient(Jstruct_Client client) throws JDAOException {
        Jstruct_Client updatedClient = clientDao.update(client);
        return updatedClient;
    }
    
    
    /**
     * add a new client to the database
     * @param client to add
     * @param user
     * @return the brand new client
     * @throws JDAOException 
     */
    public Jstruct_Client addClient(Jstruct_Client client, Jstruct_User user) throws JDAOException {
        
        client.setInsertDate(new Date());
        client.setUpdateDate(new Date());
        
        client.setInsertUser(user);
        client.setUpdateUser(user);

        Jstruct_Client updatedClient = clientDao.add(client);
        return updatedClient; 
    }
    
    
    
 
    
    
}
