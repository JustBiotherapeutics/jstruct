package com.just.jstruct.dao.service;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbJrnlDAO;
import com.just.jstruct.dao.interfaces.IPdbJrnlDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbJrnl;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbJrnlService implements Serializable {


    /* dao classes */
    
    private final IPdbJrnlDAO pdbJrnlDao;


    
    
    /* constructor */
    
    public PdbJrnlService() {
        pdbJrnlDao = new PdbJrnlDAO();
    }



    

    /**
     * get a list of PdbJrnls based on a structVerId
     * @param structVerId of PdbJrnls to find
     * @return PdbJrnls that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbJrnl> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbJrnlDao.getByStructVerId(structVerId);
    }

    
    /**
     * get the primary PdbJrnl based on a structVerId
     * @param structVerId of PdbJrnl to find
     * @param forceSpaceBetweenAuthorNames true to force a space after commas and semi-colons in the author names if not already there (helps with word-wrap on display)
     * @return PdbJrnl that matches structVerId
     * @throws JDAOException
     */
    public Jstruct_PdbJrnl getPrimaryByStructVerId(Long structVerId, boolean forceSpaceBetweenAuthorNames) throws JDAOException {
        Jstruct_PdbJrnl jrnl = pdbJrnlDao.getPrimaryByStructVerId(structVerId);
        if(jrnl!=null){
            if(forceSpaceBetweenAuthorNames && StringUtil.isSet(jrnl.getAuth())){
                String auth = jrnl.getAuth();
                auth = auth.replaceAll("[,;]", "$0 ").replaceAll("\\s+", " ");
                jrnl.setAuth(auth);
            }
        }
        return jrnl;
    }

    
    
    
    
    /**
     * delete all PdbJrnls related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbJrnlDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbJrnls
     * @param pdbJrnls
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbJrnl> batchInsert(List<Jstruct_PdbJrnl> pdbJrnls) throws JDAOException {
        return pdbJrnlDao.batchInsert(pdbJrnls);
    }
    
    
    /**
     * insert a batch/list of PdbJrnls for a specific structVerId
     * @param structVerId
     * @param pdbJrnls
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbJrnl> batchInsert(Long structVerId, List<Jstruct_PdbJrnl> pdbJrnls) throws JDAOException {
        if(CollectionUtil.hasValues(pdbJrnls)){
            pdbJrnls.stream().forEach((pdbJrnl) -> {
                pdbJrnl.setStructVerId(structVerId);
            });
            return batchInsert(pdbJrnls);
        }
        return null;
    }
    
    
    public Jstruct_PdbJrnl update(Jstruct_PdbJrnl pdbJrnl) throws JDAOException {
        return pdbJrnlDao.update(pdbJrnl);
    }
    
    
}
