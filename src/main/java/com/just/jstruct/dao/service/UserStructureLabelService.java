package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.UserStructureLabelDAO;
import com.just.jstruct.dao.interfaces.IUserStructureLabelDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_UserStructureLabel;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class UserStructureLabelService implements Serializable {


    /* dao classes */
    
    private final IUserStructureLabelDAO userStructureLabelDao;


    
    
    /* constructor */
    
    public UserStructureLabelService() {
        userStructureLabelDao = new UserStructureLabelDAO();
    }


    
    /**
     * 
     * @param userId
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_UserStructureLabel> getByUserIdAndStructureId(Long userId, Long structureId) throws JDAOException {
        return userStructureLabelDao.getByUserIdAndStructureId(userId, structureId);
    }
    
    
    /**
     * return collection of all UserStructureLabels for a user
     * @param userId
     * @return all JstructUserStructureLabels based on userId
     * @throws JDAOException
     */
    public List<Jstruct_UserStructureLabel> getByUserId(Long userId) throws JDAOException {
        return userStructureLabelDao.getByUserId(userId);
    }


    


    
    
    
    
    
    
 
    
    
}
