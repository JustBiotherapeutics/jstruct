package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbKeywordDAO;
import com.just.jstruct.dao.interfaces.IPdbKeywordDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbKeyword;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbKeywordService implements Serializable {


    /* dao classes */
    
    private final IPdbKeywordDAO pdbKeywordDao;


    
    
    /* constructor */
    
    public PdbKeywordService() {
        pdbKeywordDao = new PdbKeywordDAO();
    }



    

    /**
     * get a list of PdbKeywords based on a structVerId
     * @param structVerId of PdbKeywords to find
     * @return PdbKeywords that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_PdbKeyword> getByStructVerId(Long structVerId) throws JDAOException {
        return pdbKeywordDao.getByStructVerId(structVerId);
    }

    
    /**
     * delete all PdbKeywords related to a structVerId
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        pdbKeywordDao.deleteByStructVerId(structVerId);
    }
    
    
    /**
     * insert a batch/list of PdbKeywords
     * @param pdbKeywords
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbKeyword> batchInsert(List<Jstruct_PdbKeyword> pdbKeywords) throws JDAOException {
        return pdbKeywordDao.batchInsert(pdbKeywords);
    }
    
    
    /**
     * insert a batch/list of PdbKeywords for a specific structVerId
     * @param structVerId
     * @param pdbKeywords
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_PdbKeyword> batchInsert(Long structVerId, List<Jstruct_PdbKeyword> pdbKeywords) throws JDAOException {
        if(CollectionUtil.hasValues(pdbKeywords)){
            pdbKeywords.stream().forEach((pdbKeyword) -> {
                pdbKeyword.setStructVerId(structVerId);
            });
            return batchInsert(pdbKeywords);
        }
        return null;
    }
    
    
    
    
    
}
