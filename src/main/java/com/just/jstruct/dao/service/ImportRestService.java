package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.ImportRestDAO;
import com.just.jstruct.dao.interfaces.IImportRestDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportRest;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportRestService implements Serializable {


    /* dao classes */
    
    private final IImportRestDAO importRestDao;


    
    
    /* constructor */
    
    public ImportRestService() {
        importRestDao = new ImportRestDAO();
    }



    /**
     * get a ImportRest based on it's primary key
     * @param importRestId of ImportRest to find
     * @return importRest that matches importRestId
     * @throws JDAOException
     */
    public Jstruct_ImportRest getByImportRestId(Long importRestId) throws JDAOException {
        return importRestDao.getByImportRestId(importRestId);
    }
    
    
    
    /**
     * 
     * @param importJobId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_ImportRest> getByImportJobId(Long importJobId) throws JDAOException {
        return importRestDao.getByImportJobId(importJobId);
    }
    
    
    
    
    /**
     * persist a new importRest 
     * @param importRest
     * @return
     * @throws JDAOException 
     */
    public Jstruct_ImportRest add(Jstruct_ImportRest importRest) throws JDAOException {
        
        Jstruct_ImportRest savedImportRest =  importRestDao.add(importRest);
        
        //after saving the importRest, save it's collection of importPdbs
        if(CollectionUtil.hasValues(importRest.getImportPdbs())){
            
            ImportPdbService importPdbService = new ImportPdbService();
            importPdbService.bulkInsert(importRest.getImportPdbs(), savedImportRest.getImportRestId());
            
        }
        
        return savedImportRest;
    }
    
    
    
    
}
