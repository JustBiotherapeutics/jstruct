package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.NoteDAO;
import com.just.jstruct.dao.interfaces.INoteDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Note;
import com.just.jstruct.model.Jstruct_User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class NoteService implements Serializable {


    /* dao classes */
    
    private final INoteDAO noteDao;


    
    
    /* constructor */
    
    public NoteService() {
        noteDao = new NoteDAO();
    }

    

    
    
    
    /**
     * get note by Id 
     * @param noteId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Note getByNoteId(Long noteId) throws JDAOException {
        return noteDao.getByNoteId(noteId);
    }
    
    /**
     * get all notes for a structureId that a user can view/edit (all public notes, plus notes that this user has entered) 
     * @param structureId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_Note> getByStructureIdForUser(Long structureId, Long userId) throws JDAOException {
        return noteDao.getByStructureIdForUser(structureId, userId);
    }
    
    
    
    /**
     * get all notes for a structVerId that a user can view/edit (all public notes, plus notes that this user has entered) 
     * @param structVerId
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_Note> getByStructVerIdForUser(Long structVerId, Long userId) throws JDAOException {
        return noteDao.getByStructVerIdForUser(structVerId, userId);
    }

    

    /**
     * create an add a note for a user/structure
     * @param structureId
     * @param structVerId
     * @param formattedComment
     * @param lowerComment
     * @param isPublic
     * @param insertUser
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Note createNote(Long structureId, Long structVerId, String formattedComment, String lowerComment, boolean isPublic, Jstruct_User insertUser) throws JDAOException {
        
        //else create a new one and return it
        Jstruct_Note newNote = new Jstruct_Note();
        newNote.setStructureId(structureId);
        newNote.setStructVerId(structVerId);
        newNote.setFormattedComment(formattedComment);
        newNote.setLowerComment(lowerComment);
        newNote.setNotePublic(isPublic);

        newNote.setInsertUser(insertUser);
        newNote.setInsertDate(new Date());
        newNote.setUpdateUser(insertUser);
        newNote.setUpdateDate(new Date());

        newNote = noteDao.add(newNote);
        return newNote;
    }

  
    
    /**
     * update an existing note with it's new values
     * @param note
     * @param updateUser
     * @return 
     * @throws com.just.jstruct.exception.JDAOException 
     */
    public Jstruct_Note updateNote(Jstruct_Note note, Jstruct_User updateUser) throws JDAOException {
        note.setUpdateUser(updateUser);
        note.setUpdateDate(new Date());
        note = noteDao.update(note);
        return note;
    }
    
    
    
    /**
     * remove a note from the database (permanent deletion)
     * @param doomedNote 
     * @throws com.just.jstruct.exception.JDAOException 
     */
    public void deleteNote(Jstruct_Note doomedNote) throws JDAOException {
        noteDao.delete(doomedNote);
    }
    
    
 
    
    
    
}
