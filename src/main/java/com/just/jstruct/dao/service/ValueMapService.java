package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.ValueMapDAO;
import com.just.jstruct.dao.interfaces.IValueMapDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ValueMap;
import com.just.jstruct.pojos.ValueMapGroup;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ValueMapService implements Serializable {


    /* dao classes */
    
    private final IValueMapDAO valueMapDao;


    
    
    /* constructor */
    
    public ValueMapService() {
        valueMapDao = new ValueMapDAO();
    }



    /**
     * return collection of all ValueMaps
     * @return all ValueMaps
     * @throws JDAOException
     */
    public List<Jstruct_ValueMap> getAll() throws JDAOException {
        return valueMapDao.getAll();
    }


    /**
     * get a ValueMap based on it's primary key
     * @param valueMapId of ValueMap to find
     * @return ValueMap that matches valueMapId
     * @throws JDAOException
     */
    public Jstruct_ValueMap getById(Long valueMapId) throws JDAOException {
        return valueMapDao.getByValueMapId(valueMapId);
    }


    /**
     * return collection of all ValueMaps for a particular group
     * @param itemGroup of ValueMaps to find
     * @return all ValueMaps for a particular itemGroup
     * @throws JDAOException
     */
    public List<Jstruct_ValueMap> getbyGroup(String itemGroup) throws JDAOException {
        return valueMapDao.getbyGroup(itemGroup);
    }


    /**
     * get a ValueMap record based on it's itemKey
     * @param itemKey of ValueMap to find
     * @return ValueMap that matches itemKey
     * @throws JDAOException
     */
    public Jstruct_ValueMap getByItemKey(String itemKey) throws JDAOException {
        return valueMapDao.getByItemKey(itemKey);
    }


    /**
     * get a collection of ValueMapGroup objects (each vmg object contains all ValueMap records for a group)
     * @return collection of ValueMapGroup objects
     * @throws JDAOException
     */
    public List<ValueMapGroup> getValueMapGroups() throws JDAOException {
        return valueMapDao.getValueMapGroups();
    }
    
    /**
     * get a map of ValueMap objects (key: valueMap.itemKey)
     * @return map of all valueMaps with itemKey as map key
     * @throws JDAOException
     
    public Map<String, List<Jstruct_ValueMap>> getValueMapGroups() throws JDAOException {
        return valueMapDao.getMapOfValueMaps();
    } */


    /**
     * update a ValueMap in the database
     * @param valueMap to update
     * @return the updated valueMap from the database
     * @throws JDAOException
     */
    public Jstruct_ValueMap updateValueMap(Jstruct_ValueMap valueMap) throws JDAOException {
        
        Jstruct_ValueMap updatedValueMap = valueMapDao.update(valueMap);
        ValueMapUtils vmu = ValueMapUtils.getInstance().refreshInstance();
        return updatedValueMap;
        
    }



    //refresh session bean holding all value map objects - do this when one is updated
//    private void refreshValueMapInSession(){
//        //update the session scoped bean after a successful save of a new value
//        ValueMapAccess dbValue = new ValueMapAccess();
//        FacesUtil.storeOnSession("dbValue", dbValue);
//    }

    
    
    
    
}
