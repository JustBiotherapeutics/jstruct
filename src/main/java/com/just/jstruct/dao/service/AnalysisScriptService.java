package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.AnalysisScriptDAO;
import com.just.jstruct.dao.interfaces.IAnalysisScriptDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisScript;
import com.just.jstruct.model.Jstruct_AnalysisScriptVer;
import com.just.jstruct.model.Jstruct_User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisScriptService implements Serializable {

    /* -------------------------------------------------------------------------
        dao classes , service classes
    ---------------------------------------------------- */
    
    private final IAnalysisScriptDAO analysisScriptDao;
    private final AnalysisScriptVerService scriptVerService = new AnalysisScriptVerService();

    /* -------------------------------------------------------------------------
        constructor 
    ------------------------------------------------------------------------- */
    
    public AnalysisScriptService() {
        analysisScriptDao = new AnalysisScriptDAO();
    }


    
    /* -------------------------------------------------------------------------
        methods 
    ------------------------------------------------------------------------- */
    
    /**
     * add a new AnalysisScript to the database
     * @param analysisScript to add
     * @param user
     * @return the newly persisted analysisScript
     * @throws JDAOException 
     */
    public Jstruct_AnalysisScript add(Jstruct_AnalysisScript analysisScript, Jstruct_User user) throws JDAOException 
    {
        analysisScript.setInsertUser(user); 
        analysisScript.setOwner(user); 
        
        if(analysisScript.getInsertDate() == null){
            analysisScript.setInsertDate(new Date()); 
        }
        
        Jstruct_AnalysisScript newAnalysisScript = analysisScriptDao.add(analysisScript);
        return newAnalysisScript; 
    }
    
    
    /**
     * update a AnalysisScript in the database
     * @param analysisScript to update
     * @return the updated analysisScript from the database
     * @throws JDAOException
     */
    public Jstruct_AnalysisScript update(Jstruct_AnalysisScript analysisScript) throws JDAOException 
    {
        Jstruct_AnalysisScript updatedAnalysisScript = analysisScriptDao.update(analysisScript);
        return updatedAnalysisScript;
    }
    
    
    
    
    
    
    
    
    
    /**
     * get a AnalysisScript based on it's primary key
     * @param id of AnalysisScript to find
     * @param includeVersions
     * @return AnalysisScript that matches id
     * @throws JDAOException
     */
    public Jstruct_AnalysisScript getById(Long id, boolean includeVersions) throws JDAOException 
    {
        Jstruct_AnalysisScript analysisScript = analysisScriptDao.getById(id);
        
        if(includeVersions)
        {
            analysisScript = retrieveScriptVersions(analysisScript);
        }
        return analysisScript;
    }
    
    
  
 
    
    
    /**
     * return collection of all AnalysisScript for a ownerId
     * @param ownerId
     * @param includeVersions
     * @param includeDeleted
     * @return all AnalysisScript based on ownerId
     * @throws JDAOException
     */
    public List<Jstruct_AnalysisScript> getByOwnerId(Long ownerId, boolean includeVersions, boolean includeDeleted) throws JDAOException 
    {
        List<Jstruct_AnalysisScript> analysisScripts = analysisScriptDao.getByOwnerId(ownerId, includeDeleted);
        
        if(includeVersions)
        {
            analysisScripts = retrieveScriptVersions(analysisScripts);
        }
        
        return analysisScripts;
    }
    
    
    /**
     * 
     * @param includeVersions
     * @param includeDeleted
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_AnalysisScript> getGlobal(boolean includeVersions, boolean includeDeleted) throws JDAOException 
    {
        List<Jstruct_AnalysisScript> globalScripts = analysisScriptDao.getAllMarkedAsGlobal(includeDeleted);
        
        if(includeVersions)
        {
            globalScripts = retrieveScriptVersions(globalScripts);
        }
        
        return globalScripts;
    }
    
    

    
    
    /* -------------------------------------------------------------------------
        private 
    ------------------------------------------------------------------------- */
    
    
    
    
    private List<Jstruct_AnalysisScript> retrieveScriptVersions(List<Jstruct_AnalysisScript> analysisScripts) throws JDAOException
    {
        if(CollectionUtil.hasValues(analysisScripts))
        {
            for(Jstruct_AnalysisScript analysisScript : analysisScripts)
            {
                List<Jstruct_AnalysisScriptVer> scriptVers = scriptVerService.getByAnalysisScriptId(analysisScript.getAnalysisScriptId(), false, false);
                analysisScript.addScriptVers(scriptVers); 
            }
        }
        return analysisScripts;
    }
    
    
    private Jstruct_AnalysisScript retrieveScriptVersions(Jstruct_AnalysisScript analysisScript) throws JDAOException
    {
        if(analysisScript != null)
        {
            List<Jstruct_AnalysisScriptVer> scriptVers = scriptVerService.getByAnalysisScriptId(analysisScript.getAnalysisScriptId(), false, false);
            analysisScript.addScriptVers(scriptVers); 
        }
        return analysisScript;
    }

    


    
    
    
    
    
    
    
    
    
 
    
    
}
