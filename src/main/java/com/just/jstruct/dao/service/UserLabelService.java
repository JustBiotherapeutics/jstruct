package com.just.jstruct.dao.service;

import com.hfg.html.attribute.HTMLColor;
import com.just.jstruct.dao.hibernate.UserLabelDAO;
import com.just.jstruct.dao.interfaces.IUserLabelDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserLabel;
import com.just.jstruct.utilities.JStringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class UserLabelService implements Serializable {


    /* dao classes */
    
    private final IUserLabelDAO userLabelDao;


    
    
    /* constructor */
    
    public UserLabelService() {
        userLabelDao = new UserLabelDAO();
    }


    
    /**
     * get a UserLabel based on it's primary key
     * @param userLabelId of UserLabel to find
     * @return userLabel that matches userLabelId
     * @throws JDAOException
     */
    public Jstruct_UserLabel getByUserLabelId(Long userLabelId) throws JDAOException {
        return userLabelDao.getByUserLabelId(userLabelId);
    }
    
    
     /**
     * @param commaSeperatedUserLabelIds
     * @return all JstructUserLabels based on userId
     * @throws JDAOException
     */
    public List<Jstruct_UserLabel> getByCommaSeperatedUserLabelIds(String commaSeperatedUserLabelIds) throws JDAOException {
        String[] userLabelIdParams = StringUtils.split(commaSeperatedUserLabelIds, ",");
        List<Jstruct_UserLabel> userLabels = new ArrayList<>(userLabelIdParams.length);
        for(String userLabelIdParam : userLabelIdParams){
            Long userLabelId = JStringUtil.stringToLong(userLabelIdParam);
            userLabels.add(getByUserLabelId(userLabelId));
        }
        return userLabels;
    }

    
    /**
     * return collection of all UserLabels for a user
     * @param userId
     * @return all JstructUserLabels based on userId
     * @throws JDAOException
     */
    public List<Jstruct_UserLabel> getByUserId(Long userId) throws JDAOException {
        return userLabelDao.getByUserId(userId);
    }


    
    
    /**
     * return collection of all UserLabels for a user and structure
     * @param userId
     * @param structureId
     * @return all JstructUserLabels based on userId/structureId
     * @throws JDAOException
     */
    public List<Jstruct_UserLabel> getByUserIdStructureId(Long userId, Long structureId) throws JDAOException {
        return userLabelDao.getByUserIdStructureId(userId,structureId);
    }


    
    /**
     * 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_UserLabel> getUnusedByUserId(Long userId) throws JDAOException {
        return userLabelDao.getUnusedByUserId(userId);
    }

    


    
    
    
    
    /**
     * update a UserLabel in the database
     * @param userLabel to update
     * @param user
     * @return the updated userLabel from the database
     * @throws JDAOException
     */
    public Jstruct_UserLabel update(Jstruct_UserLabel userLabel, Jstruct_User user) throws JDAOException {
        
        userLabel.setUpdateUser(user);
        userLabel.setUpdateDate(new Date());
        
        String contrastingColor = HTMLColor.valueOf(userLabel.getBackgroundColor()).getContrastingColor().toHex();
        userLabel.setTextColor(contrastingColor.startsWith("#") ? contrastingColor.substring(1) : contrastingColor);
        
        Jstruct_UserLabel updatedUserLabel = userLabelDao.update(userLabel);
        return updatedUserLabel;
    }
    
    
    /**
     * add a new userLabel to the database
     * @param userLabel to add
     * @param user
     * @return the brand new userLabel
     * @throws JDAOException 
     */
    public Jstruct_UserLabel add(Jstruct_UserLabel userLabel, Jstruct_User user) throws JDAOException {
        
        userLabel.setInsertUser(user);
        userLabel.setUpdateUser(user);
        
        userLabel.setInsertDate(new Date());
        userLabel.setUpdateDate(new Date());
        
        String contrastingColor = HTMLColor.valueOf(userLabel.getBackgroundColor()).getContrastingColor().toHex();
        userLabel.setTextColor(contrastingColor.startsWith("#") ? contrastingColor.substring(1) : contrastingColor);
        
        Jstruct_UserLabel updatedUserLabel = userLabelDao.add(userLabel);
        return updatedUserLabel; 
    }
    
    
    
    /**
     * 
     * @param doomedUserLabel
     * @throws JDAOException 
     */
    public void delete(Jstruct_UserLabel doomedUserLabel) throws JDAOException {
        userLabelDao.delete(doomedUserLabel);
    }
    
    
 
    
    
}
