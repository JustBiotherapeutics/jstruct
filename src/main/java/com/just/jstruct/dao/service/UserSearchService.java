package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.UserSearchDAO;
import com.just.jstruct.dao.interfaces.IUserSearchDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SearchSubquery;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserSearch;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class UserSearchService implements Serializable {


    /* dao classes */
    
    private final IUserSearchDAO userSearchDao;


    
    
    /* constructor */
    
    public UserSearchService() {
        userSearchDao = new UserSearchDAO();
    }



    
  
    /**
     * 
     * @param userSearchId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_UserSearch getByUserSearchId(Long userSearchId) throws JDAOException {
        return userSearchDao.getByUserSearchId(userSearchId);
    }

    
    
    
    /**
     * 
     * @param userId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_UserSearch> getByUserId(Long userId) throws JDAOException {
        return userSearchDao.getByUserId(userId);
    }

  
    
    /**
     * 
     * @param userSearch
     * @return
     * @throws JDAOException 
     */
    public Jstruct_UserSearch update(Jstruct_UserSearch userSearch) throws JDAOException {
        Jstruct_UserSearch updatedUserSearch = userSearchDao.update(userSearch);
        return updatedUserSearch;
    }
    
    
    /**
     * 
     * @param userSearch
     * @param user
     * @return
     * @throws JDAOException 
     */
    public Jstruct_UserSearch insert(Jstruct_UserSearch userSearch, Jstruct_User user) throws JDAOException {
        
        userSearch.setInsertDate(new Date());
        userSearch.setUpdateDate(new Date());
        
        userSearch.setInsertUser(user);
        userSearch.setUpdateUser(user);

        Jstruct_UserSearch insertedUserSearch = userSearchDao.add(userSearch);
        return insertedUserSearch; 
    }
    
    
    
    public void delete(Jstruct_UserSearch doomedUserSearch) throws JDAOException {
        //first delete all related subquerys
        SearchSubqueryService searchSubqueryService = new SearchSubqueryService();
        List<Jstruct_SearchSubquery> doomedSearchSubquerys = searchSubqueryService.getByUserSearchId(doomedUserSearch.getUserSearchId());
        if(CollectionUtil.hasValues(doomedSearchSubquerys)){
            for(Jstruct_SearchSubquery doomedSearchSubquery : doomedSearchSubquerys){
                searchSubqueryService.delete(doomedSearchSubquery);
            }
        }
        
        //delete the userSearch
        userSearchDao.delete(doomedUserSearch);
    }
   
    
    
  
    
    
}
