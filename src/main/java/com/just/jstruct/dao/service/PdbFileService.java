package com.just.jstruct.dao.service;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.PdbFileDAO;
import com.just.jstruct.dao.interfaces.IPdbFileDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_PdbAuthor;
import com.just.jstruct.model.Jstruct_PdbFile;
import com.just.jstruct.model.Jstruct_PdbKeyword;
import com.just.jstruct.utilities.JStringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PdbFileService implements Serializable {


    /* dao classes */
    
    private final IPdbFileDAO pdbFileDao;


    
    
    /* constructor */
    
    public PdbFileService() {
        pdbFileDao = new PdbFileDAO();
    }



    

    /**
     * get a PdbFile based on a structVerId
     * @param structVerId of PdbFile to find
     * @return PdbFile that matches structVerId
     * @throws JDAOException
     */
    public Jstruct_PdbFile getByStructVerId(Long structVerId) throws JDAOException {
        
        PdbKeywordService keywordService = new PdbKeywordService();
        PdbAuthorService authorService = new PdbAuthorService();
        
        List<Jstruct_PdbKeyword> keywords = keywordService.getByStructVerId(structVerId);
        List<Jstruct_PdbAuthor> authors = authorService.getByStructVerId(structVerId);
        
        Jstruct_PdbFile pdbFile = pdbFileDao.getByStructVerId(structVerId);
        
        if(CollectionUtil.hasValues(authors)){
            pdbFile.setAuthorString(transformPdbAuthorsIntoString(authors));
        }
        if(CollectionUtil.hasValues(keywords)){
            pdbFile.setKeywordString(transformPdbKeywordsIntoString(keywords));
        }
        
        return pdbFile;
    }
    
    private String transformPdbKeywordsIntoString(List<Jstruct_PdbKeyword> keywords){
        if(CollectionUtil.hasValues(keywords)){
            List<String> kwList = new ArrayList<>(keywords.size());
            for(Jstruct_PdbKeyword keyword : keywords){
                kwList.add(keyword.getValue());
            }
            return StringUtil.join(kwList, ", ");
        }
        return null;
    }
    
    private String transformPdbAuthorsIntoString(List<Jstruct_PdbAuthor> authors){
        if(CollectionUtil.hasValues(authors)){
            List<String> authList = new ArrayList<>(authors.size());
            for(Jstruct_PdbAuthor auth : authors){
                authList.add(auth.getValue());
            }
            return StringUtil.join(authList, ", ");
        }
        return null;
    }

    
    
    
    /**
     * delete a PdbFile 
     * @param doomedPdbFile
     * @throws JDAOException 
     */
    public void delete(Jstruct_PdbFile doomedPdbFile) throws JDAOException {
        
        PdbSplitService pdbSplitService = new PdbSplitService();
        PdbKeywordService pdbKeywordService = new PdbKeywordService();
        PdbAuthorService pdbAuthorService = new PdbAuthorService();
        PdbJrnlService pdbJrnlService = new PdbJrnlService();
        PdbCompndService pdbCompndService = new PdbCompndService();
        PdbSourceService pdbSourceService = new PdbSourceService();
        PdbRevdatService pdbRevdatService = new PdbRevdatService();
        PdbRemarkService pdbRemarkService = new PdbRemarkService();
        PdbSsbondService pdbSsbondService = new PdbSsbondService();
        PdbSeqresService pdbSeqresService = new PdbSeqresService();
        
        Long doomedStructVerId = doomedPdbFile.getStructVerId();
        
        //prior to deleting the file, delete all the Jstruct_Pdb* children
        pdbSplitService.deleteByStructVerId(doomedStructVerId);
        pdbKeywordService.deleteByStructVerId(doomedStructVerId);
        pdbAuthorService.deleteByStructVerId(doomedStructVerId);
        pdbJrnlService.deleteByStructVerId(doomedStructVerId);
        pdbCompndService.deleteByStructVerId(doomedStructVerId);
        pdbSourceService.deleteByStructVerId(doomedStructVerId);
        pdbRevdatService.deleteByStructVerId(doomedStructVerId);
        pdbRemarkService.deleteByStructVerId(doomedStructVerId);
        pdbSsbondService.deleteByStructVerId(doomedStructVerId);
        pdbSeqresService.deleteByStructVerId(doomedStructVerId);
        
        //finally, delete the file
        pdbFileDao.delete(doomedPdbFile);
    }
    
    
    
    
    /**
     * insert a PdbFile, and all related Jstruct_Pdb* children 
     * @param pdbFile
     * @return
     * @throws JDAOException 
     */
    public Jstruct_PdbFile add(Jstruct_PdbFile pdbFile) throws JDAOException {
        
        PdbSplitService pdbSplitService = new PdbSplitService();
        PdbKeywordService pdbKeywordService = new PdbKeywordService();
        PdbAuthorService pdbAuthorService = new PdbAuthorService();
        PdbJrnlService pdbJrnlService = new PdbJrnlService();
        PdbCompndService pdbCompndService = new PdbCompndService();
        PdbSourceService pdbSourceService = new PdbSourceService();
        PdbRevdatService pdbRevdatService = new PdbRevdatService();
        PdbRemarkService pdbRemarkService = new PdbRemarkService();
        PdbSsbondService pdbSsbondService = new PdbSsbondService();
        PdbSeqresService pdbSeqresService = new PdbSeqresService();
        
        
        //first add the pdbFile
        pdbFile = pdbFileDao.add(pdbFile);
        
        //then add all the Jstruct_Pdb* children 
        pdbFile.setPdbSplits(pdbSplitService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbSplits()));
        pdbFile.setPdbKeywords(pdbKeywordService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbKeywords()));
        pdbFile.setPdbAuthors(pdbAuthorService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbAuthors()));
        pdbFile.setPdbJrnls(pdbJrnlService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbJrnls()));
        pdbFile.setPdbCompnds(pdbCompndService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbCompnds()));
        pdbFile.setPdbSources(pdbSourceService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbSources()));
        pdbFile.setPdbRevdats(pdbRevdatService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbRevdats()));
        pdbFile.setPdbRemarks(pdbRemarkService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbRemarks()));
        pdbFile.setPdbSsbonds(pdbSsbondService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbSsbonds()));
        pdbFile.setPdbSeqress(pdbSeqresService.batchInsert(pdbFile.getStructVerId(), pdbFile.getPdbSeqress()));
        
        return pdbFile;
        
    }
    
    
    /**
     * 
     * @param pdbFile
     * @param updateKeywordString
     * @param updateAuthorString
     * @return
     * @throws JDAOException 
     */
    public Jstruct_PdbFile update(Jstruct_PdbFile pdbFile, boolean updateKeywordString, boolean updateAuthorString) throws JDAOException {
        
        if(updateKeywordString){
            PdbKeywordService keywordService = new PdbKeywordService();
            keywordService.deleteByStructVerId(pdbFile.getStructVerId());
            
            if(StringUtil.isSet(pdbFile.getKeywordString())){
                String[] keywordsArray = JStringUtil.splitOnCommaAndTrim(pdbFile.getKeywordString());
                List<Jstruct_PdbKeyword> pdbKeywords = new ArrayList<>(keywordsArray.length);
                for (String keyword : keywordsArray) {
                    pdbKeywords.add(new Jstruct_PdbKeyword(pdbFile.getStructVerId(), keyword));
                }
                keywordService.batchInsert(pdbKeywords);
            }
        }
        
        if(updateAuthorString){
            PdbAuthorService authorService = new PdbAuthorService();
            authorService.deleteByStructVerId(pdbFile.getStructVerId());
            
            if(StringUtil.isSet(pdbFile.getAuthorString())){
                String[] authorArray = JStringUtil.splitOnCommaAndTrim(pdbFile.getAuthorString());
                List<Jstruct_PdbAuthor> pdbAuthors = new ArrayList<>(authorArray.length);
                for (String author : authorArray) {
                    pdbAuthors.add(new Jstruct_PdbAuthor(pdbFile.getStructVerId(), author));
                }
                authorService.batchInsert(pdbAuthors);
            }
        }
        
        //first update the pdbFile
        pdbFile = pdbFileDao.update(pdbFile);
        
        //re-get it to include keywords/authors
        return getByStructVerId(pdbFile.getStructVerId());
    }
    
    
    
    
    
}
