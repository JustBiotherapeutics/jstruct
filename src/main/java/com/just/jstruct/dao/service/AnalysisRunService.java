package com.just.jstruct.dao.service;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.AnalysisRunDAO;
import com.just.jstruct.dao.interfaces.IAnalysisRunDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AnalysisRun;
import com.just.jstruct.model.Jstruct_AnalysisTask;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AnalysisRunService implements Serializable {

    /* -------------------------------------------------------------------------
        dao classes 
    ------------------------------------------------------------------------- */
    
    private final IAnalysisRunDAO analysisRunDao;

    private final AnalysisTaskService analysisTaskService = new AnalysisTaskService();

    /* -------------------------------------------------------------------------
        constructor 
    ------------------------------------------------------------------------- */
    
    public AnalysisRunService() {
        analysisRunDao = new AnalysisRunDAO();
    }


    
    /* -------------------------------------------------------------------------
        methods 
    ------------------------------------------------------------------------- */
    
    /**
     * add a new AnalysisRun to the database
     * @param analysisRun to add
     * @return the newly persisted analysisRun
     * @throws JDAOException 
     */
    public Jstruct_AnalysisRun add(Jstruct_AnalysisRun analysisRun) throws JDAOException 
    {
        if(analysisRun.getStartDate() == null){
            analysisRun.setStartDate(new Date()); 
        }
        
        if(!StringUtil.isSet(analysisRun.getStatus())){
            analysisRun.setStatus(Jstruct_AnalysisRun.STATUS_NOT_STARTED);  
        }
        
        Jstruct_AnalysisRun newAnalysisRun = analysisRunDao.add(analysisRun);
        return newAnalysisRun; 
    }
    
    
    /**
     * update a AnalysisRun in the database
     * @param analysisRun to update
     * @return the updated analysisRun from the database
     * @throws JDAOException
     */
    public Jstruct_AnalysisRun update(Jstruct_AnalysisRun analysisRun) throws JDAOException 
    {
        Jstruct_AnalysisRun updatedAnalysisRun = analysisRunDao.update(analysisRun);
        return updatedAnalysisRun;
    }
    
    
    
    
    
    
    
    
    
    /**
     * get a AnalysisRun based on it's primary key
     * @param id of AnalysisRun to find
     * @param includeTasks
     * @return AnalysisRun that matches id
     * @throws JDAOException
     */
    public Jstruct_AnalysisRun getById(Long id, boolean includeTasks) throws JDAOException 
    {
        Jstruct_AnalysisRun run = analysisRunDao.getById(id);
        
        if(includeTasks)
        {
            run = retrieveTasks(run);
        }
        
        return run;
    }
    
    
  
    /**
     * return collection of all AnalysisRun for a status
     * @param status
     * @return all AnalysisRun based on status
     * @throws JDAOException
     */
    public List<Jstruct_AnalysisRun> getByStatus(String status) throws JDAOException 
    {
        return analysisRunDao.getByStatus(status);
    }
    
    
    
    /**
     * return collection of all AnalysisRun for a analysisScriptVerId
     * @param analysisScriptVerId
     * @return all AnalysisRun based on analysisScriptVerId
     * @throws JDAOException
     */
    public List<Jstruct_AnalysisRun> getByAnalysisScriptVerId(Long analysisScriptVerId) throws JDAOException 
    {
        return analysisRunDao.getByAnalysisScriptVerId(analysisScriptVerId);
    }


    


    
    
    
    
    
    /* -------------------------------------------------------------------------
        private 
    ------------------------------------------------------------------------- */
    
    
    private  List<Jstruct_AnalysisRun> retrieveTasks(List<Jstruct_AnalysisRun> runs) throws JDAOException 
    {
        if(CollectionUtil.hasValues(runs))
        {
            for(Jstruct_AnalysisRun run : runs)
            {
                List<Jstruct_AnalysisTask> tasks = analysisTaskService.getByAnalysisRunId(run.getAnalysisRunId());
                run.addAnalysisTasks(tasks);
            }
        }
        return runs;
    }
    
    
    private Jstruct_AnalysisRun retrieveTasks(Jstruct_AnalysisRun run) throws JDAOException 
    {
        if(run != null)
        {
            List<Jstruct_AnalysisTask> tasks = analysisTaskService.getByAnalysisRunId(run.getAnalysisRunId());
            run.addAnalysisTasks(tasks);
        }
        return run;
    }
    
    
    
    
 
    
    
}
