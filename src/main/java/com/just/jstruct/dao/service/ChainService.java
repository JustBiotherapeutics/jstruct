package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.ChainDAO;
import com.just.jstruct.dao.interfaces.IChainDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AtomGrouping;
import com.just.jstruct.model.Jstruct_Chain;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ChainService implements Serializable {


    /* dao classes */
    
    private final IChainDAO chainDao;


    
    
    /* constructor */
    
    public ChainService() {
        chainDao = new ChainDAO();
    }



    


    /**
     * get a Chain based on it's primary key
     * @param chainId of Chain to find
     * @param includeAtomGroupings
     * @param includeAtomGroupingResidues
     * @param includeRelatedSeqResidues
     * @return chain that matches chainId
     * @throws JDAOException
     */
    public Jstruct_Chain getByChainId(Long chainId, 
                                      boolean includeAtomGroupings, 
                                      boolean includeAtomGroupingResidues, 
                                      boolean includeRelatedSeqResidues) 
                                                                    throws JDAOException {
        Jstruct_Chain chain = chainDao.getByChainId(chainId);

        //if flagged, get AtomGroupings
        if (includeAtomGroupings) {
            AtomGroupingService atomGroupingService = new AtomGroupingService();
            chain.setAtomGroupings(atomGroupingService.getByChainId(chain.getChainId(), includeAtomGroupingResidues));

        }

        //if flagged, get SeqResidues
        if (includeRelatedSeqResidues) {
            SeqResidueService seqResidueService = new SeqResidueService();
            chain.setSeqResidues(seqResidueService.getByChainId(chain.getChainId()));
        }
        
        return chain;
    }
    


    /**
     * get a list of Chains based on a structVerId
     * @param structVerId of Chains to find
     * @return chains that matches structVerId
     * @throws JDAOException
     */
    public List<Jstruct_Chain> getByStructVerId(Long structVerId) throws JDAOException {
        return chainDao.getByStructVerId(structVerId);
    }


    /**
     * 
     * @param structVerIds
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_Chain> getByStructVerIds(List<Long> structVerIds) throws JDAOException {
        return chainDao.getByStructVerIds(structVerIds);
    }
    
    
    
    
    
    /**
     * 
     * @param structVerId
     * @param includeAtomGroupings
     * @param includeRelatedSeqResidues
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_Chain> getByStructVerId(Long structVerId, boolean includeAtomGroupings, boolean includeRelatedSeqResidues) throws JDAOException {
        
        //get Chains
        List<Jstruct_Chain> chains = chainDao.getByStructVerId(structVerId);
        
        if(CollectionUtil.hasValues(chains)){
            
            //if flagged, get AtomGroupings
            if(includeAtomGroupings){
                AtomGroupingService atomGroupingService = new AtomGroupingService();
                for(Jstruct_Chain chain : chains){
                    chain.setAtomGroupings(atomGroupingService.getByChainId(chain.getChainId(), false));
                }
            }

            //if flagged, get SeqResidues
            if(includeRelatedSeqResidues){
                SeqResidueService seqResidueService = new SeqResidueService();
                for(Jstruct_Chain chain : chains){
                    chain.setSeqResidues(seqResidueService.getByChainId(chain.getChainId()));
                }
            }
            
        }
        
        return chains;
    }
    
    
    /**
     * 
     * @param structVerId
     * @return
     * @throws JDAOException 
     */
    public Integer getCountByStructVerId(Long structVerId) throws JDAOException {
        return chainDao.getCountByStructVerId(structVerId);
    }
    
    
    
    /**
     * 
     * @param chains
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_Chain> insert(List<Jstruct_Chain> chains) throws JDAOException {
        
        if(!CollectionUtil.hasValues(chains)){
            return null;
        }
        
        AtomGroupingService atomGroupingService = new AtomGroupingService();
        SeqResidueService seqResidueService = new SeqResidueService();
        ResidueService residueService = new ResidueService();
        
        //insert each Chains
        for(Jstruct_Chain chain : chains){
            
            chainDao.insert(chain); 
            
            //insert AtomGroupings
            if(CollectionUtil.hasValues(chain.getAtomGroupings())){
                for(Jstruct_AtomGrouping ag : chain.getAtomGroupings()){
                    
                    //insert each AtomGrouping...
                    ag.setChainId(chain.getChainId());
                    atomGroupingService.insert(ag);  
                    
                    //...and bulk insert it's residues
                    if(CollectionUtil.hasValues(ag.getResidues())){
                        residueService.bulkInsert(ag.getResidues(), ag.getAtomGroupingId());
                    }
                    
                }
            }
            
            //insert SeqResidues
            if(CollectionUtil.hasValues(chain.getSeqResidues())){
                seqResidueService.bulkInsert(chain.getSeqResidues(), chain.getChainId());
            }
            
            
        }
        
        return chains;
    }
    
    
    /**
     * 
     * @param structVerId
     * @throws JDAOException 
     */
    public void deleteByStructVerId(Long structVerId) throws JDAOException {
        
        ResidueService residueService = new ResidueService();
        AtomGroupingService atomGroupingService = new AtomGroupingService();
        SeqResidueService seqResidueService = new SeqResidueService();
           
        //delete Residues, AtomGroupings, and SeqResidues
        residueService.deleteByStructVerId(structVerId);
        atomGroupingService.deleteByStructVerId(structVerId);
        seqResidueService.deleteByStructVerId(structVerId);
        
        //delete Chains
        chainDao.deleteByStructVerId(structVerId);
        
    }
    
    
    
    
    
    
    
    
    
    
}
