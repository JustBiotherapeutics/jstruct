package com.just.jstruct.dao.service;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.SearchSubqueryDAO;
import com.just.jstruct.dao.interfaces.ISearchSubqueryDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_SearchParam;
import com.just.jstruct.model.Jstruct_SearchSubquery;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SearchSubqueryService implements Serializable {


    /* dao classes */
    
    private final ISearchSubqueryDAO searchSubqueryDao;


    
    
    /* constructor */
    
    public SearchSubqueryService() {
        searchSubqueryDao = new SearchSubqueryDAO();
    }



    
  
    /**
     * 
     * @param searchSubqueryId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_SearchSubquery getBySearchSubqueryId(Long searchSubqueryId) throws JDAOException {
        return searchSubqueryDao.getBySearchSubqueryId(searchSubqueryId);
    }

    
    
    
    /**
     * 
     * @param userSearchId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_SearchSubquery> getByUserSearchId(Long userSearchId) throws JDAOException {
        return searchSubqueryDao.getByUserSearchId(userSearchId);
    }

  

    
    /**
     * 
     * @param searchSubquery
     * @return
     * @throws JDAOException 
     */
    public Jstruct_SearchSubquery insert(Jstruct_SearchSubquery searchSubquery) throws JDAOException {
        Jstruct_SearchSubquery insertedSearchSubquery = searchSubqueryDao.add(searchSubquery);
        return insertedSearchSubquery; 
    }
    
    
   
    /**
     * 
     * @param doomedSearchSubquery 
     * @throws com.just.jstruct.exception.JDAOException 
     */
    public void delete(Jstruct_SearchSubquery doomedSearchSubquery) throws JDAOException {
        //first delete all related SearchParams (todo make bulk delete)
        SearchParamService searchParamService = new SearchParamService();
        List<Jstruct_SearchParam> doomedSearchParams = searchParamService.getBySearchSubqueryId(doomedSearchSubquery.getSearchSubqueryId());
        if(CollectionUtil.hasValues(doomedSearchParams)){
            for(Jstruct_SearchParam doomedSearchParam : doomedSearchParams){
                searchParamService.delete(doomedSearchParam); 
            }
        }
        
        //delete th doomedSearchSubquery
        searchSubqueryDao.delete(doomedSearchSubquery);
    }
    
  
    
    
}
