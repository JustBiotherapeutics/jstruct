package com.just.jstruct.dao.service;

import com.hfg.ldap.LDAP_User;
import com.just.jstruct.dao.hibernate.JstructUserDAO;
import com.just.jstruct.dao.interfaces.IJstructUserDAO;
import com.just.jstruct.enums.UserStatus;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.JEmailUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JstructUserService implements Serializable {


    /* dao classes */
    
    private final IJstructUserDAO jstructUserDao;


    
    
    /* constructor */
    
    public JstructUserService() {
        jstructUserDao = new JstructUserDAO();
    }



    /**
     * return collection of all JstructUsers
     * @return all JstructUsers
     * @throws JDAOException
     */
    public List<Jstruct_User> getAll() throws JDAOException {
        return jstructUserDao.getAll();
    }


    /**
     * get a JstructUser based on it's primary key
     * @param userId of JstructUser to find
     * @return JstructUser that matches userId
     * @throws JDAOException
     */
    public Jstruct_User getById(Long userId) throws JDAOException {
        return jstructUserDao.getByUserId(userId);
    }


    /**
     * get a JstructUser record based on it's UID 
     * @param uid of JstructUser to find
     * @return JstructUser that matches uid
     * @throws JDAOException
     */
    public Jstruct_User getByUid(String uid) throws JDAOException {
        return jstructUserDao.getByUid(uid);
    }
    
    
    /**
     * get the System user
     * @return system user
     * @throws JDAOException 
     */
    public Jstruct_User getSystemUser() throws JDAOException {
        return jstructUserDao.getSystemUser();
    }
    
    
    /**
     * get the System user
     * @return guest user
     * @throws JDAOException 
     */
    public Jstruct_User getGuestUser() throws JDAOException {
        
        Jstruct_User guestUser = jstructUserDao.getGuestUser();
        
        if(guestUser==null){
            guestUser = new Jstruct_User(-1L);
            guestUser.setUid(Jstruct_User.GUEST_UID);
            guestUser.setName("Guest");
            guestUser.setStatus(UserStatus.ACTIVE);
        }
        
        return guestUser;
    }
    
     
    
    /**
     * add a new jstruct user to the database
     * @param jstructUser
     * @return
     * @throws JDAOException 
     */
    private Jstruct_User addJstructUser(Jstruct_User jstructUser) throws JDAOException {
        return jstructUserDao.add(jstructUser);
    }
    
    
    /**
     * update a JstructUser in the database
     * @param jstructUser to update
     * @return the updated jstructUser from the database
     * @throws JDAOException
     */
    public Jstruct_User updateJstructUser(Jstruct_User jstructUser) throws JDAOException {
        return jstructUserDao.update(jstructUser);
    }
    
    
    
    /**
     * using an LDAP_User object as 'base' save a new Jstruct_User and give it the appropriate roles
     * @param ldapUser
     * @param giveContribute
     * @param giveAdmin
     * @param giveDeveloper
     * @return
     * @throws JDAOException 
     */
    public Jstruct_User addUserWithRoles(LDAP_User ldapUser, boolean giveContribute, boolean giveAdmin, boolean giveDeveloper) throws JDAOException {
        
        Jstruct_User newUser = new Jstruct_User();
        
        newUser.setUid(ldapUser.getUID());
        newUser.setName(ldapUser.getName());
        newUser.setEmail(ldapUser.getEmail());
        newUser.setImgUrl(ldapUser.getImageUrl());
        
        newUser.setStatus(UserStatus.ACTIVE);
        
        newUser.setRoleContributor(giveContribute);
        newUser.setRoleAdmin(giveAdmin);
        newUser.setRoleDeveloper(giveDeveloper);
        
        newUser.setInsertUser(getSystemUser());
        newUser.setUpdateUser(getSystemUser());
        newUser.setInsertDate(new Date());
        newUser.setUpdateDate(new Date());
        
        //send a 'new user' email
        JEmailUtil.sendNewUserLoginEmail(newUser);
        
        return addJstructUser(newUser);
        
    }
    
    
    
    /**
     * return a list of all active JstructUsers that match the inputed criteria
     * @param criteria may be first name, familiar name, last name
     * @return list of all users that match criteria
     * @throws JDAOException
     */
    public List<Jstruct_User> getUsersByGeneralCriteria(String criteria) throws JDAOException {
        return jstructUserDao.getUsersByGeneralCriteria(criteria);
    }
    
    
    /***
     * get a list of active user with developer role
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_User> getActiveRoleDevelopers() throws JDAOException {
        return jstructUserDao.getActiveRoleDevelopers();
    }
    
    /***
     * get a list of active user with admin role
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_User> getActiveRoleAdmins() throws JDAOException {
        return jstructUserDao.getActiveRoleAdmins();
    }
    
    /***
     * get a list of active user with contributer role
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_User> getActiveRoleContributers() throws JDAOException {
        return jstructUserDao.getActiveRoleContributers();
    }
    
    
    
    
    
    private static Map<Long, Jstruct_User> userIdCache = new HashMap<>();
    
    public static Jstruct_User getUserFromCache(Long userId) throws JDAOException{

        Jstruct_User user = userIdCache.get(userId);
        if(null == user){
            JstructUserService jstructUserService = new JstructUserService();
            user = jstructUserService.getById(userId);
            userIdCache.put(userId, user);
        }
        return user;
    }
    
    
    
    private static Map<String, Jstruct_User> userUidCache = new HashMap<>();
    
    public static Jstruct_User getUserFromCache(String uid) throws JDAOException{

        Jstruct_User user = userUidCache.get(uid);
        if(null == user){
            JstructUserService jstructUserService = new JstructUserService();
            user = jstructUserService.getByUid(uid);
            userUidCache.put(uid, user);
        }
        return user;
    }
    
 
    
    
}
