package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.ImportPdbDAO;
import com.just.jstruct.dao.interfaces.IImportPdbDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ImportPdb;
import java.io.Serializable;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportPdbService implements Serializable {


    /* dao classes */
    
    private final IImportPdbDAO importPdbDao;


    
    
    /* constructor */
    
    public ImportPdbService() {
        importPdbDao = new ImportPdbDAO();
    }



    /**
     * get a ImportPdb based on it's primary key
     * @param importPdbId of ImportPdb to find
     * @return importPdb that matches importPdbId
     * @throws JDAOException
     */
    public Jstruct_ImportPdb getByImportPdbId(Long importPdbId) throws JDAOException {
        return importPdbDao.getByImportPdbId(importPdbId);
    }
    
    
    
    /**
     * 
     * @param importRestId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_ImportPdb> getByImportRestId(Long importRestId) throws JDAOException {
        return importPdbDao.getByImportRestId(importRestId);
    }
    
    
    
    
    /**
     * 
     * @param importJobId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_ImportPdb> getByImportJobId(Long importJobId) throws JDAOException {
        return importPdbDao.getByImportRestId(importJobId);
    }
    
    
    
    
    /**
     * 
     * @param importPdbs
     * @param importRestId
     * @return
     * @throws JDAOException 
     */
    public List<Jstruct_ImportPdb> bulkInsert(List<Jstruct_ImportPdb> importPdbs, Long importRestId) throws JDAOException{
        return importPdbDao.bulkInsert(importPdbs,importRestId);
             
    }
    
    
}
