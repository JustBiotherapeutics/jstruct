package com.just.jstruct.dao.service;

import com.just.jstruct.dao.hibernate.StructureDAO;
import com.just.jstruct.dao.interfaces.IStructureDAO;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_Structure;
import java.io.Serializable;
import java.util.Date;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureService implements Serializable {


    /* dao classes */
    
    private final IStructureDAO structureDao;


    
    
    /* constructor */
    
    public StructureService() {
        structureDao = new StructureDAO();
    }



    /**
     * return collection of all Structures
     * @return all JstructStructures
     * @throws JDAOException
     */
    //public List<Jstruct_Structure> getAll() throws JDAOException {
    //    return structureDao.getAll();
    //}


    /**
     * get a Structure based on it's primary key
     * @param structureId of Structure to find
     * @return structure that matches structureId
     * @throws JDAOException
     */
    public Jstruct_Structure getByStructureId(Long structureId) throws JDAOException {
        return structureDao.getByStructureId(structureId);
    }

   

    
    /**
     * count of Active RCSB files
     * @return
     * @throws JDAOException 
     */
    public Integer getActiveRcsbFileCount() throws JDAOException {
        return structureDao.getFileCount(true, false);
    }
 
    
    /**
     * count of Obsolete RCSB files
     * @return
     * @throws JDAOException 
     */
    public Integer getObsoleteRcsbFileCount() throws JDAOException {
        return structureDao.getFileCount(true, true);
    }
 
    
    
    /**
     * count of Active manual uploaded files
     * @return
     * @throws JDAOException 
     */
    public Integer getActiveManualUploadFileCount() throws JDAOException {
        return structureDao.getFileCount(false, false);
    }
 
    
    /**
     * count of Obsolete manual uploaded files
     * @return
     * @throws JDAOException 
     */
    public Integer getObsoleteManualUploadFileCount() throws JDAOException {
        return structureDao.getFileCount(false, true);
    }
 
    
    /**
     * 
     * @param structure
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure add(Jstruct_Structure structure) throws JDAOException {
        return structureDao.add(structure);
    }
    
 
    
    /**
     * 
     * @param structure
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure update(Jstruct_Structure structure) throws JDAOException {
        return structureDao.update(structure);
    }
    
    
    
    /**
     * set a structure to obsolete, and set the obsolete date to now
     * @param structure
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure setToObsolete(Jstruct_Structure structure) throws JDAOException
    {
        structure.setObsolete(true);
        structure.setObsoleteDate(new Date());
        return structureDao.update(structure);
    }
    
    /**
     * set a structure to active, and remove any obsolete date 
     * @param structure
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure setToActive(Jstruct_Structure structure) throws JDAOException {
        structure.setObsolete(false);
        structure.setObsoleteDate(null);
        return structureDao.update(structure);
    }
    
    
    /**
     * toggle the 'canAllRead' value to false
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure removeAllCanRead(Long structureId) throws JDAOException {
        Jstruct_Structure structure = getByStructureId(structureId);
        structure.setCanAllRead(false);
        return structureDao.update(structure);
    }
    
    /**
     * toggle the 'canAllRead' value to true
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure addAllCanRead(Long structureId) throws JDAOException {
        Jstruct_Structure structure = getByStructureId(structureId);
        structure.setCanAllRead(true);
        return structureDao.update(structure);
    }
    
    
    /**
     * toggle the 'canAllWrite' value to false
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure removeAllCanWrite(Long structureId) throws JDAOException {
        Jstruct_Structure structure = getByStructureId(structureId);
        structure.setCanAllWrite(false);
        return structureDao.update(structure);
    }
    
    /**
     * toggle the 'canAllWrite' value to true
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure addAllCanWrite(Long structureId) throws JDAOException {
        Jstruct_Structure structure = getByStructureId(structureId);
        structure.setCanAllWrite(true);
        return structureDao.update(structure);
    }
    
    
    /**
     * 
     * @param structureId
     * @param allCanRead
     * @param allCanWrite
     * @return 
     */
    public Jstruct_Structure updatePermissions(Long structureId, boolean allCanRead, boolean allCanWrite) throws JDAOException {
        Jstruct_Structure structure = getByStructureId(structureId);
        structure.setCanAllRead(allCanRead);
        structure.setCanAllWrite(allCanWrite);
        return structureDao.update(structure);
    }
    
    
    
    /**
     * set a structure to obsolete, and set the obsolete date to now
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure setToObsolete(Long structureId) throws JDAOException {
        Jstruct_Structure structure = structureDao.getByStructureId(structureId);
        return setToObsolete(structure);
    }
    
    /**
     * set a structure to active, and remove any obsolete date 
     * @param structureId
     * @return
     * @throws JDAOException 
     */
    public Jstruct_Structure setToActive(Long structureId) throws JDAOException {
        Jstruct_Structure structure = structureDao.getByStructureId(structureId);
        return setToActive(structure);
    }
    

    
    
    
}
