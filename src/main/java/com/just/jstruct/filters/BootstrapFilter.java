package com.just.jstruct.filters;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.IOException;
import java.rmi.ServerException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * filter that checks if this has ever been started-up before; if not it redirects to a 'startup' page, 
 * else it just passes the request along.
 * 
 * notes: http://stackoverflow.com/questions/8480100/how-implement-a-login-filter-in-jsf
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@WebFilter({"*.xhtml", "*.jsp"})
public class BootstrapFilter implements Filter{
    
    
    private static final String BOOTSTRAP_COMPLETE = "bootstrapComplete"; 
    private static final String BOOTSTRAP_URL = "/jstruct/bootstrap.xhtml"; 

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
    
    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        try {
            
            HttpServletRequest req = (HttpServletRequest) request;
            Boolean bootstrapComplete = (Boolean) req.getSession().getAttribute(BOOTSTRAP_COMPLETE);
            
            if(bootstrapComplete == null){
                bootstrapComplete = false;
                req.getSession().setAttribute(BOOTSTRAP_COMPLETE, bootstrapComplete);   
            }

            
            if(bootstrapComplete) {
                // if we'e already checked, the BOOTSTRAP_COMPLETE variable will be true (this way we 
                // dont hit the DB every request) this case happens like 99.99999 percent of the time
                chain.doFilter(request, response);
                
            } else {
                
                
                if (req.getRequestURI().contains(BOOTSTRAP_URL)) {
                    
                    //we're at the bootstrap page.. just pass it through
                    chain.doFilter(request, response);
                    
                } else {

                    if(ValueMapUtils.getInstance().isBootstrapComplete()){
                        
                        //bootstrap is complete - set the session variable, and get movin' on
                        req.getSession().setAttribute(BOOTSTRAP_COMPLETE, true); 
                        chain.doFilter(request, response);
                        
                    } else {

                        //bootstrap is required - redirect to bootstrap page, after forceing a logout
                        
                        HttpServletResponse res = (HttpServletResponse) response;
                        res.sendRedirect(BOOTSTRAP_URL);

                    }

                }
            }

            
        } catch (JDAOException ex){
            throw new ServerException("Error in Bootstrap Filter", ex);
        }
        
    }
    
    
    

    
    
}
