package com.just.jstruct.pojos;

import com.just.bio.structure.enums.ResidueType;
import com.just.jstruct.model.Jstruct_Residue;
import com.just.jstruct.model.Jstruct_SeqResidue;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ResAttribute {


    // ********************************************************************************************************************************************
    //  static amino acid definition mapping
    // ********************************************************************************************************************************************


    //todo it may be unnecessary to merge amino acid definition stuff into this attr since we already have residue_type
    //private static HashMap<String, AminoAcid> mAminoAcidMap;

    //private static HashMap<String, AminoAcid> getAminoAcidMap() throws SQLException {
    //    if(mAminoAcidMap==null) {
    //        mAminoAcidMap = Jstruct_AminoAcid.getAminoAcidMap();
    //    }
    //    return mAminoAcidMap;
   // }



    // ********************************************************************************************************************************************
    //
    // ********************************************************************************************************************************************


    private int ordinal;
    private Character icode;
    private String abbrev;
    private ResidueType residueType;
    private String atoms;


    public ResAttribute(Jstruct_SeqResidue seqResidue) {
        this.ordinal = seqResidue.getOrdinal();
        this.abbrev = seqResidue.getAbbrev();
        this.residueType = ResidueType.valueOf(seqResidue.getResidueType());
    }


    public ResAttribute(Jstruct_Residue residue) {
        this.ordinal = residue.getResSeqNumber();
        this.icode = residue.getIcode();
        this.abbrev = residue.getAbbrev();
        this.residueType = ResidueType.valueOf(residue.getResidueType());
        this.atoms = residue.getAtoms();
    }


    public int getOrdinal() {return ordinal;}
    public Character getIcode() {return icode;}
    public String getAbbrev() {return abbrev;}
    public ResidueType getResidueType() {return residueType;}
    public String getAtoms(){return atoms;}


}
