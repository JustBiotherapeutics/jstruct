package com.just.jstruct.pojos;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class DisplayResidue {
    
    
    private Integer ordinal;
    private Character icode;
    private char letter;
    private String abbrev;
    private String residueType;
    private String hexColor;
    private String atoms;

    
    
    public DisplayResidue(Integer ordinal, Character icode, char letter, String abbrev, String residueType, String hexColor, String atoms) {
        this.ordinal = ordinal;
        this.icode = icode;
        this.letter = letter;
        this.abbrev = abbrev;
        this.residueType = residueType;
        this.hexColor = hexColor;
        this.atoms = atoms;
    }

    public DisplayResidue(char letter) {
        //used if no residueMap
        this.letter = letter;
        this.hexColor = "555555";
    }
    
    
    
    
    
    
    
    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public Character getIcode() {
        return icode;
    }

    public void setIcode(Character icode) {
        this.icode = icode;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    public String getResidueType() {
        return residueType;
    }

    public void setResidueType(String residueType) {
        this.residueType = residueType;
    }

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }

    public String getAtoms() {
        return atoms;
    }

    public void setAtoms(String atoms) {
        this.atoms = atoms;
    }
    
    
    
    
    
    
}
