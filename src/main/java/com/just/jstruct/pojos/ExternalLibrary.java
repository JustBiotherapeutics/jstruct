package com.just.jstruct.pojos;

import java.util.HashMap;
import java.util.Map;


/**
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ExternalLibrary implements Comparable<ExternalLibrary> {
 
    
    
    private String libraryName;       
    private String libraryVersion;      
    private String libraryURL; 
    private Map<String, String> licenseMap;
    //private String licenseName;      
    //private String licenseUrl;      

    public ExternalLibrary(
            String libraryName, String libraryVersion, String libraryURL, 
            String licenseName, String licenseUrl) 
    {
        this.libraryName = libraryName;
        this.libraryVersion = libraryVersion;
        this.libraryURL = libraryURL;
        
        this.licenseMap = new HashMap<>(1);
        this.licenseMap.put(licenseName, licenseUrl);
                
    }
    
    
    public ExternalLibrary(
            String libraryName, String libraryVersion, String libraryURL, 
            Map<String, String> licenseMap) 
    {
        this.libraryName = libraryName;
        this.libraryVersion = libraryVersion;
        this.libraryURL = libraryURL;
        
        this.licenseMap = licenseMap;
                
    }

    
    
    
    
    
    
    
    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibraryVersion() {
        return libraryVersion;
    }

    public void setLibraryVersion(String libraryVersion) {
        this.libraryVersion = libraryVersion;
    }

    public String getLibraryURL() {
        return libraryURL;
    }

    public void setLibraryURL(String libraryURL) {
        this.libraryURL = libraryURL;
    }

    public Map<String, String> getLicenseMap() {
        return licenseMap;
    }

    public void setLicenseMap(Map<String, String> licenseMap) {
        this.licenseMap = licenseMap;
    }

    

    
    
    
    @Override
    public int compareTo(ExternalLibrary that) {
        return this.getLibraryName().compareToIgnoreCase(that.getLibraryName());
    }




    
    
}
