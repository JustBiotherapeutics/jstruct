package com.just.jstruct.pojos;
import com.hfg.util.UserImpl;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.AuthUtil;
import static com.just.jstruct.utilities.AuthUtil.browserSessionCookieIsValid;
import static com.just.jstruct.utilities.AuthUtil.createUserObjFromBrowserSessionCookie;
import static com.just.jstruct.utilities.AuthUtil.getBrowserSessionCookie;
import com.just.jstruct.utilities.FacesUtil;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * General info used throughout each session (session scoped)
 * includes stuff like user, tab states, ...
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "sessionBean")
@SessionScoped
public class SessionBean implements Serializable {
    
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger LOGGER = Logger.getLogger(SessionBean.class.getPackage().getName());
    

    
    //current tab state of pages with primefaces tabs
    private int tabProperties = 0;
    private int tabLookups = 0;
    private int tabHomeAccordian = 0;

    
    
    //constructor - remember, this 'session scoped' bean is only called once, at the start of a users session!
    public SessionBean() {
    }
    



    //getters/setters
    
    public int getTabProperties() { return tabProperties; }
    public void setTabProperties(int tabProperties) { this.tabProperties = tabProperties; }

    public int getTabLookups() { return tabLookups; }
    public void setTabLookups(int tabLookups) { this.tabLookups = tabLookups; }

    public int getTabHomeAccordian() { return tabHomeAccordian; }
    public void setTabHomeAccordian(int tabHomeAccordian) { this.tabHomeAccordian = tabHomeAccordian; }
    
    

    /**
     * grab the user from the request/session/cookie 
     * @return 
     */
    public Jstruct_User getUser(){
        HttpServletRequest request = FacesUtil.getRequest();
        return SessionBean.getUser(request);
    }
    
    /**
     * grab the user from the request/session/cookie 
     * if the user is not in the session, then get the guest user (unauthenticated) and put that user in the session
     * @param request
     * @return
     */
    public static Jstruct_User getUser(HttpServletRequest request) {
        
        //if there is no request, it's likely this call is happening somewhere in an API lifecycle (and no one is logged in)
        if(request==null){
            try{
                JstructUserService jstructUserService = new JstructUserService();
                return jstructUserService.getGuestUser(); 
            } catch (JDAOException ex) {
                return null;
            }
        }
        
        // (1) attempt to get a user from the session.
        Jstruct_User currentUser = (Jstruct_User) request.getSession().getAttribute(AuthUtil.SESSION_JSTRUCT_USER); //this doesnt get it outta the cookie, just session.


        if (null == currentUser) {

            // (2) see if there is a valid cookie holding an authenticated user (this should never be the Guest user)
            Cookie browserSessionCookie = getBrowserSessionCookie(request);
            try {
                if (browserSessionCookieIsValid(browserSessionCookie))
                {
                    UserImpl userImpl = createUserObjFromBrowserSessionCookie(browserSessionCookie);
                    currentUser = userImplToExistingJstructUser(userImpl);
                    
                    //if user is null again, then there is no existing DB user that matches the cookie
                    if(currentUser == null){
                        JstructUserService jstructUserService = new JstructUserService();
                        currentUser = jstructUserService.getGuestUser();
                    }
                        
                }
                else
                {
                    // (3) authenticated user is not in session or cookie - get the Guest user from the database 
                    JstructUserService jstructUserService = new JstructUserService();
                    currentUser = jstructUserService.getGuestUser();
                }

                //put user into session (NOT into cookie) 
                request.getSession().setAttribute(AuthUtil.SESSION_JSTRUCT_USER, currentUser);

                //add name to tomcat for the 
                SessionBean.addUserNameToTomcat(request, currentUser);
                
                
                
            } catch (Exception e){
                ///TODO
                e.printStackTrace();
                LOGGER.log(Level.SEVERE, "Error getting user! Message:{0}", e.getMessage());
                throw new RuntimeException(e);
            }

        }

        return currentUser;
    }
    
    
     
    /**
     * 
     * @param userImpl
     * @return 
     */
    private static Jstruct_User userImplToExistingJstructUser(UserImpl userImpl) {

        Jstruct_User jstructUser;

        try {
            JstructUserService jstructUserService = new JstructUserService();
            jstructUser = jstructUserService.getByUid(userImpl.getUID());
        } catch (JDAOException jde) {
            return null;
        } 

        return jstructUser;
    }
    
    
    /**
     * 
     * @param request
     * @param currentUser 
     */
    public static void addUserNameToTomcat(HttpServletRequest request, Jstruct_User currentUser)
    {
        StringBuilder usernameForTomcat = new StringBuilder();
        if (null == currentUser)
        {
            usernameForTomcat.append("Guest");
        }
        else
        {
            usernameForTomcat.append(currentUser.getName());
            usernameForTomcat.append(" [UID: ").append(currentUser.getUid()).append("]");
        }
        
        usernameForTomcat.append(" [remoteHost: ").append(request.getRemoteHost()).append("]");
        request.getSession().setAttribute(AuthUtil.SESSION_USER_NAME, usernameForTomcat.toString());
    }


    

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SessionBean [currentUser: ").append(getUser().getUid()).append("]");
        return sb.toString();
    }
    
    
    
}
