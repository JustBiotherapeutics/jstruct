package com.just.jstruct.pojos;

import com.just.jstruct.model.Jstruct_User;

/**
 * Authentication state after a user attempts to log in
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AuthState {
 
    
    
    private boolean mAuthenticated = false; //true if authentication is successful
    private String mAuthFailMessage;        //if authentication is NOT successful, this is the message
    private Exception mAuthFailException;   //if authentication is NOT successful, this is the exception thrown
    private Jstruct_User jstructUser;




    public boolean isAuthenticated() {return mAuthenticated;}
    public void setAuthenticated(boolean inAuthenticated) {this.mAuthenticated = inAuthenticated;}

    public String getAuthFailMessage() {return mAuthFailMessage;}
    public void setAuthFailMessage(String inAuthFailMessage) {this.mAuthFailMessage = inAuthFailMessage;}

    public Exception getAuthFailException() {return mAuthFailException;}
    public void setAuthFailException(Exception inAuthFailException) {this.mAuthFailException = inAuthFailException;}

    public Jstruct_User getJstructUser() {return jstructUser;}
    public void setJstructUser(Jstruct_User jstructUser) {this.jstructUser = jstructUser;}
    
    
}
