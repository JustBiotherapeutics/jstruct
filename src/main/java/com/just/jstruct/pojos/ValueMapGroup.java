package com.just.jstruct.pojos;


import com.just.jstruct.model.Jstruct_ValueMap;
import java.io.Serializable;
import java.util.List;


/**
 * each ValueMapGroup object contains all ValueMap records for a group
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ValueMapGroup implements Serializable
{
 
    private static final long serialVersionUID = 1L;

    //##################################################################################################################
    // data members
    //##################################################################################################################

    private List<Jstruct_ValueMap> valueMaps;
    private String itemGroup;

    


    //##################################################################################################################
    // constructor
    //##################################################################################################################

    public ValueMapGroup(List<Jstruct_ValueMap> valueMaps, String itemGroup)
    {
        this.valueMaps = valueMaps;
        this.itemGroup = itemGroup;
    }



   //##################################################################################################################
    // geters/setters
    //##################################################################################################################
 
    public List<Jstruct_ValueMap> getValueMaps() { return valueMaps; }
    public void setValueMaps(List<Jstruct_ValueMap> valueMaps) { this.valueMaps = valueMaps; }

    public String getItemGroup() { return itemGroup; }
    public void setItemGroup(String itemGroup) { this.itemGroup = itemGroup; }




    


}
