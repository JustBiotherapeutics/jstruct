package com.just.jstruct.pojos;


import com.hfg.util.StringUtil;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.Serializable;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JstructId implements Serializable {
 
    private static final long serialVersionUID = 1L;

    
    
    private String idParam;
    private boolean invalidFormat = false;

    private String rcsbId;
    private Long structureId;
    private Integer structureVersion;
    


  
    /* constructors */ 
    
    public JstructId(String idParam) {
        this.idParam = idParam;
        initByIdParam();
    }
    
    public JstructId(Jstruct_FullStructure fullStructure) {
        initByFullStructure(fullStructure);
    }
    
    
    
    
    

    /**
     * parse the jstruct id from an existing full structure
     * @param fullStructure 
     */
    private void initByFullStructure(Jstruct_FullStructure fullStructure){
        if(fullStructure.isFromRcsb())
        {
            this.rcsbId = fullStructure.getPdbIdentifier();
            this.structureId = fullStructure.getStructureId();
        }
        else 
        {
            this.structureId = fullStructure.getStructureId();
            this.structureVersion = fullStructure.getVersion();
        }
    }
    
    
    
    /**
     * parse the structure id from a string
     * could be in formats: 
     *      JSTRUCT-12345, JSTRUCT-12345.3, 12345, 12345.3
     *      A12B
     */
    private void initByIdParam(){
        if(StringUtil.isSet(this.idParam)){
            
            String id = idParam.toUpperCase();
            
            if(id.length() == 4){
                this.rcsbId = id;
            }
            else 
            {
                //strip off JSTRUCT- if it has it
                String structureIdPrefix = ValueMapUtils.getInstance().getStructureIdPrefix();
                if (id.startsWith(structureIdPrefix.toUpperCase())) {
                    id = id.substring(structureIdPrefix.length());
                }
                
                //check for version appended after period
                if(id.contains(".")){
                    //get version if it's there
                    String[] splitId = id.split("\\.");
                    id = splitId[0];
                    String ver = splitId[1];
                    if(JStringUtil.isPositiveInteger(id) && JStringUtil.isPositiveInteger(ver)){
                        this.structureId = JStringUtil.stringToLong(id);
                        this.structureVersion = JStringUtil.stringToInteger(ver);
                    } else {
                        invalidFormat = true;
                    }
                }
                else 
                {
                    if(JStringUtil.isPositiveInteger(id)){
                        this.structureId = JStringUtil.stringToLong(id);
                    } else {
                        invalidFormat = true;
                    }
                }
                
            }
            
        }
    }
    

    
   
    
    
    /* getters setters */
    
    public String getIdParam() {return idParam;}
    
    public boolean isInvalidFormat() { return invalidFormat;}
    public void setInvalidFormat(boolean invalidFormat) {this.invalidFormat = invalidFormat;}
    

    public String getRcsbId() {return rcsbId;}
    public void setRcsbId(String rcsbId) { this.rcsbId = rcsbId; }

    public Long getStructureId() {return structureId;}
    public void setStructureId(Long jstructId) { this.structureId = jstructId; }

    public Integer getStructureVersion() {return structureVersion; }
    public void setStructureVersion(Integer jstructVersion) {this.structureVersion = jstructVersion;}

    public boolean hasRcsbId(){
        return StringUtil.isSet(rcsbId);
    }
    
    public boolean hasStructureId(){
        return structureId != null;
    }

    public boolean hasStructureVersion(){
        return structureVersion != null;
    }


   
    /* #################################################################################################
            static helper methods
       ################################################################################################# */





    
    /**
     * create the full jstructId
     * @param fullStructure
     * @return 
     */
    public static String createJstructIdForDisplay(Jstruct_FullStructure fullStructure){
        return createJstructIdForDisplay(fullStructure, 0);
    }

    /**
     * 
     * @param fullStructure
     * @param versionOffset
     * @return 
     */
    public static String createJstructIdForDisplay(Jstruct_FullStructure fullStructure, int versionOffset){

        int version = fullStructure.getVersion() + versionOffset;

        StringBuilder fullId = new StringBuilder();
        
        fullId.append(ValueMapUtils.getInstance().getStructureIdPrefix());
        fullId.append(fullStructure.getStructureId());
        
        if(fullStructure.isFromRcsb()){
            fullId.append(" [").append(fullStructure.getPdbIdentifier()).append("]");
        } else {
            fullId.append(".").append(version);
        }

        return fullId.toString();
    }
    
   
    
    
    
    
    

    


}
