package com.just.jstruct.pojos;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.AminoAcidService;
import com.just.jstruct.exception.JException;
import com.just.jstruct.exception.JImportException;
import com.just.jstruct.model.Jstruct_PdbFile;
import com.just.jstruct.model.Jstruct_StructVersion;
import com.just.jstruct.model.Jstruct_Structure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureImport.PdbDataUtils;
import com.just.jstruct.utilities.JFileUtils;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class BulkFileItem {

    
    private final AminoAcidService aminoAcidService = new AminoAcidService();
    


    private Jstruct_Structure structure;
    private Jstruct_StructVersion structVersion;
    private Jstruct_PdbFile pdbFile;
    private File file;
    private boolean pdbOrCifFile;
    
    private List<String> validationErrors;

    
    public BulkFileItem(File file, Jstruct_User user, String pdbTitle) {
        this.file = file;
        init(user, pdbTitle);
    }
    

    private void init(Jstruct_User user, String pdbTitle){
        
        try{
             
            if(JFileUtils.isPdbOrCifFile(file)){
                
                pdbOrCifFile = true;
                pdbFile = PdbDataUtils.createTempPdbFileFromParsedData(file, null, user);
                
            } else {
                pdbOrCifFile = false;
                pdbFile = new Jstruct_PdbFile();
                pdbFile.setTitle(pdbTitle);
            }
            
            //structVersion = new Jstruct_StructVersion(user);
            //structVersion.setVersion(1);
            
            structure = new Jstruct_Structure(user);
            structure.setFromRcsb(false);
            structure.setObsolete(false);
            structure.setOwner(user);
            structure.setOwnerId(user.getUserId());
            structure.setSourceName(ValueMapUtils.getInstance().getManualUploadSource());
            
        } catch (JException | JImportException e) {
            e.printStackTrace();
        }
    }

    
    
    
    
    
    public Jstruct_Structure getStructure() { return structure; }
    public void setStructure(Jstruct_Structure structure) { this.structure = structure;}
    
    public Jstruct_StructVersion getStructVersion() {return structVersion; }
    public void setStructVersion(Jstruct_StructVersion structVersion) { this.structVersion = structVersion; }

    public Jstruct_PdbFile getPdbFile() { return pdbFile; }
    public void setPdbFile(Jstruct_PdbFile pdbFile) { this.pdbFile = pdbFile;} 
    
    public File getFile() { return file; }

    public boolean isPdbOrCifFile() {return pdbOrCifFile;}
    public void setPdbOrCifFile(boolean pdbOrCifFile) { this.pdbOrCifFile = pdbOrCifFile; }

    
    public void clearValidationErrors(){
        validationErrors = null;
    }
    
    public List<String> getValidationErrors() { return validationErrors; }
    
    public String getValidationErrorsString(){
        if(CollectionUtil.hasValues(validationErrors)){
            return StringUtil.join(validationErrors, ";  ");
        }
        return null;
    }
    
    public void addValidationErrors(String validationError) {
        if(!CollectionUtil.hasValues(validationErrors)){
            validationErrors = new ArrayList<>(1);
        }
        validationErrors.add(validationError);
    }
   

    
    
    
    
    
    

    
}
