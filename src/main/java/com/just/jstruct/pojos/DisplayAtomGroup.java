package com.just.jstruct.pojos;

import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.enums.ResidueType;
import com.just.jstruct.model.Jstruct_AtomGrouping;
import com.just.jstruct.model.Jstruct_Residue;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class DisplayAtomGroup {
    
    
    
    private Jstruct_AtomGrouping atomGrouping;
    private String indexLabel;
    
    private List<DisplayResidue> anyCoordsResidues;
    private List<DisplayResidue> allCoordsResidues;
    private List<DisplayResidue> caCoordsResidues;
    private List<DisplayResidue> bkbnCoordsResidues;
    private List<DisplayResidue> bkbnCbCoordsResidues;

    private Map<Integer, ResAttribute> anyCoordsResidueMap;
    private Map<Integer, ResAttribute> allCoordsResidueMap;
    private Map<Integer, ResAttribute> caCoordsResidueMap;
    private Map<Integer, ResAttribute> bkbnCoordsResidueMap;
    private Map<Integer, ResAttribute> bkbnCbCoordsResidueMap;
    
    
    
    
    public DisplayAtomGroup(Jstruct_AtomGrouping atomGrouping, String indexLabel) {
        
        this.atomGrouping = atomGrouping;
        this.indexLabel = indexLabel;
        
        anyCoordsResidueMap = new LinkedHashMap<>();
        allCoordsResidueMap = new LinkedHashMap<>();
        caCoordsResidueMap = new LinkedHashMap<>();
        bkbnCoordsResidueMap = new LinkedHashMap<>();
        bkbnCbCoordsResidueMap = new LinkedHashMap<>();
        
        int iAny, iAll, iCa, iBkbn, iBkbnCb;
        iAny = iAll = iCa = iBkbn = iBkbnCb = 0;
        for(Jstruct_Residue residue : atomGrouping.getResidues()){
            
            anyCoordsResidueMap.put(iAny++, new ResAttribute(residue));
            
            if(residue.hasAllCoords()){
                allCoordsResidueMap.put(iAll++, new ResAttribute(residue));
            }
            if(residue.hasCaCoords()){
                caCoordsResidueMap.put(iCa++, new ResAttribute(residue));
            }
            if(residue.hasBkbnCoords()){
                bkbnCoordsResidueMap.put(iBkbn++, new ResAttribute(residue));
            }
            if(residue.hasBkbnCbCoords()){
                bkbnCbCoordsResidueMap.put(iBkbnCb++, new ResAttribute(residue));
            }
        }
        
        anyCoordsResidues = createDisplayResidueList(atomGrouping.getSeqWithAnyCoords(), anyCoordsResidueMap);
        allCoordsResidues = createDisplayResidueList(atomGrouping.getSeqWithAllCoords(), allCoordsResidueMap);
        caCoordsResidues = createDisplayResidueList(atomGrouping.getSeqWithCaCoords(), caCoordsResidueMap);
        bkbnCoordsResidues = createDisplayResidueList(atomGrouping.getSeqWithBkbnCoords(), bkbnCoordsResidueMap);
        bkbnCbCoordsResidues = createDisplayResidueList(atomGrouping.getSeqWithBkbnCbCoords(), bkbnCbCoordsResidueMap);
 
    }

    
    
    
    /**
     * 
     * @param seq 
     */
    private List<DisplayResidue> createDisplayResidueList(String seq, Map<Integer, ResAttribute> residueMap){
        
        List<DisplayResidue> displayResidues = new ArrayList<>(residueMap.size());

        int len = seq.length();
        for (int i=0; i<len; i++){
            
            char letter = seq.charAt(i);
            if(CollectionUtil.hasValues(residueMap)) {
                
                ResAttribute resAttr = residueMap.get(i);
                String hexColor = determineHexColor(resAttr.getResidueType());
                Character icode = resAttr.getIcode();
                
                displayResidues.add(new DisplayResidue(resAttr.getOrdinal(), icode, letter, resAttr.getAbbrev(), resAttr.getResidueType().name(), hexColor, resAttr.getAtoms()));
                
            } else {
                
                displayResidues.add(new DisplayResidue(letter));
                
            }
            
        }
        return displayResidues;

    }
    
    
    
    private String determineHexColor(ResidueType residuetype) {

        String hexColor = "000000";
        switch (residuetype) {
            case STANDARD_AMINO_ACID:
            case NUCLEOTIDE:
                break;
            case NON_STANDARD_AMINO_ACID:
                hexColor = "cc22cc";
                break;
            case UNKNOWN:
                //n and X are unknown. red.
                hexColor = "ff0000";
                break;
        }
        return hexColor;
    }
    
    
    
    /* getters and setters */

    public Jstruct_AtomGrouping getAtomGrouping() {
        return atomGrouping;
    }

    public void setAtomGrouping(Jstruct_AtomGrouping atomGrouping) {
        this.atomGrouping = atomGrouping;
    }

    public String getIndexLabel() {
        return indexLabel;
    }

    public void setIndexLabel(String indexLabel) {
        this.indexLabel = indexLabel;
    }

    public List<DisplayResidue> getAnyCoordsResidues() {
        return anyCoordsResidues;
    }

    public void setAnyCoordsResidues(List<DisplayResidue> anyCoordsResidues) {
        this.anyCoordsResidues = anyCoordsResidues;
    }

    public List<DisplayResidue> getAllCoordsResidues() {
        return allCoordsResidues;
    }

    public void setAllCoordsResidues(List<DisplayResidue> allCoordsResidues) {
        this.allCoordsResidues = allCoordsResidues;
    }

    public List<DisplayResidue> getCaCoordsResidues() {
        return caCoordsResidues;
    }

    public void setCaCoordsResidues(List<DisplayResidue> caCoordsResidues) {
        this.caCoordsResidues = caCoordsResidues;
    }

    public List<DisplayResidue> getBkbnCoordsResidues() {
        return bkbnCoordsResidues;
    }

    public void setBkbnCoordsResidues(List<DisplayResidue> bkbnCoordsResidues) {
        this.bkbnCoordsResidues = bkbnCoordsResidues;
    }

    public List<DisplayResidue> getBkbnCbCoordsResidues() {
        return bkbnCbCoordsResidues;
    }

    public void setBkbnCbCoordsResidues(List<DisplayResidue> bkbnCbCoordsResidues) {
        this.bkbnCbCoordsResidues = bkbnCbCoordsResidues;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
