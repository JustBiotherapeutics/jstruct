package com.just.jstruct.pojos;

import com.just.jstruct.model.Jstruct_FullStructure;
import java.util.Date;
import org.apache.commons.io.FileUtils;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JFileAttributes {
    
    
    private String uploadFileName;
    private String actualFileName;
    private String displayedFileName;
    
    private Date creationDate;
    private Date modifiedDate;
    private Date depositDate;
    private Date jstructInsertDate;
    private Date jstructUpdateDate;
    
    private Integer compressedSize;
    private Long uncompressedSize;
    private String compressedSizeDisplay;
    private String uncompressedSizeDisplay;
    
    private String ownerName;
    private String sourceName;

    
    
    
    public JFileAttributes(Jstruct_FullStructure fullStructure) {
        
        uploadFileName = fullStructure.getUploadFileName();
        actualFileName = fullStructure.getActualFileName();
        
        creationDate = fullStructure.getFileCreationDate();
        modifiedDate = fullStructure.getFileModifiedDate();
        depositDate = fullStructure.getPdbDepositionDate();
        jstructInsertDate = fullStructure.getStructFileInsertDate();
        jstructUpdateDate = fullStructure.getStructFileUpdateDate();
        
        compressedSize = fullStructure.getCompressedSize();
        uncompressedSize = fullStructure.getUncompressedSize();
        
        compressedSizeDisplay = FileUtils.byteCountToDisplaySize(compressedSize==null ? 0 : compressedSize);
        uncompressedSizeDisplay = FileUtils.byteCountToDisplaySize(uncompressedSize==null ? 0 : uncompressedSize);
        
        ownerName = fullStructure.getOwnerName();
        sourceName = fullStructure.getSourceName();
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String getActualFileName() {
        return actualFileName;
    }

    public void setActualFileName(String actualFileName) {
        this.actualFileName = actualFileName;
    }

    public String getDisplayedFileName() {
        return displayedFileName;
    }

    public void setDisplayedFileName(String displayedFileName) {
        this.displayedFileName = displayedFileName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public Integer getCompressedSize() {
        return compressedSize;
    }

    public void setCompressedSize(Integer compressedSize) {
        this.compressedSize = compressedSize;
    }

    public Long getUncompressedSize() {
        return uncompressedSize;
    }

    public void setUncompressedSize(Long uncompressedSize) {
        this.uncompressedSize = uncompressedSize;
    }

    public String getCompressedSizeDisplay() {
        return compressedSizeDisplay;
    }

    public void setCompressedSizeDisplay(String compressedSizeDisplay) {
        this.compressedSizeDisplay = compressedSizeDisplay;
    }

    public String getUncompressedSizeDisplay() {
        return uncompressedSizeDisplay;
    }

    public void setUncompressedSizeDisplay(String uncompressedSizeDisplay) {
        this.uncompressedSizeDisplay = uncompressedSizeDisplay;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public Date getJstructInsertDate() {
        return jstructInsertDate;
    }

    public void setJstructInsertDate(Date jstructInsertDate) {
        this.jstructInsertDate = jstructInsertDate;
    }

    public Date getJstructUpdateDate() {
        return jstructUpdateDate;
    }

    public void setJstructUpdateDate(Date jstructUpdateDate) {
        this.jstructUpdateDate = jstructUpdateDate;
    }
    
    
    
}
