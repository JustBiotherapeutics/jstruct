package com.just.jstruct.pojos;

import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.enums.ResidueType;
import com.just.jstruct.model.Jstruct_Chain;
import com.just.jstruct.model.Jstruct_SeqResidue;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class DisplaySequence {
    
    
    
    private Jstruct_Chain chain;
    private List<DisplayResidue> displayResidues;
    private String indexLabel;

    private Map<Integer, ResAttribute> seqResidueMap;
    
    
    
    
    public DisplaySequence(Jstruct_Chain chain, String indexLabel) {
        
        this.chain = chain;
        this.indexLabel = indexLabel;
        
        seqResidueMap = new LinkedHashMap<>();
        int i=0;
        if(CollectionUtil.hasValues(chain.getSeqResidues())){
            for(Jstruct_SeqResidue seqResidue : chain.getSeqResidues()){
                seqResidueMap.put(i++, new ResAttribute(seqResidue));
            }
        }
        
        init(chain.getSeqTheoretical());
    }

    
    
    
    /**
     * 
     * @param seq 
     */
    private void init(String seq){
        
        displayResidues = new ArrayList<>();

        int len = seq.length();
        for (int i=0; i<len; i++){
            
            char letter = seq.charAt(i);
            if(CollectionUtil.hasValues(seqResidueMap)) {
                
                ResAttribute resAttr = seqResidueMap.get(i);
                String hexColor = determineHexColor(resAttr.getResidueType());
                Character icode = resAttr.getIcode();
                
                displayResidues.add(new DisplayResidue(resAttr.getOrdinal(), icode, letter, resAttr.getAbbrev(), resAttr.getResidueType().name(), hexColor, resAttr.getAtoms()));
                
            } else {
                
                displayResidues.add(new DisplayResidue(letter));
                
            }
            
        }


    }
    
    
    
    private String determineHexColor(ResidueType residuetype) {

        String hexColor = "000000";
        switch (residuetype) {
            case STANDARD_AMINO_ACID:
            case NUCLEOTIDE:
                break;
            case NON_STANDARD_AMINO_ACID:
                hexColor = "cc22cc";
                break;
            case UNKNOWN:
                //n and X are unknown. red.
                hexColor = "ff0000";
                break;
        }
        return hexColor;
    }
    
    
    
    
    
    
    
    
    
    
    
    public Jstruct_Chain getChain() {
        return chain;
    }

    public void setChain(Jstruct_Chain chain) {
        this.chain = chain;
    }

    public List<DisplayResidue> getDisplayResidues() {
        return displayResidues;
    }

    public void setDisplayResidues(List<DisplayResidue> displayResidues) {
        this.displayResidues = displayResidues;
    }

    public String getIndexLabel() {
        return indexLabel;
    }

    public void setIndexLabel(String indexLabel) {
        this.indexLabel = indexLabel;
    }

    public Map<Integer, ResAttribute> getSeqResidueMap() {
        return seqResidueMap;
    }

    public void setSeqResidueMap(Map<Integer, ResAttribute> seqResidueMap) {
        this.seqResidueMap = seqResidueMap;
    }
    
    
    
    
    
    
    
    
    
}
