package com.just.jstruct.pojos;

import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.utilities.FastaUtil;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureFastaLink {

    
    
    private final String proteinSeqresFastaUrl;
    private final String proteinAtomFastaUrl;
    private final String nucleicSeqresFastaUrl;
    private final String nucleicAtomFastaUrl;
    private final String allSeqresFastaUrl;
    private final String allAtomFastaUrl;
    
    
    
    public StructureFastaLink(Jstruct_FullStructure fullStructure) {
        
        
        String jstructId = fullStructure.getJstructDisplayId().replace(" ", "");
        
        String FASTA_SUBQUERY_STRING = "STRUCT_VER_IDS;inputStructVerIds:" + fullStructure.getStructVerId().toString();
        
        
        proteinSeqresFastaUrl = FastaUtil.createFastaUrl(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.AND, 
                "ATOM", 
                FastaUtil.CHAIN_TYPE_PROTEIN, 
                jstructId + "_protein_atom.fasta", 
                FASTA_SUBQUERY_STRING);

        proteinAtomFastaUrl = FastaUtil.createFastaUrl(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.AND, 
                "SEQRES", 
                FastaUtil.CHAIN_TYPE_PROTEIN, 
                jstructId + "_protein_seqres.fasta", 
                FASTA_SUBQUERY_STRING);

        nucleicSeqresFastaUrl = FastaUtil.createFastaUrl(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.AND, 
                "ATOM", 
                FastaUtil.CHAIN_TYPE_NUCLEIC_ACID, 
                jstructId + "_nucleicacid_atom.fasta", 
                FASTA_SUBQUERY_STRING);
 
        nucleicAtomFastaUrl = FastaUtil.createFastaUrl(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.AND, 
                "SEQRES", 
                FastaUtil.CHAIN_TYPE_NUCLEIC_ACID, 
                jstructId + "_nucleicacid_seqres.fasta", 
                FASTA_SUBQUERY_STRING);

        allSeqresFastaUrl = FastaUtil.createFastaUrl(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.AND, 
                "ATOM", 
                FastaUtil.CHAIN_TYPE_ALL, 
                jstructId + "_atom.fasta", 
                FASTA_SUBQUERY_STRING);

        allAtomFastaUrl = FastaUtil.createFastaUrl(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.AND, 
                "SEQRES", 
                FastaUtil.CHAIN_TYPE_ALL, 
                jstructId+ "_seqres.fasta", 
                FASTA_SUBQUERY_STRING);
    
    }
    
    
    
    
    
    public String getProteinSeqresFastaUrl() {return proteinSeqresFastaUrl;}
    public String getProteinAtomFastaUrl() {return proteinAtomFastaUrl;}
    public String getNucleicSeqresFastaUrl() {return nucleicSeqresFastaUrl;}
    public String getNucleicAtomFastaUrl() {return nucleicAtomFastaUrl;}
    public String getAllSeqresFastaUrl() {return allSeqresFastaUrl;}
    public String getAllAtomFastaUrl() {return allAtomFastaUrl;}
    
    
    
    
    
    
    
    
    
}
