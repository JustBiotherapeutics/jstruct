package com.just.jstruct.enums;



/* *
 *   Enumeration of JStruct User status values
 *   <div>
 *    @author Russell Elbert Williams
 *   </div>
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum UserStatus
{
   ACTIVE,
   INACTIVE;


   public UserStatus next() {
      return this.ordinal() < UserStatus.values().length -1
              ? UserStatus.values()[this.ordinal() + 1]
              : UserStatus.ACTIVE;
   }

}

