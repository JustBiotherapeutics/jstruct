package com.just.jstruct.enums;

import java.util.ArrayList;
import java.util.List;




/**
 *   Enumeration for the
 *      STRUCT_FILE.PROCESS_CHAIN_STATUS
 *      Jstruct_StructFile - processChainStatus()
 *   field
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum ProcessChainStatus
{

   TODO,
   FAILED,
   COMPLETE;



    /**
     *
     * @return
     */
    public static List<ProcessChainStatus> processChainStatuss(){
        List<ProcessChainStatus> statuss = new ArrayList<>(3);
        for(ProcessChainStatus pcs : ProcessChainStatus.values()){
            statuss.add(pcs);
        }
        return statuss;
    }

}
