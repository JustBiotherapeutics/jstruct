package com.just.jstruct.enums;




/**
 *   Enumeration for the
 *      IMPORT_JOB.STATUS
 *      Jstruct_ImportJob - status()
 *   field
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum JobType
{
   INITIAL_RCSB_RUN,
   INITIAL_RCSB_RUN_RECOVER,
   INCREMENTAL_RCSB_RUN,
   INITIAL_RCSB_RERUN,
   RCSB_HARMONIZE,
   SELECTED_RCSB_FILES,
   
   MANUAL_IMPORT,
   
   UPDATE_ALL_CHAINS,
   UPDATE_MANUAL_CHAINS,
   UPDATE_MISSING_CHAINS,
   
   CREATE_BLAST_DB;

}
