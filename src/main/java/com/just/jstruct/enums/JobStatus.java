package com.just.jstruct.enums;




/**
 *   Enumeration for the
 *      IMPORT_JOB.STATUS
 *      Jstruct_ImportJob - status()
 *   field
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum JobStatus
{

   INITIALIZING,
   QUERYING,
   IMPORTING,
   PROCESSING,
   FAILED,
   SUCCESS,
   CANCELLED;

}
