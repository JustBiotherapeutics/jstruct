package com.just.jstruct.enums;

import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * some predefined fasta download urls - currently shows up in the Admin Actions section
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum FastaQuery {
    
    ALL_RCSB (
            "All RCSB structures currently listed as Active",
            "all_rcsb",
            FileStatus.FILE_STATUS_ACTIVE,
            FileStatus.FILE_STATUS_NONE,
            QueryMode.AND,
            "EVERYTHING"),
    
    
    ONLY_MANUAL_MODELS(
            "Manually uploaded 'model' structures",
            "models",
            FileStatus.FILE_STATUS_NONE,
            FileStatus.FILE_STATUS_ACTIVE,
            QueryMode.AND,
            "MODEL;yesNoModelOption:OPTION_YES"),
    
    
    ONLY_MANUAL_NON_MODELS(
            "Manually uploaded 'non-model' structures",
            "non_models",
            FileStatus.FILE_STATUS_NONE,
            FileStatus.FILE_STATUS_ACTIVE,
            QueryMode.AND,
            "MODEL;yesNoModelOption:OPTION_NO");

    
    
    private final String description;
    private final String outputFileNameRoot;
    private final FileStatus fileStatusRcsb;
    private final FileStatus fileStatusManual;
    private final QueryMode queryMode;
    private final String subquery; 
    
    
    

    
    FastaQuery(String description, String outputFileNameRoot, FileStatus fileStatusRcsb, FileStatus fileStatusManual, QueryMode queryMode, String subquery) {
        this.description = description;
        this.outputFileNameRoot = outputFileNameRoot;
        this.fileStatusRcsb = fileStatusRcsb;
        this.fileStatusManual = fileStatusManual;
        this.queryMode = queryMode;
        this.subquery = subquery;
    }

    
    public String getDescription() { return description;}
    public String getOutputFileNameRoot() {return outputFileNameRoot;}
    public FileStatus getFileStatusRcsb() {return fileStatusRcsb;}
    public FileStatus getFileStatusManual() {return fileStatusManual;}
    public QueryMode getQueryMode() {return queryMode; }
    public String getSubQuery() {return subquery; }
    
    
    /*
    public String getProteinSequresUrl(){
        return FastaUtil.createFastaUrl(fileStatusRcsb, fileStatusManual, queryMode, "SEQRES", FastaUtil.chainTypeProtein, outputFileNameRoot + "protein_seqres.fasta", subquery);
    }
    
    public String getProteinAtomUrl(){
        return FastaUtil.createFastaUrl(fileStatusRcsb, fileStatusManual, queryMode, "ATOM", FastaUtil.chainTypeProtein, outputFileNameRoot + "protein_atom.fasta", subquery);
    }
    
    
    
    public String getNucleicAcidSeqresUrl(){
        return FastaUtil.createFastaUrl(fileStatusRcsb, fileStatusManual, queryMode, "SEQRES", FastaUtil.chainTypeNucleicAcid, outputFileNameRoot + "nucleicacid_seqres.fasta", subquery);
    }
    
    public String getNucleicAcidAtomUrl(){
        return FastaUtil.createFastaUrl(fileStatusRcsb, fileStatusManual, queryMode, "ATOM", FastaUtil.chainTypeNucleicAcid, outputFileNameRoot + "nucleicacid_atom.fasta", subquery);
    }
    
    
    
    public String getAllSeqresUrl(){
        return FastaUtil.createFastaUrl(fileStatusRcsb, fileStatusManual, queryMode, "SEQRES", FastaUtil.chainTypeAll, outputFileNameRoot + "seqres.fasta", subquery);
    }
    
    public String getAllAtomUrl(){
        return FastaUtil.createFastaUrl(fileStatusRcsb, fileStatusManual, queryMode, "ATOM", FastaUtil.chainTypeAll, outputFileNameRoot + "atom.fasta", subquery);
    }
    
    */
    
    
    
    
    /**
     *
     * @return
     */
    public static List<FastaQuery> allFastaQuerys() {
        List<FastaQuery> all = new ArrayList<>(3);
        all.addAll(Arrays.asList(FastaQuery.values()));
        return all;
    }

}
