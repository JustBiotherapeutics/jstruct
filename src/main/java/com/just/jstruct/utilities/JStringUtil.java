package com.just.jstruct.utilities;

import com.hfg.util.StringUtil;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;





/**
 * general String utilities
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public final class JStringUtil {
    
    
    private final static Pattern LTRIM = Pattern.compile("^\\s+");
    private final static Pattern RTRIM = Pattern.compile("\\s+$");
    private final static Pattern WHTE_SPCE = Pattern.compile("\\s+");
    
    private final static Pattern NUMERIC = Pattern.compile("[-+]?\\d*\\.?\\d+");
    
    
    //convert a String to a <String> List based on a delimiter
    public static List<String> stringToList(String s, String delimiter){
        String [] stringArray = s.split(delimiter);
        List <String> list = Arrays.asList(stringArray);
        return list;
    }
    


    //convert a List of strings to one string, putting the 'delimiter' between each element
    public static String listToString(List<String> strings, String delimiter){
        StringBuilder sb = new StringBuilder("");
        //create a single string(buffer) outta our list of strings
        if (strings!=null && !strings.isEmpty()){
            for (String s : strings){
                sb.append(s);
                sb.append(delimiter);
            }
            //remove the final seperator
            sb.setLength(sb.length() - delimiter.length());
        }
        return sb.toString();
    }




    //convert a List of BigDecimal to one string, putting the 'delimiter' between each element
    public static String bigDListToString(List<BigDecimal> bigDs, String delimiter){
        StringBuilder sb = new StringBuilder("");
        //create a single string(buffer) outta our list of numbers
        if (bigDs!=null && !bigDs.isEmpty()){
            for (BigDecimal d : bigDs){
                sb.append(d.toString());
                sb.append(delimiter);
            }
            //remove the final seperator
            sb.setLength(sb.length() - delimiter.length());
        }
        return sb.toString();
    }
    
    
    //convert a List of BigDecimal to one string, putting the 'delimiter' between each element
    public static String longListToString(List<Long> longs, String delimiter){
        StringBuilder sb = new StringBuilder("");
        //create a single string(buffer) outta our list of numbers
        if (longs!=null && !longs.isEmpty()){
            for (Long l : longs){
                sb.append(l.toString());
                sb.append(delimiter);
            }
            //remove the final seperator
            sb.setLength(sb.length() - delimiter.length());
        }
        return sb.toString();
    }



    //convert a String to a <String> Collection based on a delimiter
    public static Collection<String> stringToCollection(String s, String delimiter){
        String [] stringArray = s.split(delimiter);
        Collection <String> c = Arrays.asList(stringArray);
        return c;
    }
    
    
    public static List<String> splitByChunkSize(String s, int chunkSize) {

        List<String> strings = new ArrayList<>();
        int index = 0;
        while (index < s.length()) {
            strings.add(s.substring(index, Math.min(index + chunkSize, s.length())));
            index += chunkSize;
        }
        return strings;
    }
    



    //convert a collection of strings to one string, putting the 'delimiter' between each element
    public static String collectionToString(Collection<String> strings, String delimiter){
        StringBuilder sb = new StringBuilder("");
        //create a single string(buffer) outta our collection of strings
        if (strings!=null && !strings.isEmpty()){
            for (String s : strings){
                sb.append(s);
                sb.append(delimiter);
            }
            //remove the final seperator
            sb.setLength(sb.length() - delimiter.length());
        }
        return sb.toString();
    }


 
    //method that converts a string to a Long (returns null if string cant be converted)
    public static Long stringToLong(String s){
        Long longValue;
        try {
            longValue = Long.parseLong(s);
        } catch (Exception e) {
            longValue = null;
        }
        return longValue;
    }
    
    
     //method that converts a string to a double (returns null if string cant be converted)
    public static Double stringToDouble(String s){
        Double doubleValue;
        try {
            doubleValue = Double.parseDouble(s);
        } catch (Exception e) {
            doubleValue = null;
        }
        return doubleValue;
    }


    public static String longToString(Long l){
        String s = String.valueOf(l);
        return s;
    }
    
    
    public static String integerToString(Integer i){
        if(null==i){
            return null;
        }
        return i.toString();
    }
    
    //method that converts a string to an Integer (returns null if string cant be converted)
    public static Integer stringToInteger(String s){
        Integer integerValue;
        try {
            integerValue = Integer.parseInt(s);
        } catch (Exception e) {
            integerValue = null;
        }
        return integerValue;
    }
    


    public static String booleanToString(boolean b){
        return b ? "True" : "False";
    }


    
    //return true if a string is null or blank/empty (including white space)
    public static boolean isNullOrEmpty(String s){
        boolean x = (s==null || s.length()==0 || s.trim().equals(""));
        return x;
    }


    //check
    public static boolean valuesAreSame(String val1, String val2, boolean ignoreCase) {
        
        val1 = isNullOrEmpty(val1) ? "" : val1.trim();
        val2 = isNullOrEmpty(val2) ? "" : val2.trim();
        
        if (ignoreCase) {
            return val1.equalsIgnoreCase(val2);
        }
        return val1.equals(val2);
    }
   


    //return the stack trace of an exception as a string
    //ues in liu of doing a (thread unsafe) e.printStackTrace
    public static String stackTraceAsString(Exception exception, boolean formatForHtml) {

        String stackTraceOut;

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        stackTraceOut = sw.toString();
        if (formatForHtml) {
            stackTraceOut = stackTraceOut.replaceAll("<", "&lt;");
            stackTraceOut = stackTraceOut.replaceAll(">", "&gt;");
            stackTraceOut = stackTraceOut.replaceAll("at ", "<br/>&nbsp;&nbsp;&nbsp;&nbsp; at ");
            stackTraceOut = stackTraceOut.replaceAll("com.just.jstruct", "<font color='#0000ff'>com.just.jstruct</font>");
        }

        return stackTraceOut;

    }




    public static Collection<String> sortStringCollection(Collection<String> stringCollection) {
        List<String> stringList = new ArrayList<>(stringCollection);
        Collections.sort(stringList);
        Collection<String> sortedCollection = new ArrayList<>(stringList);
        return sortedCollection;
    }






    public static String safeHtml(String in){

        String out = org.apache.commons.lang.StringEscapeUtils.escapeHtml(in);
        out = out.replaceAll(" ", "&nbsp;");
        out = out.replaceAll("\\n", "<br/>");

        return out;
    }
    
    
    
    /**
     * determine if a string can evaluate to a number
     * @param s to parse
     * @return true if string evaluates to a number (if string is not set, this will return false)
     */
    public static boolean isNumeric(String s){
        if(!StringUtil.isSet(s)){
            return false;
        }
        Matcher m = NUMERIC.matcher(s);
        return m.matches();
    }

    /**
     * determine if a string can evaluate to a positive integer/int
     * @param s to parse
     * @return true if the number evaluates to a positive integer (0 is also returns true)
     */
    public static boolean isPositiveInteger(String s){
        if (s == null) {
            return false;
        }
        int length = s.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        for (; i < length; i++) {
            char c = s.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

    
    
    
    
    public static String cleaveString(String s, int len){
        if(!StringUtil.isSet(s)){
            return "";
        }
        if (len<0){
            return s;
        }
        if(s.length()<len+1){
            return s;
        }
        return s.substring(0,len) + "...";
    }


    /**
     * Left trim whitespace from a string
     * @param s string to trim
     * @return trimmed string
     */
    public static String leftTrim(String s) {
        return LTRIM.matcher(s).replaceAll("");
    }


    /**
     * Right trim whitespace from a string
     * @param s string to trim
     * @return trimmed string
     */
    public static String rightTrim(String s) {
        return RTRIM.matcher(s).replaceAll("");
    }

    /**
     * condense all whitespace to single space
     * @param s string to condense
     * @return string with condensed whitespace
     */
    public static String collapseWhitespace(String s) {
        return WHTE_SPCE.matcher(s).replaceAll(" ");
    }


    public static String onlyTextBeforeFirstSpace(String s){
        if(!StringUtil.isSet(s)){
            return "";
        }
        int spaceIndex = s.indexOf(" ");
        if(spaceIndex != -1){
            return s.substring(0, spaceIndex);
        }
        return s;
    }

    
    public static String onlyTextAfterLastOccurance(String s, String delimitor){
        if(!StringUtil.isSet(s)){
            return "";
        }
        if(!s.contains(delimitor)){
            return s;
        }
        int delIndex = s.lastIndexOf(delimitor);
        return s.substring(delIndex+1);
    }
    
    public static String onlyTextBeforeLastOccurance(String s, String delimitor){
        if(!StringUtil.isSet(s)){
            return "";
        }
        if(!s.contains(delimitor)){
            return s;
        }
        int delIndex = s.lastIndexOf(delimitor);
        return s.substring(0,delIndex);
    }



    /**
     * split a string on any whitespace - return an array
     * @param s string to split
     * @return array of strings split based on any whitespace
     */
    public static String[] splitOnWhitespace(String s) {
        if(!StringUtil.isSet(s)){
            return null;
        }
        return s.split("\\s+");
    }

    /**
     * split a string on commas - return an array where each element is already trimmed
     * @param s string to split
     * @return array of strings split on their comma - all elements will also be trimmed
     */
    public static String[] splitOnCommaAndTrim(String s) {
        if(!StringUtil.isSet(s)){
            return null;
        }
        return  s.split("\\s*,\\s*");
    }


    /**
     * split a string on spaces and commas (works well for splitting user inputted PDB Identifiers
     * @param s
     * @return
     */
    public static String[] splitOnSpaceAndComma(String s){
        if(!StringUtil.isSet(s)){
            return null;
        }
        String trimmed = s.trim();
        if(!StringUtil.isSet(trimmed)){
            return null;
        }
        return trimmed.replaceAll(",", " ").split("\\s+");
    }


    /**
     * return a sequence string split into multiple lines
     * @param inSeq Sequence to re-format
     * @param inLineLength max length of each line
     * @return list of sequence strings, that are only inLineLength long
     */
    public static List<String> splitSequence(String inSeq, int inLineLength) {

        if (!StringUtil.isSet(inSeq)) {
            return Arrays.asList();
        }
        if (inLineLength < 1) {
            return Arrays.asList(inSeq);
        }

        List<String> parts = new ArrayList<>();
        int len = inSeq.length();
        for (int i=0; i<len; i+=inLineLength)
        {
            parts.add(inSeq.substring(i, Math.min(len, i + inLineLength)));
        }

        return parts;

    }
}
