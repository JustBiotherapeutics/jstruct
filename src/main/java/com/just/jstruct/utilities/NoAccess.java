package com.just.jstruct.utilities;

import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.SessionBean;
import java.util.Date;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;




/**
 * backing bean of the no_access page
 * when a user attempts to access a page they do not have permissions for
 * send them here with some specific info about their attempt and their role9s)
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="noAccess")
@ViewScoped
public class NoAccess {

    private static final Logger LOGGER = Logger.getLogger(NoAccess.class.getName());
    
    @ManagedProperty(value="#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    
    private final NoAccessException noAccessException;
    private final String when;
    
    
    /*
     * 
     */
    public NoAccess() {

        noAccessException = (NoAccessException) FacesUtil.retrieveFromSession("no_access_exception");
        when = new Date().toString();
        
    }


    //getters/setters
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    public Jstruct_User getCurrentUser() { return currentUser; }
    
    public NoAccessException getException() { return noAccessException; }
    public String getWhen() { return when; }



    /**
     * static function to be called when an attempt to access a page user doesn't have permissions for
     * this will store the information/exception in the session, and redirect to the no_access page
     * where the info will be displayed to the user and instructions on what to do next (or something...)
     * @param nae 
     */
    public static void gotoNoAccessPage(NoAccessException nae){
        //put exception in session, then go to error page
        FacesUtil.storeOnSession("no_access_exception", nae);
        FacesUtil.redirectRequest("/no_access.xhtml?faces-redirect=true");
    }


    
}
