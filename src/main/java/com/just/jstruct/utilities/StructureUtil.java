package com.just.jstruct.utilities;

import com.hfg.html.Div;
import com.hfg.html.HTMLTag;
import com.hfg.svg.SVG;
import com.hfg.svg.SvgGroup;
import com.hfg.svg.SvgRect;
import com.hfg.util.StringUtil;
import com.hfg.xml.XMLNode;
import com.hfg.xml.XMLTag;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.PdbRevdatService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_PdbRevdat;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * utility methods for Structure
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureUtil {

    
    
    
    
    /**
     * convert a string parameter to a jstruct id 
     * @param s
     * @return 
     */
    public static Long stringToJstructId(String s) {
        
        if(StringUtil.isSet(s) && s.length() > 4){
          
            //strip the JSTRUCT- prefix if it has it
            String structureIdPrefix = ValueMapUtils.getInstance().getStructureIdPrefix();

            if (s.toUpperCase().startsWith(structureIdPrefix.toUpperCase())) {
                s = s.substring(structureIdPrefix.length());
            }

            if (JStringUtil.isPositiveInteger(s)) {
                return JStringUtil.stringToLong(s);

            }
        }
        
        return null;
    }
    
    
    

    /**
     * convert a string parameter to a rcsb id (4 character string)
     * @param s
     * @return 
     */
    public static String stringToRcsbId(String s){
        
        if(StringUtil.isSet(s) && s.length() == 4){
            return s.toUpperCase();
        }
        
        return null;
    }
    
    
    
    private static final int FONT_SIZE = 20;
    private static final int HEADER_Y = 25;

    private static final int GRID_X = 10;
    private static final int GRID_Y = 30;
    private static final int LINE_SPACE = 30;
    private static final int LINE_X = 100;

    private static final int FULL_WIDTH = 950;

    private static final Date START_DATE = JDateUtil.createDate(1970, Calendar.JANUARY, 1);
    
    
    public static HTMLTag createRcsbLineageDiagram(String pdbIdentifier) throws JDAOException {
        
        //service classes
        JstructUserService jstructUserService = new JstructUserService();
        FullStructureService fullStructureService = new FullStructureService(jstructUserService.getGuestUser());
        PdbRevdatService pdbRevdatService = new PdbRevdatService();
        
        //need all related full structures that are part of the lineage
        List<Jstruct_FullStructure> fullLineageStructures = fullStructureService.getFullStructureLineageBasedOnPdbIdentifier(pdbIdentifier);
        
        
         Div div = new Div();
        
        //viewbox, and date headers
            int viewboxHeight = 55 + (LINE_SPACE * (fullLineageStructures.size() - 1));
            SVG svg = new SVG().setViewBox(new Rectangle(0, 0, FULL_WIDTH, viewboxHeight)).setStyle("width:100%; height:100%;");
            SvgGroup textGrp = svg.addGroup().setFontFamily("monospace").setFontWeight("normal").setFontSize(JStringUtil.integerToString(FONT_SIZE));
            svg.setAttribute("preserveAspectRatio", "none");
            textGrp.addText(JDateUtil.defaultDateFormat(START_DATE)).setX(LINE_X).setY(HEADER_Y);
            textGrp.addText(JDateUtil.defaultDateFormat(new Date())).setX(FULL_WIDTH -120).setY(HEADER_Y);

            //create each PDB structure line in the viewbox
            int i = 0;
            for(Jstruct_FullStructure lineageStructure : fullLineageStructures){

                boolean isThisOne = lineageStructure.getPdbIdentifier().equalsIgnoreCase(pdbIdentifier);

                //white/grey backgrounds for row
                svg.addRect(new Rectangle(GRID_X, GRID_Y + (LINE_SPACE * i), LINE_X-20, LINE_SPACE-5))
                        .setFill(isThisOne ? Color.WHITE : Color.decode("#f8f8fc"))
                        .setStroke(Color.decode("#d3e0ff"))
                        .setStrokeWidth(1);

                svg.addRect(new Rectangle(LINE_X, GRID_Y + (LINE_SPACE * i), FULL_WIDTH-LINE_X, LINE_SPACE-5))
                        .setFill(isThisOne ? Color.WHITE : Color.decode("#f8f8fc"))
                        .setStroke(Color.decode("#d3e0ff"))
                        .setStrokeWidth(1);

                //bar colors / title
                String depoBarColor = "#777777";
                String releaseBarColor = "#009900";
                String barTitleDeposition = "";
                String barTitleRelease = "";
                String barTitleObsolete = "";

                //get all dates for our calculations of bar x and y
                Date depositionDate = lineageStructure.getPdbDepositionDate();
                Date releaseDate = null;
                Jstruct_PdbRevdat revdat = pdbRevdatService.getInitialReleaseByStructVerId(lineageStructure.getStructVerId());
                if(revdat != null && revdat.getModdate() != null){
                    releaseDate = revdat.getModdate();
                }
                Date obsoleteDate = lineageStructure.getObsoleteDate();

                //calculate start and end of bars based on dates
                int depoBarStart = -1;
                int depoBarEnd = -1;
                int releaseBarStart = -1;
                int releaseBarEnd = FULL_WIDTH;

                if(depositionDate != null){
                    depoBarStart = dateToPointOnLine(depositionDate) + LINE_X;
                    barTitleDeposition = "\n  deposited: " + JDateUtil.defaultDateFormat(depositionDate);
                }

                if(obsoleteDate != null){
                    depoBarEnd = depositionDate!=null ? dateToPointOnLine(obsoleteDate) + LINE_X : -1;
                    releaseBarEnd = releaseDate!=null ? dateToPointOnLine(obsoleteDate) + LINE_X : -1;
                    barTitleObsolete = "\n  obsoleted: " + JDateUtil.defaultDateFormat(obsoleteDate);
                    releaseBarColor = "#990000";
                }

                if(releaseDate != null){
                    depoBarEnd = depositionDate!=null ? dateToPointOnLine(releaseDate) + LINE_X : -1;
                    releaseBarStart = dateToPointOnLine(releaseDate) + LINE_X;
                    barTitleRelease = "\n  initial release: " + JDateUtil.defaultDateFormat(releaseDate);
                }

                String barTitle = lineageStructure.getPdbIdentifier() + barTitleDeposition + barTitleRelease + barTitleObsolete;

                //label (pdb identifier) -- TODO, can we turn this into a link to the structure page?
                svg.addText(
                    lineageStructure.getPdbIdentifier(),
                    new Font("monospace", isThisOne ? Font.BOLD : Font.PLAIN, FONT_SIZE),
                    new Point(GRID_X + 10, GRID_Y + (LINE_SPACE * i) + FONT_SIZE));
      
                //grey deposition bar
                SvgRect depoRect = new SvgRect(new Rectangle(depoBarStart, GRID_Y + (LINE_SPACE * i) + 7, depoBarEnd - depoBarStart, LINE_SPACE - 17)).setFill(Color.decode(depoBarColor));
                XMLNode titleTag = new XMLTag("title").setContent(barTitle);
                depoRect.addSubtag(titleTag);
                svg.addSubtag(depoRect);

                //red/green bar
                SvgRect rect = new SvgRect(new Rectangle(releaseBarStart, GRID_Y + (LINE_SPACE * i) + 7, releaseBarEnd - releaseBarStart, LINE_SPACE - 17)).setFill(Color.decode(releaseBarColor));
                //XMLNode titleTag = new XMLTag("title").setContent(barTitle.toString());
                rect.addSubtag(titleTag);
                svg.addSubtag(rect);

                i++;
            }

            Div drawDiv = new Div().addStyle("margin-right:10px;");
            drawDiv.addSubtag(svg);

            div.addDiv(drawDiv);
            
            return div;
        
    }
    
    
    private static int dateToPointOnLine(Date inDate){
        if(inDate==null){
            return 0;
        }

        Calendar startCal = Calendar.getInstance(); startCal.setTime(START_DATE);
        Double startMillis = (double) startCal.getTimeInMillis();

        Calendar endCal = Calendar.getInstance(); endCal.setTime(new Date());
        Double endMillis = (double) endCal.getTimeInMillis();

        Calendar cal = Calendar.getInstance(); cal.setTime(inDate);
        Double pointMillis = (double) cal.getTimeInMillis();

        Double locationPercent = (pointMillis-startMillis)/(endMillis-startMillis);
        Double locationPoint = locationPercent * (FULL_WIDTH - LINE_X);

        return locationPoint.intValue();
    }
    
    
    
    
    
    
    
    
}
