package com.just.jstruct.utilities;

import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_User;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "facesUtil")
@SessionScoped
public final class FacesUtil {
    
    
    public static HttpServletResponse getResponse(){
        return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
    }
    
    
    public static HttpServletRequest getRequest(){
        if(FacesContext.getCurrentInstance() == null){
            return null;
        }
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }
    
    
    
    
     /**
     * based on a key, return the related value from the current URL (as String)
     *
     * @param urlKey
     * @return String version of the value
     */
    public static String getStringValueFromUrl(String urlKey) {
        String value = "";
        if (!"".equals(urlKey) && urlKey != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            value = context.getExternalContext().getRequestParameterMap().get(urlKey);

            //if nuttin on URL with this key, get similar one from session
            //but if it is on URL add it to session for later
            if (null == value || value.equals("")) {
                value = (String) retrieveFromSession("sneaky_" + urlKey);
            } else {
                storeOnSession("sneaky_" + urlKey, value);
            }

        }
        return value;
    }
    
    
    /**
     * return true if a key is on the URL 
     * @param urlKey
     * @return true if key is a parameter for the current URL
     */
    public static boolean urlContainsKey(String urlKey) {
        FacesContext context = FacesContext.getCurrentInstance();
        return context.getExternalContext().getRequestParameterMap().containsKey(urlKey);
    }

    /**
     * based on a key, return the related value from the current URL (as Long)
     *
     * @param urlKey
     * @return Long version of the value (or null if it cant be converted)
     */
    public static Long getLongValueFromUrl(String urlKey) {
        String stringValue = getStringValueFromUrl(urlKey);
        return JStringUtil.stringToLong(stringValue);
    }

    /**
     * store an object in the session (key/value pair)
     *
     * @param key
     * @param object
     */
    public static void storeOnSession(String key, Object object) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        Map sessionState = ctx.getExternalContext().getSessionMap();
        sessionState.put(key, object);
    }

    /**
     * retrieve an object from the session (based on its key)
     *
     * @param keyOrBeanName
     * @return Object from session
     */
    public static Object retrieveFromSession(String keyOrBeanName) {
        Object o = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(keyOrBeanName);
        return o;
    }

    /**
     * delete an object from the session (based on its key)
     *
     * @param key
     */
    public static void deleteFromSession(String key) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        Map sessionState = ctx.getExternalContext().getSessionMap();
        sessionState.remove(key);
    }

    /**
     * get the session bean outta the FacesContext
     *
     * @return
     */
    //public static SessionBean getSessionBean() {
    //    FacesContext context = FacesContext.getCurrentInstance();
    //    SessionBean sessionBean = context.getApplication().evaluateExpressionGet(context, "#{SessionBean}", SessionBean.class);
    //    return sessionBean;
    //}  
    
    
    
    /**
     * 
     * @param request
     * @return 
     * @throws com.just.jstruct.exception.JDAOException 
     */
    public static Jstruct_User determineCurrentUser(HttpServletRequest request) throws JDAOException {
        
        Jstruct_User user;
        
        if(request==null){
            request = FacesUtil.getRequest();
        }

        if(request!=null){
            user = (Jstruct_User) request.getSession().getAttribute(AuthUtil.SESSION_JSTRUCT_USER); 
        } 
        else 
        {
            //if there is no request, it's likely this call is happening somewhere in an API lifecycle
            JstructUserService jstructUserService = new JstructUserService();
            user = jstructUserService.getGuestUser(); 
        }
       
        return user;
        
    }
    
    
   

    /**
     * refresh the custom SessionBean currently stored in the users session
     */
    //public static void refreshSessionBean() {
    //    SessionBean sessionBean = getSessionBean();
    //    sessionBean.refreshBean();
    //}

    /**
     * return the value of a parameter from the jsp, based on the param name
     * i.e.: <h:commandLink value="click here" action="#{beanName.action}">
     * <f:param name="paramName1" value="paramValue1"/> <f:param
     * name="paramName2" value="paramValue2"/> </h:commandLink>
     *
     * @param paramName
     * @return
     */
    public static String getRequestParameter(String paramName) {
        return (String) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(paramName);
    }

    /**
     * Returns the value to which this map maps the specified key. Returns null
     * if the map contains no mapping for this key. (into the REQUEST map)
     *
     * @param key - key whose associated value is to be returned.
     * @return
     */
    public static Object getRequestMapValue(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(key);
    }

    /**
     * Associates the specified value with the specified key in this map. If the
     * map previously contained a mapping for this key, the old value is
     * replaced by the specified value. (from the REQUEST map)
     *
     * @param key - key with which the specified value is to be associated
     * @param value - value to be associated with the specified key.
     */
    public static void setRequestMapValue(String key, String value) {
        FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(key, value);
    }

    /**
     * Append a FacesMessage to the set of messages associated with the
     * specified client identifier
     * http://developers.sun.com/docs/jscreator/apis/jsf/javax/faces/context/FacesContext.html
     *
     * @param clientId - The client identifier with which this message is
     * associated (if null this will do the same as addGeneralErrorMessage(..))
     * @param summaryMsg - a user friendly version of the message
     * @param e - the exception that we want to get a bit more detail outta
     * (currently e.getLocalizedMessage())
     */
    public static void addClientErrorMessage(String clientId, String summaryMsg, Exception e) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summaryMsg, findErrorMsgInException(e));
        ctx.addMessage(clientId, message);
    }

    //utiltiy function to find error message string in an exception
    public static String findErrorMsgInException(Exception e) {
        String errMsg = "";
        if (null != e) {
            errMsg = e.getLocalizedMessage();
            if (null == errMsg || errMsg.equals("")) {
                errMsg = e.getMessage();
            }
            if (null == errMsg || errMsg.equals("")) {
                errMsg = e.toString();
            }
        }
        return errMsg;
    }

    /**
     * from the api:
     *
     * Returns a String containing the real path for a given virtual path. For
     * example, the path "/index.html" returns the absolute file path on the
     * server's filesystem would be served by a request for
     * "http://host/contextPath/index.html", where contextPath is the context
     * path of this ServletContext..
     *
     * The real path returned will be in a form appropriate to the computer and
     * operating system on which the servlet container is running, including the
     * proper path separators. This method returns null if the servlet container
     * cannot translate the virtual path to a real path for any reason (such as
     * when the content is being made available from a .war archive).
     *
     * @param target a String specifying a virtual path
     * @return a String specifying the real path, or null if the translation
     * cannot be performed
     */
    public static String getServletRealPath(String target) {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext external = context.getExternalContext();
        ServletContext servletContext = (ServletContext) external.getContext();
        String realPath = servletContext.getRealPath(target);
        //System.out.println("-- realPath: " + realPath);
        //System.out.println("-- contextPath: " + servletContext.getContextPath());
        return realPath;
    }

    //explicitly invalidate the user session and send them back to the home page
    public static String invalidateSessionAndGoHome() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return "/home.xhtml?faces-redirect=true";
    }

    //hijack whatever function that called this and just sent user to a new page
    public static void redirectRequest(String optionalNextPage) {
        
        try {
            
            if (null == optionalNextPage || "".equals(optionalNextPage)) {
                optionalNextPage = "/home.xhtml?faces-redirect=true";
            }
            FacesContext ctx = FacesContext.getCurrentInstance();
            ExternalContext extContext = ctx.getExternalContext();
            
            HttpServletRequest request = (HttpServletRequest) extContext.getRequest();
            String contextPath = request.getContextPath();

            extContext.redirect(contextPath + optionalNextPage);
            
        } catch (IOException ioe) {
            throw new FacesException(ioe);
        }
    }
    
    
    
    /**
     * parse out the query parameters from the urls parameter string
     * @param paramString
     * @return 
     * @throws java.io.UnsupportedEncodingException 
     */
    public static Map<String, List<String>> getQueryParams(String paramString) throws UnsupportedEncodingException{

        Map<String, List<String>> params = new HashMap<>();
        for (String param : paramString.split("&")) {
            String[] pair = param.split("=");
            String key = URLDecoder.decode(pair[0], "UTF-8");
            String value = "";
            if (pair.length > 1) {
                value = URLDecoder.decode(pair[1], "UTF-8");
            }

            List<String> values = params.get(key);
            if (values == null) {
                values = new ArrayList<>();
                params.put(key, values);
            }
            values.add(value);
        }

        return params;
       
    }

 
    
    
    
}
