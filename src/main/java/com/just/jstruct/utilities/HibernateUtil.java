package com.just.jstruct.utilities;

import com.hfg.security.CredentialsMgr;
import com.hfg.security.LoginCredentials;
import com.hfg.util.StringUtil;
import com.just.jstruct.pojos.SessionBean;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class HibernateUtil {
    
    
    private static final Logger LOGGER = Logger.getLogger(SessionBean.class.getPackage().getName());
    
    
    

    //name of our persistence unit in persistence.xml
    private static final String PERSISTENCE_UNIT_NAME = "JSTRUCT_PERSISTENCE_UNIT";


    // static variable for keeping around a single copy of entity mgr factory
    private static final HashMap<String, EntityManagerFactory> HASH_ENTITY_MANAGER_FACTORY = new HashMap();

    
    

    /**
     * Gets entity manager factory out of static hash map, or create factory if it doesn't exist
     * Note that EntityManagerFactory methods are thread-safe, but EntityManager is not.
     * @return
     */
    public static synchronized EntityManagerFactory getEntityManagerFactory() {
        
        EntityManagerFactory emf = HASH_ENTITY_MANAGER_FACTORY.get(PERSISTENCE_UNIT_NAME);
        
        if (emf == null) {

            emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, additionalPersistenceProperties());
            HASH_ENTITY_MANAGER_FACTORY.put(PERSISTENCE_UNIT_NAME, emf);
            EntityManager em = emf.createEntityManager();

        }
        return emf;
    }


    /**
     * gets da entity manager
     * @return
     */
    public static synchronized EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }

    
    
    
    
    
    /**
     * retrieve the username/password from the .credMgr file (path and key are located in ApplicationResources.properties)
     * @return 
     */
    private static Properties additionalPersistenceProperties() {

        Properties props = new Properties();
        String credMgrFileName;
        String credMgrKey;
            
        try {
            credMgrFileName = getCredMgrFileName();
            credMgrKey = getCredMgrKey();
        } catch (IOException ioe) 
        {
            ioe.printStackTrace();
            LOGGER.log(Level.SEVERE, "Error getting .credMgr file info! Message:{0}", ioe.getMessage());
            throw new RuntimeException(ioe);
        }

        CredentialsMgr credentialsMgr = StringUtil.isSet(credMgrFileName) ? new CredentialsMgr(new File(credMgrFileName.trim())) : new CredentialsMgr();
        LoginCredentials credentials = credentialsMgr.get(credMgrKey);
        if (null == credentials)
        {
            throw new SecurityException("No credentials found for key " + StringUtil.singleQuote(credMgrKey)
                  + " in " + StringUtil.singleQuote(credentialsMgr.getFile().getPath()) + "!");
        }
        
        props.setProperty("hibernate.hikari.dataSource.user", credentials.getUser());
        props.setProperty("hibernate.hikari.dataSource.password", credentials.getPasswordString());
        return props;
    }
    
    
    
    
    
    /**
     * get CredMgrFile
     */
    private static String getCredMgrFileName() throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = HibernateUtil.class.getClassLoader().getResourceAsStream("/ApplicationResources.properties");
        properties.load(inputStream);
        String property = properties.getProperty("CredMgrFile");
        return property;
    }
    /**
     * get CredMgrKey
     */
    private static String getCredMgrKey() throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = HibernateUtil.class.getClassLoader().getResourceAsStream("/ApplicationResources.properties");
        properties.load(inputStream);
        String property = properties.getProperty("CredMgrKey");
        return property;
    }
    

}