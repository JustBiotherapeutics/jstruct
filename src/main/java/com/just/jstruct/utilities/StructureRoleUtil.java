package com.just.jstruct.utilities;

import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.StructureRoleService;
import com.just.jstruct.enums.UserStatus;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import java.util.List;
import java.util.Objects;


/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class StructureRoleUtil {

    
    
    //lists of manually uploaded structVerIds that current user has been granted a specific role 
    private static List<Long> readStructureIds;
    private static List<Long> writeStructureIds;
    private static List<Long> grantStructureIds;
    
    private Jstruct_User currentUser;
    
    
   
    //##################################################################################################################
    // constructor
    //##################################################################################################################

    public StructureRoleUtil(Jstruct_User currentUser){
        this.currentUser = currentUser;
        init();   
    }
    
    
    private void init(){
        try {
            
            if(currentUser==null){
                JstructUserService jstructUserService = new JstructUserService();
                currentUser = jstructUserService.getGuestUser();
            }
           
            StructureRoleService structureRoleService = new StructureRoleService();
            readStructureIds = structureRoleService.getReadStructureIdsForUser(currentUser.getUserId());
            writeStructureIds = structureRoleService.getWriteStructureIdsForUser(currentUser.getUserId());
            grantStructureIds = structureRoleService.getGrantStructureIdsForUser(currentUser.getUserId());
            
        } catch (JDAOException e){
            throw new RuntimeException("Exception occurred creating 'StructureRoleUtil' for " + currentUser.getName(), e);
        }
    }
    
    
    
   /*
    public final Jstruct_User determineCurrentUser() throws JDAOException {
        
        Jstruct_User user;
        HttpServletRequest request = FacesUtil.getRequest();
        
        //if there is no request, it's likely this call is happening somewhere in an API lifecycle
        if(request!=null){
            user = (Jstruct_User) request.getSession().getAttribute(AuthUtil.SESSION_JSTRUCT_USER); 
        } else {
            JstructUserService jstructUserService = new JstructUserService();
            user = jstructUserService.getGuestUser(); 
        }
        
        //user is still null?!
        if(user==null) {
            user = Jstruct_User.fakeGuestUser();
        }

        return user;
        
    }*/


    
    //##################################################################################################################
    // public methods
    //##################################################################################################################

    
    
    /**
     * 
     * @param fullStructure 
     * @return  
     */
    public Jstruct_FullStructure setupFullStructureRoles(Jstruct_FullStructure fullStructure) {
        fullStructure.setCurrentUserCanRead(canRead(fullStructure));
        fullStructure.setCurrentUserCanWrite(canWrite(fullStructure));
        fullStructure.setCurrentUserCanGrant(canGrant(fullStructure));
        return fullStructure;
    }
    
    
    
    
    /**
     * 
     * @param fullStructure
     * @return 
     */
    public boolean canRead(Jstruct_FullStructure fullStructure){
        
        if(fullStructure.isFromRcsb() || fullStructure.isCanAllRead() || Objects.equals(fullStructure.getOwnerId(), currentUser.getUserId())){
            return true;
        }
        if(readStructureIds.contains(fullStructure.getStructureId())){
            return true;
        }
        return false;

    }
    
    
    /**
     * 
     * @param fullStructure
     * @return 
     */
    public boolean canWrite(Jstruct_FullStructure fullStructure){
        
        if(fullStructure.isFromRcsb()){
            return false;
        }
        
        if(currentUser.getIsRoleContributor() && currentUser.getStatus().equals(UserStatus.ACTIVE)){
            
            if(fullStructure.isCanAllWrite() || Objects.equals(fullStructure.getOwnerId(), currentUser.getUserId())){
                return true;
            }
            if(writeStructureIds.contains(fullStructure.getStructureId())){
                return true;
            }
        }
        
        return false;

    }
    
    
    /**
     * 
     * @param fullStructure
     * @return 
     */
    public boolean canGrant(Jstruct_FullStructure fullStructure){
        
        if(fullStructure.isFromRcsb()){
            return false;
        }
        
        if(currentUser.getIsRoleContributor() && currentUser.getStatus().equals(UserStatus.ACTIVE)){
            
            if(Objects.equals(fullStructure.getOwnerId(), currentUser.getUserId())){
                return true;
            }
            if(grantStructureIds.contains(fullStructure.getStructureId())){
                return true;
            }
        }
        
        return false;

    }
    

    
    
    //##################################################################################################################
    // private methods
    //##################################################################################################################


    


    
    


   

}
