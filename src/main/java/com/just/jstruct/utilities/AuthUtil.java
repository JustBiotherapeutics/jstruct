package com.just.jstruct.utilities;


import com.hfg.ldap.LDAPClient;
import com.hfg.ldap.LDAP_Config;
import com.hfg.ldap.LDAP_Exception;
import com.hfg.ldap.LDAP_User;
import com.hfg.ldap.ad.ActiveDirectoryUser;
import com.hfg.ldap.ad.ActiveDirectoryUserFactory;
import com.hfg.security.LoginCredentials;
import com.hfg.util.CryptoUtil;
import com.hfg.util.StringUtil;
import com.hfg.util.UserImpl;
import com.hfg.webapp.filter.auth.AuthenticationFilter;
import com.hfg.xml.XMLTag;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.AuthState;
import com.just.jstruct.pojos.SessionBean;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLDecoder;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.AuthenticationException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;




/**
 * Authentication Utilities
 * 
 * some resources:
 *   LDAP_INVALID_CREDENTIALS - http://ldapwiki.willeke.com/wiki/LDAP_INVALID_CREDENTIALS
 *   Common Active Directory Bind Errors - http://ldapwiki.willeke.com/wiki/Common%20Active%20Directory%20Bind%20Errors
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class AuthUtil implements Serializable {
    
    
    // session keys
    public static final String SESSION_JSTRUCT_USER = "JStructUser";
    public static final String SESSION_USER_NAME = "userName";
    public static final String BROWSER_SESSION_AUTH_COOKIE_NAME = "com.hfg.auth.session";

    private final String mLDAP_CorpDomain;
    private final String mLDAP_ServerURL;
    private final String mLDAP_UserContext;
    private final String mLDAP_PrincipalFieldName;
    private final String mLDAP_PrincipalSuffix;

    private final static File EC_KEY_DIR;
    private final static KeyPair EC_KEY_PAIR;

    private static final Logger LOGGER = Logger.getLogger(AuthUtil.class.getPackage().getName());
    
    static
    {
        EC_KEY_DIR = new File(System.getProperty("user.home"));
        EC_KEY_PAIR = getEcKeyPair(EC_KEY_DIR);
    }
    
    
    
    
    public AuthUtil()
    {
        ValueMapUtils vmu = ValueMapUtils.getInstance();

        this.mLDAP_CorpDomain = vmu.getLDAP_CorpDomain();
        this.mLDAP_ServerURL = vmu.getLDAP_ServerURL();
        this.mLDAP_UserContext = vmu.getLDAP_UserContext();
        this.mLDAP_PrincipalFieldName = vmu.getLDAP_PrincipalFieldName();
        this.mLDAP_PrincipalSuffix = vmu.getLDAP_PrincipalSuffix();
    }
    

    /**
     * 
     * @param username
     * @param password
     * @param inRequest
     * @param inResponse
     * @return 
     */
    public AuthState authenticateUser(String username, String password, HttpServletRequest inRequest, HttpServletResponse inResponse) {

        AuthState authState = new AuthState();
        Jstruct_User jstructUser = null;
       

        try {
                
            LoginCredentials credentials = new LoginCredentials(username, password.toCharArray());
            //LDAP_User ldapUser = getVerifiedUser(credentials);
            LDAP_User ldapUser = getEvotecVerifiedUser(credentials);

            if (ldapUser != null)
            {
                LOGGER.log(Level.INFO, "{0} successfully authenticated via LDAP", ldapUser.getName());

                ldapUser.setClientHost(inRequest.getRemoteHost());

    //            if (StringUtil.isSet(mDomainRestriction) && ! mDomainRestriction.equals(ldapUser.getDomain())) {
    //                authState.setAuthenticated(false);
    //                authState.setAuthFailMessage("Invalid Domain Login");
    //                authState.setAuthFailException(new InvalidDomainException());
    //            }

                jstructUser = ldapUserToExistingJstructUser(ldapUser);
                if(null==jstructUser){
                    JstructUserService jstructUserService = new JstructUserService();
                    jstructUser = jstructUserService.addUserWithRoles(ldapUser, grantNewUserContributorRole(), grantNewUserAdminRole(), false);
                }

                authState.setAuthenticated(true);
                authState.setJstructUser(jstructUser);

                inRequest.getSession().setAttribute(SESSION_JSTRUCT_USER, jstructUser);

                if (EC_KEY_PAIR != null) {
                    inResponse.addCookie(generateBrowserSessionCookie(ldapUser));
                }

            }
            else
            {
                authState.setAuthenticated(false);
                authState.setAuthFailMessage("Unable to login; username and/or password may be invalid.");

            }
        }
        catch (AuthenticationException | LDAP_Exception ex)
        {
            authState.setAuthenticated(false);
            authState.setAuthFailException(ex);
            authState.setAuthFailMessage(determineAuthErrorMsg(ex));
        }
        catch (Exception e)
        {
            authState.setAuthenticated(false);
            authState.setAuthFailException(e);
            authState.setAuthFailMessage(e.getMessage());
        }

        //if the authentication failed, remove any previous session attribute and cookie
        if(!authState.isAuthenticated()){
            logout(inRequest, inResponse);
        }
        
        SessionBean.addUserNameToTomcat(inRequest, jstructUser);

        return authState;
    }
    
    
    /**
     * 
     * @param inServletRequest
     * @param inServletResponse 
     */
    public static void logout(HttpServletRequest inServletRequest, HttpServletResponse inServletResponse) {
        
        //remove user from session
        inServletRequest.getSession().removeAttribute(SESSION_JSTRUCT_USER);

        // Expire the cookie. (Won't hurt if it's not in use)
        Cookie cookie = new Cookie(AuthenticationFilter.BROWSER_SESSION_AUTH_COOKIE_NAME, "");
        cookie.setPath("/");
        cookie.setMaxAge(0); // Expire now
        inServletResponse.addCookie(cookie);
              
    }
    
    
    
    
    

    
    
    
    //##################################################################################################################
    // private helper methods
    //##################################################################################################################
    
    /*
    private LDAP_User getVerifiedUser(LoginCredentials inCredentials) {
        LDAPClient client = new LDAPClient()
                .setServerURL(mLDAP_ServerURL)
                .setUserContext(mLDAP_UserContext)
                .setPrincipalCredentials(inCredentials);

        //if (mLDAP_PrincipalFieldName != null) { 
        client.setPrincipalFieldName(mLDAP_PrincipalFieldName);
        //}
        client.setPrincipalSuffix(mLDAP_PrincipalSuffix);

        return client.getInfoForUser("(&(objectCategory=Person)(objectClass=user)(" + mLDAP_PrincipalFieldName + "=" + inCredentials.getUser() + "))");
    }*/


    public LDAP_User getEvotecVerifiedUser(LoginCredentials inCredentials)
    {
        //set the evotec domain into the credentials
        inCredentials.setDomain(mLDAP_CorpDomain);

        LDAP_Config config = new LDAP_Config()
                  .setServerURL(mLDAP_ServerURL)
                  .setPrincipalCredentials(inCredentials)
                  .setUserContext(mLDAP_UserContext);

        LDAPClient<ActiveDirectoryUser> ldapClient = new LDAPClient<>(config);
        ldapClient.setPrincipalSuffix(mLDAP_PrincipalSuffix);
        ldapClient.setUserFactory(new ActiveDirectoryUserFactory());

        LDAP_User infoForUser = ldapClient.getInfoForUser("(&(objectCategory=Person)"
                                                        + "(" + mLDAP_PrincipalFieldName + "=" + inCredentials.getUser() + "))");

        return infoForUser;
    }
    
    
    
    private boolean grantNewUserContributorRole(){
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        String defaultRole = vmu.getDefaultRole();
        if(StringUtils.containsIgnoreCase(defaultRole, "Contributor")){
            return true;
        }
        return false;
    }
    
    private boolean grantNewUserAdminRole(){
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        String defaultRole = vmu.getDefaultRole();
        if(StringUtils.containsIgnoreCase(defaultRole, "Admin")){
            return true;
        }
        return false;
    }
    
    
    
    private String determineAuthErrorMsg(Exception ex) {
        //http://www-01.ibm.com/support/docview.wss?uid=swg21290631
        //https://confluence.atlassian.com/bitbucketserverkb/ldap-error-code-49-779171410.html
        String errorMsg = ex.getMessage();
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put("data 525", "The specified account does not exist");
        //errorMap.put("data 52e", "Invalid password");                         //for some reason 52e is what is mostly returned, even if not a valid name. we'll override it with line below
        errorMap.put("data 52e", "Invalid credentials");
        errorMap.put("data 530", "Account login time restricted");
        errorMap.put("data 531", "Not permitted to login from this workstation");  //eh?
        errorMap.put("data 532", "The password has expired");
        errorMap.put("data 533", "This account is currently disabled");
        errorMap.put("data 701", "This account has expired");
        errorMap.put("data 773", "Password must change before logging in");
        errorMap.put("data 775", "This account is currently locked");
        for(Map.Entry<String, String> entry : errorMap.entrySet()){
            if(errorMsg.contains(entry.getKey())){
                return entry.getValue();
            }
        }
        return "Invalid credentials";  //"Username or password is invalid"
    }
    
    
    private static Jstruct_User ldapUserToExistingJstructUser(LDAP_User ldapUser) {

        Jstruct_User jstructUser = null;

        try {
            
            JstructUserService jstructUserService = new JstructUserService();
            jstructUser = jstructUserService.getByUid(ldapUser.getUID());
            
        } catch (JDAOException jde) {
            return null;
        } 

        return jstructUser;
    }
    
    
   
    

    //--------------------------------------------------------------------------
    private Cookie generateBrowserSessionCookie(UserImpl inUser) throws Exception 
    {
        Cookie cookie = null;

        if (EC_KEY_PAIR != null)
        {
            String data = inUser.toXMLTag().toXML();
            byte[] signature = CryptoUtil.generateSignatureWithECDSA(EC_KEY_PAIR.getPrivate(), data);

            cookie = new Cookie(BROWSER_SESSION_AUTH_COOKIE_NAME, Base64.getEncoder().encodeToString((data + "Sig:" + Base64.getEncoder().encodeToString(signature)).getBytes()));
            cookie.setPath("/");
            cookie.setMaxAge(-1); // Expire at the end of the browser session
        }

        return cookie;
    }


   
    //---------------------------------------------------------------------------
    public static Cookie getBrowserSessionCookie(HttpServletRequest inRequest)
    {
        Cookie sessionCookie = null;
        if (inRequest.getCookies() != null) {
            for (Cookie cookie : inRequest.getCookies()) {
                if (cookie.getName().equals(BROWSER_SESSION_AUTH_COOKIE_NAME)) {
                    sessionCookie = cookie;
                    break;
                }
            }
        }
        return sessionCookie;
    }



    //---------------------------------------------------------------------------
    public static boolean browserSessionCookieIsValid(Cookie inSessionCookie) throws Exception
    {
        boolean result = false;

        if (inSessionCookie != null && EC_KEY_PAIR != null)
        {
            String cookieValue = getCookieValue(inSessionCookie);
            
            int sigIndex = cookieValue.indexOf("Sig:");
            
            if(sigIndex > 0)
            {            
                String cookieData = cookieValue.substring(0, sigIndex);
                byte[] signature = Base64.getDecoder().decode(cookieValue.substring(sigIndex + 4));

                result = CryptoUtil.verifySignatureWithECDSA(EC_KEY_PAIR.getPublic(), cookieData, signature);
                
                if(!result)
                {
                    System.out.println("cookie signature verification failed; cookieData: " + cookieData);
                }
            }
        }

        return result;
    }



    //---------------------------------------------------------------------------
    public static UserImpl createUserObjFromBrowserSessionCookie(Cookie inSessionCookie) throws IOException 
    {
        String cookieValue = getCookieValue(inSessionCookie);
        
        int sigIndex = cookieValue.indexOf("Sig:");
        String cookieData = cookieValue.substring(0, sigIndex);

        XMLTag xmlTag = new XMLTag(new ByteArrayInputStream(cookieData.getBytes()));
        return new UserImpl(xmlTag);
    }



    private static String getCookieValue(Cookie inSessionCookie)
    {
        String cookieValue = null;

        if (inSessionCookie != null)
        {
            try
            {
                String urlDecodedCookieValue = inSessionCookie.getValue();
                if(inSessionCookie.getValue().contains("%"))
                {
                    //percent sign indicates that the current value has been encoded.
                    urlDecodedCookieValue = URLDecoder.decode(inSessionCookie.getValue(), "UTF-8");
                }
                
                cookieValue = new String(Base64.getDecoder().decode(urlDecodedCookieValue));
            } 
            catch (Exception e)
            {
                //backwards compatibilty with the un-base64 version of the cookie. just in case. //todo remove eventually.
                cookieValue = inSessionCookie.getValue();
            }
        }
        return cookieValue;
    }





    //---------------------------------------------------------------------------
    public static KeyPair getEcKeyPair(File inKeyDir)
    {
        KeyPair keyPair = null;

        boolean createNewKeys = false;
        File keyDir = inKeyDir;
        if (! keyDir.exists())
        {
            LOGGER.warning("The specified " + EC_KEY_DIR + ", " + StringUtil.singleQuote(inKeyDir.getPath()) + " does not exist!");
            createNewKeys = true;
        }
        else
        {
            File publicKeyFile = new File(keyDir, "ECPublic.key");
            if (! publicKeyFile.exists())
            {
                LOGGER.warning("The public key file " + StringUtil.singleQuote(publicKeyFile.getPath()) + " does not exist!");
                createNewKeys = true;
            }
            else if (! publicKeyFile.canRead())
            {
                LOGGER.warning("The public key file " + StringUtil.singleQuote(publicKeyFile.getPath()) + " is not readable by the user executing the app server!");
                createNewKeys = true;
            }

            File privateKeyFile = new File(keyDir, "ECPrivate.key");
            if (! privateKeyFile.exists())
            {
                LOGGER.warning("The private key file " + StringUtil.singleQuote(privateKeyFile.getPath()) + " does not exist!");
                createNewKeys = true;
            }
            else if (! publicKeyFile.canRead())
            {
                LOGGER.warning("The private key file " + StringUtil.singleQuote(privateKeyFile.getPath()) + " is not readable by the user executing the app server!");
                createNewKeys = true;
            }

            try
            {
                PublicKey publicKey = CryptoUtil.readPublicEllipticCurveKeyFile(publicKeyFile);
                PrivateKey privateKey = CryptoUtil.readPrivateEllipticCurveKeyFile(privateKeyFile);

                keyPair = new KeyPair(publicKey, privateKey);
            }
            catch (Exception e)
            {
                LOGGER.warning("Problem reading the EC key files: " + e.getMessage());
                createNewKeys = true;
            }
        }

        if (createNewKeys)
        {
            if (! keyDir.exists())
            {
                keyDir = new File(System.getProperty("user.dir"));
            }

            try
            {
                keyPair = CryptoUtil.generateEllipticCurveKeyPair();
                CryptoUtil.writePublicEllipticCurveKeyToFile(keyPair.getPublic(), new File(keyDir, "ECPublic.key"));
                CryptoUtil.writePrivateEllipticCurveKeyToFile(keyPair.getPrivate(), new File(keyDir, "ECPrivate.key"));
                LOGGER.warning("New EC keys created in " + StringUtil.singleQuote(keyDir.getPath()) + ".");
            }
            catch (Exception e)
            {
                LOGGER.warning("Could not create EC keys in directory " + StringUtil.singleQuote(keyDir.getPath()) + "!");
                keyPair = null;
            }
        }


        return keyPair;
    }




    
    
    
    
}
