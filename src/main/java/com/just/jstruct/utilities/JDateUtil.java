package com.just.jstruct.utilities;

import com.hfg.util.StringUtil;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.time.DateUtils;



/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JDateUtil {
    
    
    public static String defaultFormat = "yyyy-MM-dd";


    public static Date createDate(int year, int month, int day){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }


    public static Long dateAsTime (Date d) {
        if(null==d){
            return null;
        }
        return d.getTime();
    }

    /**
     * parse a very specific format (the one found in PDB files)
     * @param inDate the date as a string to parse
     * @return parsed date as date
     */
    public static Date parse_dd_MMM_yy_Date (String inDate) {

        //http://stackoverflow.com/questions/29490893/parsing-string-to-local-date-doesnt-use-desired-century

        if (!StringUtil.isSet(inDate)) {
            return null;
        }

        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .appendPattern("dd-MMM-")
                .appendValueReduced(ChronoField.YEAR, 2, 2, Year.now().getValue() - 90)
                .toFormatter(Locale.US);
        LocalDate obsDate = LocalDate.parse(inDate, formatter);
        Instant instant = obsDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);

        return date;

    }

    /**
     * throughout the app we have a default display date format to use (date only, no time)
     * format: yyyy-MM-dd
     * @param inDate
     * @return
     */
    public static String defaultDateFormat (Date inDate) {
        if(null==inDate){
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(defaultFormat);
        return sdf.format(inDate);
    }

    /**
     * check a string to see if it is a valid date in our default date format: yyyy-MM-dd
     * @param stringToCheck
     * @return
     */
    public static boolean isValidDefaultDateFormat(String stringToCheck){
        if(!StringUtil.isSet(stringToCheck)){
            return false;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(defaultFormat);
        try{
            sdf.parse(stringToCheck);
            return true;
        } catch (ParseException pe) {
            return false;
        }
    }





    /**
     * convenience method for formatting a date
     * @param inDate
     * @param inFormat
     * @return
     */
    public static String formatDate (Date inDate, String inFormat) {
        if(null==inDate || !StringUtil.isSet(inFormat)){
            return null;
        }
        else
        {
            SimpleDateFormat sdf = new SimpleDateFormat(inFormat);
            return sdf.format(inDate);
        }
    }



    /**
     * throughout the app we have a default display date and time format to use
     * format: yyy-MM-dd HH:mm:ss
     * @param inDate
     *
     * @return
     */
    public static String defaultDateAndTimeFormat (Date inDate) {
        return defaultDateAndTimeFormat(inDate, true);
    }

    /**
     * throughout the app we have a default display date and time format to use
     * format: yyy-MM-dd
     * @param inDate
     * @param showSeconds
     * @return
     */
    public static String defaultDateAndTimeFormat (Date inDate, boolean showSeconds) {
        if(null==inDate){
            return null;
        }
        if(showSeconds){
            SimpleDateFormat sdf = new SimpleDateFormat(defaultFormat + " HH:mm:ss");
            return sdf.format(inDate);
        }
        SimpleDateFormat sdf = new SimpleDateFormat(defaultFormat + " HH:mm");
        return sdf.format(inDate);

    }


    /**
     * compare 2 dates and return a pretty formatted string stating the difference in days, hours, minutes, seconds.
     * @param d1
     * @param d2
     * @return
     */
    public static String differenceBetweenDatesToElapsedTime(Date d1, Date d2){
        if(null==d1 || null==d2){
            return " ";
        }
        long millis = differenceBetweenDatesInMillis(d1, d2);
        return convertMillisecondsToElapsedTime(millis);

    }



    /**
     * return the difference between dates in milliseconds (absolute value)
     * @param d1
     * @param d2
     * @return
     */
    public static long differenceBetweenDatesInMillis(Date d1, Date d2){
        long duration = d1.getTime() - d2.getTime();
        duration = Math.abs(duration);
        return duration;
    }


    /**
     * return a pretty formatted string stating amount of time in days, hours, minutes, seconds that milliseconds converts to
     * @param millis
     * @return
     */
    public static String convertMillisecondsToElapsedTime(long millis){

        if(millis < 0)
        {
            return "Less Than Zero";
            //throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        StringBuilder sb = new StringBuilder(64);
        if(days > 0) {
            sb.append(days).append(" Day");
            sb.append(days==1 ? ", " : "s, ");
        }

        if(hours > 0) {
            sb.append(hours).append(" Hour");
            sb.append(hours==1 ? ", " : "s, ");
        }
        if(minutes > 0) {
            sb.append(minutes).append(" Minute");
            sb.append(minutes==1 ? ", " : "s, ");
        }

        sb.append(seconds).append(" Second");
        sb.append(seconds==1 ? "" : "s");


        return(sb.toString());
    }



    public static Date filetimeToDate(FileTime ft){
        if(null==ft){
            return null;
        }
        return new Date(ft.toMillis());
    }


    public static boolean isSameDay(Date d1, Date d2){
        if(null==d1 || null==d2){
            return false;
        }
        return DateUtils.isSameDay(d1, d2);
    }
    
    
    
    /**
     * 
     * @param numberOfDays can be positive or negative
     * @return 
     */
    public static Date addDaysToNow(int numberOfDays) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, numberOfDays);
        return calendar.getTime();

    }





    /**
     * Remove the time portion of a date.
     * <p>
     * example:
     *   input: 2016-12-11 09:19:01  returns 2016-12-11 00:00:00
     * @param d  the Date to strip out the time
     * @return   the Date with the time removed
     */
    public static Date removeTimeFromDate(Date d)
    {
        Calendar cal = Calendar.getInstance(); // locale-specific
        cal.setTime(d);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Date(cal.getTimeInMillis());
    }
    
    
}
