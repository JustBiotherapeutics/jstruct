package com.just.jstruct.utilities;

import com.hfg.util.FileUtil;
import com.hfg.util.StringUtil;
import com.just.jstruct.exception.JException;
import java.io.*;
import java.nio.file.Files;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;


/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JFileUtils {


    /**
     * determine if a string resolves to an existing directory
     * @param path
     * @return
     */
    public static boolean directoryExists(String path){
        if(!StringUtil.isSet(path)){
            return false;
        }
        File f = new File(path);
        
        return f.exists() && f.isDirectory();
    }


    /**
     * determine if a string resolves to an existing file
     * @param path
     * @return
     */
    public static boolean fileExists(String path){
        if(!StringUtil.isSet(path)){
            return false;
        }
        File f = new File(path);
        
        return f.exists() && !f.isDirectory();
    }
    
    /**
     * create a directory if it doesn't yet exist
     * @param path 
     * @throws java.io.IOException 
     */
    public static void createDirectoryIfNotAlready(String path) throws IOException {
        FileUtils.forceMkdir(new File(path));
    }



    /**
     * read in a file, return a byte array
     * @param inFile File to read
     * @return byte array of file contents
     * @throws com.just.jstruct.exception.JException
     */
    public static byte[] readFile (File inFile) throws JException {

        byte[] bytes;

        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(30000 * 1024);

            FileInputStream fis = new FileInputStream(inFile);
            BufferedInputStream bis = new BufferedInputStream(fis);

            byte[] transferBuffer = new byte[32 * 1024];
            int readSize;
            while ((readSize = bis.read(transferBuffer)) != -1)
            {
                baos.write(transferBuffer, 0, readSize);
            }
            fis.close();
            bis.close();
            baos.close();

            bytes = baos.toByteArray();

        }
        catch (IOException e)
        {
            throw new JException("Error reading [unzipped] file into [unzipped] byte array!", e);
        }

        return bytes;
    }



    /**
     * read in a GZipped file, return uncompressed byte array
     * @param inFile GZipped File to read
     * @return byte array of file contents (un-GZipped)
     * @throws com.just.jstruct.exception.JException
     */
    public static byte[] readGZippedFile(File inFile) throws JException {

        byte[] bytes;

        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(30000 * 1024);

            FileInputStream fis = new FileInputStream(inFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            GZIPInputStream gis = new GZIPInputStream(fis);

            byte[] transferBuffer = new byte[32 * 1024];
            int readSize;
            while ((readSize = gis.read(transferBuffer)) != -1)
            {
                baos.write(transferBuffer, 0, readSize);
            }
            bytes = baos.toByteArray();

            fis.close();
            bis.close();
            gis.close();
            baos.close();

        }
        catch (IOException e)
        {
            throw new JException("Error reading [zipped] file into [unzipped] byte array!", e);
        }

        return bytes;
    }


    /**
     * return the unzipped file bytes regardless of if the file itself is gzipped or not
     * @param file
     * @return
     * @throws IOException
     * @throws JException
     */
    public static byte[] getUnzippedBytes(File file) throws IOException, JException {

        boolean isFileGzipped = isFileGZipped(file);

        byte[] unzippedBytes;
        if(isFileGzipped){
            unzippedBytes = readGZippedFile(file);
        } else {
            unzippedBytes = readFile(file);
        }
        return unzippedBytes;
    }




    /**
     * use GZIP to compress a byte array
     * @param uncompressedBytes byte array of decompressed bytes
     * @return compressed byte array
     */
    public static byte[] compress(byte[] uncompressedBytes ){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try{
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(uncompressedBytes);
            gzipOutputStream.close();
        } catch(IOException e){
            throw new RuntimeException(e);
        }
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * use GZIP to decompress a byte array
     * @param compressedBytes byte array of compressed bytes
     * @return decompressed byte array
     */
    public static byte[] decompress(byte[] compressedBytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try{
            IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(compressedBytes)), out);
        } catch(IOException e){
            throw new RuntimeException(e);
        }
        return out.toByteArray();
    }


    /**
     * determine the file type
     * http://docs.oracle.com/javase/7/docs/api/java/nio/file/spi/FileTypeDetector.html
     * @param inFile File to evaluate
     * @return file type as string
     * @throws java.io.IOException
     */
    public static String probeFileType(File inFile) throws IOException
    {
        String fileType = Files.probeContentType(inFile.toPath());
        return fileType;
    }


    /**
     * determine if a file is GZipped
     * @param inFile file to evaluate
     * @return true if the file appears to be GZipped
     * @throws java.io.IOException
     */
    public static boolean isFileGZipped(File inFile) throws IOException
    {
        String fileType = probeFileType(inFile);
        if(StringUtil.isSet(fileType))
        {
            return fileType.equalsIgnoreCase("application/gzip");
        }
        return false;
    }


    /**
     * determine if a file is a PDB or CIF file
     * @param inFile file to evaluate
     * @return true if file is [probably] a PDB or CIF file
     * @throws com.just.jstruct.exception.JException
     */
    public static boolean isPdbOrCifFile(File inFile) throws JException
    {

        if(null == inFile)
        {
            return false;
        }

        try
        {
            String fileName = getFileName(inFile);

            if(isFileGZipped(inFile))
            {
                GzipCompressorInputStream gcis = new GzipCompressorInputStream(new FileInputStream(inFile));
                String insideFileName = gcis.getMetaData().getFilename();
                if(StringUtil.isSet(insideFileName))
                {
                    fileName = insideFileName;
                }
            }
 
            return isPdbOrCifFile(fileName);
            
        } catch (IOException | JException e)
        {
            throw new JException ("Error attempting to get filename from (possibly Gzipped) file in order to determine if file is in PDB/CIF format");
        }
    }
    
    public static boolean isPdbOrCifFile(String fileName) throws JException
    {
        if (StringUtil.isSet(fileName) && 
                  (fileName.toUpperCase().endsWith(".PDB")
                || fileName.toUpperCase().endsWith(".PDB.GZ")
                || fileName.toUpperCase().endsWith(".ENT")
                || fileName.toUpperCase().endsWith(".ENT.GZ")
                || fileName.toUpperCase().endsWith(".CIF")
                || fileName.toUpperCase().endsWith(".CIF.GZ")))
        {
            return true;
        }
        return false;
    }
    
    
    


    /**
     * a gzipped file MIGHT have a different name after it is uncompressed
     * (  not just removing the .gz, but an actual different name
     *    example: the file '2RS3.pdb.gz' ungzips to 'pdb2rs3.ent'  )
     * this will return the ungzipped name, if the file is gzipped
     * or just the 'normal' name if it is not gzipped
     * @param inFile
     * @return
     * @throws com.just.jstruct.exception.JException
     */
    public static String getUnGzippedFileName(File inFile) throws JException {

        String fileName = "";

        try {
            if(isFileGZipped(inFile)){
                GzipCompressorInputStream gcis = new GzipCompressorInputStream(new FileInputStream(inFile));
                fileName = gcis.getMetaData().getFilename();
                if(null==fileName){
                    //actual filename not stored in metadata - just remove the gz
                    fileName = FileUtil.getNameMinusExtension(inFile);
                }
            }else{
                fileName = getFileName(inFile);
            }
        } catch (IOException ioe){
            throw new JException("Error attempting to get filename from (possibly Gzipped) file", ioe);
        }


        return fileName;
    };

    /**
     * retrieve the filename from a file
     * @param inFile file to evaluate
     * @return filename
     */
    public static String getFileName(File inFile){
        return inFile.getName();
    }

    /**
     * retrieve the filename from a URL (everything after final / separator)
     * @param inUrl URL to evaluate
     * @return filename
     */
    public static String getFileNameFromUrl(String inUrl){
        return getFileName(inUrl);
    }

    /**
     * retrieve the filename from a string path (everything after last / separator)
     * @param fullPathWithFileName
     * @return
     */
    public static String getFileName(String fullPathWithFileName){
        if(!StringUtil.isSet(fullPathWithFileName)){
            return null;
        }
        String[] tokens = fullPathWithFileName.split("[\\\\|/]");

        String maybeHasParams = tokens[tokens.length-1];
        if(maybeHasParams.contains("?")){
            return maybeHasParams.substring(0, maybeHasParams.indexOf("?"));
        }
        return maybeHasParams;
    }



    /**
     * retrieve the file extension, in the case of a gzipped file (file with .gz as extension) return extension plus the extension prior to the .gz
     * @param inFile file to evaluate
     * @return file extension
     */
    public static String getExtension(File inFile){
        String fileName = inFile.getName();
        if(fileName.toUpperCase().endsWith(".GZ")){
            //return everything past 2nd to last .
            return fileName.substring(StringUtils.lastOrdinalIndexOf(fileName, ".", 2));
        } else {
            //return the 'normal' extension
            return FilenameUtils.getExtension(inFile.getName());
        }
    }

    /**
     * retrieve the file extension, in the case of a gzipped file (file with .gz as extension) return extension plus the extension prior to the .gz
     * @param fileName name of file to evaluate
     * @return file extension
     */
    public static String getExtension(String fileName){

        if(!StringUtil.isSet(fileName)){
            return null;
        }

        if(fileName.toUpperCase().endsWith(".GZ")){
            //return everything past 2nd to last .
            return fileName.substring(StringUtils.lastOrdinalIndexOf(fileName, ".", 2)+1);
        } else {
            //return the 'normal' extension
            return FilenameUtils.getExtension(fileName);
        }
    }

    public static String getExtensionWithoutGz(String fileName){

        //strip off the ".gz" if it's there
        if(fileName.toUpperCase().endsWith(".GZ")){
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
        }
        return FilenameUtils.getExtension(fileName);

    }

    public static String getFileNameWithoutGz(String fileName){

        //strip off the ".gz" if it's there
        if(fileName.toUpperCase().endsWith(".GZ")){
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
        }
        return fileName;

    }




    /**
     * return a list of all files in a folder
     * @param recursive true if the list should include files in all subfolders
     * @return

    private List<File> getFilesInFolder(String rootFolder, boolean recursive) throws JException {

    List<File> allFiles;

    try {

    int depth = recursive ? Integer.MAX_VALUE : 1;

    allFiles = Files.walk(Paths.get(rootFolder.getAbsolutePath()), depth)
    .filter(Files::isRegularFile)
    .map(Path::toFile)
    .collect(Collectors.toList());

    } catch (IOException ioe) {
    throw new JException("Error retrieving all files in the directory: " + mRootDir.getAbsolutePath(), ioe);
    }

    return allFiles;


    }*/




}