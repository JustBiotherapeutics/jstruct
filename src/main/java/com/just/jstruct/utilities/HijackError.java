package com.just.jstruct.utilities;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;



/**
 * http://en.wiktionary.org/wiki/hijack
 *    ...
 *    To seize control of some process to achieve a purpose other than its originally intended one.
 *    ...
 * when an unrecoverable error is thrown we need to warn the user and possibly stop the current process
 * instead of ignoring it and continuing. calling the static function gotoErrorPage(Exception e) in 
 * this class will handle the flow of the page and displaying the error message to the user.
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="hijackError")
@ViewScoped
public class HijackError {
    
    
    
    private static final Logger LOGGER = Logger.getLogger(HijackError.class.getName());
    
    private final Exception exception;
    private final String message;
    private final String errToString;
    private final String when;
    private String stackTraceOut;
    
    
    /*
     * the constructor will hijack the current page flow and force the user to head over to an error page
     * that contains information about the error and what to do next
     */
    public HijackError() {

        exception = (Exception)FacesUtil.retrieveFromSession("hijacked_error");

        System.out.println("********************************************************************");
        System.out.println("HijackError caught Exception ...");
        LOGGER.log(Level.SEVERE, "HijackError caught Exception", exception);


        //store the error message info
        message = findErrorMsgInException(exception);
        errToString = exception.toString();
        when = new Date().toString();


        //store the stack trace
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        stackTraceOut = sw.toString();
        stackTraceOut = stackTraceOut.replaceAll("<", "&lt;");
        stackTraceOut = stackTraceOut.replaceAll(">", "&gt;");
        stackTraceOut = stackTraceOut.replaceAll("at ", "<br/>&nbsp;&nbsp;&nbsp;&nbsp; at ");
        stackTraceOut = stackTraceOut.replaceAll("com.just.jstruct", "<font color='#0000ff'>com.just.jstruct</font>");
        stackTraceOut = stackTraceOut.replaceAll("Caused by:", "<br/><font color='#00A600'>Caused by:</font>");
        
    }


    //getters
    public Exception getException() { return exception; }
    public String getMessage() { return message; }
    public String getErrToString() { return errToString; }
    public String getWhen() { return when; }
    public String getStackTraceOut() { return stackTraceOut; }



    /**
     * static function to be called when a unrecoverable error happens
     * this will store the exception in the session, and redirect (hijack the page flow) to the error page
     * where the error will be displayed to the user and instructions on what to do next (or something...)
     * @param e the thrown exception that caused the user to no longer believe in the magic of technology
     */
    public static void gotoErrorPage(Exception e){
        //put exception in session, then go to error page
        FacesUtil.storeOnSession("hijacked_error", e);
        FacesUtil.redirectRequest("/error.xhtml?faces-redirect=true");
    }



    //figure out the main error message from the exception (localizedmessage, message, or tostring)
    private static String findErrorMsgInException(Exception e) {
        String errMsg = "";
        if (null != e) {
            errMsg = e.getLocalizedMessage();
            if (null == errMsg || errMsg.equals("")) {
                errMsg = e.getMessage();
            }
            if (null == errMsg || errMsg.equals("")) {
                errMsg = e.toString();
            }
        }
        return errMsg;
    }
    
    
    
}
