package com.just.jstruct.utilities;

import com.hfg.util.StringUtil;
import com.just.bio.structure.enums.ChainType;
import com.just.jstruct.model.Jstruct_SearchParam;
import com.just.jstruct.servlets.ApiSequenceServlet;
import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.structureSearch.subQuery.SubQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class FastaUtil {
    
    
    public static final List<ChainType> CHAIN_TYPE_PROTEIN = Arrays.asList(ChainType.PROTEIN);
    public static final List<ChainType> CHAIN_TYPE_NUCLEIC_ACID = Arrays.asList(ChainType.NUCLEIC_ACID);
    public static final List<ChainType> CHAIN_TYPE_ALL = Arrays.asList(ChainType.values());
    
    
    /**
     * public static method that creates a fasta url (see: ApiSequenceServlet) based on the fullQueryInput 
     * @param fqi
     * @param sequenceFrom
     * @param chainTypes
     * @param outputFileName
     * @return 
     * @throws java.lang.NoSuchFieldException 
     */
    public static String createFastaUrl(FullQueryInput fqi, String sequenceFrom, List<ChainType> chainTypes, String outputFileName) throws NoSuchFieldException {
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        HttpServletRequest request = FacesUtil.getRequest(); 
        
        List<String> chainTypeList = new ArrayList<>(chainTypes.size());
        for(ChainType chainType : chainTypes){
            chainTypeList.add(chainType.name());
        }
        
        StringBuilder url = new StringBuilder();
        
        url.append(vmu.getBaseUrl(request));
        url.append(ApiSequenceServlet.API_SEQUENCE_BASE);
        url.append(ApiSequenceServlet.FASTA_FILE_ACTION).append("?");
        
        url.append("sequence_from=").append(sequenceFrom);
        url.append("&chain_types=").append(StringUtil.join(chainTypeList, ","));
        url.append("&output_file_name=").append(outputFileName);
        
        url.append("&file_status_rcsb=").append(fqi.getFileStatusRcsb().name());
        url.append("&file_status_manual=").append(fqi.getFileStatusManual().name());
        url.append("&query_mode=").append(fqi.getQueryMode().name());

        
        for(SubQuery subQuery : fqi.getAsSubQuerys()){
             
            url.append("&subquery=").append(subQuery.getSubQueryKey()).append(";"); 
            
            List<Jstruct_SearchParam> searchParams = PostgresDao.createSearchParamsForThisSubquery(subQuery.getSubQueryType(), subQuery.getSubQueryInput(), null);
            List<String> keyVal = new ArrayList<>(searchParams.size());
            for(Jstruct_SearchParam searchParam : searchParams){
                keyVal.add(searchParam.getParamKey() + ":" + searchParam.getParamValue());
            }
            url.append(StringUtil.join(keyVal, ";"));

        }
        
        return url.toString();
    }
    
    
    /**
     * 
     * @param fileStatusRcsb
     * @param fileStatusManual
     * @param queryMode
     * @param sequenceFrom
     * @param chainTypes
     * @param outputFileName
     * @param subquery
     * @return 
     */
    public static String createFastaUrl(FileStatus fileStatusRcsb, FileStatus fileStatusManual, QueryMode queryMode,
                                        String sequenceFrom, List<ChainType> chainTypes, String outputFileName, String subquery){
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        HttpServletRequest request = FacesUtil.getRequest(); 
        
        List<String> chainTypeList = new ArrayList<>(chainTypes.size());
        for(ChainType chainType : chainTypes){
            chainTypeList.add(chainType.name());
        }
        
        StringBuilder url = new StringBuilder();
        
        url.append(vmu.getBaseUrl(request));
        url.append(ApiSequenceServlet.API_SEQUENCE_BASE);
        url.append(ApiSequenceServlet.FASTA_FILE_ACTION).append("?");
        
        url.append("sequence_from=").append(sequenceFrom);
        url.append("&chain_types=").append(StringUtil.join(chainTypeList, ","));
        url.append("&output_file_name=").append(outputFileName);
        
        url.append("&file_status_rcsb=").append(fileStatusRcsb.name());
        url.append("&file_status_manual=").append(fileStatusManual.name());
        url.append("&query_mode=").append(queryMode.name());
        
        url.append("&subquery=").append(subquery);
        
        return url.toString();
    }
    
    
    
    
    
    
    
}
