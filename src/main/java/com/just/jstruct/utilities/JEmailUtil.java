package com.just.jstruct.utilities;

import com.hfg.datetime.DateUtil;
import com.hfg.util.StackTraceUtil;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_ImportRest;
import com.just.jstruct.model.Jstruct_StructVersion;
import com.just.jstruct.model.Jstruct_User;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * class containing static methods related to sending messages from Jstruct
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JEmailUtil {
    


    /**
     * return true if the specified string will evaluate to a valid email address
     * @param emailAttempt
     * @return
     */
    public static boolean isValidEmailFormat(String emailAttempt){
        Pattern p = Pattern.compile("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$");
        Matcher m = p.matcher(emailAttempt.trim());
        return m.matches();
    }


    //###########################################################################
    // PUBLIC METHODS - primary sendEmail methods
    //###########################################################################

    public static boolean sendEmail(boolean isHtml, String subject, String body){
        
        ValueMapUtils valueMapUtils = ValueMapUtils.getInstance();
        String errorEmailRecipients = valueMapUtils.getErrorEmailRecipients();
        String defaultSender = valueMapUtils.getEmailDefaultSender();

        return sendEmail(isHtml, subject, body, errorEmailRecipients, defaultSender);
    }

    public static boolean sendEmail(boolean isHtml, String subject, String body, String recipients){
        
        ValueMapUtils valueMapUtils = ValueMapUtils.getInstance();
        String defaultSender = valueMapUtils.getEmailDefaultSender();
        
        return sendEmail(isHtml, subject, body, recipients, defaultSender);
    }

    public static boolean sendEmail(boolean isHtml, String subject, String body, String recipients, String sender) {

        ValueMapUtils valueMapUtils = ValueMapUtils.getInstance();
        String appInstance = valueMapUtils.getAppInstance();
        
        //if this is NOT production...
        if(!appInstance.equalsIgnoreCase("production")) {
            subject = "[" + appInstance + "] " + subject;
            //...override the recipients of this message
            if(StringUtil.isSet(valueMapUtils.getDevTestRedirectRecipients())) {
                String originalRecipients = recipients;
                recipients = valueMapUtils.getDevTestRedirectRecipients();
                body = "MESSAGE REDIRECTED IN " + appInstance.toUpperCase() + " ENVIRONMENT. ORIGINAL RECIPIENT(S) WERE: " + originalRecipients
                        + (isHtml ? "<br/><br/>" : "\n\n") + body;
            } else {
                return true;
            }
        }



        try {

            InternetAddress[] recipientAddresses = createAddressesFromString(recipients);
            InternetAddress senderAddress = new InternetAddress(sender);
            String mailHost = valueMapUtils.getMailHost();

            if (recipientAddresses != null && StringUtil.isSet(mailHost)) {
                // Initialize the JavaMail Session
                Properties props = System.getProperties();
                props.put("mail.smtp.host", mailHost);
                Session session = Session.getDefaultInstance(props);
                //Session session = Session.getInstance(props);

                Message msg = new MimeMessage(session);

                //FROM
                msg.setFrom(senderAddress);

                //TO
                msg.setRecipients(Message.RecipientType.TO, recipientAddresses);
                //add in CC, BCC here...

                //SUBJECT
                msg.setSubject(subject);

                //BODY
                if (isHtml) {
                    msg.setContent(body, "text/html; charset=utf-8");
                } else  {
                    msg.setText(body);
                }

                //DATE
                msg.setSentDate(new Date());

                // send the thing off
                Transport.send(msg);
                return true;
            }
            return false;
        }
        catch (Exception e)
        {
            // Don't derail handling. Just print to the log.
            e.printStackTrace();
            return false;
        }
    }




    //###########################################################################
    // PUBLIC METHODS - functionality specific emails
    //###########################################################################

    
    public static boolean sendJstructStartupEmail(){
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        
        if(vmu.getAppInstance().equalsIgnoreCase("production")) {
            
            String recipients = vmu.getErrorEmailRecipients();
            String subject = "JStruct startup...";
            StringBuilder body = new StringBuilder();

            body.append("JStruct started. JStructContextListener.contextInitialized has been called");
            body.append("<ul>");
            body.append("    <li>Environment: ").append(vmu.getAppInstance()).append("</li>");
            body.append("    <li>Date: ").append(DateUtil.getYYYY_MM_DD_HH_mm_ss(new Date())).append("</li>");
            body.append("</ul>");

            return sendEmail(true, subject, body.toString(), recipients);
        }
        return false;
    }
   
    
    /**
     * send a message when a RCSB Import process is complete
     * @param inImportJob
     * @param importRests
     * @param inUser
     * @param ex
     * @param optionalExceptions
     * @return
     */
    public static boolean sendJobCompleteEmail(Jstruct_ImportJob inImportJob, List<Jstruct_ImportRest> importRests, Jstruct_User inUser,
                                                      Throwable ex, List<Exception> optionalExceptions){

        String recipients = ValueMapUtils.getInstance().getProcessEmailRecipients();
        String subject;
        StringBuilder body = new StringBuilder();

        if (null != ex) {
            subject = "JStruct Job (ImportJobId: " + inImportJob.getImportJobId() + ") FAILED";
            body.append("<h3 style=\"color:#ff0000;\">A JStruct Job has FAILED</h3>");
        } else {
            subject = "JStruct Job (ImportJobId: " + inImportJob.getImportJobId() + ") successful";
            body.append("<h3>The JStruct Job was successful</h3>");
        }

        body.append("<h4>Job Data</h4>");
        body.append("<ul>");
        body.append("    <li>run by: ").append(inUser.getName()).append("</li>");
        body.append("    <li>job type: ").append(inImportJob.getImportType()).append("</li>");
        body.append("    <li>status: ").append(inImportJob.getImportStatus()).append("</li>");
        body.append("    <li>Job ImportJobId: ").append(inImportJob.getImportJobId()).append("</li>"); //todo include link
        body.append("</ul>");
        

        body.append("<h4>Timing</h4>");
        body.append("<ul>");
        body.append("    <li>job start: ").append(DateUtil.getYYYY_MM_DD_HH_mm_ss(inImportJob.getRunStart())).append("</li>");

        if(null!=inImportJob.getRunEnd()) {
            body.append("    <li>job end: ").append(DateUtil.getYYYY_MM_DD_HH_mm_ss(inImportJob.getRunEnd())).append("</li>");
            String totalTime = JDateUtil.differenceBetweenDatesToElapsedTime(inImportJob.getRunStart(), inImportJob.getRunEnd());
            body.append("        <ul><li><span style='color:#777777;'>elapsed time: ").append(totalTime).append("</span></li></ul>");
        }
        body.append("</ul>");

        
        body.append("<h4>Structures/Files</h4>");
        body.append("<ul>");
        body.append("    <li>Total files identified: ").append(inImportJob.getCountUniqueSource()).append(" <span style='color:#777777;'>(count of files found in RCSB)</span>");
        body.append("    <li>Files imported: ").append(inImportJob.getCountImported()).append(" <span style='color:#777777;'>(count of files successfully imported)</span>");
        body.append("        <ul>"); 
        body.append("            <li>Added to JStruct: ").append(inImportJob.getCountAdded()).append("</li>");
        body.append("            <li>Updated in JStruct: ").append(inImportJob.getCountUpdated()).append("</li>");
        body.append("        </ul>");
        body.append("    </li>");
        //body.append("    <li>Moebatch files processed: ").append(inImportJob.getCountMoebatched());
        body.append("</ul>");
        

        if(CollectionUtil.hasValues(importRests)) {
            body.append("<h4>Structures/Files Details</h4>");
            body.append("<ul>");
            for (Jstruct_ImportRest importRest : importRests) {
                body.append(" <li>");
                body.append("       ").append(importRest.getSearchDesc()).append(": ").append(importRest.getResponsePdbCount());
                if(CollectionUtil.hasValues(importRest.getImportPdbs())){
                    body.append("  <ul><li><span style='color:#777777; font-size:9px;'>");
                    body.append(StringUtil.join(importRest.getImportPdbIds(), ", "));
                    body.append("  </span></li></ul>");
                }
                body.append(" </li>");
            }
            body.append("</ul>");
        }

        
        if(CollectionUtil.hasValues(optionalExceptions)){
            body.append("<h3 style=\"color:#ff0000;\">Non-fatal Import Errors:</h3>");
            body.append("<ul>");
            for (Exception je : optionalExceptions) {
                body.append("<li title='").append(StackTraceUtil.getExceptionStackTrace(je)).append("'>");
                body.append(je.getMessage());
                
                body.append("  <ul><li><span style='font-family:monospace; font-size:9px;'>");
                body.append(prepStringForHtmlEmail(StackTraceUtil.getExceptionStackTrace(je)));
                body.append("  </span></li></ul>");
                
                body.append("</li>");
            }
            body.append("</ul>");
        }

        
        if(null != ex){
            String stackTrace = StackTraceUtil.getExceptionStackTrace(ex);
            body.append("<h3 style=\"color:#ff0000;\">Stack trace:</h3>");
            body.append("<p style=\"font-family:monospace;\">").append(prepStringForHtmlEmail(stackTrace)).append("</p>");
        }

        return sendEmail(true, subject, body.toString(), recipients);

    }
    
    


    /**
     * error email sent when an Import throws an error (Import Job)
     * @param ex
     * @return
     */
    public static boolean sendJobErrorEmail(Throwable ex){

        String errorEmailRecipients = ValueMapUtils.getInstance().getErrorEmailRecipients();
        String processEmailRecipients = ValueMapUtils.getInstance().getProcessEmailRecipients();

        String recipients = errorEmailRecipients + ", " + processEmailRecipients; //defined in context.xml file
        String subject;
        StringBuilder body = new StringBuilder();

        subject = "JStruct Job threw a FATAL ERROR!";

        body.append("<h3 style=\"color:#ff0000;\">A JStruct Job has FAILED... like really really bad.</h3>");

        String stackTrace = StackTraceUtil.getExceptionStackTrace(ex);
        
        body.append("<h3 style=\"color:#ff0000;\">Stack trace:</h3>");
        body.append("<p style=\"font-family:monospace;\">").append(prepStringForHtmlEmail(stackTrace)).append("</p>");

        return sendEmail(true, subject, body.toString(), recipients);

    }




    /**
     * send message when a manual import is complete
     * @param inImportJob
     * @param inUser
     * @param inStructVersion
     * @param ex
     * @return
     */
    public static boolean sendManualImportCompleteEmail(Jstruct_ImportJob inImportJob, Jstruct_User inUser, Jstruct_StructVersion inStructVersion, Throwable ex){

        String recipients = ValueMapUtils.getInstance().getProcessEmailRecipients();
        String subject;
        StringBuilder body = new StringBuilder();

        if (null != ex) {
            subject = "JStruct Manual Upload (ImportJobId: " + inImportJob.getImportJobId() + ") FAILED";
            body.append("<h3 style=\"color:#ff0000;\">A JStruct Manual Upload has FAILED</h3>");
        } else {
            subject = "JStruct Manual Upload (ImportJobId: " + inImportJob.getImportJobId() + ") successful";
            body.append("<h3>The JStruct Manual Upload was successful</h3>");
        }

        body.append("<ul>");
        body.append("    <li>upload by: ").append(inUser.getName()).append("</li>");
        body.append("    <li>status: ").append(inImportJob.getImportStatus()).append("</li>");

        if(null!=inImportJob.getRunStart()) {
            body.append("    <li>start: ").append(DateUtil.getYYYY_MM_DD_HH_mm_ss(inImportJob.getRunStart())).append("</li>");
        }
        if(null!=inImportJob.getRunEnd()) {
            body.append("    <li>end: ").append(DateUtil.getYYYY_MM_DD_HH_mm_ss(inImportJob.getRunEnd())).append("</li>");
        }

        if(null != inStructVersion){
            body.append("    <li>struct version id: ").append(inStructVersion.getStructVerId()).append(" (todo - change to link) </li>"); //todo change to link
        }

        body.append("    <li>Import Job ImportJobId: ").append(inImportJob.getImportJobId()).append(" (todo - change to link) </li>"); //todo change to link
        body.append("</ul>");

        if(null != ex){
            String stackTrace = StackTraceUtil.getExceptionStackTrace(ex);
            body.append("<h4>Stack trace:</h4>");
            body.append("<p style=\"font-family:monospace;\">").append(prepStringForHtmlEmail(stackTrace)).append("</p>");
        }

        return sendEmail(true, subject, body.toString(), recipients);

    }


    /**
     * send a message when a job is initiated, but the same job is already running
     * @param jobName
     * @param params
     * @return
     */
    public static boolean sendJobCurrentlyRunningEmail(String jobName, Map<String, String> params){

        String errorEmailRecipients = ValueMapUtils.getInstance().getErrorEmailRecipients();
        String processEmailRecipients = ValueMapUtils.getInstance().getProcessEmailRecipients();

        String recipients = errorEmailRecipients + ", " + processEmailRecipients; //defined in context.xml file
        String subject = jobName + " execution disallowed while another instance is running.";
        StringBuilder body = new StringBuilder();

        body.append("<h3>Attempt to run a '").append(jobName).append("' Job while another instance is already running.</h3>");

        body.append("<p>");
        body.append(" Only one ").append(jobName).append(" job is allowed to run at a time. ");
        body.append(" An attempt to initiate a ").append(jobName).append(" job occurred at: ");
        body.append(DateUtil.getYYYY_MM_DD_HH_mm_ss(new Date()));
        body.append("</p>");

        if(null!=params){
            body.append("<p>job parameters:");
            body.append("   <ul>");
            for(Map.Entry<String, String> entry : params.entrySet()) {
                body.append("       <li>");
                body.append("            ").append(entry.getKey()).append(": ").append(entry.getValue());
                body.append("       </li>");
            }
            body.append("   </ul>");
            body.append("</p>");
        }

        return sendEmail(true, subject, body.toString(), recipients);

    }
    
    
    
    public static boolean sendNewUserLoginEmail(Jstruct_User user){
        
        String recipients = ValueMapUtils.getInstance().getNewUserRecipients();
        String subject;
        StringBuilder body = new StringBuilder();

        subject = "JStruct - a new user has logged in to the system for the first time";
        
        body.append("<h3>First time access for: ").append(user.getUid()).append("</h3>");

        body.append("<ul>");
        
        body.append("  <li> Date/time: <b>").append(JDateUtil.defaultDateAndTimeFormat(new Date(), false)).append("</b></li>");
        
        body.append("  <li> UID: <b>").append(user.getUid()).append("</b></li>");
        body.append("  <li> Name: <b>").append(user.getName()).append("</b></li>");
        body.append("  <li> Email: <b>").append(user.getEmail()).append("</b></li>");
        
        body.append("  <li>Status: <b>").append(user.getStatus()).append("</b></p>");
        body.append("  <li>is Contributor: <b>").append(user.getIsRoleContributor()).append("</b></li>");
        body.append("  <li>is Admin: <b>").append(user.getIsRoleAdmin()).append("</b></li>");
        body.append("  <li>is Developer: <b>").append(user.getIsRoleDeveloper()).append("</b></li>");
        
        body.append("</ul>");

        return sendEmail(true, subject, body.toString(), recipients);
        
    }



    //###########################################################################
    // HELPER METHODS
    //###########################################################################

    public static InternetAddress[] createAddressesFromString(String recipientsString) throws AddressException {

        List<InternetAddress> addresses = null;
        if (StringUtil.isSet(recipientsString))
        {
            addresses = new ArrayList<>(3);
            String[] recipients = recipientsString.split("[\\s\\,\\;]+");
            for (String recipient : recipients)
            {
                if (StringUtil.isSet(recipient))
                {
                    addresses.add(new InternetAddress(recipient));
                }
            }
        }
        return (addresses != null ? addresses.toArray(new InternetAddress[addresses.size()]) : null);

    }

    //###########################################################################
    // PRIVATE METHODS
    //###########################################################################

    private static String prepStringForHtmlEmail(String originalString) {
        
        String changeGt = originalString.replaceAll(">", "&gt;");
        String changeLt = changeGt.replaceAll("<", "&lt;");
        
        String changeLinebreaks = changeLt.replaceAll("(\\r\\n|\\n)", "<br />");
        String changeTabs = changeLinebreaks.replaceAll("(\\t)", "&nbsp;&nbsp;&nbsp;&nbsp;");
        
        return changeTabs;
    }

}
