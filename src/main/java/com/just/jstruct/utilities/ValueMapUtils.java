package com.just.jstruct.utilities;

import com.just.jstruct.dao.service.ValueMapService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_ValueMap;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ValueMapUtils {

    //##################################################################################################################
    // data members that map to entries in the ValueMap table
    //##################################################################################################################

    
    //anything in this list that has a value assigned does not have a corresponding entry in the ValueMap table
    //this just allows us to keep settings that should NOT be changed is a convenient spot with most everything else (also see the context file)

    //EMAIL
    private String MailHost;
    private String ErrorEmailRecipients;
    private String ProcessEmailRecipients;
    private String DevTestRedirectRecipients;
    private String EmailDefaultSender;
    private String NewUserRecipients;

    //SYSTEM
    private String APP_NAME = "JStruct";
    private String LocalTmpDirectory;
    private String DefaultRole;
    private String DefaultStructurePermission;
    private String ClientSupportType;

    //IMPORT
    private String RcsbImportCron;
    private String CronUrl;

    //URL
    private String RcsbBaseUrl;
    private String RcsbObsoleteUrl;
    private String RcsbRestFileUrl;
    private String RcsbRestDownloadUrl;
    private String RcsbViewerUrl;
    
    private String JustBioHomeUrl = "https://just-evotecbiologics.com/";
    private String JstructCodeRepoUrl = "https://bitbucket.org/AbacusAbTools/jstruct/src";
    private String JstructLicenseUrl = "https://bitbucket.org/AbacusAbTools/jstruct/wiki/License";
    private String PermalinkStructureBase = "permalink/structure/";
    private String ApiStructureBase = "api/structure/";

    //STRUCTURE
    private String StructureIdPrefix;
    private String ManualUploadSource;
    
    
    //ApplicationResources.properties

    private String PermalinkBaseUrl;

    private String LDAP_CorpDomain;
    private String LDAP_ServerURL;
    private String LDAP_UserContext;
    private String LDAP_PrincipalFieldName;
    private String LDAP_PrincipalSuffix;
    
    private String CredMgrFile;
    private String CredMgrKey;

    private String AppVersion;
    private String AppDate;
    private String AppDatabase;
    private String AppInstance;
    private String AppFullInstance;
    
  


    //##################################################################################################################
    // Singleton Pattern constructor and static initialization block
    //##################################################################################################################

    private static ValueMapUtils valueMapUtils;

    //private constructor so this can't be instantiated
    private ValueMapUtils(){
        try {
            init();
        } catch (JDAOException | IllegalAccessException | NoSuchFieldException | IOException e){
            throw new RuntimeException("Exception occurred creating 'ValueMapUtils' singleton instance", e);
        }
    }


    //##################################################################################################################
    // public methods
    //##################################################################################################################

    /**
     * 
     * @return 
     */
    public static ValueMapUtils getInstance() {
        if(valueMapUtils == null){
            valueMapUtils = new ValueMapUtils();
        }
        return valueMapUtils;
    }


    /**
     *
     * @return
     */
    public ValueMapUtils refreshInstance() {
        valueMapUtils = new ValueMapUtils();
        return valueMapUtils;
    }
    
    
    public boolean isBootstrapComplete() throws JDAOException {
        
        ValueMapService valueMapService = new ValueMapService();
        List<Jstruct_ValueMap> valueMaps = valueMapService.getAll();
        
        for(Jstruct_ValueMap valueMap : valueMaps){
            if(!valueMap.isInitialized()){
                return false;
            }
        }
        return true;
    }
    
    
    /**
     * something like:
     *   http://jstruct.evotec.com/jstruct/
     *   http://localhost:8080/jstruct/
     * @param request
     * @return 
     */
    public String getBaseUrl(HttpServletRequest request) {

        StringBuilder sb = new StringBuilder();
        sb.append(request.getScheme());
        sb.append("://");
        sb.append(request.getServerName());
        sb.append(":");
        sb.append(request.getLocalPort());
        sb.append(request.getContextPath());
        sb.append("/");

        return sb.toString();
    }
    
    public String getBaseUrl(){
        return getBaseUrl(FacesUtil.getRequest());
    }

    
    
    
    //EMAIL
    public String getMailHost() {return MailHost;}
    public String getErrorEmailRecipients() {return ErrorEmailRecipients;}
    public String getProcessEmailRecipients() {return ProcessEmailRecipients;}
    public String getDevTestRedirectRecipients() {return DevTestRedirectRecipients;}
    public String getEmailDefaultSender() {return EmailDefaultSender;}
    public String getNewUserRecipients() {return NewUserRecipients;}
        
        
    //SYSTEM
    public String getAppName(){ return APP_NAME; }
    public String getLocalTmpDirectory() {return LocalTmpDirectory;}
    public String getDefaultRole() {return DefaultRole;}
    public String getDefaultStructurePermission() {return DefaultStructurePermission;}
    public String getClientSupportType() {return ClientSupportType;}
    
    public boolean isClientSupportTypeIsNone(){return ClientSupportType.equalsIgnoreCase("none");}
    public boolean isClientSupportTypeIsOptional(){return ClientSupportType.equalsIgnoreCase("optional");}
    public boolean isClientSupportTypeIsRequired(){return ClientSupportType.equalsIgnoreCase("required");}

    //IMPORT
    public String getRcsbImportCron() {return RcsbImportCron;}
    public String getCronUrl() {return CronUrl;}

    //URL
    public String getRcsbBaseUrl() {return RcsbBaseUrl;}
    public String getRcsbObsoleteUrl() {return RcsbObsoleteUrl;}
    public String getRcsbRestFileUrl() {return RcsbRestFileUrl;}
    public String getRcsbRestDownloadUrl() {return RcsbRestDownloadUrl;}
    public String getRcsbViewerUrl() {return RcsbViewerUrl;}

    public String getJustBioHomeUrl() {return JustBioHomeUrl;}
    public String getJstructCodeRepoUrl() {return JstructCodeRepoUrl;}
    public String getJstructLicenseUrl() {return JstructLicenseUrl;}
    public String getPermalinkStructureBase() {return PermalinkStructureBase;}
    public String getApiStructureBase() {return ApiStructureBase;}

    //STRUCTURE
    public String getStructureIdPrefix() {return StructureIdPrefix;}
    public String getManualUploadSource() {return ManualUploadSource;}
    
    
    //ApplicationResources.properties
    
    public String getPermalinkBaseUrl() {return PermalinkBaseUrl;}
    
    public String getLDAP_CorpDomain() {return LDAP_CorpDomain;}
    public String getLDAP_ServerURL() {return LDAP_ServerURL;}
    public String getLDAP_PrincipalFieldName() {return LDAP_PrincipalFieldName;}
    public String getLDAP_PrincipalSuffix() {return LDAP_PrincipalSuffix;}
    public String getLDAP_UserContext() {return LDAP_UserContext;}
    
    public String getCredMgrFile() {return CredMgrFile;}
    public String getCredMgrKey() {return CredMgrKey;}
    
    public String getAppVersion() {return AppVersion;}
    public String getAppDate() {return AppDate;}
    public String getAppDatabase() {return AppDatabase;}
    public String getAppInstance() {return AppInstance;}
    public String getAppFullInstance() {return AppFullInstance;}
    




    //##################################################################################################################
    // private methods
    //##################################################################################################################


    /**
     *
     * @throws SQLException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    private void init() throws JDAOException, IllegalAccessException, NoSuchFieldException, IOException {

        //first get all ValueMaps from the database
        
        ValueMapService valueMapService = new ValueMapService();
        List<Jstruct_ValueMap> valueMaps = valueMapService.getAll();

        //use reflection to set the value of each field based on the item key
        //this requires that this class data member names match exactly the itemKey values in the table
        for (Jstruct_ValueMap valueMap : valueMaps) {

            ValueMapUtils vmu = this; 
            Class vmuClass = vmu.getClass();
            Field field = vmuClass.getDeclaredField(valueMap.getItemKey());

            if (valueMap.getIsValidInt()) {
                field.setInt(vmu, JStringUtil.stringToInteger(valueMap.getItemValue()));
            } else {
                field.set(vmu, valueMap.getItemValue());
            }

        }
        
        
        //next get all properties from the ApplicationResources.properties file
        Properties allProperties = getAllProperties();
        for(Map.Entry<Object, Object> entry : allProperties.entrySet()){
            
            String propKey = (String) entry.getKey();
            String propValue = (String) entry.getValue();
            
            //again with the reflection
            ValueMapUtils vmu = this; 
            Class vmuClass = vmu.getClass();
            Field field = vmuClass.getDeclaredField(propKey);
            field.set(vmu, propValue);
            
        }
        

    }



    /**
     * get all properties from ApplicationResources.properties file
     * @return all the properties
     * @throws IOException
     */
    public Properties getAllProperties() throws IOException {

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/ApplicationResources.properties");
        Properties properties = new Properties();
        properties.load(inputStream);
        return properties;

    }

    
    
    /**
     * get a property from ApplicationResources.properties file, or if the
     * key is not found in that file, look for it in the ValueMap table  - TODO how to manage properties/valuemap/etc
     *
     * @param key which property to find
     * @return the String value for that property; if not found, return null
     * @throws Exception
     
    public String getProperty(String key) throws Exception {

        String propValue;

        //first look in the properties file for a match
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/ApplicationResources.properties");
        Properties properties = new Properties();

        properties.load(inputStream);
        propValue = properties.getProperty(key);

        //if not in the properties file, look in the database  --- TODO
        try {
            if (null==propValue){
      //          ValueMapService valueMapService = new ValueMapService();
      //          ValueMap valueMap = valueMapService.getByItemKey(key);
      //          propValue = valueMap.getItemValue();
            }
        } catch (NoResultException nre) {
            propValue = "unable to find property: " + key;
            Logger.getLogger(PropUtil.class.getName()).log(Level.SEVERE, null, nre);
        }

        return propValue;
    }
    * */

}
