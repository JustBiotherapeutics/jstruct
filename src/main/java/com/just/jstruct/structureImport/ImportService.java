package com.just.jstruct.structureImport;

import com.hfg.sql.SQLUtil;
import com.hfg.util.StackTraceUtil;
import com.hfg.util.StringUtil;
import com.just.bio.structure.Structure;
import com.just.bio.structure.exceptions.StructureParseException;
import com.just.bio.structure.pdb.PdbFileParse;
import com.just.jstruct.dao.service.FileImportJobService;
import com.just.jstruct.dao.service.ImportMsgService;
import com.just.jstruct.dao.service.PdbFileService;
import com.just.jstruct.dao.service.PdbLineageService;
import com.just.jstruct.dao.service.StructFileService;
import com.just.jstruct.dao.service.StructVersionService;
import com.just.jstruct.dao.service.StructureService;
import com.just.jstruct.enums.ProcessChainStatus;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.exception.JImportException;
import com.just.jstruct.model.Jstruct_FileImportJob;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_ImportMsg;
import com.just.jstruct.model.Jstruct_PdbFile;
import com.just.jstruct.model.Jstruct_PdbJrnl;
import com.just.jstruct.model.Jstruct_StructFile;
import com.just.jstruct.model.Jstruct_StructVersion;
import com.just.jstruct.model.Jstruct_Structure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;




/**
 * Service class for database access dealing with importing files
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportService {




    //##################################################################################################################
    // MANUAL IMPORT METHODS
    //##################################################################################################################


    /**
     * upload a structure file and all associated entities (may be brand spankin' new, or to replace an existing version)
     * this function is used for uploading a single new structure file from the MANUAL UPLOAD PROCESS (i.e. a single file at at time, partner)
     * (may be a PDB/CIF file or a non-PDB/CIF file)
     * includes uploading to:
     *     - structure
     *     - structure_version
     *     - struct_file (includes chain, residue)
     *     - pdb_file (and all appropriate pdb_* 'child' tables)
     * @param isParsable
     * @param inImportJob
     * @param inFile
     * @param inStructure
     * @param inStructVersion
     * @param inUser
     * @param inPdbJrnl
     * @param inPdbFile
     * @return
     * @throws JImportException
     */
    public static Jstruct_StructVersion uploadManualImportStructureStuff(boolean isParsable,
                                                                         Jstruct_ImportJob inImportJob, 
                                                                         File inFile,
                                                                         Jstruct_Structure inStructure,
                                                                         Jstruct_StructVersion inStructVersion,
                                                                         Jstruct_PdbFile inPdbFile,
                                                                         Jstruct_PdbJrnl inPdbJrnl,
                                                                         Jstruct_User inUser) throws JImportException {

        try {

            if (null == inFile) {
                throw new JImportException("Error - File is null, unable to perform manual upload!");
            }

            PdbFileService pdbFileService = new PdbFileService();
            FileImportJobService fileImportJobService = new FileImportJobService();

            //----------------------------------------------------------------------------------------------------------
            //create the Structure and StructureVersion entities, and save them too. (existingStructure will be null if this is for a brand new structure)
            Jstruct_StructVersion structVersion = insertManualImportStructureAndVersion(inStructure, inStructVersion);
            Long structVerId = structVersion.getStructVerId();

            
            //parse the file so we can get a structure object, then create and persist the file as a Jstruct_StructFile
            PdbFileParse pdbFileParse = new PdbFileParse();
            Structure structureOjb = pdbFileParse.executeParseByLocalFile(inFile);

            Jstruct_StructFile structFile = JImportUtils.createStructFileFromFile(inFile, structVersion, structureOjb, inUser);

            //delete the local temp file created by the parse process
            structureOjb.getLocalFile().delete();

            if (!isParsable) {

                //if this file is NOT parsable, merge the user entered PdbFile and PdbJrnl into our object
                if (StringUtil.isSet(inPdbFile.getKeywordString())) {
                    String[] keywordsArray = JStringUtil.splitOnCommaAndTrim(inPdbFile.getKeywordString());
                    inPdbFile.setPdbKeywords(PdbDataUtils.transformKeywords(Arrays.asList(keywordsArray), structVerId));
                }
                if (StringUtil.isSet(inPdbFile.getAuthorString())) {
                    String[] authorsArray = JStringUtil.splitOnCommaAndTrim(inPdbFile.getAuthorString());
                    inPdbFile.setPdbAuthors(PdbDataUtils.transformAuthors(Arrays.asList(authorsArray), structVerId));
                }

                //Jstruct_PdbJrnl pdbJrnl = new Jstruct_PdbJrnl(structVerId, uploadInput.getJrnlAuth(), uploadInput.getJrnlTitl(), true);
                List<Jstruct_PdbJrnl> pdbJrnls = new ArrayList<>(1);
                pdbJrnls.add(inPdbJrnl);
                inPdbFile.setPdbJrnls(pdbJrnls);

            }
            inPdbFile.setStructVerId(structVerId);
            inPdbFile.setDepositionDate(new Date()); //the deposit date for manually uploaded files is when it was uploaded.
            pdbFileService.add(inPdbFile); //note: saving a pdb_file will save all related pdb_* 'child' tables

            //----------------------------------------------------------------------------------------------------------
            //add to intersection table tracking import to struct file
            Jstruct_FileImportJob fileImportJob = new Jstruct_FileImportJob(structVerId, inImportJob.getImportJobId());
            fileImportJobService.add(fileImportJob);

            return structVersion;

        } catch (StructureParseException | JDAOException | JException ex) {
            throw new JImportException("Error during insert processing of new Structure File!", ex);
        }

    }




    /**
     *
     * @param structure
     * @param inUploadInput
     * @param inUser
     * @return
     * @throws JImportException
     */
    private static Jstruct_StructVersion insertManualImportStructureAndVersion(Jstruct_Structure structure,
                                                                               Jstruct_StructVersion structVersion) throws JImportException {
        
        StructureService structureService = new StructureService();
        StructVersionService structVersionService = new StructVersionService();
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        
        try {

            //a not yet saved structure was passed in, we're adding a brand new struct file to a brand new structure (else we're updating the version)
            if(null==structure.getStructureId()){
                
                boolean canAllReadDefault = false;
                boolean canAllWriteDefault = false;
                
                if(vmu.getDefaultStructurePermission().equalsIgnoreCase("write")){
                    canAllReadDefault = true;
                    canAllWriteDefault = true;
                } else if (vmu.getDefaultStructurePermission().equalsIgnoreCase("read")){
                    canAllReadDefault = true;
                }
                
                structure.setCanAllRead(canAllReadDefault);
                structure.setCanAllWrite(canAllWriteDefault);
                
                structureService.add(structure);
            } 

            structVersion.setStructureId(structure.getStructureId());
            structVersionService.add(structVersion);

            return structVersion;

        } catch (JDAOException ex) {
            throw new JImportException("Database error during processing of Structure & Structure Version File (manual upload)!", ex);
        }

    }





    //##################################################################################################################
    // RCSB RUN IMPORT METHODS
    //##################################################################################################################



    /**
     * upload a new PDB structure file and all associated entities.
     * this function is used for inserting new PDB structure files from the AUTOMATED IMPORT PROCESS (RCSB)
     * includes uploading to:
     *     - structure
     *     - structure_version
     *     - struct_file (includes chain, residue)
     *     - pdb_file (and all pdb_* 'child' tables)
     * @param inImportJob
     * @param inStructFile
     * @param inStructureFormat
     * @param inUser
     * @return 
     * @throws JImportException
     */
    public static Jstruct_StructVersion insertNewRcsbRunStructureStuff(Jstruct_ImportJob inImportJob,
                                                                       Jstruct_StructFile inStructFile,
                                                                       Structure inStructureFormat,
                                                                       Jstruct_User inUser) throws JImportException {
        
        PdbLineageService pdbLineageService = new PdbLineageService();
        StructFileService structFileService = new StructFileService();
        PdbFileService pdbFileService = new PdbFileService();
        FileImportJobService fileImportJobService = new FileImportJobService();

        if (null == inStructFile) {
            throw new JImportException("Error - StructFile is null, unable to upload!");
        }
        if (null != inStructFile.getStructVerId()) {
            throw new JImportException("Error - StructFile is not new; already contains an STRUCT_VER_ID: " + inStructFile.getStructVerId() + ". Unable to insert as new!");
        }

        try {

            String pdbIdentifier = inStructureFormat.getMetadata().getPdbIdentifier();
            Date obslteDate = inStructureFormat.getMetadata().getObslteDate();
            Date sprsdeDate = inStructureFormat.getMetadata().getSprsdeDate();
            List<String> obsltes = inStructureFormat.getMetadata().getObsltes();
            List<String> sprsdes = inStructureFormat.getMetadata().getSprsdes();


            //update the PDB_LINEAGE table
            pdbLineageService.updatePdbLineageWithObsltlData(pdbIdentifier, obsltes, obslteDate);
            pdbLineageService.updatePdbLineageWithSprsdeData(sprsdes, pdbIdentifier, sprsdeDate); 


            //create the Structure and StructureVersion entities, and save them too.
            Jstruct_StructVersion structVersion = insertNewRcsbRunStructureAndVersion(obslteDate, inUser);
            Long structVerId = structVersion.getStructVerId();

            //persist the StructFile!
            inStructFile.setProcessChainStatus(ProcessChainStatus.TODO);
            inStructFile.setStructVerId(structVerId);
            structFileService.add(inStructFile);


            //create and save PDB File and all it's info
            Jstruct_PdbFile pdbFile = PdbDataUtils.createPdbFileFromParsedData(structVerId, inStructureFormat, inImportJob, inUser);
            pdbFileService.add(pdbFile); //note: saving a pdb_file will save all related pdb_* 'child' tables


            //add to intersection table tracking import to struct file
            Jstruct_FileImportJob fileImportJob = new Jstruct_FileImportJob(structVerId, inImportJob.getImportJobId());
            fileImportJobService.add(fileImportJob);


            return structVersion;


        } catch (JDAOException ex) {
            throw new JImportException("Error during insert processing of new file from RCSB! (" + ex.getMessage() + ")", ex);
        }

    }


    /**
     *
     * @param inImportJob
     * @param inExistingStructFile
     * @param inIncomingStructFile
     * @param structureObj
     * @param inUser
     * @throws JImportException
     */
    public static void updateExistingRcsbRunStructureStuff(Jstruct_ImportJob inImportJob,
                                                           Jstruct_StructFile inExistingStructFile, Jstruct_StructFile inIncomingStructFile,
                                                           Structure structureObj,
                                                           Jstruct_User inUser) throws JImportException {
        try {
            
            PdbFileService pdbFileService = new PdbFileService();
            StructFileService structFileService = new StructFileService();
            PdbLineageService pdbLineageService = new PdbLineageService();
            FileImportJobService fileImportJobService = new FileImportJobService();

            Long structVerId = inExistingStructFile.getStructVerId();

            //blow away all 'child' records that need to be replaced (chain/atomGroupings, pdbFile/pdb_*)
            Jstruct_PdbFile doomedPdbFile = pdbFileService.getByStructVerId(structVerId);
            if(null != doomedPdbFile){
                pdbFileService.delete(doomedPdbFile);
            }

            //merge the incomingStructFile (new) updates into the existingStructfile; and save the updates
            inExistingStructFile.setUncompressedSize(inIncomingStructFile.getUncompressedSize());
            inExistingStructFile.setFromLocation(inIncomingStructFile.getFromLocation());
            inExistingStructFile.setUploadFileName(inIncomingStructFile.getUploadFileName());
            inExistingStructFile.setUploadFileExt(inIncomingStructFile.getUploadFileExt());
            inExistingStructFile.setActualFileName(inIncomingStructFile.getActualFileName());
            inExistingStructFile.setActualFileExt(inIncomingStructFile.getActualFileExt());
            //inExistingStructFile.setProcessChainStatus(ProcessChainStatus.TODO); //flag so that the chains will be re-processed
            structFileService.update(inExistingStructFile);
            //note: file contents will be saved by the calling method.


            String pdbIdentifier = structureObj.getMetadata().getPdbIdentifier();
            Date obslteDate = structureObj.getMetadata().getObslteDate();
            Date sprsdeDate = structureObj.getMetadata().getSprsdeDate();
            List<String> obsltes = structureObj.getMetadata().getObsltes();
            List<String> sprsdes = structureObj.getMetadata().getSprsdes();

            //update the PDB_LINEAGE table
            pdbLineageService.updatePdbLineageWithObsltlData(pdbIdentifier, obsltes, obslteDate);
            pdbLineageService.updatePdbLineageWithSprsdeData(sprsdes, pdbIdentifier, sprsdeDate); //todo - determine if it is redundant to do both of these lines
            

            //ensure the structure is in the correct active/obsolete state
            if(null != obslteDate){
                JImportUtils.updateStructureForPdbFileToObsolete(inExistingStructFile, obslteDate);
            } else {
                JImportUtils.updateStructureForPdbFileToActive(inExistingStructFile);
            }


            //create and save PDB File and all it's info
            Jstruct_PdbFile pdbFile = PdbDataUtils.createPdbFileFromParsedData(structVerId, structureObj, inImportJob, inUser);
            pdbFileService.add(pdbFile);  //note: saving a pdb_file will save all related pdb_* 'child' tables


            //add to intersection table tracking import to struct file
            Jstruct_FileImportJob fileImportJob = new Jstruct_FileImportJob(structVerId, inImportJob.getImportJobId());
            fileImportJobService.add(fileImportJob);

        }

        catch(JDAOException | JException ex){
            String inExistingPdbId = "unknown";
            String inIncomingPdbId = "unknown";
            
            if(inExistingStructFile != null && inExistingStructFile.getPdbIdentifier() != null){
                inExistingPdbId = inExistingStructFile.getPdbIdentifier();
            }
            if(inIncomingStructFile != null && inIncomingStructFile.getPdbIdentifier() != null){
                inIncomingPdbId = inIncomingStructFile.getPdbIdentifier();
            }
            
            throw new JImportException("Error during update processing of Structure File and related entities! "
                                     + "(Existing PDB ID = " + inExistingPdbId + ", Incomming PDB ID = " + inIncomingPdbId + ")", ex);
        }


    }





    /**
     *
     * @param inObsoleteDate
     * @param inUser
     * @return
     * @throws JImportException
     */
    private static Jstruct_StructVersion insertNewRcsbRunStructureAndVersion(Date inObsoleteDate, Jstruct_User inUser) throws JImportException {
       
        StructureService structureService = new StructureService();
        StructVersionService structVersionService = new StructVersionService();
        
        try {

            //Save the 'top level' structure entity
            Jstruct_Structure structure = new Jstruct_Structure(inUser);
            structure.setOwnerId(inUser.getUserId());
            structure.setFromRcsb(true);
            structure.setSourceName("RCSB"); //todo refactor this out into the 'repo' tables
            structure.setCanAllRead(true);   //all-can-read is always true for RCSB files
            structure.setCanAllWrite(false); //all-can-write is not applicable to RCSB files

            if (null == inObsoleteDate) {
                structure.setObsolete(false);
                structure.setObsoleteDate(null);
            } else {
                structure.setObsolete(true);
                structure.setObsoleteDate(inObsoleteDate);
            }
            structureService.add(structure);


            //create 'child' structure_version record (note, PDB imported files will never have versioning, hence we set version to 0)
            Jstruct_StructVersion structVersion = new Jstruct_StructVersion(inUser);
            structVersion.setStructureId(structure.getStructureId());
            structVersion.setVersion(0);
            structVersionService.add(structVersion);

            return structVersion;

        } catch (JDAOException ex) {
            throw new JImportException("SQL/Connection error during processing of Structure & Structure Version File (RCSB upload)!", ex);
        }
    }






    /**
     * save a message (typically a warning) during an import job
     * @param inImportJob
     * @param inUser
     * @param inFileInfo
     * @param inMessage
     * @param optionalException
     * @return
     * @throws com.just.jstruct.exception.JDAOException
     */
    public static Jstruct_ImportMsg persistImportWarning(Jstruct_ImportJob inImportJob, Jstruct_User inUser, String inFileInfo, String inMessage, Throwable optionalException) throws JDAOException {

        ImportMsgService importMsgService = new ImportMsgService();
        
        Jstruct_ImportMsg importMsg = new Jstruct_ImportMsg(inUser);
        
        
        importMsg.setImportJobId(inImportJob.getImportJobId());
        importMsg.setFileInfo(inFileInfo);
        importMsg.setMessage(SQLUtil.sqlString(inMessage));
        if (null != optionalException) {
            importMsg.setStackTrace(StackTraceUtil.getExceptionStackTrace(optionalException));
        }
        importMsgService.add(importMsg);


        return importMsg;
    }


    /**
     * save a message (typically a warning) during an import job
     * @param inImportJob
     * @param inUser
     * @param inFile
     * @param inMessage
     * @param optionalException
     * @return
     * @throws JDAOException
     */
    public static Jstruct_ImportMsg persistImportWarning(Jstruct_ImportJob inImportJob, Jstruct_User inUser, File inFile, String inMessage, Exception optionalException) throws JDAOException {
        String fullPath = null;
        if (null != inFile) {
            Path filePath = Paths.get(inFile.getPath());
            fullPath = filePath.toAbsolutePath().toString();
            // note: if the file no longer exists, toAbsolutePath will still get the path from the inFile..
            // however if toRealPath were to be used, this would throw an IOExeption if the file doesn't exist IRL (not what we want...)
        }
        return persistImportWarning(inImportJob, inUser, fullPath, inMessage, optionalException);
    }





}
