package com.just.jstruct.structureImport.rcsbRestImport;


/**
 * RESTful RCSB web service helper
 * 
 * http://www.rcsb.org/pdb/software/rest.do
 * http://www.rcsb.org/pdb/software/static.do?p=/software/webservices/PostXMLQuery.jsp
 * http://www.rcsb.org/pdb/static.do?p=download/http/index.html
 */


@Deprecated
public class RcsbRestHelper_OLD
{
/*

  //  public static final String RCSB_PDB_REST_ALL_CURRENT ="http://www.rcsb.org/pdb/rest/getCurrent";
  //  public static final String RCSB_PDB_REST_ALL_OBSOLETE ="http://www.rcsb.org/pdb/rest/getObsolete";

  //  public static final String RCSB_PDB_REST_SEARCH_SERVICE ="http://www.rcsb.org/pdb/rest/search";


    / **
     * find ALL Current (aka Active) PDB Identifiers
     * @return
     * @throws JException
     * /
    public static List<String> queryForAllCurrentPdbIds() throws JException {
        List<String> pdbIds = pdbIdsGetQuery(RCSB_PDB_REST_ALL_CURRENT, "current");
        return pdbIds;
    }


    / * *
     * find ALL Obsolete PDB Identifiers
     * @return
     * @throws JException
     * /
    public static List<String> queryForAllObsoletePdbIds() throws JException {
        List<String> pdbIds = pdbIdsGetQuery(RCSB_PDB_REST_ALL_OBSOLETE, "obsolete");
        return pdbIds;
    }







    public static List<String> queryForPdbIdsSearchService(String postXml) throws JException
    {
        try
        {
            List<String> pdbIds = postSearchQuery(postXml);
            if(CollectionUtil.hasValues(pdbIds))
            {
                for(String pdbId : pdbIds)
                {
                    if(pdbId.length() > 4)
                    {
                        throw new JException("Error querying RCSB PDB for PDB IDs via Search Service! " 
                                           + "Response appears to have returned PDB IDs that are not 4 characters in length: " + StringUtil.join(pdbIds, ", ")
                                           + "\nPOST query: \n" + postXml + "\n");
                    }
                }
            }
            return pdbIds;

        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
            throw new JException("Error querying RCSB PDB for PDB IDs via Search Service! (" + RCSB_PDB_REST_SEARCH_SERVICE + " - POST query: \n" + postXml + "\n)", ioe);
        }

    }

    
    
    public static String createPdbReleaseDateQuery(Date minDate, Date maxDate){

        // see sample xml queries at bottom of this page: http://www.rcsb.org/pdb/software/rest.do#current
        
        StringBuilderPlus postXml = new StringBuilderPlus();

        postXml.appendln(" <orgPdbQuery> ");
        postXml.appendln("     <queryType>org.pdb.query.simple.ReleaseDateQuery</queryType> ");
        postXml.appendln("     <pdbx_audit_revision_history.revision_date.comparator>between</pdbx_audit_revision_history.revision_date.comparator> ");
        if(null!=minDate) {
            String minDateParam = JDateUtil.formatDate(minDate, "yyyy-MM-dd");
            postXml.appendln("     <pdbx_audit_revision_history.revision_date.min>" + minDateParam + "</pdbx_audit_revision_history.revision_date.min> ");
        }
        if(null!=maxDate) {
            String maxDateParam = JDateUtil.formatDate(maxDate, "yyyy-MM-dd");
            postXml.appendln("     <pdbx_audit_revision_history.revision_date.max>" + maxDateParam + "</pdbx_audit_revision_history.revision_date.max> ");
        }
        //postXml.appendln("      <pdbx_audit_revision_history.ordinal.value>1</pdbx_audit_revision_history.ordinal.value> ");
        postXml.appendln(" </orgPdbQuery> ");

        return postXml.toString();
    }

    public static String createPdbReviseDateQuery(Date minDate, Date maxDate){

        StringBuilderPlus postXml = new StringBuilderPlus();

        postXml.appendln(" <orgPdbQuery> ");
        postXml.appendln("     <queryType>org.pdb.query.simple.ReviseDateQuery</queryType> ");
        postXml.appendln("     <pdbx_audit_revision_history.revision_date.comparator>between</pdbx_audit_revision_history.revision_date.comparator> ");
        if(null!=minDate) {
            String minDateParam = JDateUtil.formatDate(minDate, "yyyy-MM-dd");
            postXml.appendln("     <pdbx_audit_revision_history.revision_date.min>" + minDateParam + "</pdbx_audit_revision_history.revision_date.min> ");
        }
        if(null!=maxDate) {
            String maxDateParam = JDateUtil.formatDate(maxDate, "yyyy-MM-dd");
            postXml.appendln("     <pdbx_audit_revision_history.revision_date.max>" + maxDateParam + "</pdbx_audit_revision_history.revision_date.max> ");
        }
        //postXml.appendln("      <pdbx_audit_revision_history.ordinal.value>1</pdbx_audit_revision_history.ordinal.value> ");
        postXml.appendln(" </orgPdbQuery> ");

        return postXml.toString();
    }




    //###########################################################################
    // HTTP
    //###########################################################################












    //###########################################################################
    // GET
    //###########################################################################


    / ** call an endpoint from the RESTful RCSB web service
     * @param getUrl
     * @return a list of PDB ids.
     * /
    private static List<String> pdbIdsGetQuery(String getUrl, String rootElement) throws JException {

        String startTag = "<" + rootElement.toUpperCase() + ">";
        String endTag = "</" + rootElement.toUpperCase() + ">";

        List<String> parsedPdbIds = new ArrayList<>(999); 

        try 
        {
            URL u = new URL(getUrl);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(u.openStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) 
            {

                if (line.toUpperCase().contains(startTag)) 
                {
                    while ((line = bufferedReader.readLine()) != null) 
                    {
                        if (line.toUpperCase().contains(endTag)) 
                        {
                            break;
                        }
                        String[] split = line.split("\"");
                        parsedPdbIds.add(split[1]);
                    }

                } 
                else if (line.toLowerCase().contains("<html xmlns=")) 
                {
                    throw new JException("Error - URL (" + getUrl + ") appears to be redirecting to an HTML page, instead of XML containing the root tag: " + startTag);
                }


            }
        } 
        catch (IOException ioe) 
        {
            throw new JException("Error retrieving " + startTag + " Structure IDs from: '" + getUrl, ioe);
        }

        return parsedPdbIds;

    }





    //###########################################################################
    // POST
    //###########################################################################

    / ** post am XML query (PDB XML query format) to the RESTful RCSB web service
     *
     * @param postXml
     * @return a list of PDB ids.
     * /
    private static List<String> postSearchQuery(String postXml) throws IOException
    {
        URL u = new URL(RCSB_PDB_REST_SEARCH_SERVICE);
        String encodedXML = URLEncoder.encode(postXml,"UTF-8");
        InputStream in =  doPOST(u, encodedXML);

        List<String> pdbIds = new ArrayList<>();

        BufferedReader rd = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = rd.readLine()) != null)
        {
            pdbIds.add(line);
        }
        rd.close();

        return pdbIds;

    }


    / ** do a POST to a URL and return the response stream for further processing elsewhere.
     *
     * @param url
     * @return
     * @throws IOException
     * /
    private static InputStream doPOST(URL url, String data) throws IOException
    {
        // Send data
        URLConnection conn = url.openConnection();

        conn.setDoOutput(true);

        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

        wr.write(data);
        wr.flush();

        // Get the response
        return conn.getInputStream();

    }





*/
}



