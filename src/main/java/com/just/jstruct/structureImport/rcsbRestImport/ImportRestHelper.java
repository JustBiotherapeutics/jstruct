package com.just.jstruct.structureImport.rcsbRestImport;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.dao.service.ImportRestService;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_ImportRest;
import com.just.jstruct.utilities.ValueMapUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class ImportRestHelper
{

    public static String DESC_GET_ALL_CURRENT = "All Current Structures";
    public static String DESC_GET_ALL_OBSOLETE = "All Obsolete Structures";

    public static String DESC_GET_RELEASED = "Released Structures";
    public static String DESC_GET_REVISED = "Revised Structures";
    public static String DESC_GET_OBSOLETED = "Obsoleted Structures";

    public static String DESC_GET_LIST = "List of Structures";
    public static String DESC_GET_SINGLE = "Single Structure";



    public static List<Jstruct_ImportRest> importRestsForAllPdbs(Jstruct_ImportJob importJob) throws JException
    {
        List<Jstruct_ImportRest> importRests = new ArrayList<>(2);
        importRests.add(importRestForAllObsoletePdbs(importJob));
        importRests.add(importRestForAllCurrentPdbs(importJob));
        return importRests;
    }


    /**
     * returns a ImportRest object containing all CURRENT PDB Identifiers - /pdb/rest/getCurrent
     * @param importJob
     * @return
     * @throws JException
     */
    public static Jstruct_ImportRest importRestForAllCurrentPdbs(Jstruct_ImportJob importJob) throws JException
    {
        ImportRestService importRestService = new ImportRestService();
        RcsbRestHelper rcsbRestHelper = new RcsbRestHelper();
        
        Jstruct_ImportRest importRest = new Jstruct_ImportRest();
        importRest.setImportJobId(importJob.getImportJobId());
        importRest.setPdbRestUrl(RcsbRestHelper.RCSB_PDB_REST_ALL_CURRENT);
        importRest.setSearchDesc(DESC_GET_ALL_CURRENT);
        importRest.setSearchDateStart(new Date());
        importRest.setSearchDateEnd(new Date());

        try
        {
            List<String> pdbIds = rcsbRestHelper.queryForAllCurrentPdbIds();
            if (CollectionUtil.hasValues(pdbIds))
            {
                for (String pdbId : pdbIds)
                {
                    importRest.addImportPdb(pdbId);
                }
                importRest.setResponsePdbCount(Long.valueOf(pdbIds.size()));
            } 
            else
            {
                importRest.setResponsePdbCount(0L);
            }
            importRestService.add(importRest);

        }  
        catch (JDAOException ex)
        {
            throw new JException("Error creating/saving Import REST data (" + DESC_GET_ALL_CURRENT + ") for Import Job: " + importJob.getImportJobId(), ex);
        }

        return importRest;
    }


    /**
     * returns a ImportRest object containing all OBSOLETE PDB Identifiers - /pdb/rest/getObsolete
     * @param importJob
     * @return
     * @throws JException
     */
    public static Jstruct_ImportRest importRestForAllObsoletePdbs(Jstruct_ImportJob importJob) throws JException
    {
        ImportRestService importRestService = new ImportRestService();
        RcsbRestHelper rcsbRestHelper = new RcsbRestHelper();
        
        Jstruct_ImportRest importRest = new Jstruct_ImportRest();
        importRest.setImportJobId(importJob.getImportJobId());
        importRest.setPdbRestUrl(RcsbRestHelper.RCSB_PDB_REST_ALL_OBSOLETE);
        importRest.setSearchDesc(DESC_GET_ALL_OBSOLETE);
        importRest.setSearchDateStart(new Date());
        importRest.setSearchDateEnd(new Date());
       
        try
        {
            List<String> pdbIds = rcsbRestHelper.queryForAllObsoletePdbIds();
            if(CollectionUtil.hasValues(pdbIds))
            {
                for(String pdbId : pdbIds)
                {
                    importRest.addImportPdb(pdbId);
                }
                importRest.setResponsePdbCount(Long.valueOf(pdbIds.size()));
            }
            else
            {
                importRest.setResponsePdbCount(0L);
            }
            importRestService.add(importRest);

        }  
        catch (JDAOException ex)
        {
            throw new JException("Error creating/saving Import REST data (" + DESC_GET_ALL_OBSOLETE+ ") for Import Job: " + importJob.getImportJobId(), ex);
        }

        return importRest;
    }




    public static List<Jstruct_ImportRest> importRestsForReleasedAndRevisedPdbs(Jstruct_ImportJob importJob, Date lastSuccessfulPdbRunDate) throws JException
    {
        List<Jstruct_ImportRest> importRests = new ArrayList<>(3);
        importRests.add(importRestForReleasedPdbs(importJob, lastSuccessfulPdbRunDate));
        importRests.add(importRestForRevisedPdbs(importJob, lastSuccessfulPdbRunDate));
 //       importRests.add(importRestForObsoletePdbs(importJob, lastSuccessfulPdbRunDate));
        return importRests;
    }


    /**
     * returns a ImportRest object containing all PDB Identifiers that have been RELEASED since a particular date
     * @param importJob
     * @param lastSuccessfulPdbRunDate
     * @return
     * @throws JException
     */
    public static Jstruct_ImportRest importRestForReleasedPdbs(Jstruct_ImportJob importJob, Date lastSuccessfulPdbRunDate) throws JException
    {
        ImportRestService importRestService = new ImportRestService();
        RcsbRestHelper rcsbRestHelper = new RcsbRestHelper();
        
        String releaseDateJsonQuery = rcsbRestHelper.createPdbReleaseDateJsonQuery(lastSuccessfulPdbRunDate, null);
        
        Jstruct_ImportRest importRest = new Jstruct_ImportRest();
        importRest.setImportJobId(importJob.getImportJobId());
        importRest.setPdbRestUrl(RcsbRestHelper.RCSB_SEARCH_BASE_URI);
        importRest.setPdbRestCriteria(releaseDateJsonQuery);
        importRest.setSearchDesc(DESC_GET_RELEASED);
        importRest.setSearchDateStart(lastSuccessfulPdbRunDate);
        importRest.setSearchDateEnd(new Date());

        try
        {
            List<String> pdbIds = rcsbRestHelper.queryPdbByJsonQuery(releaseDateJsonQuery);

            if(CollectionUtil.hasValues(pdbIds))
            {
                for(String pdbId : pdbIds)
                {
                    importRest.addImportPdb(pdbId);
                }
                importRest.setResponsePdbCount(Long.valueOf(pdbIds.size()));
            } 
            else
            {
                importRest.setResponsePdbCount(0L);
            }
            importRestService.add(importRest);

        } 
        catch (JDAOException ex)
        {
            throw new JException("Error creating/saving Import REST data (" + DESC_GET_RELEASED + ") for Import Job: " + importJob.getImportJobId(), ex);
        }

        return importRest;

    }


    /**
     * returns a ImportRest object containing all PDB Identifiers that have been REVISED since a particular date
     * @param importJob
     * @param lastSuccessfulPdbRunDate
     * @return
     * @throws JException
     */
    public static Jstruct_ImportRest importRestForRevisedPdbs(Jstruct_ImportJob importJob, Date lastSuccessfulPdbRunDate) throws JException
    {
        ImportRestService importRestService = new ImportRestService();
        RcsbRestHelper rcsbRestHelper = new RcsbRestHelper();
        
        String reviseDateJsonQuery = rcsbRestHelper.createPdbReviseDateJsonQuery(lastSuccessfulPdbRunDate, null);

        Jstruct_ImportRest importRest = new Jstruct_ImportRest();
        importRest.setImportJobId(importJob.getImportJobId());
        importRest.setPdbRestUrl(RcsbRestHelper.RCSB_SEARCH_BASE_URI);
        importRest.setPdbRestCriteria(reviseDateJsonQuery);
        importRest.setSearchDesc(DESC_GET_REVISED);
        importRest.setSearchDateStart(lastSuccessfulPdbRunDate);
        importRest.setSearchDateEnd(new Date());

        try
        {
            List<String> pdbIds = rcsbRestHelper.queryPdbByJsonQuery(reviseDateJsonQuery);

            if(CollectionUtil.hasValues(pdbIds))
            {
                for(String pdbId : pdbIds)
                {
                    importRest.addImportPdb(pdbId);
                }
                importRest.setResponsePdbCount(Long.valueOf(pdbIds.size()));
            }
            else
            {
                importRest.setResponsePdbCount(0L);
            }

            importRestService.add(importRest);

        }
        catch (JDAOException ex)
        {
            throw new JException("Error creating/saving Import REST data (" + DESC_GET_REVISED + ") for Import Job: " + importJob.getImportJobId(), ex);
        }

        return importRest;

    }


    /**
     * returns a ImportRest object containing all PDB Identifiers that have been OBSOLETED since a particular date
     * - currently there is no RCSB PDB REST API query that returns this data.. so we'll manually create the list based
     * - on comparing all Obsolete PDB Identifiers with what's currently in the database
     * @param importJob
     * @param lastSuccessfulPdbRunDate
     * @return
     * @throws JException
     */
    public static Jstruct_ImportRest importRestForObsoletePdbs(Jstruct_ImportJob importJob, Date lastSuccessfulPdbRunDate) throws JException
    {

        Jstruct_ImportRest importRest = new Jstruct_ImportRest();
        importRest.setImportJobId(importJob.getImportJobId());
        importRest.setPdbRestUrl(RcsbRestHelper.RCSB_PDB_REST_ALL_OBSOLETE);
        importRest.setPdbRestCriteria("* custom logic *");
        importRest.setSearchDesc(DESC_GET_OBSOLETED);
        importRest.setSearchDateStart(lastSuccessfulPdbRunDate);
            
        try
        {
            ImportRestService importRestService = new ImportRestService();
            RcsbRestHelper rcsbRestHelper = new RcsbRestHelper();
            JstructUserService jstructUserService = new JstructUserService();
            FullStructureService fullStructureService = new FullStructureService(jstructUserService.getGuestUser());

            List<String> obsoletePdbIds = rcsbRestHelper.queryForAllObsoletePdbIds();
            List<String> obsoleteJstructIds = fullStructureService.getObsoletePdbIdsFromRcsbRun();

            obsoletePdbIds.removeAll(obsoleteJstructIds);

            if(CollectionUtil.hasValues(obsoletePdbIds))
            {
                for(String pdbId : obsoletePdbIds)
                {
                    importRest.addImportPdb(pdbId);
                }
                importRest.setResponsePdbCount(Long.valueOf(obsoletePdbIds.size()));
            }
            else
            {
                importRest.setResponsePdbCount(0L);
            }

            importRestService.add(importRest);

        } 
        catch (JDAOException ex)
        {
            throw new JException("Error creating/saving Import REST data (" + DESC_GET_OBSOLETED + ") for Import Job: " + importJob.getImportJobId(), ex);
        }

        return importRest;

    }


    /**
     *
     * @param importJob
     * @param pdbIdentifiers
     * @return
     * @throws com.just.jstruct.exception.JDAOException
     */
    public static Jstruct_ImportRest createImportRestWithPdbs(Jstruct_ImportJob importJob, String[] pdbIdentifiers) throws JDAOException
    {
        //if obsolete, this call actually returns the current (version) - not what we want - so we'll fake it out and just use the http service instead
        //postXml.appendln(" <orgPdbQuery> ");
        //postXml.appendln("     <queryType>org.pdb.query.simple.StructureIdQuery</queryType> ");
        //postXml.appendln("     <structureIdList>" + pdbIdentifier + "</structureIdList> ");
        //postXml.appendln(" </orgPdbQuery> ");

        ImportRestService importRestService = new ImportRestService();
        
        Jstruct_ImportRest importRest = new Jstruct_ImportRest();
        importRest.setImportJobId(importJob.getImportJobId());
            
        ValueMapUtils vmu = ValueMapUtils.getInstance();

        importRest.setPdbRestUrl(vmu.getRcsbRestFileUrl() + "...");

        importRest.setPdbRestCriteria(StringUtil.join(pdbIdentifiers, ", "));
        importRest.setSearchDesc(DESC_GET_LIST);
        for (String pdbId : pdbIdentifiers)
        {
            importRest.addImportPdb(pdbId);
        }
        importRest.setResponsePdbCount(Long.valueOf(pdbIdentifiers.length));
        importRestService.add(importRest);

        return importRest;
    }


    /*
    public static Jstruct_ImportRest importRestForSinglePdbId(Connection conn, Jstruct_ImportJob importJob, String pdbIdentifier) throws JException {

        Jstruct_ImportRest importRest = new Jstruct_ImportRest(importJob.getId());

        try {

            HashMap<String, String> urls = PdbRestService.createPdbStructureFileLocations(pdbIdentifier);
            importRest.setPdbRestUrl(JStringUtils.valuesOfHashmap(urls, ", "));

            importRest.setPdbRestCriteria(pdbIdentifier);
            importRest.setSearchDesc(DESC_GET_SINGLE);
            importRest.addImportPdb(pdbIdentifier);
            importRest.setResponsePdbCount(1L);
            importRest.save(conn);

        } catch (SQLException sqle) {
            sqle.printStackTrace();
            throw new JException("Error creating/saving Import REST data (" + DESC_GET_SINGLE + ") for Import Job: " + importJob.getId(), sqle);
        }

        return importRest;

    }
    */


}








