package com.just.jstruct.structureImport.rcsbRestImport;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.StructureService;
import com.just.jstruct.enums.JobType;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_Structure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.AuthUtil;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections4.CollectionUtils;


/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class RcsbHarmonySync
{

    private static final Logger LOGGER = Logger.getLogger(AuthUtil.class.getPackage().getName());
    
    
    private Jstruct_User currentUser;
    private final FullStructureService fullStructureService;
    private final RcsbRestHelper rcsbRestHelper = new RcsbRestHelper();
    private final StructureService structureService = new StructureService();



    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################


    public RcsbHarmonySync(Jstruct_User currentUser) throws JDAOException
    {
        this.currentUser = currentUser;
        this.fullStructureService = new FullStructureService(currentUser);
    }



    //###########################################################################
    // PUBLIC METHODS
    //###########################################################################

    //------------------------------------------------------------------------------------------------------------------
    /**
     * This is the primary public method to ensure harmony between RCSB-PDB and JStruct files.
     * IT will compare what RCSB claims to have as both current and obsolete with what JStruct
     * has listed as current and obsolete from RCSB.
     *
     */
    public void syncFilesWithRcsb()  
    {
        Set<String> uniquePdbIdsToHarmonize = null;

        try 
        {
            if(null==currentUser)
            {
                currentUser = JstructUserService.getUserFromCache(Jstruct_User.SYSTEM_UID);
            }

            //get lists of current and obsolete files from JStruct - for PDB Runs only.
            List<String> jstructCurrentFiles = fullStructureService.getActivePdbIdsFromRcsbRun();
            List<String> jstructObsoleteFiles = fullStructureService.getObsoletePdbIdsFromRcsbRun();

            //get lists of current and obsolete files from PDB
            List<String> pdbCurrentFiles = rcsbRestHelper.queryForAllCurrentPdbIds();
            List<String> pdbObsoleteFiles = rcsbRestHelper.queryForAllObsoletePdbIds();

            //create a single unique list of differences
            Collection<String> uniqueCurrent = CollectionUtils.disjunction(pdbCurrentFiles, jstructCurrentFiles);
            Collection<String> uniqueObsolete = CollectionUtils.disjunction(pdbObsoleteFiles, jstructObsoleteFiles);

            uniquePdbIdsToHarmonize = new HashSet<>(uniqueCurrent);
            uniquePdbIdsToHarmonize.addAll(uniqueObsolete);

        } 
        catch (JDAOException | JException ex) 
        {
            //if we catch an error here  log and send email to admin.
            ex.printStackTrace();
            LOGGER.log(Level.SEVERE, "Error syncing (harmonizing) files with RCSB", ex);
            //JEmailUtils.sendImportFatalErrorEmail(ex); todo.
        }



        if(CollectionUtil.hasValues(uniquePdbIdsToHarmonize))
        {
            String[] pdbIds = uniquePdbIdsToHarmonize.toArray(new String[uniquePdbIdsToHarmonize.size()]);

            RcsbImport pdbRestImport = new RcsbImport(currentUser);
            pdbRestImport.processSelectedRcsbFiles(pdbIds, false, JobType.RCSB_HARMONIZE);

        } 
        else 
        {
            System.out.println("*** NO files to harmonize.");
        }




        try
        {
            setStructuresAsObsoleted();
        }
        catch (Exception ex)
        {
            //if we catch an error here  log and send email to admin.
            ex.printStackTrace();
            LOGGER.log(Level.SEVERE, "Error obsoleting removed files", ex);
            //JEmailUtils.sendImportFatalErrorEmail(ex); todo.
        }


    }

    // dec 2020 RCSB now has the consept of 'removed' structures.
    //
    // these appear in the obsoleted list, BUT the structure file
    // does NOT necessarly have the obsoleted line
    //
    // so, after to running this harmonize process, we will set all
    // JSTRUCT obsoleted files to obsoleted just based on the list
    // of obsoleted files returned by PDB
    private void setStructuresAsObsoleted() throws JException, JDAOException
    {
        List<String> pdbObsoleteIds = rcsbRestHelper.queryForAllObsoletePdbIds();

        if(CollectionUtil.hasValues(pdbObsoleteIds))
        {
            List<Jstruct_FullStructure> fullStructures = fullStructureService.getByPdbIds(pdbObsoleteIds, true);

            if(CollectionUtil.hasValues(fullStructures))
            {
                for(Jstruct_FullStructure fullStructure : fullStructures)
                {
                    if(!fullStructure.isObsolete())
                    {
                        Jstruct_Structure structure = structureService.getByStructureId(fullStructure.getStructureId());
                        structureService.setToObsolete(structure);
                        System.out.println("Setting " + fullStructure.getPdbIdentifier() + " to Obsolete.");
                    }
                }
            }

        }


    }


















}
