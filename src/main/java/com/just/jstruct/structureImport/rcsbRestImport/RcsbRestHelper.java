package com.just.jstruct.structureImport.rcsbRestImport;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.exception.JException;
import com.just.jstruct.utilities.JDateUtil;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * updated (Dec 2020) RCSB PDB Search API
 *
 * overview:                https://www.rcsb.org/docs/programmatic-access/web-services-overview
 *
 * SearchDocumentation:     https://search.rcsb.org/
 *
 * Migration guide:         https://data.rcsb.org/migration-guide.html
 *
 * RCSB PDB: Search API Documentation
 *
 *      Search API:         https://search.rcsb.org/redoc/index.html
 *      Search Attributes:  https://search.rcsb.org/search-attributes.html
 *      Search Editor:      https://search.rcsb.org/query-editor.html
 *
 */
public class RcsbRestHelper
{

    // ------------------------------------------------------------------------------------------------------
    // data members
    // ------------------------------------------------------------------------------------------------------


    public static final String RCSB_PDB_REST_ALL_CURRENT ="https://data.rcsb.org/rest/v1/holdings/current/entry_ids";
    public static final String RCSB_PDB_REST_ALL_OBSOLETE ="https://data.rcsb.org/rest/v1/holdings/removed/entry_ids";

    // https://search.rcsb.org/#search-api
    public static final String RCSB_SEARCH_BASE_URI = "https://search.rcsb.org/rcsbsearch/v2/query"; //?json={search-request}
    // example:
    //         https://search.rcsb.org/rcsbsearch/v2/query?json={"query":{"type":"terminal","service":"text"},"return_type":"entry"}


    // https://search.rcsb.org/search-attributes.html
    public static final String RCSB_PDB_ATTR_RELEASE_DATE ="rcsb_accession_info.initial_release_date";
    public static final String RCSB_PDB_ATTR_REVISION_DATE ="rcsb_accession_info.revision_date";


    private final Gson gson;



    // ------------------------------------------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------------------------------------------


    public RcsbRestHelper()
    {
        gson = new GsonBuilder()
                .setDateFormat("YYYY-MM-dd'T'HH:mm:ss'Z'")  // https://search.rcsb.org/#field-queries
                .setPrettyPrinting()
                .create();
    }




    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------
    // PUBLIC METHODS
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------


    /**
     * find ALL Current (aka Active) PDB Identifiers
     * @return
     * @throws JException
     */
    public List<String> queryForAllCurrentPdbIds() throws JException
    {
        return pdbIdsByEndpoint(RCSB_PDB_REST_ALL_CURRENT);
    }


    /**
     * find ALL Obsolete PDB Identifiers
     * @return
     * @throws JException
     */
    public List<String> queryForAllObsoletePdbIds() throws JException
    {
        return pdbIdsByEndpoint(RCSB_PDB_REST_ALL_OBSOLETE);
    }




    /**
     * pass in a json query to retrieve a list of PDB IDs
     * valid queries:
     *      createPdbReleaseDateJsonQuery
     *      createPdbReviseDateJsonQuery
     * @param query
     * @return
     * @throws JException
     */
    public List<String> queryPdbByJsonQuery(String query) throws JException
    {
        //System.out.println("query   : " + query);

        String queryEncoded = "";
        String fullUrl = "";

        List<String> parsedPdbIds = null;

        try
        {
            //encode the query; create the full URL
            queryEncoded = URLEncoder.encode(query, "UTF-8");
            fullUrl = RCSB_SEARCH_BASE_URI + "?json=" + queryEncoded;
            URL u = new URL(fullUrl);

            //retrieve results from RCSB PDB
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(u.openStream()));

            //pull out all the PDB IDs
            PdbResultsRoot pdbResultsRoot = gson.fromJson(bufferedReader, PdbResultsRoot.class);

            if(pdbResultsRoot != null && CollectionUtil.hasValues(pdbResultsRoot.result_set))
            {
                parsedPdbIds = new ArrayList<>(pdbResultsRoot.total_count);

                for(PdbResultSet pdbResult : pdbResultsRoot.result_set)
                {
                    parsedPdbIds.add(pdbResult.identifier);
                }
            }

        }
        catch (Exception e)
        {
            System.out.println("query   : " + query);
            System.out.println("queryEncoded  : " + queryEncoded);
            System.out.println("fullUrl : " + fullUrl);

            e.printStackTrace();
            throw new JException("Error retrieving Structure IDs from: '" + query, e);
        }

        return parsedPdbIds;

    }



    /**
     * JSON to query for PDB IDs by Release date
     * @param fromDate
     * @param toDate
     * @return
     */
    public String createPdbReleaseDateJsonQuery(Date fromDate, Date toDate)
    {
        return createPdbDateJsonQuery(RCSB_PDB_ATTR_RELEASE_DATE, fromDate, toDate);
    }


    /**
     * JSON to query for PDB IDs by Revise date
     * @param fromDate
     * @param toDate
     * @return
     */
    public String createPdbReviseDateJsonQuery(Date fromDate, Date toDate)
    {
        return createPdbDateJsonQuery(RCSB_PDB_ATTR_REVISION_DATE, fromDate, toDate);
    }










    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------
    // PRIVATE METHODS
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------



    /**
     *
     * @param paramAttr attribute parameter
     *                             this:     RCSB_PDB_ATTR_RELEASE_DATE ="rcsb_accession_info.initial_release_date"
     *                             or this:  RCSB_PDB_ATTR_REVISION_DATE ="rcsb_accession_info.revision_date"
     * @param fromDate
     * @param toDate
     * @return
     */
    private String createPdbDateJsonQuery(String paramAttr, Date fromDate, Date toDate)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("{");
        sb.append("    \"query\":");
        sb.append("    {");
        sb.append("        \"type\": \"terminal\",");

        sb.append("        \"service\": \"text\",");

        if(fromDate != null || toDate != null)
        {
            sb.append("   \"parameters\":");
            sb.append("   {");
            sb.append("       \"attribute\": \"").append(paramAttr).append("\",");

            if(toDate == null && fromDate != null)
            {
                sb.append("       \"operator\": \"greater_or_equal\",");
                sb.append("       \"value\": ").append(dateOnlyJson(fromDate)).append("");
            }
            else if (fromDate == null && toDate != null)
            {
                sb.append("       \"operator\": \"less_or_equal\",");
                sb.append("       \"value\": ").append(dateOnlyJson(toDate)).append("");
            }
            else
            {
                sb.append("       \"operator\": \"range\",");
                sb.append("       \"value\":");
                sb.append("       {");
                sb.append("           \"from\": ").append(dateOnlyJson(fromDate)).append(",");
                sb.append("           \"include_lower\": true,");
                sb.append("           \"to\": ").append(dateOnlyJson(toDate)).append(",");
                sb.append("           \"include_upper\": true");
                sb.append("       }");
            }

            sb.append("   }");
        }

        sb.append("   },");

        sb.append("   \"request_options\":");
        sb.append("   {");
        sb.append("       \"return_all_hits\": true");
        sb.append("   },");

        sb.append("    \"return_type\": \"entry\"");
        sb.append("}");

        return sb.toString().replaceAll(" ", "");
    }

    /**
     * see constructor for setting the date format
     * @param d
     * @return
     */
    private String dateOnlyJson(Date d)
    {
        if(d != null)
        {
            return gson.toJson(JDateUtil.removeTimeFromDate(d));
        }
        return "";
    }





    /**
     * call an endpoint from the RESTful RCSB web service
     * this is specific to getting all current or obsolete PDB ids from the urls:
     *      https://data.rcsb.org/rest/v1/holdings/current/entry_ids
     *      https://data.rcsb.org/rest/v1/holdings/removed/entry_ids
     *
     * todo speed up parsing the results.
     *
     * @param getUrl
     * @return a list of PDB ids.
     */
    private static List<String> pdbIdsByEndpoint(String entryIdsUrl) throws JException
    {
        List<String> parsedPdbIds = new ArrayList<>(9999);

        try
        {
            URL u = new URL(entryIdsUrl);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(u.openStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                //there's really just one very long line of PDB ids returned from the call
                //format: ["ABCD","EFGH",..."WXYZ"]

                if (line.startsWith("["))
                {
                    //remove first2 and last2 charaters: [" and "]
                    line = line.substring(2, line.length() - 2 );

                    //split the line on: ","
                    String[] split = line.split("\",\"");
                    parsedPdbIds.addAll(Arrays.asList(split));

                }
                else
                {
                    throw new JException("Results returned by " + entryIdsUrl + " are not formatted as expected: [\"ABCD\",\"EFGH\",...\"WXYZ\"]");
                }

            }
        }
        catch (IOException ioe)
        {
            throw new JException("Error retrieving Structure IDs from: '" + entryIdsUrl, ioe);
        }

        return parsedPdbIds;

    }
    






    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------
    // CLASSES TO SUPPORT JSON RESULTS
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------

    private class PdbResultsRoot
    {
        public String query_id;
        public String result_type;
        public int total_count;
        public List<PdbResultSet> result_set;
    }

    private class PdbResultSet
    {
        public String identifier;
    }

















}



