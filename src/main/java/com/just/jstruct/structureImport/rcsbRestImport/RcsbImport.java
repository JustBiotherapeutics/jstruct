package com.just.jstruct.structureImport.rcsbRestImport;

import com.hfg.util.FileUtil;
import com.hfg.util.StackTraceUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.Structure;
import com.just.bio.structure.pdb.PdbFileParse;
import com.just.bio.structure.services.RcsbRestService;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.dao.service.ImportJobService;
import com.just.jstruct.dao.service.ImportPdbService;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.StructFileService;
import com.just.jstruct.enums.JobStatus;
import com.just.jstruct.enums.JobType;
import com.just.jstruct.enums.ProcessChainStatus;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.exception.JImportException;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_ImportPdb;
import com.just.jstruct.model.Jstruct_ImportRest;
import com.just.jstruct.model.Jstruct_StructFile;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureImport.ImportService;
import com.just.jstruct.structureImport.JImportUtils;
import com.just.jstruct.utilities.AuthUtil;
import com.just.jstruct.utilities.JDateUtil;
import com.just.jstruct.utilities.JEmailUtil;
import com.just.jstruct.utilities.JFileUtils;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class RcsbImport {

    
    
    private static final Logger LOGGER = Logger.getLogger(AuthUtil.class.getPackage().getName());
    

    private Jstruct_User currentUser;



    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################


    public RcsbImport(Jstruct_User inUser) {
        init(null, inUser);
    }

    public RcsbImport(Long inUserId) {
        init(inUserId, null);
    }

    public RcsbImport() {
        init(null, null);
    }



    private void init(Long inUserId, Jstruct_User inUser)  {

        if(null != inUser) {
            //if a user is passed in, use it. done.
            currentUser = inUser;
        } else if (null != inUserId) {
            try {
                currentUser = JstructUserService.getUserFromCache(inUserId);
            } catch (JDAOException ex) {
                //nothing. user will remain null for now.. catch it in the processing (todo - fix this bad programming)
            }

        } else {
            //else we get system user
            try {
                currentUser = JstructUserService.getUserFromCache(Jstruct_User.SYSTEM_UID);
            } catch (JDAOException ex) {
                //nothing. user will remain null for now.. catch it in the processing (todo - fix this bad programming)
            } 

        }


    }





    //###########################################################################
    // PUBLIC METHODS
    //###########################################################################

    //------------------------------------------------------------------------------------------------------------------
    /**
     * This is the primary public method to initiate an RCSB import job
     *
     * it will perform an INITIAL_RCSB_RUN if it has never been successful before, else it will
     * will perform a INCREMENTAL_RCSB_RUN based on the last successful INITIAL/INCREMENTAL run
     *
     * @param jobType
     */
    public void processAllRcsbFileChanges(JobType jobType)  
    {
        ImportJobService importJobService = new ImportJobService();
        
        List<Jstruct_ImportRest> importRests = null;
        boolean skipExisting = false;

        //setup for some of the jobtypes
        try 
        {

            Jstruct_ImportJob importJob = new Jstruct_ImportJob(currentUser, jobType, JobStatus.QUERYING);
            importJobService.add(importJob); 

            switch (jobType) 
            {
                case INITIAL_RCSB_RUN:
                case INITIAL_RCSB_RERUN:

                    importRests = ImportRestHelper.importRestsForAllPdbs(importJob);
                    skipExisting = false;
                    
                    break;

                case INITIAL_RCSB_RUN_RECOVER:

                    importRests = ImportRestHelper.importRestsForAllPdbs(importJob);
                    skipExisting = true;
                    
                    break;

                case INCREMENTAL_RCSB_RUN:

                    Jstruct_ImportJob lastSuccessfulRcsbRun = getMostRecentSuccessfulRcsbRun();

                    if(null == lastSuccessfulRcsbRun)
                    {
                        //called to run an incremental, but there is no most recent successful run, so make the parameters similar to an initial recover run
                        importRests = ImportRestHelper.importRestsForAllPdbs(importJob);
                        skipExisting = true;
                    }
                    else 
                    {
                        //'normal' incremental run - just get changes since last successful run
                        Date lastSuccessfulRcsbRunDate = lastSuccessfulRcsbRun.getRunStart();
                        importRests = ImportRestHelper.importRestsForReleasedAndRevisedPdbs(importJob, lastSuccessfulRcsbRunDate);
                        skipExisting = false;
                    }

                    break;
                    
                default:
                    throw new InputMismatchException("Invalid JobType for processAllRcsbFileChanges: " + jobType.name());
                    
            }

            importJob.setImportStatus(JobStatus.IMPORTING);
            importJobService.update(importJob);



            

            //import all files (released and revisioned in this time-range - if it's the first time run it will process all current/obsolete files)
            List<Exception> exceptions = importProcessRcsbFiles(importJob, importRests, skipExisting);

            //update the import_job record with SUCCESS status, then send completed success email
            updateImportJobSuccessful(importJob);
            JEmailUtil.sendJobCompleteEmail(importJob, importRests, currentUser, null, exceptions); //complete, and successful :)

            //run the harmonize job after an incremental is completed
            if (jobType.equals(JobType.INCREMENTAL_RCSB_RUN)) 
            {
                RcsbHarmonySync rcsbHarmonySync = new RcsbHarmonySync(currentUser);
                rcsbHarmonySync.syncFilesWithRcsb();
            } 
                        
                
            
            
        } 
        catch (JDAOException | JException | InputMismatchException ex) 
        {
            //if we catch an error here, log and send email to admin.
            ex.printStackTrace();
            LOGGER.log(Level.SEVERE, "Error processing RCSB file changes", ex);
            JEmailUtil.sendJobErrorEmail(ex);
        }


    }

    





    /**
     * 
     * @param pdbIdentifiers
     * @param skipExisting
     * @param importJobType 
     */
    public void processSelectedRcsbFiles(String[] pdbIdentifiers, boolean skipExisting, JobType importJobType)
    {
        // ---------------------------------------------------------------------
        // prep
        ImportJobService importJobService = new ImportJobService();
        
        Jstruct_ImportJob importJob = null;
        List<Jstruct_ImportRest> importRests = new ArrayList<>(1);

        
        // ---------------------------------------------------------------------
        // create the 'ImportJob' and 'ImportRest' pojo for this process
        try
        {
            importJob = new Jstruct_ImportJob(currentUser, importJobType, JobStatus.IMPORTING);
            importJobService.add(importJob);
            
            Jstruct_ImportRest importRest = ImportRestHelper.createImportRestWithPdbs(importJob, pdbIdentifiers);

            importRest.setImportJobId(importJob.getImportJobId()); //redundant, eh.
            importRests.add(importRest);

        } 
        catch (JDAOException ex)
        {
            //if we catch an error here log and send email to admin.
            ex.printStackTrace();
            LOGGER.log(Level.SEVERE, "Error processing selected PDB file changes", ex);
            JEmailUtil.sendJobErrorEmail(ex);
        } 


        // ---------------------------------------------------------------------
        // enter or update all the PDB files identified in the ImportRest
        try 
        {
            //import
            List<Exception> exceptions = importProcessRcsbFiles(importJob, importRests, skipExisting);

            //update the import_job record with SUCCESS status, then send completed success email
            updateImportJobSuccessful(importJob);
            JEmailUtil.sendJobCompleteEmail(importJob, importRests, currentUser, null, exceptions); //complete, and successful :)

        } 
        catch (JDAOException | JException ex)
        {
            //if we catch an error here  log and send email to admin.
            ex.printStackTrace();
            LOGGER.log(Level.SEVERE, "Error processing selected PDB file changes", ex);
            JEmailUtil.sendJobErrorEmail(ex);
        }

    }






    //###########################################################################
    // PRIVATE METHODS
    //###########################################################################

    

    /**
     * based on the PDB Identifiers within the importRests objects, import them 
     * all into the database (adding/updating all relevant objects)
     *
     * @param importJob
     * @param importRests
     * @return
     * @throws JException
     */
    private List<Exception> importProcessRcsbFiles(Jstruct_ImportJob importJob, List<Jstruct_ImportRest> importRests, boolean skipExisting)
            throws JException
    {
        List<Exception> exceptions = new ArrayList<>();

        try 
        {
            // ----------------------------------------------------------------------------------------------------
            //create a unique list of pdb identifiers based on the lists passed in
            Set<String> uniquePdbIdentifiers = combinePdbIdentifiersIntoUniqueList(importJob, importRests, skipExisting);


            // ----------------------------------------------------------------------------------------------------
            //process all the PDB identifiers that were identified in our initial REST calls

            int index = 0; 
            if (CollectionUtil.hasValues(uniquePdbIdentifiers)) 
            {

                int loopErrorCount = 0;
                int loopErrorMax = JImportUtils.maxErrorsBeforeImportFail(uniquePdbIdentifiers.size(), 10);

                for (String pdbIdentifier : uniquePdbIdentifiers) 
                {

                    System.out.println("pdbIdentifier: " + pdbIdentifier + "  -  " + new Date());

                    // todo FOR TESTING ONLY ******************************************************************************
                    // ****************************************************************************************************
                    //if(!pdbIdentifier.startsWith("6A5")){ // && !pdbIdentifier.startsWith("3TG")) { //1VY
                    //if(!pdbIdentifier.equals("2D15")){
                    //    if (!ValueMapUtils.getInstance().getAppInstance().equalsIgnoreCase("production")) { //just to ensure this doesn't make it into prod...
                    //        System.out.println(" * skipping: " + pdbIdentifier);
                    //        continue;
                    //    }
                    //}
                    // ****************************************************************************************************
                    // ****************************************************************************************************
                   

                    try 
                    {
                        // ----------------------------------------------------------------------------------------------------
                        //process this PDB Identifier
                        //  - retrieve file from PDB REST API
                        //  - create and save all appropriate database objects (insert or update as applicable)
                        //  - will also delete the local temporary File created by the process
                        processPdbIdentifierIntoStructFile(importJob, pdbIdentifier);

                        updateImportJobCountImported(importJob, ++index);

                    } 
                    catch (JImportException jie) 
                    {
                        System.out.println(": " + jie.getMessage());
                        loopErrorCount++;
                        exceptions.add(jie);

                        if(loopErrorCount > loopErrorMax)
                        {
                            //if there are too many errors from the loops we may need to kill the import and send a fail message
                            throw new JException("Too many file errors have occurred during the import process.(Max errors allowd for this run: "+loopErrorMax+")");
                        }
                    }

                }  //END OF LOOP FOR SINGLE PDB ID
            }

            //update the file_import_job record import completed timestamp
            updateImportJobImportDate(importJob);


        } 
        catch (JDAOException ex) 
        {
            uhOhJobImportFailedSadFace(importJob, ex);
            JEmailUtil.sendJobCompleteEmail(importJob, importRests, currentUser, ex, exceptions); //complete, but failed :(
        }

        return exceptions;
    }
    
    
    
    
    /**
     * update the import job to successful, and add the run-end date
     * @return
     * @throws SQLException
     */
    private Jstruct_ImportJob getMostRecentSuccessfulRcsbRun() throws JDAOException {
        ImportJobService importJobService = new ImportJobService();
        Jstruct_ImportJob lastSuccessfulPdbRun = importJobService.getMostRecentSuccessfulRcsbRun();
        return lastSuccessfulPdbRun;
    }



    

    /**
     * 
     * @param importJob
     * @param ex
     * @throws JException 
     */
    private void uhOhJobImportFailedSadFace (Jstruct_ImportJob importJob, Exception ex) throws JException {
        try {
            
            ImportJobService importJobService = new ImportJobService();
          
            importJob.setFailureMessage("ERROR MESSAGE: " + ex.getMessage() + " \n" + "STACK TRACE: \n" + StackTraceUtil.getExceptionStackTrace(ex));
            importJob.setRunEnd(new Date());
            importJob.setImportStatus(JobStatus.FAILED);
            importJobService.update(importJob);

        } catch (JDAOException jdaoex) {
            throw new JException("Database Error attempting to set Import Job to FAILED", jdaoex);
        } 
    }



    /**
     * process a single pdb identifier and create/updated the appropriate entities (Structure, Struct_Version, Struct_File, PDB_File, PDB_*...)
     * @param importJob
     * @param pdbIdentifier
     * @return
     */
    private void processPdbIdentifierIntoStructFile(Jstruct_ImportJob importJob, String pdbIdentifier) throws JImportException 
    {
        try 
        {
            // -----------------------------------------------------------------
            // prepare services and stuff

            Jstruct_User systemUser = JstructUserService.getUserFromCache(Jstruct_User.SYSTEM_UID);

            String rcsbRestFileUrl = ValueMapUtils.getInstance().getRcsbRestFileUrl();
            String rcsbRestDownloadUrl = ValueMapUtils.getInstance().getRcsbRestDownloadUrl();
            PdbFileParse pdbFileParse = new PdbFileParse();

            RcsbRestService rcsbRestService = RcsbRestService.getInstance(rcsbRestFileUrl, rcsbRestDownloadUrl);
            StructFileService structFileService = new StructFileService();


            // -----------------------------------------------------------------
            //based on a PDB Identifier retrieve the file, and parse all the content (uses RCSB REST APIs)
            //this currently will parse a PDB -or- CIF file into our pdb format

            Structure structureObj;

            //check that the file exists in RCSB before attempting to download
            if(rcsbRestService.pdbFileExistsInRcsb(pdbIdentifier))
            {
                //file exists in RCSB
                structureObj = pdbFileParse.executeParseByRestServices(pdbIdentifier, rcsbRestFileUrl, rcsbRestDownloadUrl);
            }
            else
            {
                //file does not exist in RCSB - create a fake one
                File file = createFileStub(pdbIdentifier);
                structureObj = pdbFileParse.executeParseByLocalFile(file);
            }

            
            Jstruct_StructFile incomingJstructFile = new Jstruct_StructFile(systemUser, structureObj);
            Jstruct_StructFile existingJstructFile = structFileService.getPdbImportedRowByPdbIdentifier(incomingJstructFile.getPdbIdentifier(), currentUser);

            //check if file exists in the database
            if (null != existingJstructFile) 
            {
                // struct file already exists in DB, based on PDB Identifier - this means we need to update the existing
                ImportService.updateExistingRcsbRunStructureStuff(importJob, existingJstructFile, incomingJstructFile, structureObj, systemUser);
                importJob.setCountUpdated(importJob.getCountUpdated() + 1);

            } 
            else 
            {
                //upload NEW file and structure/version data and pdb file
                ImportService.insertNewRcsbRunStructureStuff(importJob, incomingJstructFile, structureObj, systemUser);
                importJob.setCountAdded(importJob.getCountAdded() + 1);
                existingJstructFile = incomingJstructFile; //make existing be the primary file we're dealin' with
            }

            //now that the structFile has been inserted or updated, save the actual file content to the database
            structFileService.saveDataFromFile(structureObj.getLocalFile(), existingJstructFile.getStructVerId());

            //next up is to process the chains/sequences. the Structure object contains Chain/Residue/Atom data
            JImportUtils.updateChainsForStructure(structureObj, existingJstructFile.getStructVerId());

            existingJstructFile.setProcessChainStatus(ProcessChainStatus.COMPLETE);

            //commit this transaction
            //conn.commit();

            //once everything is persisted and committed, delete the local (temporary) file
            structureObj.getLocalFile().delete();

        }

        //catch (JDAOException | StructureParseException | JImportException ex) {
        catch (Throwable ex)
        {
            //if any errors happen within a loop it's probably specific to that file.. we'll catch it here, log it, and continue with the import

            ex.printStackTrace();
            try
            {
                ImportService.persistImportWarning(importJob, currentUser, pdbIdentifier, "File error during import: " + ex.getMessage(), ex);
            }
            catch (JDAOException jdaoex)
            {
                 throw new JImportException(jdaoex);
            }

            throw new JImportException(ex);
        }


    }

    private File createFileStub(String pdbIdentifier) throws IOException
    {
        String tmpStubDir = "/web/tmp/jstruct_stubs";
        JFileUtils.createDirectoryIfNotAlready(tmpStubDir);

        String d = JDateUtil.formatDate(new Date(), "dd-MMM-yy").toUpperCase();

        File file = new File(tmpStubDir + "/stub_" + pdbIdentifier + ".pdb");

     // "HEADER    classification                          date.....   xxxx              "
     // "OBSLTE     DD-MMM-YY xxxx      xxxx                                             "
     // "TITLE     title                                                                 "

        StringBuilder sb = new StringBuilder();
        sb.append("HEADER    unknown                                 " + d + "   " + pdbIdentifier + "              \n");
        sb.append("OBSLTE     " + d + "                                                            \n");
        sb.append("TITLE     https://www.rcsb.org/structure/removed/" + pdbIdentifier + " \n");

        FileUtil.write(file, sb.toString());

        return file;
    }




    /**
     * pdbIdentifiers in the importRests may have some redundancy. (if a file was released AND revisioned in the timeperiod)
     * this will merge the pdb lists in the importRests list into a unique list
     *
     * @param inImportJob
     * @param inImportRests
     * @return
     * @throws JDAOException
     */
    private Set<String> combinePdbIdentifiersIntoUniqueList(Jstruct_ImportJob inImportJob, List<Jstruct_ImportRest> inImportRests, boolean skipExisting) throws JDAOException {

        ImportPdbService importPdbService = new ImportPdbService();
        
        Set<String> uniquePdbIdentifiers = new HashSet<>();

        for(Jstruct_ImportRest importRest : inImportRests){
            List<Jstruct_ImportPdb> importPdbs = importPdbService.getByImportRestId(importRest.getImportRestId());
            if(CollectionUtil.hasValues(importPdbs)) {
                for (Jstruct_ImportPdb importPdb : importPdbs) {
                    uniquePdbIdentifiers.add(importPdb.getPdbIdentifier().toUpperCase());
                }
            }
        }

        //if we are going to skip existing files for this import (done when 'recovering' a initial import)
        if(skipExisting){
            
            FullStructureService fullStructureService = new FullStructureService(currentUser);
            
            List<String> allPdbIds = fullStructureService.getAllPdbIdsFromRcsbRun();
            uniquePdbIdentifiers.removeAll(allPdbIds);
        }


        updateImportJobUniqueSourceCount(inImportJob, uniquePdbIdentifiers.size());

        return uniquePdbIdentifiers;

    }


    
    
    
    /**
     * update a Jstruct_ImportJob as successful
     * @param inImportJob
     * @throws JDAOException 
     */
    private void updateImportJobSuccessful(Jstruct_ImportJob inImportJob) throws JDAOException {
        ImportJobService importJobService = new ImportJobService();
        inImportJob.setRunEnd(new Date());
        inImportJob.setImportStatus(JobStatus.SUCCESS);
        importJobService.update(inImportJob);
    }
    
    
    

    /**
     *
     * @param inImportJob
     * @param uniqueSourceCount
     * @throws SQLException
     */
    private void updateImportJobUniqueSourceCount(Jstruct_ImportJob inImportJob, Integer uniqueSourceCount) throws JDAOException {
        
        ImportJobService importJobService = new ImportJobService();
        
        inImportJob.setCountUniqueSource(uniqueSourceCount);
        importJobService.update(inImportJob);
        
    }


    /**
     *
     * @param inImportJob
     * @param inImportDate
     * @throws SQLException
     */
    private void updateImportJobImportDate(Jstruct_ImportJob inImportJob) throws JDAOException {
        
        ImportJobService importJobService = new ImportJobService();
        
        inImportJob.setImportEnd(new Date());
        importJobService.update(inImportJob);
        
    }


    /**
     * 
     * @param inImportJob
     * @param inImportCount
     * @throws JDAOException 
     */
    private void updateImportJobCountImported(Jstruct_ImportJob inImportJob, int inImportCount) throws JDAOException {
        
        ImportJobService importJobService = new ImportJobService();
        
        inImportJob.setCountImported(inImportCount);
        importJobService.update(inImportJob);
        
    }









}



