package com.just.jstruct.structureImport;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.AtomGroup;
import com.just.bio.structure.Chain;
import com.just.bio.structure.Structure;
import com.just.bio.structure.enums.ChainType;
import com.just.bio.structure.pdb.StructureUtils;
import com.just.jstruct.dao.service.ChainService;
import com.just.jstruct.dao.service.StructFileService;
import com.just.jstruct.dao.service.StructVersionService;
import com.just.jstruct.dao.service.StructureService;
import com.just.jstruct.enums.ProcessChainStatus;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.exception.JImportException;
import com.just.jstruct.model.Jstruct_Chain;
import com.just.jstruct.model.Jstruct_StructFile;
import com.just.jstruct.model.Jstruct_StructVersion;
import com.just.jstruct.model.Jstruct_Structure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.JDateUtil;
import com.just.jstruct.utilities.JFileUtils;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;


/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JImportUtils {




    public static Jstruct_StructFile createStructFileFromFile(File inFile, Jstruct_StructVersion inStructVersion, Structure structureObj, Jstruct_User user) 
            throws JDAOException, JException {
        
        StructFileService structFileService = new StructFileService();

        Jstruct_StructFile structFile;

        try {
            Path filePath = Paths.get(inFile.getPath());
            BasicFileAttributes attr = Files.getFileAttributeView(filePath, BasicFileAttributeView.class).readAttributes();

            String uploadFileName = JFileUtils.getFileName(inFile);

            structFile = new Jstruct_StructFile(user);

            String pdbIdentifier = StructureUtils.parsePdbIdentifierFromFileName(uploadFileName);
            if(!StringUtil.isSet(pdbIdentifier)) {
                //sometimes the PDB file name is formatted wrong, in that case attempt get the pdb identifier from the file itself
                if (structureObj.getMetadata() != null) {
                    pdbIdentifier = structureObj.getMetadata().getPdbIdentifier();
                }
            }

            structFile.setPdbIdentifier(pdbIdentifier);
            structFile.setFromLocation(filePath.toRealPath().toString());
            structFile.setUploadFileName(uploadFileName);
            structFile.setUploadFileExt(JFileUtils.getExtension(uploadFileName));
            structFile.setActualFileName(JFileUtils.getUnGzippedFileName(inFile));
            structFile.setActualFileExt(JFileUtils.getExtensionWithoutGz(structFile.getActualFileName()));

            structFile.setFileCreationDate(JDateUtil.filetimeToDate(attr.creationTime()));
            structFile.setFileModifiedDate(JDateUtil.filetimeToDate(attr.lastModifiedTime()));

            structFile.setStructVerId(inStructVersion.getStructVerId());

            structFile.setUncompressedSize(structureObj.getUncompressedSizeInBytes());
            structFile.setChecksum(structureObj.getMd5Checksum());

            structFile.setProcessChainStatus(ProcessChainStatus.TODO);
            

            //1) persist the brand new structFile, 2) save the binary file contents/data, 3) and then  all the chains
            structFileService.add(structFile);
            structFileService.saveDataFromFile(structureObj.getLocalFile(), structFile.getStructVerId());
            JImportUtils.updateChainsForStructure(structureObj, structFile.getStructVerId());


        } catch (IOException ex) {
            throw new JDAOException("Error during processing of Structure File! (manual import process)", ex);
        }

        return structFile;
    }







    /**
     * @param inStructureObj
     * @param inStructVerId
     * @throws JDAOException
     */
    public static void updateChainsForStructure(Structure inStructureObj, Long inStructVerId) throws JDAOException {
        
        ChainService chainService = new ChainService();

        //first, delete any existing chains
        chainService.deleteByStructVerId(inStructVerId);

        //create new chains - along with only the atom grouping(s) we want to keep - and save 'em
        if(CollectionUtil.hasValues(inStructureObj.getAtomicCoordinates().getModels())){
            
            List<Jstruct_Chain> newChains = new ArrayList<>();
            
            for(Chain chainObj : inStructureObj.getAtomicCoordinates().getModels().get(0).getChains()){
                
                //remove this line if you wanna save ALL atom groupings of a chain.. for now, we're filtering some out.
                chainObj.setAtomGroups(filterAtomGroupings(chainObj.getAtomGroups()));

                if(CollectionUtil.hasValues(chainObj.getAtomGroups())) {         //note: if there are no PROTEIN or NUCLEIC_ATOM atom groups, don't even bother saving the chain.
                    Jstruct_Chain chain = new Jstruct_Chain(chainObj, inStructVerId);
                    newChains.add(chain);
                }
            }
            
            //persist all the chains.
             if(CollectionUtil.hasValues(newChains)){
                chainService.insert(newChains);
            }
        }
        
    }


    //list of chainTypes to save. others are ignored
    private final static List<ChainType> CHAIN_TYPES_TO_SAVE = new ArrayList<>(2);
    static{
        CHAIN_TYPES_TO_SAVE.add(ChainType.PROTEIN);
        CHAIN_TYPES_TO_SAVE.add(ChainType.NUCLEIC_ACID);
    }

    // prior to updating the chains, we need to filter some of the atom groupings
    // logic to filter atom groupings:
    //       - only include types: PROTEIN or NUCLEIC_ACID
    //       - if there is both a PROTEIN and NUCLEIC_ACID, only include the PROTEIN
    private static List<AtomGroup> filterAtomGroupings(List<AtomGroup> allAtomGroupings){
        if(!CollectionUtil.hasValues(allAtomGroupings)){
            return null;
        }

        List<AtomGroup> agFilter1 = new ArrayList<>(1);

        for(AtomGroup ag : allAtomGroupings){
            if(CHAIN_TYPES_TO_SAVE.contains(ag.getParentChain().getType())){
                agFilter1.add(ag);
            }
        }

        //if only one grouping left (either a PROTEIN or NUCLEIC_ACID, return it -- this is normal)
        if(agFilter1.size()<=1){
            return agFilter1;
        }

        //else we need to either return the PROTEIN(s), or if no PROTEIN(s), then the NUCLEIC_ACID(S)
        Map<ChainType, List<AtomGroup>> atomGroupingMap = new LinkedHashMap<>();
        for(AtomGroup ag : agFilter1){
            if(atomGroupingMap.containsKey(ag.getParentChain().getType())){
                atomGroupingMap.get(ag.getParentChain().getType()).add(ag);
            } else {
                List<AtomGroup> first = new ArrayList<>();
                first.add(ag);
                atomGroupingMap.put(ag.getParentChain().getType(), first);
            }
        }
        
        if(atomGroupingMap.containsKey(ChainType.PROTEIN)){
            return atomGroupingMap.get(ChainType.PROTEIN);
            
        } else if(atomGroupingMap.containsKey(ChainType.NUCLEIC_ACID)){
            return atomGroupingMap.get(ChainType.NUCLEIC_ACID);
        }

        return null;

    }



    /**
     * check the database to see if a file exists based on if the checksum is present
     * @param inConn
     * @param inFile
     * @return
     * @throws JImportException
     
    public static boolean fileChecksumExistsInDatabase(Connection inConn, File inFile) throws JImportException {
        try {
            if (null == inFile || !inFile.exists()) {
                throw new JImportException("Error determining file checksum; file is null or does not exist!");
            }
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(inFile));
            byte[] checksum = ChecksumUtil.calculateMD5(bis);
            return fileChecksumExistsInDatabase(inConn, HexUtil.byteArrayToHexString(checksum));
        } catch (IOException ioe) {
            throw new JImportException("Input/Output error attempting to check the database for an existing checksum!", ioe);
        }
    }*/

    /**
     * check the database to see if a checksum has already been created for any existing file
     * @param inConn
     * @param inChecksum
     * @return
     
    public static boolean fileChecksumExistsInDatabase(Connection inConn, String inChecksum) {
        Jstruct_StructFile structFile = Jstruct_StructFile.getRowByChecksum(inConn, inChecksum);
        return null != structFile;
    }*/


    /**
     * determine if the checksum for two JstructFile objects are equal (aka it's the same file)
     * @param structFile1
     * @param structFile2
     * @return
     * @throws JImportException
     
    public static boolean jstructFileChecksumsAreIdentical(Jstruct_StructFile structFile1, Jstruct_StructFile structFile2) throws JImportException {
        if (null == structFile1 || null == structFile2) {
            throw new JImportException("Error determining if file checksums are identical; one or both JstructFile objects are null!");
        }
        return structFile1.getChecksum().equals(structFile2.getChecksum());
    }*/




    /**
     * 
     * @param inFile
     * @return
     * @throws JImportException 
     */
    public static boolean fileExistsInActivePdbDirectory(File inFile) throws JImportException{
        return fileExistsInSpecifiedPdbDirectory(inFile, "divided"); //todo make a constant, or put in context-test/prod.xml or properties file
    }


    /**
     * 
     * @param inFile
     * @return
     * @throws JImportException 
     */
    public static boolean fileExistsInObsoletePdbDirectory(File inFile) throws JImportException {
        return fileExistsInSpecifiedPdbDirectory(inFile, "obsolete"); //todo make a constant, or put in context-test/prod.xml or properties file
    }



    /**
     * 
     * @param inStructFile
     * @throws JDAOException
     * @throws JException 
     */
    public static void updateStructureForPdbFileToActive(Jstruct_StructFile inStructFile) throws JDAOException, JException {
        updateActiveStateOfStructureForPdbFile(inStructFile, null);
    }


    /**
     *
     * @param inStructFile
     * @param inObsoleteDate
     * @throws JDAOException
     * @throws JException
     */
    public static void updateStructureForPdbFileToObsolete(Jstruct_StructFile inStructFile, Date inObsoleteDate) throws JDAOException, JException {
        updateActiveStateOfStructureForPdbFile(inStructFile, inObsoleteDate);
    }


    /**
     * 
     * @param count
     * @param percent
     * @return 
     */
    public static int maxErrorsBeforeImportFail(int count, int percent){
        double actualPercent = (percent * 1.0) / 100;
        int maxFailCount = (int) (actualPercent * count);
        if(maxFailCount > 500){
            return 500;
        } else if (maxFailCount < 50) {
            return 50;
        }
        return maxFailCount;
    }



    //###########################################################################
    // PRIVATE METHODS
    //###########################################################################





    private static boolean fileExistsInSpecifiedPdbDirectory(File inFile, String directoryToLookFor) throws JImportException {
        //todo - have ability to toggle this check on/off
        try {
            Path filePath = Paths.get(inFile.getPath());
            String fullPath = filePath.toRealPath().toString();
            return fullPath.contains(directoryToLookFor);
        } catch (IOException ioe) {
            throw new JImportException("Error determining original location of a PDB file (cannot identify if it is in the '"+directoryToLookFor+"' directory)!", ioe);
        }
    }


    private static Jstruct_StructFile updateActiveStateOfStructureForPdbFile(Jstruct_StructFile inStructFile, Date inObsoleteDate) throws JDAOException, JException {

        StructVersionService structVersionService = new StructVersionService();
        StructureService structureService = new StructureService();
        
        
        if(null==inStructFile){
            throw new JException("StructFile/StructVersion is required when attempting to set the state of a Structure!");
        }

        Jstruct_StructVersion structVersion = structVersionService.getByStructVerId(inStructFile.getStructVerId());

        if(null==structVersion) {
            throw new JException("Error finding StructVersion by StructFile VALUE_MAP_ID (" + inStructFile.getStructVerId() + ") - no version found!");
        }

        Jstruct_Structure structure = structureService.getByStructureId(structVersion.getStructureId());

        if (null == inObsoleteDate) {
            structure.setObsolete(false);
            structure.setObsoleteDate(null);
        } else {
            structure.setObsolete(true);
            structure.setObsoleteDate(inObsoleteDate);
        }
        structureService.update(structure);

        return inStructFile;

    }




}
