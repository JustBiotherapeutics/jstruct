package com.just.jstruct.structureImport;


import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.Structure;
import com.just.bio.structure.exceptions.StructureParseException;
import com.just.bio.structure.pdb.*;
import com.just.bio.structure.pdb.pojo.*;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JImportException;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_PdbAuthor;
import com.just.jstruct.model.Jstruct_PdbCompnd;
import com.just.jstruct.model.Jstruct_PdbFile;
import com.just.jstruct.model.Jstruct_PdbJrnl;
import com.just.jstruct.model.Jstruct_PdbKeyword;
import com.just.jstruct.model.Jstruct_PdbRemark;
import com.just.jstruct.model.Jstruct_PdbRevdat;
import com.just.jstruct.model.Jstruct_PdbSeqres;
import com.just.jstruct.model.Jstruct_PdbSource;
import com.just.jstruct.model.Jstruct_PdbSplit;
import com.just.jstruct.model.Jstruct_PdbSsbond;
import com.just.jstruct.model.Jstruct_User;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class PdbDataUtils {



    //------------------------------------------------------------------------------------------------------------------

    /**
     * return a temporary PDB File object for use during the 1st step of the manual import
     * note: this can NOT be persisted.
     * @param file
     * @param importJob
     * @param user
     * @return
     * @throws JImportException
     */ 
    public static Jstruct_PdbFile createTempPdbFileFromParsedData(File file, Jstruct_ImportJob importJob, Jstruct_User user) throws JImportException {
        try {
            
            PdbFileParse pdbFileParse = new PdbFileParse();
            Structure structureFormat = pdbFileParse.executeParseByLocalFile(file);
            return createPdbFileFromParsedData(0L, structureFormat, importJob, user);

        } catch (JDAOException | StructureParseException  ex) {
            throw new JImportException("Error creating temporary PDB file from parsed data", ex);
        }
    }


    /**
     * 
     * @param structVerId
     * @param structureObj
     * @param importJob
     * @param user
     * @return
     * @throws JDAOException 
     */
    public static Jstruct_PdbFile createPdbFileFromParsedData(Long structVerId, Structure structureObj, Jstruct_ImportJob importJob, Jstruct_User user) throws JDAOException {

        //create a new PdbFile entity object...
        Jstruct_PdbFile pdbFile = new Jstruct_PdbFile();
        pdbFile.setStructVerId(structVerId);

        //break up the pdbFileParse object into its 'convenience objects'
        PdbSectionMetadata pdbSectionMetadata = structureObj.getMetadata();
        PdbSectionConnectivity pdbSectionConnectivity = structureObj.getConnectivity();
        PdbSectionPrimary pdbSectionPrimary = structureObj.getPrimary();
        PdbSectionRemark pdbSectionRemark = structureObj.getRemark();

        String inferredPdbIdentifier = pdbSectionMetadata.getPdbIdentifier();

        //...and populate all it's things-and-stuff
        pdbFile.setClassification(pdbSectionMetadata.getClassification());
        pdbFile.setDepositionDate(pdbSectionMetadata.getDepositionDate());
        if(StringUtil.isSet(pdbSectionMetadata.getTitle())){
            pdbFile.setTitle(pdbSectionMetadata.getTitle());
        } else {
            pdbFile.setTitle(inferredPdbIdentifier + " (no TITLE lines found in PDB file)");
            ImportService.persistImportWarning(importJob, user, inferredPdbIdentifier, "TITLE line is missing or is blank. Setting title to: '" + pdbFile.getTitle(), null);
        }
        pdbFile.setCaveat(pdbSectionMetadata.getCaveat());
        pdbFile.setExpdta(pdbSectionMetadata.getExpdta());
        pdbFile.setNummdl(pdbSectionMetadata.getNummdl());
        pdbFile.setMdltyp(pdbSectionMetadata.getMdltyp());
        pdbFile.setResolutionLine(pdbSectionRemark.getResolutionLine());
        pdbFile.setResolutionUnit(pdbSectionRemark.getResolutionUnit());
        pdbFile.setResolutionMeasure(pdbSectionRemark.getResolution());

        pdbFile.setPdbSplits(PdbDataUtils.transformSplits(pdbSectionMetadata.getSplits(), structVerId));
        pdbFile.setPdbKeywords(PdbDataUtils.transformKeywords(pdbSectionMetadata.getKeyWrds(), structVerId));
        pdbFile.setPdbAuthors(PdbDataUtils.transformAuthors(pdbSectionMetadata.getAuthors(), structVerId));

        pdbFile.setPdbCompnds(PdbDataUtils.transformCompnds(pdbSectionMetadata.getCompnds(), structVerId));
        pdbFile.setPdbSources(PdbDataUtils.transformSources(pdbSectionMetadata.getSources(), structVerId));

        pdbFile.setPdbRemarks(PdbDataUtils.transformRemarks(pdbSectionRemark.getRemarks(), structVerId));

        pdbFile.setPdbRevdats(PdbDataUtils.transformRevdats(pdbSectionMetadata.getPdbRevdats(), structVerId, inferredPdbIdentifier, importJob, user));
        pdbFile.setPdbJrnls(PdbDataUtils.transformJrnls(pdbSectionMetadata.getPdbJrnls(), structVerId));

        pdbFile.setPdbSsbonds(PdbDataUtils.transformSsbonds(pdbSectionConnectivity.getPdbSsbonds(), structVerId));
        pdbFile.setPdbSeqress(PdbDataUtils.transformSeqress(pdbSectionPrimary.getPdbSeqress(), structVerId));

        pdbFile.setPdbIdentifier(inferredPdbIdentifier);

        return pdbFile;
    }


/*
    public static Jstruct_PdbFile createPdbHeaderDataFromUploadInput(Jstruct_StructFile structFile, UploadInput uploadInput) {

        
        Long structVerId =  structFile.getStructVerId();
        
        //create a new PdbFile entity object...
        Jstruct_PdbFile pdbFile = new Jstruct_PdbFile();
        pdbFile.setStructVerId(structVerId);


        //...based on user input (UploadInput) fill out the PDB objects that we're currently allowing the user to populate
        pdbFile.setTitle(uploadInput.getPdbTitle());
        pdbFile.setClassification(uploadInput.getClassification());
        pdbFile.setDepositionDate(new Date());
        pdbFile.setCaveat(uploadInput.getCaveat());
        pdbFile.setExpdta(uploadInput.getExpdta());
        pdbFile.setNummdl(uploadInput.getNummdl());
        pdbFile.setMdltyp(uploadInput.getMdltyp());
        pdbFile.setResolutionUnit(uploadInput.getResolutionUnit());
        pdbFile.setResolutionMeasure(uploadInput.getResolutionMeasure());

        pdbFile.setPdbKeywords(PdbDataUtils.transformKeywords(uploadInput.getKeywordsAsList(), structVerId));
        pdbFile.setPdbAuthors(PdbDataUtils.transformAuthors(uploadInput.getAuthorsAsList(), structVerId));

        Jstruct_PdbJrnl pdbJrnl = new Jstruct_PdbJrnl(structVerId, uploadInput.getJrnlAuth(), uploadInput.getJrnlTitl(), true);
        List<Jstruct_PdbJrnl> pdbJrnls = new ArrayList<>(1);
        pdbJrnls.add(pdbJrnl);
        pdbFile.setPdbJrnls(pdbJrnls);

        //todo: any validation checks??

        return pdbFile;
    }
*/

    /**
     * transform a list of PdbSplits into Jstruct Entity objects (Jstruct_PdbSplit) for persisting
     * @param splits
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbSplit> transformSplits(List<PdbSplit> splits, Long structVerId) {

        if(!CollectionUtil.hasValues(splits)){
            return null;
        }

        List<Jstruct_PdbSplit> pdbSplits = new ArrayList<>(splits.size());
        for (PdbSplit split : splits) {
            pdbSplits.add(new Jstruct_PdbSplit(structVerId, split.getPdbIdentifier(), split.getDbName(), split.getContentType(), split.getDetail()));
        }
        return pdbSplits;
    }


    /**
     * transform a list of Strings into Jstruct Entity objects (Jstruct_PdbKeyword) for persisting
     * @param keywords
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbKeyword> transformKeywords(List<String> keywords, Long structVerId) {

        if(!CollectionUtil.hasValues(keywords)){
            return null;
        }

        List<Jstruct_PdbKeyword> pdbKeywords = new ArrayList<>(keywords.size());
        for (String keyword : keywords) {
            pdbKeywords.add(new Jstruct_PdbKeyword(structVerId, keyword));
        }
        return pdbKeywords;
    }


    /**
     * transform a list of Strings into Jstruct Entity objects (Jstruct_PdbAuthor) for persisting
     * @param authors
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbAuthor> transformAuthors(List<String> authors, Long structVerId) {

        if(!CollectionUtil.hasValues(authors)){
            return null;
        }

        List<Jstruct_PdbAuthor> pdbAuthors = new ArrayList<>(authors.size());
        for (String author : authors) {
            pdbAuthors.add(new Jstruct_PdbAuthor(structVerId, author));
        }
        return pdbAuthors;
    }


    /**
     * transform a list representing COMPNDs into Jstruct Entity objects (Jstruct_PdbCompnd) for persisting
     * @param compnds
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbCompnd> transformCompnds(List<PdbCompnd> compnds, Long structVerId) {

        if(!CollectionUtil.hasValues(compnds)){
            return null;
        }

        List<Jstruct_PdbCompnd> pdbCompnds = new ArrayList<>(compnds.size());

        //iterate over our list of PdbCompnds and create a Jstruct_PdbCompnds collection
        for (PdbCompnd compnd : compnds) {
            String linesValue = StringUtil.join(compnd.getLines(), "\n");
            pdbCompnds.add(new Jstruct_PdbCompnd(structVerId, compnd.getMolId() , compnd.getName(), linesValue));
        }
        return pdbCompnds;
    }


    /**
     * transform a list representing SOURCEs into Jstruct Entity objects (Jstruct_PdbSource) for persisting
     * @param sources
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbSource> transformSources(List<PdbSource> sources, Long structVerId) {

        if(!CollectionUtil.hasValues(sources)){
            return null;
        }

        List<Jstruct_PdbSource> pdbSources = new ArrayList<>(sources.size());

        //iterate over our list of PdbSources and create a Jstruct_PdbSources collection
        for (PdbSource source : sources) {
            String linesValue = StringUtil.join(source.getLines(), "\n");
            pdbSources.add(new Jstruct_PdbSource(structVerId, source.getMolId() , source.getName(), linesValue));
        }
        return pdbSources;
    }


    /**
     * transform a List of PdbRemarks representing REMARKs into Jstruct Entity objects (Jstruct_PdbRemark) for persisting
     * @param remarks
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbRemark> transformRemarks(List<PdbRemark> remarks, Long structVerId) {

        if(!CollectionUtil.hasValues(remarks)){
            return null;
        }

        List<Jstruct_PdbRemark> pdbRemarks = new ArrayList<>(remarks.size());

        //iterate over our hashmap and create a PdbRemark collection
        for (PdbRemark pdbRemark : remarks) {
            String remarkValue = StringUtil.join(pdbRemark.getLines(), "\n");
            pdbRemarks.add(new Jstruct_PdbRemark(structVerId, pdbRemark.getRemNumber(), pdbRemark.getRemName(), remarkValue));
        }
        return pdbRemarks;
    }



    /**
     * 
     * transform a list of PdbRevdat objects into Jstruct Entity objects (Jstruct_PdbRevdat) for persisting
     * @param revdats
     * @param structVerId
     * @param pdbIdentifier
     * @param importJob
     * @param user
     * @return
     * @throws JDAOException 
     */
    public static List<Jstruct_PdbRevdat> transformRevdats(List<PdbRevdat> revdats, Long structVerId, String pdbIdentifier, Jstruct_ImportJob importJob, Jstruct_User user)
            throws JDAOException {

        if(!CollectionUtil.hasValues(revdats)){
            return null;
        }

        List<Jstruct_PdbRevdat> pdbRevdats = new ArrayList<>(revdats.size());

        for (PdbRevdat rvdt : revdats) {
            if (null == rvdt.getModdate()) {
                ImportService.persistImportWarning(importJob, user, pdbIdentifier,
                        "REVDAT line for the file being imported contains no date (this line will not be persisted)", null);
                continue;
            }

            pdbRevdats.add(new Jstruct_PdbRevdat(structVerId, rvdt.getModnum(), rvdt.getModdate(), rvdt.getModType(), rvdt.getDetail()));
        }

        return pdbRevdats;

    }


    /**
     * transform a list of PdbJrnl objects into Jstruct Entity objects (Jstruct_PdbJrnl) for persisting
     * @param jrnls
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbJrnl> transformJrnls(List<PdbJrnl> jrnls, Long structVerId){

        if(!CollectionUtil.hasValues(jrnls)){
            return null;
        }

        List<Jstruct_PdbJrnl> pdbJrnls = new ArrayList<>(jrnls.size());

        for (PdbJrnl inJrnl : jrnls) {
            Jstruct_PdbJrnl pdbJrnl =
                    new Jstruct_PdbJrnl(
                            structVerId,
                            inJrnl.getAuth(),
                            inJrnl.getTitl(),
                            inJrnl.getEdit(),
                            inJrnl.getRef(),
                            inJrnl.getPubl(),
                            inJrnl.getRefn(),
                            inJrnl.getPmid(),
                            inJrnl.getDoi(),
                            inJrnl.getIsPrimary(),
                            inJrnl.getSection());

            pdbJrnls.add(pdbJrnl);
        }

        return pdbJrnls;

    }


    /**
     * transform a list of PdbSsbond objects into a list of Jstruct Entity Objects (Jstruct_PdbSsbond) for persisting
     * @param pdbSsbonds
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbSsbond> transformSsbonds(List<PdbSsbond> pdbSsbonds, Long structVerId) {

        if (!CollectionUtil.hasValues(pdbSsbonds)) {
            return null;
        }

        List<Jstruct_PdbSsbond> ssBonds = new ArrayList<>(pdbSsbonds.size());

        for(PdbSsbond pdbSsbond : pdbSsbonds){
            ssBonds.add( new Jstruct_PdbSsbond(
                            structVerId, pdbSsbond.getSerNum(),
                            pdbSsbond.getResidue1(),pdbSsbond.getChainId1(), pdbSsbond.getSeqNum1(),pdbSsbond.getIcode1(),
                            pdbSsbond.getResidue2(),pdbSsbond.getChainId2(),pdbSsbond.getSeqNum2(),pdbSsbond.getIcode2(),
                            pdbSsbond.getSym1(), pdbSsbond.getSym2(), pdbSsbond.getBondLength())
            );
        }

        return ssBonds;
    }





    /**
     * transform a list of PdbSeqres objects into a list of Jstruct Entity Objects (Jstruct_PdbSeqres) for persisting
     * @param pdbSeqress
     * @param structVerId required for persisting
     * @return
     */
    public static List<Jstruct_PdbSeqres> transformSeqress(List<PdbSeqres> pdbSeqress, Long structVerId) {

        if (!CollectionUtil.hasValues(pdbSeqress)) {
            return null;
        }

        List<Jstruct_PdbSeqres> seqress = new ArrayList<>(pdbSeqress.size());

        for(PdbSeqres pdbSeqres : pdbSeqress){
            seqress.add( new Jstruct_PdbSeqres(structVerId, pdbSeqres.getChainId(), pdbSeqres.getNumRes(), pdbSeqres.getResidues()));
        }

        return seqress;
    }





}
