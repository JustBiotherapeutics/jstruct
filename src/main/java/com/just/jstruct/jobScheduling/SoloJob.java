package com.just.jstruct.jobScheduling;


import com.hfg.exception.ProgrammingException;
import com.just.jstruct.enums.JobType;
import com.just.jstruct.sequenceProcessing.SequenceUpdate;
import com.just.jstruct.structureImport.rcsbRestImport.RcsbImport;
import com.just.jstruct.utilities.JEmailUtil;
import com.just.jstruct.utilities.JStringUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.quartz.*;


/**
 * Job that runs a manually initiated something-or-other
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class SoloJob implements Job {


    public static String ADDITIONAL_PARAMS = "additional_params";
    public static String USER_ID =           "user_id";
    public static String IS_FULL_PDB_RUN =   "is_full_pdb_run";
    public static String JOB_TYPE =          "job_type";
    


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        //grab Job Parameters
        //https://quartz-scheduler.org/generated/2.2.2/html/qtz-all/#page/quartz-scheduler-webhelp%2Fre-exp_example4.html%23
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();

        String addtitionalParams = jobDataMap.getString(ADDITIONAL_PARAMS);
        Long userId = jobDataMap.getLong(USER_ID);
        boolean isFullPdbRun = jobDataMap.getBoolean(IS_FULL_PDB_RUN); //true if this is the ad hoc import job, false if user entered id(s)
        String jobTypeParam = jobDataMap.getString(JOB_TYPE);


        //if a SoloJob is already running, dont allow another to start
        try {
            List<JobExecutionContext> jobs = jobExecutionContext.getScheduler().getCurrentlyExecutingJobs();
            for (JobExecutionContext job : jobs) {
                if (job.getTrigger().equals(jobExecutionContext.getTrigger()) && !job.getJobInstance().equals(this)) {
                    Map<String, String> params = new HashMap<>(4);
                    params.put(ADDITIONAL_PARAMS, addtitionalParams);
                    params.put(USER_ID, JStringUtil.longToString(userId));
                    params.put(IS_FULL_PDB_RUN, JStringUtil.booleanToString(isFullPdbRun));
                    params.put(JOB_TYPE, jobTypeParam);
                    JEmailUtil.sendJobCurrentlyRunningEmail("SoloJob", params);
                    //System.out.println("ANOTHER INSTANCE OF SoloJob is ALREADY RUNNING");
                    return;
                }
            }
        } catch (SchedulerException se) {
            se.printStackTrace();
            throw new JobExecutionException("Error determining if a current Solo Job is already running!", se);
        }


        //run the solo job!
        
        RcsbImport pdbRestImport = new RcsbImport(userId);
        
        JobType jobType = JobType.valueOf(jobTypeParam);
        //if(isFullPdbRun) {
            
            switch (jobType){
               
                case UPDATE_ALL_CHAINS:
                case UPDATE_MANUAL_CHAINS:
                case UPDATE_MISSING_CHAINS:
                    
                    SequenceUpdate sequenceUpdate = new SequenceUpdate(userId);
                    sequenceUpdate.processSequenceUpdates(jobType);
                    break;
                    
                case INITIAL_RCSB_RUN:
                case INITIAL_RCSB_RERUN:
                case INITIAL_RCSB_RUN_RECOVER:
                case INCREMENTAL_RCSB_RUN:
                    
                    pdbRestImport.processAllRcsbFileChanges(JobType.valueOf(jobTypeParam));
                    break;
                    
                case SELECTED_RCSB_FILES:
                    
                    //change the comma separated string into an array, then process just these pdb identifier(s)
                    String[] pdbIdentifiers = JStringUtil.splitOnCommaAndTrim(addtitionalParams);
                    pdbRestImport.processSelectedRcsbFiles(pdbIdentifiers, false, JobType.SELECTED_RCSB_FILES); 
                    break;
                    
                //case CREATE_BLAST_DB:

                //    CreateBlastDb createBlastDb = new CreateBlastDb(userId);
                //    createBlastDb.processSequenceIntoDb(jobType, parseBlastDbTypes(addtitionalParams));                 
                //    break;
                    
                    
                 default:
                    throw new  ProgrammingException("Invalid JobType when attempting to execute solo job: " + jobType.name());
                    
            }
           
        //} 


    }

    
    
    
    
    
    




}
