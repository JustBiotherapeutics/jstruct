package com.just.jstruct.jobScheduling;


import com.just.jstruct.enums.JobType;
import com.just.jstruct.exception.JException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SoloRunnable implements Runnable {


    private Scheduler scheduler;
    private String params;
    private Long userId;
    private JobType jobType;

    public SoloRunnable(Scheduler scheduler, String params, Long userId, JobType importJobType){
        this.scheduler = scheduler;
        this.params = params;
        this.userId = userId;
        this.jobType = importJobType;
    }

    @Override
    public void run(){

        try{

            scheduler.start();
            scheduler.clear(); //Clears (deletes!) all existing scheduling data - all Jobs, Triggers Calendars.

            scheduler = JSoloScheduler.scheduleSoloJobs(scheduler, params, userId, jobType);

            //wait a couple seconds to ensure it's started
            Thread.sleep(1L * 1000L);

            //https://quartz-scheduler.org/api/2.2.1/org/quartz/Scheduler.html#shutdown()
            //passing true to shutdown will make the scheduler finish the job before it actually performs the shutdown -
            //using 'true' will make this call NOT allow the method to return until all jobs are complete
            //hence the reason it's being run in it's own thread...
            scheduler.shutdown(true);


        } catch (SchedulerException | InterruptedException | JException ex) {
            //todo - maybe send an error email?
            ex.printStackTrace();
        }

    }
}
