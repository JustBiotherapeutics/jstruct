package com.just.jstruct.jobScheduling;


import com.just.jstruct.enums.JobType;
import com.just.jstruct.structureImport.rcsbRestImport.RcsbImport;
import com.just.jstruct.utilities.JEmailUtil;
import java.util.List;
import org.quartz.*;


/**
 * Job that runs PdbRestImport processAllRcsbFileChanges()
 *
 * useful: http://quartz-scheduler.org/documentation/quartz-2.x/tutorials/tutorial-lesson-03
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class ImportJob implements Job {


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException
    {

        //if a ImportJob is already running, dont allow another to start
        try
        {
            List<JobExecutionContext> jobs = jobExecutionContext.getScheduler().getCurrentlyExecutingJobs();
            for (JobExecutionContext job : jobs)
            {
                if (job.getTrigger().equals(jobExecutionContext.getTrigger()) && !job.getJobInstance().equals(this))
                {
                    JEmailUtil.sendJobCurrentlyRunningEmail("ImportJob", null);
                    //System.out.println("ANOTHER INSTANCE OF ImportJob is ALREADY RUNNING");
                    return;
                }
            }
        } 
        catch (SchedulerException se)
        {
            se.printStackTrace();
            throw new JobExecutionException("Error determining if a current Import Job is already running!", se);
        }

        //perform an incremental import
        RcsbImport pdbRestImport = new RcsbImport();
        pdbRestImport.processAllRcsbFileChanges(JobType.INCREMENTAL_RCSB_RUN);


    }








}
