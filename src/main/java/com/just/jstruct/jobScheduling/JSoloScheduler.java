package com.just.jstruct.jobScheduling;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.enums.JobType;
import com.just.jstruct.exception.JException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import org.quartz.*;
import static org.quartz.JobBuilder.newJob;
import org.quartz.impl.StdSchedulerFactory;


/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JSoloScheduler {


    public static String QUARTZ_PROPERTIES_SOLO = "/quartz_solo.properties";

    public static String SOLO_JOB_NAME_PDBLIST = "solo_job__pdb_list";
    public static String SOLO_JOB_NAME_FULLRUN = "solo_job__full_run";

    public static String SOLO_GROUP_NAME = "solo_group1";
    public static String SOLO_TRIGGER_NAME = "solo_trigger1";





    //##################################################################################################################
    //  METHODS - gettin' the SOLO scheduler based on the properties file quart_solo.properties
    //##################################################################################################################


    /**
     * return the 'solo job' scheduler (based on settings in the quartz_solo.properties file)
     * a 'solo' job is when a user manually initiates a specific  job by either entering a PDB identifier to import
     * or by clickin a button to force the import job to run outside it's normally scheduled schedule (see JImportScheduler)
     * or some of the other actions that an admin can initiate
     * @return
     * @throws JException
     */
    public static Scheduler getSoloScheduler() throws JException {
        try {

            InputStream stream = JSoloScheduler.class.getResourceAsStream(QUARTZ_PROPERTIES_SOLO);
            Properties prop = new Properties();
            prop.load(stream);

            StdSchedulerFactory sf = new StdSchedulerFactory();
            sf.initialize(prop);
            Scheduler scheduler = sf.getScheduler();

            return scheduler;

        } catch (IOException | SchedulerException se) {
            throw new JException("Error creating the Solo Job scheduler from properties file (" + QUARTZ_PROPERTIES_SOLO + ")", se);
        }
    }




    //##################################################################################################################
    // PUBLIC METHODS - startin' stoppin' and doin' schedulin' stuff.
    //  -- this section everything is related to the 'SOLO' scheduler referenced in quart_solo.properties
    //##################################################################################################################


    /**
     * run the solo import process in another thread
     * @param params
     * @param userId
     * @param jobType
     * @throws JException
     */
    public static void runSoloProcess(String params, Long userId, JobType jobType) throws JException
    {

        try
        {
            Scheduler soloScheduler = getSoloScheduler();

            if(isAnythingRunning())
            {
                throw new JException("A Solo job/process is already running...");
            }

            //run the solo process in another thread.. this way we can kick it off, and have it wait until
            //its complete before it shuts it down... but yet return focus to our user
            SoloRunnable soloRunnable = new SoloRunnable(soloScheduler, params, userId, jobType);
            Thread t = new Thread(soloRunnable);
            t.start();

            //wait a couple seconds to ensure it's started
            Thread.sleep(2L * 1000L);

        } 
        catch (InterruptedException ie)
        {
            throw new JException("Error creating the Solo Scheduler (" 
                                                    +  "params: " + params + ", "
                                                    +  "jobType: " + jobType.name() + ", "
                                                    +  "userId: " + userId + ")", ie);
        }

    }


    /**
     *
     * @param scheduler
     * @param params
     * @param userId
     * @param jobType
     * @return
     * @throws JException
     */
    public static Scheduler scheduleSoloJobs(Scheduler scheduler, String params, Long userId, JobType jobType) throws JException {
        try {
            
            String jobName = (jobType.equals(JobType.SELECTED_RCSB_FILES) ? SOLO_JOB_NAME_PDBLIST : SOLO_JOB_NAME_FULLRUN);

            JobDetail job1 = newJob(SoloJob.class)
                    .withIdentity(jobName, SOLO_GROUP_NAME)
                    .build();

            Trigger trigger1 = TriggerBuilder.newTrigger()
                    .withIdentity(SOLO_TRIGGER_NAME, SOLO_GROUP_NAME)
                    .startNow()
                    .build();

            job1.getJobDataMap().put(SoloJob.ADDITIONAL_PARAMS, params);
            job1.getJobDataMap().put(SoloJob.USER_ID, userId);
            job1.getJobDataMap().put(SoloJob.JOB_TYPE, jobType.name());

            scheduler.scheduleJob(job1, trigger1);

        } catch (SchedulerException se) {
            throw new JException("Error creating the Solo Scheduler (" 
                                                    +  "params: " + params + ", "
                                                    +  "jobType: " + jobType.name() + ", "
                                                    +  "userId: " + userId + ")", se);
        }
        return scheduler;
    }




    public static boolean isAnythingRunning() throws JException {
        boolean isAnythingRunning = false;
        try {

            Scheduler scheduler = getSoloScheduler();

            List<JobExecutionContext> currentJobs = scheduler.getCurrentlyExecutingJobs();
            if(CollectionUtil.hasValues(currentJobs)){
                isAnythingRunning = true;
            }

        } catch (SchedulerException se) {
            throw new JException("Error checking if any Solo Import Processes are currently running", se);
        }
        return isAnythingRunning;
    }



    public static boolean isRunningWithFullRun() throws JException {

        try {
            Scheduler scheduler = getSoloScheduler();

            List<JobExecutionContext> currentJobs = scheduler.getCurrentlyExecutingJobs();
            if(CollectionUtil.hasValues(currentJobs)){
                for (JobExecutionContext jobCtx : currentJobs) {
                    String thisJobName = jobCtx.getJobDetail().getKey().getName();
                    String thisGroupName = jobCtx.getJobDetail().getKey().getGroup();
                    if(thisJobName.equalsIgnoreCase(SOLO_JOB_NAME_FULLRUN) && thisGroupName.equalsIgnoreCase(SOLO_GROUP_NAME)){
                        return true;
                    }
                    //System.out.println("JImportSoloScheduler.getCurrentlyExecutingJobs  -- thisJobName: " + thisJobName + " -  thisGroupName: " + thisGroupName);
                }

            }
            return false;

        } catch (SchedulerException se) {
            throw new JException("Error checking if any Solo Process (Full Run) is currently running", se);
        }
    }


    public static boolean isRunningWithPdbList() throws JException {

        try {
            Scheduler scheduler = getSoloScheduler();

            List<JobExecutionContext> currentJobs = scheduler.getCurrentlyExecutingJobs();
            if(CollectionUtil.hasValues(currentJobs)){
                for (JobExecutionContext jobCtx : currentJobs) {
                    String thisJobName = jobCtx.getJobDetail().getKey().getName();
                    String thisGroupName = jobCtx.getJobDetail().getKey().getGroup();
                    if(thisJobName.equalsIgnoreCase(SOLO_JOB_NAME_PDBLIST) && thisGroupName.equalsIgnoreCase(SOLO_GROUP_NAME)){
                        return true;
                    }
                    //System.out.println("JImportSoloScheduler.getCurrentlyExecutingJobs  -- thisJobName: " + thisJobName + " -  thisGroupName: " + thisGroupName);
                }

            }
            return false;

        } catch (SchedulerException se) {
            throw new JException("Error checking if any Solo Process is currently running", se);
        }
    }


}
