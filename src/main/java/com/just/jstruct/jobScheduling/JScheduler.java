package com.just.jstruct.jobScheduling;


import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.exception.JException;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import org.quartz.*;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;



/**
 * primary scheduler that runs the RCSB import job
 * (probably weekly - reconciles changes to the PDB * repo from RCSB)
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JScheduler {


    public static String QUARTZ_PROPERTIES_AUTO = "/quartz.properties";
    public static String IMPORT_JOB_NAME = "import_job";
    public static String IMPORT_GROUP_NAME = "import_group";
    public static String IMPORT_TRIGGER_NAME = "import_trigger";


    //public static String MOEBATCH_JOB_NAME = "moebatch_job";
   // public static String MOEBATCH_GROUP_NAME = "moebatch_group";
   // public static String MOEBATCH_TRIGGER_NAME = "moebatch_trigger";




    //##################################################################################################################
    //  METHODS - gettin' the scheduler based on the properties file: quartz.properties
    //##################################################################################################################

    /**
     * return the primary scheduler (based on settings in the quartz.properties file)
     * @return
     * @throws JException
     */
    public static Scheduler getImportScheduler() throws JException {
        try {

            
            //InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("/ApplicationResources.properties");
            InputStream stream = JScheduler.class.getResourceAsStream(QUARTZ_PROPERTIES_AUTO);
            Properties prop = new Properties();
            prop.load(stream);

            //https://quartz-scheduler.org/generated/2.2.2/html/qtz-all/#page/quartz-scheduler-webhelp%2Fre-snip_create_scheduler.html%23
            StdSchedulerFactory sf = new StdSchedulerFactory();
            sf.initialize(prop);
            Scheduler scheduler = sf.getScheduler();

            return scheduler;

        } catch (IOException | SchedulerException se) {
            throw new JException("Error creating the Import Scheduler from properties file (" + QUARTZ_PROPERTIES_AUTO + ")", se);
        }
    }




    //##################################################################################################################
    // PUBLIC METHODS - startin' stoppin' and doin' schedulin' stuff.
    //  -- this section everything is related to the 'primary' scheduler referenced in quartz.properties
    //##################################################################################################################


    /**
     * start the primary scheduler if it is not already started; and add (schedule) all the appropriate jobs
     * @return
     * @throws JException
     */
    public static Scheduler startImportProcess() throws JException {

        try {

            Scheduler scheduler = getImportScheduler();

            if(scheduler.isStarted()){
                return scheduler;
            }
            scheduler.start();
            scheduler.clear(); //Clears (deletes!) all scheduling data - all Jobs, Triggers Calendars.

            scheduleImportJobs(scheduler);

            //wait a couple seconds to ensure it's started
            Thread.sleep(2L * 1000L);

            return scheduler;

        } catch (InterruptedException | SchedulerException ex) {
            throw new JException("Error starting the Scheduler (Automated Process)", ex);
        }

    }



    /**
     * schedule any jobs we want for our primary scheduler
     *
     * https://quartz-scheduler.org/generated/2.2.2/html/qtz-all/#page/quartz-scheduler-webhelp%2Fre-exp_example3.html%23
     * http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/crontrigger
     * http://quartz-scheduler.org/documentation/quartz-2.x/tutorials/tutorial-lesson-03
     * http://quartz-scheduler.org/api/2.2.0/org/quartz/DisallowConcurrentExecution.html
     *
     * @param scheduler
     * @return
     * @throws SchedulerException
     */
    private static Scheduler scheduleImportJobs(Scheduler scheduler) throws JException {
        try {
            //        String rootFolder = JstructParam.PDB_MIRROR;
            //       String obsoleteDat = JstructParam.PDB_OBSOLETE_DAT;
            String importCron = ValueMapUtils.getInstance().getRcsbImportCron();
            //importCron = "0/25 * * * * ?";  //every 25 seconds - used for testing.

            JobDetail job1 = newJob(ImportJob.class)
                    .withIdentity(IMPORT_JOB_NAME, IMPORT_GROUP_NAME)
                    .build();

            CronTrigger trigger1 = newTrigger()
                    .withIdentity(IMPORT_TRIGGER_NAME, IMPORT_GROUP_NAME)
                    .withSchedule(cronSchedule(importCron))
                    .build();

            scheduler.scheduleJob(job1, trigger1);




            //String moebatchCron = JstructParam.MOEBATCH_CRON;
            //moebatchCron = "0/10 * * * * ?";  //every 10 seconds - used for testing.

            //JobDetail job2 = newJob(MoebatchJob.class)
            //        .withIdentity(MOEBATCH_JOB_NAME, MOEBATCH_GROUP_NAME)
            //        .build();

            //CronTrigger trigger2 = newTrigger()
            //        .withIdentity(MOEBATCH_TRIGGER_NAME, MOEBATCH_GROUP_NAME)
            //        .withSchedule(cronSchedule(moebatchCron))
            //        .build();

            //scheduler.scheduleJob(job2, trigger2);

            //       job1.getJobDataMap().put(ImportJob.ROOT_FOLDER_PARAM, rootFolder);
            //       job1.getJobDataMap().put(ImportJob.OBSOLETE_FILE_PARAM, obsoleteDat);

        } catch (SchedulerException se) {
            throw new JException("Error creating the Scheduler (Automated Process)", se);
        }
        return scheduler;
    }


    /**
     * shutdown the primary scheduler
     * @return
     * @throws JException
     */
    public static Scheduler shutdownImportProcess() throws JException {
        try {

            Scheduler scheduler = getImportScheduler();

            scheduler.clear(); //Clears (deletes!) all scheduling data - all Jobs, Triggers Calendars.
            scheduler.shutdown();

            return scheduler;

        } catch (SchedulerException se) {
            throw new JException("Error shutting down the Scheduler (Automated Process)", se);
        }
    }


    /**
     * determine if the primary scheduler is currently started (aka running)
     * @return
     * @throws JException
     */
    public static boolean isStarted() throws JException {
        boolean isStarted = false;
        try {

            Scheduler scheduler = getImportScheduler();

            isStarted = scheduler.isStarted();
            if(!isStarted){
                scheduler.shutdown();
            }

        } catch (SchedulerException se) {
            throw new JException("Error checking if the Scheduler (Automated Process) is started", se);
        }
        return isStarted;
    }


    /**
     * determine if a specific job is currently executing
     * @param jobName
     * @param groupName
     * @return
     * @throws JException
     */
    public static boolean isImportJobRunning(String jobName, String groupName) throws JException {
        try {
            Scheduler scheduler = getImportScheduler();

            if(!scheduler.isStarted()){
                scheduler.shutdown();
                return false;
            }

            List<JobExecutionContext> currentJobs = scheduler.getCurrentlyExecutingJobs();

            for (JobExecutionContext jobCtx : currentJobs) {

                String thisJobName = jobCtx.getJobDetail().getKey().getName();
                String thisGroupName = jobCtx.getJobDetail().getKey().getGroup();

                if (jobName.equalsIgnoreCase(thisJobName) && groupName.equalsIgnoreCase(thisGroupName)) {
                    return true;
                }
            }
        } catch (SchedulerException se) {
            throw new JException("Error checking if an Import Process is currently running (jobName: "+jobName+", groupName: " + groupName + ")", se);
        }
        return false;

    }




    /**
     * get trigger for a job - this can be useful if we want to determine when a job was run last, or will fire next
     * @param inJobName
     * @param inGroupName
     * @return
     * @throws JException
     */
    public static Trigger getTriggerForJob(String inJobName, String inGroupName) throws JException {
        try {
            Scheduler scheduler = getImportScheduler();

            if(!scheduler.isStarted()){
                scheduler.shutdown();
                return null;
            }

            for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(inGroupName))) {

                if(jobKey.getName().equalsIgnoreCase(inJobName)){
                    List<Trigger> triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
                    return triggers.get(0);//.getNextFireTime();
                }
            }


        } catch (SchedulerException se) {
            throw new JException("Error retrieving next fire date for a job", se);
        }
        return null;

    }


    /**
     *
     * @return
     * @throws JException
     */
    public static boolean isAnythingRunning() throws JException {

        try {

            Scheduler scheduler = getImportScheduler();

            if(!scheduler.isStarted()){
                scheduler.shutdown();
                return false;
            }

            List<JobExecutionContext> currentJobs = scheduler.getCurrentlyExecutingJobs();
            if(CollectionUtil.hasValues(currentJobs)){
                //for (JobExecutionContext jobCtx : currentJobs) {
                //    String thisJobName = jobCtx.getJobDetail().getKey().getName();
                //    String thisGroupName = jobCtx.getJobDetail().getKey().getGroup();
                //    System.out.println("JImportScheduler.getCurrentlyExecutingJobs  -- thisJobName: " + thisJobName + " -  thisGroupName: " + thisGroupName);
                //}
                return true;
            }
            return false;

        } catch (SchedulerException se) {
            throw new JException("Error checking if any Import Processes are currently running", se);
        }
    }



}
