package com.just.jstruct.backing;

import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.primefaces.event.TabChangeEvent;


/**
 * bean that backs the entire lookups page (all tabs)
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="lookups")
@RequestScoped
public class LookupsBean implements Serializable {
 
    private static final long serialVersionUID = 1L;

    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    

    
  
    /* constructor */
    public LookupsBean() {}
    
    @PostConstruct
    public void init() {
        checkPagePermissions();
    }

    //access to this page is avaliable only for Admin/Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleAdmin() && !sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access page managing lookup values (Client, Program, ...)",
                                                            "Lookups",
                                                            "n/a"));
        }
    }

    
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    
    
    


    //every time a tab is clicked we set it to the current index,
    //so that coming back to this page returns user to same tab
    public void onTabChange(TabChangeEvent event) { 
        
        //todo: make this tons and tons better
       String tabId =  event.getTab().getId();
       int tabIndex = 0;
       if(tabId.equalsIgnoreCase("tab_lookup_program")){ tabIndex=0; }
       else if(tabId.equalsIgnoreCase("tab_lookup_client")){ tabIndex=1; }
       sessionBean.setTabLookups(tabIndex);
       
    }

    
    
    

    
}
