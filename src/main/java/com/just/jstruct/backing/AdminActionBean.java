package com.just.jstruct.backing;

import com.hfg.util.StringUtil;
import com.just.bio.structure.enums.ChainType;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.enums.FastaQuery;
import com.just.jstruct.enums.JobType;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.jobScheduling.JSoloScheduler;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.FastaUtil;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JDateUtil;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.NoAccess;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *  backingbean for the admin actions page
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="adminActionBean")
@ViewScoped
public class AdminActionBean implements Serializable {

    private static final long serialVersionUID = 1L;

    //service classes
    private FullStructureService fullStructureService;
    
    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    private Jstruct_User currentUser;
    
    private SchedulingController controller;
    
    private String pdbIdsInput;
    
    private String structVerIdInput;
    private Jstruct_FullStructure foundFullStrcuture;


    
    /* service classes */
  
    
    
    /* constructor */
    public AdminActionBean(){ }
    
    @PostConstruct
    public void init() {
        try {
            
            currentUser = sessionBean.getUser();
            checkPagePermissions();
            controller = new SchedulingController();
            fullStructureService = new FullStructureService(currentUser);
            
            //setup options for canned fasta downloads
            fastaQueryOptions = FastaQuery.allFastaQuerys();
            
            fastaChainOptions = new ArrayList<>(3);
            fastaChainOptions.add("Protein");
            fastaChainOptions.add("Nucleic Acid");
            fastaChainOptions.add("All");
            
            fastaSequenceFromOptions = new ArrayList<>(2);
            fastaSequenceFromOptions.add("Atomic Coordinates");
            fastaSequenceFromOptions.add("SEQRES");
            

        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }

    
    //access to this page is avaliable only for Admin/Developer users
    private void checkPagePermissions() {
        if (!currentUser.getIsRoleAdmin() && !currentUser.getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Admin Actions page",
                                                            "Admin Actions",
                                                            "n/a"));
        }
    }

    
    
    
    /**
     * import RCSB structures related to the list of PDB IDs entered by the user
     */
    public void importPdbIds() {
        try {
            
            String errMsg = validatePdbIdParams(pdbIdsInput);
            if(StringUtil.isSet(errMsg)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, errMsg, ""));
            } else {
            
                if(controller.isAnyJobIsRunning()){
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "A solo job is already running; please wait until finished before running another solo job...", ""));
                } else {

                    String[] idsArray = JStringUtil.splitOnSpaceAndComma(pdbIdsInput);
                    String idsParam = StringUtil.join(idsArray, ", ");
                
                    //do it!!
                    JSoloScheduler.runSoloProcess(idsParam, sessionBean.getUser().getUserId(), JobType.SELECTED_RCSB_FILES);
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Import job running...", ""));
                
                }
                
                init();
                
                //PdbRestImport pdbRestImport = new PdbRestImport(sessionBean.getUser().getUserId());
                //String[] pdbIdentifiers = JStringUtil.splitOnCommaAndTrim("4HHB,1MAG");
                //pdbRestImport.processSelectedPdbFiles(pdbIdentifiers, false, ImportJobType.SELECTED_RCSB_FILES);
            }

        } catch (JException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
 
    
    
    private String validatePdbIdParams(String soloIdsParam){

        if(!StringUtil.isSet(soloIdsParam)){
            return "One or more PDB Identifiers are required.";
        }

        String[] idsArray = JStringUtil.splitOnSpaceAndComma(soloIdsParam);
        for (String id : idsArray) {
            if (id.length() != 4) {
                return "All PDB Identifiers must be 4 characters in length.";
            }
        }

        return null;
    }
    
    
    
    
    
    
    /* getters/setters */

    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public SchedulingController getController() { return controller;}

    public String getPdbIdsInput() { return pdbIdsInput;}
    public void setPdbIdsInput(String pdbIdsInput) { this.pdbIdsInput = pdbIdsInput; }
    
    public String getStructVerIdInput() { return structVerIdInput;}
    public void setStructVerIdInput(String structVerIdInput) { this.structVerIdInput = structVerIdInput; }

    public Jstruct_FullStructure getFoundFullStrcuture() { return foundFullStrcuture;}
    public void setFoundFullStrcuture(Jstruct_FullStructure foundFullStrcuture) { this.foundFullStrcuture = foundFullStrcuture;}

    
    
    
    public List<FastaQuery> getFastaQueryOptions() {return fastaQueryOptions;}
    public void setFastaQueryOptions(List<FastaQuery> fastaQueryOptions) {this.fastaQueryOptions = fastaQueryOptions;}

    public FastaQuery getSelectedFastaQuery() {return selectedFastaQuery; }
    public void setSelectedFastaQuery(FastaQuery selectedFastaQuery) { this.selectedFastaQuery = selectedFastaQuery; }

    public List<String> getFastaChainOptions() { return fastaChainOptions; }
    public void setFastaChainOptions(List<String> fastaChainOptions) {this.fastaChainOptions = fastaChainOptions; }

    public String getSelectedFastaChain() {return selectedFastaChain; }
    public void setSelectedFastaChain(String selectedFastaChain) {this.selectedFastaChain = selectedFastaChain; }

    public List<String> getFastaSequenceFromOptions() {return fastaSequenceFromOptions; }
    public void setFastaSequenceFromOptions(List<String> fastaSequenceFromOptions) {this.fastaSequenceFromOptions = fastaSequenceFromOptions; }

    public String getSelectedFastaSequenceFrom() {return selectedFastaSequenceFrom;}
    public void setSelectedFastaSequenceFrom(String selectedFastaSequenceFrom) {this.selectedFastaSequenceFrom = selectedFastaSequenceFrom;}

    public String getFastaUrlForSelectedOptions() { return fastaUrlForSelectedOptions; }
    public void setFastaUrlForSelectedOptions(String fastaUrlForSelectedOptions) { this.fastaUrlForSelectedOptions = fastaUrlForSelectedOptions; }

    
    
    private final FastaQuery fastaUrlAllRcsb = FastaQuery.ALL_RCSB;
    private final FastaQuery fastaUrlOnlyManualModels = FastaQuery.ONLY_MANUAL_MODELS;
    private final FastaQuery fastaUrlOnlyManualNonModels = FastaQuery.ONLY_MANUAL_NON_MODELS;
    
    public FastaQuery getFastaUrlAllRcsb() {return fastaUrlAllRcsb; }
    public FastaQuery getFastaUrlOnlyManualModels() { return fastaUrlOnlyManualModels; }
    public FastaQuery getFastaUrlOnlyManualNonModels() {return fastaUrlOnlyManualNonModels; }

 
    private List<FastaQuery> fastaQueryOptions;
    private FastaQuery selectedFastaQuery;
    
    private List<String> fastaChainOptions;
    private String selectedFastaChain;
    
    private List<String> fastaSequenceFromOptions;
    private String selectedFastaSequenceFrom;
    
    private String fastaUrlForSelectedOptions;
    
    
    
    /**
     * 
     */
    public void createFastaFileForSelectedOptions(){
        
        try{
        
            String fastaUrl = createFastaUrlForSelectedOptions();

            FacesContext ctx = FacesContext.getCurrentInstance();
            ExternalContext extContext = ctx.getExternalContext();
            extContext.redirect(fastaUrl);
        
        } catch (IOException ex) {
            HijackError.gotoErrorPage(ex);
        }

    }
    
    
     public void showFastaUrlForSelectedOptions(){
         fastaUrlForSelectedOptions = createFastaUrlForSelectedOptions();    
     }
     
     
     private String createFastaUrlForSelectedOptions(){
         
         StringBuilder outputFileName = new StringBuilder(selectedFastaQuery.getOutputFileNameRoot());

        List<ChainType> chainTypes = new ArrayList<>(1);
        switch(selectedFastaChain){
            case("Protein"):
                chainTypes.add(ChainType.PROTEIN);
                outputFileName.append("_protein");
                break;
            case("Nucleic Acid"):
                chainTypes.add(ChainType.NUCLEIC_ACID);
                outputFileName.append("_nucleicacid");
                break;
            case("All"):
                chainTypes = Arrays.asList(ChainType.values());
                break;
        }

        String sequenceFrom = "";
        switch(selectedFastaSequenceFrom){
            case("Atomic Coordinates"):
                sequenceFrom = "ATOM";
                outputFileName.append("_atom");
                break;
            case("SEQRES"):
                sequenceFrom = "SEQRES";
                outputFileName.append("_seqres");
                break;
        }

        String fastaUrl = FastaUtil.createFastaUrl(selectedFastaQuery.getFileStatusRcsb(), 
                                                   selectedFastaQuery.getFileStatusManual(), 
                                                   selectedFastaQuery.getQueryMode(), 
                                                   sequenceFrom, 
                                                   chainTypes, 
                                                   outputFileName.toString(), 
                                                   selectedFastaQuery.getSubQuery());

        return fastaUrl;
        
     }
        
    
    
    
    /**
     * date/time tomcat thinks it is
     * @return 
     */
    public String getCurrentTime(){
        return JDateUtil.defaultDateAndTimeFormat(new Date());
    }
    
    
    /**
     * 
     */
    public void runInitialImport(){
        try {
                                        //String idsParam, Long userId, boolean isFullRcsbRun, ImportJobType importJobType
            JSoloScheduler.runSoloProcess(null, sessionBean.getUser().getUserId(), JobType.INITIAL_RCSB_RUN);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("RCSB Import - Initial Import started..."));
        } catch (JException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     */
    public void recoverInitialImport(){
        try {
            JSoloScheduler.runSoloProcess(null, sessionBean.getUser().getUserId(), JobType.INITIAL_RCSB_RUN_RECOVER);
            controller = new SchedulingController();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("RCSB Import - Re-running (recovering) the Initial Import..."));
        } catch (JException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     */
    public void runForceIncrementalImport(){
        try {
            JSoloScheduler.runSoloProcess(null, sessionBean.getUser().getUserId(), JobType.INCREMENTAL_RCSB_RUN);
            controller = new SchedulingController();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("RCSB Import - Kicking off an incremental import..."));
        } catch (JException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     */
    public void rerunInitialImport()
    {
        try
        {
            JSoloScheduler.runSoloProcess(null, sessionBean.getUser().getUserId(), JobType.INITIAL_RCSB_RERUN);
            controller = new SchedulingController();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("RCSB Import - Re-running the Initial Import..."));
        } 
        catch (JException e)
        {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     */
    public void updateAllSequences(){
        try {
            JSoloScheduler.runSoloProcess(null, sessionBean.getUser().getUserId(), JobType.UPDATE_ALL_CHAINS);
            controller = new SchedulingController();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Updating all sequences for all structures..."));
        } catch (JException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     */
    public void updateManualSequences(){
        try {
            JSoloScheduler.runSoloProcess(null, sessionBean.getUser().getUserId(), JobType.UPDATE_MANUAL_CHAINS);
            controller = new SchedulingController();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Updating sequences for all manually imported structures..."));
        } catch (JException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     */
    public void updateMissingSequences(){
        try {
            JSoloScheduler.runSoloProcess(null, sessionBean.getUser().getUserId(), JobType.UPDATE_MISSING_CHAINS);
            controller = new SchedulingController();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Updating sequences where structure is missing SEQRES data or Atomin Coordinates..."));
        } catch (JException e) {
            HijackError.gotoErrorPage(e);
        }
    }


    
    public void findFullStructureByStructVerId(){
        try {
           
            Long structVerId = JStringUtil.stringToLong(structVerIdInput); 

            if(structVerId == null){
                foundFullStrcuture = null;
                return;
            }

            foundFullStrcuture = fullStructureService.getByStructVerId(structVerId);
  
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
        
        
    }
    
    
    
    
}