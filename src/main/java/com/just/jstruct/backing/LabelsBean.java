package com.just.jstruct.backing;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.UserLabelService;
import com.just.jstruct.dao.service.UserStructureLabelService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserLabel;
import com.just.jstruct.model.Jstruct_UserStructureLabel;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.RowEditEvent;

/**
 * Backing bean for the structure page
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="labelsBean") 
@ViewScoped   
public class LabelsBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    

    
    private Map<Jstruct_UserLabel, Integer> userLabelsMap;
    private List<Jstruct_UserLabel> unusedUserLabels;
    private Jstruct_UserLabel newLabel;
    
    
    
    
    /* service classes */

    private final UserLabelService userLabelService = new UserLabelService();
    private final UserStructureLabelService userStructureLabelService = new UserStructureLabelService();
            
   
    
    //constructor
    public LabelsBean() { 
    }
    
    @PostConstruct
    public void init() {
        try {
            
            checkPagePermissions();
            
            currentUser = sessionBean.getUser();
            
            newLabel = new Jstruct_UserLabel(currentUser);
            
            List<Jstruct_UserStructureLabel> userStructureLabels = userStructureLabelService.getByUserId(currentUser.getUserId());

                if (CollectionUtil.hasValues(userStructureLabels)) {
                    userLabelsMap = new LinkedHashMap<>();
                    for (Jstruct_UserStructureLabel usl : userStructureLabels) {
                        Jstruct_UserLabel ul = userLabelService.getByUserLabelId(usl.getUserLabelId());
                        if (userLabelsMap.containsKey(ul)) {
                            userLabelsMap.put(ul, userLabelsMap.get(ul) + 1);
                        } else {
                            userLabelsMap.put(ul, 1);
                        }
                    }
                }
                unusedUserLabels = userLabelService.getUnusedByUserId(currentUser.getUserId());
            
            
        } catch (InvalidParameterException | JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    //access to this page is avaliable only for logged in users (not guests role)
    private void checkPagePermissions() {
        if (sessionBean.getUser().isGuest()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Labels page",
                                                            "My Labels", "n/a"));
        }
    }

    
    
    
    
    public void createNewLabel(ActionEvent actionEvent) {
        try {
            Jstruct_UserLabel createdLabel = userLabelService.add(newLabel, currentUser);
            
            newLabel = new Jstruct_UserLabel(currentUser);
            unusedUserLabels = userLabelService.getUnusedByUserId(currentUser.getUserId());
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "New Label created", createdLabel.getLabelName()));

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
   
    
    
    
    //save updates to an MyLabels row (for used lables (remember, these are stored in a map, not list)
    public void onEditUsedLabelRow(RowEditEvent event) {
        try{
            Map.Entry<Jstruct_UserLabel, Integer> updatedUserLabelMap = (Map.Entry<Jstruct_UserLabel, Integer>) event.getObject();
            Jstruct_UserLabel updatedUserLabel = updatedUserLabelMap.getKey();
            
            userLabelService.update(updatedUserLabel, currentUser);
            
            FacesMessage msg = new FacesMessage("Structure Label updated", updatedUserLabel.getLabelName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (JDAOException jde) {
            
            jde.printStackTrace();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Structure Label update failed", jde.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }
    
    //save updates to an unusedLabel row 
    public void onEditUnusedLabelRow(RowEditEvent event) {
        try{

            Jstruct_UserLabel updatedUserLabel = (Jstruct_UserLabel) event.getObject();
            userLabelService.update(updatedUserLabel, currentUser);
            
            FacesMessage msg = new FacesMessage("Structure Label updated", updatedUserLabel.getLabelName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (JDAOException jde) {
            
            jde.printStackTrace();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Structure Label update failed", jde.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }
     
    //fuhgeddaboudit
    public void onCancelRow(RowEditEvent event) {
        //FacesMessage msg = new FacesMessage("Edit Cancelled", ((Jstruct_AminoAcid) event.getObject()).getName());
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    /**
     * 
     * @param actionEvent 
     */
    public void deleteLabel(ActionEvent actionEvent) {
        try {
            Long doomedUserLabelId = (Long) actionEvent.getComponent().getAttributes().get("doomedUserLabelId");
            Jstruct_UserLabel doomedUserLabel = userLabelService.getByUserLabelId(doomedUserLabelId);
            String doomedLabelName = doomedUserLabel.getLabelName();
            userLabelService.delete(doomedUserLabel);
            
            unusedUserLabels = userLabelService.getUnusedByUserId(currentUser.getUserId());
            
            FacesMessage msg = new FacesMessage("Structure Label deleted", doomedLabelName);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    
    
    /* getters/setters */
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    public Jstruct_UserLabel getNewLabel() {return newLabel;}
    public void setNewLabel(Jstruct_UserLabel newLabel) {  this.newLabel = newLabel;}
    
    public Map<Jstruct_UserLabel, Integer> getUserLabelsMap() {return userLabelsMap;}
    
    public List<Jstruct_UserLabel> getUnusedUserLabels() {return unusedUserLabels;}

   

    
    
    
    
    
}
