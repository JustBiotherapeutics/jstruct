package com.just.jstruct.backing;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.AnalysisFileService;
import com.just.jstruct.dao.service.AnalysisRunService;
import com.just.jstruct.dao.service.AnalysisScriptService;
import com.just.jstruct.dao.service.AnalysisScriptVerService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_AnalysisFile;
import com.just.jstruct.model.Jstruct_AnalysisRun;
import com.just.jstruct.model.Jstruct_AnalysisScript;
import com.just.jstruct.model.Jstruct_AnalysisScriptVer;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.NoAccess;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;


/**
 * back the page that manages scripts
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2017 Just Biotherapeutics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------

@ManagedBean(name="scriptMgrBean")
@ViewScoped
public class ScriptMgrBean implements Serializable {
 
    
    private static final long serialVersionUID = 1L;

    
    
    /* -------------------------------------------------------------------------
        data members 
    ------------------------------------------------------------------------- */
    
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private static final String JSTRUCT_ID_SUBSTITUTE = "@JSTRUCT_ID";
    public String getJstructIdSubstitute() {return JSTRUCT_ID_SUBSTITUTE;}
    
    //only Active and Defunct since user cannot change status if it is Current
    private List<String> scriptVersionStatuss  = Arrays.asList(Jstruct_AnalysisScriptVer.STATUS_ACTIVE, Jstruct_AnalysisScriptVer.STATUS_DEFUNCT);
    
    //1
    private List<Jstruct_AnalysisScript> userAnalysisScripts;
    private List<Jstruct_AnalysisScript> globalAnalysisScripts;
    
    //2
    private Jstruct_AnalysisScript selectedAnalysisScript;  //contains versions
    
    //3
    private Jstruct_AnalysisScriptVer selectedAnalysisScriptVer; //contains runs
    
    //4
    private Jstruct_AnalysisRun selectedAnalysisRun; //contains tasks

    
    //new
    private String newScriptSubmitErrorMsg;
    private Jstruct_AnalysisScript newScript;
    private Jstruct_AnalysisScriptVer newScriptVer;
    private List<Jstruct_AnalysisFile> newAnalysisFiles;
    
    //another version
    private String versionxScriptSubmitErrorMsg;
    private Jstruct_AnalysisScriptVer versionxScriptVer;
    
    //edit
    private Jstruct_AnalysisScript editScript;
    private String editScriptSubmitErrorMsg;
    
    private Jstruct_AnalysisScriptVer editScriptVer;
    private String editScriptVerSubmitErrorMsg;
    
    /* -------------------------------------------------------------------------
        service classes 
    ------------------------------------------------------------------------- */
 
    private final AnalysisScriptService analysisScriptService = new AnalysisScriptService();
    private final AnalysisScriptVerService analysisScriptVerService = new AnalysisScriptVerService();
    private final AnalysisRunService analysisRunService = new AnalysisRunService();
    private final AnalysisFileService analysisFileService = new AnalysisFileService();
    
    /* -------------------------------------------------------------------------
        constructor 
    ------------------------------------------------------------------------- */
    
    public ScriptMgrBean() {}
    
    @PostConstruct
    private void init(){
        try {
            
            checkPagePermissions();
            
            userAnalysisScripts = analysisScriptService.getByOwnerId(sessionBean.getUser().getUserId(), true, false);
            globalAnalysisScripts = analysisScriptService.getGlobal(true, false);
            
            prepNewForm();
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }

    //access to this page is avaliable only for logged in users (not guests role)
    private void checkPagePermissions() {
        if (sessionBean.getUser().isGuest()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Script Manager page", "Script Manager", "n/a"));
        }
    }
    
    private void prepNewForm()
    {
        newScript = new Jstruct_AnalysisScript();
        newScriptVer = new Jstruct_AnalysisScriptVer();
        newScriptVer.setStatus(Jstruct_AnalysisScriptVer.STATUS_CURRENT);
        newScriptVer.setVersion(1);
        newAnalysisFiles = null;
    }
    
      
    
    /* -------------------------------------------------------------------------
        getters/setters 
    ------------------------------------------------------------------------- */
    
    // getters/setters
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public List<String> getScriptVersionStatuss() { return scriptVersionStatuss;  }
    public void setScriptVersionStatuss(List<String> scriptVersionStatuss) { this.scriptVersionStatuss = scriptVersionStatuss; }
    
    
    public List<Jstruct_AnalysisScript> getUserAnalysisScripts() { return userAnalysisScripts; }
    public void setUserAnalysisScripts(List<Jstruct_AnalysisScript> userAnalysisScripts) { this.userAnalysisScripts = userAnalysisScripts; }

    public List<Jstruct_AnalysisScript> getGlobalAnalysisScripts() { return globalAnalysisScripts; }
    public void setGlobalAnalysisScripts(List<Jstruct_AnalysisScript> globalAnalysisScripts) { this.globalAnalysisScripts = globalAnalysisScripts; }

    public Jstruct_AnalysisScript getSelectedAnalysisScript() {  return selectedAnalysisScript; }
    public void setSelectedAnalysisScript(Jstruct_AnalysisScript selectedAnalysisScript) { this.selectedAnalysisScript = selectedAnalysisScript; }

    public Jstruct_AnalysisScriptVer getSelectedAnalysisScriptVer() { return selectedAnalysisScriptVer; }
    public void setSelectedAnalysisScriptVer(Jstruct_AnalysisScriptVer selectedAnalysisScriptVer) { this.selectedAnalysisScriptVer = selectedAnalysisScriptVer; }

    public Jstruct_AnalysisRun getSelectedAnalysisRun() {  return selectedAnalysisRun; }
    public void setSelectedAnalysisRun(Jstruct_AnalysisRun selectedAnalysisRun) { this.selectedAnalysisRun = selectedAnalysisRun;}

    
    public String getNewScriptSubmitErrorMsg() {  return newScriptSubmitErrorMsg; }
    public void setNewScriptSubmitErrorMsg(String newScriptSubmitErrorMsg) { this.newScriptSubmitErrorMsg = newScriptSubmitErrorMsg; }
    
    public Jstruct_AnalysisScript getNewScript() { return newScript; }
    public void setNewScript(Jstruct_AnalysisScript newScript) { this.newScript = newScript;}

    public Jstruct_AnalysisScriptVer getNewScriptVer() { return newScriptVer; }
    public void setNewScriptVer(Jstruct_AnalysisScriptVer newScriptVer) { this.newScriptVer = newScriptVer; }

    public List<Jstruct_AnalysisFile> getNewAnalysisFiles() {  return newAnalysisFiles; }
    public void setNewAnalysisFiles(List<Jstruct_AnalysisFile> newAnalysisFiles) {  this.newAnalysisFiles = newAnalysisFiles; }

    
    public String getVersionxScriptSubmitErrorMsg() { return versionxScriptSubmitErrorMsg; }
    public void setVersionxScriptSubmitErrorMsg(String versionxScriptSubmitErrorMsg) { this.versionxScriptSubmitErrorMsg = versionxScriptSubmitErrorMsg; }

    public Jstruct_AnalysisScriptVer getVersionxScriptVer() { return versionxScriptVer;}
    public void setVersionxScriptVer(Jstruct_AnalysisScriptVer versionxScriptVer) { this.versionxScriptVer = versionxScriptVer; }
    
    
    
    public Jstruct_AnalysisScript getEditScript() { return editScript; }
    public void setEditScript(Jstruct_AnalysisScript editScript) {  this.editScript = editScript;}
    
    public String getEditScriptSubmitErrorMsg() {  return editScriptSubmitErrorMsg; }
    public void setEditScriptSubmitErrorMsg(String editScriptSubmitErrorMsg) {this.editScriptSubmitErrorMsg = editScriptSubmitErrorMsg;}

    
    public Jstruct_AnalysisScriptVer getEditScriptVer() { return editScriptVer; }
    public void setEditScriptVer(Jstruct_AnalysisScriptVer editScriptVer) {  this.editScriptVer = editScriptVer;}
    
    public String getEditScriptVerSubmitErrorMsg() {  return editScriptVerSubmitErrorMsg; }
    public void setEditScriptVerSubmitErrorMsg(String editScriptVerSubmitErrorMsg) {this.editScriptVerSubmitErrorMsg = editScriptVerSubmitErrorMsg;}

    
    
    /* --------------------------------------------------------------------------------------------------------------------------
        methods 
    -------------------------------------------------------------------------------------------------------------------------- */

   

    
    

    /**
     * 
     * @param event 
     */
    public void uploadFileEvent(FileUploadEvent event)  
    {
        try 
        {
            try 
            {
                UploadedFile uf = event.getFile();
                InputStream inputstream = uf.getInputstream();
                String fileName = uf.getFileName();
                
                Jstruct_AnalysisFile af = new Jstruct_AnalysisFile(fileName, IOUtils.toByteArray(inputstream));
                
                if(newAnalysisFiles == null)
                {
                    newAnalysisFiles = new ArrayList<>(10);
                }
                newAnalysisFiles.add(af);
                
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error uploading file...", ex.getMessage()));
            }
            
            Thread.sleep(500); //gotta stall as sometimes the uploadFileComplete gets called before the last file is actually uploaded!

        } catch (InterruptedException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }

    public void removeFiles()
    {
        newAnalysisFiles = new ArrayList<>(10);
    }
    
    
    
    public void resetNewVerson1()
    {
        prepNewForm();
    }
    
    public void submitNewVersion1()
    {
        //validate
        newScriptSubmitErrorMsg = "";
        List<String> errors = new ArrayList<>(5);
        if(!StringUtil.isSet(newScript.getScriptName())){
            errors.add("Script Name is Required");
        }
        if(!StringUtil.isSet(newScript.getScriptType())){
            errors.add("Script Type is Required");
        }
        if(!StringUtil.isSet(newScriptVer.getDefaultCommand())){
            errors.add("Default Command is Required");
        } 
        else if(!newScriptVer.getDefaultCommand().toUpperCase().contains(JSTRUCT_ID_SUBSTITUTE.toUpperCase())) {
            errors.add("Default Command must contain the value: " + JSTRUCT_ID_SUBSTITUTE);
        }
        if(!CollectionUtil.hasValues(newAnalysisFiles)){
            errors.add("Input must have at least one Script File uploaded");
        }
        
        if(CollectionUtil.hasValues(errors)){
            newScriptSubmitErrorMsg = StringUtil.join(errors, "; ") + ".";
            return;
        }
        
        
        try
        {
            //validation passed, save new script.
            
            newScript = analysisScriptService.add(newScript, sessionBean.getUser());
            newScriptVer.setAnalysisScriptId(newScript.getAnalysisScriptId());
            newScriptVer = analysisScriptVerService.add(newScriptVer, sessionBean.getUser());
            
            for(Jstruct_AnalysisFile af : newAnalysisFiles)
            {
                af.setAnalysisScriptVerId(newScriptVer.getAnalysisScriptVerId()); 
                analysisFileService.add(af);
            }
            
            //clear form, update list of existing scripts.
            prepNewForm();
            userAnalysisScripts = analysisScriptService.getByOwnerId(sessionBean.getUser().getUserId(), true, false);
            globalAnalysisScripts = analysisScriptService.getGlobal(true, false);
            newAnalysisFiles = null;
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "New Analysis Script Uploaded.", newScript.getScriptName());
            FacesContext.getCurrentInstance().addMessage(null, msg);

        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    
    
    
    
    public void editAnalysisScript(Long scriptId)
    {
        try 
        {
            editScript = analysisScriptService.getById(scriptId, false);
        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    public void editAnalysisScriptSave()
    {
        //validate
        editScriptSubmitErrorMsg = "";
        List<String> errors = new ArrayList<>(3);
        if(!StringUtil.isSet(editScript.getScriptName())){
            errors.add("Script Name is Required");
        }
        if(!StringUtil.isSet(editScript.getScriptType())){
            errors.add("Script Type is Required");
        }
        if(CollectionUtil.hasValues(errors)){
            editScriptSubmitErrorMsg = StringUtil.join(errors, "; ") + ".";
            return;
        }
        
        try
        {
            editScript = analysisScriptService.update(editScript);
            
            userAnalysisScripts = analysisScriptService.getByOwnerId(sessionBean.getUser().getUserId(), true, false);
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Analysis Script updated.", editScript.getScriptName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            //close dialog
            RequestContext.getCurrentInstance().execute("PF('dlgEditScript').hide();");
            
        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    public void markScriptAsDeleted()
    {
        try
        {
            editScript.setDeleteDate(new Date());
            editScript.setDeleteUser(sessionBean.getUser()); 
            editScript = analysisScriptService.update(editScript);
            
            userAnalysisScripts = analysisScriptService.getByOwnerId(sessionBean.getUser().getUserId(), true, false);
            selectedAnalysisScript = null;
            selectedAnalysisScriptVer = null;
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Analysis Script deleted.", editScript.getScriptName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            //close dialog
            RequestContext.getCurrentInstance().execute("PF('dlgEditScript').hide();");
            
        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    
    
    
    
    public void editAnalysisScriptVer(Long scriptVerId)
    {
        try 
        {
            editScriptVer = analysisScriptVerService.getById(scriptVerId, false, false);
        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    public void editAnalysisScriptVerSave()
    {
        //validate
        editScriptVerSubmitErrorMsg = "";
        List<String> errors = new ArrayList<>(3);
        if(!StringUtil.isSet(editScriptVer.getDefaultCommand())){
            errors.add("Default Command is Required");
        } 
        else if(!editScriptVer.getDefaultCommand().toUpperCase().contains(JSTRUCT_ID_SUBSTITUTE.toUpperCase())) {
            errors.add("Default Command must contain the value: " + JSTRUCT_ID_SUBSTITUTE);
        }
        if(CollectionUtil.hasValues(errors)){
            editScriptVerSubmitErrorMsg = StringUtil.join(errors, "; ") + ".";
            return;
        }
        
        try
        {
            editScriptVer = analysisScriptVerService.update(editScriptVer);
            
            selectedAnalysisScript = analysisScriptService.getById(editScriptVer.getAnalysisScriptId(), true);
            
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Analysis Script Version updated.", 
                    selectedAnalysisScript.getScriptName() + " - version: " + editScriptVer.getVersion());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            //close dialog
            RequestContext.getCurrentInstance().execute("PF('dlgEditScriptVer').hide();");
            
        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    
    
    
    
    public void selectAnalysisScript(Long scriptId)
    {
        try 
        {
            selectedAnalysisScriptVer = null;
            selectedAnalysisRun = null;
            selectedAnalysisScript = analysisScriptService.getById(scriptId, true);
        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    
    
    public void selectAnalysisScriptVer(Long scriptVerId)
    {
        try 
        {
            selectedAnalysisRun = null;
            selectedAnalysisScriptVer = analysisScriptVerService.getById(scriptVerId, true, true);
        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    public void selectAnalysisRun(Long runId)
    {
        try 
        {
            selectedAnalysisRun = analysisRunService.getById(runId, true);
        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    
    
    
    
    
    public void initiateNewScriptVersionX()
    {
        prepNewVersionX();
    }
    
    
    public void resetNewVersionX()
    {
        prepNewVersionX();
    }
    
    private void prepNewVersionX()
    {
        versionxScriptSubmitErrorMsg = null;
        
        //get current (most recent) version
        Jstruct_AnalysisScriptVer currentScriptVer = selectedAnalysisScript.getCurrentScriptVer(); 
                
        versionxScriptVer = new Jstruct_AnalysisScriptVer();
        versionxScriptVer.setStatus(Jstruct_AnalysisScriptVer.STATUS_CURRENT);
        versionxScriptVer.setVersion(currentScriptVer.getVersion() + 1);
        versionxScriptVer.setDefaultCommand(currentScriptVer.getDefaultCommand()); 
        
        newAnalysisFiles = null;
    }
 
  
    public void submitNewVersionX()
    {
        //validate
        versionxScriptSubmitErrorMsg = "";
        List<String> errors = new ArrayList<>(5);
        
        if(!StringUtil.isSet(versionxScriptVer.getDefaultCommand())){
            errors.add("Default Command is Required");
        } 
        else if(!versionxScriptVer.getDefaultCommand().toUpperCase().contains(JSTRUCT_ID_SUBSTITUTE.toUpperCase())) {
            errors.add("Default Command must contain the value: " + JSTRUCT_ID_SUBSTITUTE);
        }
        if(!CollectionUtil.hasValues(newAnalysisFiles)){
            errors.add("Input must have at least one Script File uploaded");
        }
        
        if(CollectionUtil.hasValues(errors)){
            versionxScriptSubmitErrorMsg = StringUtil.join(errors, "; ") + ".";
            return;
        }
        
        //validation passed
        
        try
        {
            //update current script version with overwrite data, and set to active
            
            Jstruct_AnalysisScriptVer currentScriptVer = selectedAnalysisScript.getCurrentScriptVer(); 
            currentScriptVer.setStatus(Jstruct_AnalysisScriptVer.STATUS_ACTIVE);
            currentScriptVer.setOverwriteDate(new Date());
            currentScriptVer.setOverwriteUser(sessionBean.getUser());
            analysisScriptVerService.update(currentScriptVer);
            
            //save new script version
            
            versionxScriptVer.setAnalysisScriptId(selectedAnalysisScript.getAnalysisScriptId());
            versionxScriptVer = analysisScriptVerService.add(versionxScriptVer, sessionBean.getUser());
            
            for(Jstruct_AnalysisFile af : newAnalysisFiles)
            {
                af.setAnalysisScriptVerId(versionxScriptVer.getAnalysisScriptVerId()); 
                analysisFileService.add(af);
            }
            
            
            //clear form, update lists of existing scripts/versions.
            prepNewVersionX();
            userAnalysisScripts = analysisScriptService.getByOwnerId(sessionBean.getUser().getUserId(), true, false);
            globalAnalysisScripts = analysisScriptService.getGlobal(true, false);
            selectAnalysisScript(selectedAnalysisScript.getAnalysisScriptId());
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "New Analysis Script Version Uploaded.", 
                                                selectedAnalysisScript.getScriptName() + " - version: " + versionxScriptVer.getVersion());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            //close dialog
            RequestContext.getCurrentInstance().execute("PF('dlgNewVersion').hide();");

        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    



    
}
