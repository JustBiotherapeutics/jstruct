package com.just.jstruct.backing;

import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.JDateUtil;
import com.just.jstruct.utilities.JEmailUtil;
import com.just.jstruct.utilities.NoAccess;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.TabChangeEvent;

/**
 * bean that backs the entire properties page (all tabs)
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="propertiesGeneral")
@ViewScoped
public class PropertiesGeneral implements Serializable {
 
    private static final long serialVersionUID = 1L;

    
     /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;

    
    /* constructor */
    public PropertiesGeneral() {}
    
    @PostConstruct
    public void init() {
        checkPagePermissions();
    }
    //access to this page is avaliable only for Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Properties page",
                                                            "Properties",
                                                            "n/a"));
        }
    }
   

    // getters/setters
   
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}


    //every time a tab is clicked we set it to the current index,
    //so that coming back to this page returns user to same tab
    public void onTabChange(TabChangeEvent event) { 
        
        //todo: make this tons and tons better
       String tabId =  event.getTab().getId();
       int tabIndex = 0;
       if(tabId.equalsIgnoreCase("tab_prop_dynamic")){ tabIndex=0; }
       else if(tabId.equalsIgnoreCase("tab_prop_static")){ tabIndex=1; }
       else if(tabId.equalsIgnoreCase("tab_prop_session")){ tabIndex=2; }
       else if(tabId.equalsIgnoreCase("tab_prop_request")){ tabIndex=3; }
       sessionBean.setTabProperties(tabIndex);
       
    }
    
    private String blarg;
    public String getBlarg(){ return blarg; }
    public void setBlarg(String blarg) { this.blarg = blarg;}
    



    public void sendTestEmail(String emailAddress)
    {
        try
        {

            ValueMapUtils vmu = ValueMapUtils.getInstance();

            String subject = "JStruct email test";
            StringBuilder body = new StringBuilder();
            body.append("<p style='font-weight:bold;'>This is a test message sent from JStruct.<p>");
            body.append("<p>time: ").append(JDateUtil.defaultDateAndTimeFormat(new Date(), true)).append("</p>");
            body.append("<p>send to: ").append(emailAddress).append("</p>");
            body.append("<p>current user: ").append(sessionBean.getUser().getName()).append("</p>");
            body.append("<p>instance: ").append(vmu.getAppFullInstance()).append("</p>");

            JEmailUtil.sendEmail(true, subject, body.toString(), emailAddress);

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Test email sent", "Email sent to: " + emailAddress));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error sending test email", e.getMessage()));
        }
    }


    
}
