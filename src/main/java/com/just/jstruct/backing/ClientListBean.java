package com.just.jstruct.backing;

import com.just.jstruct.dao.service.ClientService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_Client;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;


/**
 * list of all records from client table - and ability to manage 'em
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="clientList")
@RequestScoped
public class ClientListBean implements Serializable {
 
    private static final long serialVersionUID = 1L;

    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private List<Jstruct_Client> clients;
    private Jstruct_Client newClient = new Jstruct_Client();


    /* supporting service classes */
    ClientService clientService = new ClientService();

    
    /* constructor */
    public ClientListBean() {}
    
    @PostConstruct
    public void init() {
        try {
            
            checkPagePermissions();
           
            clients = clientService.getAll();
            newClient.setDeprecated(false);
           
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    //access to this page is avaliable only for Admin/Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleAdmin() && !sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Admin Actions page",
                                                            "Admin Actions",
                                                            "n/a"));
        }
    }
    
    
    /* getterts/setters */
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public List<Jstruct_Client> getClients() {return clients;}
    public void setClients(List<Jstruct_Client> clients) {this.clients = clients;}

    public Jstruct_Client getNewClient() {return newClient;}
    public void setNewClient(Jstruct_Client newClient) { this.newClient = newClient;}

    
    
    //add a Jstruct_Client, return to the (same) list page
    public String save() {
        try {
            clientService.addClient(newClient, sessionBean.getUser());
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        } 
        return "/lookups.xhtml?faces-redirect=true";
    }
    
    
    
    //save updates to a Client row
    public void onRowEdit(RowEditEvent event) {
        try{
            Jstruct_Client updatedClient = (Jstruct_Client) event.getObject();
            clientService.updateClient(updatedClient);
            
            FacesMessage msg = new FacesMessage("Client updated", ((Jstruct_Client) event.getObject()).getName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (JDAOException jde) {
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Client update failed", jde.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }
     
    //fuhgeddaboudit
    public void onRowCancel(RowEditEvent event) {
        //FacesMessage msg = new FacesMessage("Edit Cancelled", ((Jstruct_Client) event.getObject()).getName());
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    
    




  

    


    
}
