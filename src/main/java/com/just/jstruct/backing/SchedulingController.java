package com.just.jstruct.backing;

import com.just.jstruct.dao.service.ImportJobService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.jobScheduling.JScheduler;
import com.just.jstruct.jobScheduling.JSoloScheduler;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import net.redhogs.cronparser.CronExpressionDescriptor;
import org.quartz.Trigger;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SchedulingController implements Serializable {

    private static final long serialVersionUID = 1L;

    
    /* data members */
    private boolean autoProcessIsOn;
    private boolean importJobIsRunning;
    private boolean soloJobFullIsRunning;
    private boolean soloJobListIsRunning;
    private boolean anyJobIsRunning;

    private boolean initialRunAttempted ;
    private boolean initialRunSuccessful;
    
    private String cronExpression;
    private String cronEnglish;
    private String cronUrl;
    
    private Date lastSuccessfulImportDate;
    private Date nextScheduledImportDate;
    
    
    /* service classes */
    private final ImportJobService importJobService = new ImportJobService();
    private final ValueMapUtils valueMapUtils = ValueMapUtils.getInstance();
  

    
    /* constructor */
    public SchedulingController(){
        try {
            
            autoProcessIsOn = JScheduler.isStarted();
            importJobIsRunning = JScheduler.isImportJobRunning(JScheduler.IMPORT_JOB_NAME, JScheduler.IMPORT_GROUP_NAME);
            
            soloJobFullIsRunning = JSoloScheduler.isRunningWithFullRun();
            soloJobListIsRunning = JSoloScheduler.isRunningWithPdbList();
            
            anyJobIsRunning = importJobIsRunning || soloJobFullIsRunning || soloJobListIsRunning;

            initialRunAttempted = importJobService.hasInitialImportRun();
            initialRunSuccessful = importJobService.wasInitialImportSuccessful();
            
            cronExpression = valueMapUtils.getRcsbImportCron();
            cronEnglish = CronExpressionDescriptor.getDescription(cronExpression);
            cronUrl = valueMapUtils.getCronUrl();
            
            Jstruct_ImportJob mostRecentSuccessfulRcsbRun = importJobService.getMostRecentSuccessfulRcsbRun();
            if(mostRecentSuccessfulRcsbRun != null){
                lastSuccessfulImportDate = mostRecentSuccessfulRcsbRun.getRunStart();
            }
            Trigger jobTrigger = JScheduler.getTriggerForJob(JScheduler.IMPORT_JOB_NAME, JScheduler.IMPORT_GROUP_NAME);
            if(jobTrigger != null){
                nextScheduledImportDate = jobTrigger.getNextFireTime();
            } 
            
            
        } catch (JException | JDAOException | ParseException e) {
            HijackError.gotoErrorPage(e);
        }
    }


    
    
    
    
    /**
     * start the primary automated process (JImportScheduler)
     */
    public void startProcess(){
        try {
            
           //start automated process
           JScheduler.startImportProcess();
           
           FacesMessage msg = new FacesMessage("Automated Process Started");
           FacesContext.getCurrentInstance().addMessage(null, msg);
           
           autoProcessIsOn = true;
   
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    /**
     * stop the primary automated process (JImportScheduler)
     */
    public void stopProcess(){
        try {
            
           //stop automated process
           JScheduler.shutdownImportProcess();
           
           FacesMessage msg = new FacesMessage("Automated Process Stopped");
           FacesContext.getCurrentInstance().addMessage(null, msg);
           
           autoProcessIsOn = false;
   
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    

    
    
    
    
    /* getters/setters */


    public boolean isAutoProcessIsOn() {
        return autoProcessIsOn;
    }

    public void setAutoProcessIsOn(boolean autoProcessIsOn) {
        this.autoProcessIsOn = autoProcessIsOn;
    }

    public boolean isImportJobIsRunning() {
        return importJobIsRunning;
    }

    public void setImportJobIsRunning(boolean importJobIsRunning) {
        this.importJobIsRunning = importJobIsRunning;
    }

    public boolean isSoloJobFullIsRunning() {
        return soloJobFullIsRunning;
    }

    public void setSoloJobFullIsRunning(boolean soloJobFullIsRunning) {
        this.soloJobFullIsRunning = soloJobFullIsRunning;
    }

    public boolean isSoloJobListIsRunning() {
        return soloJobListIsRunning;
    }

    public void setSoloJobListIsRunning(boolean soloJobListIsRunning) {
        this.soloJobListIsRunning = soloJobListIsRunning;
    }

    public boolean isAnyJobIsRunning() {
        return anyJobIsRunning;
    }

    public void setAnyJobIsRunning(boolean anyJobIsRunning) {
        this.anyJobIsRunning = anyJobIsRunning;
    }

    public boolean isInitialRunAttempted() {
        return initialRunAttempted;
    }

    public void setInitialRunAttempted(boolean initialRunAttempted) {
        this.initialRunAttempted = initialRunAttempted;
    }

    public boolean isInitialRunSuccessful() {
        return initialRunSuccessful;
    }

    public void setInitialRunSuccessful(boolean initialRunSuccessful) {
        this.initialRunSuccessful = initialRunSuccessful;
    }

    public Date getLastSuccessfulImportDate() {
        return lastSuccessfulImportDate;
    }

    public void setLastSuccessfulImportDate(Date lastSuccessfulImportDate) {
        this.lastSuccessfulImportDate = lastSuccessfulImportDate;
    }

    public Date getNextScheduledImportDate() {
        return nextScheduledImportDate;
    }

    public void setNextScheduledImportDate(Date nextScheduledImportDate) {
        this.nextScheduledImportDate = nextScheduledImportDate;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getCronEnglish() {
        return cronEnglish;
    }

    public void setCronEnglish(String cronEnglish) {
        this.cronEnglish = cronEnglish;
    }

    public String getCronUrl() {
        return cronUrl;
    }

    public void setCronUrl(String cronUrl) {
        this.cronUrl = cronUrl;
    }

   



    
    
}