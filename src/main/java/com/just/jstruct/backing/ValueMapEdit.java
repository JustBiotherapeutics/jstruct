package com.just.jstruct.backing;

import com.just.jstruct.dao.service.ValueMapService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_ValueMap;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="valueMapEdit")
@ViewScoped
public class ValueMapEdit implements Serializable {
    

    /* data members */
    
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_ValueMap valueMap;

    //supporting service classes
    private final ValueMapService valueMapService = new ValueMapService();


    /** Constructor */
    public ValueMapEdit() {
    }
   
    
    
    @PostConstruct
    private void init(){
        try{
            checkPagePermissions();

            //grab valuemap id from the URL as a long, then get the valuemap record based on the id
            Long valueMapId = FacesUtil.getLongValueFromUrl("value_map_id");
            valueMap = valueMapService.getById(valueMapId);
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    //access to this page is avaliable only for Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Value Map (dynamic properties) edit page",
                                                            "dynamic_property_edit", "n/a"));
        }
    }

    
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    public Jstruct_ValueMap getValueMap() { return valueMap; }
    public void setValueMap(Jstruct_ValueMap valueMap) { this.valueMap = valueMap; }
    


    //update a ValueMap, return to the list page
    public String save() {
        try {
            valueMapService.updateValueMap(valueMap);
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        } 
        return "/properties.xhtml?faces-redirect=true";
    }


    
    //user pressed cancel, return them to the page from whence they came 
    public String cancel(){
        return "/properties.xhtml?faces-redirect=true";
    }
    

    
}
