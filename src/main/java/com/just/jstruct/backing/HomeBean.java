package com.just.jstruct.backing;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.dao.service.StructureService;
import com.just.jstruct.dao.service.StructureUserService;
import com.just.jstruct.dao.service.UserLabelService;
import com.just.jstruct.dao.service.UserSearchService;
import com.just.jstruct.dao.service.UserStructureLabelService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_StructureUser;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserLabel;
import com.just.jstruct.model.Jstruct_UserSearch;
import com.just.jstruct.model.Jstruct_UserStructureLabel;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.TabChangeEvent;

/**
 * Backing bean for the home page
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="homeBean") 
@ViewScoped   //todo test out with sessionscope
public class HomeBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    
    /* data members */
    
    @ManagedProperty(value="#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    
    private final Integer maxForRecent = 25;
    
    private List<Jstruct_FullStructure> favoriteFullStructures;
    private List<Jstruct_FullStructure> recentFullStructures;
    private List<Jstruct_UserSearch> userSearches;
    private Map<Jstruct_UserLabel, Integer> userLabelsMap;
    private List<Jstruct_UserLabel> unusedUserLabels;
    
   
    //search section stuff 
    private FileStatus selectedFileStatusRcsb;
    private FileStatus selectedFileStatusManual;

    private SubQueryInput subQueryInput;
    private String searchSubmitError;
    
    //metrics
    private Integer activeRcsbFileCount;
    private Integer activeManualFileCount;
    private String randomStructureUrl;
    private Jstruct_FullStructure randomFullStructure;
    
    
    /* service classes */
    
    private FullStructureService fullStructureService;
    private final StructureUserService structureUserService = new StructureUserService();
    private final StructureService structureService = new StructureService();
    private final UserSearchService userSearchService = new UserSearchService();
    private final UserStructureLabelService userStructureLabelService = new UserStructureLabelService();
    private final UserLabelService userLabelService = new UserLabelService();
    
    
    
    
    /* getters/setters */
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public Jstruct_User getCurrentUser() {return currentUser; }

    public Integer getMaxForRecent() { return maxForRecent;}
    
    public List<Jstruct_FullStructure> getFavoriteFullStructures() {return favoriteFullStructures;} 
    public List<Jstruct_FullStructure> getRecentFullStructures() {return recentFullStructures;} 
    public List<Jstruct_UserSearch> getUserSearches() {return userSearches;}
    public Map<Jstruct_UserLabel, Integer> getUserLabelsMap() {return userLabelsMap;}
    public List<Jstruct_UserLabel> getUnusedUserLabels() {return unusedUserLabels;}

    public Integer getActiveRcsbFileCount() { return activeRcsbFileCount;}
    public Integer getActiveManualFileCount() { return activeManualFileCount; }
    public String getRandomStructureUrl() {return randomStructureUrl;}
    public Jstruct_FullStructure getRandomStructure() {return randomFullStructure;}

    
    
    public FileStatus getSelectedFileStatusRcsb() { return selectedFileStatusRcsb; }
    public void setSelectedFileStatusRcsb(FileStatus selectedFileStatusRcsb) { this.selectedFileStatusRcsb = selectedFileStatusRcsb; }

    public FileStatus getSelectedFileStatusManual() { return selectedFileStatusManual; }
    public void setSelectedFileStatusManual(FileStatus selectedFileStatusManual) {this.selectedFileStatusManual = selectedFileStatusManual;}
   
    public SubQueryInput getSubQueryInput() {return subQueryInput;}
    public void setSubQueryInput(SubQueryInput subQuery) { this.subQueryInput = subQuery;}

    public String getSearchSubmitError() {return searchSubmitError;}
    public void setSearchSubmitError(String searchSubmitError) { this.searchSubmitError = searchSubmitError;}
    
    

    
    
    
    //public void onTabChange(TabChangeEvent event) {
        //System.out.println("------------------------------TabChangeEvent: " + event.getTab().getTitle());
        //FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    //}
         
    
    
    
    //constructor
    public HomeBean() {
    }

    
    
    @PostConstruct
    public void init() {
        try {
            
            currentUser = sessionBean.getUser();
            fullStructureService = new FullStructureService(currentUser);
            subQueryInput = new SubQueryInput(currentUser);

            //setup Dropdown Defaults
            selectedFileStatusRcsb = FileStatus.FILE_STATUS_ACTIVE;
            selectedFileStatusManual = FileStatus.FILE_STATUS_ACTIVE;
            
            randomFullStructure = fullStructureService.getRandomActiveRcsbFullStructure();
            if(randomFullStructure != null){
                randomStructureUrl = "/jstruct/api/structure/" + randomFullStructure.getStructureId() + ".0." + randomFullStructure.getActualFileExt();
            }

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    /**
     * called after load (remoteCommand) to display favorites (structures)
     */
    public void retrieveFavoriteFullStructures() {
        try {
            if(!currentUser.isGuest()){
                List<Jstruct_StructureUser> favorites = structureUserService.getFavoritesByUserId(currentUser.getUserId());
                favoriteFullStructures = fullStructureService.getByStructureUsers(favorites, true, false);
            }
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    /**
     * called after load (remoteCommand) to display recents (structures)
     */
    public void retrieveRecentFullStructures() {
        try {
            if(!currentUser.isGuest()){
                List<Jstruct_StructureUser> recents = structureUserService.getRecentsByUserId(currentUser.getUserId(), maxForRecent);
                recentFullStructures = fullStructureService.getByStructureUsers(recents, false, true);
            }
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    /**
     * called after load (remoteCommand) to display saved searches
     */
    public void retrieveSavedUserSearches() {
        try {
            if(!currentUser.isGuest()){
                userSearches = userSearchService.getByUserId(currentUser.getUserId());
            }
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    /**
     * called after load (remoteCommand) to display user labels
     */
    public void retrieveUserLabels() {
        try {

            if (!currentUser.isGuest()) {
                List<Jstruct_UserStructureLabel> userStructureLabels = userStructureLabelService.getByUserId(currentUser.getUserId());

                if (CollectionUtil.hasValues(userStructureLabels)) {
                    userLabelsMap = new LinkedHashMap<>();
                    for (Jstruct_UserStructureLabel usl : userStructureLabels) {
                        Jstruct_UserLabel ul = new Jstruct_UserLabel(usl);
                        if (userLabelsMap.containsKey(ul)) {
                            userLabelsMap.put(ul, userLabelsMap.get(ul) + 1);
                        } else {
                            userLabelsMap.put(ul, 1);
                        }
                    }
                }
                unusedUserLabels = userLabelService.getUnusedByUserId(currentUser.getUserId());
            }

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    /**
     * called after load (remoteCommand) to display metrics (file counts)
     */
    public void retrieveMetrics() {
        try {
             activeRcsbFileCount = structureService.getActiveRcsbFileCount();
             activeManualFileCount = structureService.getActiveManualUploadFileCount();
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
   
    
    /**
     * perform any validation on the home page search form
     * if there are any errors, set the searchSubmitError
     *  ---- for now we'll let the search results page catch any errors ----
     * @param actionEvent 
     */
    public void searchSubmitValidation(ActionEvent actionEvent){
        //todo validate deposit dates - at least one must be entered
        //searchSubmitError = "error message here";
    }

 
    public String searchSubmit(){
    
        if(StringUtil.isSet(searchSubmitError))
        {
            return null;
        }
        
        //cleanup existing b/4 our next search
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);  
        
        FullQueryInput fullQueryInput = new FullQueryInput(selectedFileStatusRcsb, selectedFileStatusManual, QueryMode.AND);
        fullQueryInput.addSubQueryInput(subQueryInput);
        FacesUtil.storeOnSession("fullQueryInput", fullQueryInput);
        
        return "/structure_search.xhtml?faces-redirect=true";
    }
     
    
    
    
    /**
     * called from the home page 'Saved Searches' list
     * based on the selected userSearchId, get da fullQueryInupt data and redirect to the search(results) page
     * @param userSearchId
     * @return 
     */
    public String doSelectedSearch(Long userSearchId){
        try {
            
            //cleanup existing b/4 our next search
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);  
      
            //get search data from DB for the selected userSearchId, and store it on the session for the search (results) page
            FullQueryInput fullQueryInput = PostgresDao.retrieveCriteriaFromDatabase(userSearchId, currentUser);
            FacesUtil.storeOnSession("fullQueryInput", fullQueryInput);

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
        //redirect to search [results] page
        return "/structure_search.xhtml?faces-redirect=true";
        
    }
    
    
    
    
    
    
    //every time a tab is clicked we set it to the current index,
    //so that coming back to this page returns user to same accordian tab
    public void onTabChange(TabChangeEvent event) { 
        
        //todo: make this tons and tons better
       String tabId =  event.getTab().getId();
       int tabIndex = 0;
       if(tabId.equalsIgnoreCase("favoritesTab")){ tabIndex=0; }
       else if(tabId.equalsIgnoreCase("recentsTab")){ tabIndex=1; }
       else if(tabId.equalsIgnoreCase("savedSearchesTab")){ tabIndex=2; }
       else if(tabId.equalsIgnoreCase("labelsTab")){ tabIndex=3; }
       sessionBean.setTabHomeAccordian(tabIndex);
       
    }
    
    
    
    
    
}
