package com.just.jstruct.backing;

import com.just.jstruct.pojos.ExternalLibrary;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;


/**
 * backing bean for the 'about' page
 */ 
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------

@ManagedBean(name="aboutBean")
@RequestScoped
public class AboutBean implements Serializable {
 
    
    private static final long serialVersionUID = 1L;

    
    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private List<ExternalLibrary> externalLibrarys;
    



    
    /* constructor */
    public AboutBean() {
    }
    
    @PostConstruct
    private void init(){
        createPageContent();
    }
    
   
    
    



    // getters/setters
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public List<ExternalLibrary> getExternalLibrarys() {return externalLibrarys; }
    public void setExternalLibrarys(List<ExternalLibrary> externalLibrarys) {this.externalLibrarys = externalLibrarys;}
    
    
    

    private void createPageContent() {
        try {
            
            externalLibrarys = new ArrayList<>();
            
            //add in all the external iibraries
            
            externalLibrarys.add(new ExternalLibrary("Apache Commons", "", "https://commons.apache.org/", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("CoreStruct", "", "https://bitbucket.org/JustBiotherapeutics/corestruct", "GNU GPL 3.0", "https://www.gnu.org/licenses/gpl-3.0.en.html"));
            externalLibrarys.add(new ExternalLibrary("cron-parser", "2.9", "https://github.com/RedHogs/cron-parser", "MIT", "https://opensource.org/licenses/MIT"));
            externalLibrarys.add(new ExternalLibrary("Google API Client Library for Java", "1.20","https://developers.google.com/api-client-library/java/", "Apache 2.0","http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("Hibernate", "5.2", "http://hibernate.org/", "LGPL v2.1", "http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html"));
            externalLibrarys.add(new ExternalLibrary("HikariCP", "2.5", "http://brettwooldridge.github.io/HikariCP/", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("Java Hamcrest", "1.3", "http://hamcrest.org/JavaHamcrest/", "BSD License", "https://opensource.org/licenses/BSD-3-Clause"));
            externalLibrarys.add(new ExternalLibrary("javax-servlet", "1.2", "https://jstl.java.net/", "Common Development and Distribution License (CDDL) Version 1.0", "https://opensource.org/licenses/CDDL-1.0"));
            externalLibrarys.add(new ExternalLibrary("Joda-Time", "2.4", "http://www.joda.org/joda-time/", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("JUnit", "4.12", "http://junit.org/junit4/", "Eclipse Public License 1.0", "http://www.eclipse.org/legal/epl-v10.html"));
            externalLibrarys.add(new ExternalLibrary("Jackson JSON Processor", "2.1.3", "http://wiki.fasterxml.com/JacksonHome", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("Jsoup", "1.10", "https://jsoup.org/", "MIT", "https://opensource.org/licenses/MIT"));
            externalLibrarys.add(new ExternalLibrary("log4j", "1.2", "http://logging.apache.org/log4j/1.2/", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("Maven", "3.3", "https://maven.apache.org/", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("PostgreSQL JDBC Driver", "9.4", "https://jdbc.postgresql.org/", "BSD License", "https://opensource.org/licenses/BSD-3-Clause"));
            externalLibrarys.add(new ExternalLibrary("PrimeFaces", "6", "http://www.primefaces.org/", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("Quartz", "2.2", "http://www.quartz-scheduler.org/", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0"));
            externalLibrarys.add(new ExternalLibrary("SAX (Simple API for XML)", "2", "http://www.saxproject.org/", "Public Domain", "http://www.saxproject.org/copying.html"));
            externalLibrarys.add(new ExternalLibrary("SLF4J", "1.7", "http://www.slf4j.org/", "MIT", "https://opensource.org/licenses/MIT"));
            externalLibrarys.add(new ExternalLibrary("startools", "0.2", "http://www.globalphasing.com/startools/", "'BSD-like licence'", "http://www.globalphasing.com/startools/javadoc/StarToolsLicence.txt"));
            externalLibrarys.add(new ExternalLibrary("NGL iewer", "", "https://github.com/arose/ngl", "MIT", "https://opensource.org/licenses/MIT"));
            //externalLibrarys.add(new ExternalLibrary("", "", "", "", ""));
            //externalLibrarys.add(new ExternalLibrary(libraryName, libraryVersion, libraryURL, licenseName, licenseUrl));
                    
            //add libraries that have multiple licenses
            Map<String, String> comHfgLicenseMap = new HashMap<>(2);
            comHfgLicenseMap.put("GNU Library or Lesser General Public License version 2.0", "https://www.gnu.org/licenses/old-licenses/lgpl-2.0.en.html");
            comHfgLicenseMap.put("GNU Library or Lesser General Public License version 3.0", "https://www.gnu.org/licenses/lgpl-3.0.en.html");
            externalLibrarys.add(new ExternalLibrary("com-hfg", "", "https://sourceforge.net/projects/com-hfg/", comHfgLicenseMap));
            
            Map<String, String> javamailHfgLicenseMap = new HashMap<>(2);
            javamailHfgLicenseMap.put("Common Development and Distribution License (CDDL) v1.1",      "https://glassfish.java.net/public/CDDL+GPL_1_1.htmll");
            javamailHfgLicenseMap.put("GNU General Public License (GPL) v2 with Classpath Exception", "http://openjdk.java.net/legal/gplv2+ce.html");
            externalLibrarys.add(new ExternalLibrary("JavaMail API", "1.5.4", "https://java.net/projects/javamail/pages/Home", javamailHfgLicenseMap));
            
            Collections.sort(externalLibrarys); 
           
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }

    }

    


    
}
