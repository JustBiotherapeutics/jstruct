package com.just.jstruct.backing;

import com.hfg.util.StringUtil;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * back the page that manages Roles
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------

@ManagedBean(name="rolesBean")
@RequestScoped
public class RolesBean implements Serializable {
 
    private static final long serialVersionUID = 1L;

    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private List<Jstruct_User> users;
    private Jstruct_User newUser = new Jstruct_User();


    /* supporting service classes */
    JstructUserService userService = new JstructUserService();

    
    /* constructor */
    public RolesBean() {}
    
    @PostConstruct
    private void init(){
        try {
            
            checkPagePermissions();
            
            //all records from the users table
            users = userService.getAll();
           
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }

    //access to this page is avaliable only for Admin/Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleAdmin() && !sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access User/Roles page",
                                                            "User/Roles",
                                                            "n/a"));
        }
    }
    
    /**
     * 
     * @param actionEvent 
     */
    public void toggleStatus(ActionEvent actionEvent) {
        try {
            Long userId = (Long) actionEvent.getComponent().getAttributes().get("userId");
            Jstruct_User user = userService.getById(userId);
            
            user.setStatus(user.getStatus().next());
            userService.updateJstructUser(user);
            
            users = userService.getAll();
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "User status updated", user.getStatus().name()));
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    

    
    /**
     * 
     * @param actionEvent 
     */
    public void toggleContributor(ActionEvent actionEvent) {
        try {
            Long userId = (Long) actionEvent.getComponent().getAttributes().get("userId");
            toggleRole(userId, "contributor");
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     * @param actionEvent 
     */
    public void toggleAdmin(ActionEvent actionEvent) {
        try {
            Long userId = (Long) actionEvent.getComponent().getAttributes().get("userId");
            toggleRole(userId, "admin");
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     * @param actionEvent 
     */
    public void toggleDeveloper(ActionEvent actionEvent) {
        try {
            Long userId = (Long) actionEvent.getComponent().getAttributes().get("userId");
            toggleRole(userId, "developer");
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    /**
     * 
     * @param userId
     * @param role
     * @throws JDAOException
     */
    private void toggleRole(Long userId, String role) throws JDAOException {

        Jstruct_User user = userService.getById(userId);

        switch (role){
            case "contributor":
                user.setRoleContributor(!user.getIsRoleContributor());
                break;
            case "admin":
                user.setRoleAdmin(!user.getIsRoleAdmin());
                break;
            case "developer":
                user.setRoleDeveloper(!user.getIsRoleDeveloper());
                break;
        }
        userService.updateJstructUser(user);
        users = userService.getAll();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "User role updated", roleDetailMsg(user)));

    }
    
    
    private String roleDetailMsg(Jstruct_User user) {
        
        List<String> roles = new ArrayList<>(1);

        if (user.getIsRoleContributor()) {
            roles.add("Contributor");
        }
        if (user.getIsRoleAdmin()) {
            roles.add("Admin");
        }
        if (user.getIsRoleDeveloper()) {
            roles.add("Developer");
        }
        return user.getName() + " roles: " + StringUtil.join(roles, ", ");
    }
    
    
    

    // getters/setters
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    public List<Jstruct_User> getUsers() { return users; }
    public void setUsers(List<Jstruct_User> users) { this.users = users; }
    
    public Jstruct_User getNewUser() { return newUser; }
    public void setNewUser(Jstruct_User newUser) { this.newUser = newUser; }
    
    
    
    
    
    
 
  
    
    



    
}
