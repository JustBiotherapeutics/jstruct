package com.just.jstruct.backing;

import com.just.jstruct.dao.service.ValueMapService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.pojos.ValueMapGroup;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * list of all records from VALUE_MAP table
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="propertiesDynamic")
@RequestScoped
public class PropertiesDynamic implements Serializable {
 
    private static final long serialVersionUID = 1L;

    /* data members */
    private List<ValueMapGroup> valueMapGroups;


    /* supporting service classes */
    ValueMapService valueMapService = new ValueMapService();

    
    /* constructor */
    public PropertiesDynamic() {
    }
    
    @PostConstruct
    public void init() {
        try {
            //all records from the ValueMap table
            valueMapGroups = valueMapService.getValueMapGroups();
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }



    // getters/setters
    public List<ValueMapGroup> getValueMapsGroups() { return valueMapGroups; }
    public void setValueMapsGroups(List<ValueMapGroup> valueMapGroups) { this.valueMapGroups = valueMapGroups; }
    



    


    
}
