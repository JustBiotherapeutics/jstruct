package com.just.jstruct.backing;

import com.hfg.util.StringUtil;
import com.just.bio.structure.Structure;
import com.just.bio.structure.exceptions.StructureParseException;
import com.just.bio.structure.pdb.PdbFileParse;
import com.just.jstruct.dao.service.ImportJobService;
import com.just.jstruct.dao.service.PdbFileService;
import com.just.jstruct.dao.service.PdbJrnlService;
import com.just.jstruct.dao.service.StructFileService;
import com.just.jstruct.dao.service.StructVersionService;
import com.just.jstruct.dao.service.StructureService;
import com.just.jstruct.enums.JobStatus;
import com.just.jstruct.enums.JobType;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.exception.JImportException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_Chain;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_PdbFile;
import com.just.jstruct.model.Jstruct_PdbJrnl;
import com.just.jstruct.model.Jstruct_StructFile;
import com.just.jstruct.model.Jstruct_StructVersion;
import com.just.jstruct.model.Jstruct_Structure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.BulkFileItem;
import com.just.jstruct.pojos.DisplaySequence;
import com.just.jstruct.pojos.JFileAttributes;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.sequenceProcessing.SequenceService;
import com.just.jstruct.structureImport.ImportService;
import com.just.jstruct.structureImport.PdbDataUtils;
import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JFileUtils;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.NoAccess;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 * Backing bean for the structure page
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "newStructureBean")
@ViewScoped
public class NewStructureBean implements Serializable {

    private static final long serialVersionUID = 1L;

    public static String JSTRUCT_TEMP_DIR = "/tmp/tmp_request/jstruct/";

    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;

    private Long replaceStructVerId;
    private String replaceJstructId;
    private String currentStep;  //upload_file, add_data_one, add_data_multiple
    
    private String titlePrepend;
    private String titleAppend; 
    private boolean titleFromFilename;

    private List<File> files = new ArrayList<>();
    private List<BulkFileItem> bulkFileItems;
    private boolean singleFileIsParsable;

    private Jstruct_ImportJob importJob;

    private Jstruct_StructVersion previousStructVersion;
    private Jstruct_Structure previousStructure;
    private Jstruct_StructFile previousStructFile;
    private Jstruct_PdbFile previousPdbFile;
    private Jstruct_PdbJrnl previousPdbJrnl;

    private Jstruct_StructVersion nextStructVersion;
    private Jstruct_Structure nextStructure = null;
    private Jstruct_StructFile tempStructFile = new Jstruct_StructFile();
    private Jstruct_PdbFile nextPdbFile = new Jstruct_PdbFile();
    private Jstruct_PdbJrnl nextPdbJrnl = new Jstruct_PdbJrnl();

    private Jstruct_FullStructure tempFullStructure;
    private JFileAttributes tempFileAttributes;
    private List<DisplaySequence> tempDisplaySequences;

    /* service classes */
    private final ImportJobService importJobService = new ImportJobService();
    private final StructureService structureService = new StructureService();
    private final StructVersionService structVersionService = new StructVersionService();
    private final StructFileService structFileService = new StructFileService();
    private final PdbFileService pdbFileService = new PdbFileService();
    private final PdbJrnlService pdbJrnlService = new PdbJrnlService();
    //private final AminoAcidService aminoAcidService = new AminoAcidService();

    //constructor
    public NewStructureBean() {
    }

    @PostConstruct
    public void init() {
        try {
            
            checkPagePermissions();
            
            currentUser = sessionBean.getUser();
            nextStructure = new Jstruct_Structure(currentUser);

            importJob = new Jstruct_ImportJob(currentUser, JobType.MANUAL_IMPORT, JobStatus.IMPORTING);
            importJobService.add(importJob);

            currentStep = "upload_file";

            String replaceStructVerIdParam = FacesUtil.getRequestParameter("id");

            if (StringUtil.isSet(replaceStructVerIdParam)) {

                replaceStructVerId = JStringUtil.stringToLong(replaceStructVerIdParam);

                previousStructVersion = structVersionService.getByStructVerId(replaceStructVerId);
                previousStructure = structureService.getByStructureId(previousStructVersion.getStructureId());
                previousStructFile = structFileService.getByStructVerId(replaceStructVerId);
                previousPdbFile = pdbFileService.getByStructVerId(replaceStructVerId);
                previousPdbJrnl = pdbJrnlService.getPrimaryByStructVerId(replaceStructVerId, true);
                
                if (previousStructure.isFromRcsb()) {
                    throw new JDAOException("Structure ID '" + replaceStructVerIdParam + "' evaluates to a structure from RCSB. These structures cannot be manually replaced.");
                }
                
                replaceJstructId = previousStructVersion.getStructureId() + "." + previousStructVersion.getVersion();

                nextStructure = structureService.getByStructureId(previousStructVersion.getStructureId());
                nextStructVersion = clonePrevToNextStructVersion();
                nextPdbJrnl = clonePrevToNextJrnl();

            } else {
                nextStructVersion = new Jstruct_StructVersion(currentUser);
                nextStructVersion.setVersion(1);

                nextPdbJrnl.setPrimary(true);
            }

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    //access to this page is avaliable only for Admin/Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleContributor() && !sessionBean.getUser().getIsRoleAdmin() && !sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access New Structure page",
                                                            "New Structure",
                                                            "n/a"));
        }
    }

    // ********************************************************************************************************************************************
    //     actions/actionlisteners from UI
    // ********************************************************************************************************************************************
    
    
    public void cancelFromUploadFile(ActionEvent actionEvent) {

        importJob.setImportStatus(JobStatus.CANCELLED);

        if (isReplaceMode()) {
            //if replacing, redirect back to that strucure
            FacesUtil.redirectRequest("/structure.xhtml?id=" + replaceJstructId + "&faces-redirect=true");
        } else {
            //else, back to last page before we came to this page
            String previousPage = (String) FacesUtil.retrieveFromSession("lastPageBeforeNewStructurePage");
            FacesUtil.redirectRequest("/" + previousPage + "?faces-redirect=true");
        }
    }

    public void cancelFromAddData(ActionEvent actionEvent) {
        //remove uploaded file(s) from server
        for (File file : files) {
            file.delete();    //delete from JSTRUCT_TEMP_DIR (/tmp/tmp_request/jstruct/)
        }
        cancelFromUploadFile(actionEvent);
    }

    /**
     * this just uploads the files to our temp directory. right after it's
     * finished the front-end calls the function uploadFileComplete() to process
     * all files (which could be one or many)
     *
     * @param event
     */
    public void uploadFileEvent(FileUploadEvent event)  {
        try {

            UploadedFile uploadedFile = event.getFile();
            File file = uploadedFileToTempDir(uploadedFile);
            files.add(file);
            
            Thread.sleep(500); //gotta stall as sometimes the uploadFileComplete gets called before the last file is actually uploaded!

        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }

    /**
     * process the one or many files that were just uploaded
     * 
     * @param actionEvent
     */
    public void uploadFileComplete(ActionEvent actionEvent) {
        try {
            if (files.size() == 1) {
                processSingleFile(files.get(0));
                currentStep = "add_data_one";
            } else {
                processMultipleFiles();
                currentStep = "add_data_multiple";
            }
        } catch (IOException | JException | JDAOException | StructureParseException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }

    /**
     * clicking the UploadData button when ONE file is uploaded
     * @return 
     */
    public String addDataOne() {
        String returnString = "";
        try {

            //field verification
            if (!singleFileIsParsable && !StringUtil.isSet(nextPdbFile.getTitle())) {

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Title is required", null);
                FacesContext.getCurrentInstance().addMessage(null, msg);

            } else {

                File file = files.get(0); //there's only one file at this point.

                //upload file (et al), then delete the file
                Jstruct_StructVersion newStructVersion = ImportService.uploadManualImportStructureStuff(singleFileIsParsable, importJob, file, nextStructure, nextStructVersion, nextPdbFile, nextPdbJrnl, currentUser);
                file.delete(); //delete from JSTRUCT_TEMP_DIR (/tmp/tmp_request/jstruct/)

                returnString = "/structure.xhtml?faces-redirect=true&id=" + newStructVersion.getStructureId();
            }

        } catch (Exception ex) {
            HijackError.gotoErrorPage(ex);
        }
        return returnString;
    }

    
    /**
     * clicking the UploadData button when MULTIPLE files are uploaded
     * @return 
     */
    public String addDataMultiple() {
        String returnString = "";
        try {
            
            //validate fields of all entries
            if (validateBulkEntries()) {

                List<Jstruct_StructVersion> newStructVersions = new ArrayList<>(bulkFileItems.size());

                //import, then delete each file
                for (BulkFileItem bfi : bulkFileItems) {
                    Jstruct_StructVersion structVersion = ImportService.uploadManualImportStructureStuff(bfi.isPdbOrCifFile(), importJob, bfi.getFile(), bfi.getStructure(), bfi.getStructVersion(), bfi.getPdbFile(), new Jstruct_PdbJrnl(), currentUser);
                    newStructVersions.add(structVersion);
                    bfi.getFile().delete();    
                }
                
                //all files uploaded! redirect to search results page with all jstruct ids as criteria
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);  //cleanup any existing searches...
                FacesUtil.storeOnSession("fullQueryInput", createFullQueryInput(newStructVersions));
                returnString = "/structure_search.xhtml?faces-redirect=true";

            } else {
                
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error submitting form..", null);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }

        } catch (Exception ex) {
            HijackError.gotoErrorPage(ex);
        }
        return returnString;
    }

    
    private boolean validateBulkEntries(){
        
        boolean isValid = true;
        
        for (BulkFileItem bfi : bulkFileItems) {
            bfi.clearValidationErrors();
            
            if(bfi.isPdbOrCifFile()){
                //todo any validation specifically for pdb/cif files
            } else {
                if(!StringUtil.isSet(bfi.getPdbFile().getTitle())){
                    bfi.addValidationErrors("Title is required");
                    isValid = false;
                }
                
            }
            //any validation for all variations of files goes here.
            if(ValueMapUtils.getInstance().isClientSupportTypeIsRequired() && bfi.getStructVersion().getClientId()==null){
                bfi.addValidationErrors("Client is required");
                isValid = false;
            }
            
        }
        
        return isValid;
    }
    
    private FullQueryInput createFullQueryInput(List<Jstruct_StructVersion> newStructVersions) {

        List<String> ids = new ArrayList<>(newStructVersions.size());
        for (Jstruct_StructVersion structVersion : newStructVersions) {
            ids.add(JStringUtil.longToString(structVersion.getStructureId()));
        }

        FullQueryInput fullQueryInput = new FullQueryInput(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.AND);
        SubQueryInput subQueryInput = new SubQueryInput(currentUser);
        subQueryInput.setSelectedSubqueryType(SubQueryType.IDENTIFIER); 
        subQueryInput.setInputIdentifier(StringUtil.join(ids, ", "));
        fullQueryInput.addSubQueryInput(subQueryInput);

        return fullQueryInput;

    }
    
    
    
    
    // ********************************************************************************************************************************************
    //     private methods
    // ********************************************************************************************************************************************
    private File uploadedFileToTempDir(UploadedFile uploadedFile) throws IOException {

        JFileUtils.createDirectoryIfNotAlready(JSTRUCT_TEMP_DIR);

        String filename = FilenameUtils.getName(uploadedFile.getFileName());
        InputStream input = uploadedFile.getInputstream();
        OutputStream output = new FileOutputStream(new File(JSTRUCT_TEMP_DIR, filename));

        File file = new File(JSTRUCT_TEMP_DIR, filename);

        try {
            IOUtils.copy(input, output);
        } finally {
            IOUtils.closeQuietly(input);
            IOUtils.closeQuietly(output);
        }

        return file;

    }

    private void processSingleFile(File file) throws IOException, JException, JDAOException, StructureParseException {

        String filename = file.getName();
        singleFileIsParsable = JFileUtils.isPdbOrCifFile(file);

        if (singleFileIsParsable) {
            nextPdbFile = getTempPdbFile(file, importJob);

            PdbFileParse pdbFileParse = new PdbFileParse();
            Structure structureObj = pdbFileParse.executeParseByLocalFile(file);
            SequenceService sequenceService = new SequenceService(0L, structureObj);
            List<Jstruct_Chain> chains = sequenceService.createChains();
            tempDisplaySequences = new ArrayList<>(chains.size());
            for (Jstruct_Chain chain : chains) {
                tempDisplaySequences.add(new DisplaySequence(chain, "Index:"));
            }

        } else if (replaceStructVerId != null) {
            //if it's not parseable, we'll check for a previous version and copy its values
            clonePrevToNextPdbFile();
        }

        
        if (isReplaceMode()) {

            tempFullStructure = new Jstruct_FullStructure(previousStructure, nextStructVersion, tempStructFile, nextPdbFile);

        } else {

            nextStructure.setFromRcsb(false);
            nextStructure.setObsolete(false);
            nextStructure.setOwner(currentUser);
            nextStructure.setOwnerId(currentUser.getUserId());
            nextStructure.setSourceName(ValueMapUtils.getInstance().getManualUploadSource());

            tempFullStructure = new Jstruct_FullStructure(nextStructure, nextStructVersion, tempStructFile, nextPdbFile);
            
            //if not replacemode AND it's not parsable, create title from user input
            if(!singleFileIsParsable){
                nextPdbFile.setTitle(createTitleFromUi(filename)); 
            }
        }

        
        Path filePath = Paths.get(file.getPath());
        BasicFileAttributes attr = Files.getFileAttributeView(filePath, BasicFileAttributeView.class).readAttributes();

        tempFileAttributes = new JFileAttributes(tempFullStructure);
        tempFileAttributes.setActualFileName(filename);
        tempFileAttributes.setUploadFileName(filename);
        tempFileAttributes.setCreationDate(new Date(attr.creationTime().toMillis()));
        tempFileAttributes.setModifiedDate(new Date(attr.lastModifiedTime().toMillis()));
        tempFileAttributes.setUncompressedSizeDisplay(FileUtils.byteCountToDisplaySize(file.length()));

    }

    private Jstruct_PdbFile getTempPdbFile(File file, Jstruct_ImportJob importJob) throws JException {
        Jstruct_PdbFile tempPdbFile;
        try {
            tempPdbFile = PdbDataUtils.createTempPdbFileFromParsedData(file, importJob, currentUser);
        } catch (JImportException ex) {
            throw new JException("Error attempting to retrieve temporary PDB Structure file.", ex);
        }
        return tempPdbFile;
    }

    
    
    //todo move this to service class
    private Jstruct_StructVersion cloneStructVersion(Jstruct_StructVersion originalStructVer){
        
        Jstruct_StructVersion clonedStructVersion = new Jstruct_StructVersion(currentUser);
        
        clonedStructVersion.setStructureId(originalStructVer.getStructureId());
        clonedStructVersion.setVersion(1);
        clonedStructVersion.setStructMethod(originalStructVer.getStructMethod());
        clonedStructVersion.setClientId(originalStructVer.getClientId());
        clonedStructVersion.setProgramId(originalStructVer.getProgramId());
        clonedStructVersion.setTarget(originalStructVer.getTarget());
        clonedStructVersion.setDescription(originalStructVer.getDescription());
        clonedStructVersion.setModel(originalStructVer.isModel());
        
        return clonedStructVersion;
    }
    
    
    
    private Jstruct_StructVersion clonePrevToNextStructVersion() throws JDAOException {

        Jstruct_StructVersion sv = cloneStructVersion(previousStructVersion);
        Jstruct_StructVersion youngestByStructureId = structVersionService.getYoungestByStructureId(previousStructVersion.getStructureId());
        sv.setVersion(youngestByStructureId.getVersion() + 1);

        return sv;
    }

    private Jstruct_PdbJrnl clonePrevToNextJrnl() {
        if (previousPdbJrnl == null) {
            nextPdbJrnl = new Jstruct_PdbJrnl();
        } else {
            nextPdbJrnl.setAuth(previousPdbJrnl.getAuth());
            nextPdbJrnl.setTitl(previousPdbJrnl.getTitl());
        }
        nextPdbJrnl.setPrimary(true);
        return nextPdbJrnl;
    }

    private void clonePrevToNextPdbFile() {
        nextPdbFile.setPdbIdentifier(previousPdbFile.getPdbIdentifier());
        nextPdbFile.setTitle(previousPdbFile.getTitle());
        nextPdbFile.setClassification(previousPdbFile.getClassification());
        nextPdbFile.setCaveat(previousPdbFile.getCaveat());
        nextPdbFile.setExpdta(previousPdbFile.getExpdta());
        nextPdbFile.setNummdl(previousPdbFile.getNummdl());
        nextPdbFile.setMdltyp(previousPdbFile.getMdltyp());
        nextPdbFile.setResolutionLine(previousPdbFile.getResolutionLine());
        nextPdbFile.setResolutionUnit(previousPdbFile.getResolutionUnit());
        nextPdbFile.setResolutionMeasure(previousPdbFile.getResolutionMeasure());
        if (previousPdbJrnl != null) {
            nextPdbJrnl.setAuth(previousPdbJrnl.getAuth());
            nextPdbJrnl.setTitl(previousPdbJrnl.getTitl());
        }
    }

    private void processMultipleFiles() {

        bulkFileItems = new ArrayList<>(files.size());

        for (File file : files) {
            
            BulkFileItem bfi = new BulkFileItem(file, currentUser, createTitleFromUi(file.getName()));   //create title from ui is ignored if this is a pdb/cif file
            bfi.setStructVersion(cloneStructVersion(nextStructVersion)); //user entered metadata from first step
            
            bulkFileItems.add(bfi);

        }

    }
    
    
    
    private String createTitleFromUi(String filename) {
        StringBuilder sb = new StringBuilder();
        sb.append(titlePrepend);
        if (titleFromFilename) {
            sb.append(JStringUtil.onlyTextBeforeLastOccurance(filename, "."));
        }
        sb.append(titleAppend);
        return sb.toString();
    }

    // ********************************************************************************************************************************************
    //     getters/setters
    // ********************************************************************************************************************************************
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public boolean isReplaceMode() {
        return replaceStructVerId != null;
    }

    public boolean isUploadFileStep() {
        return "upload_file".equalsIgnoreCase(currentStep);
    }

    public boolean isAddDataOneStep() {
        return "add_data_one".equalsIgnoreCase(currentStep);
    }

    public boolean isAddDataMultipleStep() {
        return "add_data_multiple".equalsIgnoreCase(currentStep);
    }

    public String getTitlePrepend() {
        return titlePrepend;
    }

    public void setTitlePrepend(String titlePrepend) {
        this.titlePrepend = titlePrepend;
    }

    public String getTitleAppend() {
        return titleAppend;
    }

    public void setTitleAppend(String titleAppend) {
        this.titleAppend = titleAppend;
    }

    public boolean isTitleFromFilename() {
        return titleFromFilename;
    }

    public void setTitleFromFilename(boolean titleFromFilename) {
        this.titleFromFilename = titleFromFilename;
    }

    
    
    public Long getReplaceStructVerId() {
        return replaceStructVerId;
    }

    public void setReplaceStructVerId(Long replaceStructVerId) {
        this.replaceStructVerId = replaceStructVerId;
    }
    
    public String getReplaceJstructId() {
        return replaceJstructId;
    }

    public void setReplaceJstructId(String replaceJstructId) {
        this.replaceJstructId = replaceJstructId;
    }
    
    

    public boolean isParsable() {
        return singleFileIsParsable;
    }

    public void setParsable(boolean singleFileIsParsable) {
        this.singleFileIsParsable = singleFileIsParsable;
    }

    public List<BulkFileItem> getBulkFileItems() {
        return bulkFileItems;
    }

    public void setBulkFileItems(List<BulkFileItem> bulkFileItems) {
        this.bulkFileItems = bulkFileItems;
    }

    public Jstruct_StructVersion getPreviousStructVersion() {
        return previousStructVersion;
    }

    public void setPreviousStructVersion(Jstruct_StructVersion previousStructVersion) {
        this.previousStructVersion = previousStructVersion;
    }

    public Jstruct_Structure getPreviousStructure() {
        return previousStructure;
    }

    public void setPreviousStructure(Jstruct_Structure previousStructure) {
        this.previousStructure = previousStructure;
    }

    public Jstruct_StructFile getPreviousStructFile() {
        return previousStructFile;
    }

    public void setPreviousStructFile(Jstruct_StructFile previousStructFile) {
        this.previousStructFile = previousStructFile;
    }

    public Jstruct_PdbFile getPreviousPdbFile() {
        return previousPdbFile;
    }

    public void setPreviousPdbFile(Jstruct_PdbFile previousPdbFile) {
        this.previousPdbFile = previousPdbFile;
    }

    public Jstruct_PdbJrnl getPreviousPdbJrnl() {
        return previousPdbJrnl;
    }

    public void setPreviousPdbJrnl(Jstruct_PdbJrnl previousPdbJrnl) {
        this.previousPdbJrnl = previousPdbJrnl;
    }

    public Jstruct_StructVersion getNextStructVersion() {
        return nextStructVersion;
    }

    public void setNextStructVersion(Jstruct_StructVersion nextStructVersion) {
        this.nextStructVersion = nextStructVersion;
    }

    public Jstruct_Structure getNextStructure() {
        return nextStructure;
    }

    public void setNextStructure(Jstruct_Structure nextStructure) {
        this.nextStructure = nextStructure;
    }

    public Jstruct_StructFile getTempStructFile() {
        return tempStructFile;
    }

    public void setTempStructFile(Jstruct_StructFile nextStructFile) {
        this.tempStructFile = nextStructFile;
    }

    public Jstruct_PdbFile getNextPdbFile() {
        return nextPdbFile;
    }

    public void setNextPdbFile(Jstruct_PdbFile nextPdbFile) {
        this.nextPdbFile = nextPdbFile;
    }

    public Jstruct_FullStructure getTempFullStructure() {
        return tempFullStructure;
    }

    public void setTempFullStructure(Jstruct_FullStructure tempFullStructure) {
        this.tempFullStructure = tempFullStructure;
    }

    public JFileAttributes getTempFileAttributes() {
        return tempFileAttributes;
    }

    public void setTempFileAttributes(JFileAttributes tempFileAttributes) {
        this.tempFileAttributes = tempFileAttributes;
    }

    public List<DisplaySequence> getTempDisplaySequences() {
        return tempDisplaySequences;
    }

    public void setTempDisplaySequences(List<DisplaySequence> tempDisplaySequences) {
        this.tempDisplaySequences = tempDisplaySequences;
    }

    public Jstruct_PdbJrnl getNextPdbJrnl() {
        return nextPdbJrnl;
    }

    public void setNextPdbJrnl(Jstruct_PdbJrnl nextPdbJrnl) {
        this.nextPdbJrnl = nextPdbJrnl;
    }

}
