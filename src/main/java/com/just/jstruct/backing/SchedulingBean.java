package com.just.jstruct.backing;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.ImportJobService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_ImportMsg;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JDateUtil;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="schedulingBean")
@ViewScoped
public class SchedulingBean implements Serializable {

    private static final long serialVersionUID = 1L;

    
    /* service classes */
    private final ImportJobService importJobService = new ImportJobService();
    
    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private SchedulingController controller;
    private List<Jstruct_ImportJob> importJobs;
    
    private Jstruct_ImportJob selectedImportJob;
    private String selectedImportJobElapsedTime;

    private Jstruct_ImportMsg selectedImportMsg;
    
    
    /* constructor */
    public SchedulingBean(){ 
    }
    
    @PostConstruct
    public void init() {
        try {
            checkPagePermissions();
            
            controller = new SchedulingController();
            importJobs = importJobService.getAllRcsbImportJobs();
            
            if(selectedImportJob == null){
                getMostRecentImportJobInList();
            }
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    //access to this page is avaliable only for Admin/Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleAdmin() && !sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access page managing scheduling and import jobs/data",
                                                            "Schedule", "n/a"));
        }
    }
    
    
     
    /* getters/setters */
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public SchedulingController getController() { return controller; }

    public List<Jstruct_ImportJob> getImportJobs() { return importJobs; }
    public void setImportJobs(List<Jstruct_ImportJob> importJobs) { this.importJobs = importJobs; }

    public Jstruct_ImportJob getSelectedImportJob() { return selectedImportJob;}
    public void setSelectedImportJob(Jstruct_ImportJob selectedImportJob) { this.selectedImportJob = selectedImportJob; }

    public String getSelectedImportJobElapsedTime() { return selectedImportJobElapsedTime; }
    public void setSelectedImportJobElapsedTime(String selectedImportJobElapsedTime) {this.selectedImportJobElapsedTime = selectedImportJobElapsedTime; }

    public Jstruct_ImportMsg getSelectedImportMsg() { return selectedImportMsg; }
    public void setSelectedImportMsg(Jstruct_ImportMsg selectedImportMsg) { this.selectedImportMsg = selectedImportMsg; }

    
    


    /**
     * only called if there is no currently selected import job (page load)
     * @throws JDAOException 
     */
    private void getMostRecentImportJobInList() throws JDAOException
    {
        if(CollectionUtil.hasValues(importJobs))
        {
            selectedImportJob = importJobs.get(0);
            for(Jstruct_ImportJob ij : importJobs)
            {
                if(ij.getImportJobId() > selectedImportJob.getImportJobId()){
                    selectedImportJob = ij;
                }
            }
            //need to re-get the import job with all it's importRests
            selectedImportJob = importJobService.getByImportJobId(selectedImportJob.getImportJobId(), true, true);
            selectedImportJobElapsedTime = JDateUtil.differenceBetweenDatesToElapsedTime(selectedImportJob.getRunStart(), selectedImportJob.getRunEnd());
        }
    }
    
    
    
    
    
    
    public void selectImportJob(ActionEvent actionEvent) {
        try {
            Long selectedImportJobId = (Long) actionEvent.getComponent().getAttributes().get("selectedImportJobId");
            selectedImportJob = importJobService.getByImportJobId(selectedImportJobId, true, true);
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }





    public void prepForErrorDlg(String importMsgIdParam)
    {
        Long importMsgId = JStringUtil.stringToLong(importMsgIdParam);

        for(Jstruct_ImportMsg importMsg : selectedImportJob.getImportMsgs())
        {
            if(importMsg.getImportMsgId().equals(importMsgId))
            {
                selectedImportMsg = importMsg;
            }
        }
    }
    
    
    
    
    
     
}