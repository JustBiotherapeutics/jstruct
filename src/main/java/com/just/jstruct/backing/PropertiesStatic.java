package com.just.jstruct.backing;

import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Properties;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


/**
 * list of static application properties (what's in the ApplicationResources.properties file)
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="propertiesStatic")
@RequestScoped
public class PropertiesStatic implements Serializable {
 
    private static final long serialVersionUID = 1L;

    /* data members */
    private TreeMap applicationProperties = new TreeMap();

    
    /* constructor */
    public PropertiesStatic() {
    }
    
    @PostConstruct
    public void init() {
        retrieveProperties();
    }


    // getters/setters
    public TreeMap getApplicationProperties() { return applicationProperties; }
    public void setApplicationProperties(TreeMap applicationProperties) { this.applicationProperties = applicationProperties; }



    /**
     * gotta get 'em all
     */
    private void retrieveProperties() {
        try {
            
            //get all properties from the ApplicationResources.properties file
            Properties props = ValueMapUtils.getInstance().getAllProperties();
            Enumeration e = props.propertyNames();
            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                String val = props.getProperty(key);
                applicationProperties.put(key, val);
            }
            
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }

    }

    
    


    
}
