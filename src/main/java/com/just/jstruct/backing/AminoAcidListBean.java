package com.just.jstruct.backing;

import com.just.bio.structure.AminoAcid3D;
import com.just.bio.structure.format.pdb.PDB_AtomName;
import com.just.jstruct.dao.service.AminoAcidService;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;


/**
 *  back the page that manages amino acids
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------

@ManagedBean(name="aminoAcidList")
@RequestScoped
public class AminoAcidListBean implements Serializable {
 
    private static final long serialVersionUID = 1L;

    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private List<AminoAcid3D> aminoAcids;
    private HashMap<String, Character> nucleicAcidMap;
    private List<PDB_AtomName> bkbnAtoms;
    private List<PDB_AtomName> bkbnCbAtoms;
    
    
    //private List<Jstruct_AminoAcid> aminoAcids;
    //private Jstruct_AminoAcid newAminoAcid = new Jstruct_AminoAcid();


    /* supporting service classes */
    private final AminoAcidService aminoAcidService = new AminoAcidService();

    
    /* constructor */
    public AminoAcidListBean() {
    }
    
    @PostConstruct
    private void init(){
        checkPagePermissions();
        retrieveAminoAcids();
    }
    
    //access to this page is avaliable only for Admin/Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleAdmin() && !sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Amino Acids definition management page",
                                                            "Amino Acids",
                                                            "n/a"));
        }
    }
    
    



    // getters/setters
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    public List<AminoAcid3D> getAminoAcids() { return aminoAcids; }
    public void setAminoAcids(List<AminoAcid3D> aminoAcids) { this.aminoAcids = aminoAcids; }

    public HashMap<String, Character> getNucleicAcidMap() {return nucleicAcidMap; }
    public void setNucleicAcidMap(HashMap<String, Character> nucleicAcidMap) {this.nucleicAcidMap = nucleicAcidMap; }

    public List<PDB_AtomName> getBkbnAtoms() {return bkbnAtoms; }
    public void setBkbnAtoms(List<PDB_AtomName> bkbnAtoms) {this.bkbnAtoms = bkbnAtoms; }

    public List<PDB_AtomName> getBkbnCbAtoms() {return bkbnCbAtoms; }
    public void setBkbnCbAtoms(List<PDB_AtomName> bkbnCbAtoms) {this.bkbnCbAtoms = bkbnCbAtoms; }
    
    
    
    
    
    //public Jstruct_AminoAcid getNewAminoAcid() { return newAminoAcid; }
    //public void setNewAminoAcid(Jstruct_AminoAcid newAminoAcid) { this.newAminoAcid = newAminoAcid; }
    
    
    
    //add a Jstruct_AminoAcid, return to the (same) list page
    //public String save() {
    //    try {
    //        aminoAcidService.addAminoAcid(newAminoAcid, sessionBean.getUser());
    //    } catch (JDAOException e) {
    //        HijackError.gotoErrorPage(e);
    //    } 
    //    return "/amino_acids.xhtml?faces-redirect=true";
    //}
    
    
    //save updates to an AminoAcid row
    //public void onRowEdit(RowEditEvent event) {
    //    try{
    //        Jstruct_AminoAcid updatedAminoAcid = (Jstruct_AminoAcid) event.getObject();
    //        aminoAcidService.updateAminoAcid(updatedAminoAcid, sessionBean.getUser());
    //        
    //        FacesMessage msg = new FacesMessage("Amino Acid updated", ((Jstruct_AminoAcid) event.getObject()).getName());
    //        FacesContext.getCurrentInstance().addMessage(null, msg);
    //        
    //    } catch (JDAOException jde) {
    //        
    //        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Amino Acid update failed", jde.getMessage());
    //        FacesContext.getCurrentInstance().addMessage(null, msg);
    //    }
    //    
    //}
     
    //fuhgeddaboudit
    //public void onRowCancel(RowEditEvent event) {
    //    //FacesMessage msg = new FacesMessage("Edit Cancelled", ((Jstruct_AminoAcid) event.getObject()).getName());
    //    //FacesContext.getCurrentInstance().addMessage(null, msg);
    //}
    
    

    private void retrieveAminoAcids() {
        try {
            
            //all records from the AminoAcidDef table
            aminoAcids = aminoAcidService.getAll();
            //and other stuff
            nucleicAcidMap= aminoAcidService.getNucleicAcidMap();
            bkbnAtoms = aminoAcidService.getBkbnAtoms();
            bkbnCbAtoms = aminoAcidService.getBkbnCbAtoms();
           
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }

    }

    


    
}
