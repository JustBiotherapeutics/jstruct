package com.just.jstruct.backing;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.ValueMapService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_ValueMap;
import com.just.jstruct.pojos.AuthState;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.pojos.ValueMapGroup;
import com.just.jstruct.utilities.AuthUtil;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * if there are ANY value_map records that have 'N' for 'initialized' this page will show up when access is attempted for ANY page 
 * this happens upon first startup, as well as anytime a new value_map record is added
 * 
 * if firstTimeStartup is true, the user will also be asked for their user-id, and will become the first developer user
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="bootstrapBean")
@RequestScoped
public class BootstrapBean implements Serializable {
 
    private static final long serialVersionUID = 1L;

    /* data members */
    
    @ManagedProperty(value="#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    
    private String username;
    private String password;
    
    private List<ValueMapGroup> valueMapGroups;
    private boolean firstTimeStartup = false;
    private List<Jstruct_User> activeDeveloperUsers;
    
    private List<String> errors;
    


    /* supporting service classes */
    private final ValueMapService valueMapService = new ValueMapService();
    private final JstructUserService jstructUserService = new JstructUserService();

    
    /* constructor */
    public BootstrapBean() {
    }
    
    @PostConstruct
    public void init() {
        try {
            
            currentUser = sessionBean.getUser();
            HttpServletRequest request = FacesUtil.getRequest();
            HttpServletResponse response = FacesUtil.getResponse();
            AuthUtil.logout(request, response);
            
            
            
            //all records from the ValueMap table
            valueMapGroups = valueMapService.getValueMapGroups();
            
            //it is the 'first time statup' if there is no developer listed in the 
            activeDeveloperUsers = jstructUserService.getActiveRoleDevelopers();
            if(!CollectionUtil.hasValues(activeDeveloperUsers)){
                firstTimeStartup = true;
            }
            
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }



    // getters/setters
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public Jstruct_User getCurrentUser() {return currentUser;}
    public void setCurrentUser(Jstruct_User currentUser) {this.currentUser = currentUser;}
    
    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }
    
    public List<ValueMapGroup> getValueMapsGroups() { return valueMapGroups; }
    public void setValueMapsGroups(List<ValueMapGroup> valueMapGroups) { this.valueMapGroups = valueMapGroups; }

    public boolean isFirstTimeStartup() {return firstTimeStartup;}
    public void setFirstTimeStartup(boolean firstTimeStartup) {this.firstTimeStartup = firstTimeStartup;}

    public List<Jstruct_User> getActiveDeveloperUsers() { return activeDeveloperUsers;}
    public void setActiveDeveloperUsers(List<Jstruct_User> activeDeveloperUsers) { this.activeDeveloperUsers = activeDeveloperUsers;}

    public List<String> getErrors() { return errors; }
    public void setErrors(List<String> errors) { this.errors = errors;}
    public void addError(String error){
        if(!CollectionUtil.hasValues(errors)){
            errors = new ArrayList<>(1);
        }
        errors.add(error);
    }
    

    
    

    public String saveAndContinue() {
        String returnString = "";
        try {
            
            //validate fields of all entries
            if (validateUserInput()) {
                
                //check developer login
                if(validateDeveloperUser()){
                    
                    //save 'em all - updating the 'initialized' to true as well.
                    for(ValueMapGroup vmg : valueMapGroups){
                        for(Jstruct_ValueMap vm : vmg.getValueMaps()){
                            vm.setInitialized(true);
                            valueMapService.updateValueMap(vm);
                        }
                    }
                    returnString = "/home.xhtml?faces-redirect=true";
                    
                } else {
                    //login error
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error validating user", null);
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                
                }
                
            } else {
                //form input error
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error submitting form", null);
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }

        } catch (JDAOException ex) {
            HijackError.gotoErrorPage(ex);
        }
        return returnString;
    }
    
    
    private boolean validateUserInput() {
        
        //everything is required... but this and validation is caught in the front end.
        //input validation is also currently caught in front end
        return true;
        
        
    }

    
    
    private boolean validateDeveloperUser() throws JDAOException {
        
        if(!StringUtil.isSet(username) || !StringUtil.isSet(password)){
            addError("Username and Password are required");
            return false;
        }
        
        
        HttpServletRequest request = FacesUtil.getRequest();
        HttpServletResponse response = FacesUtil.getResponse();
            
        
        AuthUtil authUtil = new AuthUtil();
        AuthState authState = authUtil.authenticateUser(username, password, request, response);
        
        if(!authState.isAuthenticated()){
            addError(authState.getAuthFailMessage());
            return false;
        }
        
        
        Jstruct_User authenticatedUser = authState.getJstructUser();
        if(!firstTimeStartup){
            //if this is NOT the first time, the user must also be an existing developer
            if(!authenticatedUser.getIsRoleDeveloper()){
                addError("Updated properties cannot be saved; " + authenticatedUser.getName() + " does not have the 'Developer' role.");
                AuthUtil.logout(request, response);
                return false;
            }
            
        } else {
            
            //if first time startup, give the authenticated user ALL roles
            authenticatedUser.setRoleDeveloper(true);
            authenticatedUser.setRoleAdmin(true);
            authenticatedUser.setRoleContributor(true);
            jstructUserService.updateJstructUser(authenticatedUser);
            
        }
            
        AuthUtil.logout(request, response);
        return true;
        
    }


    
}
