package com.just.jstruct.backing;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.AnalysisFileService;
import com.just.jstruct.dao.service.AnalysisRunService;
import com.just.jstruct.dao.service.AnalysisScriptService;
import com.just.jstruct.dao.service.AnalysisScriptVerService;
import com.just.jstruct.dao.service.AnalysisTaskService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_AnalysisFile;
import com.just.jstruct.model.Jstruct_AnalysisRun;
import com.just.jstruct.model.Jstruct_AnalysisScript;
import com.just.jstruct.model.Jstruct_AnalysisScriptVer;
import com.just.jstruct.model.Jstruct_AnalysisTask;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.NoAccess;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.UploadedFile;

/**
 * Backing bean for the Structure Search (criteria and results) page
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="scriptActionBean") 
@ViewScoped   
public class ScriptActionBean implements Serializable 
{
    private static final long serialVersionUID = 1L;
    
    
    
    /* -------------------------------------------------------------------------
        service classes 
    ------------------------------------------------------------------------- */
    
    private final AnalysisScriptService analysisScriptService = new AnalysisScriptService();
    private final AnalysisScriptVerService analysisScriptVerService = new AnalysisScriptVerService();
    private final AnalysisFileService analysisFileService = new AnalysisFileService();
    private final AnalysisRunService analysisRunService = new AnalysisRunService();
    private final AnalysisTaskService analysisTaskService = new AnalysisTaskService();
    
    /* -------------------------------------------------------------------------
        data members 
    ------------------------------------------------------------------------- */
    
    private static final String JSTRUCT_ID_SUBSTITUTE = "@JSTRUCT_ID";
    public String getJstructIdSubstitute() {return JSTRUCT_ID_SUBSTITUTE;}
    
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    @ManagedProperty("#{searchBean}")
    private SearchBean searchBean;
    
    private Jstruct_User currentUser;
    
    private List<Jstruct_FullStructure> allStructures;
    private List<Jstruct_FullStructure> readableStructures;
    
    private List<Jstruct_AnalysisScript> userAnalysisScripts;
    private List<Jstruct_AnalysisScript> globalAnalysisScripts;
    
    private List<Jstruct_AnalysisRun> analysisRuns;
    private boolean runsSaved;
    
    public int activeTabIndex;
    
    
    //upload new script stuff
    private String newScriptSubmitErrorMsg;
    private Jstruct_AnalysisScript newScript;
    private Jstruct_AnalysisScriptVer newScriptVer;
    private List<Jstruct_AnalysisFile> newAnalysisFiles;
    
    
    /* -------------------------------------------------------------------------
        constructor 
    ------------------------------------------------------------------------- */
    
    public ScriptActionBean(){}
    
    @PostConstruct
    public void init() 
    {
        checkPagePermissions();
        currentUser = sessionBean.getUser();

    }    
    
    //access to this page is avaliable only for logged in users (not guests role)
    private void checkPagePermissions() {
        if (sessionBean.getUser().isGuest()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Script Action page", "Script Action", "n/a"));
        }
    }
    
    
    /**
     * runs each time the dialog appears
     */
    public void onDialogShow()
    {
        try 
        {
            //copy structures from the *selected* search results
            if(CollectionUtil.hasValues(searchBean.getStructureSelection()))
            {
                allStructures = searchBean.getStructureSelection();
                readableStructures = new ArrayList<>(allStructures.size()); 
                for(Jstruct_FullStructure structure : allStructures)
                {
                    if(structure.isCurrentUserCanRead()){
                        readableStructures.add(structure);
                    }
                }
            }

            //get scripts for user/global
            userAnalysisScripts = analysisScriptService.getByOwnerId(sessionBean.getUser().getUserId(), true, false);
            globalAnalysisScripts = analysisScriptService.getGlobal(true, false);
        
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    /* -------------------------------------------------------------------------
        getters/setters 
    ------------------------------------------------------------------------- */
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public SearchBean getSearchBean() {  return searchBean; }
    public void setSearchBean(SearchBean searchBean) { this.searchBean = searchBean; }
    
    public List<Jstruct_FullStructure> getAllStructures() { return allStructures;}
    public void setAllStructures(List<Jstruct_FullStructure> allStructures) { this.allStructures = allStructures;}

    public List<Jstruct_FullStructure> getReadableStructures() { return readableStructures; }
    public void setReadableStructures(List<Jstruct_FullStructure> readableStructures) {this.readableStructures = readableStructures;}

    
    public List<Jstruct_AnalysisScript> getUserAnalysisScripts() { return userAnalysisScripts; }
    public void setUserAnalysisScripts(List<Jstruct_AnalysisScript> userAnalysisScripts) { this.userAnalysisScripts = userAnalysisScripts; }

    public List<Jstruct_AnalysisScript> getGlobalAnalysisScripts() { return globalAnalysisScripts; }
    public void setGlobalAnalysisScripts(List<Jstruct_AnalysisScript> globalAnalysisScripts) { this.globalAnalysisScripts = globalAnalysisScripts; }

    public List<Jstruct_AnalysisRun> getAnalysisRuns() { return analysisRuns; }
    public void setAnalysisRuns(List<Jstruct_AnalysisRun> analysisRuns) { this.analysisRuns = analysisRuns; }

    public boolean isRunsSaved() { return runsSaved; }
    public void setRunsSaved(boolean runsSaved) { this.runsSaved = runsSaved; }

    
    public int getActiveTabIndex() { return activeTabIndex; }
    public void setActiveTabIndex(int activeTabIndex) { this.activeTabIndex = activeTabIndex; }
    
    
    
    public String getNewScriptSubmitErrorMsg() {  return newScriptSubmitErrorMsg; }
    public void setNewScriptSubmitErrorMsg(String newScriptSubmitErrorMsg) { this.newScriptSubmitErrorMsg = newScriptSubmitErrorMsg; }
    
    public Jstruct_AnalysisScript getNewScript() { return newScript; }
    public void setNewScript(Jstruct_AnalysisScript newScript) { this.newScript = newScript;}

    public Jstruct_AnalysisScriptVer getNewScriptVer() { return newScriptVer; }
    public void setNewScriptVer(Jstruct_AnalysisScriptVer newScriptVer) { this.newScriptVer = newScriptVer; }

    public List<Jstruct_AnalysisFile> getNewAnalysisFiles() {  return newAnalysisFiles; }
    public void setNewAnalysisFiles(List<Jstruct_AnalysisFile> newAnalysisFiles) {  this.newAnalysisFiles = newAnalysisFiles; }
    
    
    
     
    /* #######################################################################################################################################
            methods
       #######################################################################################################################################  */
     
    
    /**
     * 
     * @param scriptVerId 
     */
    public void scriptVerCheckboxClickedUserList(String scriptVerId) 
    {
        Jstruct_AnalysisScriptVer clickedScriptVer = getClickedScriptVer(userAnalysisScripts, JStringUtil.stringToLong(scriptVerId));
        clickedScriptVer.setSelected(clickedScriptVer.isSelected());

    }
    
    /**
     * 
     * @param scriptVerId 
     */
    public void scriptVerCheckboxClickedGlobalList(String scriptVerId) 
    {
        Jstruct_AnalysisScriptVer clickedScriptVer = getClickedScriptVer(globalAnalysisScripts, JStringUtil.stringToLong(scriptVerId));
        clickedScriptVer.setSelected(clickedScriptVer.isSelected());
    }
    
    private Jstruct_AnalysisScriptVer getClickedScriptVer(List<Jstruct_AnalysisScript> scripts, Long scriptVerId)
    {
        for(Jstruct_AnalysisScript script : scripts)
        {
            for(Jstruct_AnalysisScriptVer scriptVer : script.getAllScriptVers())
            {
                if(scriptVer.getAnalysisScriptVerId().equals(scriptVerId))
                {
                    return scriptVer;
                }
            }
        }
        return null;
    }
    
    
    
    
    /**
     * when user navigates to the final tab, gather all the script versions selected, and make the beginning 'analysis_run' objects for these
     * @param event 
     */
    public void onTabChange(TabChangeEvent event) 
    {
        //if at the "Run Scripts..." tab, update the analysisRuns list
        if(event.getTab().getTitle().equalsIgnoreCase("Run Scripts...")) 
        {
            //use a set to avoid duplicates (incase user selected same version in both lists)
            LinkedHashSet<Jstruct_AnalysisScriptVer> selectedVerSet = new LinkedHashSet<>(10);
            
            selectedVerSet.addAll(getSelectedScriptVers(userAnalysisScripts));
            selectedVerSet.addAll(getSelectedScriptVers(globalAnalysisScripts));
            
            analysisRuns = new ArrayList<>(selectedVerSet.size());
            
            //create list of AnalysisRuns for each selected script ver
            if(CollectionUtil.hasValues(selectedVerSet))
            {
                for(Jstruct_AnalysisScriptVer scriptVer : selectedVerSet)
                {
                    analysisRuns.add(createNewAnalysisRun(scriptVer));
                }
            }
            
        }
    }
    

    private LinkedHashSet<Jstruct_AnalysisScriptVer> getSelectedScriptVers(List<Jstruct_AnalysisScript> scripts)
    {
        LinkedHashSet<Jstruct_AnalysisScriptVer> selectedVerSet = new LinkedHashSet<>();
        
        if(CollectionUtil.hasValues(scripts))
        {
            for(Jstruct_AnalysisScript script : scripts)
            {
                for(Jstruct_AnalysisScriptVer scriptVer : script.getAllScriptVers())
                {
                    if(scriptVer.isSelected())
                    {
                        selectedVerSet.add(scriptVer);
                    }
                }
            }
        }
        return selectedVerSet;
    }
    
    private Jstruct_AnalysisRun createNewAnalysisRun(Jstruct_AnalysisScriptVer scriptVer)
    {
        Jstruct_AnalysisRun run = new Jstruct_AnalysisRun();
        run.setAnalysisScriptVer(scriptVer);
        run.setAnalysisScriptVerId(scriptVer.getAnalysisScriptVerId());
        run.setCommandPattern(scriptVer.getDefaultCommand());
        run.setUser(currentUser);
        run.setTotalTaskCount(readableStructures.size());
        run.setSuccessfulTaskCount(0);
        run.setFailedTaskCount(0);
        return run;
    }
    
    
    
    /**
     * fore eacch script selected, save all analysis_runs and analysis_tasks
     */
    public void saveAnalysisRuns()
    {
        try
        {
            //for each analysisRun...
            for(Jstruct_AnalysisRun run : analysisRuns)
            {
                //...save overall analysis_run
                run = analysisRunService.add(run);

                //...save each analysis_task for every structure
                for(Jstruct_FullStructure structure : readableStructures) 
                {
                    String command = run.getCommandPattern().replace(JSTRUCT_ID_SUBSTITUTE, "[****" + structure.getJstructDisplayId() + "****]"); //todo what should we disply in the command?
                    
                    Jstruct_AnalysisTask task = new Jstruct_AnalysisTask();
                    task.setAnalysisRunId(run.getAnalysisRunId());
                    task.setStructVerId(structure.getStructVerId());
                    task.setCommand(command);
                    task.setStatus(Jstruct_AnalysisRun.STATUS_NOT_STARTED);
                    
                    analysisTaskService.add(task);
                }

            }

            runsSaved = true;
            activeTabIndex = 4;
        
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    
    /* #######################################################################################################################################
            methods for the upload new script dialog
       #######################################################################################################################################  */
     
    
    public void initiateNewScript()
    {
        newScript = new Jstruct_AnalysisScript();
        newScriptVer = new Jstruct_AnalysisScriptVer();
        newScriptVer.setStatus(Jstruct_AnalysisScriptVer.STATUS_CURRENT);
        newScriptVer.setVersion(1);
        newAnalysisFiles = null;
    }
    
    
    /**
     * 
     * @param event 
     */
    public void uploadFileEvent(FileUploadEvent event)  
    {
        try 
        {
            try 
            {
                UploadedFile uf = event.getFile();
                InputStream inputstream = uf.getInputstream();
                String fileName = uf.getFileName();
                
                Jstruct_AnalysisFile af = new Jstruct_AnalysisFile(fileName, IOUtils.toByteArray(inputstream));
                
                if(newAnalysisFiles == null)
                {
                    newAnalysisFiles = new ArrayList<>(10);
                }
                newAnalysisFiles.add(af);
                
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error uploading file...", ex.getMessage()));
            }
            
            Thread.sleep(500); //gotta stall as sometimes the uploadFileComplete gets called before the last file is actually uploaded!

        } catch (InterruptedException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }

    public void removeFiles()
    {
        newAnalysisFiles = new ArrayList<>(10);
    }
    
    
    
    public void resetNewVerson()
    {
        initiateNewScript();
    }
    
    public void submitNewVersion()
    {
        //validate
        newScriptSubmitErrorMsg = "";
        List<String> errors = new ArrayList<>(5);
        if(!StringUtil.isSet(newScript.getScriptName())){
            errors.add("Script Name is Required");
        }
        if(!StringUtil.isSet(newScript.getScriptType())){
            errors.add("Script Type is Required");
        }
        if(!StringUtil.isSet(newScriptVer.getDefaultCommand())){
            errors.add("Default Command is Required");
        } 
        else if(!newScriptVer.getDefaultCommand().toUpperCase().contains(JSTRUCT_ID_SUBSTITUTE.toUpperCase())) {
            errors.add("Default Command must contain the value: " + JSTRUCT_ID_SUBSTITUTE);
        }
        if(!CollectionUtil.hasValues(newAnalysisFiles)){
            errors.add("Input must have at least one Script File uploaded");
        }
        
        if(CollectionUtil.hasValues(errors)){
            newScriptSubmitErrorMsg = StringUtil.join(errors, "; ") + ".";
            return;
        }
        
        
        try
        {
            //validation passed, save new script.
            
            newScript = analysisScriptService.add(newScript, sessionBean.getUser());
            Long newScriptId = newScript.getAnalysisScriptId();
            newScriptVer.setAnalysisScriptId(newScriptId);
            newScriptVer = analysisScriptVerService.add(newScriptVer, sessionBean.getUser());
            
            for(Jstruct_AnalysisFile af : newAnalysisFiles)
            {
                af.setAnalysisScriptVerId(newScriptVer.getAnalysisScriptVerId()); 
                analysisFileService.add(af);
            }
            
            //clear form
            initiateNewScript();
            newAnalysisFiles = null;
            
            //add the new script to the existing user list
            Jstruct_AnalysisScript brandNewScript = analysisScriptService.getById(newScriptId, true);
            userAnalysisScripts.add(brandNewScript);
            
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "New Analysis Script Uploaded.", newScript.getScriptName());
            FacesContext.getCurrentInstance().addMessage(null, msg);

        } catch (JDAOException ex) {
            ex.printStackTrace();
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
}
