package com.just.jstruct.backing;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.ChainService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_AtomGrouping;
import com.just.jstruct.model.Jstruct_Chain;
import com.just.jstruct.pojos.DisplayAtomGroup;
import com.just.jstruct.pojos.DisplaySequence;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JStringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *  
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="chainDisplayBean")
@ViewScoped
public class ChainDisplayBean implements Serializable {

    private static final long serialVersionUID = 1L;

    
    
    /* data members */
    private Jstruct_Chain chain;
    private List<Jstruct_Chain> allChains;
    private DisplaySequence displaySequence;
    private List<DisplayAtomGroup> displayAtomGroups; //usually only one
    
    
    /* service classes */
    private final ChainService chainService = new ChainService();
   
  

    
    
    
    /* constructor */
    public ChainDisplayBean(){ 
    }
     
    @PostConstruct
    public void init() {
        
    }


    
    public void getDaChain(String chainIdParam){
        try {
            
            Long chainId = JStringUtil.stringToLong(chainIdParam);
            chain = chainService.getByChainId(chainId, true, true, true); // getByChainId(Long chainId, boolean includeAtomGroupings, boolean includeAtomGroupingResidues, boolean includeRelatedSeqResidues)
            
            allChains = chainService.getByStructVerId(chain.getStructVerId());
            
            displaySequence = new DisplaySequence(chain, "Index:");  // for the 'primary' theoretical sequence
            
            if(CollectionUtil.hasValues(chain.getAtomGroupings())){
                displayAtomGroups = new ArrayList<>(chain.getAtomGroupings().size());
                for(Jstruct_AtomGrouping ag : chain.getAtomGroupings()){
                    displayAtomGroups.add(new DisplayAtomGroup(ag, "Residue Sequence #:"));
                }
            }
            
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
   

    /* getters/setters */

    public Jstruct_Chain getChain() {
        return chain;
    }

    public void setChain(Jstruct_Chain chain) {
        this.chain = chain;
    }

    public List<Jstruct_Chain> getAllChains() {
        return allChains;
    }

    public void setAllChains(List<Jstruct_Chain> allChains) {
        this.allChains = allChains;
    }

    public DisplaySequence getDisplaySequence() {
        return displaySequence;
    }

    public void setDisplaySequence(DisplaySequence displaySequence) {
        this.displaySequence = displaySequence;
    }

    public List<DisplayAtomGroup> getDisplayAtomGroups() {
        return displayAtomGroups;
    }

    public void setDisplayAtomGroups(List<DisplayAtomGroup> displayAtomGroups) {
        this.displayAtomGroups = displayAtomGroups;
    }


    
    
}