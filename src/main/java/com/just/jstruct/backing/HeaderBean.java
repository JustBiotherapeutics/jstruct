package com.just.jstruct.backing;

import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.AuthState;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.AuthUtil;
import com.just.jstruct.utilities.FacesUtil;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Backing bean for the header 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="headerBean") 
@ViewScoped
public class HeaderBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    
     
     
   
    
    /* data members */
    @ManagedProperty(value="#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    
    private String username;
    private String password;
    
    

    
    //constructor
    public HeaderBean() { 
    }
    
    @PostConstruct
    public void init() {
        currentUser = sessionBean.getUser();
    }


    
    
    //getters/setters
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    public Jstruct_User getCurrentUser() { return currentUser; }
    public void setCurrentUser(Jstruct_User currentUser) { this.currentUser = currentUser; }
    
    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }
    
    
    
    /* actions */
    
    public void authAttempt(ActionEvent actionEvent){
        
        HttpServletRequest request = FacesUtil.getRequest();
        HttpServletResponse response = FacesUtil.getResponse();
        
        AuthUtil authUtil = new AuthUtil();
        AuthState authState = authUtil.authenticateUser(username, password, request, response);
        
        if(authState.isAuthenticated()){
            
            //authentication success - redirect to home
            FacesUtil.redirectRequest("/home.xhtml?faces-redirect=true"); //todo get login page from applicationresources.properties
            
        } else {
            
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, authState.getAuthFailMessage(),  null);
            FacesContext.getCurrentInstance().addMessage(null, message);
            
        }
         
    }
    
    public String logout(){
        AuthUtil.logout(FacesUtil.getRequest(), FacesUtil.getResponse());
        return "/home.xhtml?faces-redirect=true";  //todo get login page from applicationresources.properties
    }
    
    
    
    
    
    
    
    
/*
    public ProcessControl getProcessControl() { return processControl; }
    public void setProcessControl(ProcessControl processControl) { this.processControl = processControl; }

    public Collection<AutoJob> getAutoJobs() { return autoJobs; }
    public void setAutoJobs(Collection<AutoJob> autoJobs) { this.autoJobs = autoJobs; }
    
    public Collection<JobHistory> getJobHistorys() { return jobHistorys; }
    public void setJobHistorys(Collection<JobHistory> jobHistorys) { this.jobHistorys = jobHistorys; }

    public int getRecentHistoryCount() { return recentHistoryCount; }
    public void setRecentHistoryCount(int recentHistoryCount) { this.recentHistoryCount = recentHistoryCount; }
    */

    

    
    
    
    
    
    
    
    
}
