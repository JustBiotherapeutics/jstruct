package com.just.jstruct.backing;

import com.just.jstruct.exception.JException;
import com.just.jstruct.jobScheduling.JScheduler;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.ValueMapUtils;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;


/**
 * stuff for the main menu
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="menuBean") 
@ViewScoped
public class MenuBean implements Serializable {
 
    private static final long serialVersionUID = 1L;


    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    
    private HttpServletRequest request;
    private String requestURL;
    private String queryString;
    private String tomcatUrl;
    
    private boolean autoProcessIsStarted;
    
    
    //the following 'pages' identify which section of the app the user is currently in
    //there is one for each of the menu options

    //all (including guest)
    private boolean homePage = false;
    private boolean structureSearchPage = false;
    
    //logged in
    private boolean searchesPage = false;
    private boolean labelsPage = false;
    private boolean scriptMgrPage = false;
    
    //contributor
    private boolean newStructurePage = false;
    
    //all
    private boolean aboutPage = false;
    
    //admin
    private boolean schedulePage = false;
    private boolean adminActionPage = false;
    private boolean aminoAcidPage = false;
    private boolean lookupsPage = false;
    private boolean rolesPage = false;
    
    //developer
    private boolean propertiesPage = false;
    

    
    
    

    
    /* constructor */
    public MenuBean() {
    }
    
    @PostConstruct
    public void init() {
        
        try {
            
            currentUser = sessionBean.getUser();
            request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            requestURL = request.getRequestURL().toString();
            queryString = request.getQueryString();
            determinPage();

            autoProcessIsStarted = JScheduler.isStarted();
            
            tomcatUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + "/manager/html/";
        
        } catch (JException e) {
            HijackError.gotoErrorPage(e);
        }
        
    }


    // getters/setters

    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public Jstruct_User getCurrentUser() {return currentUser;}

    public HttpServletRequest getRequest() { return request; }
    public String getRequestURL() { return requestURL; }
    public String getQueryString() { return queryString; }
    public String getTomcatUrl() { return tomcatUrl; }

    public boolean isAutoProcessIsStarted() {return autoProcessIsStarted; }
    
    public boolean isHomePage() { return homePage; }
    public boolean isStructureSearchPage() { return structureSearchPage; }
    public boolean isSearchesPage() { return searchesPage; }
    public boolean isLabelsPage() { return labelsPage; }
    public boolean isScriptMgrPage() { return scriptMgrPage; }
    public boolean isNewStructurePage() { return newStructurePage; }
    public boolean isAboutPage() { return aboutPage; }
    
    public boolean isSchedulePage() { return schedulePage; }
    public boolean isAdminActionPage() { return adminActionPage; }
    public boolean isAminoAcidPage() { return aminoAcidPage; }
    public boolean isLookupsPage() { return lookupsPage; }
    public boolean isRolesPage() { return rolesPage; }
    
    
    public boolean isPropertiesPage() { return propertiesPage; }

    
    
    
    
    public String refreshSession(){
        
        ValueMapUtils vmu = ValueMapUtils.getInstance();
        vmu.refreshInstance();
        
        return FacesUtil.invalidateSessionAndGoHome();
        
    }
    
    
    
    /**
     * when heading to the new structure page, we'll first store the current page we're on
     * so that we can return here if the user clicks cancel.
     * @return 
     */
    public String gotoNewStructurePage(){
        
        if(!requestURL.contains("/new_structure.xhtml")){
            String page = JStringUtil.onlyTextAfterLastOccurance(requestURL,"/");
            FacesUtil.storeOnSession("lastPageBeforeNewStructurePage", page);
        }
        return "/new_structure.xhtml?faces-redirect=true";
    }


    
    
    /**
     * 
     */
    private void determinPage() {

        if (requestURL.contains("/home.xhtml")) {
            homePage = true;
            
        } else if (requestURL.contains("/structure_search.xhtml")) {
            structureSearchPage = true;
            
        } else if (requestURL.contains("/searches.xhtml")) {
            searchesPage = true;
            
        } else if (requestURL.contains("/labels.xhtml")) {
            labelsPage = true;
            
        } else if (requestURL.contains("/script_mgr.xhtml")) {
            scriptMgrPage = true;
            
        } else if (requestURL.contains("/new_structure.xhtml")) {
            newStructurePage = true;
            
        } else if (requestURL.contains("/about.xhtml")) {
            aboutPage = true;
            
            
            
        } else if (requestURL.contains("/schedule.xhtml")) {
            schedulePage = true;
            
        } else if (requestURL.contains("/admin_actions.xhtml")) {
            adminActionPage = true;
            
        } else if (requestURL.contains("/amino_acids.xhtml")) {
            aminoAcidPage = true;
            
        } else if (requestURL.contains("/lookups.xhtml")) {
            lookupsPage = true;
            
        } else if (requestURL.contains("/roles.xhtml")) {
            rolesPage = true;
            
            
        } else if (requestURL.contains("/properties.xhtml")
               || (requestURL.contains("/dynamic_property_edit.xhtml"))) {
            propertiesPage = true;
        }


    }
   
   
   
    
    
    
    
}
