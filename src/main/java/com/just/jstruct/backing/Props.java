package com.just.jstruct.backing;

import com.just.jstruct.utilities.ValueMapUtils;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------

@ManagedBean(name="props")
@SessionScoped
public class Props {

    /* data members */

    
    /* supporting service classes */
    private final ValueMapUtils valueMapUtils;
    
    
    /* getters */
    public ValueMapUtils getGet(){return valueMapUtils;}
    
    
    
    
    public Props() {
        valueMapUtils = ValueMapUtils.getInstance();
    }
    
    
    
    
    
    
    
    
    
    
}
