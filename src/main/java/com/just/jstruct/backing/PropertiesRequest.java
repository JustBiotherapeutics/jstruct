package com.just.jstruct.backing;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;


/**
 * just some of the current HttpServletRequest values for troubleshooting
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="propertiesRequest")
@RequestScoped
public class PropertiesRequest implements Serializable {
 
    private static final long serialVersionUID = 1L;

    /* data members */
    private HttpServletRequest currentRequest;


    
    /* constructor */
    public PropertiesRequest() {}
    
    
    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        currentRequest = (HttpServletRequest) context.getExternalContext().getRequest();
    }



    // getters/setters

    public HttpServletRequest getCurrentRequest() { return currentRequest; }
    public void setCurrentRequest(HttpServletRequest currentRequest) { this.currentRequest = currentRequest; }



    
 

    
}
