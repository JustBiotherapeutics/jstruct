package com.just.jstruct.backing;

import com.just.jstruct.dao.service.UserSearchService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_SearchParam;
import com.just.jstruct.model.Jstruct_SearchSubquery;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserSearch;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.enums.EqualsOption;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JDateUtil;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.RowEditEvent;

/**
 * Backing bean for the list of searches
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="searchListBean") 
@ViewScoped   
public class SearchListBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    
    /* service classes */
    private final UserSearchService userSearchService = new UserSearchService();
    
    
    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    
    private List<Jstruct_UserSearch> userSearches;
    
    private List<Jstruct_UserSearch> preparedSearches = new ArrayList<>();
    
    
    
    
    
            
   
    
    //constructor
    public SearchListBean() { }
    
    @PostConstruct
    public void init() {
        try {
            checkPagePermissions();
            
            currentUser = sessionBean.getUser();
            
            //get all saved searches for this user
            userSearches = userSearchService.getByUserId(currentUser.getUserId());
            
            //create some prepared searches 
            preparedSearches.add(createUserSearchCurrentOwner(-88L));
            preparedSearches.add(createUserSearchRecent(-99L));
            
                    
        } catch (InvalidParameterException | JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }

    //access to this page is avaliable only for logged in users (not guests role)
    private void checkPagePermissions() {
        if (sessionBean.getUser().isGuest()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Search List page",
                                                            "My Searches", "n/a"));
        }
    }
    
    
    
    
    /* getters/setters */
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    

    public List<Jstruct_UserSearch> getUserSearches() {return userSearches;}

    public List<Jstruct_UserSearch> getPreparedSearches() {return preparedSearches;}


    

    
    
    
    /**
     * dynamically get the fullQueryInput object, and add it to the row when the user toggles it's view
     * @param userSearchIdParam 
     */
    public void addFullQueryInput(String userSearchIdParam){
        
        try{
        
            //create the fullQueryInput from the database based on userSearchId
            Long userSearchId = JStringUtil.stringToLong(userSearchIdParam);
            FullQueryInput fullQueryInput = PostgresDao.retrieveCriteriaFromDatabase(userSearchId, currentUser);


            //add it to the correct userSearch (transient field)
            for(Jstruct_UserSearch userSearch : userSearches){

                if(userSearch.getUserSearchId().equals(userSearchId)){
                    userSearch.setFullQueryInput(fullQueryInput); 
                    break;
                }

            }
        
        } catch (JDAOException ex) {
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    
    /**
     * dynamically create the fullQueryInput object (from the prepared searches), and add it to the row when the user toggles it's view
     * @param preparedUserSearchIdParam 
     */
    public void createFullQueryInput(String preparedUserSearchIdParam){
        
        try{
        
            //create the fullQueryInput from the database based on userSearchId
            Long id = JStringUtil.stringToLong(preparedUserSearchIdParam);
            FullQueryInput fullQueryInput = getFullQueryInputForPreparedSearch(id);


            //add it to the correct userSearch (transient field)
            for(Jstruct_UserSearch userSearch : preparedSearches){

                if(userSearch.getUserSearchId().equals(id)){
                    userSearch.setFullQueryInput(fullQueryInput); 
                    break;
                }

            }
        
        } catch (JDAOException ex) {
            HijackError.gotoErrorPage(ex);
        }
    }
    
   
    
    
    
    //save updates (search name only for not) to a row 
    public void onEditRow(RowEditEvent event) {
        try{
            
            Jstruct_UserSearch updatedUserSearch = (Jstruct_UserSearch) event.getObject();
            userSearchService.update(updatedUserSearch);
            
            FacesMessage msg = new FacesMessage("Search updated", updatedUserSearch.getSearchName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (JDAOException jde) {
            
            jde.printStackTrace();
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Search  update failed", jde.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }
    
  
     
    //fuhgeddaboudit
    public void onCancelRow(RowEditEvent event) {
        //FacesMessage msg = new FacesMessage("Edit Cancelled", ((Jstruct_AminoAcid) event.getObject()).getName());
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    /**
     * 
     * @param actionEvent 
     */
    public void deleteSearch(ActionEvent actionEvent) {
        try {
            Long doomedUserSearchId = (Long) actionEvent.getComponent().getAttributes().get("doomedUserSearchId");
            Jstruct_UserSearch doomedUserSearch = userSearchService.getByUserSearchId(doomedUserSearchId);
            String doomedSearechName = doomedUserSearch.getSearchName();
            
            userSearchService.delete(doomedUserSearch);
            
            userSearches = userSearchService.getByUserId(currentUser.getUserId());
            
            FacesMessage msg = new FacesMessage("Search deleted", doomedSearechName);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    /**
     * based on the selected userSearchId, get da fullQueryInupt data and redirect to the search(results) page
     * @param userSearchId
     * @return 
     */
    public String doSelectedSearch(Long userSearchId){
        try {
            
            //cleanup existing b/4 our next search
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);  
      
            //get search data from DB for the selected userSearchId, and store it on the session for the search (results) page
            FullQueryInput fullQueryInput = PostgresDao.retrieveCriteriaFromDatabase(userSearchId, currentUser);
            FacesUtil.storeOnSession("fullQueryInput", fullQueryInput);

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
        //redirect to search [results] page
        return "/structure_search.xhtml?faces-redirect=true";
    }
    
    
    /**
     * based on the selected userSearchId, get da fullQueryInupt data and redirect to the search(results) page
     * @param id
     * @return 
     */
    public String doSelectedPreparedSearch(Long id){
        try {
            
            //cleanup existing search b/4 our next search
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);  
            
            //get the selected search, and transform it into the needed fullQueryInput object
            FullQueryInput fullQueryInput = getFullQueryInputForPreparedSearch(id);

            //store it on the session for the search (results) page
            FacesUtil.storeOnSession("fullQueryInput", fullQueryInput);

        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
        //redirect to search [results] page
        return "/structure_search.xhtml?faces-redirect=true";
    }
    
    
    private FullQueryInput getFullQueryInputForPreparedSearch(Long id) throws JDAOException {
        
        FullQueryInput fullQueryInput = null;
        
        for (Jstruct_UserSearch userSearch : preparedSearches) {
            if (userSearch.getUserSearchId().equals(id)) {
                fullQueryInput = PostgresDao.transformUserSearchToFullQueryInput(userSearch, currentUser);
                break;
            }
        }
        
        return fullQueryInput;
        
    }

    
   

    
    
    private Jstruct_UserSearch createUserSearchCurrentOwner(Long id) {

        Jstruct_SearchParam sp1 = new Jstruct_SearchParam(id, "selectedOwnerEqualsOption", EqualsOption.OPTION_EQUALS.name());
        Jstruct_SearchParam sp2 = new Jstruct_SearchParam(id, "selectedOwner", currentUser.getUid());

        Jstruct_SearchSubquery ss = new Jstruct_SearchSubquery(id, SubQueryType.OWNER, 1);
        ss.addSearchParam(sp1);
        ss.addSearchParam(sp2);

        Jstruct_UserSearch us = new Jstruct_UserSearch(currentUser, "Active structures owned by me");
        us.addSearchSubquery(ss);
        us.setUserSearchId(id);
        us.setFileStatusRcsb(FileStatus.FILE_STATUS_NONE);
        us.setFileStatusManual(FileStatus.FILE_STATUS_ACTIVE);
        us.setQueryMode(QueryMode.AND);

        return us;

    }
    
    
    private Jstruct_UserSearch createUserSearchRecent(Long id) {
        
        Date sevenDaysAgo = JDateUtil.addDaysToNow(-7);
        
        Jstruct_SearchParam sp1 = new Jstruct_SearchParam(id, "depositDateFrom", JStringUtil.longToString(sevenDaysAgo.getTime()));

        Jstruct_SearchSubquery ss = new Jstruct_SearchSubquery(id, SubQueryType.DEPOSIT_DATES, 1);
        ss.addSearchParam(sp1);

        Jstruct_UserSearch us = new Jstruct_UserSearch(currentUser, "Structures deposited within the last week");
        us.addSearchSubquery(ss);
        us.setUserSearchId(id);
        us.setFileStatusRcsb(FileStatus.FILE_STATUS_ACTIVE);
        us.setFileStatusManual(FileStatus.FILE_STATUS_ACTIVE);
        us.setQueryMode(QueryMode.AND);

        return us;

    }
    
    
}
