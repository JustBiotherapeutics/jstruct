package com.just.jstruct.backing;

import com.hfg.html.HTMLTag;
import com.hfg.util.StringUtil;
import com.just.bio.structure.Structure;
import com.just.jstruct.dao.service.ChainService;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.NoteService;
import com.just.jstruct.dao.service.PdbAuthorService;
import com.just.jstruct.dao.service.PdbCompndService;
import com.just.jstruct.dao.service.PdbFileService;
import com.just.jstruct.dao.service.PdbJrnlService;
import com.just.jstruct.dao.service.PdbKeywordService;
import com.just.jstruct.dao.service.PdbRemarkService;
import com.just.jstruct.dao.service.PdbRevdatService;
import com.just.jstruct.dao.service.PdbSourceService;
import com.just.jstruct.dao.service.PdbSplitService;
import com.just.jstruct.dao.service.StructFileService;
import com.just.jstruct.dao.service.StructVersionService;
import com.just.jstruct.dao.service.StructureLabelService;
import com.just.jstruct.dao.service.StructureRoleService;
import com.just.jstruct.dao.service.StructureService;
import com.just.jstruct.dao.service.StructureUserService;
import com.just.jstruct.dao.service.UserLabelService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.model.Jstruct_Chain;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_Note;
import com.just.jstruct.model.Jstruct_PdbAuthor;
import com.just.jstruct.model.Jstruct_PdbCompnd;
import com.just.jstruct.model.Jstruct_PdbFile;
import com.just.jstruct.model.Jstruct_PdbJrnl;
import com.just.jstruct.model.Jstruct_PdbKeyword;
import com.just.jstruct.model.Jstruct_PdbRemark;
import com.just.jstruct.model.Jstruct_PdbRevdat;
import com.just.jstruct.model.Jstruct_PdbSource;
import com.just.jstruct.model.Jstruct_PdbSplit;
import com.just.jstruct.model.Jstruct_StructVersion;
import com.just.jstruct.model.Jstruct_StructureRole;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserLabel;
import com.just.jstruct.pojos.DisplaySequence;
import com.just.jstruct.pojos.JFileAttributes;
import com.just.jstruct.pojos.JstructId;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.pojos.StructureFastaLink;
import com.just.jstruct.structureImport.JImportUtils;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JFileUtils;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.StructureUtil;
import java.io.Serializable;
import java.nio.file.AccessDeniedException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.jsoup.Jsoup;
import org.primefaces.event.RowEditEvent;

/**
 * Backing bean for the structure page
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="structureBean") 
@ViewScoped   
public class StructureBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    private final Integer maxChainsForVerboseDisplay = 25;
    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    
    private String idParam;
    private JstructId jstructId;
    private String jstructIdForDisplay;
    
    private boolean favorite;
    
    private Jstruct_FullStructure fullStructure;
    private StructureFastaLink structureFastaLink;
    private List<Jstruct_FullStructure> fullStructureVersions;
    
    private String lineageDiagram = "<div></div>";
    
    private List<Jstruct_StructureRole> structureRoles;
    private Jstruct_User newStructureRoleUser;
    
    private boolean displayPermissions = false;
    private boolean currentUserIsOwner = false;
    private boolean currentUserCanAddNotes = false;
    
    private List<Jstruct_Note> notes;
    private Jstruct_Note newNote = new Jstruct_Note(false);
    private boolean noteSectionOpen = false;
    
    private List<Jstruct_PdbSplit> pdbSplits;
    private List<Jstruct_FullStructure> splitsFullStructures;
    private List<Jstruct_PdbKeyword> keywords;
    private List<Jstruct_PdbAuthor> authors;
    private Jstruct_PdbJrnl jrnl;
    private List<Jstruct_PdbCompnd> compnds;
    private List<Jstruct_PdbSource> sources;
    private List<Jstruct_PdbRevdat> revdats;
    private List<Jstruct_PdbRemark> remarks;
    private JFileAttributes fileAttributes;
    
    private Integer chainCount;
    private List<Jstruct_Chain> chains;
    private List<DisplaySequence> displaySequences;
    private boolean sequencesBeenDisplayed = false;

    private List<Jstruct_UserLabel> allUsersLabels;
    private List<Jstruct_UserLabel> selectedUserLabels;
    private Jstruct_UserLabel newLabel;
    
    
    private String displayModeForHeader = "view";
    private String displayModeForJrnl = "view";
    private String displayModeForMetadata = "view";
    
    private Jstruct_PdbFile editablePdbFile;
    private Jstruct_StructVersion editableStructVersion;
    
    
    /* service classes */
    private FullStructureService fullStructureService;
    private final StructureService structureService = new StructureService();
    private final StructureUserService structureUserService = new StructureUserService();
    private final StructureRoleService structureRoleService = new StructureRoleService();
    private final StructVersionService structVersionService = new StructVersionService();
    private final PdbSplitService pdbSplitService = new PdbSplitService();
    private final PdbKeywordService pdbKeywordService = new PdbKeywordService();
    private final PdbAuthorService pdbAuthorService = new PdbAuthorService();
    private final PdbJrnlService pdbJrnlService = new PdbJrnlService();
    private final PdbCompndService pdbCompndService = new PdbCompndService();
    private final PdbSourceService pdbSourceService = new PdbSourceService();
    private final PdbRevdatService pdbRevdatService = new PdbRevdatService();
    private final PdbRemarkService pdbRemarkService = new PdbRemarkService();
    private final ChainService chainService = new ChainService();
    private final UserLabelService userLabelService = new UserLabelService();
    private final StructureLabelService structureLabelService = new StructureLabelService();
    private final PdbFileService pdbFileService = new PdbFileService();
    private final JstructUserService jstructUserService = new JstructUserService();
    private final NoteService noteService = new NoteService();
    
    
    
    //constructor
    public StructureBean() { }
    
    @PostConstruct
    public void init() {
        try {
            
            currentUser = sessionBean.getUser();
            fullStructureService = new FullStructureService(currentUser);
            
            idParam = FacesUtil.getStringValueFromUrl("id");
            
            jstructId = new JstructId(idParam);
            if(jstructId.isInvalidFormat()){
                throw new InvalidParameterException("Invalid JStruct ID: '" + idParam + "'");
            }
            
            
            fullStructure = fullStructureService.getByJstructId(jstructId, false);
            if(fullStructure == null){
                throw new InvalidParameterException("JStruct ID: '" + idParam + "' does not evaluate to an existing Structure.");
            }
            structureFastaLink = new StructureFastaLink(fullStructure);
            
            //update jstructId with data that comes from our fullStructure
            jstructId = new JstructId(fullStructure);
            jstructIdForDisplay = JstructId.createJstructIdForDisplay(fullStructure);
            
            //need ids for lots of db calls to get all the related data
            Long structVerId = fullStructure.getStructVerId();
            Long structureId = fullStructure.getStructureId();
            Long userId = currentUser.getUserId();
            
            if(fullStructure.isFromRcsb()){
                //lineageDiagram = ...; this happens via remote command
            } 
            else 
            {
                fullStructureVersions = fullStructureService.getByStructureId(structureId);
                structureRoles = structureRoleService.getByStructureId(structureId);
            }
            
            //setup permissions on this structure (i.e. who can read,write,grant...)
            setupPermissions();
            
            
            pdbSplits = pdbSplitService.getByStructVerId(structVerId);
            splitsFullStructures = fullStructureService.getByPdbSplits(pdbSplits);
            keywords = pdbKeywordService.getByStructVerId(structVerId);
            authors = pdbAuthorService.getByStructVerId(structVerId);
            jrnl = pdbJrnlService.getPrimaryByStructVerId(structVerId, true);
            compnds = pdbCompndService.getByStructVerId(structVerId);
            sources = pdbSourceService.getByStructVerId(structVerId);
            revdats = pdbRevdatService.getByStructVerId(structVerId);
            remarks = pdbRemarkService.getByStructVerId(structVerId);
            fileAttributes = new JFileAttributes(fullStructure);
            
            editablePdbFile = pdbFileService.getByStructVerId(structVerId);
            
            //user entered notes (both public and private)
            notes = noteService.getByStructureIdForUser(structureId, userId);
            
            
            //label stuff
            allUsersLabels = userLabelService.getByUserId(userId);
            selectedUserLabels = userLabelService.getByUserIdStructureId(userId, structureId);
            newLabel = new Jstruct_UserLabel(currentUser);
            
            // is/isn't a favorite
            favorite = structureUserService.favoriteExists(structureId, userId);
            
            //add this structure to the user's recent list
            structureUserService.addRecentToList(structureId, structVerId, userId);
           
           
        } catch (InvalidParameterException | AccessDeniedException | JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    private void setupPermissions() throws AccessDeniedException {
        
        displayPermissions = !currentUser.isGuest() && !fullStructure.isFromRcsb();
        currentUserIsOwner = currentUser.getUserId().equals(fullStructure.getOwnerId());
        currentUserCanAddNotes = fullStructure.isCurrentUserCanWrite() || (fullStructure.isFromRcsb() && currentUser.getIsRoleContributor());
        
        if(!fullStructure.isCurrentUserCanRead()){
            StringBuilder sb = new StringBuilder();
            sb.append(currentUser.getName()).append(" does not have permissions to view this structure: ");
            sb.append(fullStructure.getJstructDisplayId()).append(". ");
            sb.append("Please contact the owner (").append(fullStructure.getOwnerName()).append(") if you need access.");
            throw new AccessDeniedException(sb.toString());
        }
        
    }
    

    
    /**
     * called after load (remoteCommand) to display sequences
     *
     * usage:
     * <p:remoteCommand name="renderTheSequences" action="#{structureBean.gimmeDaSequences}" autoRun="true" update="theSequences">
     */
    public void gimmeDaSequences() {
        try {

            if (!sequencesBeenDisplayed) {

                chainCount = chainService.getCountByStructVerId(fullStructure.getStructVerId());

                if (chainCount <= maxChainsForVerboseDisplay) {
                    chains = chainService.getByStructVerId(fullStructure.getStructVerId(), false, true);
                } else {
                    chains = chainService.getByStructVerId(fullStructure.getStructVerId(), false, false);
                }

                displaySequences = new ArrayList<>(chains.size());
                for (Jstruct_Chain chain : chains) {
                    displaySequences.add(new DisplaySequence(chain, "Index:"));
                }

            }
            sequencesBeenDisplayed = true;

        } catch (InvalidParameterException | JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    public void gimmeDaPdbLineageDiagram(){
        try {
       
            HTMLTag createPdbLineageDiagram = StructureUtil.createRcsbLineageDiagram(fullStructure.getPdbIdentifier());
            lineageDiagram = createPdbLineageDiagram.toHTML();
        
      } catch (InvalidParameterException | JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    
    /**
     * 
     * @param role
     * @param newValue 
     */
    public void updateStructureRole(String role, boolean newValue){
        try {

            String roleForDisplay = "";
            switch (role) {

                case "read":
                    if (newValue) {
                        structureService.addAllCanRead(fullStructure.getStructureId());
                        roleForDisplay = "Read";
                    } else {
                        structureService.removeAllCanRead(fullStructure.getStructureId());
                        roleForDisplay = "none";
                    }
                    structureService.removeAllCanWrite(fullStructure.getStructureId());
                    break;

                case "write":
                    if (newValue) {
                        structureService.addAllCanWrite(fullStructure.getStructureId());
                        structureService.addAllCanRead(fullStructure.getStructureId());
                        roleForDisplay = "Read, Write";
                    } else {
                        structureService.removeAllCanWrite(fullStructure.getStructureId());
                        roleForDisplay = "Read";
                    }
                    break;
            }
            fullStructure = fullStructureService.getByJstructId(jstructId, false);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Permissions updated all users", roleForDisplay));
         
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    /**
     * 
     * @param userId
     * @param role
     * @param newValue 
     */
    public void updateRole(Long userId, String role, boolean newValue){
        try {
        
            String roleForDisplay = "";
            Jstruct_StructureRole selectedStructureRole = structureRoleService.getByStructureIdUserId(fullStructure.getStructureId(), userId);
            
            switch (role) {

                case "read":
                    if (newValue) {
                        structureRoleService.giveRoleUpToRead(selectedStructureRole);
                        roleForDisplay = "Read";
                    } else {
                        structureRoleService.delete(selectedStructureRole);
                        roleForDisplay = "none";
                    }
                    break;

                case "write":
                    if (newValue) {
                        structureRoleService.giveRoleUpToWrite(selectedStructureRole);
                        roleForDisplay = "Read, Write";
                    } else {
                        structureRoleService.giveRoleUpToRead(selectedStructureRole);
                        roleForDisplay = "Read";
                    }
                    break;

                case "grant":
                    if (newValue) {
                        structureRoleService.giveRoleUpToGrant(selectedStructureRole);
                        roleForDisplay = "Read, Write, Grant";
                    } else {
                        structureRoleService.giveRoleUpToWrite(selectedStructureRole);
                        roleForDisplay = "Read, Write";
                    }
                    break;
            }
            
            structureRoles = structureRoleService.getByStructureId(fullStructure.getStructureId());
            
            Jstruct_User user = jstructUserService.getById(userId);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Permissions updated for " + user.getName(), roleForDisplay));
         
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    
    
    
    public void addStructureRoleUserRead(ActionEvent actionEvent) {
        addStructureRoleUser(true, false, false);
    }
    public void addStructureRoleUserWrite(ActionEvent actionEvent) {
        addStructureRoleUser(true, true, false);
    }
    public void addStructureRoleUserGrant(ActionEvent actionEvent) {
        addStructureRoleUser(true, true, true);
    }
    
    
    private void addStructureRoleUser(boolean hasRead, boolean hasWrite, boolean hasGrant){
        try {
            
            //first find if user/structure role is existing 
            Jstruct_StructureRole existingStructureRole = structureRoleService.getByStructureIdUserId(fullStructure.getStructureId(), newStructureRoleUser.getUserId());
            if(existingStructureRole != null){
                //existing and new role is additive
                existingStructureRole.setCanRead(hasRead || existingStructureRole.isCanRead());
                existingStructureRole.setCanWrite(hasWrite || existingStructureRole.isCanWrite());
                existingStructureRole.setCanGrant(hasGrant || existingStructureRole.isCanGrant());
                structureRoleService.update(existingStructureRole);
            } else {
                Jstruct_StructureRole structureRole = new Jstruct_StructureRole(fullStructure.getStructureId(), newStructureRoleUser, hasRead, hasWrite, hasGrant);
                structureRoleService.add(structureRole);
            }
            
            structureRoles = structureRoleService.getByStructureId(fullStructure.getStructureId());
            
            String roleForDisplay = hasGrant ? "Read, Write, Grant " : hasWrite ? "Read, Write" : "Read";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Permissions added for " + newStructureRoleUser.getName(), roleForDisplay));

            newStructureRoleUser = null;            

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    
    
    /**
     * toggle the favorite star on/off for this structure/user
     * @param actionEvent 
     */
    public void toggleFav(ActionEvent actionEvent) {
        try {
            
            if(favorite){
                structureUserService.removeFavoriteFromList(fullStructure.getStructureId(), currentUser.getUserId());
            } else {
                structureUserService.addFavoriteToList(fullStructure.getStructureId(), fullStructure.getStructVerId(), currentUser.getUserId());
            }
            favorite = !favorite;
            
            //todo why is the growl not working?!
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, favorite?"Favorite added.":"Favorite removed.", fullStructure.getJstructDisplayId()));

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    /**
     * toggle the obsolete status of this structure (obsolete/active)
     */
    public void toggleObsolete() {
        try {
            
            if(fullStructure.isObsolete()){
                structureService.setToActive(fullStructure.getStructureId());
            } else {
                structureService.setToObsolete(fullStructure.getStructureId());
            }
            fullStructure = fullStructureService.getByStructVerId(fullStructure.getStructVerId());
           
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, fullStructure.isObsolete()?"Structure set to Obsolete.":"Structure set to Active.", fullStructure.getJstructDisplayId()));

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    /**
     * update all chains for this structure
     * (typically used when a change in code demands some updates to the chain info stored in the database)
     * @param actionEvent 
     */
    public void updateChainData(ActionEvent actionEvent){
        
        try {
            
            Long structVerId = fullStructure.getStructVerId();

            StructFileService structFileService = new StructFileService();
            
            //Date startTimeGetStructure = new Date();
            Structure structureObj = structFileService.getAsStructure(structVerId);

            //Date startTimeUpdateChains = new Date();
            JImportUtils.updateChainsForStructure(structureObj, structVerId);
            
            //Date endTime = new Date();
            //System.out.println("-------------------------------------------------------------------------");
            //System.out.println(" -- structure: " + fullStructure.getJstructDisplayId());
            //System.out.println(" ---- startTimeGetStructure: " + JDateUtil.defaultDateAndTimeFormat(startTimeGetStructure, true));
            //System.out.println(" ---- startTimeUpdateChains: " + JDateUtil.defaultDateAndTimeFormat(startTimeUpdateChains, true));
            //System.out.println(" ----               endTime: " + JDateUtil.defaultDateAndTimeFormat(endTime, true));
            //System.out.println(" ---- ");
            //System.out.println(" ------------ getStructure time: " + JDateUtil.differenceBetweenDatesToElapsedTime(startTimeGetStructure, startTimeUpdateChains));
            //System.out.println(" ------------ updateChains time: " + JDateUtil.differenceBetweenDatesToElapsedTime(startTimeUpdateChains, endTime));
            //System.out.println(" ------------        total time: " + JDateUtil.differenceBetweenDatesToElapsedTime(startTimeGetStructure, endTime));
            
            sequencesBeenDisplayed = false;
            gimmeDaSequences();
                    
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Updating Chain info..", fullStructure.getJstructDisplayId()));

        } catch (Exception ex) {
            HijackError.gotoErrorPage(ex);
        }
    }
    
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        actions for labels
    */
    
    public void createNewLabel(ActionEvent actionEvent) {
        try {
            Jstruct_UserLabel createdLabel = userLabelService.add(newLabel, currentUser);
            
            Jstruct_User user = sessionBean.getUser();
            newLabel = new Jstruct_UserLabel(user);
            allUsersLabels = userLabelService.getByUserId(user.getUserId());
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "New Label created", createdLabel.getLabelName()));

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    public void updateLabels(ActionEvent actionEvent) {
        try {
            structureLabelService.replaceStructureLabelsForUserAndStructure(currentUser, jstructId.getStructureId(), selectedUserLabels);
            
            selectedUserLabels = userLabelService.getByUserIdStructureId(currentUser.getUserId(), jstructId.getStructureId());
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Labels updated", ""));

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        actions for editing header/title
    */
    
    public void editHeaderSection(ActionEvent actionEvent) {
        try {
            editablePdbFile = pdbFileService.getByStructVerId(fullStructure.getStructVerId());  
            displayModeForHeader = "edit";
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    public void saveHeaderSection(ActionEvent actionEvent) {
        try {
            editablePdbFile = pdbFileService.update(editablePdbFile, true, true);
            
            fullStructure = fullStructureService.getByStructVerId(fullStructure.getStructVerId());
            keywords = pdbKeywordService.getByStructVerId(fullStructure.getStructVerId());
            authors = pdbAuthorService.getByStructVerId(fullStructure.getStructVerId());
            
            displayModeForHeader = "view";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Header/Title section updated.", ""));
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    public void cancelHeaderSection(ActionEvent actionEvent) {
        displayModeForHeader = "view";
    }
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        actions for editing jrnl
    */
    
    public void editJrnlSection(ActionEvent actionEvent) {
        try {
            jrnl = pdbJrnlService.getPrimaryByStructVerId(fullStructure.getStructVerId(), false);
            if(jrnl==null){
                jrnl = new Jstruct_PdbJrnl();
                jrnl.setPrimary(true);
                jrnl.setStructVerId(fullStructure.getStructVerId());
            }
            displayModeForJrnl = "edit";
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    public void saveJrnlSection(ActionEvent actionEvent) {
        try {
            displayModeForJrnl = "view";
            pdbJrnlService.update(jrnl);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "JRNL section updated.", ""));
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    public void cancelJrnlSection(ActionEvent actionEvent) {
        try{
            jrnl = pdbJrnlService.getPrimaryByStructVerId(fullStructure.getStructVerId(), true);
            displayModeForJrnl = "view";
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        actions for editing metadata (structVersion)
    */
    
    public void editMetadataSection(ActionEvent actionEvent) {
        try {
            editableStructVersion = structVersionService.getByStructVerId(fullStructure.getStructVerId());
            displayModeForMetadata = "edit";
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    public void saveMetadataSection(ActionEvent actionEvent) {
        try {
            editableStructVersion = structVersionService.update(editableStructVersion, currentUser);
            fullStructure = fullStructureService.getByStructVerId(fullStructure.getStructVerId());
            displayModeForMetadata = "view";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Metadata section updated.", ""));
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    public void cancelMetadataSection(ActionEvent actionEvent) {
        displayModeForMetadata = "view";
    }
    
    
     
    
    public void addNote(ActionEvent actionEvent) {
        try {
            
            if(!StringUtil.isSet(newNote.getFormattedComment())){
                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Note comment is required.", null));
            } else {
                
                String formattedComment = newNote.getFormattedComment();
                String lowerComment = Jsoup.parse(formattedComment).text().toLowerCase();
                noteService.createNote(fullStructure.getStructureId(), fullStructure.getStructVerId(), formattedComment, lowerComment, newNote.isNotePublic(), currentUser);
                
                notes = noteService.getByStructureIdForUser(fullStructure.getStructureId(), currentUser.getUserId());
                newNote.setFormattedComment("");
                newNote.setLowerComment("");
                noteSectionOpen = true;
                
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Note added.", JStringUtil.cleaveString(newNote.getLowerComment(), 50)));
            }

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
  
    //save updates to a Note row
    public void onNoteEdit(RowEditEvent event) {
        try{
            
            Jstruct_Note updatedNote = (Jstruct_Note) event.getObject();
            
            String formattedComment = updatedNote.getFormattedComment();
            updatedNote.setLowerComment( Jsoup.parse(formattedComment).text().toLowerCase());
            
            noteService.updateNote(updatedNote, currentUser);
            noteSectionOpen = true;
            
            FacesMessage msg = new FacesMessage("Note Updated", JStringUtil.cleaveString(updatedNote.getLowerComment(), 50));
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (JDAOException jde) {
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Note update failed", jde.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }
     
    //fuhgeddaboudit
    public void onNoteCancel(RowEditEvent event) {
    }
    
    /**
     * delete the user selected note
     * @param actionEvent 
     */
    public void deleteNote(ActionEvent actionEvent) {
        try {
            Long doomedNoteId = (Long) actionEvent.getComponent().getAttributes().get("doomedNoteId");
            Jstruct_Note doomedNote = noteService.getByNoteId(doomedNoteId);

            String doomedNoteComment = JStringUtil.cleaveString(doomedNote.getLowerComment(), 50);
            noteService.deleteNote(doomedNote);
            
            notes = noteService.getByStructureIdForUser(fullStructure.getStructureId(), currentUser.getUserId());
            
            noteSectionOpen = true;
            
            FacesMessage msg = new FacesMessage("Note deleted", doomedNoteComment);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
            
            
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    
    
    
    
    
    
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        action for redirecting to the 'new/edit structure page'
    */
    public String replaceStructure() {
        return "/new_structure.xhtml?faces-redirect=true&id="+fullStructure.getStructVerId();
    }
    
    
    
    
    
    
    
    /* getters/setters */
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public Jstruct_User getCurrentUser() {return currentUser; }

    public Integer getMaxChainsForVerboseDisplay() { return maxChainsForVerboseDisplay; }
    
    public String getIdParam() {return idParam;}
    public void setIdParam(String idParam) { this.idParam = idParam;}

    public JstructId getJstructId() { return jstructId;}
    public void setJstructId(JstructId jstructId) { this.jstructId = jstructId;}

    public String getJstructIdForDisplay() {return jstructIdForDisplay; }
    public void setJstructIdForDisplay(String jstructIdForDisplay) { this.jstructIdForDisplay = jstructIdForDisplay;}

    public Jstruct_FullStructure getFullStructure() { return fullStructure;}
    public void setFullStructure(Jstruct_FullStructure fullStructure) { this.fullStructure = fullStructure;}

    public Jstruct_StructVersion getEditableStructVersion() {return editableStructVersion;}
    public void setEditableStructVersion(Jstruct_StructVersion editableStructVersion) {this.editableStructVersion = editableStructVersion; }

    public Jstruct_PdbFile getEditablePdbFile() { return editablePdbFile;}
    public void setEditablePdbFile(Jstruct_PdbFile editablePdbFile) {this.editablePdbFile = editablePdbFile;}

    public String getLineageDiagram() {return lineageDiagram; }
    public void setLineageDiagram(String lineageDiagram) {this.lineageDiagram = lineageDiagram; }
    
    
    
    /**
     * the header/title (and jrnl) can be edited IF:
     *  - it is a manual upload
     *  - AND it is active
     *  - AND it is 'most current'
     *  - AND is was not 'parsable' (aka pdb/cif/moe file)
     * @return 
     */
    public boolean isAllowHeaderAndJrnlEditing(){
        if(fullStructure.isFromRcsb() || fullStructure.isObsolete() || !fullStructure.isMostCurrent() || StringUtil.isSet(fullStructure.getPdbIdentifier())) {
            return false;
        }
        return fullStructure.isCurrentUserCanWrite();
    }
  
    
    /**
     * the Metadata can be edited IF:
     *  - it is a manual upload
     *  - AND it is active
     *  - AND it is 'most current'
     * @return 
     */
    public boolean isAllowMetadataEditing(){
        if(fullStructure.isFromRcsb() || fullStructure.isObsolete() || !fullStructure.isMostCurrent()) {
            return false;
        }
        return fullStructure.isCurrentUserCanWrite();
    }
    
    
    /**
     * the file/structure can be 'replaced' with a newer version IF:
     *  - it is a manual upload
     *  - AND it is active
     *  - AND it is 'most current'
     * @return 
     */
    public boolean isAllowReplacing(){
        if(fullStructure.isFromRcsb() || fullStructure.isObsolete() || !fullStructure.isMostCurrent()) {
            return false;
        }
        return fullStructure.isCurrentUserCanWrite();
    }
  
    
    
    
    
    /**
     * a file is considered 'parsable' if it is a pdb/cif file
     * @return 
     */
    public boolean isFileParsable(){ 
        try{
            return JFileUtils.isPdbOrCifFile(fullStructure.getActualFileName());
        } catch (JException ex) {
            return false;
        } 
    }
    /**
     * a file is considered 'chain parsable' if it is a pdb/cif/moe file
     * @return 
     */
    public boolean isFileChainParsable(){ 
        try{
            String fileName = fullStructure.getActualFileName();
            if(!StringUtil.isSet(fileName)){
                return false;
            }
            return JFileUtils.isPdbOrCifFile(fileName) || fileName.toUpperCase().endsWith(".MOE");
        } catch (JException ex) {
            return false;
        } 
    }

    
    
    
    
    
    
    
    public List<Jstruct_StructureRole> getStructureRoles() {
        return structureRoles;
    }

    public void setStructureRoles(List<Jstruct_StructureRole> structureRoles) {
        this.structureRoles = structureRoles;
    }

    public Jstruct_User getNewStructureRoleUser() {
        return newStructureRoleUser;
    }

    public void setNewStructureRoleUser(Jstruct_User newStructureRoleUser) {
        this.newStructureRoleUser = newStructureRoleUser;
    }

    
    
    public boolean isDisplayPermissions() {
        return displayPermissions;
    }

    public void setDisplayPermissions(boolean displayPermissions) {
        this.displayPermissions = displayPermissions;
    }

    public boolean isCurrentUserIsOwner() {
        return currentUserIsOwner;
    }

    public void setCurrentUserIsOwner(boolean currentUserIsOwner) {
        this.currentUserIsOwner = currentUserIsOwner;
    }

    public boolean isCurrentUserCanAddNotes() {
        return currentUserCanAddNotes;
    }

    public void setCurrentUserCanAddNotes(boolean currentUserCanAddNotes) {
        this.currentUserCanAddNotes = currentUserCanAddNotes;
    }

    

    
    
    
    
    
    
    public List<Jstruct_PdbSplit> getPdbSplits() {
        return pdbSplits;
    }

    public void setPdbSplits(List<Jstruct_PdbSplit> pdbSplits) {
        this.pdbSplits = pdbSplits;
    }

    public List<Jstruct_FullStructure> getSplitsFullStructures() {
        return splitsFullStructures;
    }

    public void setSplitsFullStructures(List<Jstruct_FullStructure> splitsFullStructures) {
        this.splitsFullStructures = splitsFullStructures;
    }

    public List<Jstruct_PdbKeyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Jstruct_PdbKeyword> keywords) {
        this.keywords = keywords;
    }

    public List<Jstruct_PdbAuthor> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Jstruct_PdbAuthor> authors) {
        this.authors = authors;
    }

    public Jstruct_PdbJrnl getJrnl() {
        return jrnl;
    }

    public void setJrnl(Jstruct_PdbJrnl jrnl) {
        this.jrnl = jrnl;
    }

    public List<Jstruct_PdbCompnd> getCompnds() {
        return compnds;
    }

    public void setCompnds(List<Jstruct_PdbCompnd> compnds) {
        this.compnds = compnds;
    }

    public List<Jstruct_PdbSource> getSources() {
        return sources;
    }

    public void setSources(List<Jstruct_PdbSource> sources) {
        this.sources = sources;
    }

    public List<Jstruct_PdbRevdat> getRevdats() {
        return revdats;
    }

    public void setRevdats(List<Jstruct_PdbRevdat> revdats) {
        this.revdats = revdats;
    }

    public List<Jstruct_PdbRemark> getRemarks() {
        return remarks;
    }

    public void setRemarks(List<Jstruct_PdbRemark> remarks) {
        this.remarks = remarks;
    }

    public JFileAttributes getFileAttributes() {
        return fileAttributes;
    }

    public void setFileAttributes(JFileAttributes fileAttributes) {
        this.fileAttributes = fileAttributes;
    }

    public List<Jstruct_Chain> getChains() {
        return chains;
    }

    public void setChains(List<Jstruct_Chain> chains) {
        this.chains = chains;
    }

    public List<DisplaySequence> getDisplaySequences() {
        return displaySequences;
    }

    public void setDisplaySequences(List<DisplaySequence> displaySequences) {
        this.displaySequences = displaySequences;
    }

    public Jstruct_UserLabel getNewLabel() {
        return newLabel;
    }

    public void setNewLabel(Jstruct_UserLabel newLabel) {
        this.newLabel = newLabel;
    }

    public List<Jstruct_UserLabel> getAllUsersLabels() {
        return allUsersLabels;
    }

    public void setAllUsersLabels(List<Jstruct_UserLabel> allUsersLabels) {
        this.allUsersLabels = allUsersLabels;
    }

    public List<Jstruct_UserLabel> getSelectedUserLabels() {
        return selectedUserLabels;
    }

    public void setSelectedUserLabels(List<Jstruct_UserLabel> selectedUserLabels) {
        this.selectedUserLabels = selectedUserLabels;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getDisplayModeForHeader() {
        return displayModeForHeader;
    }

    public void setDisplayModeForHeader(String displayModeForHeader) {
        this.displayModeForHeader = displayModeForHeader;
    }

    public String getDisplayModeForJrnl() {
        return displayModeForJrnl;
    }

    public void setDisplayModeForJrnl(String displayModeForJrnl) {
        this.displayModeForJrnl = displayModeForJrnl;
    }

    public String getDisplayModeForMetadata() {
        return displayModeForMetadata;
    }

    public void setDisplayModeForMetadata(String displayModeForMetadata) {
        this.displayModeForMetadata = displayModeForMetadata;
    }

    public boolean isSequencesBeenDisplayed() {
        return sequencesBeenDisplayed;
    }

    public List<Jstruct_FullStructure> getFullStructureVersions() {
        return fullStructureVersions;
    }

    public void setFullStructureVersions(List<Jstruct_FullStructure> fullStructureVersions) {
        this.fullStructureVersions = fullStructureVersions;
    }

    public Integer getChainCount() {
        return chainCount;
    }

    public void setChainCount(Integer chainCount) {
        this.chainCount = chainCount;
    }

    public StructureFastaLink getStructureFastaLink() {
        return structureFastaLink;
    }

    public void setStructureFastaLink(StructureFastaLink structureFastaLink) {
        this.structureFastaLink = structureFastaLink;
    }

    public List<Jstruct_Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Jstruct_Note> notes) {
        this.notes = notes;
    }

    public Jstruct_Note getNewNote() {
        return newNote;
    }

    public void setNewNote(Jstruct_Note newNote) {
        this.newNote = newNote;
    }

    public boolean isNoteSectionOpen() {
        return noteSectionOpen;
    }

    public void setNoteSectionOpen(boolean noteSectionOpen) {
        this.noteSectionOpen = noteSectionOpen;
    }

    

   
    
}
