package com.just.jstruct.backing;

import com.just.jstruct.dao.service.ProgramService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.NoAccessException;
import com.just.jstruct.model.Jstruct_Program;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.NoAccess;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 * list of all records from program table - and ability to manage 'em
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="programList")
@RequestScoped
public class ProgramListBean implements Serializable {
 
    private static final long serialVersionUID = 1L;

    
    /* data members */
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private List<Jstruct_Program> programs;
    private Jstruct_Program newProgram = new Jstruct_Program();


    /* supporting service classes */
    ProgramService programService = new ProgramService();

    
    /* constructor */
    public ProgramListBean() {  }
    
    @PostConstruct
    private void init(){
        try {
            
            checkPagePermissions();
           
            programs = programService.getAll();
            newProgram.setDeprecated(false);
           
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }
    
    
    //access to this page is avaliable only for Admin/Developer users
    private void checkPagePermissions() {
        if (!sessionBean.getUser().getIsRoleAdmin() && !sessionBean.getUser().getIsRoleDeveloper()) {
            NoAccess.gotoNoAccessPage(new NoAccessException("attempt to access Admin Actions page",
                                                            "Admin Actions",
                                                            "n/a"));
        }
    }
    
    
    /* getterts/setters */

    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    public List<Jstruct_Program> getPrograms() {return programs;}
    public void setPrograms(List<Jstruct_Program> programs) {this.programs = programs;}

    public Jstruct_Program getNewProgram() {return newProgram;}
    public void setNewProgram(Jstruct_Program newProgram) { this.newProgram = newProgram;}

    
    
    //add a Jstruct_Program, return to the (same) list page
    public String save() {
        try {
            programService.addProgram(newProgram, sessionBean.getUser());
        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        } 
        return "/lookups.xhtml?faces-redirect=true";
    }
    
    
    
    //save updates to a Program row
    public void onRowEdit(RowEditEvent event) {
        try{
            Jstruct_Program updatedProgram = (Jstruct_Program) event.getObject();
            programService.updateProgram(updatedProgram);
            
            FacesMessage msg = new FacesMessage("Program updated", ((Jstruct_Program) event.getObject()).getName());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            
        } catch (JDAOException jde) {
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Program update failed", jde.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }
     
    //fuhgeddaboudit
    public void onRowCancel(RowEditEvent event) {
        //FacesMessage msg = new FacesMessage("Edit Cancelled", ((Jstruct_Program) event.getObject()).getName());
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    
    
    




  

    


    
}
