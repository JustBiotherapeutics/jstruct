package com.just.jstruct.backing;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.enums.ChainType;
import com.just.jstruct.dao.service.StructureLabelService;
import com.just.jstruct.dao.service.StructureRoleService;
import com.just.jstruct.dao.service.StructureService;
import com.just.jstruct.dao.service.UserLabelService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_StructureRole;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserLabel;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.PasSearch;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.structureSearch.subQuery.SubQuery;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.FastaUtil;
import com.just.jstruct.utilities.HijackError;
import com.just.jstruct.utilities.JStringUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.util.CollectionUtils;

/**
 * Backing bean for the Structure Search (criteria and results) page
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="searchBean") 
@SessionScoped   
public class SearchBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    
    /* service classes */
    private final StructureRoleService structureRoleService = new StructureRoleService();
    private final StructureService structureService = new StructureService();
    private final UserLabelService userLabelService = new UserLabelService();
    private final StructureLabelService structureLabelService = new StructureLabelService();
    
    /* data members */
    
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;
    
    private String renderThe;
    private String searchSubmitError;
    
    private FullQueryInput fullQueryInput;
    private List<Jstruct_FullStructure> fullStructures;
    private List<Jstruct_FullStructure> structureSelection;
    
    private String newSearchName;
    private boolean saveSearchSuccessful;

    private Integer overallCountAnd = null;
    private Integer overallCountOr = null;
    
    //urls to create fasta file based on all search results
    private String proteinSeqresFastaUrl;
    private String proteinAtomFastaUrl;
    private String nucleicSeqresFastaUrl;
    private String nucleicAtomFastaUrl;
    private String allSeqresFastaUrl;
    private String allAtomFastaUrl;
    
    //realted to bulk permission updating
    private List<Jstruct_FullStructure> bulkPermissionAllStructures;
    private List<Jstruct_FullStructure> bulkPermissionGrantableStructures;
    private Jstruct_User bulkPermissionUpdateUser;
    private boolean userCanRead;
    private boolean userCanWrite;
    private boolean userCanGrant;
    private boolean allCanRead;
    private boolean allCanWrite;
    private int activePermissionTabIndex = 0;
    
    //related to bulk label updating
    private List<Jstruct_FullStructure> bulkLabelAllStructures;
    private List<Jstruct_FullStructure> bulkLabelReadableStructures;
    private List<Jstruct_UserLabel> allUsersLabels;
    private List<Jstruct_UserLabel> selectedUserLabels = new ArrayList<>();
    private Jstruct_UserLabel newLabel;
    private int activeLabelTabIndex = 0;
    
    
    //constructor
    public SearchBean() {
    }
    
    @PostConstruct
    public void init() {
        
        try {
            
            currentUser = sessionBean.getUser();
            //fullStructureService = new FullStructureService(currentUser);
                    
            renderThe = "results";
            
            fullQueryInput = (FullQueryInput) FacesUtil.retrieveFromSession("fullQueryInput");
            
            if(fullQueryInput==null){
                fullQueryInput = new FullQueryInput(FileStatus.FILE_STATUS_ACTIVE, FileStatus.FILE_STATUS_ACTIVE, QueryMode.AND);
                fullQueryInput.addSubQueryInput(new SubQueryInput(currentUser));
                renderThe = "criteria";
            } else {
                if (performSearch()) {
                    renderThe = "results";
                } else {
                    renderThe = "criteria";
                }
            }

        } catch (JDAOException | NoSuchFieldException e) {
            HijackError.gotoErrorPage(e);
        }
        
    }    
    
    
    
    /* getters/setters */
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}

    public String getRenderThe() {return renderThe;}
    public void setRenderThe(String renderThe) {this.renderThe = renderThe;}

    public String getSearchSubmitError() {return searchSubmitError;}
    public void setSearchSubmitError(String searchSubmitError) { this.searchSubmitError = searchSubmitError;}

    public FullQueryInput getFullQueryInput() { return fullQueryInput;}
    public void setFullQueryInput(FullQueryInput fullQueryInput) {this.fullQueryInput = fullQueryInput; }
    
    public List<Jstruct_FullStructure> getFullStructures() { return fullStructures;}
    public void setFullStructures(List<Jstruct_FullStructure> fullStructures) { this.fullStructures = fullStructures; }
    
    public List<Jstruct_FullStructure> getStructureSelection() { return structureSelection;}
    public void setStructureSelection(List<Jstruct_FullStructure> structureSelection) { this.structureSelection = structureSelection;}
    
    public String getNewSearchName() {return newSearchName;}
    public void setNewSearchName(String newSearchName) { this.newSearchName = newSearchName;}

    public boolean isSaveSearchSuccessful() {return saveSearchSuccessful; }
    public void setSaveSearchSuccessful(boolean saveSearchSuccessful) { this.saveSearchSuccessful = saveSearchSuccessful;}

    public Integer getOverallCountAnd() {return overallCountAnd;}
    public void setOverallCountAnd(Integer overallCountAnd) {this.overallCountAnd = overallCountAnd;}
    public Integer getOverallCountOr() {return overallCountOr;}
    public void setOverallCountOr(Integer overallCountOr) {this.overallCountOr = overallCountOr;}

    public String getProteinSeqresFastaUrl() {return proteinSeqresFastaUrl;}
    public String getProteinAtomFastaUrl() {return proteinAtomFastaUrl;}
    public String getNucleicSeqresFastaUrl() {return nucleicSeqresFastaUrl;}
    public String getNucleicAtomFastaUrl() {return nucleicAtomFastaUrl;}
    public String getAllSeqresFastaUrl() {return allSeqresFastaUrl;}
    public String getAllAtomFastaUrl() {return allAtomFastaUrl;}

    
    
    public List<Jstruct_FullStructure> getBulkPermissionAllStructures() { return bulkPermissionAllStructures; }
    public void setBulkPermissionAllStructures(List<Jstruct_FullStructure> bulkPermissionAllStructures) {this.bulkPermissionAllStructures = bulkPermissionAllStructures;  }

    public List<Jstruct_FullStructure> getBulkPermissionGrantableStructures() {  return bulkPermissionGrantableStructures; }
    public void setBulkPermissionGrantableStructures(List<Jstruct_FullStructure> bulkPermissionGrantableStructures) {  this.bulkPermissionGrantableStructures = bulkPermissionGrantableStructures; }

    public Jstruct_User getBulkPermissionUpdateUser() { return bulkPermissionUpdateUser;}
    public void setBulkPermissionUpdateUser(Jstruct_User bulkPermissionUpdateUser) { this.bulkPermissionUpdateUser = bulkPermissionUpdateUser; }

    public boolean isUserCanRead() {return userCanRead; }
    public void setUserCanRead(boolean userCanRead) { this.userCanRead = userCanRead; }

    public boolean isUserCanWrite() { return userCanWrite; }
    public void setUserCanWrite(boolean userCanWrite) {this.userCanWrite = userCanWrite; }

    public boolean isUserCanGrant() { return userCanGrant; }
    public void setUserCanGrant(boolean userCanGrant) {this.userCanGrant = userCanGrant; }

    public boolean isAllCanRead() { return allCanRead; }
    public void setAllCanRead(boolean allCanRead) {this.allCanRead = allCanRead; }

    public boolean isAllCanWrite() { return allCanWrite; }
    public void setAllCanWrite(boolean allCanWrite) { this.allCanWrite = allCanWrite; }

    public int getActivePermissionTabIndex() { return activePermissionTabIndex; }
    public void setActivePermissionTabIndex(int activePermissionTabIndex) { this.activePermissionTabIndex = activePermissionTabIndex;}

    
    
    public List<Jstruct_FullStructure> getBulkLabelAllStructures() { return bulkLabelAllStructures; }
    public void setBulkLabelAllStructures(List<Jstruct_FullStructure> bulkLabelAllStructures) { this.bulkLabelAllStructures = bulkLabelAllStructures; }

    public List<Jstruct_FullStructure> getBulkLabelReadableStructures() { return bulkLabelReadableStructures; }
    public void setBulkLabelReadableStructures(List<Jstruct_FullStructure> bulkLabelReadableStructures) { this.bulkLabelReadableStructures = bulkLabelReadableStructures; }

    public List<Jstruct_UserLabel> getAllUsersLabels() { return allUsersLabels; }
    public void setAllUsersLabels(List<Jstruct_UserLabel> allUsersLabels) { this.allUsersLabels = allUsersLabels; }

    public List<Jstruct_UserLabel> getSelectedUserLabels() { return selectedUserLabels; }
    public void setSelectedUserLabels(List<Jstruct_UserLabel> selectedUserLabels) {this.selectedUserLabels = selectedUserLabels; }

    public Jstruct_UserLabel getNewLabel() { return newLabel; }
    public void setNewLabel(Jstruct_UserLabel newLabel) { this.newLabel = newLabel; }

    public int getActiveLabelTabIndex() { return activeLabelTabIndex; }
    public void setActiveLabelTabIndex(int activeLabelTabIndex) { this.activeLabelTabIndex = activeLabelTabIndex; }


    
    
    
    
    
     
   /* #######################################################################################################################################
                    GENERAL methods
      #######################################################################################################################################  */
    
    
    private boolean performSearch() throws JDAOException, NoSuchFieldException
    {
        //first, validate user input
        if(criteriaIsValid())
        {
            PasSearch pasSearch = new PasSearch(fullQueryInput);
            fullStructures = pasSearch.execute(currentUser);
            
            proteinAtomFastaUrl = FastaUtil.createFastaUrl(fullQueryInput, "ATOM", FastaUtil.CHAIN_TYPE_PROTEIN, "protein_atom.fasta");
            proteinSeqresFastaUrl = FastaUtil.createFastaUrl(fullQueryInput, "SEQRES", FastaUtil.CHAIN_TYPE_PROTEIN, "protein_seqres.fasta");
            nucleicAtomFastaUrl = FastaUtil.createFastaUrl(fullQueryInput, "ATOM", FastaUtil.CHAIN_TYPE_NUCLEIC_ACID, "nucleicacid_atom.fasta");
            nucleicSeqresFastaUrl = FastaUtil.createFastaUrl(fullQueryInput, "SEQRES", FastaUtil.CHAIN_TYPE_NUCLEIC_ACID, "nucleicacid_seqres.fasta");
            allAtomFastaUrl = FastaUtil.createFastaUrl(fullQueryInput, "ATOM", FastaUtil.CHAIN_TYPE_ALL, "atom.fasta");
            allSeqresFastaUrl = FastaUtil.createFastaUrl(fullQueryInput, "SEQRES", FastaUtil.CHAIN_TYPE_ALL, "seqres.fasta");
            
            return true;
        } 
        else
        {
            return false;
        }
        
    }
    
 
   
    private boolean criteriaIsValid(){
        
        boolean noErrors = true;
        
        for(SubQuery subQuery : fullQueryInput.getAsSubQuerys()){
            if(!subQuery.validateInput()){
                noErrors = false;
            }
        }
        
        return noErrors;
    }
    
    
   /* #######################################################################################################################################
                    SEARCH RESULTS methods
      #######################################################################################################################################  */
    
    /**
     * @param actionEvent 
     */
    public void refineSearch(ActionEvent actionEvent){
        renderThe = "criteria";
    }
    
    
    /**
     * 
     */
    public void openSaveSearchForm(){
        newSearchName = "";
        saveSearchSuccessful = false;
    }
    
    
    /**
     * @param actionEvent 
     */
    public void startNewSearch(ActionEvent actionEvent){
        
        structureSelection = null;
        
        FacesUtil.deleteFromSession("fullQueryInput");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);
           
        renderThe = "criteria";
    }

    /**
     * 
     * @param actionEvent 
     */
    public void saveSearch(ActionEvent actionEvent){
        
        try{
            
            PostgresDao.saveCriteriaToDatabase(fullQueryInput, currentUser, newSearchName); 
            saveSearchSuccessful = true;
            
            RequestContext.getCurrentInstance().execute("PF('saveSearchDialog').hide();");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Search saved.", newSearchName));
                                     //doesnt work: oncomplete="if(args &amp;&amp; #{searchBean.saveSearchSuccessful})  PF('saveSearchDialog').hide();"
            
        } catch (JDAOException | NoSuchFieldException ex) {
            HijackError.gotoErrorPage(ex);
        }
        
    }
    
    
    
    public void createFastaUrlForSelectedProteinAtom() {
        createFastaFileForSelectedStructures("ATOM", FastaUtil.CHAIN_TYPE_PROTEIN, "protein_atom.fasta");
    }
    public void createFastaUrlForSelectedProteinSeqres() {
        createFastaFileForSelectedStructures("SEQRES", FastaUtil.CHAIN_TYPE_PROTEIN, "protein_atom.fasta");
    }
    public void createFastaUrlForSelectedNucleicAtom() {
        createFastaFileForSelectedStructures("ATOM", FastaUtil.CHAIN_TYPE_NUCLEIC_ACID, "protein_atom.fasta");
    }
    public void createFastaUrlForSelectedNucleicSeqres() {
        createFastaFileForSelectedStructures("SEQRES", FastaUtil.CHAIN_TYPE_NUCLEIC_ACID, "protein_atom.fasta");
    }
    public void createFastaUrlForSelectedAllAtom() {
        createFastaFileForSelectedStructures("ATOM", FastaUtil.CHAIN_TYPE_ALL, "atom.fasta");
    }
    public void createFastaUrlForSelectedAllSeqres() {
        createFastaFileForSelectedStructures("SEQRES", FastaUtil.CHAIN_TYPE_ALL, "atom.fasta");
    }
    
    
    /**
     * create fasta file for all selected structure user has read access for
     * @param sequenceFrom
     * @param chainTypes
     * @param outputFileName 
     */
    private void createFastaFileForSelectedStructures(String sequenceFrom, List<ChainType> chainTypes, String outputFileName) {
        
        try{
            
            if(CollectionUtil.hasValues(structureSelection)){
                List<String> structVerIds = new ArrayList<>(structureSelection.size());
                for(Jstruct_FullStructure fs : structureSelection){
                    if(fs.isCurrentUserCanRead()){
                        structVerIds.add(fs.getStructVerId().toString());
                    }
                }
 
                SubQueryInput subQueryInput = new SubQueryInput(currentUser);
                subQueryInput.setSelectedSubqueryType(SubQueryType.STRUCT_VER_IDS);
                subQueryInput.setInputStructVerIds(CollectionUtils.join(structVerIds, ","));

                FullQueryInput fqi = new FullQueryInput(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.OR);
                fqi.addSubQueryInput(subQueryInput);
                
                String fastaUrl = FastaUtil.createFastaUrl(fqi, sequenceFrom, chainTypes, outputFileName);

                FacesContext ctx = FacesContext.getCurrentInstance();
                ExternalContext extContext = ctx.getExternalContext();
                extContext.redirect(fastaUrl);
                
            }

        } catch (IOException | NoSuchFieldException ex) {
            HijackError.gotoErrorPage(ex);
        }
        
    }
    

    /**
     * open all structures (permalink) that user has read access for
     */
    public void openSelectedInNewWindows() {

        if (CollectionUtil.hasValues(structureSelection)) {

            RequestContext requestContext = RequestContext.getCurrentInstance();

            for (Jstruct_FullStructure fs : structureSelection) {
                if(fs.isCurrentUserCanRead()){
                    String url = fs.getLinkPermalink();
                    String tab = "_newtab" + fs.getStructVerId().toString();
                    requestContext.execute("window.open('" + url + "','" + tab + "')");
                }
            }
            
        }
    }
    
    
    /**
     * download all selected files that user has read access for
     */
    public void downloadSelected() {

        if (CollectionUtil.hasValues(structureSelection)) {

            RequestContext requestContext = RequestContext.getCurrentInstance();

            for (Jstruct_FullStructure fs : structureSelection) {
                if(fs.isCurrentUserCanRead()){
                    String url = fs.getLinkDownload();
                    String tab = "_newtab" + fs.getStructVerId().toString();
                    requestContext.execute("window.open('" + url + "','" + tab + "')");
                }
            }
            
        }
    }
    
    
    
    
    public void selectedAsSearchCriteria(){
        
        //cleanup existing b/4 our next search
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);  
        
        FullQueryInput newFullQueryInput = new FullQueryInput(fullQueryInput.getFileStatusRcsb(), fullQueryInput.getFileStatusManual(), fullQueryInput.getQueryMode());
        
        List<String> structureIds = new ArrayList<>(structureSelection.size());
        for(Jstruct_FullStructure fs : structureSelection){
            structureIds.add(fs.getStructureId().toString());  
        }

        SubQueryInput subQueryInput = new SubQueryInput(currentUser);
        subQueryInput.setSelectedSubqueryType(SubQueryType.IDENTIFIER);
        subQueryInput.setInputIdentifier(CollectionUtils.join(structureIds, ", "));
                
        newFullQueryInput.addSubQueryInput(subQueryInput);
        FacesUtil.storeOnSession("fullQueryInput", newFullQueryInput);

        FacesUtil.redirectRequest("/structure_search.xhtml?faces-redirect=true"); 
        
    }
    
    

    
    
    
    
            
    /* #######################################################################################################################################
                    BULK PERMISSION UPDATE methods
      #######################################################################################################################################  */
    
    public void updateBulkPermissionStructures(){
        bulkPermissionAllStructures = structureSelection;
        bulkPermissionGrantableStructures = new ArrayList<>(structureSelection.size());
        if(CollectionUtil.hasValues(bulkPermissionAllStructures)){
            for(Jstruct_FullStructure structure : bulkPermissionAllStructures){
                if(structure.isCurrentUserCanGrant()){
                    bulkPermissionGrantableStructures.add(structure);
                }
            }
        } 
        activePermissionTabIndex = 0;
    }
    
    
    public void updateUserRoleSelections(String role, boolean newValue) 
    {                
        activePermissionTabIndex = 1;
        switch (role) 
        {
            case "read":
                userCanWrite = false;
                userCanGrant = false;
                break;

            case "write":
                if (newValue) {
                    userCanRead = true;
                    userCanWrite = true;
                    userCanGrant = false;
                } else {
                    userCanRead = true;
                    userCanWrite = false;
                    userCanGrant = false;
                }
                break;

            case "grant":
                if (newValue) {
                    userCanRead = true;
                    userCanWrite = true;
                    userCanGrant = true;
                } else {
                    userCanRead = true;
                    userCanWrite = true;
                    userCanGrant = false;
                }
                break;
        }
    }
    
    
    public void updateAllRoleSelections(String role, boolean newValue) 
    {                
        activePermissionTabIndex = 2;
        switch (role) 
        {
            case "read":
                allCanWrite = false;
                break;

            case "write":
                if (newValue) {
                    allCanRead = true;
                    allCanWrite = true;
                } else {
                    allCanRead = true;
                    allCanWrite = false;
                }
                break;
        }
    }
    
    
    public void updateUserPermissionsOverwrite()
    {
        try 
        {
            List<String> updatedStructures = new ArrayList<>(bulkPermissionGrantableStructures.size());
            if(CollectionUtil.hasValues(bulkPermissionGrantableStructures)){
                for(Jstruct_FullStructure structure : bulkPermissionGrantableStructures){
                    if(structure.isCurrentUserCanGrant())
                    {
                        Jstruct_StructureRole currentRole = structureRoleService.getByStructureIdUserId(structure.getStructureId(), bulkPermissionUpdateUser.getUserId());
                        
                        if(currentRole == null)
                        {
                            if(userCanRead  || userCanWrite  || userCanGrant)
                            {
                                currentRole = new Jstruct_StructureRole(structure.getStructureId(), bulkPermissionUpdateUser, userCanRead, userCanWrite, userCanGrant);
                                structureRoleService.add(currentRole);
                            }
                        }
                        else 
                        {
                            if(!userCanRead && !userCanWrite && !userCanGrant)
                            {
                                structureRoleService.delete(currentRole);
                            } 
                            else 
                            {
                                currentRole.setCanRead(userCanRead);
                                currentRole.setCanWrite(userCanWrite);
                                currentRole.setCanGrant(userCanGrant);
                                structureRoleService.update(currentRole);
                            }
                        }
                        
                        updatedStructures.add(structure.getJstructDisplayId());
                    }
                }
            } 
            bulkPermissionUpdateUser = null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                                "Permissions Updated (Overwrite)", 
                                                                                "Structures: " + StringUtil.join(updatedStructures, ", ")));
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error updating Permissions (Overwrite)", e.getMessage()));
        }
    }
    
    
    
    
    public void updateUserPermissionsCumulative()
    {
        try 
        {
            List<String> updatedStructures = new ArrayList<>(bulkPermissionGrantableStructures.size());
            if(CollectionUtil.hasValues(bulkPermissionGrantableStructures)){
                for(Jstruct_FullStructure structure : bulkPermissionGrantableStructures){
                    if(structure.isCurrentUserCanGrant())
                    {
                        Jstruct_StructureRole currentRole = structureRoleService.getByStructureIdUserId(structure.getStructureId(), bulkPermissionUpdateUser.getUserId());
                        if(currentRole != null)
                        {
                            currentRole.setCanRead(userCanRead || currentRole.isCanRead());
                            currentRole.setCanWrite(userCanWrite || currentRole.isCanWrite());
                            currentRole.setCanGrant(userCanGrant || currentRole.isCanGrant());
                            structureRoleService.update(currentRole);
                        }
                        else 
                        {
                            currentRole = new Jstruct_StructureRole(structure.getStructureId(), bulkPermissionUpdateUser, userCanRead, userCanWrite, userCanGrant);
                            structureRoleService.add(currentRole);
                        }
                        updatedStructures.add(structure.getJstructDisplayId());
                    }
                }
            } 
            bulkPermissionUpdateUser = null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                                "Permissions Updated (Cumulative)", 
                                                                                "Structures: " + StringUtil.join(updatedStructures, ", ")));
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error updating Permissions (Cumulative)", e.getMessage()));
        }
    }
    
    
    
    
    
    
    public void updateAllPermissionsOverwrite()
    {
        try 
        {
            List<String> updatedStructures = new ArrayList<>(bulkPermissionGrantableStructures.size());
            if(CollectionUtil.hasValues(bulkPermissionGrantableStructures)){
                for(Jstruct_FullStructure structure : bulkPermissionGrantableStructures){
                    if(structure.isCurrentUserCanGrant())
                    {
                        structureService.updatePermissions(structure.getStructureId(), allCanRead, allCanWrite);
                        updatedStructures.add(structure.getJstructDisplayId());
                    }
                }
            } 
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                                "General Permissions Updated (Overwrite)", 
                                                                                "Structures: " + StringUtil.join(updatedStructures, ", ")));
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error updating General Permissions (Overwrite)", e.getMessage()));
        }
    }
    
    
    
    
    public void updateAllPermissionsCumulative()
    {
        try 
        {
            List<String> updatedStructures = new ArrayList<>(bulkPermissionGrantableStructures.size());
            if(CollectionUtil.hasValues(bulkPermissionGrantableStructures)){
                for(Jstruct_FullStructure structure : bulkPermissionGrantableStructures){
                    if(structure.isCurrentUserCanGrant())
                    {
                        structureService.updatePermissions(structure.getStructureId(), 
                                                           allCanRead || structure.isCanAllRead(), 
                                                           allCanWrite || structure.isCanAllWrite());
                        updatedStructures.add(structure.getJstructDisplayId());
                    }
                }
            } 
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                                "General Permissions Updated (Cumulative)", 
                                                                                "Structures: " + StringUtil.join(updatedStructures, ", ")));
        } catch (Exception e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error updating General Permissions (Cumulative)", e.getMessage()));
        }
    }
    
    
    
    /* #######################################################################################################################################
                    BULK PERMISSION UPDATE methods
      #######################################################################################################################################  */
    
    public void updateBulkLabelStructures()
    {
        try 
        {
            allUsersLabels = userLabelService.getByUserId(currentUser.getUserId());
            
            bulkLabelAllStructures = structureSelection;
            bulkLabelReadableStructures = new ArrayList<>(structureSelection.size());
            if(CollectionUtil.hasValues(bulkLabelAllStructures)){
                for(Jstruct_FullStructure structure : bulkLabelAllStructures){
                    if(structure.isCurrentUserCanRead()){
                        bulkLabelReadableStructures.add(structure);
                    }
                }
            }
            
            newLabel = new Jstruct_UserLabel(currentUser);
            
            
        } catch (JDAOException ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error retrieving Structure Labels.", ex.getMessage()));
        }
    }
    
    
    public void createNewLabel(ActionEvent actionEvent) {
        try {
            
            if(!StringUtil.isSet(newLabel.getLabelName())){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Label Name Required.", ""));
                return;
            }
            
            Jstruct_UserLabel createdLabel = userLabelService.add(newLabel, currentUser);
            
            Jstruct_User user = sessionBean.getUser();
            newLabel = new Jstruct_UserLabel(user);
            allUsersLabels = userLabelService.getByUserId(user.getUserId());
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "New Label created", createdLabel.getLabelName()));

        } catch (JDAOException ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error adding new Label.", ex.getMessage()));
        }
    }
    
    
    
    public void updateBulkLabelsOverwrite(ActionEvent actionEvent)
    {
        try 
        {
            List<String> updatedStructures = new ArrayList<>(bulkLabelReadableStructures.size());
            if(CollectionUtil.hasValues(bulkLabelReadableStructures)){
                for(Jstruct_FullStructure structure : bulkLabelReadableStructures){
                    if(structure.isCurrentUserCanRead())
                    {
                        structureLabelService.replaceStructureLabelsForUserAndStructure(currentUser, structure.getStructureId(), selectedUserLabels);
                        updatedStructures.add(structure.getJstructDisplayId());
                    }
                }
            } 
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                                "Labels Updated (Overwrite)", 
                                                                                "Structures: " + StringUtil.join(updatedStructures, ", ")));
        } catch (JDAOException ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error updating Labels (Overwrite)", ex.getMessage()));
        }
    }
    
    
    public void updateBulkLabelsCumulative()
    {
        try 
        {
            List<String> updatedStructures = new ArrayList<>(bulkLabelReadableStructures.size());
            if(CollectionUtil.hasValues(bulkLabelReadableStructures)){
                for(Jstruct_FullStructure structure : bulkLabelReadableStructures){
                    if(structure.isCurrentUserCanRead())
                    {
                        structureLabelService.addStructureLabelsForStructure(currentUser, structure.getStructureId(), selectedUserLabels);
                        updatedStructures.add(structure.getJstructDisplayId());
                    }
                }
            } 
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                                                                "Labels Updated (Cumulative)", 
                                                                                "Structures: " + StringUtil.join(updatedStructures, ", ")));
        } catch (JDAOException ex) {
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error updating Labels (Cumulative)", ex.getMessage()));
        }
    }
    
    
    
    
    
    
    
    /* #######################################################################################################################################
                    SEARCH CRITERIA methods
      #######################################################################################################################################  */
    
    
    public void changeSubQueryType(AjaxBehaviorEvent ajaxBehaviorEvent){
        
        String rowIndexParam = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("rowIndex");
        Integer changedRowIndex = JStringUtil.stringToInteger(rowIndexParam);
        
        SubQueryInput subQueryInput = fullQueryInput.getSubQueryInputWithRowIndex(changedRowIndex);
        subQueryInput.clearErrorMessage();
        
    };
    
    
    
    
    /**
     * 
     * @param actionEvent 
     */
    public void addCriteria(ActionEvent actionEvent){
        
        clearCounts();
        
        SubQueryInput subQueryInput = new SubQueryInput(currentUser);
        fullQueryInput.addSubQueryInput(subQueryInput);
    
    }
    
    
    /**
     * 
     * @param doomedRowIndexParam 
     */
    public void deleteSubquery(String doomedRowIndexParam){
        
        clearCounts();
        
        Integer doomedRowIndex = JStringUtil.stringToInteger(doomedRowIndexParam);
        fullQueryInput.removeSubQueryInput(doomedRowIndex);
    }
    
    
    /**
     *
     * @param actionEvent
     */
    public void showCounts(ActionEvent actionEvent) {
        try {

            if (criteriaIsValid()) {

                PasSearch pasSearch = new PasSearch(fullQueryInput);
                pasSearch.executeCounts();
                overallCountAnd = pasSearch.getAndCount();
                overallCountOr = pasSearch.getOrCount();
                
                Map<Integer, Integer> subQueryCounts = pasSearch.getSubQueryCounts();
                for (Map.Entry<Integer, Integer> entry : subQueryCounts.entrySet()) {
                    
                    Integer rowIndex = entry.getKey();
                    Integer count = entry.getValue();
                    
                    SubQueryInput subQueryInput = fullQueryInput.getSubQueryInputWithRowIndex(rowIndex);
                    subQueryInput.setResultCount(count);
                    
                }
                
            }

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
    }

    private void clearCounts() {
        overallCountAnd = null;
        overallCountOr = null;
    }
    
    
    
    
    
    /**
     * @param actionEvent 
     */
    public void searchSubmit(ActionEvent actionEvent)
    {
        try
        {
            structureSelection = null;
            
            if (performSearch())
            {
                clearCounts();
                renderThe = "results";
            } 
            else
            {
                renderThe = "criteria";
            }
         
        } 
        catch (JDAOException | NoSuchFieldException e)
        {
            HijackError.gotoErrorPage(e);
        }
        
    }
 
    
    
    
    /**
     * 
     * @return 
     */
    public String resetForm() {
        try {

            structureSelection = null;

            FacesUtil.deleteFromSession("fullQueryInput");
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);

        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }

        return "/structure_search.xhtml?faces-redirect=true";
    }
 
    
    
   
    
    
    
}
