package com.just.jstruct.structureSearch;

import com.hfg.exception.ProgrammingException;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.subQuery.SubQuery;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

/**
 * PAS Search = Progressive Accumulative Structure Search
 *
 * this performs a Structure Search based on user entered criteria (see: StructureQuerySearch)
 * which may be a single query (find all where full text = abcdefg) or multiple queries (full text, date range, specific field(s), etc)
 *
 * usage: create the PasSearch using the only available constructor (passing in criteria object: StructureQuerySearch)
 * then call execute() to perform the search and return a list of matching FullStructure objects
 * or call executeCount() to get counts of all the subqueries
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PasSearch
{

    //###########################################################################
    // data members / getters
    //###########################################################################

    private final FullQueryInput fullQueryInput;

    public FullQueryInput getFullQueryInput()
    {
        return fullQueryInput;
    }



    //###########################################################################
    // CONSTRUCTOR
    //###########################################################################

    public PasSearch(FullQueryInput fullQueryInput)
    {
        this.fullQueryInput = fullQueryInput;
    }



    //###########################################################################
    // PUBLIC METHODS
    //###########################################################################
    

    /**
     * return fullStructures for the passed in criteria
     * @param currentUser
     * @return
     * @throws JDAOException
     */
    public List<Jstruct_FullStructure> execute(Jstruct_User currentUser) throws JDAOException
    {
        FullStructureService fullStructureService = new FullStructureService(currentUser);

        List<Long> structVerIds = executeForStructVerIds();
        List<Jstruct_FullStructure> fullStructures = fullStructureService.getByStructVerIds(structVerIds);

        return fullStructures;
    }
    
    
    
    /**
     * return structVerIds for the passed in criteria
     * @return
     * @throws JDAOException
     */
    public List<Long> executeForStructVerIds() throws JDAOException
    {
        List<SubQuery> subQueries = fullQueryInput.getAsSubQuerys();
        FileStatus fileStatusRcsb = fullQueryInput.getFileStatusRcsb();
        FileStatus fileStatusManual=  fullQueryInput.getFileStatusManual();
        QueryMode queryMode = fullQueryInput.getQueryMode();


        List<Long> structVerIds = new ArrayList<>();

        
        switch (queryMode)
        {
            case AND:
                
                List<Set<Long>> andStructVerIds = new ArrayList<>();

                //iterate over all the subquerys, getting structure_ver_ids for each subquery.
                for (SubQuery subQuery : subQueries)
                {
                    andStructVerIds.add(subQuery.findStructVerIds(fileStatusRcsb, fileStatusManual)); //hangs?!?!?!?!?!?!?!?!?!? todo fix
                }

                //keep only the struct-ver-ids that are in every subquery (AND)
                Set<Long> intersectStructVerIds = intersectionOfStructVerIds(andStructVerIds);
                if(!CollectionUtil.hasValues(intersectStructVerIds))
                {
                    return null;
                }
                
                structVerIds = new ArrayList<>(intersectStructVerIds);

                break;
                
            case OR:
                
                Set<Long> orStructVerIds = new HashSet<>();
                
                //iterate over all the subquerys, getting complete lists of structure_ver_ids - any of these that match will be returned (OR)
                for (SubQuery subQuery : subQueries)
                {
                    Set<Long> someStructVerIds = subQuery.findStructVerIds(fileStatusRcsb, fileStatusManual);
                    
                    if(CollectionUtil.hasValues(someStructVerIds))
                    {
                        orStructVerIds.addAll(someStructVerIds);
                    }
                }   
                structVerIds = new ArrayList<>(orStructVerIds);

                break;
                
            default:
                throw new ProgrammingException("Search Query Mode '" + queryMode + "' not valid.");
        }

        return structVerIds;
    }

    
    
    
    
    public static String AND_COUNT_KEY = "and_count_key";
    public static String OR_COUNT_KEY = "or_count_key";
    
    private Integer andCount;
    private Integer orCount;
    private Map <Integer, Integer> subQueryCounts = new HashMap<>();  //rowIndes/count of structures returned
    
    public Integer getAndCount() { return andCount;}
    public Integer getOrCount() { return orCount;}
    public Map<Integer, Integer> getSubQueryCounts() {return subQueryCounts;}

 
    /**
     * return just counts for the passed in criteria
     * @throws JDAOException
     */
    public void executeCounts() throws JDAOException
    {
        List<SubQuery> subQueries = fullQueryInput.getAsSubQuerys();
        FileStatus fileStatusRcsb = fullQueryInput.getFileStatusRcsb();
        FileStatus fileStatusManual=  fullQueryInput.getFileStatusManual();
        
        Set<Long> allStructVerIds = new HashSet<>();        //OR
        List<Set<Long>> structVerIds = new ArrayList<>();   //AND

        //iterate over all the subquerys, getting structure_ver_ids for each subquery.
        for (SubQuery subQuery : subQueries)
        {
            Set<Long> svis = subQuery.findStructVerIds(fileStatusRcsb, fileStatusManual);
            structVerIds.add(svis);

            if (CollectionUtil.hasValues(svis))
            {
                allStructVerIds.addAll(svis);
                subQueryCounts.put(subQuery.getRowIndex(), svis.size());
            } 
            else
            {
                subQueryCounts.put(subQuery.getRowIndex(), 0);
            }
        }
        orCount = allStructVerIds.size();

        //keep only the struct-ver-ids that are in every subquery (AND)
        Set intersectStructVerIds = intersectionOfStructVerIds(structVerIds);
        if(intersectStructVerIds!=null)
        {
            andCount = intersectStructVerIds.size();
        } 
        else
        {
            andCount = 0;
        }


    }




    //used when 'AND' is selected as the 'combine queries with.
    //returns a set of struct_ver_ids that are in ALL subqueries
    private Set<Long> intersectionOfStructVerIds( List<Set<Long>> allStructVerIds)
    {

        if(null==allStructVerIds || allStructVerIds.isEmpty())
        {
            return null;
        }

        if(allStructVerIds.size() == 1)
        {
            return allStructVerIds.get(0);
        }

        Set<Long> intersectStructVerIds = allStructVerIds.get(0);
        for(ListIterator<Set<Long>> iter = allStructVerIds.listIterator(1); iter.hasNext();)
        {
            Set<Long> next = iter.next();
            if(CollectionUtil.hasValues(next))
            {
                intersectStructVerIds.retainAll(next);
            }
        }

        return intersectStructVerIds;
    }


    
    







}
