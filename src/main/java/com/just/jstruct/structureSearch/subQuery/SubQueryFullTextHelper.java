package com.just.jstruct.structureSearch.subQuery;


import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.JStringUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * perform a 'simple search' based on basic user criteria.
 * note the static{} method defines the database fields to search.
 *
 * create the SimpleSearch using the only available constructor (passing in criteria stuff)
 * then call findFullStructures() to perform the search and return a list of matching FullStructure objects
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SubQueryFullTextHelper {



    //###########################################################################
    // STATIC stuff
    //###########################################################################

    //the alias for the 'struct_ver_id' field
    public static String STRUCT_VERSION_ID_ALIAS = "struct_version_id_alias";
    public static String TEMP_TABLE_ALIAS = "temp_table_alias";




    public final static List<FullTextSearchDef> FULL_TEXT_SEARCH_DEFS;

    static
    {
        //this here fancypants list contains the definition of the tables/fields our simple search will search.

        //any of the text columns to be searched should have a 'lower index' on them...
        //  example: CREATE INDEX index_name ON table(LOWER(field));

        FULL_TEXT_SEARCH_DEFS = new ArrayList<>();
 
        //STRUCT_VERSION
        List<FullTextSearchColumnDef> structVerCols = new ArrayList<>();
        structVerCols.add(new FullTextSearchColumnDef("Struct_Version.STRUCT_METHOD", "Structure Method", false));
        structVerCols.add(new FullTextSearchColumnDef("Struct_Version.TARGET", "Target", false));
        structVerCols.add(new FullTextSearchColumnDef("Struct_Version.DESCRIPTION", "Structure Description", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Struct_Version", structVerCols, "Struct_Version.STRUCT_VER_ID"));

        //STRUCT_FILE
        List<FullTextSearchColumnDef> structFileCols = new ArrayList<>();
        structFileCols.add(new FullTextSearchColumnDef("Struct_File.UPLOAD_FILE_NAME", "Upload File Name", false));
        structFileCols.add(new FullTextSearchColumnDef("Struct_File.UPLOAD_FILE_EXT", "Upload File Extension", false));
        structFileCols.add(new FullTextSearchColumnDef("Struct_File.ACTUAL_FILE_NAME", "Actual File Name", false));
        structFileCols.add(new FullTextSearchColumnDef("Struct_File.ACTUAL_FILE_EXT", "Actual File Extension", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Struct_File", structFileCols, "Struct_File.STRUCT_VER_ID"));

        //PDB_FILE
        List<FullTextSearchColumnDef> pdbFileCols = new ArrayList<>();
        pdbFileCols.add(new FullTextSearchColumnDef("Pdb_File.PDB_IDENTIFIER", "PBD Identifier", false));
        pdbFileCols.add(new FullTextSearchColumnDef("Pdb_File.CLASSIFICATION", "Classification", false));
        pdbFileCols.add(new FullTextSearchColumnDef("Pdb_File.TITLE", "Title", false));
        pdbFileCols.add(new FullTextSearchColumnDef("Pdb_File.CAVEAT", "CAVEAT (PDB Header)", false));
        pdbFileCols.add(new FullTextSearchColumnDef("Pdb_File.EXPDTA", "EXPDTA (PDB Header)", false));
        pdbFileCols.add(new FullTextSearchColumnDef("Pdb_File.NUMMDL", "NUMMDL (PDB Header) - number", true));
        pdbFileCols.add(new FullTextSearchColumnDef("Pdb_File.MDLTYP", "MDLTYP (PDB Header)", false));
        pdbFileCols.add(new FullTextSearchColumnDef("Pdb_File.RESOLUTION_MEASURE", "Resolution (PDB Header, REMARKS 2) - number", true));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Pdb_File", pdbFileCols, "Pdb_File.STRUCT_VER_ID"));

        //CHAIN
        List<FullTextSearchColumnDef> chainCols = new ArrayList<>();
        chainCols.add(new FullTextSearchColumnDef("Chain.SEQ_THEORETICAL", "Theortical Sequence (SEQRES - regardless of coordinate properties)", false));
        chainCols.add(new FullTextSearchColumnDef("Chain.NAME", "Sequence Name", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Chain", chainCols, "Chain.STRUCT_VER_ID"));

        //PDB_COMPND
        List<FullTextSearchColumnDef> pdbCompndCols = new ArrayList<>();
        pdbCompndCols.add(new FullTextSearchColumnDef("Pdb_Compnd.VALUE", "COMPND (PDB data)", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Pdb_Compnd", pdbCompndCols, "Pdb_Compnd.STRUCT_VER_ID"));

        //PDB_SOURCE
        List<FullTextSearchColumnDef> pdbSourceCols = new ArrayList<>();
        pdbSourceCols.add(new FullTextSearchColumnDef("Pdb_Source.VALUE", "SOURCE (PDB data)", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Pdb_Source", pdbSourceCols, "Pdb_Source.STRUCT_VER_ID"));

        //PDB_KEYWRD
        List<FullTextSearchColumnDef> pdbKeywrdCols = new ArrayList<>();
        pdbKeywrdCols.add(new FullTextSearchColumnDef("Pdb_Keyword.VALUE", "KEYWRD (PDB data)", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Pdb_Keyword", pdbKeywrdCols, "Pdb_Keyword.STRUCT_VER_ID"));

        //PDB_AUTHOR
        List<FullTextSearchColumnDef> pdbAuthorCols = new ArrayList<>();
        pdbAuthorCols.add(new FullTextSearchColumnDef("Pdb_Author.VALUE", "SOURCE (PDB data)", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Pdb_Author", pdbAuthorCols, "Pdb_Author.STRUCT_VER_ID"));

        //PDB_AUTHOR
        List<FullTextSearchColumnDef> pdbJrnlCols = new ArrayList<>();
        pdbJrnlCols.add(new FullTextSearchColumnDef("Pdb_Jrnl.AUTH", "JRNL - Author (PDB data)", false));
        pdbJrnlCols.add(new FullTextSearchColumnDef("Pdb_Jrnl.TITL", "JRNL - Title (PDB data)", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Pdb_Jrnl", pdbJrnlCols, "Pdb_Jrnl.STRUCT_VER_ID"));

        //CLIENT - multiple tables
        List<FullTextSearchColumnDef> pdbClientCols = new ArrayList<>();
        pdbClientCols.add(new FullTextSearchColumnDef("Client.NAME", "Client Name", false));
        //pdbClientCols.add(new FullTextSearchColumnDef(Jstruct_Client.INSERT_USER_ID, "INTEGER TEST"));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Client", pdbClientCols, "Struct_Version.STRUCT_VER_ID", "Struct_Version", "Client.CLIENT_ID", "Struct_Version.CLIENT_ID"));

        //PROGRAM - multiple tables
        List<FullTextSearchColumnDef> pdbProgramCols = new ArrayList<>();
        pdbProgramCols.add(new FullTextSearchColumnDef("Program.NAME", "Program Name", false));
        FULL_TEXT_SEARCH_DEFS.add(new FullTextSearchDef("Program", pdbProgramCols, "Struct_Version.STRUCT_VER_ID", "Struct_Version", "Program.PROGRAM_ID", "Struct_Version.PROGRAM_ID"));


    }


    //###########################################################################
    // PRIVATE MEMBERS
    //###########################################################################

    private final String mCriteria;
    private final String mLowerCriteria;
    private final String mUpperCriteria;

    private final FileStatus mFileStatusRcsb;
    private final FileStatus mFileStatusManual;

    private final Jstruct_User currentUser;
   

    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    /**
     *
     * @param criteria
     * @param fileStatusRcsb
     * @param fileStatusManual
     */
    public SubQueryFullTextHelper(String criteria, FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        //remove any carriage return characters from the user's input (probably none since their input is a text field, not textarea field)
        String fixedCriteria = criteria.replaceAll("[\\r\\n]", " ");

        this.mCriteria = fixedCriteria;
        this.mLowerCriteria = fixedCriteria.toLowerCase();
        this.mUpperCriteria = fixedCriteria.toUpperCase();

        this.mFileStatusRcsb = fileStatusRcsb;
        this.mFileStatusManual = fileStatusManual;
        
        this.currentUser = FacesUtil.determineCurrentUser(null); 

    }





    //###########################################################################
    // GETTERS / SETTERS
    //###########################################################################

    public String getCriteria() {
        return mCriteria;
    }

    public String getLowerCriteria() {
        return mLowerCriteria;
    }

    public String getUpperCriteria() {
        return mUpperCriteria;
    }

    public FileStatus getFileStatusRcsb() {
        return mFileStatusRcsb;
    }

    public FileStatus getFileStatusManual() {
        return mFileStatusManual;
    }


    //###########################################################################
    // PUBLIC METHODS
    //###########################################################################

    /**
     * based on criteria (entered in constructor) return all FullStructure objects that match
     * @return
     * @throws JDAOException
     */
    public List<Jstruct_FullStructure> findFullStructures() throws JDAOException {

        //create a set (unique) of struct-file-ids that have matching values
        List<String> subSqls = new ArrayList<>(FULL_TEXT_SEARCH_DEFS.size());
        StringBuilder sb = new StringBuilder();

        // for each thing we're searching, create a portion of the query.
        // these will be 'unioned' together into a single statement (instead of multiple statements with results pulled together in code)
        for (FullTextSearchDef ssd : FULL_TEXT_SEARCH_DEFS) {

            sb.append(generateSelectClause(ssd));
            sb.append(generateFromCause(ssd));
            sb.append(generateWhereCause(ssd));

            subSqls.add(sb.toString());
            sb.setLength(0);
        }
        String sql = StringUtil.join(subSqls, " UNION \n");


        List<Long> structVerIds = PostgresDao.getStructVerIds(sql);
        Set<Long> matchingStructVerIds = new HashSet<>();
        for(Long id : structVerIds){
            matchingStructVerIds.add(id);
        }

        List<Jstruct_FullStructure> fullStructures = PostgresDao.getFullStructuresByStructVerIds(currentUser, matchingStructVerIds, mFileStatusRcsb, mFileStatusManual, true);

        //execute statement and get all the struct version ids
        //PreparedStatement ps = inConn.prepareStatement(sql.toString());
        //ResultSet rs = ps.executeQuery();
        //Set<Long> matchingStructVerIds = new HashSet<>();
        //while (rs.next()){
        //    matchingStructVerIds.add(rs.getLong(STRUCT_VERSION_ID_ALIAS));
        //}

        //use the ids list to get all FullStructure objects that match...
        //fullStructures = Jstruct_FullStructure.getRowsByStructVerIds(inConn, new ArrayList<>(matchingStructVerIds),
        //                                                             mFileStatusRcsb, mFileStatusManual,
        //                                                             true);

        return fullStructures;
    }



    public Set<Long> findStructVerIds() throws JDAOException {

        //first get all fullStructures that match
        List<Jstruct_FullStructure> fullStructures = findFullStructures();

        //then pull out just the struct ver ids
        if(CollectionUtil.hasValues(fullStructures)){
            Set<Long> structVerIds = new HashSet<>(fullStructures.size());
            for(Jstruct_FullStructure fullStructure : fullStructures){
                structVerIds.add(fullStructure.getStructVerId());
            }
            return structVerIds;
        }
        return null;
    }





    private String generateSelectClause(FullTextSearchDef ssd){
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");

        if(ssd.isIsSingleTable()){
            sql.append(ssd.getStructVerIdField());
            sql.append(" AS " );
        }
        sql.append(STRUCT_VERSION_ID_ALIAS);

        sql.append(" \n");
        return sql.toString();
    }


    private String generateFromCause(FullTextSearchDef ssd){
        StringBuilder sql = new StringBuilder();
        sql.append("FROM ");
        sql.append(ssd.getTableToSearch());
        if (!ssd.isIsSingleTable()) {
            sql.append(" AS ");
            sql.append(TEMP_TABLE_ALIAS);
        }
        sql.append(" \n");
        return sql.toString();
    }


    private String generateWhereCause(FullTextSearchDef ssd) throws JDAOException {

        StringBuilder sql = new StringBuilder();
        sql.append(" WHERE " );

        List<String> wheres = new ArrayList<>();


        for (FullTextSearchColumnDef col : ssd.getFieldsToSearch()) {

            String searchColumn = col.getSearchCol();
            if(!ssd.isIsSingleTable())
            {
                String fieldNameWithoutTable = JStringUtil.onlyTextAfterLastOccurance(col.getSearchCol(), ".");
                searchColumn = TEMP_TABLE_ALIAS + "." + fieldNameWithoutTable;
            }


            if(col.isNumeric()){
                if(JStringUtil.isNumeric(mCriteria)){
                    wheres.add(searchColumn + " = " + mCriteria + " ");
                }
            }

            else 
            {
                //note, any text column referenced here should have a lowerindex on them (except seq_theoretical in CHAIN since it's a text field)
                if (col.getSearchCol().equalsIgnoreCase("Chain.SEQ_THEORETICAL")){
                    wheres.add(" " + searchColumn + " ILIKE '%" + mCriteria + "%' ");
                } else {
                    wheres.add(" lower(" + searchColumn + ") LIKE '%" + mLowerCriteria + "%' ");
                }

            }


        }

        sql.append(StringUtil.join(wheres, " \n OR "));
        sql.append(" \n");

        return sql.toString();

    }



    


}
