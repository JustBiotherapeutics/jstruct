package com.just.jstruct.structureSearch.subQuery;


import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import java.util.*;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SubQueryJrnl implements SubQuery {


    //param/criteria key(s) specific to JRNL
    
    private static final SubQueryType SUB_QUERY_TYPE = SubQueryType.JRNL;

    private final String inputCriteria;
    
    private final Integer rowIndex;
    private final SubQueryInput subQueryInput;
    
    
    
    
    


    /**
     * 
     * @param subQueryInput 
     */
    public SubQueryJrnl(SubQueryInput subQueryInput) {
        
        this.inputCriteria = subQueryInput.getInputJrnl();
        
        this.rowIndex = subQueryInput.getRowIndex();
        this.subQueryInput = subQueryInput;
        
    }

    


    




    // *************************************************************************************
    // overrides from SubQuery interface
    // *************************************************************************************

    @Override
    public SubQueryType getSubQueryType() {
        return SUB_QUERY_TYPE;
    }
    
    
    @Override
    public String getSubQueryKey(){
        return SUB_QUERY_TYPE.getValue();
    }

    @Override
    public int getRowIndex() {
        return rowIndex;
    }
    
    @Override
    public SubQueryInput getSubQueryInput(){
        return subQueryInput;
    }
    
    
    
    
    @Override
    public boolean validateInput() {
        subQueryInput.clearErrorMessage();
        if(!StringUtil.isSet(inputCriteria)){
            subQueryInput.setErrorMessage("Input is required");  
            return false;
        }
        return true;
    }
    
    
    


    @Override
    public List<Jstruct_FullStructure> findFullStructures(Jstruct_User currentUser, FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);

        if(CollectionUtil.hasValues(structVerIds)){
            FullStructureService fullStructureService = new FullStructureService(currentUser);
            fullStructureService.getByStructVerIds(new ArrayList<>(structVerIds));
        }
        return null;
    }



    @Override
    public int findFullStructureCount(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {
        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);
        if(CollectionUtil.hasValues(structVerIds)){
            return structVerIds.size();
        } 
        return 0;
    }



    @Override
    public Set<Long> findStructVerIds(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        String sql = selectSql(fileStatusRcsb, fileStatusManual);
        List<Long> structVerIds = PostgresDao.getStructVerIds(sql);
        
        if(CollectionUtil.hasValues(structVerIds)){
            return new HashSet<>(structVerIds);
        }
        
        return null;
    

    }




    /**
     * create the SELECT statement that will search within full_structure_view for all structure ids
     * @param fileStatusRcsb
     * @param fileStatusManual
     * @return
     */
    private String selectSql(FileStatus fileStatusRcsb, FileStatus fileStatusManual){

        StringBuilderPlus sql = new StringBuilderPlus();

        sql.appendln(" SELECT j.struct_ver_id "); 
        sql.appendln("   FROM pdb_jrnl j  ");
        
        sql.appendln("     LEFT JOIN full_structure_view fs ");
        sql.appendln("       ON j.struct_ver_id = fs.struct_ver_id ");

        sql.append("  WHERE (    j.auth  ILIKE '%").append(inputCriteria).appendln("%' ");
        sql.append("          OR j.titl  ILIKE '%").append(inputCriteria).appendln("%' ");
        sql.append("          OR j.edit  ILIKE '%").append(inputCriteria).appendln("%' ");
        sql.append("          OR j.ref   ILIKE '%").append(inputCriteria).appendln("%' ");
        sql.append("          OR j.publ  ILIKE '%").append(inputCriteria).appendln("%' ");
        sql.append("          OR j.refn  ILIKE '%").append(inputCriteria).appendln("%' ");
        sql.append("          OR j.pmid  ILIKE '%").append(inputCriteria).appendln("%' ");
        sql.append("          OR j.doi   ILIKE '%").append(inputCriteria).appendln("%' ");
        sql.appendln("      ) ");
        
        sql.appendln("    AND ").appendln(FileStatus.createFileStatusClause(fileStatusRcsb, fileStatusManual, true, false));

        

        
        
        return sql.toString();

    }

    




}
