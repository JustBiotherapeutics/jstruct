package com.just.jstruct.structureSearch.subQuery;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import java.util.List;
import java.util.Set;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public interface SubQuery {

    public SubQueryType getSubQueryType();
    public String getSubQueryKey();

    public int getRowIndex();
    
    public boolean validateInput();
    
    public List<Jstruct_FullStructure> findFullStructures(Jstruct_User currentUser, FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException;
    public int findFullStructureCount(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException;
    public Set<Long> findStructVerIds(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException;
    
    public SubQueryInput getSubQueryInput();

}
