package com.just.jstruct.structureSearch.subQuery;


import com.hfg.exception.ProgrammingException;
import com.hfg.sql.SQLQuery;
import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.AllAnyOption;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import java.util.*;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SubQueryLabel implements SubQuery {


    //param/criteria key(s) specific to LABEL
    private static final SubQueryType SUB_QUERY_TYPE = SubQueryType.LABEL;
    
    private final AllAnyOption selectAllAnyOption;
    private final List<Long> selectedLabelIds;

    private final Integer rowIndex;
    private final SubQueryInput subQueryInput;
    
    
    


    /**
     * 
     * @param subQueryInput 
     */
    public SubQueryLabel(SubQueryInput subQueryInput) {
        
        this.selectAllAnyOption = subQueryInput.getAllAnyLabelOption();
        this.selectedLabelIds = subQueryInput.getSelectedLabelIds();
        
        this.rowIndex = subQueryInput.getRowIndex();
        this.subQueryInput = subQueryInput;
    }

    


    




    // *************************************************************************************
    // overrides from SubQuery interface
    // *************************************************************************************

    @Override
    public SubQueryType getSubQueryType() {
        return SUB_QUERY_TYPE;
    }
    
    
    @Override
    public String getSubQueryKey(){
        String key = SUB_QUERY_TYPE.getValue();
        //System.out.println("String key = SUB_QUERY_TYPE.getValue(); '" + key + "'");
        return key;
    }

    @Override
    public int getRowIndex() {
        return rowIndex;
    }
    
    @Override
    public SubQueryInput getSubQueryInput(){
        return subQueryInput;
    }
    
    
    
    
    @Override
    public boolean validateInput() {
        subQueryInput.clearErrorMessage();
        if(!CollectionUtil.hasValues(selectedLabelIds)){
            subQueryInput.setErrorMessage("Please select at least one label.");  
            return false;
        }
        return true;
    }
    
    
    


    @Override
    public List<Jstruct_FullStructure> findFullStructures(Jstruct_User currentUser, FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);

        if(CollectionUtil.hasValues(structVerIds)){
            FullStructureService fullStructureService = new FullStructureService(currentUser);
            fullStructureService.getByStructVerIds(new ArrayList<>(structVerIds));
        }
        return null;
    }



    @Override
    public int findFullStructureCount(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {
        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);
        if(CollectionUtil.hasValues(structVerIds)){
            return structVerIds.size();
        } 
        return 0;
    }
    
    


    @Override
    public Set<Long> findStructVerIds(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        String sql = null;
        List<Long> structVerIds = null;
        
        switch (selectAllAnyOption) {
            case OPTION_HAS_ANY_OF:
                sql = selectSqlAnyOf(fileStatusRcsb, fileStatusManual);
                break;
            case OPTION_HAS_ALL_OF:
                sql = selectSqlAllOf(fileStatusRcsb, fileStatusManual);
                break;
            default:
                throw new ProgrammingException("Invalid enum value for AllAnyOption in SubQueryLabel.java ");
        }
        
        if(!StringUtil.isSet(sql)){
            return null;
        }
        
        structVerIds = PostgresDao.getStructVerIds(sql);
        
        if(CollectionUtil.hasValues(structVerIds)){
            return new HashSet<>(structVerIds);
        }
        
        
        return new HashSet<>(structVerIds);
    
    }




    /**
     * create the SELECT statement that will search for struct_ver_ids that are related to ANY of the selected labels
     * @param fileStatusRcsb
     * @param fileStatusManual
     * @return
     */
    private String selectSqlAnyOf(FileStatus fileStatusRcsb, FileStatus fileStatusManual) {
        
        if(!CollectionUtil.hasValues(selectedLabelIds)){
            return null;
        }
        
        String labelIdInPortion = StringUtil.join(selectedLabelIds, ", ");

        
        StringBuilderPlus sql = new StringBuilderPlus();

        sql.append(" SELECT fs.struct_ver_id "); 
        sql.append("   FROM full_structure_view fs ");
        sql.append("     LEFT JOIN user_structure_label_view usl ");
        sql.append("            ON fs.structure_id = usl.structure_id ");
        sql.append("  WHERE usl.user_label_id in (").append(labelIdInPortion).append(")");
        sql.append("    AND ");
        sql.appendln(FileStatus.createFileStatusClause(fileStatusRcsb, fileStatusManual, true, false));

        return sql.toString();

    }


    /**
     * create the SELECT statement that will search for struct_ver_ids that are related to ALL of the selected labels
     * @param fileStatusRcsb
     * @param fileStatusManual
     * @return
     */
    private String selectSqlAllOf(FileStatus fileStatusRcsb, FileStatus fileStatusManual) {
        
        if(!CollectionUtil.hasValues(selectedLabelIds)){
            return null;
        }
        
        String viewAlias = "fs";        //same alias as in createFileStatusClause(..)
        String tableAlias = "struct_lbl";
        
        
        
        SQLQuery query = new SQLQuery();

        query.addSelect("struct_ver_id");
        query.addFrom("full_structure_view", viewAlias);

        
        
        for(Long labelId : selectedLabelIds){

            StringBuilder from = new StringBuilder();
            from.append("(SELECT structure_id ");
            from.append("   FROM structure_label ");
            from.append("  WHERE user_label_id = ").append(labelId).append(")");
            query.addFrom(from.toString(), tableAlias + labelId);

            StringBuilder where = new StringBuilder();
            where.append(viewAlias).append(".structure_id = ");
            where.append(tableAlias).append(labelId).append(".structure_id");
            query.addWhereClause(where.toString());

        }

        StringBuilder sql = new StringBuilder(query.toSQL());
        
        sql.append("    AND ");
        sql.append(FileStatus.createFileStatusClause(fileStatusRcsb, fileStatusManual, true, false));

        return sql.toString();

    }

    
    
    




}
