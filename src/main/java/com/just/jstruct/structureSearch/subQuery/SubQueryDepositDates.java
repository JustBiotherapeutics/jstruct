package com.just.jstruct.structureSearch.subQuery;


import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.utilities.JDateUtil;
import java.util.*;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SubQueryDepositDates implements SubQuery {


    //param/criteria key(s) specific to DEPOSIT_DATES
    
    private static final SubQueryType SUB_QUERY_TYPE = SubQueryType.DEPOSIT_DATES;
    
    private final Date fromDate;
    private final Date toDate;

    private final Integer rowIndex;
    private final SubQueryInput subQueryInput;
    
    
    


    /**
     * 
     * @param subQueryInput 
     */
    public SubQueryDepositDates(SubQueryInput subQueryInput) {
        this.fromDate = subQueryInput.getDepositDateFrom();
        this.toDate = subQueryInput.getDepositDateTo();
        
        this.rowIndex = subQueryInput.getRowIndex();
        this.subQueryInput = subQueryInput;
    }

    


    




    // *************************************************************************************
    // overrides from SubQuery interface
    // *************************************************************************************

    @Override
    public SubQueryType getSubQueryType() {
        return SUB_QUERY_TYPE;
    }
    
    
    @Override
    public String getSubQueryKey(){
        return SUB_QUERY_TYPE.getValue();
    }

    @Override
    public int getRowIndex() {
        return rowIndex;
    }
    
    @Override
    public SubQueryInput getSubQueryInput(){
        return subQueryInput;
    }
    
    
    
    @Override
    public boolean validateInput() {
        subQueryInput.clearErrorMessage();
        
        if (fromDate == null && toDate == null) {
            subQueryInput.setErrorMessage("One or both dates must be filled out");  //todo validate from is before to
            return false;
        }
        
        return true;
    }
    
    
    


    @Override
    public List<Jstruct_FullStructure> findFullStructures(Jstruct_User currentUser, FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);

        if(CollectionUtil.hasValues(structVerIds)){
            FullStructureService fullStructureService = new FullStructureService(currentUser);
            fullStructureService.getByStructVerIds(new ArrayList<>(structVerIds));
        }
        return null;
    }



    @Override
    public int findFullStructureCount(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {
        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);
        if(CollectionUtil.hasValues(structVerIds)){
            return structVerIds.size();
        } 
        return 0;
    }



    @Override
    public Set<Long> findStructVerIds(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        String sql = selectSql(fileStatusRcsb, fileStatusManual);
        List<Long> structVerIds = PostgresDao.getStructVerIds(sql);
        
        if(CollectionUtil.hasValues(structVerIds)){
            return new HashSet<>(structVerIds);
        }
        
        return null;
    

    }




    /**
     * create the SELECT statement that will search within full_structure_view for all structure ids
     * @param fileStatusRcsb
     * @param fileStatusManual
     * @return
     */
    private String selectSql(FileStatus fileStatusRcsb, FileStatus fileStatusManual){

        StringBuilderPlus sql = new StringBuilderPlus();

        //change dates to postgres format (default format) yyyy-MM-dd
        String fromDateParam = JDateUtil.formatDate(fromDate, "yyyy-MM-dd");
         String toDateParam = JDateUtil.formatDate(toDate, "yyyy-MM-dd");

        sql.appendln(" SELECT fs.struct_ver_id ");
        sql.appendln("   FROM full_structure_view fs  ");

        sql.append("  WHERE  ");
        sql.appendln(FileStatus.createFileStatusClause(fileStatusRcsb, fileStatusManual, true, false));

        if (StringUtil.isSet(fromDateParam)) {
            sql.append("    AND  fs.pdb_deposition_date >= '").append(fromDateParam).appendln("' ");
        }
        if (StringUtil.isSet(toDateParam)) {
            sql.append("    AND  fs.pdb_deposition_date <= '").append(toDateParam).appendln(" 23:59:59' ");
        }

        return sql.toString();

    }

    
   




}
