package com.just.jstruct.structureSearch.subQuery;


import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.EnhancedText;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.SequenceOption;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import java.util.*;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SubQuerySequence implements SubQuery {


    //param/criteria key(s) specific to SEQUENCE
    
    private static final SubQueryType SUB_QUERY_TYPE = SubQueryType.SEQUENCE;
    
    private final SequenceOption selectedSequenceOption;
    private final EnhancedText selectedSequenceEnhancedText;
    private final String inputSequence;
    
    private final Integer rowIndex;
    private final SubQueryInput subQueryInput;
    
    


    /**
     * 
     * @param subQueryInput 
     */
    public SubQuerySequence(SubQueryInput subQueryInput) {
        this.selectedSequenceOption = subQueryInput.getSequenceOption();
        this.selectedSequenceEnhancedText = subQueryInput.getSequenceEnhancedText();
        this.inputSequence = subQueryInput.getInputSequence();
        
        this.rowIndex = subQueryInput.getRowIndex();
        this.subQueryInput = subQueryInput;
    }

    


    




    // *************************************************************************************
    // overrides from SubQuery interface
    // *************************************************************************************

    @Override
    public SubQueryType getSubQueryType() {
        return SUB_QUERY_TYPE;
    }
    
    
    @Override
    public String getSubQueryKey(){
        return SUB_QUERY_TYPE.getValue();
    }

    @Override
    public int getRowIndex() {
        return rowIndex;
    }
    
    @Override
    public SubQueryInput getSubQueryInput(){
        return subQueryInput;
    }
    
    
    
    
    @Override
    public boolean validateInput() {
        subQueryInput.clearErrorMessage();
        if(!StringUtil.isSet(inputSequence)){
            subQueryInput.setErrorMessage("Input is required");
            return false;
        }
        return true;
    }
    
    
    


    @Override
    public List<Jstruct_FullStructure> findFullStructures(Jstruct_User currentUser, FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);

        if(CollectionUtil.hasValues(structVerIds)){
            FullStructureService fullStructureService = new FullStructureService(currentUser);
            fullStructureService.getByStructVerIds(new ArrayList<>(structVerIds));
        }
        return null;
    }
    
    
    @Override
    public int findFullStructureCount(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {
        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);
        if(CollectionUtil.hasValues(structVerIds)){
            return structVerIds.size();
        } 
        return 0;
    }
    




    @Override
    public Set<Long> findStructVerIds(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        String sql = selectSql(fileStatusRcsb, fileStatusManual);
        List<Long> structVerIds = PostgresDao.getStructVerIds(sql);
        
        if(CollectionUtil.hasValues(structVerIds)){
            return new HashSet<>(structVerIds);
        }
        
        return null;
    

    }




    /**
     * create the SELECT statement that will search within for all struct_ver_ids that match the inputed sequence and selections
     * @param fileStatusRcsb
     * @param fileStatusManual
     * @return
     */
    private String selectSql(FileStatus fileStatusRcsb, FileStatus fileStatusManual){
        
        StringBuilderPlus sql = new StringBuilderPlus();
        
        
        if(selectedSequenceOption.equals(SequenceOption.OPTION_SEQRES)){
            //if searching SEQRES, search the CHAIN table
            
            sql.appendln(" SELECT c.struct_ver_id "); 
            sql.appendln("   FROM chain c  ");
        
            sql.appendln("     LEFT JOIN full_structure_view fs ");
            sql.appendln("       ON c.struct_ver_id = fs.struct_ver_id ");
            
            sql.append("  WHERE ");
            sql.appendln(EnhancedText.appendLikeNotLike(selectedSequenceEnhancedText, "c.seq_theoretical", inputSequence, false)); 

            sql.appendln("    AND ").appendln(FileStatus.createFileStatusClause(fileStatusRcsb, fileStatusManual, true, false));

        }
        else 
        {
            //if searching any atomic coordinates, search the ATOM_GROUPING table
            
            sql.appendln(" SELECT c.struct_ver_id "); 
            sql.appendln("   FROM chain c  ");
        
            sql.appendln("     LEFT JOIN full_structure_view fs ");
            sql.appendln("       ON c.struct_ver_id = fs.struct_ver_id ");
        
            sql.appendln("     LEFT JOIN atom_grouping ag ");
            sql.appendln("       ON c.chain_id = ag.chain_id ");
            
            sql.append("  WHERE ");
            sql.appendln(EnhancedText.appendLikeNotLike(selectedSequenceEnhancedText, selectedSequenceOption.getAtomGroupingFieldName(), inputSequence, false)); 

            sql.appendln("    AND ").appendln(FileStatus.createFileStatusClause(fileStatusRcsb, fileStatusManual, true, false));
            
            
        }
        
        
        return sql.toString();
        
    }

    

    




}
