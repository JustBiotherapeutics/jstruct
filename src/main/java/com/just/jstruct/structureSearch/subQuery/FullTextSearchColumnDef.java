package com.just.jstruct.structureSearch.subQuery;


/**
 * definition of a column that will be searched using the FullText functionality
 *   see: SubQueryFullText and SubQueryFullTextHelper
 *
 * a list of these 'goes into' the FullTextSearchDef object to allow for searching multiple columns of a table
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class FullTextSearchColumnDef {



    private String mSearchCol;
    private String mFriendlyName;
    private boolean mNumeric;
    


    /**
     *
     * @param searchCol database column to search
     * @param friendlyName a friendly name for use when displaying info to user
     */
    public FullTextSearchColumnDef(String searchCol, String friendlyName, boolean numeric) {
        this.mSearchCol = searchCol;
        this.mFriendlyName = friendlyName;
        this.mNumeric = numeric;
    }


    public String getSearchCol() { return mSearchCol; }
    public void setSearchCol(String inSearchCol) { this.mSearchCol = inSearchCol; }

    public String getFriendlyName() { return mFriendlyName; }
    public void setFriendlyName(String inFriendlyName) { this.mFriendlyName = inFriendlyName; }

    public boolean isNumeric() { return mNumeric;}
    public void setNumeric(boolean numeric) { this.mNumeric = numeric;}





}
