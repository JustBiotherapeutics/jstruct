package com.just.jstruct.structureSearch.subQuery;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.utilities.FacesUtil;
import java.util.*;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SubQueryFullText implements SubQuery {


    //param/criteria key(s) specific to FULL_TEXT
    private static final SubQueryType SUB_QUERY_TYPE = SubQueryType.FULL_TEXT;

    
    
    //data members
    private final Integer rowIndex;
    
    private final String inputText;
    
    private final SubQueryInput subQueryInput;
    
    
   


    /**
     * 
     * @param subQueryInput 
     */
    public SubQueryFullText(SubQueryInput subQueryInput) {
        this.rowIndex = subQueryInput.getRowIndex();

        this.inputText = subQueryInput.getInputFullText();
        this.subQueryInput = subQueryInput;
    }

    


    




    // *************************************************************************************
    // overrides from SubQuery interface
    // *************************************************************************************

    @Override
    public SubQueryType getSubQueryType() {
        return SUB_QUERY_TYPE;
    }
    
    
    @Override
    public String getSubQueryKey(){
        return SUB_QUERY_TYPE.getValue();
    }

    @Override
    public int getRowIndex() {
        return rowIndex;
    }
    
    @Override
    public SubQueryInput getSubQueryInput(){
        return subQueryInput;
    }
    
    

   
    
    @Override
    public boolean validateInput() {
        subQueryInput.clearErrorMessage();
        if(!StringUtil.isSet(inputText)){
            subQueryInput.setErrorMessage("Input is required");
            return false;
        }
        return true;
    }

    
    
    
    
    
    
    
    


    @Override
    public List<Jstruct_FullStructure> findFullStructures(Jstruct_User currentUser, FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        // use helper class to get the list of fullStructures that match
        // (note: this 'full text' search is complex enough to demand some additional classes to assist with logic and search definition)
        SubQueryFullTextHelper helper = new SubQueryFullTextHelper(inputText, fileStatusRcsb, fileStatusManual);
        List<Jstruct_FullStructure> fullStructures = helper.findFullStructures();

        return fullStructures;
    }



    @Override
    public int findFullStructureCount(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {
        
        Jstruct_User currentUser = FacesUtil.determineCurrentUser(null);
 
        List<Jstruct_FullStructure> fullStructures = findFullStructures(currentUser, fileStatusRcsb, fileStatusManual);
        if(CollectionUtil.hasValues(fullStructures)){
            return fullStructures.size();
        } 
        return 0;
    }
    


    @Override
    public Set<Long> findStructVerIds(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        SubQueryFullTextHelper helper = new SubQueryFullTextHelper(inputText, fileStatusRcsb, fileStatusManual);
        return helper.findStructVerIds();

    }




   
    




}
