package com.just.jstruct.structureSearch.subQuery;


import com.hfg.util.StringBuilderPlus;
import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.FullStructureService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.utilities.JStringUtil;
import com.just.jstruct.utilities.StructureUtil;
import java.util.*;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SubQueryStructVerIds implements SubQuery {


    //param/criteria key(s) specific to STRUCT_VER_IDS
    private static final SubQueryType SUB_QUERY_TYPE = SubQueryType.STRUCT_VER_IDS;
    
    private final String inputCriteria;

    private final Integer rowIndex;
    private final SubQueryInput subQueryInput;
    
    
    
    


    /**
     * 
     * @param subQueryInput 
     */
    public SubQueryStructVerIds(SubQueryInput subQueryInput) {
        this.inputCriteria = subQueryInput.getInputStructVerIds();
        
        this.rowIndex = subQueryInput.getRowIndex();
        this.subQueryInput = subQueryInput;
    }

    


    




    // *************************************************************************************
    // overrides from SubQuery interface
    // *************************************************************************************

    @Override
    public SubQueryType getSubQueryType() {
        return SUB_QUERY_TYPE;
    }
    
    
    @Override
    public String getSubQueryKey(){
        String key = SUB_QUERY_TYPE.getValue();
        return key;
    }

    @Override
    public int getRowIndex() {
        return rowIndex;
    }
    
    @Override
    public SubQueryInput getSubQueryInput(){
        return subQueryInput;
    }
    
    
    @Override
    public boolean validateInput() {
        subQueryInput.clearErrorMessage();
        if (!StringUtil.isSet(inputCriteria)) {
            subQueryInput.setErrorMessage("Input is required");  //todo validate comma/space seperated values; validate not empty; must all evaluate to numers
            return false;
        }
        return true;
    }
    
    
    


    @Override
    public List<Jstruct_FullStructure> findFullStructures(Jstruct_User currentUser, FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);

        if(CollectionUtil.hasValues(structVerIds)){
            FullStructureService fullStructureService = new FullStructureService(currentUser);
            fullStructureService.getByStructVerIds(new ArrayList<>(structVerIds));
        }
        return null;
    }



    @Override
    public int findFullStructureCount(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {
        Set<Long> structVerIds = findStructVerIds(fileStatusRcsb, fileStatusManual);
        if(CollectionUtil.hasValues(structVerIds)){
            return structVerIds.size();
        } 
        return 0;
    }



    @Override
    public Set<Long> findStructVerIds(FileStatus fileStatusRcsb, FileStatus fileStatusManual) throws JDAOException {

        String sql = selectSql(fileStatusRcsb, fileStatusManual);
        List<Long> structVerIds = PostgresDao.getStructVerIds(sql);
        
        if(CollectionUtil.hasValues(structVerIds)){
            return new HashSet<>(structVerIds);
        }
        
        return null;

    }




    /**
     * create the SELECT statement that will search for JStruct/RCSB identifiers in full_structure_view
     * @param fileStatusRcsb
     * @param fileStatusManual
     * @return
     */
    private String selectSql(FileStatus fileStatusRcsb, FileStatus fileStatusManual){

        //get the entered text, transform it into a list of numbers (structure_id) and a list of strings (PDB ids)
        List<Long> jstructIds = criteriaToJstructId();
        List<String> rcsbIds = criteriaToRcsbIds();

        boolean hasJstructIds = CollectionUtil.hasValues(jstructIds);
        boolean hasRcsbIds = CollectionUtil.hasValues(rcsbIds);

        if(!hasJstructIds && !hasRcsbIds){
            return null;
        }
        
        StringBuilderPlus sql = new StringBuilderPlus();

        sql.append(" SELECT fs.struct_ver_id "); 
        sql.append("   FROM full_structure_view fs ");

        sql.append("  WHERE struct_ver_id IN (").append(StringUtil.join(jstructIds, ", ")).appendln(") ");
       
        sql.append("    AND ");
        sql.appendln(FileStatus.createFileStatusClause(fileStatusRcsb, fileStatusManual, true, false));

        return sql.toString();

    }

    
    
    
    private List<Long> criteriaToJstructId(){

        String textCriteria = inputCriteria;
        if(!StringUtil.isSet(textCriteria.trim())){
            return null;
        }

        List<Long> jstructIds = new ArrayList<>();

        String[] raw = JStringUtil.splitOnSpaceAndComma(textCriteria);
        for(String s : raw){
                
            Long jstructId = StructureUtil.stringToJstructId(s);
            if(jstructId != null){
                jstructIds.add(jstructId);
            }
                
        }
        return jstructIds;
    }



    private List<String> criteriaToRcsbIds(){

        String textCriteria = inputCriteria;
        if(!StringUtil.isSet(textCriteria.trim())){
            return null;
        }

        List<String> rcsbIds = new ArrayList<>();

        String[] raw = JStringUtil.splitOnSpaceAndComma(textCriteria);
        for(String s : raw){
            
            String rcsbId = StructureUtil.stringToRcsbId(s);
            if(StringUtil.isSet(rcsbId)){
                rcsbIds.add(rcsbId);
            }
            
        }

        return rcsbIds;
    }





}
