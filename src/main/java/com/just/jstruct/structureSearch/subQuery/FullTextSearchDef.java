package com.just.jstruct.structureSearch.subQuery;


import java.util.List;

/**
 * defines the fields (defined as FullTextSearchColumnDef) of a single table to search
 * -
 * this may be a single table that contains the struct_ver_id key,
 * or a child table of a parent (where the parent contains the struct_ver_id_key)
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class FullTextSearchDef {




    private boolean mIsSingleTable;
    private String mTableToSearch;
    private List<FullTextSearchColumnDef> mFieldsToSearch;
    private String mStructVerIdCol;


    /**
     * create a SimpleSearchDefinition based on a single table, the columns to search, and the column that contains the struct_ver_id value
     * @param tableToSearch
     * @param fieldsToSearch
     * @param structVerIdCol
     */
    public FullTextSearchDef(String tableToSearch, List<FullTextSearchColumnDef> fieldsToSearch, String structVerIdCol) {

        this.mFieldsToSearch = fieldsToSearch;
        this.mStructVerIdCol = structVerIdCol;
        this.mIsSingleTable = true;
        this.mTableToSearch = tableToSearch;

    }


    /**
     * create a FullTextSearchDef based on child/parent tables,
     * where the parent table has the struct_ver_id key, and the child table has the field(s) to search
     * @param tableToSearch
     * @param fieldsToSearch
     * @param structVerIdCol
     * @param structVerIdTable 
     * @param parentKey
     * @param foreignKey
     */
    public FullTextSearchDef(String tableToSearch, List<FullTextSearchColumnDef> fieldsToSearch, String structVerIdCol, String structVerIdTable, String parentKey, String foreignKey){

        this.mFieldsToSearch = fieldsToSearch;
        this.mStructVerIdCol = structVerIdCol;
        this.mIsSingleTable = false;

        StringBuilder sb = new StringBuilder();

        //SELECT part
        sb.append(" ( \n SELECT ");
        for(FullTextSearchColumnDef searchColDef : fieldsToSearch){
            sb.append(searchColDef.getSearchCol());
            sb.append(", ");
        }
        sb.append(structVerIdCol);
        sb.append(" AS ");
        sb.append(SubQueryFullTextHelper.STRUCT_VERSION_ID_ALIAS);
        sb.append(" \n");

        //FROM part
        sb.append(" FROM ");
        sb.append(tableToSearch);
        sb.append(" \n");

        //JOIN part
        sb.append("  LEFT JOIN ");
        sb.append(structVerIdTable);
        sb.append(" \n");

        sb.append("    ON ");
        sb.append(parentKey);
        sb.append(" = ");
        sb.append(foreignKey);
        sb.append(" \n");

        //WHERE part
        sb.append(" WHERE ");
        sb.append(structVerIdCol);
        sb.append(" IS NOT NULL \n");
        sb.append(" ) ");

        this.mTableToSearch = sb.toString();

    }


    public boolean isIsSingleTable() { return mIsSingleTable; }
    public void setIsSingleTable(boolean isSingleTable) { this.mIsSingleTable = isSingleTable; }

    public String getTableToSearch() { return mTableToSearch; }
    public void setTableToSearch(String tableToSearch) { this.mTableToSearch = tableToSearch; }

    public List<FullTextSearchColumnDef> getFieldsToSearch() { return mFieldsToSearch; }
    public void setFieldsToSearch(List<FullTextSearchColumnDef> fullTextSearchColumnDefs) { this.mFieldsToSearch = fullTextSearchColumnDefs; }

    public String getStructVerIdField() { return mStructVerIdCol; }
    public void setStructVerIdField(String structVerIdField) { this.mStructVerIdCol = structVerIdField; }




}
