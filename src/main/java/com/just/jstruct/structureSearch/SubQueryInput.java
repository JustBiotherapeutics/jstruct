package com.just.jstruct.structureSearch;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.service.UserLabelService;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserLabel;
import com.just.jstruct.structureSearch.enums.AllAnyOption;
import com.just.jstruct.structureSearch.enums.EnhancedText;
import com.just.jstruct.structureSearch.enums.EqualsOption;
import com.just.jstruct.structureSearch.enums.HeaderField;
import com.just.jstruct.structureSearch.enums.SequenceOption;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.structureSearch.enums.YesNoOption;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SubQueryInput implements Serializable {
 
    
    
    private static final long serialVersionUID = 1L;
    
    private SubQueryType selectedSubqueryType;
    private Integer rowIndex;
    private Integer resultCount;
    
    
    private String errorMessage;
    
    
    // FULL_TEXT
    private String inputFullText;
    
    //IDENTIFIER
    private String inputIdentifier;
    
    //SEQUENCE
    private SequenceOption sequenceOption;
    private EnhancedText sequenceEnhancedText;
    private String inputSequence;

    
    //EVERYTHING
    //note: no user entered params
    
    //STRUCT_VER_IDS
    private String inputStructVerIds;
    
    
    //LABEL
    private AllAnyOption allAnyLabelOption;
    private List<Jstruct_UserLabel> selectedLabels = new ArrayList<>();
    private List<Jstruct_UserLabel> userLabels;
    
    //NOTE
    private EnhancedText selectedNoteEnhancedText;
    private String inputNote;
    
    
    
    
    //ANY_HEADER
    private HeaderField selectedHeaderField;
    private EnhancedText selectedAnyHeaderEnhancedText;
    private String inputAnyHeader;
    
    //TITLE
    private EnhancedText selectedTitleEnhancedText;
    private String inputTitle;
    
    //AUTHOR
    private String inputAuthor;
    
    //OWNER
    private EqualsOption selectedOwnerEqualsOption;
    private Jstruct_User selectedOwner;
    
    //MODEL
    private YesNoOption yesNoModelOption;
    
    //DEPOSIT_DATES
    private Date depositDateFrom;
    private Date depositDateTo;
    
    //PDB_REMARK
    private EnhancedText selectedPdbRemarkEnhancedText;
    private String inputPdbRemark;
    
    //JRNL
    private String inputJrnl;
    
    //COMPND
    private EnhancedText selectedCompndEnhancedText;
    private String inputCompnd;
    
    //SOURCE
    private EnhancedText selectedSourceEnhancedText;
    private String inputSource;
    
    
    
    private final UserLabelService userLabelService = new UserLabelService();
    //private final SessionBean sessionBean = FacesUtil.getSessionBean();
    
    
    public SubQueryInput(Jstruct_User currentUser) {
        try {
            
            this.rowIndex = 0;
            
            //subqueryType default
            selectedSubqueryType = SubQueryType.FULL_TEXT;
            
            //dropdown defaults
            sequenceOption = SequenceOption.OPTION_SEQRES;
            sequenceEnhancedText = EnhancedText.OPTION_CONTAINS;
            
            selectedHeaderField = HeaderField.TITLE;
            selectedAnyHeaderEnhancedText = EnhancedText.OPTION_CONTAINS;
            selectedTitleEnhancedText = EnhancedText.OPTION_CONTAINS;
            selectedNoteEnhancedText = EnhancedText.OPTION_CONTAINS;
            selectedOwnerEqualsOption = EqualsOption.OPTION_EQUALS;
            yesNoModelOption = YesNoOption.OPTION_YES;
            selectedPdbRemarkEnhancedText = EnhancedText.OPTION_CONTAINS;
            selectedCompndEnhancedText = EnhancedText.OPTION_CONTAINS;
            selectedSourceEnhancedText = EnhancedText.OPTION_CONTAINS;
            
            //plus a little special attention to the label portion
            allAnyLabelOption = AllAnyOption.OPTION_HAS_ANY_OF;
            userLabels = userLabelService.getByUserId(currentUser.getUserId());
            
        
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }

    
    
    
    public SubQueryType getSelectedSubqueryType() {
        return selectedSubqueryType;
    }

    public void setSelectedSubqueryType(SubQueryType selectedSubqueryType) {
        this.selectedSubqueryType = selectedSubqueryType;
    }

    public Integer getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(Integer rowIndex) {
        this.rowIndex = rowIndex;
    }

    public Integer getResultCount() {
        return resultCount;
    }

    public void setResultCount(Integer resultCount) {
        this.resultCount = resultCount;
    }

    
    
    
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public void clearErrorMessage(){
        errorMessage = null;
    }

    
    
    
    
    
    public String getInputFullText() {
        return inputFullText;
    }

    public void setInputFullText(String inputFullText) {
        this.inputFullText = inputFullText;
    }

    public String getInputIdentifier() {
        return inputIdentifier;
    }

    public void setInputIdentifier(String inputIdentifier) {
        this.inputIdentifier = inputIdentifier;
    }

    
    
    public SequenceOption getSequenceOption() {
        return sequenceOption;
    }

    public void setSequenceOption(SequenceOption sequenceOption) {
        this.sequenceOption = sequenceOption;
    }

    public EnhancedText getSequenceEnhancedText() {
        return sequenceEnhancedText;
    }

    public void setSequenceEnhancedText(EnhancedText sequenceEnhancedText) {
        this.sequenceEnhancedText = sequenceEnhancedText;
    }

    public String getInputSequence() {
        return inputSequence;
    }

    public void setInputSequence(String inputSequence) {
        this.inputSequence = inputSequence;
    }

    
    
    
    
    public String getInputStructVerIds() {
        return inputStructVerIds;
    }

    public void setInputStructVerIds(String inputStructVerIds) {
        this.inputStructVerIds = inputStructVerIds;
    }

    public HeaderField getSelectedHeaderField() {
        return selectedHeaderField;
    }

    public void setSelectedHeaderField(HeaderField selectedHeaderField) {
        this.selectedHeaderField = selectedHeaderField;
    }

    public EnhancedText getSelectedAnyHeaderEnhancedText() {
        return selectedAnyHeaderEnhancedText;
    }

    public void setSelectedAnyHeaderEnhancedText(EnhancedText selectedAnyHeaderEnhancedText) {
        this.selectedAnyHeaderEnhancedText = selectedAnyHeaderEnhancedText;
    }

    public String getInputAnyHeader() {
        return inputAnyHeader;
    }

    public void setInputAnyHeader(String inputAnyHeader) {
        this.inputAnyHeader = inputAnyHeader;
    }

    public EnhancedText getSelectedTitleEnhancedText() {
        return selectedTitleEnhancedText;
    }

    public void setSelectedTitleEnhancedText(EnhancedText selectedTitleEnhancedText) {
        this.selectedTitleEnhancedText = selectedTitleEnhancedText;
    }

    public String getInputTitle() {
        return inputTitle;
    }

    public void setInputTitle(String inputTitle) {
        this.inputTitle = inputTitle;
    }

    public EnhancedText getSelectedNoteEnhancedText() {
        return selectedNoteEnhancedText;
    }

    public void setSelectedNoteEnhancedText(EnhancedText selectedNoteEnhancedText) {
        this.selectedNoteEnhancedText = selectedNoteEnhancedText;
    }

    public String getInputNote() {
        return inputNote;
    }

    public void setInputNote(String inputNote) {
        this.inputNote = inputNote;
    }

    public String getInputAuthor() {
        return inputAuthor;
    }

    public void setInputAuthor(String inputAuthor) {
        this.inputAuthor = inputAuthor;
    }

    public EqualsOption getSelectedOwnerEqualsOption() {
        return selectedOwnerEqualsOption;
    }

    public void setSelectedOwnerEqualsOption(EqualsOption selectedOwnerEqualsOption) {
        this.selectedOwnerEqualsOption = selectedOwnerEqualsOption;
    }

    public Jstruct_User getSelectedOwner() {
        return selectedOwner;
    }

    public void setSelectedOwner(Jstruct_User selectedOwner) {
        this.selectedOwner = selectedOwner;
    }

    public YesNoOption getYesNoModelOption() {
        return yesNoModelOption;
    }

    public void setYesNoModelOption(YesNoOption yesNoModelOption) {
        this.yesNoModelOption = yesNoModelOption;
    }

    public Date getDepositDateFrom() {
        return depositDateFrom;
    }

    public void setDepositDateFrom(Date depositDateFrom) {
        this.depositDateFrom = depositDateFrom;
    }

    public Date getDepositDateTo() {
        return depositDateTo;
    }

    public void setDepositDateTo(Date depositDateTo) {
        this.depositDateTo = depositDateTo;
    }

    public EnhancedText getSelectedPdbRemarkEnhancedText() {
        return selectedPdbRemarkEnhancedText;
    }

    public void setSelectedPdbRemarkEnhancedText(EnhancedText selectedPdbRemarkEnhancedText) {
        this.selectedPdbRemarkEnhancedText = selectedPdbRemarkEnhancedText;
    }

    public String getInputPdbRemark() {
        return inputPdbRemark;
    }

    public void setInputPdbRemark(String inputPdbRemark) {
        this.inputPdbRemark = inputPdbRemark;
    }

    public String getInputJrnl() {
        return inputJrnl;
    }

    public void setInputJrnl(String inputJrnl) {
        this.inputJrnl = inputJrnl;
    }

    public EnhancedText getSelectedCompndEnhancedText() {
        return selectedCompndEnhancedText;
    }

    public void setSelectedCompndEnhancedText(EnhancedText selectedCompndEnhancedText) {
        this.selectedCompndEnhancedText = selectedCompndEnhancedText;
    }

    public String getInputCompnd() {
        return inputCompnd;
    }

    public void setInputCompnd(String inputCompnd) {
        this.inputCompnd = inputCompnd;
    }

    public EnhancedText getSelectedSourceEnhancedText() {
        return selectedSourceEnhancedText;
    }

    public void setSelectedSourceEnhancedText(EnhancedText selectedSourceEnhancedText) {
        this.selectedSourceEnhancedText = selectedSourceEnhancedText;
    }

    public String getInputSource() {
        return inputSource;
    }

    public void setInputSource(String inputSource) {
        this.inputSource = inputSource;
    }

    public AllAnyOption getAllAnyLabelOption() {
        return allAnyLabelOption;
    }

    public void setAllAnyLabelOption(AllAnyOption allAnyLabelOption) {
        this.allAnyLabelOption = allAnyLabelOption;
    }

    
    public List<Jstruct_UserLabel> getUserLabels() {
        return userLabels;
    }

    public void setUserLabels(List<Jstruct_UserLabel> userLabels) {
        this.userLabels = userLabels;
    }
    
    
    
    
    public List<Jstruct_UserLabel> getSelectedLabels() {
        return selectedLabels;
    }

    public void setSelectedLabels(List<Jstruct_UserLabel> selectedLabels) {
        this.selectedLabels = selectedLabels;
    }

    public List<Long> getSelectedLabelIds() {
        if(CollectionUtil.hasValues(selectedLabels)){
            List<Long> selectedLabelIds = new ArrayList<>(selectedLabels.size());
            for(Jstruct_UserLabel userLabel : selectedLabels){
                if(userLabel != null){
                    selectedLabelIds.add(userLabel.getUserLabelId());
                }
            }
            return selectedLabelIds;
        }
        return null;
    }


    
    
    
}
