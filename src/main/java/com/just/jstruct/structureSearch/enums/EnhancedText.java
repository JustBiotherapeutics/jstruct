package com.just.jstruct.structureSearch.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum EnhancedText {

    OPTION_CONTAINS("Contains"),
    OPTION_EQUALS("Equals"),
    OPTION_STARTS_WITH("Starts with"),
    OPTION_ENDS_WITH("Ends with"),
    OPTION_DOES_NOT_CONTAIN("Does NOT contain"),
    OPTION_DOES_NOT_START_WITH("Does NOT start with"),
    OPTION_DOES_NOT_END_WITH("Does NOT end with");


    private final String mDisplayName;



    EnhancedText(String inDisplayName) {
        this.mDisplayName = inDisplayName;
    }


    public String getValue() {return this.toString();}
    public String getDisplayName() {return mDisplayName;}


    /**
     * get all enums as a list
     * @return
     */
    public static List<EnhancedText> enhancedTexts(){
        List<EnhancedText> enhancedTextSelect = new ArrayList<>(7);
        for(EnhancedText ets : EnhancedText.values()){
            enhancedTextSelect.add(ets);
        }
        return enhancedTextSelect;
    }


    /**
     * get the displayName values for all enums (as list of strings)
     * @return
     */
    public static List<String> enhancedTextDisplayNames(){
        List<String> enhancedTextSelect = new ArrayList<>(7);
        for(EnhancedText ets : EnhancedText.values()){
            enhancedTextSelect.add(ets.getDisplayName());
        }
        return enhancedTextSelect;
    }


    
    /**
     * create the LIKE or NOT LIKE portion of a WHERE clause based on the dropdown selection
     *
     * @param enhancedText
     * @param databaseColumn
     * @param textCriteria
     * @param isCaseSensitive 
     * @return
     */
    public static String appendLikeNotLike(EnhancedText enhancedText, String databaseColumn, String textCriteria, boolean isCaseSensitive){

        String likeOperator = " LIKE ";

        if(!isCaseSensitive) {
            likeOperator = " ILIKE ";
            //sb.append(" LOWER(").append(databaseColumn.name()).append(")");
            //textCriteria = textCriteria.toLowerCase();
        }

        StringBuilder sb = new StringBuilder();

        sb.append(" ").append(databaseColumn);

        switch (enhancedText) {
            case OPTION_CONTAINS:
                sb.append(likeOperator).append(" '%").append(textCriteria).append("%' ");       // foo LIKE '%bar%'
                break;
            case OPTION_EQUALS:
                sb.append(likeOperator).append(" '").append(textCriteria).append("' ");         // foo LIKE 'bar'
                break;
            case OPTION_STARTS_WITH:
                sb.append(likeOperator).append(" '").append(textCriteria).append("%' ");        // foo LIKE 'bar%'
                break;
            case OPTION_ENDS_WITH:
                sb.append(likeOperator).append(" '%").append(textCriteria).append("' ");        // foo LIKE '%bar'
                break;
            case OPTION_DOES_NOT_CONTAIN:
                sb.append(" NOT ").append(likeOperator).append(" '%").append(textCriteria).append("%' ");   // foo NOT LIKE '%bar%'
                break;
            case OPTION_DOES_NOT_START_WITH:
                sb.append(" NOT ").append(likeOperator).append(" '").append(textCriteria).append("%' ");    // foo NOT LIKE 'bar%'
                break;
            case OPTION_DOES_NOT_END_WITH:
                sb.append(" NOT ").append(likeOperator).append(" '%").append(textCriteria).append("' ");    // foo NOT LIKE '%bar'
                break;
        }

        return sb.toString();
    }




}

