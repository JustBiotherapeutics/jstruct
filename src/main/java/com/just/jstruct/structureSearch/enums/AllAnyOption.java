package com.just.jstruct.structureSearch.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum AllAnyOption {

    OPTION_HAS_ANY_OF("Has ANY of"),
    OPTION_HAS_ALL_OF("Has ALL of");


    private final String mDisplayName;



    AllAnyOption(String inDisplayName) {
        this.mDisplayName = inDisplayName;
    }


    public String getValue() {return this.toString();}
    public String getDisplayName() {return mDisplayName;}


    /**
     * get all enums as a list
     * @return
     */
    public static List<AllAnyOption> allAnyOptions(){
        List<AllAnyOption> AllAnyOptions = new ArrayList<>(2);
        for(AllAnyOption aao : AllAnyOption.values()){
            AllAnyOptions.add(aao);
        }
        return AllAnyOptions;
    }


    /**
     * get the displayName values for all enums (as list of strings)
     * @return
     */
    public static List<String> allAnyOptionDisplayNames(){
        List<String> allAnyOptions = new ArrayList<>(2);
        for(AllAnyOption aao : AllAnyOption.values()){
            allAnyOptions.add(aao.getDisplayName());
        }
        return allAnyOptions;
    }






}

