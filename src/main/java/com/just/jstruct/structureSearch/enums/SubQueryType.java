package com.just.jstruct.structureSearch.enums;

import com.just.jstruct.model.Jstruct_User;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum SubQueryType {

    
    // GENERAL 
    
    FULL_TEXT (
            "Full Text Search",
            SubQueryGroup.GENERAL,
            "Search most of the Structure file for matching text.",   //todo how does the user know what is being searched?
            false,
            false),
    IDENTIFIER (
            "JStruct/RCSB Identifier(s)",
            SubQueryGroup.GENERAL,
            "Search for Structures by JStruct VALUE_MAP_ID or RCSB Identifier(s); multiple identifiers can be entered (separated by comma or space)",
            false,
            false),
    SEQUENCE (
            "Sequences",
            SubQueryGroup.GENERAL,
            "Search atomic coordinates or the SEQRES residues of Protein and/or Nucleic Acid sequences (case insensitive)",
            false,
            false),
    
    
    EVERYTHING (
            "All Structures/No parameters - Admin only",
            SubQueryGroup.GENERAL,
            "a search that returns all Structures (i.e. no user entered parameters)",
            true,
            true),
    
    STRUCT_VER_IDS (
            "Structure Version IDs - Admin only",
            SubQueryGroup.GENERAL,
            "search based only on matching structure version ids (structVerId field)",
            true,
            true),

    
    //USER_DATA
    
    LABEL (
            "User defined Labels",
            SubQueryGroup.USER_DATA,
            "Search for Structures by user-assigned labels",
            false,
            true),
    
    NOTE (
            "User entered Notes",
            SubQueryGroup.USER_DATA,
            "Search for Structures based on comments entered into Notes",
            false,
            false),
    

    //HEADER/TITLE

    ANY_HEADER (
            "Header Text Fields",
            SubQueryGroup.HEADER_TITLE,
            "Search for matching text in any of the text-based Header/Title fields",
            false,
            false),
    TITLE (
            "Structure Title",
            SubQueryGroup.HEADER_TITLE,
            "Search all or part of the Structure Title",
            false,
            false),  //todo include JRNL Title in search??
    AUTHOR (
            "Author (in Header or JRNL)",
            SubQueryGroup.HEADER_TITLE,
            "Search by Author Name; finds matches in both the Author(s) field of the Header section, and the Author(s) associated with the primary Journal",
            false,
            false),


    //FILE INFO

    OWNER (
            "Structure Owner (uploader)",
            SubQueryGroup.FILE_INFO,
            "Search by Owner; Owner is defined as the person that uploaded the file, with 'System' being used for automated processes",
            false,
            false),
    MODEL (
            "Structure Model",
            SubQueryGroup.FILE_INFO,
            "Search for Structures that have been flagged as Models",
            false,
            false),


    // DATES

    DEPOSIT_DATES (
            "Deposit Date",
            SubQueryGroup.DATES,
            "Search by date the file was deposited into RCSB (or manually uploaded); only one field is required",
            false,
            false),


    //OTHER

    PDB_REMARK (
            "Structure Remarks (PDB format only)",
            SubQueryGroup.OTHER,
            "Searches the text of the REMARK section",
            false,
            false),
    JRNL (
            "Journal (all fields of JRNL)",
            SubQueryGroup.OTHER,
            "Searches across all fields within the Journal section",
            false,
            false),
    COMPND (
            "Structure COMPND",
            SubQueryGroup.OTHER,
            "Searches the text of the COMPND section",
            false,
            false),
    SOURCE (
            "Structure SOURCE",
            SubQueryGroup.OTHER,
            "Searches the text of the SOURCE section",
            false,
            false);



    /*
        note - current SubQueryGroups to use in list above:
            GENERAL("General"),
            USER_DATA("User Data"),
            HEADER_TITLE("Header/Title"),
            FILE_INFO("File Information"),
            DATES("Dates"),
            OTHER("Other"),
            ADMIN_ONLY("Admin Only");
     */


    private final String mDisplayName;
    private final SubQueryGroup mSubQueryGroup;
    private final String mDescription;
    private final boolean mAdminOnly;
    private final boolean mAuthenticatedOnly;



    SubQueryType(String inDisplayName, SubQueryGroup inSubQueryGroup, String inDescription, boolean inAdminOnly, boolean inAuthenticatedOnly) {
        this.mDisplayName = inDisplayName;
        this.mSubQueryGroup = inSubQueryGroup;
        this.mDescription = inDescription;
        this.mAdminOnly = inAdminOnly;
        this.mAuthenticatedOnly = inAuthenticatedOnly;
    }


    public String getValue() {return this.toString();}
    public String getDisplayName() {return mDisplayName;}
    public SubQueryGroup getSubQueryGroup() {return mSubQueryGroup;}
    public String getDescription() {return mDescription;}
    public boolean isAdminOnly() {return mAdminOnly;}
    public boolean isAuthenticatedOnly() {return mAuthenticatedOnly;}



    //get all that this user can deal with, grouped by group (used in the dropdown list)
    public static Map<SubQueryGroup, List<SubQueryType>> groupedSubQueryTypeMap(Jstruct_User user){

        Map<SubQueryGroup, List<SubQueryType>> mapList = new LinkedHashMap<>();

        for(SubQueryType key : SubQueryType.values()) {

            if(isAllowed(key, user)) {

                if (mapList.containsKey(key.getSubQueryGroup())) {
                    mapList.get(key.getSubQueryGroup()).add(key);
                } else {
                    List first = new ArrayList<>();
                    first.add(key);
                    mapList.put(key.getSubQueryGroup(), first);
                }

            }
        }
        return mapList;
    }
    
    

    //get all that this user can deal with
    public static List<SubQueryType> subQueryTypeList(Jstruct_User user){

        List<SubQueryType> subQueryKeys = new ArrayList<>();
        for(SubQueryType key : SubQueryType.values()) {

            if(isAllowed(key, user)) {
                subQueryKeys.add(key);
            }

        }
        return subQueryKeys;
    }





    private static boolean isAllowed(SubQueryType subQueryKey, Jstruct_User user){

        if(subQueryKey.mAuthenticatedOnly && user.isGuest()){
            return false;
        }

        if(subQueryKey.mAdminOnly && !user.getIsRoleAdmin()){
            return false;
        }

        return true;

    }





}

