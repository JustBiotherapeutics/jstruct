package com.just.jstruct.structureSearch.enums;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum QueryMode {

    AND("AND"),
    OR("OR");
    
    
    private final String mDisplayName;
    
    QueryMode(String inDisplayName) {
        this.mDisplayName = inDisplayName;
    }
    
    public String getDisplayName() {return mDisplayName;}

    /**
     * get the values for all enums (as list of strings)
     * @return
     */
    public static List<QueryMode> getAsList(){
        List<QueryMode> searchModes = new ArrayList<>(2);
        for(QueryMode sqm : QueryMode.values()){
            searchModes.add(sqm);
        }
        return searchModes;
    }


}

