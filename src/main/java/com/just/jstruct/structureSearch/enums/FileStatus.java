package com.just.jstruct.structureSearch.enums;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import java.util.ArrayList;
import java.util.List;



/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum FileStatus {

    FILE_STATUS_ACTIVE("Active only"),
    FILE_STATUS_OBSOLETE("Obsolete only"),
    FILE_STATUS_ALL("All"),
    FILE_STATUS_NONE("None");


    private final String mDisplayName;



    FileStatus(String inDisplayName) {
        this.mDisplayName = inDisplayName;
    }


    public String getDisplayName() {return mDisplayName;}


    /**
     * get all enums as a list
     * @return
     */
    public static List<FileStatus> fileStatuss(){
        List<FileStatus> structureFileStatuses = new ArrayList<>(4);
        for(FileStatus sfs : FileStatus.values()){
            structureFileStatuses.add(sfs);
        }
        return structureFileStatuses;
    }


    /**
     * get the displayName values for all enums (as list of strings)
     * @return
     */
    public static List<String> fileStatusDisplayNames(){
        List<String> fileStatusOptions = new ArrayList<>(4);
        for(FileStatus sfs : FileStatus.values()){
            fileStatusOptions.add(sfs.getDisplayName());
        }
        return fileStatusOptions;
    }





    public static String createFileStatusClause(FileStatus fileStatusRcsb, FileStatus fileStatusManual, boolean onlyMostCurrentVersions, boolean asHql) {
        
      
        String fromRcsbField = "fs.from_Rcsb";
        String isObsoleteField = "fs.is_Obsolete";
        String structureIdField = "fs.structure_Id";
        String mostCurrentField = "fs.most_Current";
        if(asHql){
            fromRcsbField = "fs.fromRcsb";
            isObsoleteField = "fs.isObsolete";
            structureIdField = "fs.structureId";
            mostCurrentField = "fs.mostCurrent";
        }

        FileStatus fileStatusActive = FileStatus.FILE_STATUS_ACTIVE;
        FileStatus fileStatusObsolete = FileStatus.FILE_STATUS_OBSOLETE;
        FileStatus fileStatusAll = FileStatus.FILE_STATUS_ALL;
        FileStatus fileStatusNone = FileStatus.FILE_STATUS_NONE;

        boolean includeRcsbActive = false;
        boolean includeRcsbObsolete = false;
        boolean includeManualActive = false;
        boolean includeManualObsolete = false;

        if(fileStatusRcsb!=null) {
            includeRcsbActive = fileStatusRcsb.equals(fileStatusActive) || fileStatusRcsb.equals(fileStatusAll);
            includeRcsbObsolete = fileStatusRcsb.equals(fileStatusObsolete) || fileStatusRcsb.equals(fileStatusAll);
        }
        if(fileStatusManual!=null) {
            includeManualActive = fileStatusManual.equals(fileStatusActive) || fileStatusManual.equals(fileStatusAll);
            includeManualObsolete = fileStatusManual.equals(fileStatusObsolete) || fileStatusManual.equals(fileStatusAll);
        }

        StringBuilder sql = new StringBuilder();


        //add in what we want: RCSB/Manual, Active/Obsolete
        List<String> subSql = new ArrayList<>();

        if(includeRcsbActive){
            subSql.add(" ( "+fromRcsbField+" LIKE 'Y' AND "+isObsoleteField+" LIKE 'N' ) ");
        }
        if(includeRcsbObsolete){
            subSql.add(" ( "+fromRcsbField+" LIKE 'Y' AND "+isObsoleteField+" LIKE 'Y') ");
        }
        if(includeManualActive){
            subSql.add(" ( "+fromRcsbField+" LIKE 'N' AND "+isObsoleteField+" LIKE 'N') ");
        }
        if(includeManualObsolete){
            subSql.add(" ( "+fromRcsbField+" LIKE 'N' AND "+isObsoleteField+" LIKE 'Y') ");
        }

        if(CollectionUtil.hasValues(subSql)){
            sql.append(" (");
            sql.append(StringUtil.join(subSql, " OR "));
            sql.append(") ");
        } else {
            //user selected none (RCSB) and none (manual)... this will force no matches:
            sql.append(" ( ").append(structureIdField).append(" = 0 ) ");
        }


        //limit query if only searching current versions
         if(onlyMostCurrentVersions){
             sql.append(" AND ( ").append(mostCurrentField).append(" LIKE 'Y' ) ");
         }


        return sql.toString();
    }


}




