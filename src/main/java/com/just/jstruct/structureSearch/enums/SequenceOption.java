package com.just.jstruct.structureSearch.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum SequenceOption {

    OPTION_SEQRES(  "Sequence (SEQRES)",            null,                       "Sequences identified in the SEQRES record"),
    OPTION_ANY(     "Any Coordinates",              "seq_with_any_coords",      "Sequences which have ANY atomic coordinates listed"),
    OPTION_ALL(     "All Coordinates",              "seq_with_all_coords",      "Sequences which have ALL atomic coordinates listed (i.e. full set of atoms for the residue)"),
    OPTION_CA(      "CA Coordinates",               "seq_with_ca_coords",       "Sequences which have CA atomic coordinates listed"),
    OPTION_BKBN(    "Backbone Coordinates",         "seq_with_bkbn_coords",     "Sequences which have all Backbone atomic coordinates listed"),
    OPTION_BKBN_CB( "Backbone + CB Coordinates",    "seq_with_bkbn_cb_coords",  "Sequences which have all Backbone AND CB atomic coordinates listed");


    private final String mDisplayName;
    private final String mAtomGroupingFieldName;
    private final String mDescription;



    SequenceOption(String inDisplayName, String atomGroupingFieldName, String description) {
        this.mDisplayName = inDisplayName;
        this.mAtomGroupingFieldName = atomGroupingFieldName;
        this.mDescription = description;
    }


    public String getValue() {return this.toString();}
    public String getDisplayName() {return mDisplayName;}
    public String getAtomGroupingFieldName() {return mAtomGroupingFieldName;}
    public String getDescription() {return mDescription;}


    /**
     * get all enums as a list
     * @return
     */
    public static List<SequenceOption> allSequenceOptions(){
        List<SequenceOption> allSequenceOptions = new ArrayList<>(2);
        for(SequenceOption aso : SequenceOption.values()){
            allSequenceOptions.add(aso);
        }
        return allSequenceOptions;
    }


    /**
     * get the displayName values for all enums (as list of strings)
     * @return
     */
    public static List<String> allSequenceOptionDisplayNames(){
        List<String> allSequenceOptions = new ArrayList<>(2);
        for(SequenceOption aso : SequenceOption.values()){
            allSequenceOptions.add(aso.getDisplayName());
        }
        return allSequenceOptions;
    }






}

