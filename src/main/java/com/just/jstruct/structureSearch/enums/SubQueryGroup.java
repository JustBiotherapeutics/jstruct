package com.just.jstruct.structureSearch.enums;

/**
 *
 */
public enum SubQueryGroup {

    GENERAL("General"),
    USER_DATA("User Data"),
    HEADER_TITLE("Header/Title"),
    FILE_INFO("File Information"),
    DATES("Dates"),
    OTHER("Other"),
    ADMIN_ONLY("Admin Only");


    private final String mDisplayName;



    SubQueryGroup(String inDisplayName) {
        this.mDisplayName = inDisplayName;
    }


    public String displayName() {return mDisplayName;}




}




