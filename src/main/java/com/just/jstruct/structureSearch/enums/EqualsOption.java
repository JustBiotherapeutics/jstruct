package com.just.jstruct.structureSearch.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum EqualsOption {

    OPTION_EQUALS("Equals"),
    OPTION_DOES_NOT_EQUAL("Does NOT Equal");
    

    private final String mDisplayName;



    EqualsOption(String inDisplayName) {
        this.mDisplayName = inDisplayName;
    }


    public String getValue() {return this.toString();}
    public String getDisplayName() {return mDisplayName;}


    /**
     * get all enums as a list
     * @return
     */
    public static List<EqualsOption> equalsOptions(){
        List<EqualsOption> equalsOptions = new ArrayList<>(2);
        for(EqualsOption eo : EqualsOption.values()){
            equalsOptions.add(eo);
        }
        return equalsOptions;
    }


    /**
     * get the displayName values for all enums (as list of strings)
     * @return
     */
    public static List<String> equalsOptionDisplayNames(){
        List<String> equalsOptions = new ArrayList<>(2);
        for(EqualsOption eo : EqualsOption.values()){
            equalsOptions.add(eo.getDisplayName());
        }
        return equalsOptions;
    }



    
    
     /**
     * create the "=" or "!=" portion of a WHERE clause based on the dropdown selection
     *
     * @param equalsOption
     * @param databaseColumn
     * @param keyCriteria
     * @return
     */
    public static String appendEqualNotEqual(String databaseColumn, EqualsOption equalsOption, Long keyCriteria){

        StringBuilder sb = new StringBuilder();

        sb.append(" ").append(databaseColumn);

        switch (equalsOption) {

            case OPTION_EQUALS:
                sb.append(" = ").append(keyCriteria).append(" ");     // foo = 123
                break;

            case OPTION_DOES_NOT_EQUAL:
                sb.append(" != ").append(keyCriteria).append(" ");   // foo != 123
                break;

        }

        return sb.toString();
    }

    /**
     * create the "LIKE " or "NOT LIKE" portion of a WHERE clause based on the dropdown selection
     *
     * @param equalsOption
     * @param databaseColumn
     * @param keyCriteria
     * @return
     */
    public static String appendEqualNotEqual(String databaseColumn, EqualsOption equalsOption, String keyCriteria){

        StringBuilder sb = new StringBuilder();

        sb.append(" ").append(databaseColumn);

        switch (equalsOption) {

            case OPTION_EQUALS:
                sb.append(" LIKE '").append(keyCriteria).append("' ");     // foo LIKE 'bar'
                break;

            case OPTION_DOES_NOT_EQUAL:
                sb.append(" NOT LIKE '").append(keyCriteria).append("' ");   // foo NOT LIKE 'bar'
                break;

        }

        return sb.toString();
    }



}

