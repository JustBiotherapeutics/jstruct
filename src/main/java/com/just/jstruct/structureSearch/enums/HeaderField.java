package com.just.jstruct.structureSearch.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum HeaderField {

    TITLE(          "Title", "pdb_title"),
    CLASSIFICATION( "Classification", "pdb_classification"),
    CAVEAT(         "CAVEAT", "pdb_caveat"),
    EXPDTA(         "EXPDTA", "pdb_expdta"),
    MDLTYP(         "MDLTYP", "pdb_mdltyp");


    private final String mDisplayName;
    private final String mFullStructureColumn;



    HeaderField(String inDisplayName, String inFullStructureColumn) {
        this.mDisplayName = inDisplayName;
        this.mFullStructureColumn = inFullStructureColumn;
    }


    public String getValue() {return this.toString();}
    public String getDisplayName() {return mDisplayName;}
    public String fullStructureColumn() {return mFullStructureColumn;}


    /**
     * get all enums as a list
     * @return
     */
    public static List<HeaderField> headerFields(){
        List<HeaderField> headerTextFields = new ArrayList<>(5);
        for(HeaderField opt : HeaderField.values()){
            headerTextFields.add(opt);
        }
        return headerTextFields;
    }


    /**
     * get the displayName values for all enums (as list of strings)
     * @return
     */
    public static List<String> headerFieldDisplayNames(){
        List<String> fieldNames = new ArrayList<>(5);
        for(HeaderField opt : HeaderField.values()){
            fieldNames.add(opt.getDisplayName());
        }
        return fieldNames;
    }






}

