package com.just.jstruct.structureSearch.enums;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public enum YesNoOption {

    OPTION_YES("Yes"),
    OPTION_NO("No");


    private final String mDisplayName;



    YesNoOption(String inDisplayName) {
        this.mDisplayName = inDisplayName;
    }


    public String getValue() {return this.toString();}
    public String getDisplayName() {return mDisplayName;}


    /**
     * get all enums as a list
     * @return
     */
    public static List<YesNoOption> yesNoOptions(){
        List<YesNoOption> yesNoOptions = new ArrayList<>(2);
        for(YesNoOption yno : YesNoOption.values()){
            yesNoOptions.add(yno);
        }
        return yesNoOptions;
    }


    /**
     * get the displayName values for all enums (as list of strings)
     * @return
     */
    public static List<String> yesNoOptionDisplayNames(){
        List<String> yesNoOptions = new ArrayList<>(2);
        for(YesNoOption yno : YesNoOption.values()){
            yesNoOptions.add(yno.getDisplayName());
        }
        return yesNoOptions;
    }






}

