package com.just.jstruct.structureSearch;

import com.hfg.exception.ProgrammingException;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.subQuery.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class FullQueryInput implements Serializable
{
 
    private FileStatus fileStatusRcsb;
    private FileStatus fileStatusManual;
    
    private QueryMode queryMode;
    
    private List<SubQueryInput> subQueryInputs;

    
    
    
    /**
     * constructor takes the fileTypes selected
     * @param fileStatusRcsb
     * @param fileStatusManual 
     * @param queryMode 
     */
    public FullQueryInput(FileStatus fileStatusRcsb, FileStatus fileStatusManual, QueryMode queryMode)
    {
        this.fileStatusRcsb = fileStatusRcsb;
        this.fileStatusManual = fileStatusManual;
        this.queryMode = queryMode;
    }

    
    
    
    
    public FileStatus getFileStatusRcsb() { return fileStatusRcsb;}
    public void setFileStatusRcsb(FileStatus fileStatusRcsb) {this.fileStatusRcsb = fileStatusRcsb; }

    public FileStatus getFileStatusManual() { return fileStatusManual;}
    public void setFileStatusManual(FileStatus fileStatusManual) {this.fileStatusManual = fileStatusManual;}

    
    public QueryMode getQueryMode() {return queryMode; }
    public void setQueryMode(QueryMode queryMode) {this.queryMode = queryMode;}

    
    public List<SubQueryInput> getSubQueryInputs()
    {
        return subQueryInputs;
    }
    
    public SubQueryInput getSubQueryInputWithRowIndex(Integer rowIndexToFind)
    {
        if(CollectionUtil.hasValues(subQueryInputs))
        {
            for(SubQueryInput subQueryInput : subQueryInputs)
            {
                if(rowIndexToFind.equals(subQueryInput.getRowIndex()))
                {
                    return subQueryInput;
                }
            }
        }
        return null;
    }
    
    
    
    /**
     * 
     * @return 
     */
    public List<SubQuery> getAsSubQuerys()
    {
        
        if(!CollectionUtil.hasValues(subQueryInputs))
        {
            return null;
        }
        
        List<SubQuery> subquerys = new ArrayList<>(subQueryInputs.size());
        for(SubQueryInput sqi : subQueryInputs)
        {
            subquerys.add(subQueryInputToSubQuery(sqi));
        }
        return subquerys;

    }
    
    private SubQuery subQueryInputToSubQuery(SubQueryInput subQueryInput)
    {
        SubQuery subQuery = null;
        switch (subQueryInput.getSelectedSubqueryType())
        {
            //GENERAL
            
            case FULL_TEXT:
                subQuery = new SubQueryFullText(subQueryInput);
                break;
            case IDENTIFIER:
                subQuery = new SubQueryIdentifier(subQueryInput);
                break;
            case SEQUENCE:
                subQuery = new SubQuerySequence(subQueryInput);
                break;
            
            case EVERYTHING:
                subQuery = new SubQueryEverything(subQueryInput);
                break;
            case STRUCT_VER_IDS:
                subQuery = new SubQueryStructVerIds(subQueryInput);
                break;
                
                
            // USER DATA
                
            case LABEL:
                subQuery = new SubQueryLabel(subQueryInput);
                break;
                
            case NOTE:
                subQuery = new SubQueryNote(subQueryInput);
                break;
                
                
            // HEADER/TITLE
                
            case ANY_HEADER:
                subQuery = new SubQueryAnyHeader(subQueryInput);
                break;
            case TITLE:
                subQuery = new SubQueryTitle(subQueryInput);
                break; 
            case AUTHOR:
                subQuery = new SubQueryAuthor(subQueryInput);
                break;
                
                
            //FILE INFO
                
            case OWNER:
                subQuery = new SubQueryOwner(subQueryInput);  
                break;
            case MODEL:
                subQuery = new SubQueryModel(subQueryInput);
                break;
                
                
            // DATES
                
            case DEPOSIT_DATES:
                subQuery = new SubQueryDepositDates(subQueryInput);
                break;
                
                
            //OTHER
                
            case PDB_REMARK:
                subQuery = new SubQueryPdbRemark(subQueryInput);
                break;
            case JRNL:
                subQuery = new SubQueryJrnl(subQueryInput);
                break;
            case COMPND:
                subQuery = new SubQueryCompnd(subQueryInput);
                break;
            case SOURCE:
                subQuery = new SubQuerySource(subQueryInput);
                break;
                
            default:
                throw new ProgrammingException("SubQuery type '" + subQueryInput.getSelectedSubqueryType() + "' is not valid.");
        }
        return subQuery;
    }
    
    
    /**
     * 
     * @param newSubQueryInput 
     */
    public void addSubQueryInput(SubQueryInput newSubQueryInput)
    {
        Integer rowIndex = 0;
        if(CollectionUtil.hasValues(subQueryInputs))
        {
            for(SubQueryInput sqi : subQueryInputs)
            {
                if(sqi.getRowIndex() > rowIndex){
                    rowIndex = sqi.getRowIndex();
                }
            }
        }
        Integer nextRowIndex = rowIndex+1;
        newSubQueryInput.setRowIndex(nextRowIndex);
        addSubQueryInput(nextRowIndex, newSubQueryInput);   
    }
    
    /**
     * 
     * @param rowIndex
     * @param subQueryInput 
     */
    private void addSubQueryInput(Integer rowIndex, SubQueryInput subQueryInput)
    {
        if(!CollectionUtil.hasValues(subQueryInputs))
        {
            subQueryInputs = new ArrayList<>();
        }
        subQueryInput.setRowIndex(rowIndex);
        subQueryInputs.add(subQueryInput);
    }
    
    
    /**
     * 
     * @param doomedRowIndex 
     */
    public void removeSubQueryInput(Integer doomedRowIndex)
    {
        if(CollectionUtil.hasValues(subQueryInputs))
        {
            List<SubQueryInput> updatedSubQueryInputs = new ArrayList<>(subQueryInputs.size()-1);
            for(SubQueryInput sqi : subQueryInputs){
                if(!sqi.getRowIndex().equals(doomedRowIndex))
                {
                    updatedSubQueryInputs.add(sqi);
                }
            }
            subQueryInputs = updatedSubQueryInputs;

        }
        
    }
    

   
    
    
    
}
