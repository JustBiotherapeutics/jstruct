package com.just.jstruct.structureSearch.searchDao;

import com.hfg.util.StringUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.dao.hibernate.FullStructureDAO;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.SearchParamService;
import com.just.jstruct.dao.service.SearchSubqueryService;
import com.just.jstruct.dao.service.UserLabelService;
import com.just.jstruct.dao.service.UserSearchService;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.model.Jstruct_FullStructure;
import com.just.jstruct.model.Jstruct_SearchParam;
import com.just.jstruct.model.Jstruct_SearchSubquery;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.model.Jstruct_UserLabel;
import com.just.jstruct.model.Jstruct_UserSearch;
import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.SubQueryInput;
import com.just.jstruct.structureSearch.enums.AllAnyOption;
import com.just.jstruct.structureSearch.enums.EnhancedText;
import com.just.jstruct.structureSearch.enums.EqualsOption;
import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.structureSearch.enums.HeaderField;
import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.structureSearch.enums.SequenceOption;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import static com.just.jstruct.structureSearch.enums.SubQueryType.*;
import com.just.jstruct.structureSearch.enums.YesNoOption;
import com.just.jstruct.structureSearch.subQuery.SubQuery;
import com.just.jstruct.utilities.HibernateUtil;
import com.just.jstruct.utilities.JStringUtil;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class PostgresDao {
    
    
    
    /**
     * 
     * @param sql native SQL
     * @return
     * @throws JDAOException 
     */
    public static List<Long> getStructVerIds(String sql) throws JDAOException {
        
        EntityManager em = HibernateUtil.getEntityManager();
        
        List<Long> structVerIds;
        
        try {
 
            Query q = em.createNativeQuery(sql);
            List<BigInteger> structVerIdsBigInt = q.getResultList();
            
            structVerIds = new ArrayList<>(structVerIdsBigInt.size());
            for(BigInteger bi : structVerIdsBigInt){
                structVerIds.add(bi.longValue());
            }
                   
            
        } catch (Exception e) {
            throw new JDAOException(e);
        } finally {
            em.close();
        }
        
        return structVerIds;
        
    }
    
    
    
    public static List<Jstruct_FullStructure> getFullStructuresByStructVerIds(Jstruct_User currentUser,
                                                                              Set<Long> structVerIds,
                                                                              FileStatus fileStatusRcsb, FileStatus fileStatusManual,
                                                                              boolean onlyMostCurrentVersions) throws JDAOException {
        
        EntityManager em = HibernateUtil.getEntityManager();
        List<Jstruct_FullStructure> fullStructures = null;
        
        try {
            
            List<List<Long>> sublists = CollectionUtil.chunkList(new ArrayList<>(structVerIds), 999); //postgresql doesnt have a hard limit, but above 1000 performace may get ugly

            for (List<Long> sublist : sublists) {
                
                StringBuilder hql = new StringBuilder();
                
                hql.append("SELECT fs FROM Jstruct_FullStructure fs ");
                hql.append(" WHERE fs.structVerId IN (:structVerIds) ");
                
                hql.append("    AND ");
                hql.append(FileStatus.createFileStatusClause(fileStatusRcsb, fileStatusManual, onlyMostCurrentVersions, true));
                
                Query q = em.createQuery(hql.toString());
                q.setParameter("structVerIds", sublist);
                List<Jstruct_FullStructure> subFullStructures = q.getResultList();
                
                if (CollectionUtil.hasValues(subFullStructures)) {
                    if (null == fullStructures) {
                        fullStructures = new ArrayList<>();
                    }
                    fullStructures.addAll(subFullStructures);
                }
                
            }
            
            //update structures with permissions for current user
            if(CollectionUtil.hasValues(fullStructures)){ 
                FullStructureDAO fullStructureDao = new FullStructureDAO(currentUser);
                fullStructures = fullStructureDao.setupFullStructureRoles(fullStructures);
            }
            
        } finally {
            em.close();
        }
       
        return fullStructures;
    }
    
    
    
    /**
     * 
     * @param fullQueryInput 
     * @param user 
     * @param searchName 
     * @return  
     * @throws JDAOException 
     * @throws NoSuchFieldException 
     */
    public static Jstruct_UserSearch saveCriteriaToDatabase(FullQueryInput fullQueryInput, Jstruct_User user, String searchName) throws JDAOException, NoSuchFieldException {
        
        //service classes for this method
        UserSearchService userSearchService = new UserSearchService();
        SearchSubqueryService searchSubqueryService = new SearchSubqueryService();
        SearchParamService searchParamService = new SearchParamService();
        
        //create the new UserSearch
        Jstruct_UserSearch userSearch = new Jstruct_UserSearch(user, searchName);
        userSearch.setFileStatusRcsb(fullQueryInput.getFileStatusRcsb());
        userSearch.setFileStatusManual(fullQueryInput.getFileStatusManual());
        userSearch.setQueryMode(fullQueryInput.getQueryMode());
        userSearch = userSearchService.insert(userSearch, user);
        
        //for each subquery, create the SearchSubquery and all SearchParams
        for(SubQuery subquery : fullQueryInput.getAsSubQuerys()){
            
            //create and save SearchSubquery
            Jstruct_SearchSubquery searchSubquery = new Jstruct_SearchSubquery(userSearch.getUserSearchId(), subquery.getSubQueryType(), subquery.getRowIndex());
            searchSubquery = searchSubqueryService.insert(searchSubquery);
            userSearch.addSearchSubquery(searchSubquery);
            
            //create all SearchParams, and save 'em
            List<Jstruct_SearchParam> searchParams = createSearchParamsForThisSubquery(subquery.getSubQueryType(), subquery.getSubQueryInput(), searchSubquery.getSearchSubqueryId());
            for(Jstruct_SearchParam searchParam : searchParams){
                searchParam = searchParamService.insert(searchParam);
                searchSubquery.addSearchParam(searchParam);
            }
            
        }
        return userSearch;
    }
    
    
    /**
     * 
     * @param subQueryType
     * @param subQueryInput
     * @param searchSubqueryId
     * @return
     * @throws NoSuchFieldException 
     */
    public static List<Jstruct_SearchParam> createSearchParamsForThisSubquery(SubQueryType subQueryType, SubQueryInput subQueryInput, Long searchSubqueryId) throws NoSuchFieldException {
       
        List<Jstruct_SearchParam> searchParams = new ArrayList<>();
          
       
        switch (subQueryType) {

        // GENERAL 

            case FULL_TEXT:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputFullText"), subQueryInput.getInputFullText()));
                break;

            case IDENTIFIER:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputIdentifier"), subQueryInput.getInputIdentifier()));
                break;
                
            case SEQUENCE:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("sequenceOption"), subQueryInput.getSequenceOption().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("sequenceEnhancedText"), subQueryInput.getSequenceEnhancedText().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputSequence"), subQueryInput.getInputSequence()));
                break;
                
            case EVERYTHING:
                break;
                
            case STRUCT_VER_IDS:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputStructVerIds"), subQueryInput.getInputStructVerIds()));
                break;
                
        //USER DATA
                
            case LABEL:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("allAnyLabelOption"), subQueryInput.getAllAnyLabelOption().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedLabels"), StringUtil.join(subQueryInput.getSelectedLabelIds(), ","))); //change list of labels, to comma concatinated labelIds
                break;

            case NOTE:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedNoteEnhancedText"), subQueryInput.getSelectedNoteEnhancedText().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputNote"), subQueryInput.getInputNote()));
                break;
                

        //HEADER/TITLE

            case ANY_HEADER:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedHeaderField"), subQueryInput.getSelectedHeaderField().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedAnyHeaderEnhancedText"), subQueryInput.getSelectedAnyHeaderEnhancedText().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputAnyHeader"), subQueryInput.getInputAnyHeader()));
                break;

            case TITLE:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedTitleEnhancedText"), subQueryInput.getSelectedTitleEnhancedText().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputTitle"), subQueryInput.getInputTitle()));
                break;
                
            case AUTHOR:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputAuthor"), subQueryInput.getInputAuthor()));
                break;


        //FILE INFO 

            case OWNER:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedOwnerEqualsOption"), subQueryInput.getSelectedOwnerEqualsOption().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedOwner"), subQueryInput.getSelectedOwner().getUid()));
                break;
                
            case MODEL:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("yesNoModelOption"), subQueryInput.getYesNoModelOption().name()));
                break;


        // DATES

            case DEPOSIT_DATES:
                if(subQueryInput.getDepositDateFrom() != null){
                    long fromTime = subQueryInput.getDepositDateFrom().getTime();
                    String from = JStringUtil.longToString(fromTime);
                    searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("depositDateFrom"), from));
                }
                if(subQueryInput.getDepositDateTo() != null){
                    long toTime = subQueryInput.getDepositDateTo().getTime();
                    String to = JStringUtil.longToString(toTime);
                    searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("depositDateTo"), to));
                }
                break;


        // OTHER

            case PDB_REMARK:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedPdbRemarkEnhancedText"), subQueryInput.getSelectedPdbRemarkEnhancedText().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputPdbRemark"), subQueryInput.getInputPdbRemark()));
                break;
                
            case JRNL:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputJrnl"), subQueryInput.getInputJrnl()));
                break;
                
            case COMPND:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedCompndEnhancedText"), subQueryInput.getSelectedCompndEnhancedText().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputCompnd"), subQueryInput.getInputCompnd()));
                break;
                
            case SOURCE:
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("selectedSourceEnhancedText"), subQueryInput.getSelectedSourceEnhancedText().name()));
                searchParams.add(new Jstruct_SearchParam(searchSubqueryId, makeKey("inputSource"), subQueryInput.getInputSource()));
                break;
                
            default:
                throw new AssertionError(subQueryType.name());

        }
   
        return searchParams;
    }
    
    
    private static String makeKey(String fieldName) throws NoSuchFieldException {
        Field declaredField = SubQueryInput.class.getDeclaredField(fieldName);
        return declaredField.getName();
    }
    
    
    
    
    
    
    
    /**
     * 
     * @param userSearchId
     * @param user
     * @return 
     * @throws JDAOException 
     */
    public static FullQueryInput retrieveCriteriaFromDatabase(Long userSearchId, Jstruct_User user) throws JDAOException {
        
        //service classes for this method
        UserSearchService userSearchService = new UserSearchService();
        SearchSubqueryService searchSubqueryService = new SearchSubqueryService();
        
        //get userSearch from database
        Jstruct_UserSearch userSearch = userSearchService.getByUserSearchId(userSearchId);
        
        FullQueryInput fullQueryInput = new FullQueryInput(userSearch.getFileStatusRcsb(), userSearch.getFileStatusManual(), userSearch.getQueryMode());
        
        //get all subQuerys from database, and all their searchParams
        List<Jstruct_SearchSubquery> searchSubquerys = searchSubqueryService.getByUserSearchId(userSearch.getUserSearchId());
        for(Jstruct_SearchSubquery searchSubquery : searchSubquerys){  
            SubQueryInput subQueryInput = createSubQueryInputFromSearchSubquery(searchSubquery, user);
            fullQueryInput.addSubQueryInput(subQueryInput);
            
        }
        
        return fullQueryInput;
        
    }
    
    
    /**
     * 
     * @param userSearch
     * @param user
     * @return
     * @throws JDAOException 
     */
    public static FullQueryInput transformUserSearchToFullQueryInput(Jstruct_UserSearch userSearch, Jstruct_User user) throws JDAOException {
        
        FullQueryInput fullQueryInput = new FullQueryInput(userSearch.getFileStatusRcsb(), userSearch.getFileStatusManual(), userSearch.getQueryMode());
        
        for(Jstruct_SearchSubquery searchSubquery : userSearch.getSearchSubquerys()){
            SubQueryInput subQueryInput = createSubQueryInputFromSearchSubquery(searchSubquery, user);
            fullQueryInput.addSubQueryInput(subQueryInput);
        }
        
        return fullQueryInput;
        
    }
    
    
    
    
    /**
     * 
     * @param searchSubquery
     * @param user
     * @return
     * @throws JDAOException 
     */
    public static SubQueryInput createSubQueryInputFromSearchSubquery(Jstruct_SearchSubquery searchSubquery, Jstruct_User user) throws JDAOException {
        
        //service classes for this method
        SearchParamService searchParamService = new SearchParamService();
        UserLabelService userLabelService = new UserLabelService();
        JstructUserService jstructUserService = new JstructUserService();
        
        SubQueryInput subQueryInput = new SubQueryInput(user);
        subQueryInput.setSelectedSubqueryType(searchSubquery.getSubqueryType());
        subQueryInput.setRowIndex(searchSubquery.getRowIndex());
        
        List<Jstruct_SearchParam> searchParams;
        if(searchSubquery.getSearchSubqueryId() != null){
            //if we're getting params from the database
            searchParams = searchParamService.getBySearchSubqueryId(searchSubquery.getSearchSubqueryId());
        } else {
            //or if they were already on the searchSubquery
            searchParams = searchSubquery.getSearchParams();
        }
        
        if(CollectionUtil.hasValues(searchParams)){
            for(Jstruct_SearchParam searchParam : searchParams){

                switch (searchParam.getParamKey()){

                // FULL TEXT 
                    case "inputFullText":
                        subQueryInput.setInputFullText(searchParam.getParamValue());
                        break;

                // IDENTIFIER
                    case "inputIdentifier":
                        subQueryInput.setInputIdentifier(searchParam.getParamValue());
                        break;

                // SEQUENCE
                    case "sequenceOption":
                        subQueryInput.setSequenceOption(SequenceOption.valueOf(searchParam.getParamValue()));
                        break;
                    case "sequenceEnhancedText":
                        subQueryInput.setSequenceEnhancedText(EnhancedText.valueOf(searchParam.getParamValue()));
                        break;
                    case "inputSequence":
                        subQueryInput.setInputSequence(searchParam.getParamValue());
                        break;
                        
                        
                // STRUCT_VER_IDS
                    case "inputStructVerIds":
                        subQueryInput.setInputStructVerIds(searchParam.getParamValue());
                        break;
                        

                // LABEL 
                    case "allAnyLabelOption":
                        subQueryInput.setAllAnyLabelOption(AllAnyOption.valueOf(searchParam.getParamValue()));
                        break;
                    case "selectedLabels":
                        subQueryInput.setSelectedLabels(userLabelService.getByCommaSeperatedUserLabelIds(searchParam.getParamValue()));
                        break;

                // NOTE         
                    case "selectedNoteEnhancedText":
                        subQueryInput.setSelectedNoteEnhancedText(EnhancedText.valueOf(searchParam.getParamValue()));
                        break;
                    case "inputNote":
                        subQueryInput.setInputNote(searchParam.getParamValue());
                        break;
                        
                        
                // ANY_HEADER
                    case "selectedHeaderField":
                        subQueryInput.setSelectedHeaderField(HeaderField.valueOf(searchParam.getParamValue()));
                        break;
                    case "selectedAnyHeaderEnhancedText":
                        subQueryInput.setSelectedAnyHeaderEnhancedText(EnhancedText.valueOf(searchParam.getParamValue()));
                        break;
                    case "inputAnyHeader":
                        subQueryInput.setInputAnyHeader(searchParam.getParamValue());
                        break;

                // TITLE         
                    case "selectedTitleEnhancedText":
                        subQueryInput.setSelectedTitleEnhancedText(EnhancedText.valueOf(searchParam.getParamValue()));
                        break;
                    case "inputTitle":
                        subQueryInput.setInputTitle(searchParam.getParamValue());
                        break;

                // AUTHOR
                    case "inputAuthor":
                        subQueryInput.setInputAuthor(searchParam.getParamValue());
                        break;       

                // OWNER   
                    case "selectedOwnerEqualsOption":
                        subQueryInput.setSelectedOwnerEqualsOption(EqualsOption.valueOf(searchParam.getParamValue()));
                        break;
                    case "selectedOwner":
                        subQueryInput.setSelectedOwner(jstructUserService.getByUid(searchParam.getParamValue()));
                        break;

                // MODEL
                    case "yesNoModelOption":
                        subQueryInput.setYesNoModelOption(YesNoOption.valueOf(searchParam.getParamValue()));
                        break;

                // DEPOSIT_DATES
                    case "depositDateFrom":
                        subQueryInput.setDepositDateFrom(new Date(JStringUtil.stringToLong(searchParam.getParamValue())));
                        break;
                    case "depositDateTo":
                        subQueryInput.setDepositDateTo(new Date(JStringUtil.stringToLong(searchParam.getParamValue())));
                        break;



                // PDB_REMARK  
                    case "selectedPdbRemarkEnhancedText":
                        subQueryInput.setSelectedPdbRemarkEnhancedText(EnhancedText.valueOf(searchParam.getParamValue()));
                        break;
                    case "inputPdbRemark":
                        subQueryInput.setInputPdbRemark(searchParam.getParamValue());
                        break;


                // JRNL
                    case "inputJrnl":
                        subQueryInput.setInputJrnl(searchParam.getParamValue());
                        break;

                // COMPND
                    case "selectedCompndEnhancedText":
                        subQueryInput.setSelectedCompndEnhancedText(EnhancedText.valueOf(searchParam.getParamValue()));
                        break;
                    case "inputCompnd":
                        subQueryInput.setInputCompnd(searchParam.getParamValue());
                        break;

                //SOURCE
                    case "selectedSourceEnhancedText":
                        subQueryInput.setSelectedSourceEnhancedText(EnhancedText.valueOf(searchParam.getParamValue()));
                        break;
                    case "inputSource":
                        subQueryInput.setInputSource(searchParam.getParamValue());
                        break;

                    default:
                        throw new AssertionError(searchParam.getParamKey());

                }
            }
            
        }
        
        
        
        return subQueryInput;
    }
    
    
    
    
    
    /**
     * 
     * @param userLabelId
     * @param user
     * @return
     * @throws JDAOException 
     */
    public static FullQueryInput createFullQueryInputForUserLabel(Long userLabelId, Jstruct_User user) throws JDAOException {
        
        //service classes for this method
        UserLabelService userLabelService = new UserLabelService();
        
        //get label
        Jstruct_UserLabel userLabel = userLabelService.getByUserLabelId(userLabelId);
        List<Jstruct_UserLabel> userLabels = new ArrayList<>(1);
        userLabels.add(userLabel);
        
        //create subquery
        SubQueryInput subQueryInput = new SubQueryInput(user);
        subQueryInput.setSelectedSubqueryType(SubQueryType.LABEL);
        subQueryInput.setSelectedLabels(userLabels);
        
        //create fullQueryInput (based on subquery)
        FullQueryInput fullQueryInput = new FullQueryInput(FileStatus.FILE_STATUS_ALL, FileStatus.FILE_STATUS_ALL, QueryMode.AND);
        fullQueryInput.addSubQueryInput(subQueryInput);
        
        return fullQueryInput;
    }
   
    
    
    
}
