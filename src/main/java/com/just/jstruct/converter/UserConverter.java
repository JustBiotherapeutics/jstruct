package com.just.jstruct.converter;

import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.JStringUtil;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesConverter("UserConverter")
public class UserConverter implements Converter {

    public UserConverter()
    {}


    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        //value should be staff id
        if (value == null || value.length() == 0) {
            return null;
        }

        Jstruct_User u = new Jstruct_User();
        try {
            JstructUserService userService = new JstructUserService();
            u = userService.getById(JStringUtil.stringToLong(value));
        } catch (Exception e) {
            FacesMessage message = new FacesMessage();
            //message.setDetail("Person convertion error with username/staffid: " + value);
            message.setDetail("unable to find user match for UserId: " + value);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ConverterException(message);
        }
        return u;

    }




    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        
        if (value == null) {
            return "";
        }

        if (!(value instanceof Jstruct_User)) {
            throw new IllegalArgumentException("Cannot convert non-Jstruct_User object.");
        }
        
        Jstruct_User user = (Jstruct_User) value;
        if (user.getUserId()== null) {
            throw new IllegalArgumentException("Cannot convert Jstruct_User object with null Id.");
        }

        return Long.toString(user.getUserId());

    }


}
