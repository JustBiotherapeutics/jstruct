package com.just.jstruct.sequenceProcessing;

import com.hfg.util.StackTraceUtil;
import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.Structure;
import com.just.jstruct.dao.service.ImportJobService;
import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.dao.service.SequencesAtomService;
import com.just.jstruct.dao.service.SequencesSeqresService;
import com.just.jstruct.dao.service.StructFileService;
import com.just.jstruct.dao.service.StructVersionService;
import com.just.jstruct.enums.JobStatus;
import com.just.jstruct.enums.JobType;
import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.exception.JException;
import com.just.jstruct.model.Jstruct_ImportJob;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.structureImport.ImportService;
import com.just.jstruct.structureImport.JImportUtils;
import com.just.jstruct.utilities.AuthUtil;
import com.just.jstruct.utilities.JEmailUtil;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SequenceUpdate {

    
    
    private static final Logger LOGGER = Logger.getLogger(AuthUtil.class.getPackage().getName());
    

    private Jstruct_User mUser;



    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################


    public SequenceUpdate(Jstruct_User inUser) {
        init(null, inUser);
    }

    public SequenceUpdate(Long inUserId) {
        init(inUserId, null);
    }

    public SequenceUpdate() {
        init(null, null);
    }



    private void init(Long inUserId, Jstruct_User inUser)  {

        if(null != inUser) {
            //if a user is passed in, use it. done.
            mUser = inUser;
        } else if (null != inUserId) {
            try {
                mUser = JstructUserService.getUserFromCache(inUserId);
            } catch (JDAOException ex) {
                //nothing. user will remain null for now.. catch it in the processing (todo - fix this bad programming)
            }

        } else {
            //else we get system user
            try {
                mUser = JstructUserService.getUserFromCache(Jstruct_User.SYSTEM_UID);
            } catch (JDAOException ex) {
                //nothing. user will remain null for now.. catch it in the processing (todo - fix this bad programming)
            } 

        }


    }





    //###########################################################################
    // PUBLIC METHODS
    //###########################################################################

    //------------------------------------------------------------------------------------------------------------------
    /**
     * This is the primary public method to initiate a update all/manual chain job
     *
     * @param jobType
     */
    public void processSequenceUpdates(JobType jobType)  {

        ImportJobService importJobService = new ImportJobService();
        

        //setup for some of the jobtypes
        try {

            Jstruct_ImportJob importJob = new Jstruct_ImportJob(mUser, jobType, JobStatus.QUERYING);
            importJobService.add(importJob);

            List<Long> structVerIds;

            switch (jobType) {
                case UPDATE_ALL_CHAINS:
                    //get a set of all struct_ver_id's in the entire database, regardless of status, version, etc
                    structVerIds = getAllStructVerIds();
                    break;
                    
                case UPDATE_MANUAL_CHAINS:
                    //get a set of all struct_ver_id's form manually uploaded files (regardless of version, status...)
                    structVerIds = getManualStructVerIds();
                    break;
                    
                case UPDATE_MISSING_CHAINS:
                    //get struct_ver_ids that are missing seq_residue OR atom_grouping records
                    structVerIds = getStructVerIdsMissingSequenceData();
                    break;
                        
                default:
                    throw new InputMismatchException("Invalid JobType for processAllRcsbFileChanges: " + jobType.name());
                    
            }
            
            importJob.setImportStatus(JobStatus.PROCESSING);
            importJobService.update(importJob);
            
            //do the update!
            List<Exception> exceptions = updateChains(importJob, structVerIds);
            
            //update the import_job record with SUCCESS status, then send completed success email
            updateImportJobSuccessful(importJob);
            JEmailUtil.sendJobCompleteEmail(importJob, null, mUser, null, exceptions); //complete, and successful :)
            

        } catch (JDAOException | JException ex) {
            //if we catch an error here, log and send email to admin.
            ex.printStackTrace();
            LOGGER.log(Level.SEVERE, "Error processing RCSB file changes", ex);
            JEmailUtil.sendJobErrorEmail(ex);
        }


    }

    











    //###########################################################################
    // PRIVATE METHODS
    //###########################################################################

    
    
    
    /**
     *
     * @param importJob
     * @return
     * @throws JException
     */
    private List<Exception> updateChains(Jstruct_ImportJob importJob, List<Long> structVerIds) throws JException {

        StructFileService structFileService = new StructFileService();
        
        List<Exception> exceptions = new ArrayList<>();

        try {

            if (CollectionUtil.hasValues(structVerIds)) {
                updateImportJobUniqueSourceCount(importJob, structVerIds.size());

                int loopErrorCount = 0;
                int loopErrorMax = JImportUtils.maxErrorsBeforeImportFail(structVerIds.size(), 10);

                for (Long structVerId : structVerIds) {
                    
                    try {
                        
                        //create the structureObject from the existing database record (struct_file) - then update all the chains/atom_groupings
                        Structure structureObj = structFileService.getAsStructure(structVerId);
                       
                        JImportUtils.updateChainsForStructure(structureObj, structVerId);

                        importJob.setCountUpdated(importJob.getCountUpdated() + 1);
                        updateImportJobCountImported(importJob, importJob.getCountUpdated() );

                    //} catch (JDAOException | JException ex) {
                    } catch (Throwable ex) {
                        
                        ex.printStackTrace();
                        loopErrorCount++; 
                        exceptions.add(new JException(ex));
                        
                        ImportService.persistImportWarning(importJob, mUser, "structVerId: " + structVerId, "Error during processing: " + ex.getMessage(), ex);

                        if(loopErrorCount > loopErrorMax){
                            //if there are too many errors from the loops we may need to kill the import and send a fail message
                            throw new JException("Too many file errors have occurred during the " + JobType.UPDATE_ALL_CHAINS + " process.");
                        }
                    } 

                }
            } else {
                updateImportJobUniqueSourceCount(importJob, 0);
            }

            //update the file_import_job record import completed timestamp
            updateImportJobImportDate(importJob);

        } catch (JDAOException ex) {

            //SQLUtil.close(conn);
            uhOhJobImportFailedSadFace(importJob, ex);
            JEmailUtil.sendJobCompleteEmail(importJob, null, mUser, ex, exceptions); //complete, but failed :(

        }

        return exceptions;
    }
    
    
    


    /**
     * 
     * @param importJob
     * @param ex
     * @throws JException 
     */
    private void uhOhJobImportFailedSadFace (Jstruct_ImportJob importJob, Exception ex) throws JException {
        try {
            
            ImportJobService importJobService = new ImportJobService();
          
            importJob.setFailureMessage("ERROR MESSAGE: " + ex.getMessage() + " \n" + "STACK TRACE: \n" + StackTraceUtil.getExceptionStackTrace(ex));
            importJob.setRunEnd(new Date());
            importJob.setImportStatus(JobStatus.FAILED);
            importJobService.update(importJob);

        } catch (JDAOException jdaoex) {
            throw new JException("Database Error attempting to set Import Job to FAILED", jdaoex);
        } 
    }




    


    /**
     * @throws SQLException
     */
    private List<Long> getAllStructVerIds() throws JDAOException {
        StructVersionService structVersionService = new StructVersionService();
        List<Long> allStructVerIds = structVersionService.getAllStructVerIds();
        return allStructVerIds;
        
    }
    
    
    /**
     * @throws SQLException
     */
    private List<Long> getManualStructVerIds() throws JDAOException {
        StructVersionService structVersionService = new StructVersionService();
        List<Long> allStructVerIds = structVersionService.getManualStructVerIds();
        return allStructVerIds;
        
    }

    /**
     * list of structVerIds that have no entries in seq_residue OR atom_grouping
     * @return
     * @throws JDAOException 
     */
    private List<Long> getStructVerIdsMissingSequenceData() throws JDAOException {

        StructVersionService structVersionService = new StructVersionService();
        SequencesSeqresService sequencesSeqresService = new SequencesSeqresService();
        SequencesAtomService sequencesAtomService = new SequencesAtomService();
        
        List<Long> allStructVerIds = structVersionService.getAllStructVerIds();
        List<Long> seqresStructVerIds = sequencesSeqresService.getStructVerIds();
        List<Long> atomStructVerIds = sequencesAtomService.getStructVerIds();
        
        // get structVerIds that are in BOTH seq_residue and atom_grouping
        seqresStructVerIds.retainAll(atomStructVerIds);
        
        // remove from our list of all structVerIds the structVerIds that we already have full chain info for
        allStructVerIds.removeAll(seqresStructVerIds);
        
        return allStructVerIds;
        
    }









    /**
     * update a Jstruct_ImportJob as successful
     * @param inImportJob
     * @throws JDAOException 
     */
    private void updateImportJobSuccessful(Jstruct_ImportJob inImportJob) throws JDAOException {
        ImportJobService importJobService = new ImportJobService();
        inImportJob.setRunEnd(new Date());
        inImportJob.setImportStatus(JobStatus.SUCCESS);
        importJobService.update(inImportJob);
    }
    
    
    


    /**
     *
     * @param inImportJob
     * @param uniqueSourceCount
     * @throws SQLException
     */
    private void updateImportJobUniqueSourceCount(Jstruct_ImportJob inImportJob, Integer uniqueSourceCount) throws JDAOException {
        
        ImportJobService importJobService = new ImportJobService();
        
        inImportJob.setCountUniqueSource(uniqueSourceCount);
        importJobService.update(inImportJob);
        
    }


    /**
     *
     * @param inImportJob
     * @param inImportDate
     * @throws SQLException
     */
    private void updateImportJobImportDate(Jstruct_ImportJob inImportJob) throws JDAOException {
        
        ImportJobService importJobService = new ImportJobService();
        
        inImportJob.setImportEnd(new Date());
        importJobService.update(inImportJob);
        
    }


    /**
     * 
     * @param inImportJob
     * @param inImportCount
     * @throws JDAOException 
     */
    private void updateImportJobCountImported(Jstruct_ImportJob inImportJob, int inImportCount) throws JDAOException {
        
        ImportJobService importJobService = new ImportJobService();
        
        inImportJob.setCountImported(inImportCount);
        importJobService.update(inImportJob);
        
    }













}
