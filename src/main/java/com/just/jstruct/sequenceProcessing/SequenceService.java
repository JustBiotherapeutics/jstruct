package com.just.jstruct.sequenceProcessing;

import com.hfg.util.collection.CollectionUtil;
import com.just.bio.structure.AtomGroup;
import com.just.bio.structure.Chain;
import com.just.bio.structure.SeqResidue;
import com.just.bio.structure.Structure;
import com.just.jstruct.model.Jstruct_AtomGrouping;
import com.just.jstruct.model.Jstruct_Chain;
import com.just.jstruct.model.Jstruct_SeqResidue;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class SequenceService {
    
    
    private Long mStructVerId;
    private Structure mStructureObj;

    
    
    
    //###########################################################################
    // CONSTRUCTORS
    //###########################################################################

    public SequenceService(Long inStructVerId, Structure inStructureObj) {
        this.mStructVerId = inStructVerId;
        this.mStructureObj = inStructureObj;
    }
    
    
    
    
    
    //###########################################################################
    // PUBLIC METHODS
    //###########################################################################


    /**
     *
     * @return
     */
    public List<Jstruct_Chain> createChains() {

        if(null==mStructureObj
           || null==mStructureObj.getAtomicCoordinates()
           || !CollectionUtil.hasValues(mStructureObj.getAtomicCoordinates().getModels())){
            return null;
        }


        List<Jstruct_Chain> chains = new ArrayList<>(25);

        for (Chain chainObj : mStructureObj.getAtomicCoordinates().getModels().get(0).getChains()) {

            Jstruct_Chain chain = createChain(chainObj);
            chains.add(chain);

        }

        return chains;
    }
    
    
    
    //###########################################################################
    // PRIVATE METHODS
    //###########################################################################



    private Jstruct_Chain createChain(Chain chainObj)  {

        Jstruct_Chain chain = new Jstruct_Chain();

        chain.setStructVerId(mStructVerId);
        chain.setName(chainObj.getId());  // or mStructureObj.getPdbIdentifier() + "." + chainObj.getChainId()
        chain.setType(chainObj.getType().name());
        chain.setSeqTheoretical(chainObj.getTheoreticalSeq());
        chain.setResidueCount(chainObj.getSeqResidues().size());

        if(CollectionUtil.hasValues(chainObj.getAtomGroups())){
            for(AtomGroup atomGrouping : chainObj.getAtomGroups()){
                chain.addAtomGrouping(new Jstruct_AtomGrouping(chainObj, 0L));
            }
        }
        
        if(CollectionUtil.hasValues(chainObj.getSeqResidues())){
            for(SeqResidue seqResidue : chainObj.getSeqResidues()){
                chain.addSeqResidue(new Jstruct_SeqResidue(seqResidue, 0L));
            }
        }

        return chain;

    }
    
    
    
}
