package com.just.jstruct.autocomplete;

import com.just.jstruct.dao.service.JstructUserService;
import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * ajax auto complete for Jstruct_User
 * used in userLookup.xhtml
 */ 
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------

@ManagedBean(name="userAutoComplete")
@RequestScoped
public class UserAutoComplete implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    private final JstructUserService userService = new JstructUserService();
    
    

    public UserAutoComplete() {
    }



    
    
    //called from the user autocomplete component in the ui each time a key is pressed
    public List<Jstruct_User> byGeneralCriteria(String query) {
        try {
            List<Jstruct_User> possibleMatches = userService.getUsersByGeneralCriteria(query);
            return possibleMatches;
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
        return null;
    }
    


    
}