package com.just.jstruct.exception;

/**
 * Exceptions that happen during database access
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JDAOException extends JstructBaseException {

    
    
    /* constructors */
    
    
    public JDAOException(String inMessage) {
        super(inMessage);
    }

    public JDAOException(String inMessage, Throwable inException) {
        super(inMessage, inException);
    }

    public JDAOException(Throwable inException) {
        super(inException);
    }
    
    

}
