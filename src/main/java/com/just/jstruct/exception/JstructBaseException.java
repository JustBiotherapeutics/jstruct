package com.just.jstruct.exception;

/**
  * Base exception for most exception generated within JStruct.
  */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public abstract class JstructBaseException extends Exception
{
   //---------------------------------------------------------------------------
   public JstructBaseException(String inMessage)
   {
      super(inMessage);
   }

   //---------------------------------------------------------------------------
   public JstructBaseException(String inMessage, Throwable inException)
   {
      super(inMessage, inException);
   }

   //---------------------------------------------------------------------------
   public JstructBaseException(Throwable inException)
   {
      super(inException);
   }

}