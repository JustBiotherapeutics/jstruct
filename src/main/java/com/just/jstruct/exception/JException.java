package com.just.jstruct.exception;




/**
  * handles most general exceptions generated within JStruct.
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JException extends JstructBaseException
{
   //---------------------------------------------------------------------------
   public JException(String inMessage)
   {
      super(inMessage);
   }

   //---------------------------------------------------------------------------
   public JException(String inMessage, Throwable inException)
   {
      super(inMessage, inException);
   }

   //---------------------------------------------------------------------------
   public JException(Throwable inException)
   {
      super(inException);
   }

}