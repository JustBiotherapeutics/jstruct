package com.just.jstruct.exception;




/**
 * hey, you don't have access to this page!
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class NoAccessException extends Exception {

    
    
    //noaccessexception specific datamembers
    private String pageDesc = "";
    private String data = "";

    
    
    
   
    
    

    public NoAccessException(String msg, String pageDesc, String data) {
        super(msg);
        this.pageDesc = pageDesc;
        this.data = data;
    }
    
    
    
    public String getData() { return data; }
    public void setData(String data) { this.data = data; }

    public String getPageDesc() { return pageDesc; }
    public void setPageDesc(String pageDesc) { this.pageDesc = pageDesc; }

    
    
    
    
    
}
