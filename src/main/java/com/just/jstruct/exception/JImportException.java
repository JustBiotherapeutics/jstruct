package com.just.jstruct.exception;




/**
 * Exception to be thrown when something bad bad bad happens during an import..
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
public class JImportException extends JstructBaseException {


    public JImportException(String inMessage) {
        super(inMessage);
    }

    public JImportException(String inMessage, Throwable inException) {
        super(inMessage, inException);
    }

    public JImportException(Throwable inException) {
        super(inException);
    }


}
