package com.just.jstruct.justComponents;

import com.just.jstruct.model.Jstruct_User;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.structureSearch.enums.SubQueryGroup;
import com.just.jstruct.structureSearch.enums.SubQueryType;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

/*
 * select/dropdown class for subqueryTypeSelect.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "subqueryTypeSelect")
@ViewScoped
public class SubqueryTypeSelect implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ManagedProperty(value="#{sessionBean}")
    private SessionBean sessionBean;
    
    private Jstruct_User currentUser;

    private List<SelectItem> subqueryTypes;
    
    
    public List<SelectItem> getSubqueryTypes() {
        return subqueryTypes;
    }

    
    
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    
    
    public SubqueryTypeSelect() {
        
    }
    
    
    
    @PostConstruct
    public void init() {
        try {
            currentUser = sessionBean.getUser();
            
            Map<SubQueryGroup, List<SubQueryType>> groupedSubQueryKeyMap = SubQueryType.groupedSubQueryTypeMap(currentUser);

            subqueryTypes = new ArrayList<>(groupedSubQueryKeyMap.size());

            for (Map.Entry<SubQueryGroup, List<SubQueryType>> entry : groupedSubQueryKeyMap.entrySet()) {
                SubQueryGroup subQueryGroup = entry.getKey();

                SelectItemGroup group = new SelectItemGroup(subQueryGroup.displayName());
                List<SelectItem> selectItems = new ArrayList<>(entry.getValue().size());

                for (SubQueryType subQueryKey : entry.getValue()) {
                    selectItems.add(new SelectItem(subQueryKey, subQueryKey.getDisplayName(), subQueryKey.getDescription()));
                }

                SelectItem[] items = new SelectItem[selectItems.size()];
                group.setSelectItems(selectItems.toArray(items));

                subqueryTypes.add(group);

            }

        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
        
    }


}
