package com.just.jstruct.justComponents;

import com.just.jstruct.structureSearch.enums.EqualsOption;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/*
 * select/dropdown class for fileStatusSelect.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "equalsOptionSelect")
@ViewScoped
public class EqualsOptionSelect implements Serializable {

   
    private static final long serialVersionUID = 1L;
    
    
    private List<SelectItem> equalsOptionItems;

    public List<SelectItem> getEqualsOptionItems() {
        return equalsOptionItems;
    }
    
    
    
    
    public EqualsOptionSelect() {
        try {
            
            List<EqualsOption> equalsOption = EqualsOption.equalsOptions();
            
            equalsOptionItems = new ArrayList<>(equalsOption.size());
            for (EqualsOption eo : equalsOption) {
                equalsOptionItems.add(new SelectItem(eo, eo.getDisplayName()));
            }
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
