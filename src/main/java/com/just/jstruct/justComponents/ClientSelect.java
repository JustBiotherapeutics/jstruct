package com.just.jstruct.justComponents;

import com.just.jstruct.dao.service.ClientService;
import com.just.jstruct.model.Jstruct_Client;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/*
 * select/dropdown class for client.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "clientSelect")
@ViewScoped
public class ClientSelect implements Serializable {

   
    private static final long serialVersionUID = 1L;
    
    private final ClientService clientService = new ClientService();
    
    private List<Jstruct_Client> clients;

    
    
    
    public List<Jstruct_Client> getAllClients() {
        return clients;
    }
    
    
    
    
    public ClientSelect() {
        try {
            
            clients = clientService.getAll();

        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
