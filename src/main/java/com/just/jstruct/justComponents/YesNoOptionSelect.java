package com.just.jstruct.justComponents;

import com.just.jstruct.structureSearch.enums.YesNoOption;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/*
 * select/dropdown class for yesNoOptionSelect.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "yesNoOptionSelect")
@ViewScoped
public class YesNoOptionSelect implements Serializable {

   
    private static final long serialVersionUID = 1L;
    
    
    private List<SelectItem> yesNoOptionItems;

    public List<SelectItem> getYesNoOptionItems() {
        return yesNoOptionItems;
    }
    
    
    
    
    public YesNoOptionSelect() {
        try {
            
            List<YesNoOption> yesNoOptions = YesNoOption.yesNoOptions();
            
            yesNoOptionItems = new ArrayList<>(yesNoOptions.size());
            for (YesNoOption et : yesNoOptions) {
                yesNoOptionItems.add(new SelectItem(et, et.getDisplayName()));
            }
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
