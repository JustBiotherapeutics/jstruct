package com.just.jstruct.justComponents;

import com.just.jstruct.structureSearch.enums.EnhancedText;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/*
 * select/dropdown class for fileStatusSelect.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "enhancedTextSelect")
@ViewScoped
public class EnhancedTextSelect implements Serializable {

   
    private static final long serialVersionUID = 1L;
    
    
    private List<SelectItem> enhancedTextItems;

    public List<SelectItem> getEnhancedTextItems() {
        return enhancedTextItems;
    }
    
    
    
    
    public EnhancedTextSelect() {
        try {
            
            List<EnhancedText> enhancedTexts = EnhancedText.enhancedTexts();
            
            enhancedTextItems = new ArrayList<>(enhancedTexts.size());
            for (EnhancedText et : enhancedTexts) {
                enhancedTextItems.add(new SelectItem(et, et.getDisplayName()));
            }
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
