package com.just.jstruct.justComponents;

import com.just.jstruct.structureSearch.enums.SequenceOption;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/*
 * select/dropdown class for sequenceOptionSelect.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "sequenceOptionSelect")
@ViewScoped
public class SequenceOptionSelect implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    private List<SelectItem> sequenceOptionItems;

    public List<SelectItem> getSequenceOptionItems() {
        return sequenceOptionItems;
    }
    
    
    
    
    public SequenceOptionSelect() {
        try {
            
            List<SequenceOption> sequenceOptions = SequenceOption.allSequenceOptions();
            
            sequenceOptionItems = new ArrayList<>(sequenceOptions.size());
            for (SequenceOption so : sequenceOptions) {
                sequenceOptionItems.add(new SelectItem(so, so.getDisplayName(), so.getDescription()));
            }
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
