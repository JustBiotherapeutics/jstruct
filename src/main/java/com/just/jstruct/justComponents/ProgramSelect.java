package com.just.jstruct.justComponents;

import com.just.jstruct.dao.service.ProgramService;
import com.just.jstruct.model.Jstruct_Program;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/*
 * select/dropdown class for program.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "programSelect")
@ViewScoped
public class ProgramSelect implements Serializable {

   
    private static final long serialVersionUID = 1L;
    
    private ProgramService programService = new ProgramService();
    
    private List<Jstruct_Program> programs;

    
    
    
    public List<Jstruct_Program> getPrograms() {
        return programs;
    }
    
    
    public ProgramSelect() {
        try {
            
            programs = programService.getAll();

        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
