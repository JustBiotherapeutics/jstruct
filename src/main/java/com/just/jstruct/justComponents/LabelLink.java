package com.just.jstruct.justComponents;

import com.just.jstruct.exception.JDAOException;
import com.just.jstruct.pojos.SessionBean;
import com.just.jstruct.structureSearch.FullQueryInput;
import com.just.jstruct.structureSearch.searchDao.PostgresDao;
import com.just.jstruct.utilities.FacesUtil;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Backing bean for Labels - allowing them to link to Search Results
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name="labelLink") 
@ViewScoped   
public class LabelLink implements Serializable {
 
    private static final long serialVersionUID = 1L;
    
    @ManagedProperty("#{sessionBean}")
    private SessionBean sessionBean;
 
    
    public SessionBean getSessionBean() {return sessionBean;}
    public void setSessionBean(SessionBean sessionBean) {this.sessionBean = sessionBean;}
    
    
    /* constructor */
    public LabelLink() {}

    
    
   
            
            
   /**
     * based on the selected userLabelId, create da fullQueryInupt data and redirect to the search(results) page
     * @param userLabelId
     * @return 
     */
    public String userLabelSearch(Long userLabelId){
        try {
            
            //cleanup existing b/4 our next search
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("searchBean", null);  
      
            //get search data from DB for the selected userSearchId, and store it on the session for the search (results) page
            FullQueryInput fullQueryInput = PostgresDao.createFullQueryInputForUserLabel(userLabelId, sessionBean.getUser());
            FacesUtil.storeOnSession("fullQueryInput", fullQueryInput);

        } catch (JDAOException e) {
            HijackError.gotoErrorPage(e);
        }
        //redirect to search [results] page
        return "/structure_search.xhtml?faces-redirect=true";
    }
    
    
    
    
}
