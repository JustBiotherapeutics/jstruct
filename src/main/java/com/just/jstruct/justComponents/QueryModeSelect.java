package com.just.jstruct.justComponents;

import com.just.jstruct.structureSearch.enums.QueryMode;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/*
 * select/dropdown class for queryModeSelect.xhtml  (AND, OR)
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "queryModeSelect")
@ViewScoped
public class QueryModeSelect implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    private List<SelectItem> queryModeItems;

    public List<SelectItem> getQueryModeItems() {
        return queryModeItems;
    }
    
    
    
    
    public QueryModeSelect() {
        try {
            
            List<QueryMode> queryModes = QueryMode.getAsList();
            
            queryModeItems = new ArrayList<>(queryModes.size());
            for (QueryMode fs : queryModes) {
                queryModeItems.add(new SelectItem(fs, fs.toString()));
            }
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
