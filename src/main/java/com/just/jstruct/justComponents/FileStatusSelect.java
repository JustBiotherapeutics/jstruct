package com.just.jstruct.justComponents;

import com.just.jstruct.structureSearch.enums.FileStatus;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/*
 * select/dropdown class for fileStatusSelect.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "fileStatusSelect")
@ViewScoped
public class FileStatusSelect implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    private List<SelectItem> fileStatusItems;

    public List<SelectItem> getFileStatusItems() {
        return fileStatusItems;
    }
    
    
    
    
    public FileStatusSelect() {
        try {
            
            List<FileStatus> fileStatuss = FileStatus.fileStatuss();
            
            fileStatusItems = new ArrayList<>(fileStatuss.size());
            for (FileStatus fs : fileStatuss) {
                fileStatusItems.add(new SelectItem(fs, fs.getDisplayName()));
            }
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
