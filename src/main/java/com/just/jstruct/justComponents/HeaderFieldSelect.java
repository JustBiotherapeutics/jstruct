package com.just.jstruct.justComponents;

import com.just.jstruct.structureSearch.enums.HeaderField;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/*
 * select/dropdown class for headerFieldSelect.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "headerFieldSelect")
@ViewScoped
public class HeaderFieldSelect implements Serializable {

    private static final long serialVersionUID = 1L;
    
    
    private List<SelectItem> headerFieldItems;

    public List<SelectItem> getHeaderFieldItems() {
        return headerFieldItems;
    }
    
    
    
    
    public HeaderFieldSelect() {
        try {
            
            List<HeaderField> headerFields = HeaderField.headerFields();
            
            headerFieldItems = new ArrayList<>(headerFields.size());
            for (HeaderField hf : headerFields) {
                headerFieldItems.add(new SelectItem(hf, hf.getDisplayName()));
            }
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
