package com.just.jstruct.justComponents;

import com.just.jstruct.structureSearch.enums.AllAnyOption;
import com.just.jstruct.utilities.HijackError;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

/*
 * select/dropdown class for yesNoOptionSelect.xhtml
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@ManagedBean(name = "allAnyOptionSelect")
@ViewScoped
public class AllAnyOptionSelect implements Serializable {

   
    private static final long serialVersionUID = 1L;
    
    
    private List<SelectItem> allAnyOptionItems;

    public List<SelectItem> getAllAnyOptionItems() {
        return allAnyOptionItems;
    }
    
    
    
    
    public AllAnyOptionSelect() {
        try {
            
            List<AllAnyOption> allAnyOptions = AllAnyOption.allAnyOptions();
            
            allAnyOptionItems = new ArrayList<>(allAnyOptions.size());
            for (AllAnyOption opt : allAnyOptions) {
                allAnyOptionItems.add(new SelectItem(opt, opt.getDisplayName()));
            }
        } catch (Exception e) {
            HijackError.gotoErrorPage(e);
        }
    }
    

    
    
    
}
