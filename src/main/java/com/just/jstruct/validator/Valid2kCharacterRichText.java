package com.just.jstruct.validator;

import com.hfg.util.StringUtil;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.jsoup.Jsoup;

/**
 * custom validator - validate that the text of a rich text editor field is only
 * 2000 characters after all formatting is removed
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("valid2kCharacterRichText")
public class Valid2kCharacterRichText implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the fully formatted string
        String formattedText = (String) value;

        if (StringUtil.isSet(formattedText)) {

            String unformattedText = Jsoup.parse(formattedText).text();

            if (StringUtil.isSet(unformattedText)) {

                if (unformattedText.length() > 1999) {
                    
                    FacesMessage message = new FacesMessage();
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    message.setDetail("The unformatted text evaluates to more than 2000 characters.");
                    throw new ValidatorException(message);
                    
                }

            }

            

        }

    }

}
