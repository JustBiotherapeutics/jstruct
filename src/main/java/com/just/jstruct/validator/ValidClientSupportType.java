package com.just.jstruct.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * custom validator - validate the text (string) entered by the user
 *                    evaluates to one of a few expected values (none | optional | required)
 *
 * example usage:
	<h:inputText id="clientSupportTypeInput" value="#{Bean.clientSupportTypeInput}" >
		<f:validator validatorId="validClientSupportType" />
	</h:inputText>
	<h:message for="clientSupportTypeInput"/>
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("validClientSupportType")
public class ValidClientSupportType implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the string value the user entered as the number
        String param = (String) value;
        String val = param==null?"":param;
        

        //... and check if it's invalid
        if (!val.equalsIgnoreCase("none") && !val.equalsIgnoreCase("optional") && !val.equalsIgnoreCase("required")){

            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setDetail("The entered value '" + val + "' is invalid. It must be one of: none | optional | required");
            throw new ValidatorException(message);

        }

    }
    
    
}
