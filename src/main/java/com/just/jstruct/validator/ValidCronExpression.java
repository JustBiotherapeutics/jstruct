package com.just.jstruct.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.quartz.CronExpression;

/**
 * custom validator - validate the text (string) entered by the user
 *                    evaluates to a valid cron expression 
 *
 * example usage:
	<h:inputText id="cronAttempt" value="#{Bean.cronAttempt}" >
		<f:validator validatorId="ValidCronExpression" />
	</h:inputText>
	<h:message for="cronAttempt"/>
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("validCronExpression")
public class ValidCronExpression implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the string value the user entered as the number
        String val = (String) value;
        String cronAttempt = val==null?"":val;
        

        //... and check if it's invalid
        if (!CronExpression.isValidExpression(cronAttempt)){
            
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setDetail("'" + cronAttempt + "' is not a valid Quartz cron expression");
            throw new ValidatorException(message);

        }

    }
    
    
}
