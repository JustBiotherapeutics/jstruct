package com.just.jstruct.validator;

import com.just.jstruct.utilities.JStringUtil;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * custom validator - validate the text (string) entered by the user
 *                    evaluates to one or more of: "none", "Contributor", "Admin" separated by comma or space
 *
 * example usage:
	<h:inputText id="defaultRoleInput" value="#{Bean.defaultRoleInput}" >
		<f:validator validatorId="validDefaultRole" />
	</h:inputText>
	<h:message for="defaultRoleInput"/>
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("validDefaultRole")
public class ValidDefaultRole implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the string value the user entered as the number
        String param = (String) value;
        
        String[] roles = JStringUtil.splitOnSpaceAndComma(param);
        
        for(String role : roles){
            if(!isValidRole(role)){
                
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setDetail("The entered value '" + param + "' is invalid. It must be one or more of: 'none', 'Contributor', 'Admin'");
                throw new ValidatorException(message);
                
            }
        }
   

    }
    
    
    private boolean isValidRole(String possibleRole){    
        if("none Contributor Admin".contains(possibleRole)){
            return true;
        }
        return false;
    }
    
    
}
