package com.just.jstruct.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * custom validator - validate the text (string) entered by the user
 *                    evaluates to one of: 'none', 'Read', or 'Write'"
 *
 * example usage:
	<h:inputText id="defaultStructurePermissionInput" value="#{Bean.defaultStructurePermissionInput}" >
		<f:validator validatorId="validDefaultStructurePermission" />
	</h:inputText>
	<h:message for="defaultStructurePermissionInput"/>
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("validDefaultStructurePermission")
public class ValidDefaultStructurePermission implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the string value the user entered as the number
        String param = (String) value;

        if(!param.equalsIgnoreCase("none") && !param.equalsIgnoreCase("read") && !param.equalsIgnoreCase("write")){
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setDetail("The entered value '" + param + "' is invalid. It must be one of: 'none', 'Read', or 'Write'");
            throw new ValidatorException(message);
        } 

      
    }
    
    

    
}
