package com.just.jstruct.validator;


import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * ensure a string can be broken into one a valid URL 
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("validUrl")
public class ValidUrl implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the value entered in the field, and check if it starts with http
        String val = (String) value;
        String textEntered = val==null?"":val.trim();
        
        
        if (!textEntered.startsWith("http")) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setDetail("URL must start with http:// or https://");
            throw new ValidatorException(message);
        }
        
        if (textEntered.contains(" ")) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setDetail("The URL cannot contain any spaces");
            throw new ValidatorException(message);
        }

    }



}



