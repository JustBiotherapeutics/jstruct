package com.just.jstruct.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * custom validator - validate the text (string) entered by the user is an integer
 *
 * example usage:
	<h:inputText id="numb" value="#{Bean.numb}" >
		<f:validator validatorId="validInteger" />
	</h:inputText>
	<h:message for="numb"/>
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("validInteger")
public class ValidInteger implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the string value the user entered as the number
        String numberAttempt = (String) value;

        try {
            //ensure it can be converted, throw ValidatorException if not
            Integer.parseInt(numberAttempt);
        } catch (Exception e) {
            FacesMessage message = new FacesMessage();
            message.setDetail("Invalid value, must be an integer: " + numberAttempt);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
    
    
}
