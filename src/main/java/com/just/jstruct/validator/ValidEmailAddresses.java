package com.just.jstruct.validator;

import com.just.jstruct.utilities.JEmailUtil;
import com.just.jstruct.utilities.JStringUtil;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * ensure a string can be broken into one or more email addresses
 */ 
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("validEmailAddresses")
public class ValidEmailAddresses implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the value entered in the field, and convert it to a list we can iterate thru
        String val = (String) value;
        String textEntered = val==null?"":val;
        List <String> emails = JStringUtil.stringToList(textEntered, ",");
        List <String> invalidEmails = new ArrayList();
        
        //validate each email
        for (String email : emails){
            //if it doesnt match, add it ot our invalid email list
            if (!JEmailUtil.isValidEmailFormat(email)){
                invalidEmails.add(email.trim());
            }
        }

        //if there are any emails in our invalid list, they are invalid!
        if (!invalidEmails.isEmpty()){
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setDetail("Invalid email format: " + JStringUtil.listToString(invalidEmails, ", "));
            throw new ValidatorException(message);

        }
    }


   


}



