package com.just.jstruct.validator;

import com.just.jstruct.utilities.JEmailUtil;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * ensure a string evaluates to only one email address
 */
// -------------------------------------------------------------------------------------
// Copyright (c) 2023 Just-Evotec Biologics, Inc.
// All rights reserved
// See the LICENSE.txt file included with this library for license terms and conditions.
// -------------------------------------------------------------------------------------
@FacesValidator("validEmailAddress")
public class ValidEmailAddress implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        //grab the value entered in the field...
        String val = (String) value;
        String email = val==null?"":val;
        

        //... and check if it's invalid
        if (!JEmailUtil.isValidEmailFormat(email)){
            
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setDetail("Invalid email format: " + email);
            throw new ValidatorException(message);

        }
    }


    


}



