package com.just.jstruct;

import com.hfg.util.collection.CollectionUtil;
import com.just.jstruct.exception.JException;
import com.just.jstruct.structureImport.rcsbRestImport.RcsbRestHelper;
import com.just.jstruct.utilities.JDateUtil;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 */
public class RcsbRestHelperJsonTest
{

    @BeforeClass
    public static void setUpClass() { }

    @AfterClass
    public static void tearDownClass() { }

    @Before
    public void setUp() { }

    @After
    public void tearDown() { }


    




    @Test
    public void testQueryForAllCurrentPdbIds() throws JException
    {
        RcsbRestHelper rcsbRestHelper2 = new RcsbRestHelper();

        List<String> pdbIds = rcsbRestHelper2.queryForAllCurrentPdbIds();

        Assert.assertTrue(pdbIds.size() > 100000);
    }



    @Test
    public void testQueryForAllObsoletePdbIds() throws JException
    {
        RcsbRestHelper rcsbRestHelper2 = new RcsbRestHelper();

        List<String> pdbIds = rcsbRestHelper2.queryForAllObsoletePdbIds();

        Assert.assertTrue(pdbIds.size() > 100);
    }



    @Test
    public void testCreatePdbReleaseDateJsonQuery() throws JException
    {
        Date fromDate = JDateUtil.createDate(2021, 01, 01);
        Date toDate = JDateUtil.createDate(2021, 03, 31);

        RcsbRestHelper rcsbRestHelper2 = new RcsbRestHelper();


        //open ended range
        String releaseDateJsonQuery1 = rcsbRestHelper2.createPdbReleaseDateJsonQuery(fromDate, null);

        List<String> pdbIds = rcsbRestHelper2.queryPdbByJsonQuery(releaseDateJsonQuery1);

        Assert.assertTrue(CollectionUtil.hasValues(pdbIds));


        //closed range
        String releaseDateJsonQuery2 = rcsbRestHelper2.createPdbReleaseDateJsonQuery(fromDate, toDate);

        List<String> pdbIds2 = rcsbRestHelper2.queryPdbByJsonQuery(releaseDateJsonQuery2);

        Assert.assertTrue(CollectionUtil.hasValues(pdbIds2));
    }



    @Test
    public void testCreatePdbReviseDateJsonQuery() throws JException
    {
        Date fromDate = JDateUtil.createDate(2021, 01, 01);
        Date toDate = JDateUtil.createDate(2021, 03, 31);

        RcsbRestHelper rcsbRestHelper2 = new RcsbRestHelper();


        //open ended range
        String reviseDateJsonQuery1 = rcsbRestHelper2.createPdbReviseDateJsonQuery(fromDate, null);

        List<String> pdbIds = rcsbRestHelper2.queryPdbByJsonQuery(reviseDateJsonQuery1);

        Assert.assertTrue(CollectionUtil.hasValues(pdbIds));


        //closed range
        String reviseDateJsonQuery2 = rcsbRestHelper2.createPdbReviseDateJsonQuery(fromDate, toDate);

        List<String> pdbIds2 = rcsbRestHelper2.queryPdbByJsonQuery(reviseDateJsonQuery2);

        Assert.assertTrue(CollectionUtil.hasValues(pdbIds2));
    }


}
