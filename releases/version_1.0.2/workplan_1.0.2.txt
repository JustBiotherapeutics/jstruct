
--   |/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|
--   |/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|
--   |/|\|/|       WORK-PLAN for       |/|\|/|
--   |/|\|/|       J S t r u c t       |/|\|/|
--   |/|\|/|       version 1.0.2       |/|\|/|
--   |/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|
--   |/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|/|\|

-- -----------------------------------------------------------------------------
-- -----------------------------------------------------------------------------

-- changes
* fix bug: refactoring implementation of how structure permissions are handled

-- -----------------------------------------------------------------------------
-- -----------------------------------------------------------------------------
-- release application to tomcat (AWS EC2 instance)


(-)
on 1p-10-0-1-33 archive the current .war file
      ls -la /just/apps/tomcat/jstruct-prod/webapps
      sudo cp /just/apps/tomcat/jstruct-prod/webapps/jstruct.war /home/svc-jdesign/webapp_archive/jstruct.war.2017-xx-xx

(-)
ensure tomcat is shutdown on local machine

(-)
on production, stop the automated process via the application

(-)
update the AppVersion/AppDate in ALL the ApplicationResources.properties files

(-)
run any ddl/dml in the 'incremental sql' file

(-)
deploy from command line:

  LOCAL: cd /home/russellw/git/jstruct
         mvn -Plocal tomcat:undeploy
         mvn -Plocal clean package tomcat:deploy -Dmaven.test.skip=true

  johndeere: 
         cd /home/russellw/git/jstruct
         mvn -Pjohndeere tomcat:undeploy
         mvn -Pjohndeere clean package tomcat:deploy -Dmaven.test.skip=true

  PROD:  cd /home/russellw/git/jstruct
         mvn -Pprod tomcat:undeploy
         mvn -Pprod clean package tomcat:deploy -Dmaven.test.skip=true


(-)
test the app - production:
   http://jstruct.justbiotherapeutics.com

(-)
on production, start the automated process via application




-- -----------------------------------------------------------------------------
-- -----------------------------------------------------------------------------
-- restart tomcat - only as necessary

Login to EC2 server

        cd /home/russellw
        ssh -i "jstruct-ec2-keypair.pem" ubuntu@10.0.1.33


        sudo /etc/init.d/tomcat_jstruct stop
        sudo /etc/init.d/tomcat_jstruct start




