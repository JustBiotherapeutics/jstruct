
-- -----------------------------------------------------------------------------
-- continue with schema creation - create all the tables and views
-- (run one of init_x.x.x_DEV.sql or init_x.x.x_PROD.sql first) 







-- STRUCTURE
drop table if exists structure;
create table structure (
  structure_id        bigserial primary key      not null,
  owner_id            bigint                     not null,
  from_rcsb           char                       not null,
  can_all_read        char                       not null,
  can_all_write       char                       not null,
  source_name         varchar(255)               null,
  is_obsolete         char                       not null,
  obsolete_date       timestamp with time zone   null
);
--foreign keys
alter table structure add foreign key (owner_id) references jstruct_user(user_id);
--indexes
create index ix_structure_ownerid on structure(owner_id);
create index ix_structure_fromrcsb on structure(from_rcsb);
CREATE INDEX ix_structure_sourcename ON structure(source_name);
CREATE INDEX ix_structure_isobsolete ON structure(is_obsolete);
CREATE INDEX ix_structure_obsoletedate ON structure(obsolete_date);

--start sequence at 10001
ALTER SEQUENCE structure_structure_id_seq RESTART WITH 10001;






-- CLIENT
drop table if exists client;
create table client (
  client_id           bigserial primary key      not null,
  name                varchar(255)               not null,
  deprecated          char                       not null,
  insert_date         timestamp with time zone   not null,
  insert_user_id      bigint                     not null,
  update_date         timestamp with time zone   not null,
  update_user_id      bigint                     not null
);
--foreign keys
alter table client add foreign key (insert_user_id) references jstruct_user(user_id);
alter table client add foreign key (update_user_id) references jstruct_user(user_id);
--indexes
create index ix_client_insertuserid on client(insert_user_id);
create index ix_client_updateuserid on client(update_user_id);
create index ix_client_deprecated on client(deprecated);
--insert initial values (update this list here, and/or manage it through the UI after install)
insert into client (name, deprecated, insert_date, insert_user_id, update_date, update_user_id)
values
  ('N/A', 'N', current_timestamp, 1, current_timestamp, 1),
  ('default', 'N', current_timestamp, 1, current_timestamp, 1),
  ('internal', 'N', current_timestamp, 1, current_timestamp, 1)
;





-- PROGRAM
drop table if exists program;
create table program (
  program_id          bigserial primary key      not null,
  name                varchar(255)               not null,
  deprecated          char                       not null,
  insert_date         timestamp with time zone   not null,
  insert_user_id      bigint                     not null,
  update_date         timestamp with time zone   not null,
  update_user_id      bigint                     not null
);
--foreign keys
alter table program add foreign key (insert_user_id) references jstruct_user(user_id);
alter table program add foreign key (update_user_id) references jstruct_user(user_id);
--indexes
create index ix_program_insertuserid on program(insert_user_id);
create index ix_program_updateuserid on program(update_user_id);
create index ix_program_deprecated on program(deprecated);
--insert initial values (update this list here, and/or manage it through the UI after install)
insert into program (name, deprecated, insert_date, insert_user_id, update_date, update_user_id)
values
  ('N/A', 'N', current_timestamp, 1, current_timestamp, 1),
  ('default', 'N', current_timestamp, 1, current_timestamp, 1),
  ('internal', 'N', current_timestamp, 1, current_timestamp, 1)
;







-- IMPORT_JOB
drop table if exists import_job;
create table import_job (
  import_job_id         bigserial primary key      not null,
  run_by_user_id        bigint                     not null,

  run_start             timestamp with time zone   not null,
  import_end            timestamp with time zone   null,
  run_end               timestamp with time zone   null,

  import_type           varchar(255)               not null,
  import_status         varchar(255)               not null,
  failure_message       text                       null,

  count_unique_source       integer                not null,
  count_imported            integer                not null,
  count_added               integer                not null,
  count_updated             integer                not null,

  pdb_rest_url          varchar(2000)              null,
  pdb_rest_criteria     text                       null,
  pdb_search_date       timestamp with time zone   null

);
--foreign keys
alter table import_job add foreign key (run_by_user_id) references jstruct_user(user_id);
--indexes
create index ix_importjob_runbyuserid on import_job(run_by_user_id);
create index ix_importjob_importstatus on import_job(import_status);
create index ix_importjob_importtype on import_job(import_type);



-- IMPORT_MSG
drop table if exists import_msg;
create table import_msg (
  import_msg_id         bigserial primary key      not null,
  import_job_id         bigint                     null,
  file_info             varchar(2000)              null,
  message               text                       not null,
  stack_trace           text                       NULL,
  insert_date           timestamp with time zone   not null,
  insert_user_id        bigint                     not null
);
--foreign keys
alter table import_msg add foreign key (import_job_id) references import_job(import_job_id);
alter table import_msg add foreign key (insert_user_id) references jstruct_user(user_id);
--indexes
create index ix_importmsg_importjobid on import_msg(import_job_id);
create index ix_importmsg_filename on import_msg(file_info);
create index ix_importmsg_insertuserid on import_msg(insert_user_id);





-- IMPORT_REST
drop table if exists import_rest;
create table import_rest (
  import_rest_id       bigserial primary key    not null,

  search_desc          varchar(255)             not null,

  pdb_rest_url         varchar(2000)            not null,
  pdb_rest_criteria    text                     null,
  search_date_start    timestamp                null,
  search_date_end      timestamp                null,

  response_pdb_count   bigint                   not null,

  import_job_id        bigint                   not null
);
--foreign keys
alter table import_rest add foreign key (import_job_id) references import_job(import_job_id);
--indexes
create index ix_importrest_importjobid on import_rest(import_job_id);
create index ix_importrest_searchdatestart on import_rest(search_date_start);
create index ix_importrest_searchdateend on import_rest(search_date_end);




-- IMPORT_PDB
drop table if exists import_pdb;
create table import_pdb (
  import_pdb_id       bigserial primary key    not null,
  pdb_identifier      varchar(4)               not null,
  import_rest_id      bigint                   not null
);
--foreign keys
alter table import_pdb add foreign key (import_rest_id) references import_rest(import_rest_id);
--indexes
create index ix_importpdb_importrestid on import_pdb(import_rest_id);











-- STRUCT_VERSION
drop table if exists struct_version;
create table struct_version (
  struct_ver_id        bigserial primary key     not null,
  structure_id         bigint                    not null,
  version              integer                   not null,

  struct_method        varchar(255)              null,
  client_id            bigint                    null,
  program_id           bigint                    null,
  target               varchar(255)              null,
  is_model             char                      null,
  description          text                      null,

  insert_date         timestamp with time zone   not null,
  insert_user_id      bigint                     not null,
  update_date         timestamp with time zone   not null,
  update_user_id      bigint                     not null
);
--foreign keys
alter table struct_version add foreign key (structure_id) references structure(structure_id);
alter table struct_version add foreign key (client_id) references client(client_id);
alter table struct_version add foreign key (program_id) references program(program_id);
alter table struct_version add foreign key (insert_user_id) references jstruct_user(user_id);
alter table struct_version add foreign key (update_user_id) references jstruct_user(user_id);
--indexes
create index ix_structversion_structureid on struct_version(structure_id);
create index ix_structversion_clientid on struct_version(client_id);
create index ix_structversion_programid on struct_version(program_id);
create index ix_structversion_insertuserid on struct_version(insert_user_id);
create index ix_structversion_updateuserid on struct_version(update_user_id);
create index ix_structversion_version on struct_version(version);
create index ix_structversion_ismodel on struct_version(is_model);
--start sequence at 10001
ALTER SEQUENCE struct_version_struct_ver_id_seq RESTART WITH 10001;






-- STRUCT_FILE
drop table if exists struct_file;
create table struct_file (
  struct_file_id              bigserial primary key      not null,
  struct_ver_id               bigint                     not null,

  pdb_identifier              varchar(4)                 null,
  data                        bytea                      null,
  checksum                    varchar(50)                null,
  uncompressed_size           bigint                     not null,

  from_location        varchar(2000)              not null,
  upload_file_name     varchar(255)               not null,
  upload_file_ext      varchar(255)               null,
  actual_file_name     varchar(255)               not null,
  actual_file_ext      varchar(255)               null,

  file_creation_date     timestamp with time zone   null,
  file_modified_date     timestamp with time zone   null,

  process_chain_status   varchar(10)                not null,

  insert_date            timestamp with time zone   not null,
  insert_user_id         bigint                     not null,
  update_date            timestamp with time zone   not null,
  update_user_id         bigint                     not null
);
--one-to-one primary/foreign key (note: struct_ver_id is bigint, not serial)
alter table struct_file add foreign key (struct_ver_id) references struct_version(struct_ver_id);
create unique index ux_structfile_structverid on struct_file(struct_ver_id);
--foreign keys
alter table struct_file add foreign key (insert_user_id) references jstruct_user(user_id);
alter table struct_file add foreign key (update_user_id) references jstruct_user(user_id);
--indexes
create index ix_structfile_pdbidentifier on struct_file(pdb_identifier);
create index ix_structfile_insertuserid on struct_file(insert_user_id);
create index ix_structfile_updateuserid on struct_file(update_user_id);
create index ix_structfile_checksum on struct_file(checksum);
create index ix_structfile_processchainstatus on struct_file(process_chain_status);


-- PDB_FILE
drop table if exists pdb_file;
create table pdb_file (
  pdb_file_id         bigserial primary key      not null,
  struct_ver_id       bigint                     not null,
  pdb_identifier      varchar(4)                 null,
  title               text                       not null,
  classification      varchar(100)               null,
  deposition_date     timestamp with time zone   null,
  caveat              text                       null,
  expdta              text                       null,
  nummdl              bigint                     null,
  mdltyp              text                       null,
  resolution_line      varchar(100)              null,
  resolution_unit      varchar(100)              null,
  resolution_measure   numeric                   null

);
--one-to-one primary/foreign key (note: struct_ver_id is bigint, not serial)
alter table pdb_file add foreign key (struct_ver_id) references struct_version(struct_ver_id);
create unique index ux_pdbfile_structverid on pdb_file(struct_ver_id);
--foreign keys
alter table pdb_file add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbfile_pdbidentifier on pdb_file(pdb_identifier);


-- PDB_REVDAT
drop table if exists pdb_revdat;
create table pdb_revdat (
  pdb_revdat_id     bigserial primary key       not null,
  struct_ver_id     bigint                      not null,
  modnum            int                         not null,
  moddate           timestamp with time zone    not null,
  modtype           int                         not null,
  detail            varchar(1000)               null
);
--foreign keys
alter table pdb_revdat add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbrevdat_structverid on pdb_revdat(struct_ver_id);
create index ix_pdbrevdat_modnum on pdb_revdat(modnum);


-- PDB_REMARK
drop table if exists pdb_remark;
create table pdb_remark (
  pdb_remark_id     bigserial primary key   not null,
  struct_ver_id     bigint                  not null,
  rem_num           integer                 null,
  name              varchar(100)            not null,
  value             text                    not null
);
--foreign keys
alter table pdb_remark add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbremark_structverid on pdb_remark(struct_ver_id);
create index ix_pdbremark_remnum on pdb_remark(rem_num);
--thie one relies on the extension: pg_trgm
create index concurrently ix_pdbremark_value_gin on pdb_remark using gin(value gin_trgm_ops);

-- PDB_COMPND
drop table if exists pdb_compnd;
create table pdb_compnd (
  pdb_compnd_id     bigserial primary key   not null,
  struct_ver_id     bigint                  not null,
  mol_id            bigint                  not null,
  name              varchar(100)            not null,
  value             text                    not null
);
--foreign keys
alter table pdb_compnd add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbcompnd_strucverid on pdb_compnd(struct_ver_id);
create index ix_pdbcompnd_molid on pdb_compnd(mol_id);

-- PDB_SOURCE
drop table if exists pdb_source;
create table pdb_source (
  pdb_source_id     bigserial primary key   not null,
  struct_ver_id     bigint                  not null,
  mol_id            bigint                  not null,
  name              varchar(100)            not null,
  value             text                    not null
);
--foreign keys
alter table pdb_source add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbsource_structverid on pdb_source(struct_ver_id);
create index ix_pdbsource_molid on pdb_compnd(mol_id);

-- PDB_SPLIT
drop table if exists pdb_split;
create table pdb_split (
  pdb_split_id      bigserial primary key   not null,
  struct_ver_id     bigint                  not null,
  pdb_identifier    text                    not null,
  db_name           varchar(100)            null,
  content_type      varchar(100)            null,
  detail            varchar(2000)           null
);
--foreign keys
alter table pdb_split add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbsplit_structverid on pdb_split(struct_ver_id);
create index ix_pdbsplit_dbname on pdb_split(db_name);

-- PDB_KEYWORD
drop table if exists pdb_keyword;
create table pdb_keyword (
  pdb_keyword_id    bigserial primary key   not null,
  struct_ver_id     bigint                  not null,
  value             varchar(1000)           not null
);
--foreign keys
alter table pdb_keyword add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbkeyword_structverid on pdb_keyword(struct_ver_id);

-- PDB_AUTHOR
drop table if exists pdb_author;
create table pdb_author (
  pdb_author_id      bigserial primary key   not null,
  struct_ver_id      bigint                  not null,
  value              varchar(1000)           not null
);
--foreign keys
alter table pdb_author add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbauthor_structverid on pdb_author(struct_ver_id);

-- PDB_JRNL
drop table if exists pdb_jrnl;
create table pdb_jrnl (
  pdb_jrnl_id       bigserial primary key   not null,
  struct_ver_id     bigint                  not null,
  auth              text                    null,
  titl              text                    null,
  edit              text                    null,
  ref               text                    null,
  publ              text                    null,
  refn              text                    null,
  pmid              text                    null,
  doi               text                    null,
  is_primary        char(1)                 null,
  section           varchar(100)            null
);
--foreign keys
alter table pdb_jrnl add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbjrnl_structverid on pdb_jrnl(struct_ver_id);
create index ix_pdbjrnl_isprimary on pdb_jrnl(is_primary);
create index ix_pdbjrnl_section on pdb_jrnl(section);


-- PDB_SSBOND
drop table if exists pdb_ssbond;
create table pdb_ssbond (
  pdb_ssbond_id     bigserial primary key   not null,
  struct_ver_id     bigint                  not null,
  ser_num           integer                 null,
  residue_1         varchar(3)              null,
  chain_id_1        varchar(4)              null,
  seq_num_1         integer                 null,
  icode_1           varchar(1)              null,
  residue_2         varchar(3)              null,
  chain_id_2        varchar(4)              null,
  seq_num_2         integer                 null,
  icode_2           varchar(1)              null,
  sym_1             varchar(6)              null,
  sym_2             varchar(6)              null,
  bond_length       numeric                 null
);
--foreign keys
alter table pdb_ssbond add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbssbond_structverid on pdb_ssbond(struct_ver_id);
create index ix_pdbssbond_sernum on pdb_ssbond(ser_num);
create index ix_pdbssbond_chainid1 on pdb_ssbond(chain_id_1);
create index ix_pdbssbond_chainid2 on pdb_ssbond(chain_id_2);


-- PDB_SEQRES
drop table if exists pdb_seqres;
create table pdb_seqres (
  pdb_seqres_id     bigserial primary key   not null,
  struct_ver_id     bigint                  not null,
  chain_id          varchar(4)              null,
  num_res           integer                 null,
  residues          text                    null
);
--foreign keys
alter table pdb_seqres add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_pdbseqres_structverid on pdb_seqres(struct_ver_id);
create index ix_pdbseqres_chainid on pdb_seqres(chain_id);








-- FILE_IMPORT_JOB
drop table if exists file_import_job;
create table file_import_job (
  file_import_job_id   bigserial primary key   not null,
  struct_ver_id        bigint                  not null,
  import_job_id        bigint                  not null
);
--foreign keys
alter table file_import_job add foreign key (struct_ver_id) references struct_version(struct_ver_id);
alter table file_import_job add foreign key (import_job_id) references import_job(import_job_id);
--indexes
create index ix_fileimportjob_structverid on file_import_job(struct_ver_id);
create index ix_fileimportjob_importjobid on file_import_job(import_job_id);







-- CHAIN
drop table if exists chain;
create table chain (
  chain_id              bigserial primary key   not null,
  struct_ver_id         bigint                  not null,
  name                  varchar(255)            null,
  type                  varchar(255)            not null,
  residue_count         integer                 not null,
  seq_theoretical       text                    null
);
--foreign keys
alter table chain add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_chain_structverid on chain(struct_ver_id);




-- ATOM_GROUPING
drop table if exists ATOM_GROUPING;
create table ATOM_GROUPING (
  atom_grouping_id         bigserial primary key   not null,
  chain_id                 bigint                  not null,
  type                     varchar(255)            not null,
  seq_with_all_coords      text                    null,
  seq_with_any_coords      text                    null,
  seq_with_ca_coords       text                    null,
  seq_with_bkbn_coords     text                    null,
  seq_with_bkbn_cb_coords  text                    null
);
alter table ATOM_GROUPING add foreign key (chain_id) references chain(chain_id);
create index ix_atomgrouping_chainid on atom_grouping(chain_id);
create index ix_atomgrouping_type on atom_grouping(type);






-- RESIDUE
drop table if exists residue;
create table residue (
  residue_id           bigserial primary key    not null,
  atom_grouping_id     bigint                   not null,
  abbrev               varchar(3)               not null,
  residue_type         varchar(100)             not null,
  res_seq_number       integer                  not null,
  icode                char                     null,
  atoms                text                     null,
  has_all_coords        char                    not null,
  has_any_coords        char                    not null,
  has_ca_coords         char                    not null,
  has_bkbn_coords       char                    not null,
  has_bkbn_cb_coords    char                    not null
);
--foreign keys
alter table residue add foreign key (atom_grouping_id) references atom_grouping(atom_grouping_id);
--indexes
create index ix_residue_atomgroupingid on residue(atom_grouping_id);
create index ix_residue_abbrev on residue(abbrev);




-- SEQ_RESIDUE
drop table if exists seq_residue;
create table seq_residue (
  seq_residue_id     bigserial primary key    not null,
  chain_id           bigint                   not null,
  abbrev             varchar(3)               not null,
  residue_type       varchar(100)             not null,
  ordinal            integer                  not null
);
--foreign keys
alter table seq_residue add foreign key (chain_id) references chain(chain_id);
--indexes
create index ix_seqresidue_chainid on seq_residue(chain_id);







-- STRUCTURE_USER
drop table if exists structure_user;
create table structure_user (
  structure_user_id   bigserial primary key      not null,
  structure_id        bigint                     not null,
  struct_ver_id       bigint                     not null,
  user_id             bigint                     not null,
  is_favorite         char                       not null,
  is_recent           char                       not null,
  recent_count        integer                    null,
  insert_date         timestamp with time zone   not null
);
--foreign keys
alter table structure_user add foreign key (structure_id) references structure(structure_id);
alter table structure_user add foreign key (struct_ver_id) references struct_version(struct_ver_id);
alter table structure_user add foreign key (user_id) references jstruct_user(user_id);
--indexes
create index ix_structureuser_structurerid on structure_user(structure_id);
create index ix_structureuser_structverrid on structure_user(struct_ver_id);
create index ix_structureuser_userid on structure_user(user_id);
create index ix_structureuser_isfavorite on structure_user(is_favorite);
create index ix_structureuser_isrecent on structure_user(is_recent);






-- STRUCTURE_ROLE
drop table if exists structure_role;
create table structure_role (
  structure_role_id   bigserial primary key      not null,
  structure_id        bigint                     not null,
  user_id             bigint                     not null,
  can_read            char                       not null,
  can_write           char                       not null,
  can_grant           char                       not null
);
--foreign keys
alter table structure_role add foreign key (structure_id) references structure(structure_id);
alter table structure_role add foreign key (user_id) references jstruct_user(user_id);
--indexes
create index ix_structurerole_structurerid on structure_user(structure_id);
create index ix_structurerole_userid on structure_user(user_id);





-- USER_SEARCH
drop table if exists user_search;
create table user_search (
  user_search_id      bigserial primary key      not null,

  user_id             bigint                     not null,
  search_name         varchar(255)               not null,

  file_status_rcsb    varchar(100)               not null,
  file_status_manual  varchar(100)               not null,
  query_mode          varchar(10)                not null,

  insert_date         timestamp with time zone   not null,
  insert_user_id      bigint                     not null,
  update_date         timestamp with time zone   not null,
  update_user_id      bigint                     not null
);
--foreign keys
alter table user_search add foreign key (user_id) references jstruct_user(user_id);
alter table user_search add foreign key (insert_user_id) references jstruct_user(user_id);
alter table user_search add foreign key (update_user_id) references jstruct_user(user_id);
--indexes
create index ix_usersearch_userid on user_search(user_id);
create index ix_usersearch_insertuserid on user_search(insert_user_id);
create index ix_usersearch_updateuserid on user_search(update_user_id);
create index ix_usersearch_searchname on user_search(search_name);



-- SEARCH_SUBQUERY
drop table if exists search_subquery;
create table search_subquery (
  search_subquery_id    bigserial primary key      not null,
  user_search_id        bigint                     not null,
  subquery_type         varchar(100)               not null,
  row_index             integer                    not null
);
--foreign keys
alter table search_subquery add foreign key (user_search_id) references user_search(user_search_id);
--indexes
create index ix_searchsubquery_usersearchid on search_subquery(user_search_id);




-- SEARCH_PARAM
drop table if exists search_param;
create table search_param (
  search_param_id       bigserial primary key      not null,
  search_subquery_id    bigint                     not null,
  param_key             varchar(255)               not null,
  param_value           varchar(2000)              null
);
--foreign keys
alter table search_param add foreign key (search_subquery_id) references search_subquery(search_subquery_id);
--indexes
create index ix_searchparam_searchsubqueryid on search_subquery(search_subquery_id);






-- USER_LABEL
drop table if exists user_label;
create table user_label (

  user_label_id       bigserial primary key      not null,

  user_id             bigint                     not null,

  label_name          varchar(255)               not null,
  description         varchar(2000)              null,
  background_color    varchar(6)                 not null,
  text_color          varchar(6)                 not null,
  ordinal             integer                    not null,

  insert_date         timestamp with time zone   not null,
  insert_user_id      bigint                     not null,
  update_date         timestamp with time zone   not null,
  update_user_id      bigint                     not null
);
--foreign keys
alter table user_label add foreign key (user_id) references jstruct_user(user_id);
alter table user_label add foreign key (insert_user_id) references jstruct_user(user_id);
alter table user_label add foreign key (update_user_id) references jstruct_user(user_id);
--indexes
create index ix_userlabel_userid on user_label(user_id);
create index ix_userlabel_insertuserid on user_label(insert_user_id);
create index ix_userlabel_updateuserid on user_label(update_user_id);
create index ix_userlabel_labelname on user_label(label_name);
create index ix_userlabel_ordinal on user_label(ordinal);





-- STRUCTURE_LABEL
drop table if exists structure_label;
create table structure_label (

  structure_label_id  bigserial primary key      not null,

  user_label_id       bigint                     not null,
  structure_id        bigint                     not null,

  insert_date         timestamp with time zone   not null,
  insert_user_id      bigint                     not null
);
--foreign keys
alter table structure_label add foreign key (user_label_id) references user_label(user_label_id);
alter table structure_label add foreign key (structure_id) references structure(structure_id);
alter table structure_label add foreign key (insert_user_id) references jstruct_user(user_id);
--indexes
create index ix_structurelabel_userlabelid on structure_label(user_label_id);
create index ix_structurelabel_structureid on structure_label(structure_id);
create index ix_structurelabel_insertuserid on structure_label(insert_user_id);



-- view combining user_label and structure_label for ease of access
--USER_STRUCTURE_LABEL_VIEW
drop view if exists user_structure_label_view;

create or replace view user_structure_label_view as
  SELECT
    sl.structure_label_id   as structure_label_id,
    sl.structure_id         as structure_id,
    ul.user_label_id        as user_label_id,
    ul.user_id              as user_id,
    ul.label_name           as label_name,
    ul.description          as description,
    ul.background_color     as background_color,
    ul.text_color           as text_color,
    ul.ordinal              as ordinal
  FROM structure_label sl
    left join user_label ul
      on sl.user_label_id = ul.user_label_id;









-- NOTE
drop table if exists note;
create table note (
  note_id             bigserial primary key      not null,
  structure_id        bigint                     not null,
  struct_ver_id       bigint                     not null,

  formatted_comment   text                       not null,
  lower_comment       varchar(2000)              not null,
  is_public           char                       not null,

  insert_date         timestamp with time zone   not null,
  insert_user_id      bigint                     not null,
  update_date         timestamp with time zone   not null,
  update_user_id      bigint                     not null
);
--foreign keys
alter table note add foreign key (struct_ver_id)  references  struct_version(struct_ver_id);
alter table note add foreign key (structure_id)   references  structure(structure_id);
alter table note add foreign key (insert_user_id) references  jstruct_user(user_id);
alter table note add foreign key (update_user_id) references  jstruct_user(user_id);
--indexes
create index ix_note_structversion on struct_version(struct_ver_id);
create index ix_note_structversion on structure(structure_id);
create index ix_note_insertuserid on struct_version(insert_user_id);
create index ix_note_updateuserid on struct_version(update_user_id);

CREATE INDEX ix_lwr_note_lowercomment ON note(LOWER(lower_comment));







-- PDB_LINEAGE
drop table if exists pdb_lineage;
create table pdb_lineage (
  pdb_lineage_id       bigserial primary key      not null,
  pdb_predecessor_id   varchar(4)                 not null,
  pdb_descendant_id    varchar(4)                 null,
  obsolete_date        timestamp with time zone   not null
);
--foreign keys
--  note: the predecessor and descendant ids are 'soft keys', so used to find lineage, but not enforced at the database
--indexes
create index ix_pdblineage_predecessorid on pdb_lineage(pdb_predecessor_id);
create index ix_pdblineage_descendantid on pdb_lineage(pdb_descendant_id);






-- AMINO_ACID_DEF
drop table if exists amino_acid_def;
create table amino_acid_def (
  amino_acid_def_id   bigserial primary key      not null,
  name                varchar(255)               not null,
  abbrev              varchar(3)                 not null,
  one_letter          char                       not null,
  is_standard         char                       not null,
  atoms               varchar(255)               not null,
  insert_date         timestamp with time zone   not null,
  insert_user_id      bigint                     not null,
  update_date         timestamp with time zone   not null,
  update_user_id      bigint                     not null
);
--foreign keys
alter table amino_acid_def add foreign key (insert_user_id) references jstruct_user(user_id);
alter table amino_acid_def add foreign key (update_user_id) references jstruct_user(user_id);
--indexes
create index ix_aminoaciddef_insertuserid on amino_acid_def(insert_user_id);
create index ix_aminoaciddef_updateuserid on amino_acid_def(update_user_id);
create index ix_aminoaciddef_isstandard on amino_acid_def(is_standard);
create unique index ux_aminoaciddef_apprev on amino_acid_def(abbrev);
create unique index ux_aminoaciddef_name   on amino_acid_def(name);
--insert all 20 'standard' amino acid definitions, and all the others.
insert into amino_acid_def (name, one_letter, abbrev, is_standard, atoms, insert_date, insert_user_id, update_date, update_user_id)
values
  ('Alanine',                   'A', 'ALA', 'Y', 'N,CA,CB,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Cysteine',                  'C', 'CYS', 'Y', 'N,CA,CB,SG,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Aspartic Acid',             'D', 'ASP', 'Y', 'N,CA,CB,CG,OD1,OD2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Glutamic Acid',             'E', 'GLU', 'Y', 'N,CA,CB,CG,CD,OE1,OE2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Phenylalanine',             'F', 'PHE', 'Y', 'N,CA,CB,CG,CD1,CE1,CZ,CE2,CD2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Glycine',                   'G', 'GLY', 'Y', 'N,CA,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Histidine',                 'H', 'HIS', 'Y', 'N,CA,CB,CG,ND1,CE1,NE2,CD2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Isoleucine',                'I', 'ILE', 'Y', 'N,CA,CB,CG2,CG1,CD1,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Lysine',                    'K', 'LYS', 'Y', 'N,CA,CB,CG,CD,CE,NZ,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Leucine',                   'L', 'LEU', 'Y', 'N,CA,CB,CG,CD1,CD2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Methionine',                'M', 'MET', 'Y', 'N,CA,CB,CG,SD,CE,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Asparagine',                'N', 'ASN', 'Y', 'N,CA,CB,CG,OD1,ND2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Proline',                   'P', 'PRO', 'Y', 'N,CA,CB,CG,CD,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Glutamine',                 'Q', 'GLN', 'Y', 'N,CA,CB,CG,CD,OE1,NE2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Arginine',                  'R', 'ARG', 'Y', 'N,CA,CB,CG,CD,NE,CZ,NH1,NH2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Serine',                    'S', 'SER', 'Y', 'N,CA,CB,OG,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Threonine',                 'T', 'THR', 'Y', 'N,CA,CB,CG2,OG1,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Valine',                    'V', 'VAL', 'Y', 'N,CA,CB,CG1,CG2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Tryptophan',                'W', 'TRP', 'Y', 'N,CA,CB,CG,CD1,NE1,CE2,CZ2,CH2,CZ3,CE3,CD2,C,O', current_timestamp, 1, current_timestamp, 1),
  ('Tyrosine',                  'Y', 'TYR', 'Y', 'N,CA,CB,CG,CD1,CE1,CZ,OH,CE2,CD2,C,O', current_timestamp, 1, current_timestamp, 1),

  ('Asparagine or Aspartate',   'B', 'ASX', 'Y', 'N,CA,CB,C,O,CG,XD1,XD2', current_timestamp, 1, current_timestamp, 1),
  ('Glutamine or Glutamate',    'Z', 'GLX', 'Y', 'N,CA,CB,C,O,CG,CD,XE1,XE2', current_timestamp, 1, current_timestamp, 1),

  ('D-Alanine',                 'A', 'DAL', 'N', 'N,CA,CB,C,O', current_timestamp, 1, current_timestamp, 1),
  ('D-Cysteine',                'C', 'DCY', 'N', 'N,CA,CB,C,O,SG', current_timestamp, 1, current_timestamp, 1),
  ('D-Aspartate',               'D', 'DAS', 'N', 'N,CA,CB,C,O,CG,OD1,OD2', current_timestamp, 1, current_timestamp, 1),
  ('D-Glutamic Acid',           'E', 'DGL', 'N', 'N,CA,CB,C,O,CG,CD,OE1,OE2', current_timestamp, 1, current_timestamp, 1),
  ('D-Phenylalanine',           'F', 'DPN', 'N', 'N,CA,CB,C,O,CG,CD1,CE1,CZ,CE2,CD2', current_timestamp, 1, current_timestamp, 1),
  ('D-Histidine',               'H', 'DHI', 'N', 'N,CA,CB,C,O,CG,ND1,CE1,NE2,CD2', current_timestamp, 1, current_timestamp, 1),
  ('D-Isoleucine',              'I', 'DIL', 'N', 'N,CA,CB,C,O,CG2,CG1,CD1', current_timestamp, 1, current_timestamp, 1),
  ('D-Lysine',                  'K', 'DLY', 'N', 'N,CA,CB,C,O,CG,CD,CE,NZ', current_timestamp, 1, current_timestamp, 1),
  ('D-Leucine',                 'L', 'DLE', 'N', 'N,CA,CB,C,O,CG,CD1,CD2', current_timestamp, 1, current_timestamp, 1),
  ('D-Methionine',              'M', 'MED', 'N', 'N,CA,CB,C,O,CG,SD,CE', current_timestamp, 1, current_timestamp, 1),
  ('D-Asparagine',              'N', 'DSG', 'N', 'N,CA,CB,C,O,CG,OD1,ND2', current_timestamp, 1, current_timestamp, 1),
  ('D-Proline',                 'P', 'DPR', 'N', 'N,CA,CB,C,O,CD,CG', current_timestamp, 1, current_timestamp, 1),
  ('D-Glutamine',               'Q', 'DGN', 'N', 'N,CA,CB,C,O,CG,CD,NE2,OE1', current_timestamp, 1, current_timestamp, 1),
  ('D-Arginine',                'R', 'DAR', 'N', 'N,CA,CB,C,O,CG,CD,NE,CZ,NH1,NH2', current_timestamp, 1, current_timestamp, 1),
  ('D-Serine',                  'S', 'DSN', 'N', 'N,CA,CB,C,O,OG', current_timestamp, 1, current_timestamp, 1),
  ('D-Threonine',               'T', 'DTH', 'N', 'N,CA,CB,C,O,CG2,OG1', current_timestamp, 1, current_timestamp, 1),
  ('D-Valine',                  'V', 'DVA', 'N', 'N,CA,CB,C,O,CG1,CG2', current_timestamp, 1, current_timestamp, 1),
  ('D-Tryptophan',              'W', 'DTR', 'N', 'N,CA,CB,C,O,CG,CD1,NE1,CE2,CZ2,CH2,CZ3,CE3,CD2', current_timestamp, 1, current_timestamp, 1),
  ('D-Tyrosine',                'Y', 'DTY', 'N', 'N,CA,CB,C,O,CG,CD1,CE1,CZ,OH,CE2,CD2', current_timestamp, 1, current_timestamp, 1),

  ('Histidine protonated on ND',              'H', 'HID', 'N', 'OAB,OAA,CAJ,CAH,HAH,CAL,CAF,NAI,CAN,CAM,CAE,CAD,CAK,CAG,OAC', current_timestamp, 1, current_timestamp, 1),
  ('Histidine protonated on NE',              'H', 'HIE', 'N', 'O1,O2,O3,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20,C21.C22,C23,N1,N2,N3,N4,F1,F2,F3,F4', current_timestamp, 1, current_timestamp, 1),
  ('Histidine protonated on ND and NE',       'H', 'HIP', 'N', 'N,CA,CB,C,O,CG,CD2,NE2,CE1,ND1,P,O1P,O2P,O3P', current_timestamp, 1, current_timestamp, 1),

  ('alpha-aminobutyric acid',                         'X', 'ABA', 'N', 'N,CA,CB,C,O,CG', current_timestamp, 1, current_timestamp, 1),
  ('N(6)-ACETYLLYSINE',                               'X', 'ALY', 'N', 'N,CA,CB,C,O,CG,CD,CE,NZ,CH,OH,CH3', current_timestamp, 1, current_timestamp, 1),
  ('S,S-(2-Hydroxyethyl)Thiocysteine',                'X', 'CME', 'N', 'N,CA,CB,C,O,SG,SD,CE,CZ,OH', current_timestamp, 1, current_timestamp, 1),
  ('3-sulfinoalanine',                                'X', 'CSD', 'N', 'N,CA,CB,C,O,SG,OD1,OD2', current_timestamp, 1, current_timestamp, 1),
  ('Selenocysteine',                                  'X', 'CSE', 'N', 'N,CA,CB,C,O,SE', current_timestamp, 1, current_timestamp, 1),
  ('S-Hydroxycysteine',                               'X', 'CSO', 'N', 'N,CA,CB,C,O,SG,OD', current_timestamp, 1, current_timestamp, 1),
  ('Cysteine-s-Dioxide',                              'X', 'CSW', 'N', 'N,CA,CB,C,O,SG,OD2,OD1', current_timestamp, 1, current_timestamp, 1),
  ('2-Amino-4-Mercapto-Butyric Acid',                 'X', 'HCS', 'N', 'N,CA,CB,C,O,CG,SD', current_timestamp, 1, current_timestamp, 1),
  ('4-Methyl-Histidine',                              'X', 'HIC', 'N', 'N,CA,CB,C,O,CG,ND1,CE1,CZ,NE2,CD2', current_timestamp, 1, current_timestamp, 1),
  ('Hydroxyproline',                                  'X', 'HYP', 'N', 'N,CA,CB,C,O,CG,CD,OD', current_timestamp, 1, current_timestamp, 1),
  ('Lysine Nz-Carboxylic Acid',                       'X', 'KCX', 'N', 'N,CA,CB,C,O,CG,CD,CE,NZ,CX,OQ1,OQ2', current_timestamp, 1, current_timestamp, 1),
  ('N''-Pyridoxyl-Lysine-5''-Monophosphate',          'X', 'LLP', 'N', 'N,CA,CB,C,O,CG,N1,C2,C2'',C3,O3,C4,C4'',C5,C6,C5'',OP4,P,OP1,OP2,OP3,CD,CE,NZ', current_timestamp, 1, current_timestamp, 1),
  ('N-Trimethyllysine',                               'X', 'M3L', 'N', 'N,CA,CB,C,O,CG,CD,CE,NZ,CM1,CM2,CM3', current_timestamp, 1, current_timestamp, 1),
  ('N-Dimethyl-Lysine',                               'X', 'MLY', 'N', 'N,CA,CB,C,O,CG,CD,CE,NZ,CH1,CH2', current_timestamp, 1, current_timestamp, 1),
  ('Selenomethionine',                                'X', 'MSE', 'N', 'N,CA,CB,C,O,CG,SE,CE', current_timestamp, 1, current_timestamp, 1),
  ('N-Methylvaline',                                  'X', 'MVA', 'N', 'N,CA,CB,C,O,CN,CG1,CG2', current_timestamp, 1, current_timestamp, 1),
  ('Norleucine',                                      'X', 'NLE', 'N', 'N,CA,CB,C,O,CG,CD,CE', current_timestamp, 1, current_timestamp, 1),
  ('Cysteinesulfonic Acid',                           'X', 'OCS', 'N', 'N,CA,CB,C,O,SG,OD1,OD3,OD2', current_timestamp, 1, current_timestamp, 1),
  ('Phosphotyrosine',                                 'X', 'PTR', 'N', 'N,CA,CB,C,O,CG,CD1,CE1,CZ,OH,CE2,CD2,P,O3P,O2P,O1P', current_timestamp, 1, current_timestamp, 1),
  ('Phosphoserine',                                   'X', 'SEP', 'N', 'N,CA,CB,C,O,OG,P,O2P,O1P,O3P', current_timestamp, 1, current_timestamp, 1),
  ('Phosphothreonine',                                'X', 'TPO', 'N', 'N,CA,CB,C,O,CG2,OG1,P,O2P,O3P,O1P', current_timestamp, 1, current_timestamp, 1),
  ('Sulfonated Tyrosine',                             'X', 'TYS', 'N', 'N,CA,CB,C,O,CG,CD1,CE1,CZ,OH,CE2,CD2,S,O1,O2,O3', current_timestamp, 1, current_timestamp, 1),
  ('Pyroglutamic Acid (5HP)',                         'X', '5HP', 'N', 'N,CA,CB,C,O,CG,CD,OD', current_timestamp, 1, current_timestamp, 1),
  ('Alpha-aminoisobutyric Acid',                      'X', 'AIB', 'N', 'N,CA,C,O,CB1,CB2', current_timestamp, 1, current_timestamp, 1),
  ('4-methyl-4-[(e)-2-butenyl]-4,n-methyl-threonine', 'X', 'BMT', 'N', 'N,CA,CB,C,O,CG2,CN,OG1,CD1,CD2,CE,CZ,CH', current_timestamp, 1, current_timestamp, 1),
  ('S-Hydroxy Cysteine',                              'X', 'CEA', 'N', 'N,CA,CB,C,O,SG,O1', current_timestamp, 1, current_timestamp, 1),
  ('s-Phosphocysteine',                               'X', 'CSP', 'N', 'N,CA,CB,C,O,SG,P,O1P,O2P,O3P', current_timestamp, 1, current_timestamp, 1),
  ('N-Sarboxy Methionine',                            'X', 'CXM', 'N', 'N,CA,CB,C,O,CG,SD,CE,CN,ON1,ON2', current_timestamp, 1, current_timestamp, 1),
  ('D-Isovaline',                                     'X', 'DIV', 'N', 'N,CA,CB1,CB2,C,O,CG1', current_timestamp, 1, current_timestamp, 1),
  ('N-Formyl Methionine',                             'X', 'FME', 'N', 'N,CA,CB,C,O,CG,SD,CE,CN,O1', current_timestamp, 1, current_timestamp, 1),
  ('Isovaline',                                       'X', 'IVA', 'N', 'CA,CB,C,O,CG1,CG2', current_timestamp, 1, current_timestamp, 1),
  ('N-Methyl Alanine',                                'X', 'MAA', 'N', 'N,CA,CB,C,O,CM', current_timestamp, 1, current_timestamp, 1),
  ('N-Methyl Asparagine',                             'X', 'MEN', 'N', 'N,CA,CB,C,O,CG,OD1,ND2,CE2', current_timestamp, 1, current_timestamp, 1),
  ('N-Methyl Leucine',                                'X', 'MLE', 'N', 'N,CA,CB,C,O,CG,CN,CD1,CD2', current_timestamp, 1, current_timestamp, 1),
  ('2-Amino-Pentanoic Acid (Norvaline)',              'X', 'NVA', 'N', 'N,CA,CB,C,O,CG,CD', current_timestamp, 1, current_timestamp, 1),
  ('S-Dioxymethionine',                               'X', 'OMT', 'N', 'N,CA,CB,C,O,CG,SD,CE,OD1,OD2', current_timestamp, 1, current_timestamp, 1),
  ('Ornithine',                                       'X', 'ORN', 'N', 'N,CA,CB,C,O,CG,CD,NE', current_timestamp, 1, current_timestamp, 1),
  ('Pyroglutamic Acid (PCA)',                         'X', 'PCA', 'N', 'N,CA,CB,C,O,CG,CD,OE', current_timestamp, 1, current_timestamp, 1),
  ('n-Methylglycine',                                 'X', 'SAR', 'N', 'N,CA,C,O,CN', current_timestamp, 1, current_timestamp, 1)
;



-- -----------------------------------------------------------------------------
--create all views



drop view if exists full_structure_view;

create or replace view full_structure_view as
  select
    max_ver_subquery2.max_version          as max_version,
    max_ver_subquery2.max_struct_ver_id    as max_struct_ver_id,

    CASE WHEN max_version=version THEN 'Y'
    WHEN max_version>1 THEN 'N'
    ELSE 'X'
    END as most_current,

    s.structure_id    as structure_id,
    s.owner_id        as owner_id,
    u2.name           as owner_name,
    s.from_rcsb       as from_rcsb,
    s.source_name     as source_name,
    s.can_all_read    as can_all_read,
    s.can_all_write   as can_all_write,
    s.is_obsolete     as is_obsolete,
    s.obsolete_date   as obsolete_date,

    sv.struct_ver_id     as struct_ver_id,
    sv.version           as version,
    sv.struct_method     as struct_method,
    sv.target            as target,
    sv.is_model          as is_model,
    sv.description       as description,
    sv.insert_date       as struct_ver_insert_date,
    sv.insert_user_id    as struct_ver_insert_user_id,
    u1.name              as struct_ver_insert_user_name,
    sv.update_date       as struct_ver_update_date,
    sv.update_user_id    as struct_ver_update_user_id,
    u4.name              as struct_ver_update_user_name,
    c.name              as client_name,
    c.client_id         as client_id,
    p.name              as program_name,
    p.program_id        as program_id,

    sf.checksum                     as checksum,
    sf.uncompressed_size            as uncompressed_size,
    ( select compressed_size
      from (select length(data) as compressed_size
            from struct_file sf3
            where sf3.struct_file_id = sf.struct_file_id)
        as temp_table )               as compressed_size,
    sf.from_location                as from_location,
    sf.upload_file_name             as upload_file_name,
    sf.upload_file_ext              as upload_file_ext,
    sf.actual_file_name             as actual_file_name,
    sf.actual_file_ext              as actual_file_ext,
    sf.file_creation_date           as file_creation_date,
    sf.file_modified_date           as file_modified_date,
    sf.process_chain_status         as process_chain_status,
    sf.insert_date                  as struct_file_insert_date,
    sf.insert_user_id               as struct_file_insert_user_id,
    u3.name                         as struct_file_insert_user_name,
    sf.update_date                  as struct_file_update_date,
    sf.update_user_id               as struct_file_update_user_id,
    u5.name                         as struct_file_update_user_name,

    sf.pdb_identifier      as pdb_identifier,
    pf.classification      as pdb_classification,
    pf.deposition_date     as pdb_deposition_date,
    pf.title               as pdb_title,
    pf.caveat              as pdb_caveat,
    pf.expdta              as pdb_expdta,
    pf.nummdl              as pdb_nummdl,
    pf.mdltyp              as pdb_mdltyp,
    pf.resolution_line     as pdb_resolution_line,
    pf.resolution_unit     as pdb_resolution_unit,
    pf.resolution_measure  as pdb_resolution_measure

  from struct_version sv
    left join structure s
      on sv.structure_id = s.structure_id

    left join struct_file sf
      on sv.struct_ver_id = sf.struct_ver_id

    left join pdb_file pf
      on sf.struct_ver_id = pf.struct_ver_id
    left join client c
      on sv.client_id = c.client_id
    left join program p
      on sv.program_id = p.program_id
    left join jstruct_user u1
      on sv.insert_user_id = u1.user_id
    left join jstruct_user u2
      on s.owner_id = u2.user_id
    left join jstruct_user u3
      on sf.insert_user_id = u3.user_id
    left join jstruct_user u4
      on sv.update_user_id = u4.user_id
    left join jstruct_user u5
      on sf.update_user_id = u5.user_id
    LEFT JOIN (
                SELECT sv2.struct_ver_id as max_struct_ver_id,
                  sv2.structure_id,
                       max_ver_subquery.max_version as max_version
                FROM struct_version sv2,
                  structure s2,
                  (select structure_id, MAX(version) AS max_version
                   from struct_version
                   group by structure_id) max_ver_subquery
                WHERE s2.structure_id = max_ver_subquery.structure_id
                      AND sv2.structure_id = s2.structure_id
                      AND sv2.version = max_ver_subquery.max_version
              ) max_ver_subquery2
      ON s.structure_id = max_ver_subquery2.structure_id
;

-- -----------------------------------------------------------------------------
-- adding LOWER indexes to all search fields

CREATE INDEX ix_lwr_structversion_structmethod ON struct_version(LOWER(struct_method));
CREATE INDEX ix_lwr_structversion_target ON struct_version(LOWER(target));
CREATE INDEX ix_lwr_structversion_description ON struct_version(LOWER(description));

CREATE INDEX ix_lwr_structfile_pdbidentifier ON struct_file(LOWER(pdb_identifier));
CREATE INDEX ix_lwr_structfile_uploadpath ON struct_file(LOWER(from_location));
CREATE INDEX ix_lwr_structfile_uploadfilename ON struct_file(LOWER(upload_file_name));
CREATE INDEX ix_lwr_structfile_uploadfileext ON struct_file(LOWER(upload_file_ext));
CREATE INDEX ix_lwr_structfile_actualfilename ON struct_file(LOWER(actual_file_name));
CREATE INDEX ix_lwr_structfile_actualfileext ON struct_file(LOWER(actual_file_ext));

CREATE INDEX ix_lwr_pdbfile_pdbidentifier ON pdb_file(LOWER(pdb_identifier));
CREATE INDEX ix_lwr_pdbfile_classification ON pdb_file(LOWER(classification));
CREATE INDEX ix_lwr_pdbfile_title ON pdb_file(LOWER(title));
CREATE INDEX ix_lwr_pdbfile_caveat ON pdb_file(LOWER(caveat));
CREATE INDEX ix_lwr_pdbfile_expdta ON pdb_file(LOWER(expdta));
CREATE INDEX ix_lwr_pdbfile_mdltyp ON pdb_file(LOWER(mdltyp));
CREATE INDEX ix_pdbfile_nummdl ON pdb_file(nummdl);
CREATE INDEX ix_pdbfile_resolutionmeasure on pdb_file(resolution_measure);

-- CREATE INDEX ix_lwr_chain_sequencefull ON chain(LOWER(sequence_full));  cant really index a text field in postgresql - search on error msg: index row size XXXX exceeds maximum 2712 for index...
CREATE INDEX ix_lwr_chain_name ON chain(LOWER(name));
-- CREATE INDEX ix_lwr_chain_type ON chain(LOWER(type));

CREATE INDEX ix_lwr_pdbcompnd_value ON pdb_compnd(LOWER(value));

-- CREATE INDEX ix_lwr_pdbsource_value ON pdb_source(LOWER(value));

CREATE INDEX ix_lwr_pdbkeyword_value ON pdb_keyword(LOWER(value));

CREATE INDEX ix_lwr_pdbauthor_value ON pdb_author(LOWER(value));

CREATE INDEX ix_lwr_pdbjrnl_auth ON pdb_jrnl(LOWER(auth));
CREATE INDEX ix_lwr_pdbjrnl_titl ON pdb_jrnl(LOWER(titl));

CREATE INDEX ix_lwr_client_name ON client(LOWER(name));

CREATE INDEX ix_lwr_program_name ON program(LOWER(name));

CREATE INDEX ix_lwr_note_comment ON note(LOWER(comment));






-- ---------------------------------------------------------------------------------------
--create some views to help with gettin' chain data (seqres and atom coords)


drop view if exists SEQUENCES_SEQRES_VIEW;

create or replace view SEQUENCES_SEQRES_VIEW as
  select 
    s.from_rcsb         as from_rcsb,
    s.source_name       as source_name,
    s.is_obsolete       as is_obsolete,
    s.obsolete_date     as obsolete_date,
    sv.structure_id     as structure_id,
    sv.struct_ver_id    as struct_ver_id,
    sv.version          as version,
    pf.title            as title,
    pf.pdb_identifier   as pdb_identifier,
    c.chain_id          as chain_id,
    c.name              as chain_name,
    c.type              as chain_type,
    c.residue_count     as residue_count,
    c.seq_theoretical   as seq_theoretical

  from chain c
    left join pdb_file pf
      on c.struct_ver_id = pf.struct_ver_id
    left join struct_version sv
      on c.struct_ver_id = sv.struct_ver_id
    left join structure s
      on sv.structure_id = s.structure_id
;



drop view if exists SEQUENCES_ATOM_VIEW;

create or replace view SEQUENCES_ATOM_VIEW as
  select 
    s.from_rcsb          as from_rcsb,
    s.source_name        as source_name,
    s.is_obsolete        as is_obsolete,
    s.obsolete_date      as obsolete_date,
    sv.structure_id      as structure_id,
    sv.version           as version,
    sv.struct_ver_id     as struct_ver_id,
    pf.title             as title,
    pf.pdb_identifier    as pdb_identifier,
    c.name               as chain_name,
    c.type               as chain_type,
    c.residue_count      as residue_count,
    ag.atom_grouping_id  as atom_grouping_id,
    ag.type              as atom_grouping_type,
    ag.seq_with_all_coords      as seq_with_all_coords,
    ag.seq_with_any_coords      as seq_with_any_coords,
    ag.seq_with_ca_coords       as seq_with_ca_coords,
    ag.seq_with_bkbn_coords     as seq_with_bkbn_coords,
    ag.seq_with_bkbn_cb_coords  as seq_with_bkbn_cb_coords
    
  from atom_grouping ag  
    left join chain c
      on ag.chain_id = c.chain_id
    left join pdb_file pf
      on c.struct_ver_id = pf.struct_ver_id
    left join struct_version sv
      on c.struct_ver_id = sv.struct_ver_id
    left join structure s
      on sv.structure_id = s.structure_id
;
