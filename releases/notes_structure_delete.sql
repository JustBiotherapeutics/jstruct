-- -----------------------------------------------------------------------------
-- query for data by structure_id
-- -----------------------------------------------------------------------------

--structure
select * from structure where structure_id = 132394;

--structure version
select * from struct_version where structure_id = 132394;

--struct label 
select * from structure_label where structure_id = 132394;

--structure role
select * from structure_role where structure_id = 132394;

--structure user
select * from structure_user where structure_id = 132394;

--note
select * from note where structure_id = 132394;

--

--structure file
select * from struct_file where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);

--PDB file
select * from pdb_file where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);

--PDB *
select * from pdb_split   where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_keyword where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_author  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_jrnl    where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_compnd  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_source  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_revdat  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_remark  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_ssbond  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
select * from pdb_seqres  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);

--

--file import job
select * from file_import_job where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);

--chain
select * from chain where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);

--atom grouping
select * from atom_grouping where chain_id in (select chain_id from chain where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394));

--residue
select * from residue where atom_grouping_id in (select atom_grouping_id from atom_grouping where chain_id in (select chain_id from chain where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394)));

--seq_residue
select * from seq_residue where chain_id in (select chain_id from chain where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394));


-- -----------------------------------------------------------------------------
-- perform the deletes
-- -----------------------------------------------------------------------------

--seq_residue
delete from seq_residue where chain_id in (select chain_id from chain where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394));
--residue
delete from residue where atom_grouping_id in (select atom_grouping_id from atom_grouping where chain_id in (select chain_id from chain where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394)));
--atom grouping
delete from atom_grouping where chain_id in (select chain_id from chain where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394));
--chain
delete from chain where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
--file import job
delete from file_import_job where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);

--PDB *
delete from pdb_split   where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_keyword where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_author  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_jrnl    where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_compnd  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_source  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_revdat  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_remark  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_ssbond  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
delete from pdb_seqres  where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);

--PDB file
delete from pdb_file where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);
--structure file
delete from struct_file where struct_ver_id in (select struct_ver_id from struct_version where structure_id = 132394);

--note
delete from note where structure_id = 132394;
--structure user
delete from structure_user where structure_id = 132394;
--structure role
delete from structure_role where structure_id = 132394;
--struct label 
delete from structure_label where structure_id = 132394;
--structure version
delete from struct_version where structure_id = 132394;
--structure
delete from structure where structure_id = 132394;




