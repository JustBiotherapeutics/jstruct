
-- -----------------------------------------------------------------------------
-- drop everything that exists
drop schema public cascade;
create schema public;
drop owned by jstruct_rw;
drop user if exists jstruct_rw;

--couple of extensions
CREATE EXTENSION btree_gist;
CREATE EXTENSION pg_trgm;


-- -----------------------------------------------------------------------------
--create users and all their permissions

--align password with .credMgr file
create user jstruct_rw with password '********';

grant usage on schema public to jstruct_rw;

ALTER DEFAULT PRIVILEGES IN schema public GRANT ALL PRIVILEGES ON TABLES TO jstruct_rw;
ALTER DEFAULT PRIVILEGES IN schema public GRANT ALL PRIVILEGES ON SEQUENCES TO jstruct_rw;


-- JSTRUCT_USER (note: PK will be created after default users are added)
drop table if exists jstruct_user;
create table jstruct_user (
  user_id           bigserial                  not null,
  uid               varchar(1000)              not null,
  name              varchar(100)               not null,
  email             varchar(100)               not null,
  img_url           varchar(256)               null,
  status            varchar(100)               not null,
  role_developer    char                       not null,
  role_admin        char                       not null,
  role_contribute   char                       not null,
  insert_date       timestamp with time zone   not null,
  insert_user_id    bigint                     not null,
  update_date       timestamp with time zone   not null,
  update_user_id    bigint                     not null
);

--indexes
create unique index ux_jstructuser_uid on jstruct_user(uid);

--insert default users
insert into jstruct_user (user_id, uid, name, email, img_url, status, role_developer, role_admin, role_contribute, insert_date, insert_user_id, update_date, update_user_id)
VALUES (1, 'JSTRUCT_SYSTEM',    'System',              'russell.williams@justbiotherapeutics.com',  null,  'ACTIVE',   'N', 'Y', 'Y', current_timestamp, 1, current_timestamp, 1);

insert into jstruct_user (user_id, uid, name, email, img_url, status, role_developer, role_admin, role_contribute, insert_date, insert_user_id, update_date, update_user_id)
VALUES (2, 'JSTRUCT_GUEST',     'Guest',               'do_not_reply@justbiotherapeutics.com',      null,  'ACTIVE',   'N', 'N', 'N', current_timestamp, 1, current_timestamp, 1);

--insert into jstruct_user (user_id, uid, name, email, img_url, status, role_developer, role_admin, role_contribute, insert_date, insert_user_id, update_date, update_user_id)
--values (3, 'rwilliams',         'Russell Williams',    'russell.williams@justbiotherapeutics.com',  null,  'ACTIVE', 'Y', 'Y', 'Y', current_timestamp, 1, current_timestamp, 1);

insert into jstruct_user (user_id, uid, name, email, img_url, status, role_developer, role_admin, role_contribute, insert_date, insert_user_id, update_date, update_user_id)
values (21, 'TEST_NO_ROLE',     'Ro Noles',            'russell.williams@justbiotherapeutics.com',  null,  'ACTIVE',   'N', 'N', 'N', current_timestamp, 1, current_timestamp, 1);

insert into jstruct_user (user_id, uid, name, email, img_url, status, role_developer, role_admin, role_contribute, insert_date, insert_user_id, update_date, update_user_id)
values (22, 'TEST_CONTRIBUTE',  'Charlie Chaplin',     'russell.williams@justbiotherapeutics.com',  null,  'ACTIVE',   'N', 'N', 'Y', current_timestamp, 1, current_timestamp, 1);

insert into jstruct_user (user_id, uid, name, email, img_url, status, role_developer, role_admin, role_contribute, insert_date, insert_user_id, update_date, update_user_id)
values (23, 'TEST_ADMIN',       'Adam Ant',            'russell.williams@justbiotherapeutics.com',  null,  'ACTIVE',   'N', 'Y', 'N', current_timestamp, 1, current_timestamp, 1);

insert into jstruct_user (user_id, uid, name, email, img_url, status, role_developer, role_admin, role_contribute, insert_date, insert_user_id, update_date, update_user_id)
values (24, 'TEST_INACTIVE',    'Isabella Rossellini', 'russell.williams@justbiotherapeutics.com',  null,  'INACTIVE', 'Y', 'Y', 'Y', current_timestamp, 1, current_timestamp, 1);

--create the primary key constraint (once the default users have been created)
alter table jstruct_user add constraint pk_jstruct_user_user_id primary key (user_id);

--restart the implicit jstruct_user.user_id sequence (jstruct_user_user_id_seq) with 101
alter sequence jstruct_user_user_id_seq RESTART with 101;
CREATE INDEX ix_jstructuser_uid ON jstruct_user(uid);
CREATE INDEX ix_jstructuser_name ON jstruct_user(name);
CREATE INDEX ix_jstructuser_status ON jstruct_user(status);





-- VALUE_MAP
drop table if exists value_map;
create table value_map (
  VALUE_MAP_ID   bigserial primary key   not null,
  ITEM_GROUP	   varchar(100)            not null,
  ITEM_KEY	   varchar(100)            not null,
  ITEM_VALUE	   varchar(2000)           null,
  DESCRIPTION	   varchar(2000)           not null,
  VALID_URL      char                    not null,
  VALID_EMAIL    char                    not null,
  VALID_EMAILS   char                    not null,
  VALID_INT      char                    not null,
  initialized    char                    not null
);
--start sequence at 101
ALTER SEQUENCE value_map_value_map_id_seq RESTART WITH 101;
-- indexes
CREATE INDEX ix_valuemap_itemgroup ON value_map (ITEM_GROUP);
CREATE UNIQUE INDEX ux_valuemap_itemkey ON value_map(ITEM_KEY);

--initial values for value_map table

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'EMAIL', 'MailHost', 'smtp-relay.gmail.com', 'N','N','N','N',
        'Mail host used for sending SMTP emails');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'EMAIL', 'ErrorEmailRecipients', 'russell.williams@justbiotherapeutics.com', 'N','N','Y','N',
        'ErrorEmailRecipients are sent an email containing the stacktrace for any exceptions thrown by the application');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'EMAIL', 'ProcessEmailRecipients', 'russell.williams@justbiotherapeutics.com', 'N','N','Y','N',
        'ProcessEmailRecipients receive emails when import job/run is completed');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'EMAIL', 'DevTestRedirectRecipients', 'russell.williams@justbiotherapeutics.com', 'N','N','Y','N',
        'ALL emails are redirected to this email in dev and test. Does not affect production');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'EMAIL', 'EmailDefaultSender', 'russell.williams@justbiotherapeutics.com', 'N','Y','N','N',
        'Default email sender for most (all) messages sent from JStruct. Must be a single properly formatted email address');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY, ITEM_VALUE, valid_url, valid_email, valid_emails, valid_int, DESCRIPTION)
values ('N', 'EMAIL', 'NewUserRecipients', 'russell.williams@justbiotherapeutics.com', 'N','N','Y','N',
        'NewUserRecipients are sent an email when a new user is added/logs in.');


insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'SYSTEM', 'LocalTmpDirectory', '/web/tmp', 'N','N','N','N',
        'Local temporary directory used when processing structure files');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY, ITEM_VALUE, valid_url, valid_email, valid_emails, valid_int, DESCRIPTION)
values ('N', 'SYSTEM', 'DefaultRole', 'Contributor', 'N','N','N','N',
        'Role granted to new users of the system; this MUST be one of: none, Contributor, Admin. Multiple values are allowed if seperated by comma or space.');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'SYSTEM', 'ClientSupportType', 'required', 'N','N','N','N',
        'Value determining use of -Client- dropdown field. Must be one of: none | optional | required');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'SYSTEM', 'DefaultStructurePermission', 'none', 'N','N','N','N',
        'Initial default permission for manually uploaded Structures; must be one of: none | Read | Write');



insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'IMPORT', 'RcsbImportCron', '0 0 21 ? * FRI', 'N','N','N','N',
        'Schedule for the RCSB import job. Must be a valid Quartz cron expression');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('Y', 'IMPORT', 'CronUrl', 'http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/crontrigger', 'Y','N','N','N',
        'URL where more info can be found on the Quartz cron expression');


insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('Y', 'URL', 'RcsbBaseUrl', 'http://www.rcsb.org/pdb/explore.do?structureId=', 'Y','N','N','N',
        'Base URL for viewing active structure information from RCSB (aka: the RCSB web page to view the active structure)');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('Y', 'URL', 'RcsbObsoleteUrl', 'http://www.rcsb.org/pdb/explore/obsolete.do?obsoleteId=', 'Y','N','N','N',
        'Base URL for viewing obsolete structure information from RCSB (aka: the RCSB web page to view the structure once it becomes obsolete)');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('Y', 'URL', 'RcsbRestFileUrl', 'http://cftp.rcsb.org/view/', 'Y','N','N','N',
        'Base URL for viewing structure file content from RCSB');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('Y', 'URL', 'RcsbRestDownloadUrl', 'http://ftp.rcsb.org/download/', 'Y','N','N','N',
        'Base URL for downloading structure files from RCSB');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY, ITEM_VALUE, valid_url, valid_email, valid_emails, valid_int, DESCRIPTION)
values ('Y', 'URL', 'RcsbViewerUrl', 'http://www.rcsb.org/pdb/ngl/ngl.do?pdbid=', 'Y','N','N','N',
        'Base URL for a 3D structure viewer offered from RCSB');



insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY,ITEM_VALUE, valid_url,valid_email,valid_emails,valid_int, DESCRIPTION)
values ('N', 'STRUCTURE', 'StructureIdPrefix', 'JSTRUCT-', 'N','N','N','N',
        'Prefix appended to the Structure ID assigned by JStruct. Alphanumeric characters only.');

insert into VALUE_MAP (initialized, ITEM_GROUP,ITEM_KEY, ITEM_VALUE, valid_url, valid_email, valid_emails, valid_int, DESCRIPTION)
values ('N', 'STRUCTURE', 'ManualUploadSource', 'Just', 'N','N','N','N',
        'Source for manually uploaded Structure files; likely a short name to identify your company');

