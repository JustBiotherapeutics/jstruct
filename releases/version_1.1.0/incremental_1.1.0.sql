-- ---------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------
--      incremental SQL for version 1.1.0
-- ---------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------

select * from analysis_script;
select * from analysis_script_ver;
select * from analysis_file;

select * from analysis_run;
select * from analysis_task;

-- ---------------------------------------------------------------------------------------
-- analysis tables for persisting analysis script, runs, and tasks
-- ---------------------------------------------------------------------------------------

--drop all for testing
drop table if exists analysis_task;
drop table if exists analysis_run;
drop table if exists analysis_file;
drop table if exists analysis_script_ver;
drop table if exists analysis_script;




-- ANALYSIS_SCRIPT
drop table if exists analysis_script;
create table analysis_script 
(
  analysis_script_id    bigserial primary key      not null,

  owner_id              bigint                     not null,
  script_name           varchar(255)               not null,
  description           varchar(2000)              null,
  script_type           varchar(255)               not null,
  is_global             char                       not null,

  insert_date         timestamp with time zone     not null,
  insert_user_id      bigint                       not null,
  delete_date         timestamp with time zone     null,
  delete_user_id      bigint                       null
);
--foreign keys
alter table analysis_script add foreign key (owner_id) references jstruct_user(user_id);
alter table analysis_script add foreign key (insert_user_id) references jstruct_user(user_id);
alter table analysis_script add foreign key (delete_user_id) references jstruct_user(user_id);
--indexes
create index ix_analysisscript_ownerid on analysis_script(owner_id);
create index ix_analysisscript_insertuserid on analysis_script(insert_user_id);
create index ix_analysisscript_deleteuserid on analysis_script(delete_user_id);
create index ix_analysisscript_scriptname on analysis_script(script_name);
--start sequence at 11
ALTER SEQUENCE analysis_script_analysis_script_id_seq RESTART WITH 11;



-- ANALYSIS_SCRIPT_VER
drop table if exists analysis_script_ver;
create table analysis_script_ver 
(
  analysis_script_ver_id    bigserial primary key   not null,

  analysis_script_id        bigint                  not null,
  owner_id                  bigint                  not null,
  comments                  varchar(2000)           null,
  default_command           varchar(2000)           not null,
  status                    varchar(255)            not null,
  version                   integer                 not null,

  insert_date            timestamp with time zone   not null,
  insert_user_id         bigint                     not null,
  overwrite_date         timestamp with time zone   null,
  overwrite_user_id      bigint                     null
);
--foreign keys
alter table analysis_script_ver add foreign key (analysis_script_id) references analysis_script(analysis_script_id);
alter table analysis_script_ver add foreign key (owner_id) references jstruct_user(user_id);
alter table analysis_script_ver add foreign key (insert_user_id) references jstruct_user(user_id);
alter table analysis_script_ver add foreign key (overwrite_user_id) references jstruct_user(user_id);
--indexes
create index ix_analysisscriptver_analysisscriptid on analysis_script_ver(analysis_script_id);
create index ix_analysisscriptver_ownerid on analysis_script_ver(owner_id);
create index ix_analysisscriptver_insertuserid on analysis_script_ver(insert_user_id);
create index ix_analysisscriptver_overwriteuserid on analysis_script_ver(overwrite_user_id);
create index ix_analysisscriptver_status on analysis_script_ver(status);
create index ix_analysisscriptver_version on analysis_script_ver(version);
--start sequence at 101
ALTER SEQUENCE analysis_script_ver_analysis_script_ver_id_seq RESTART WITH 101;




-- ANALYSIS_FILE
drop table if exists analysis_file;
create table analysis_file 
(
  analysis_file_id          bigserial primary key     not null,

  analysis_script_ver_id    bigint                    not null,
  file_name                 varchar(255)              not null,
  file_data                 bytea                     null
);
--foreign keys
alter table analysis_file add foreign key (analysis_script_ver_id) references analysis_script_ver(analysis_script_ver_id);
--indexes
create index ix_analysisfile_analysisscriptverid on analysis_file(analysis_script_ver_id);
--start sequence at 2001
ALTER SEQUENCE analysis_file_analysis_file_id_seq RESTART WITH 2001;




-- ANALYSIS_RUN
drop table if exists analysis_run;
create table analysis_run 
(
  analysis_run_id           bigserial primary key   not null,

  analysis_script_ver_id    bigint                  not null,
  user_id                   bigint                  not null,
  command_pattern           varchar(2000)           not null,
  description               varchar(2000)           null,
  status                    varchar(255)            not null,
  total_task_count          integer                 not null,
  successful_task_count     integer                 not null,
  failed_task_count         integer                 not null,

  start_date            timestamp with time zone    not null,
  end_date              timestamp with time zone    null
);
--foreign keys
alter table analysis_run add foreign key (analysis_script_ver_id) references analysis_script_ver(analysis_script_ver_id);
alter table analysis_run add foreign key (user_id) references jstruct_user(user_id);
--indexes
create index ix_analysisrun_analysisscriptverid on analysis_run(analysis_script_ver_id);
create index ix_analysisrun_analysisuserid on analysis_run(user_id);
create index ix_analysisrun_analysisstatus on analysis_run(status);
--start sequence at 1001
ALTER SEQUENCE analysis_run_analysis_run_id_seq RESTART WITH 1001;



-- ANALYSIS_TASK
drop table if exists analysis_task;
create table analysis_task 
(
  analysis_task_id          bigserial primary key   not null,

  analysis_run_id           bigint                  not null,
  struct_ver_id             bigint                  not null,
  command                   varchar(2000)           not null,
  status                    varchar(255)            not null,
  error                     text                    null,

  start_date            timestamp with time zone    not null,
  end_date              timestamp with time zone    null
);
--foreign keys
alter table analysis_task add foreign key (analysis_run_id) references analysis_run(analysis_run_id);
alter table analysis_task add foreign key (struct_ver_id) references struct_version(struct_ver_id);
--indexes
create index ix_analysistask_analysisrunid on analysis_task(analysis_run_id);
create index ix_analysistask_structverid on analysis_task(struct_ver_id);
create index ix_analysistask_status on analysis_task(status);
--start sequence at 10001
ALTER SEQUENCE analysis_task_analysis_task_id_seq RESTART WITH 10001;








